<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> 
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />     

<% beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="1300px" align="center" class="fondoTabla">
    <tr>
        <td>
            <div align="center" id="tituloForma" >
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="COORDINACION TERAPIA" />
                </jsp:include>
            </div>	
        </td>
    </tr> 
    <tr>
        <td>
            <table width="100%" height="500">
                <tr>
                    <td>
                        <table width="100%" >
                            <tr class="titulosListaEspera" align="center">
                                <td colspan="3">PACIENTE</td> 
                                <td width="15%">FECHA DESDE</td>    
                                <td width="15%">FECHA HASTA</td>  
                                <td width="15%">PROGRAMADO</td>  
                            </tr>
                            <tr class="estiloImput"> 
                                <td bgcolor="#FD93B8">
                                    <select size="1" id="cmbTipoId" style="width:80%" tabindex="100" >
                                        <option value=""></option>
                                        <%     resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(105);
                                            ComboVO cmb105;
                                            for (int k = 0; k < resultaux.size(); k++) {
                                                cmb105 = (ComboVO) resultaux.get(k);
                                        %>
                                        <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                                            <%= cmb105.getId() + "  " + cmb105.getDescripcion()%></option>
                                            <%}%>				
                                    </select>

                                </td>
                                <td>
                                    <input  type="text" id="txtIdentificacion"  placeholder="N&uacute;mero de Documento" onKeyPress="javascript:checkKey2(event);" />	
                                </td>
                                <td>
                                    <input type="text" id="txtIdBusPaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPaciente', 307)"  style="width:99%"/>
                                </td>                   
                                <td ><input type="text" id="txtDocFechaDesde" style="width:50%"/></td>    
                                <td ><input type="text" id="txtDocFechaHasta" style="width:50%"/></td>     
                                <td>
                                    <select id="cmbProgramacion" style="width:90%"  >
                                        <option value=""></option>
                                        <option value="N">SIN PROGRAMAR</option>
                                        <option value="S">PROGRAMADO</option>					
                                    </select>
                                </td>                         

                            </tr>	 
                            <tr class="titulosListaEspera"> 
                                <td width="15%">SEDE</td>
                                <td width="15%">ESPECIALIDAD</td> 
                                <td width="25%">PROFESIONAL</td> 
                                <td>DEPARTAMENTO</td> 
                                <td>MUNICIPIO</td>
                                <td>COMUNA / LOCALIDAD</td>
                            </tr>     
                            <tr class="estiloImput">
                                <td>
                                    <select id="cmbIdSedeBus" style="width:90%" onfocus="comboDependienteSede('cmbIdSedeBus', '557')" onchange="asignaAtributoCombo('cmbIdEspecialidadBus', '', ''); asignaAtributoCombo('cmbIdProfesionales', '', '');">
                                    </select>
                                </td>          
                                <td><select id="cmbIdEspecialidadBus" style="width:90%" onfocus="cargarEspecialidadDesdeSede('cmbIdSedeBus', 'cmbIdEspecialidadBus')" onchange="asignaAtributoCombo('cmbIdProfesionales', '', '')" >	                                        
                                        <option value=""></option>                           					
                                    </select>	                   
                                </td>                   
                                <td><select id="cmbIdProfesionales" onfocus="cargarProfesionalesDesdeEspecialidadDocumentos(this.id)" style="width:90%"  >	                                        
                                        <option value="" ></option>                                             
                                    </select>	                   
                                </td> 
                                <td>
                                    <select id="cmbIdDepartamento" style="width:90%" 
                                            onfocus="cargarDepDesdeServicio('txtIdTipoServicio', 'cmbIdDepartamento')"
                                            onchange="limpiaAtributo('cmbIdMunicipio', 0); limpiaAtributo('cmbIdLocalidad', 0);">	                                        
                                        <option value=""></option>
                                    </select>	                   
                                </td>   
                                <td>
                                    <select size="1" id="cmbIdMunicipio" style="width:90%"  
                                            onfocus="cargarMunicipioDesdeDepartamento('cmbIdDepartamento', 'txtIdTipoServicio', 'cmbIdMunicipio')"
                                            onchange="limpiaAtributo('cmbIdLocalidad', 0);">	                                        
                                        <option value=""></option>                                  
                                    </select>	                   
                                </td>  
                                <td>
                                    <select id="cmbIdLocalidad" style="width:90%" tabindex="14"
                                            onfocus="cargarLocalidad('cmbIdDepartamento', 'cmbIdMunicipio', 'txtIdTipoServicio', 'cmbIdLocalidad')">
                                        <option value=""></option>
                                    </select>	                   
                                </td>    

                            </tr>     

                            <tr class="estiloImput" >
                                <td colspan="6">
                                    <input type="button" class="small button blue" value="BUSCAR FOLIOS" onclick="buscarAGENDA('listCoordinacionTerapia');"   />                   
                                    <input type="button" class="small button blue" value="VISTA PREVIA" onClick="formatoPDFHC();"/>  
                                    <input type="button" class="small button blue" value="LIMPIAR FILTROS" onclick="limpiarDivEditarJuan('limpiarCoordinacionTerapia')"   />                   

                                  </td>    
                            </tr>

                            <tr>
                                <td colspan="6" height="400">
                                    <table id="listCoordinacionTerapia"></table>            
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                              
                <tr>       
                    <td>  
                        <table  width="100%">
                            <tr class="titulosListaEspera"> 
                                <td>ID_PACIENTE</td>
                                <td>IDENTIFICACION</td>
                                <td>NOMBRE PACIENTE</td>  
                                <td>ID FOLIO</td>
                                <td>FOLIO</td>  
                                <td>ID ADMISION</td> 
                                <td>ESPECIALIDAD</td>
                                <td>FACTURACION</td>
                                                
                            </tr>
                            <tr class="estiloImput">
                                <td><label id="lblIdPaciente"></label></td>
                                <td><label id="lblIdentificacion"></label></td>
                                <td><label id="lblNombrePaciente"></label></td>   
                                <td><label id="lblIdDocumento"></label></td>     
                                <td><label id="lblTipoDocumento"></label>-<label id="lblDescripcionTipoDocumento"></label></td>    
                                <td><label id="lblIdAdmision"></label></td>
                                <td><label id="lblEspecialidad"></label></td>
                                <td><label id="lblIdPlan"></label>-<label id="lblPlan"></label></td>
                            </tr>
                            </table>
                            </td>
                </tr>

<tr>


    <td colspan="6">

        <div id="tabsCoordinacionTerapia" style="width:98%; height:auto">
           <ul>
              <li> <a href="#divProcedimientosOrdenados" onclick="buscarAGENDA('listCoordinacionTerapiaOrdenados');tabActivo='divProcedimientosOrdenados'""><center>PROCEDIMIENTOS ORDENADOS</center></a> </li>
             <li> <a href="#divProcedimientosHistoricos" onclick="buscarAGENDA('listCoordinacionTerapiaHistoricos');tabActivo='divProcedimientosHistoricos'""><center>PROCEDIMIENTOS HISTORICOS</center></a> </li>
           </ul>
           <div id="divProcedimientosOrdenados" style="width:98%;"> 
              <table width="100%" style="background-color: #E8E8E8;">
                <tr>
                    <td class="titulos" height="200">
                        <table id="listCoordinacionTerapiaOrdenados"></table>            
                    </td>
                </tr>
              </table>
              </div>

            <div id="divProcedimientosHistoricos" style="width:98%"> 
                <table width="100%" style="background-color: #E8E8E8;">
                    <tr>
                        <td class="titulos" height="200">
                            <table id="listCoordinacionTerapiaHistoricos"></table>            
                        </td>
                    </tr>
                  </table>
             
          </div>  
        
        
        </div>

     </td>
    </tr>

<tr>
    <td colspan="6" class="titulosCentrados" style="font-size: 13px">PROGRAMACION TERAPIAS</td>
</tr>

<tr>
<td colspan="6">

    <table width="100%" cellpadding="0" cellspacing="0" align="center">
        <tr class="titulosListaEspera">
          <td width="15%">SEDE</td>
          <td width="15%">ESPECIALIDAD</td>
          <td width="15%">TIPO CITA</TD>
          <td width="15%">PROFESIONAL</td>
          <td width="10%">FECHA INICIO</td>
          <td width="10%">CANTIDAD CITAS</td>
          <td width="10%">FRECUENCIA</td>
          <td width="10%"></td>
        </tr>
        <tr class="estiloImput">
          <td>
            <select size="1" id="cmbSedeTerapia" style="width:95%" title="GD48"
              onfocus="comboDependienteSede('cmbSedeTerapia','1127')"
              onchange="asignaAtributo('cmbIdEspecialidadTerapia', '', 0); asignaAtributo('cmbProfesionalesTerapia', '', 0);cargarTipoCitaSegunEspecialidad('cmbTipoCitaTerapia', 'cmbIdEspecialidad')">
            </select>
          </td>
          <td>
            <select size="1" id="cmbIdEspecialidadTerapia" style="width:99%" onfocus="cargarEspecialidadDesdeSede('cmbSedeTerapia', 'cmbIdEspecialidadTerapia')"
            onchange="asignaAtributo('cmbTipoCitaTerapia', '', 0)">	                                        
              <option value=""></option> 
            </select>
          </td>
          <td>
            <select id="cmbTipoCitaTerapia"  onfocus="cargarCitaSegunEspecialidad(this.id,'cmbIdEspecialidadTerapia')"  style="width:90%" tabindex="120" >
              <option value=""></option>
            </select>  
          </td>
          <td>
            <select id="cmbProfesionalesTerapia" style="width:80%" onfocus="cargarProfesionalDesdeEspecialidad('cmbIdEspecialidadTerapia',this.id)" tabindex="14">
              <option value="" selected="">[ Seleccione ]</option>
            </select>
          </td>

            <td ><input id="txtFechaInicioTerapia" type="text"  /></td> 
          
          <td>

            <select id="cmbCantidadCitasTerapia"  style="width:80%" >	                                        
              <option value=""></option>	
              <option value="1">1</option>						
              <option value="2">2</option>	
              <option value="3">3</option>						
              <option value="4">4</option>						                                                                  					
              <option value="5">5</option>						
              <option value="6">6</option>	
              <option value="7">7</option>						
              <option value="8">8</option>	
              <option value="9">9</option>	
              <option value="10">10</option>						
              <option value="11">11</option> 
              <option value="12">12</option>	
              <option value="13">13</option>						
              <option value="14">14</option>						                                                                  					
              <option value="15">15</option>						
              <option value="16">16</option>	
              <option value="17">17</option>						
              <option value="18">18</option>	
              <option value="19">19</option>	
              <option value="20">20</option>	 
              <option value="21">21</option> 
              <option value="22">22</option>	
              <option value="23">23</option>						
              <option value="24">24</option>						                                                                  					
              <option value="25">25</option>						
              <option value="26">26</option>	
              <option value="27">27</option>						
              <option value="28">28</option>	
              <option value="29">29</option>	
              <option value="30">30</option> 								                                                                     
                          </select>  

          </td>
<td>

<select id="cmbFrecuenciaTerapia"  style="width:80%">
  <option value=""></option>	
  <option value="1">1 dia</option>
  <option value="2">2 dias</option>
  <option value="3">3 dias</option>
  <option value="4">4 dias</option>
  <option value="5">5 dias</option>
  <option value="6">6 dias</option>
  <option value="7">7 dias</option>
  <option value="8">8 dias</option>
  <option value="9">9 dias</option>
  <option value="10">10 dias</option>
  <option value="11">11 dias</option>
  <option value="12">12 dias</option>
  <option value="13">13 dias</option>
  <option value="14">14 dias</option>
  <option value="15">15 dias</option>
</select>

</td>

          <td ><input type="button" class="small button blue" title="BL231"
            value="Programar" onClick="modificarCRUD('crearCitasTerapias')" /></td>     
        </tr> 
        
        <tr>
          <td class="titulos" colspan="8" height="200">
            <table id="listaProgramacionTerapias"></table> 
          </td>
        </tr>
      </table>

</td>

</tr>



</tr>

                           	                         
                        </table>   
      

                        <div id="divVentanitaEditarTerapia" style="display:none; z-index:2000; top:400px; left:50px; ">

                            <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
                            </div>
                            <div style="z-index:2004; position:fixed; top:300px;left:400px; width:90%">
                                <table width="70%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
                                    <tr class="estiloImput">
                                        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                                                onclick="ocultar('divVentanitaEditarTerapia')" /></td>
                                        <td colspan="6">&nbsp;</td>
                                        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                                                 onclick="ocultar('divVentanitaEditarTerapia')"/></td>
                                    </tr>
                                    <tr class="titulosListaEspera">
                                        <td width="10%">ID</td>
                                        <td width="20%">SEDE</td>
                                        <td width="20%">ESPECIALIDAD</td>
                                        <td width="20%">TIPO CITA</td>
                                        <td width="20%">PROFESIONAL</td>
                                        <td width="10%"> FECHA INICIO</td>
                                    </tr>
                                    <tr class="estiloImput">
                                      <td><label id="lblIdProgramacionTerapiaEditar"></label></td>
                          
                                      <td>
                                        <select size="1" id="cmbSedeTerapiaEditar" style="width:95%" title="GD48"
                                          onfocus="comboDependienteSede('cmbSedeTerapiaEditar','1127')"
                                          onchange="asignaAtributo('cmbIdEspecialidadTerapiaEditar', '', 0); asignaAtributo('cmbProfesionalesTerapiaEditar', '', 0);cargarTipoCitaSegunEspecialidad('cmbTipoCitaTerapiaEditar', 'cmbIdEspecialidadEditar')">
                                        </select>
                                      </td>
                                      <td>
                                        <select size="1" id="cmbIdEspecialidadTerapiaEditar" style="width:99%" onfocus="cargarEspecialidadDesdeSede('cmbSedeTerapiaEditar', 'cmbIdEspecialidadTerapiaEditar')"
                                        onchange="asignaAtributo('cmbTipoCitaTerapiaEditar', '', 0)">	                                        
                                          <option value=""></option> 
                                        </select>
                                      </td>
                                      <td>
                                        <select id="cmbTipoCitaTerapiaEditar"  onfocus="cargarCitaSegunEspecialidad(this.id,'cmbIdEspecialidadTerapiaEditar')"  style="width:90%" tabindex="120" >
                                          <option value=""></option>
                                        </select>  
                                      </td>
                                      <td>
                                        <select id="cmbProfesionalesTerapiaEditar" style="width:80%" onfocus="cargarProfesionalDesdeEspecialidad('cmbIdEspecialidadTerapiaEditar',this.id)" tabindex="14">
                                          <option value="" selected="">[ Seleccione ]</option>
                                        </select>
                                      </td>
                                        <td ><input id="txtFechaInicioTerapiaEditar" type="text" /></td> 
                                    </tr>
                                    <tr class="estiloImputListaEspera">
                                        <td colspan="9">
                                            <input id="BTN_MO" title="BI101" type="button" class="small button blue"
                                                   value="MODIFICAR"
                                                   onclick="modificarCRUD('modificarProgramacionTerapia')"
                                                   tabindex="309" />
                                            <input id="BTN_MO" title="BI102" type="button" class="small button blue"
                                                   value="ELIMINAR"
                                                   onclick="modificarCRUD('eliminarProgramacionTerapia')"
                                                   tabindex="309" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                          </div>



<input type="hidden" id="txtPrincipalHC" value="" />  
<input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />       
<input type="hidden"  id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden"  id="txtTipoDocumentoVistaPrevia" value="" />                        
<input type="hidden"  id="lblTituloDocumentoVistaPrevia" value=""  />      
<input type="hidden"  id="lblIdAdmisionVistaPrevia" value=""  />          
<input type="hidden" id="txtIdTipoServicio" value="APT">    
<input type="hidden"  id="lblIdOrden" value=""  />          
<input type="hidden" id="lblIdProcedimientoAutoriza"/> 
<input type="hidden"  id="lblNomProcedimientoAutoriza"/> 
<input type="hidden"  id="lblCantidadLab"/> 
<input type="hidden" id="lblDiagnosticoLab"/> 
<input type="hidden"  id="lblIdProcedimientoLE"/> 
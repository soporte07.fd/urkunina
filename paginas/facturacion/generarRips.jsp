<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<%@ page import="javax.naming.*" %>

<%@ page import="java.io.*" %> 
<%@ page import="java.awt.Font" %> 

<%@ page import="java.util.zip.ZipEntry" %> 
<%@ page import="java.util.zip.ZipOutputStream" %> 
<%@ page import="java.util.List" %> 
<%@ page import="java.util.ArrayList" %> 

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> 


<!DOCTYPE HTML >  
<%

    boolean hay_datos = false;
    String ruta = "/opt/tomcat8/webapps/rips/";
    String archivo = "";
    Connection conexion = beanSession.cn.getConexion();

    String id_envio = request.getParameter("id_envio");
    String remision = request.getParameter("remision");
    //String nit = request.getParameter("NIT");

    String ruta_tipificacion = "/opt/tomcat8/webapps/tipificacion/";
    String ruta_archivo_tipificacion_comprimido = ruta_tipificacion + "000_" + remision + ".zip";

    List<String> srcFiles = new ArrayList();

    FileWriter fichero = null;

    PreparedStatement pr = null;
    String sql = "";
    String sqlT = "";
    String separador = ",";
    String cad_datos = "";

    /*
          RIPS US
     */
    sql = "SELECT\n "
            + "tipo_identificacion,\n "
            + "identificacion,\n "
            + "codigo_administradora,\n "
            + "tipo_usuario,\n "
            + "apellido1,\n "
            + "apellido2,\n "
            + "nombre1,\n "
            + "nombre2,\n "
            + "edad,\n "
            + "unidad_edad,\n "
            + "sexo,\n "
            + "departamento,\n "
            + "municipio,\n "
            + "residencia\n "
            + "FROM \n "
            + "facturacion.rips_us us\n "
            + "INNER JOIN facturacion.tipo_rips_archivos rips ON us.id_archivo = rips.id\n "
            + "WHERE\n "
            + "	rips.id_envio = " + id_envio;

    try {

        System.out.println("RIPS US");
        System.out.println(sql);

        archivo = ruta + "US" + remision + ".txt";

        pr = conexion.prepareStatement(sql);
        ResultSet resultado = pr.executeQuery();
        hay_datos = false;

        while (resultado.next()) {
            hay_datos = true;
            cad_datos += resultado.getString("tipo_identificacion") + separador;
            cad_datos += resultado.getString("identificacion");
            cad_datos += separador + resultado.getString("codigo_administradora");
            cad_datos += separador + resultado.getString("tipo_usuario");
            cad_datos += separador + resultado.getString("apellido1");
            cad_datos += separador + resultado.getString("apellido2");
            cad_datos += separador + resultado.getString("nombre1");
            cad_datos += separador + resultado.getString("nombre2");
            cad_datos += separador + resultado.getString("edad");
            cad_datos += separador + resultado.getString("unidad_edad");
            cad_datos += separador + resultado.getString("sexo");
            cad_datos += separador + resultado.getString("departamento");
            cad_datos += separador + resultado.getString("municipio");
            cad_datos += separador + resultado.getString("residencia");
            cad_datos += "\r\n";
        }

        if (hay_datos) {
            srcFiles.add(archivo);
            fichero = new FileWriter(archivo);
            fichero.write(cad_datos);
            fichero.close();
        }
    } catch (IOException ioe) {
        ioe.printStackTrace();
    }


    /*
          RIPS AP
     */
    sql = "SELECT\n "
            + "numero_factura,\n "
            + "codigo_prestador,\n "
            + "tipo_identificacion,\n "
            + "identificacion,\n "
            + "fecha_procedimiento,\n "
            + "autorizacion,\n "
            + "procedimiento,\n "
            + "ambito,\n "
            + "finalidad,\n "
            + "BTRIM(personal_atiende) personal_atiende,\n "
            + "diagnostico,\n "
            + "diagnostico_relacionado,\n "
            + "complicacion,\n "
            + "BTRIM(acto_quirurgico) acto_quirurgico,\n "
            + "valor\n "
            + "FROM \n "
            + "facturacion.rips_ap ap\n "
            + "INNER JOIN facturacion.tipo_rips_archivos rips ON ap.id_archivo = rips.id\n "
            + "WHERE \n "
            + "rips.id_envio = " + id_envio;

    try {

        System.out.println("RIPS AP");
        System.out.println(sql);
        archivo = ruta + "AP" + remision + ".txt";

        cad_datos = "";
        pr = conexion.prepareStatement(sql);
        ResultSet resultado = pr.executeQuery();
        hay_datos = false;

        while (resultado.next()) {
            hay_datos = true;
            cad_datos += resultado.getString("numero_factura") + separador;
            cad_datos += resultado.getString("codigo_prestador");
            cad_datos += separador + resultado.getString("tipo_identificacion");
            cad_datos += separador + resultado.getString("identificacion");
            cad_datos += separador + resultado.getString("fecha_procedimiento");
            cad_datos += separador + resultado.getString("autorizacion");
            cad_datos += separador + resultado.getString("procedimiento");
            cad_datos += separador + resultado.getString("ambito");
            cad_datos += separador + resultado.getString("finalidad");
            cad_datos += separador + resultado.getString("personal_atiende");
            cad_datos += separador + resultado.getString("diagnostico");
            cad_datos += separador + resultado.getString("diagnostico_relacionado");
            cad_datos += separador + resultado.getString("complicacion");
            cad_datos += separador + resultado.getString("acto_quirurgico");
            cad_datos += separador + resultado.getString("valor");
            cad_datos += "\r\n";
        }

        if (hay_datos) {
            srcFiles.add(archivo);
            fichero = new FileWriter(archivo);
            fichero.write(cad_datos);
            fichero.close();
        }
    } catch (IOException ioe) {
        ioe.printStackTrace();
    }

    /*
          RIPS AF
     */
    sql = "SELECT\n "
            + "codigo_prestador,\n "
            + "razon_social,\n "
            + "tipo_id_prestador,\n "
            + "id_prestador,\n "
            + "numero_factura,\n "
            + "fecha_expedicion,\n "
            + "fecha_inicio,\n "
            + "fecha_final,\n "
            + "codigo_administradora,\n "
            + "nombre_admisnitradora,\n "
            + "numero_contrato,\n "
            + "plan_beneficios,\n "
            + "num_poliza,\n "
            + "total_copago,\n "
            + "valor_comision,\n "
            + "total_descuentos,\n "
            + "valor_neto\n "
            + "FROM \n "
            + "	facturacion.rips_af af\n "
            + "INNER JOIN facturacion.tipo_rips_archivos rips ON af.id_archivo = rips.id\n "
            + "WHERE \n "
            + "	rips.id_envio = " + id_envio;

    try {

        System.out.println("RIPS AF");
        System.out.println(sql);
        archivo = ruta + "AF" + remision + ".txt";

        cad_datos = "";
        pr = conexion.prepareStatement(sql);
        ResultSet resultado = pr.executeQuery();
        hay_datos = false;

        while (resultado.next()) {
            hay_datos = true;
            cad_datos += resultado.getString("codigo_prestador") + separador;
            cad_datos += resultado.getString("razon_social");
            cad_datos += separador + resultado.getString("tipo_id_prestador");
            cad_datos += separador + resultado.getString("id_prestador");
            cad_datos += separador + resultado.getString("numero_factura");
            cad_datos += separador + resultado.getString("fecha_expedicion");
            cad_datos += separador + resultado.getString("fecha_inicio");
            cad_datos += separador + resultado.getString("fecha_final");
            cad_datos += separador + resultado.getString("codigo_administradora");
            cad_datos += separador + resultado.getString("nombre_admisnitradora");
            cad_datos += separador + resultado.getString("numero_contrato");
            cad_datos += separador + resultado.getString("plan_beneficios");
            cad_datos += separador + resultado.getString("num_poliza");
            cad_datos += separador + resultado.getString("total_copago");
            cad_datos += separador + resultado.getString("valor_comision");
            cad_datos += separador + resultado.getString("total_descuentos");
            cad_datos += separador + resultado.getString("valor_neto");
            cad_datos += "\r\n";
        }

        if (hay_datos) {
            srcFiles.add(archivo);
            fichero = new FileWriter(archivo);
            fichero.write(cad_datos);
            fichero.close();
        }
    } catch (IOException ioe) {
        ioe.printStackTrace();
    }

    /*
          RIPS AC
     */
    sql = "SELECT \n "
            + "numero_factura,\n "
            + "codigo_prestador,\n "
            + "tipo_identificacion,\n "
            + "BTRIM(identificacion) identificacion,\n "
            + "fecha_consulta,\n "
            + "autorizacion,\n "
            + "procedimiento,\n "
            + "BTRIM(TO_CHAR(finalidad::INTEGER, '00')) finalidad,\n "
            + "causa_externa,\n "
            + "diagnostico,\n "
            + "diagnostico_relacionado1,\n "
            + "diagnostico_relacionado2,\n "
            + "diagnostico_relacionado3,\n "
            + "tipo_diagnostico,\n "
            + "valor,\n "
            + "valor_cuota,\n "
            + "valor_neto\n "
            + "FROM \n "
            + "  facturacion.rips_ac ac\n "
            + "INNER JOIN facturacion.tipo_rips_archivos rips ON ac.id_archivo = rips.id\n "
            + "WHERE \n "
            + "    rips.id_envio = " + id_envio;

    try {

        System.out.println("RIPS AC");
        System.out.println(sql);
        archivo = ruta + "AC" + remision + ".txt";

        cad_datos = "";
        pr = conexion.prepareStatement(sql);
        ResultSet resultado = pr.executeQuery();
        hay_datos = false;

        while (resultado.next()) {
            hay_datos = true;
            cad_datos += resultado.getString("numero_factura") + separador;
            cad_datos += resultado.getString("codigo_prestador");
            cad_datos += separador + resultado.getString("tipo_identificacion");
            cad_datos += separador + resultado.getString("identificacion");
            cad_datos += separador + resultado.getString("fecha_consulta");
            cad_datos += separador + resultado.getString("autorizacion");
            cad_datos += separador + resultado.getString("procedimiento");
            cad_datos += separador + resultado.getString("finalidad");
            cad_datos += separador + resultado.getString("causa_externa");
            cad_datos += separador + resultado.getString("diagnostico");
            cad_datos += separador + resultado.getString("diagnostico_relacionado1");
            cad_datos += separador + resultado.getString("diagnostico_relacionado2");
            cad_datos += separador + resultado.getString("diagnostico_relacionado3");
            cad_datos += separador + resultado.getString("tipo_diagnostico");
            cad_datos += separador + resultado.getString("valor");
            cad_datos += separador + resultado.getString("valor_cuota");
            cad_datos += separador + resultado.getString("valor_neto");
            cad_datos += "\r\n";
        }

        if (hay_datos) {
            srcFiles.add(archivo);
            fichero = new FileWriter(archivo);
            fichero.write(cad_datos);
            fichero.close();
        }
    } catch (IOException ioe) {
        ioe.printStackTrace();
    }


    /*
          RIPS AT
     */
    sql = "SELECT \n "
            + "numero_factura,\n "
            + "codigo_prestador,\n "
            + "tipo_identificacion,\n "
            + "identificacion,\n "
            + "autorizacion,\n "
            + "tipo_servicio,\n "
            + "codigo_servicio,\n "
            + "nombre_servicio,\n "
            + "cantidad,\n "
            + "valor_unitario,\n "
            + "valor_total\n "
            + "FROM \n "
            + "  facturacion.rips_at at\n "
            + "INNER JOIN facturacion.tipo_rips_archivos rips ON at.id_archivo = rips.id\n "
            + "WHERE \n "
            + "rips.id_envio = " + id_envio;

    try {

        System.out.println("RIPS AT");
        System.out.println(sql);
        archivo = ruta + "AT" + remision + ".txt";

        cad_datos = "";
        pr = conexion.prepareStatement(sql);
        ResultSet resultado = pr.executeQuery();
        hay_datos = false;

        while (resultado.next()) {

            hay_datos = true;
            cad_datos += resultado.getString("numero_factura") + separador;
            cad_datos += resultado.getString("codigo_prestador");
            cad_datos += separador + resultado.getString("tipo_identificacion");
            cad_datos += separador + resultado.getString("identificacion");
            cad_datos += separador + resultado.getString("autorizacion");
            cad_datos += separador + resultado.getString("tipo_servicio");
            cad_datos += separador + resultado.getString("codigo_servicio");
            cad_datos += separador + resultado.getString("nombre_servicio");
            cad_datos += separador + resultado.getString("cantidad");
            cad_datos += separador + resultado.getString("valor_unitario");
            cad_datos += separador + resultado.getString("valor_total");
            cad_datos += "\r\n";
        }

        if (hay_datos) {
            srcFiles.add(archivo);
            fichero = new FileWriter(archivo);
            fichero.write(cad_datos);
            fichero.close();
        }

    } catch (IOException ioe) {
        ioe.printStackTrace();
    }


    /*
          RIPS CT
     */
    sql = "SELECT \n "
            + "codigo_prestador,\n "
            + "fecha_remision,\n "
            + "codigo_archivo,\n "
            + "remision,\n "
            + "total_registro\n "
            + "FROM \n "
            + "  facturacion.rips_ct ct\n "
            + "INNER JOIN facturacion.tipo_rips_archivos rips ON ct.id_archivo = rips.id\n "
            + "WHERE \n "
            + "rips.id_envio = " + id_envio;

    try {

        System.out.println("RIPS CT");
        System.out.println(sql);
        archivo = ruta + "CT" + remision + ".txt";

        cad_datos = "";
        pr = conexion.prepareStatement(sql);
        ResultSet resultado = pr.executeQuery();
        hay_datos = false;

        while (resultado.next()) {
            hay_datos = true;
            cad_datos += resultado.getString("codigo_prestador") + separador;
            cad_datos += resultado.getString("fecha_remision");
            cad_datos += separador + resultado.getString("codigo_archivo") + resultado.getString("remision");
            cad_datos += separador + resultado.getString("total_registro");
            cad_datos += "\r\n";
        }

        if (hay_datos) {
            srcFiles.add(archivo);
            fichero = new FileWriter(archivo);
            fichero.write(cad_datos);
            fichero.close();
        }
    } catch (IOException ioe) {
        ioe.printStackTrace();
    }

    FileOutputStream fos = new FileOutputStream(ruta + "rips" + remision + ".zip");
    ZipOutputStream zipOut = new ZipOutputStream(fos);
    for (String srcFile : srcFiles) {
        File fileToZip = new File(srcFile);
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
        zipOut.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        fis.close();
    }

    //GENERAR ZIP TIPIFICACION
    sql = "SELECT DISTINCT"
            + "	af.numero_factura id_factura "
            + "FROM "
            + "	facturacion.rips_af af "
            + "    INNER JOIN facturacion.tipo_rips_archivos tra ON tra.id = af.id_archivo "
            + "	INNER JOIN archivos.tipificacion t ON af.numero_factura = t.id_factura "
            + "WHERE "
            + "	tra.id_envio = " + id_envio;
    
    System.err.println(sql + "#########");

    pr = conexion.prepareStatement(sql);
    ResultSet facturas = pr.executeQuery();


    FileOutputStream fosT = new FileOutputStream(ruta_archivo_tipificacion_comprimido);
    ZipOutputStream zipOutT = new ZipOutputStream(fosT);

    while (facturas.next()) {
        
        sqlT = "SELECT " +
                "	BTRIM(nombre_archivo) nombre_archivo " +
                "FROM " +
                "	archivos.tipificacion " +
                "WHERE " +
                "	id_factura = '" + facturas.getString("id_factura")+"'";

        PreparedStatement prT = conexion.prepareStatement(sqlT);
        ResultSet archivosFactura = prT.executeQuery();

        while (archivosFactura.next()) {
            System.err.println(ruta_tipificacion + facturas.getString("id_factura") + "/" + archivosFactura.getString("nombre_archivo") + ".pdf");
            File fileToZip = new File(ruta_tipificacion + facturas.getString("id_factura") + "/" + archivosFactura.getString("nombre_archivo") + ".pdf");
            
            System.err.println("NUEVO ARCHIVO ZIP: " + fileToZip.getAbsolutePath());
            FileInputStream fis = new FileInputStream(fileToZip);
            ZipEntry zipEntry = new ZipEntry(facturas.getString("id_factura") + "/" + fileToZip.getName());
            zipOutT.putNextEntry(zipEntry);

            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOutT.write(bytes, 0, length);
            }
            fis.close();
        }
    }
    zipOutT.close();
    fosT.close();

    //AÑADIR ARCHIVO COMPRIMIDO DE FACTURAS TIPIFICADAS AL COMPRIMIDO RIPS
    File fileToZip = new File(ruta_archivo_tipificacion_comprimido);
    FileInputStream fis = new FileInputStream(fileToZip);
    ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
    zipOut.putNextEntry(zipEntry);

    byte[] bytes = new byte[1024];
    int length;
    while ((length = fis.read(bytes)) >= 0) {
        zipOut.write(bytes, 0, length);
    }
    fis.close();

    zipOut.close();
    fos.close();

    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment;filename=rips" + remision + ".zip");

    File file = new File(ruta + "rips" + remision + ".zip");
    FileInputStream fileIn = new FileInputStream(file);
    ServletOutputStream out2 = response.getOutputStream();

    byte[] outputByte = new byte[4096];
    while (fileIn.read(outputByte, 0, 4096) != -1) {
        out2.write(outputByte, 0, 4096);
    }
    fileIn.close();
    out2.flush();
    out2.close();
%>
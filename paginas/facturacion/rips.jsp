<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="RIPS" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>

            <div style="overflow:auto;height:400px; width:1050px" id="divContenido">
              <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
              <div id="divBuscar" style="display:block">
                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                  <tr class="titulos" align="center">
                    <td width="20%">SEDE</td>
                    <td width="25%">PLAN CONTRATACIÓN</td>
                    <td Width="10">PERIODO</td>
                    <td Width="10">TIPO</td>
                    <td width="15%" style="background-color: darkgray;">CUENTA</td>
                    <td Width="10">REMISION</td>
                  </tr>
                  <tr class="estiloImput">
                    <td>
                      <select style="width:95%" id="cmbSede" onchange="$('#cmbIdPlan').empty();"> 
                        <option value="">[ TODAS ]</option>                    
                        <% resultaux.clear();
                           resultaux=(ArrayList)beanAdmin.combo.cargar(963);   
                           ComboVO cmb33;
                          for(int k=0;k<resultaux.size();k++){
                            cmb33=(ComboVO)resultaux.get(k);
                          %>
                        <option value="<%= cmb33.getId()%>" title="<%= cmb33.getTitle()%>"><%= cmb33.getDescripcion()%></option>
                       <%}%>  
                      </select>
                    </td>
                    <td>
                      <select size="1" id="cmbIdPlan" style="width:95%" onfocus="cargarComboGRALCondicion2('', '', this.id, 196, valorAtributo('cmbSede'), valorAtributo('cmbSede'))">
                         <option value=""></option>                                                    
                      </select>
                    </td>
                    <td>
                      <select id="cmbIdAnio" style="width:45%">
                        <option value=""></option>
                        <%resultaux.clear();
                          resultaux=(ArrayList)beanAdmin.combo.cargar(71);  
                          ComboVO cmbAn; 
                          for(int k=0;k<resultaux.size();k++){ 
                                cmbAn=(ComboVO)resultaux.get(k);
                                %>
                        <option value=" <%= cmbAn.getId()%>" title="<%= cmbAn.getTitle()%>"><%=cmbAn.getDescripcion()%>
                        </option>
                        <%} %>
                      </select>
                      <select id="cmbIdMes" style="width:45%">
                        <option value=""></option>
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septienbre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                      </select>
                    </td>
                    <td>
                      <select id="cmbIdTipo" style="width:90%" title="QC55">

                        <%     resultaux.clear();
                   resultaux=(ArrayList)beanAdmin.combo.cargar(55);  
                   ComboVO cmbT; 
                   for(int k=0;k<resultaux.size();k++){ 
                         cmbT=(ComboVO)resultaux.get(k);
                                %>
                        <option value="<%= cmbT.getId()%>" title="<%= cmbT.getTitle()%>"><%=cmbT.getDescripcion()%>
                        </option>
                        <%} %>
                      </select>
                    </td>
                    <td style="background-color: darkgray;">
                      <input type="text" id="txtPrefijoCuenta" style="width:30%" placeholder="Prefijo"/> -
                      <input type="text" id="txtCuenta" style="width:60%" placeholder="Numero"/>
                    </td> 
                    <td>
                      <input type="text" id="txtRemision" style="width:95%" />
                    </td>
                  </tr>
                  <tr>
                    <td colspan="6">
                      <table width="100%">
                        <tr>
                          <td><hr></td>
                          <td width="33%" align="CENTER">
                            <input title="BF932" type="button" class="small button blue" value="Buscar RIPS"
                              onclick="buscarFacturacion('listGrillaRips')"/>
                              <input title="BF932" type="button" class="small button blue" value="Crear RIPS"
                              onclick="modificarCRUD('crearArchivosRips');"/>
                          </td>
                          <td><hr></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                <table id="listGrillaRips" class="scroll"></table>
              </div>
            </div>
          </td>
        </tr>
      </table>

      <div id="divVentanitaGeneraRips" style="display:none; z-index:2000; top:0px; width:1100px; left:150px;">

        <div class="transParencia"
          style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>

        <div style="z-index:2002; position:fixed; top:40px; left:180px; width:1100px">

          <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
              <td align="left" colspan="4"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                  onclick="ocultar('divVentanitaGeneraRips')" /></td>
              <td align="right" colspan="5"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                  onclick="ocultar('divVentanitaGeneraRips')" /></td>
            <tr>
            <tr class="titulos">
              <td width="10%">ID</td>
              <td width="10%">ID RIPS</td>
              <td width="10%">RIPS</td>
              <td width="10%">ID PLAN</td>
              <td width="20%">PLAN</td>
              <td width="10%">RANGO</td>
              <td width="10%">POS</td>
              <td width="10%">ID ENVIO</td>
              <td width="10%">REMISION</td>
            </tr>
            <tr class="estiloImput">
              <td id="lblIdArchivoVentanita"></td>
              <td id="lblIdRipsVentanita"></td>
              <td id="lblRipsVentanita"></td>
              <td id="lblIdPlanVentanita"></td>
              <td id="lblPlanVentanita"></td>
              <td id="lblRangoVentanita"></td>
              <td id="lblPosVentanita"></td>
              <td id="lblIdEnvioVentanita"></td>
              <td id="lblRemisionVentanita"></td>
            </tr>
            <tr>
              <td align="center" colspan="2">
                <input type="button" class="small button blue" value="CREAR ARCHIVO RIPS"
                  onclick="crearArchivoRips();" />
              </td>
              <td align="center" colspan="2">
                <input type="button" class="small button blue" value="CREAR TODOS LOS RIPS" onclick="crearRips();" />
              </td>
              <td align="left">
                <input type="button" class="small button blue" value="EXCEPCIONES DE ENVIO"
                  onclick="mostrar('divVentanitaExcepcionesRips'); ocultar('divVentanitaGeneraRips'); buscarFacturacion('listExcepcionesEnvio')" />
              </td>
              <td align="left">
                <input type="button" class="small button blue" value="ELIMINAR ENVIO"
                  onclick="modificarCRUD('eliminarEnvioRips')" />
              </td>
            </tr>
            <tr>
              <td colspan="8">
                <hr>
              </td>
            </tr>
            <tr class="titulos">
              <td colspan="5" align="CENTER">
                <label>TOTAL ENVIO - </label><label id="lblTotalEnvio"></label>
              </td>
            </tr>
          </table>

          <table width="100%" border="2" class="fondoTabla">
            <tr class="titulosCentrados">
              <td width="60%">FACTURAS RIPS
              </td>
              <td>FACTURAS PENDIENTES
              </td>
            </tr>
            <tr class="titulos">
              <td id="contenedorRegistros">
              </td>
              <td id="contenedorRegistrosPendientes">
              </td>
            </tr>
            <tr>
              <td align="CENTER">
                <input type="button" class="small button blue" value="ELIMINAR FACTURAS DE ENVIO ACTUAL"
                  onclick="modificarCRUD('eliminarRegistroDeEnvioRips');" />
              </td>
              <td align="CENTER">
                <input type="button" class="small button blue" value="ADICIONAR FACTURAS A ENVIO ACTUAL"
                  onclick="modificarCRUD('agregarRegistrosAEnvioRips');" />
              </td>
            </tr>
          </table>
          <table width="100%" border="2" class="fondoTabla">
            <tr class="titulosCentrados">
              <td colspan="4">DETALLES DE FACTURA
              </td>
            </tr>
            <tr class=" estiloImputIzq2">
              <td width="15%">Archivo:</td>
              <td width="85%" colspan="3"><label id="lblIdArchivoFactura"></label></td>
            </tr>
            <tr class=" estiloImputIzq2">
              <td width="15%">Id Envio:</td>
              <td colspan="3"><label id="lblIdEnvioFactura"></label></td>
            </tr>
            <tr class=" estiloImputIzq2">
              <td>Plan</td>
              <td colspan="3"><label id="lblIdPlanFactura"></label>-<label id="lblPlanFactura"></label></td>
            </tr>
            <tr class=" estiloImputIzq2">
              <td>Id Factura:</td>
              <td colspan="3"><label id="lblIdFactura"></label></td>
            </tr>
            <tr class="estiloImputIzq2">
              <td>Numero Factura:</td>
              <td colspan="3"><label id="lblNumeroFactura"></label></td>
            </tr>
            <tr class="titulos">
              <td>
                <input title="BF73" type="button" class="small button blue" value="IMPRIMIR"
                  onclick="formatoPDFFactura(valorAtributo('lblIdFactura'));" />
              </td>
              <td align="CENTER">
                <input type="button" id="btnTipificacion" class="small button blue" value="TIPIFICAR FACTURAS SELECCIONADAS"
                  onclick="tipificarFacturas(obtenerListaFacturasTipificacionRips())"/>
                <div id="loaderTipificacion" hidden>
                  <img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="20px" height="20px" >
                  Tipificando facturas <label id="progreso">0</label>%
                </div>
              </td>
            </tr>
          </table>
        </div>
      </div>


      <div id="divVentanitaExcepcionesRips" style="display:none; z-index:2000; top:1px; width:1100px; left:150px;">
        <div class="transParencia"
          style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>

        <div style="z-index:2002; position:fixed; top:100px; left:180px; width:1100px">


          <table width="100%" border="2" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                    onclick="ocultar('divVentanitaExcepcionesRips');mostrar('divVentanitaGeneraRips')"/></td>
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                    onclick="ocultar('divVentanitaExcepcionesRips');mostrar('divVentanitaGeneraRips')"/></td>
              <tr>
              <tr class="titulos">
                <td colspan="2" align="CENTER">
                  <label>EXCEPCIONES DE ENVIO</label>
                </td>
              </tr>
              <tr>
                <td colspan="2"><hr></td>
              </tr>
            <tr class="titulosCentrados">
              <td width='50%'>DIAGNÓSTICOS DE INGRESO NO VALIDOS
              </td>
              <td>MUNICIPOS NO VIGENTES - PLANES PGP 
              </td>
            </tr>
            <tr class="titulos">
              <td>
                  <table id="listGrillaExcepcionesDx" class="scroll"></table>
              </td>
              <td>
                  <table id="listGrillaExcepcionesMunicipios" class="scroll"></table>
              </td>
            </tr>
          </table>
        </div>
      </div>


      <div id="divVentanitaArchivosTipificacion" style="display:none; z-index:2000; top:1px; width:700px; left:150px;">
        <div class="transParencia"
          style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>
        <div style="z-index:2002; position:fixed; top:150px; left:150; width:700">
          <table width="100%" border="2" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                    onclick="ocultar('divVentanitaArchivosTipificacion')"/></td>
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                    onclick="ocultar('divVentanitaArchivosTipificacion')"/></td>
              <tr>
              <tr class="titulos">
                  <td align="center" colspan="2"><input align="center" type="button" class="boton" value="REFRESCAR LISTADO" onclick="buscarHistoria('listArchivosAdjuntosVarios')" /></td>
              </tr>                          
              <tr class="titulos">
                  <td colspan="2">                                    
                      <table id="listArchivosTificacion"></table>
                  </td>
              </tr>
          </table>
        </div>
      </div>


      <div id="divVentanitaTipificacion"  style="position:absolute; display:none; background-color:#E2E1A5; top:10px; left:100px; width:600px; height:90px; z-index:2004">
        <div class="transParencia" style="z-index:2005; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
        </div> 
        <div style="z-index:2006; position:absolute; top:200px; left:100px; width:95%">          
            <table id="idSubVentanita" width="100%" class="fondoTabla">
                <tr class="estiloImput">
                    <td colspan="3" align="right">
                        <input name="btn_cerrar" type="button" value="CERRAR" onclick="ocultar('divVentanitaTipificacion')"/>
                    </td>  
                <tr>     
                <tr class="titulos">
                    <td width="5%">ID FACTURA</td>  
                    <td width="65%">NOMBRE ARCHIVO</td>
                    <td width="30%"></td>                      
                <tr>           
                <tr class="estiloImput" >  
                    <td><label id="lblIdFactura"></label></td>            
                    <td align="CENTER" onClick="abrirPopupArchivoTipificacion()">
                        <label id="lblTipoArchvio"></label><label id="lblImagen"></label><a><u>Abrir</u></a>
                    </td>            
                <tr> 
            </table>  
        </div>            
    </div> 
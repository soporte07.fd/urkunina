  <%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
  <%@ page import = "java.text.*,java.sql.*"%>
  <%@ page import = "Sgh.Utilidades.*" %>
  <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
  <%@ page import = "Clinica.Presentacion.*" %>
  
 <%@ page import="javax.naming.*" %>
  
  <%@ page import="java.io.*" %> 
  <%@ page import="java.awt.Font" %> 

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> 

 <!DOCTYPE HTML >  
 <% 
     
 	boolean estado;
	String ruta="/opt/tomcat8/webapps/rips/";
	String archivo = "";
    Connection conexion = beanSession.cn.getConexion();

	String id_envio = request.getParameter("id_envio");
	String id_archivo = request.getParameter("id_archivo");
	String id_rips = request.getParameter("id_rips");
	String remision = request.getParameter("remision");


	FileWriter fichero = null;
		
	PreparedStatement pr=null;
	String sql ="";
	String separador = ",";
	String cad_datos = "";

	switch (id_rips) {


		case "US":

			sql ="SELECT\n "+ 
				  "tipo_identificacion,\n "+
				  "identificacion,\n "+
				  "codigo_administradora,\n "+
				  "tipo_usuario,\n "+
				  "apellido1,\n "+
				  "apellido2,\n "+
				  "nombre1,\n "+
				  "nombre2,\n "+
				  "edad,\n "+
				  "unidad_edad,\n "+
				  "sexo,\n "+
				  "departamento,\n "+
				  "municipio,\n "+
				  "residencia\n "+
				"FROM \n "+
				  "facturacion.rips_us us\n "+
				"WHERE\n "+ 
				"	us.id_archivo = "+id_archivo;						
			
			try{

			System.out.println("RIPS US");
			System.out.println(sql);

			archivo = ruta+ "US"+remision+".txt";
			System.out.println(archivo);

			pr= conexion.prepareStatement(sql);
			ResultSet resultado = pr.executeQuery();
			while (resultado.next()) { 
				cad_datos += resultado.getString("tipo_identificacion")+ separador;
				cad_datos += resultado.getString("identificacion");
				cad_datos += separador + resultado.getString("codigo_administradora");
				cad_datos += separador + resultado.getString("tipo_usuario");
				cad_datos += separador + resultado.getString("apellido1");
				cad_datos += separador + resultado.getString("apellido2");
				cad_datos += separador + resultado.getString("nombre1");
				cad_datos += separador + resultado.getString("nombre2");
				cad_datos += separador + resultado.getString("edad");
				cad_datos += separador + resultado.getString("unidad_edad");
				cad_datos += separador + resultado.getString("sexo");
				cad_datos += separador + resultado.getString("departamento");
				cad_datos += separador + resultado.getString("municipio");
				cad_datos += separador + resultado.getString("residencia");
				cad_datos += "\r\n";
			}
			
				fichero = new FileWriter(archivo);
				fichero.write(cad_datos + "\r\n");
				fichero.close();
			}
			catch(IOException ioe)
			{
				ioe.printStackTrace();
			} 

		break;



		case "AP":

			sql ="SELECT\n "+
				  "numero_factura,\n "+
				  "codigo_prestador,\n "+
				  "tipo_identificacion,\n "+
				  "identificacion,\n "+
				  "fecha_procedimiento,\n "+
				  "autorizacion,\n "+
				  "procedimiento,\n "+
				  "ambito,\n "+
				  "finalidad,\n "+
				  "personal_atiende,\n "+
				  "diagnostico,\n "+
				  "diagnostico_relacionado,\n "+
				  "complicacion,\n "+
				  "acto_quirurgico,\n "+
				  "valor\n "+
				"FROM \n "+
				  "facturacion.rips_ap ap\n "+
				"WHERE \n "+
					"ap.id_archivo = "+id_archivo;		
			
		
			try{


				System.out.println("RIPS US");
				System.out.println(sql);
				archivo = ruta + "AP"+remision+".txt";

				cad_datos = "";
				pr= conexion.prepareStatement(sql);
				ResultSet resultado = pr.executeQuery();


			while (resultado.next()) { 
				cad_datos +=  resultado.getString("numero_factura")+ separador;
				cad_datos += resultado.getString("codigo_prestador");
				cad_datos += separador + resultado.getString("tipo_identificacion");
				cad_datos += separador + resultado.getString("identificacion");
				cad_datos += separador + resultado.getString("fecha_procedimiento");				
				cad_datos += separador + resultado.getString("autorizacion");
				cad_datos += separador + resultado.getString("procedimiento");
				cad_datos += separador + resultado.getString("ambito");
				cad_datos += separador + resultado.getString("finalidad");
				cad_datos += separador + resultado.getString("personal_atiende");
				cad_datos += separador + resultado.getString("diagnostico");
				cad_datos += separador + resultado.getString("diagnostico_relacionado");				
				cad_datos += separador + resultado.getString("complicacion");
				cad_datos += separador + resultado.getString("acto_quirurgico");
				cad_datos += separador + resultado.getString("valor");
				cad_datos += "\r\n";
			}
			
			
				fichero = new FileWriter(archivo);
				fichero.write(cad_datos + "\r\n");
				fichero.close();
			}
			catch(IOException ioe)
			{
				ioe.printStackTrace();
			} 


		break;


		case "AF":

			sql ="SELECT\n "+
					"codigo_prestador,\n "+
					"razon_social,\n "+
					"tipo_id_prestador,\n "+
					"id_prestador,\n "+
					"numero_factura,\n "+
					"fecha_expedicion,\n "+
					"fecha_inicio,\n "+
					"fecha_final,\n "+
					"codigo_administradora,\n "+
					"nombre_admisnitradora,\n "+
					"numero_contrato,\n "+
					"plan_beneficios,\n "+
					"num_poliza,\n "+
					"total_copago,\n "+
					"valor_comision,\n "+
					"total_descuentos,\n "+
					"valor_neto\n "+
				"FROM \n "+
				"	facturacion.rips_af af\n "+
				"INNER JOIN facturacion.tipo_rips_archivos rips ON af.id_archivo = rips.id\n "+
				"WHERE \n "+
				"	rips.id_envio = "+id_envio;		

		
			try{


				System.out.println("RIPS AF");
				System.out.println(sql);
				archivo = ruta + "AF"+remision+".txt";

				cad_datos = "";
				pr= conexion.prepareStatement(sql);
				ResultSet resultado = pr.executeQuery();

			while (resultado.next()) { 
				cad_datos +=  resultado.getString("codigo_prestador")+ separador;
				cad_datos += resultado.getString("razon_social");
				cad_datos += separador + resultado.getString("tipo_id_prestador");
				cad_datos += separador + resultado.getString("id_prestador");
				cad_datos += separador + resultado.getString("numero_factura");				
				cad_datos += separador + resultado.getString("fecha_expedicion");
				cad_datos += separador + resultado.getString("fecha_inicio");
				cad_datos += separador + resultado.getString("fecha_final");
				cad_datos += separador + resultado.getString("codigo_administradora");
				cad_datos += separador + resultado.getString("nombre_admisnitradora");
				cad_datos += separador + resultado.getString("numero_contrato");
				cad_datos += separador + resultado.getString("plan_beneficios");				
				cad_datos += separador + resultado.getString("num_poliza");
				cad_datos += separador + resultado.getString("total_copago");
				cad_datos += separador + resultado.getString("valor_comision");
				cad_datos += separador + resultado.getString("total_descuentos");
				cad_datos += separador + resultado.getString("valor_neto");
				cad_datos += "\r\n";
			}
			
				fichero = new FileWriter(archivo);
				fichero.write(cad_datos + "\r\n");
				fichero.close();
			}
			catch(IOException ioe)
			{
				ioe.printStackTrace();
			} 




		break;

		case "AC":


		sql ="SELECT \n "+
				  "numero_factura,\n "+
				  "codigo_prestador,\n "+
				  "tipo_identificacion,\n "+
				  "identificacion,\n "+
				  "fecha_consulta,\n "+
				  "autorizacion,\n "+
				  "procedimiento,\n "+
				  "finalidad,\n "+
				  "causa_externa,\n "+
				  "diagnostico,\n "+
				  "diagnostico_relacionado1,\n "+
				  "diagnostico_relacionado2,\n "+
				  "diagnostico_relacionado3,\n "+
				  "tipo_diagnostico,\n "+
				  "valor,\n "+
				  "valor_cuota,\n "+
				  "valor_neto\n "+
				"FROM \n "+
				"  facturacion.rips_ac ac\n "+
				"INNER JOIN facturacion.tipo_rips_archivos rips ON ac.id_archivo = rips.id\n "+
				"WHERE \n "+
				"    rips.id_envio = "+id_envio;		

		
			try{


				System.out.println("RIPS AC");
				System.out.println(sql);
				archivo = ruta + "AC"+remision+".txt";

				cad_datos = "";
				pr= conexion.prepareStatement(sql);
				ResultSet resultado = pr.executeQuery();

			while (resultado.next()) { 
				cad_datos +=  resultado.getString("numero_factura")+ separador;
				cad_datos += resultado.getString("codigo_prestador");
				cad_datos += separador + resultado.getString("tipo_identificacion");
				cad_datos += separador + resultado.getString("identificacion");
				cad_datos += separador + resultado.getString("fecha_consulta");				
				cad_datos += separador + resultado.getString("autorizacion");
				cad_datos += separador + resultado.getString("procedimiento");
				cad_datos += separador + resultado.getString("finalidad");
				cad_datos += separador + resultado.getString("causa_externa");
				cad_datos += separador + resultado.getString("diagnostico");
				cad_datos += separador + resultado.getString("diagnostico_relacionado1");
				cad_datos += separador + resultado.getString("diagnostico_relacionado2");				
				cad_datos += separador + resultado.getString("diagnostico_relacionado3");
				cad_datos += separador + resultado.getString("tipo_diagnostico");
				cad_datos += separador + resultado.getString("valor");
				cad_datos += separador + resultado.getString("valor_cuota");
				cad_datos += separador + resultado.getString("valor_neto");
				cad_datos += "\r\n";
			}
			
					fichero = new FileWriter(archivo);
					fichero.write(cad_datos + "\r\n");
					fichero.close();
			}
			catch(IOException ioe)
			{
				ioe.printStackTrace();
			}


		break;

		case "AT":

		sql ="SELECT \n "+
				  "numero_factura,\n "+
				  "codigo_prestador,\n "+
				  "tipo_identificacion,\n "+
				  "identificacion,\n "+
				  "autorizacion,\n "+
				  "tipo_servicio,\n "+
				  "codigo_servicio,\n "+
				  "nombre_servicio,\n "+
				  "cantidad,\n "+
				  "valor_unitario,\n "+
				  "valor_total\n "+
				"FROM \n "+
				"  facturacion.rips_at at\n "+
				"INNER JOIN facturacion.tipo_rips_archivos rips ON at.id_archivo = rips.id\n "+
				"WHERE \n "+
					"rips.id_envio = "+id_envio;
							
			try{
				System.out.println("RIPS AT");
				System.out.println(sql);
				archivo = ruta + "AT"+remision+".txt";

				cad_datos = "";
				pr= conexion.prepareStatement(sql);
				ResultSet resultado = pr.executeQuery();

			while (resultado.next()) {

				cad_datos +=  resultado.getString("numero_factura")+ separador;
				cad_datos += resultado.getString("codigo_prestador");
				cad_datos += separador + resultado.getString("tipo_identificacion");
				cad_datos += separador + resultado.getString("identificacion");
				cad_datos += separador + resultado.getString("autorizacion");
				cad_datos += separador + resultado.getString("tipo_servicio");
				cad_datos += separador + resultado.getString("codigo_servicio");
				cad_datos += separador + resultado.getString("nombre_servicio");
				cad_datos += separador + resultado.getString("cantidad");
				cad_datos += separador + resultado.getString("valor_unitario");
				cad_datos += separador + resultado.getString("valor_total");
				cad_datos += "\r\n";
			}
			
					fichero = new FileWriter(archivo);
					fichero.write(cad_datos + "\r\n");
					fichero.close();
				
			}
			catch(IOException ioe)
			{
				ioe.printStackTrace();
			}


		break;

		case "CT":

		sql ="SELECT \n "+
				  "codigo_prestador,\n "+
				  "fecha_remision,\n "+
					"codigo_archivo,\n "+
					"remision,\n "+
				  "total_registro\n "+
				"FROM \n "+
				"  facturacion.rips_ct ct\n "+
				"INNER JOIN facturacion.tipo_rips_archivos rips ON ct.id_archivo = rips.id\n "+
				"WHERE \n "+
				    "rips.id_envio = "+id_envio; 		

		
			try{


				System.out.println("RIPS CT");
				System.out.println(sql);
				archivo = ruta + "CT"+remision+".txt";

				cad_datos = "";
				pr= conexion.prepareStatement(sql);
				ResultSet resultado = pr.executeQuery();


			while (resultado.next()) { 
				cad_datos +=  resultado.getString("codigo_prestador")+ separador;
				cad_datos += resultado.getString("fecha_remision");
				cad_datos += separador + resultado.getString("codigo_archivo") + resultado.getString("remision");
				cad_datos += separador + resultado.getString("total_registro");
				cad_datos += "\r\n";
			}
			
					fichero = new FileWriter(archivo);
					fichero.write(cad_datos + "\r\n");
					fichero.close();
			}
			catch(IOException ioe)
			{
				ioe.printStackTrace();
			}


		break;
		
	}

	response.setContentType("application/octet-stream");
	response.setHeader("Content-Disposition","attachment;filename="+id_rips+remision+".txt");


	OutputStream out2 = response.getOutputStream();
	FileInputStream in2 = new FileInputStream(archivo);
	byte[] buffer2 = new byte[4096];
	int length2;
	while ((length2 = in2.read(buffer2)) > 0){
    	out2.write(buffer2, 0, length2);
	}
	in2.close();
	out2.flush();

%>
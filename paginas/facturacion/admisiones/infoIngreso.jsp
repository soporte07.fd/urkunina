<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="100%">
    <tr class="titulos">
        <td style="display: none;"><strong></strong></td>
        <td><strong>Institución que remite</strong></td>
        <td><strong>Profesional Admisión</strong></td>
    </tr>
    <tr class="estiloImput">
        <td style="display: none;"> 
                <input type="text" id="txtIdDx" class="w90" tabindex="123" value="0-" >
                <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
                    onclick="traerVentanita(this.id,'','divParaVentanita','txtIdDx','5')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
        </td>
        <td>
            <input type="text" id="txtIPSRemite" onkeypress="llamarAutocomIdDescripcionConDato('txtIPSRemite',313)"
                class="w85" tabindex="121" />
            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIpsRemT"
                onclick="traerVentanita(this.id,'','divParaVentanita','txtIPSRemite','4')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
        </td>
        <td>
            <select id="cmbIdProfesionales" style="width: 95%;" tabindex="128"
                onfocus="comboDependienteSede('cmbIdProfesionales','77')">
                <option value=""></option>
            </select>
        </td>
    </tr>
</table>

<table width="100%">
    <tr class="titulos">
        <td width="30%"><strong>Administradora</strong></td>
        <td width="15%"><strong>Tipo Régimen - Tipo Plan</strong></td>
        <td width="15%">
            <img id="editarAuto" width="18px" height="18px" align="middle"
                src="/clinica/utilidades/imagenes/acciones/editar.gif"><strong>Número de Autorización</strong>
        </td>
        <td width="10%"><strong>Fecha</strong></td>
        <td width="30%"><strong>Tipo Admisión</strong></td>
    </tr>
    <tr class="estiloImput">
        <td>
            <input type="text" id="txtAdministradora1"
                onchange="limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')"
                onkeypress="llamarAutocompletarEmpresa('txtAdministradora1',184)" style="width: 85%;" tabindex="118" />
            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaAdminRem"
                onclick="traerVentanitaEmpresa(this.id,'divParaVentanita','txtAdministradora1','24');limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
        </td>
        <td>
            <select id="cmbIdTipoRegimen" style="width: 95%;" onclick="limpiarDivEditarJuan('limpiarDatosPlan')"
                onfocus="limpiarDivEditarJuan('limpiarDatosRegimenFactura');comboDependienteAutocompletarSede('cmbIdTipoRegimen','txtAdministradora1','79')"
                tabindex="117">
                <option value=""></option>
            </select>
        </td>
        <td>
            <input type="text" id="txtNoAutorizacion" style="width: 95%;"
                onKeyPress="javascript:return teclearExcluirCaracter(event,'%');"
                onblur="guardarYtraerDatoAlListado('nuevaAdmisionCuenta')" tabindex="119" />
        </td>
        <td>
            <input type="text" class="hasDatepicker" style="width: 95%;" id="txtFechaVigenciaAutorizacion"
                tabindex="120">
        </td>
        <td>
            <select id="cmbTipoAdmision" style="width: 95%;"
                onchange="buscarAGENDA('listAdmisionCEXProcedimientoTraer')"
                onfocus="cargarTipoCitaSegunEspecialidad('cmbTipoAdmision','cmbIdEspecialidad')" tabindex="125">
                <option value=""></option>
            </select>
        </td>
    </tr>
</table>

<table width="100%">
    <tr class="titulos">
        <td width="50%"><strong>Plan de Contratación</strong></td>
        <td width="25%"><strong>Tipo Afiliado</strong></td>
        <td width="25%"><strong>Rango</strong></td>
    </tr>
    <tr class="estiloImput">
        <td>
            <div hidden>
                <label id="lblIdPlanContratacion"></label>-<label id="lblNomPlanContratacion"></label>
            </div>
            <select id="cmbIdPlan" style="width: 95%;"
                onfocus="limpiarDivEditarJuan('limpiarDatosRegimenFactura'); limpiarDivEditarJuan('limpiarDatosPlan')
                         traerPlanesDeContratacion(this.id, '165', 'txtAdministradora1', 'cmbIdTipoRegimen')"
                onchange="asignaAtributo('lblIdPlanContratacion', valorAtributo(this.id), 0)"
                tabindex="117">
                <option value=""></option>
            </select>
        </td>
        <td>
            <select id="cmbIdTipoAfiliado" style="width: 95%;"
                onfocus="limpiarDivEditarJuan('limpiarDatosAfiliadoFactura');comboDependiente('cmbIdTipoAfiliado','lblIdPlanContratacion','30')"
                tabindex="117">
                <option value=""></option>
            </select>
        </td>
        <td>
            <select id="cmbIdRango" style="width: 95%;"
                onfocus="comboDependienteDosCondiciones('cmbIdRango','lblIdPlanContratacion','cmbIdTipoAfiliado','31')"
                tabindex="117">
                <option value=""></option>
            </select>
        </td>
    </tr>
</table>


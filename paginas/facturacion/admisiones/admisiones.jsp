<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>


<table width="1200px" align="center" border="0" cellspacing="0" cellpadding="1" class="fondoTabla">
    <tr>
        <td class="w100">
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="ADMISIONES" />
                </jsp:include>
            </div>
        </td>
    </tr>


    <tr>
        <td>
            <div>
                <table width="100%">
                    <tr class="titulos">
                        <td width="5%"><strong>Id Cita</strong></td>
                        <td width="5%"></td>
                        <td width="5%"></td>
                        <td width="15%"><strong>Servicio</strong></td>
                        <td width="60%"><strong>Paciente</strong></td>
                        <td width="10%"></td>
                    </tr>
                    <tr class="estiloImput">
                        <td>
                            <input type="hidden" id="lblIdAgendaDetalle" />
                            <label id="lblIdCita" style="width: 90%;"></label>
                        </td>
                        <td>
                            <input type="button" class="small button blue" title="ADM57" value="Agenda"
                                onclick="abrirVentanaTraerCitaAlaAdmisionCuenta()" style="width: 90%;" />
                        </td>
                        <td>
                            <input type="button" class="small button blue" value="Internación"
                                onclick="abrirVentanaAdmisionUrgencia()" style="width: 90%;" />
                        </td>
                        <td>
                            <select id="cmbIdTipoServicio" tabindex="14" style="width: 100%;"
                                onfocus="comboDependienteEmpresa('cmbIdTipoServicio','74')"
                                onchange="traerInfoServicio()">
                                <option value=""></option>
                            </select>
                        </td>
                        <td>
                            <input type="text" id="txtIdBusPaciente" oncontextmenu="return false" tabindex="99"
                                style="width: 95%;"
                                onfocus="javascript:return document.getElementById('txtApellido1').focus()" />
                        </td>
                        <td>
                            <input name="btn_busAgenda" type="button" class="small button blue" title="ADMI87"
                                style="width: 90%;" value="BUSCAR." onclick="buscarInformacionBasicaPaciente()" />
                        </td>
                    </tr>
                </table>
                <div class="btitulos">
                    <label class="bloque w10">Tipo Id</label>
                    <label class="bloque w20">Identificación</label>
                    <label class="bloque w25">Apellidos</label>
                    <label class="bloque w25">Nombres</label>
                    <label class="bloque w10">Fecha Nacimiento</label>
                    <label class="bloque w10">Sexo</label>
                </div>
                <div class="bestiloImput">
                    <div class="bloque w10">
                        <select id="cmbTipoId" class="w90" tabindex="100">
                            <option value="CC">Cedula de Ciudadania</option>
                            <option value="CE">Cedula de Extranjeria</option>
                            <option value="MS">Menor sin Identificar</option>
                            <option value="PA">Pasaporte</option>
                            <option value="RC">Registro Civil</option>
                            <option value="TI">Tarjeta de identidad</option>
                            <option value="CD">Carnet Diplomatico</option>
                            <option value="NV">Certificado nacido vivo</option>
                            <option value="AS">Adulto sin Identificar</option>
                        </select>
                    </div>
                    <div class="bloque w20">
                        <input type="text" id="txtIdentificacion" class="90w"
                            onfocus="javascript:return document.getElementById('txtApellido1').focus()"
                            onKeyPress="javascript:checkKey2(event); return teclearsoloTelefono(event);"
                            oncontextmenu="return false" tabindex="101" />
                        <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaPacienT"
                            onclick="traerVentanitaPaciente(this.id,'','divParaVentanita','txtIdBusPaciente','27')"
                            src="/clinica/utilidades/imagenes/acciones/buscar.png">
                    </div>
                    <div class="bloque w25">
                        <input readonly type="text" id="txtApellido1"
                            onkeypress="javascript:return teclearsoloan(event);" onblur="v28(this.value,this.id);"
                            tabindex="102" style="width:45%" />
                        <input readonly type="text" id="txtApellido2"
                            onkeypress="javascript:return teclearsoloan(event);" onblur="v28(this.value,this.id);"
                            tabindex="103" style="width:45%" />
                    </div>
                    <div class="bloque w25">
                        <input readonly type="text" id="txtNombre1" onkeypress="javascript:return teclearsoloan(event);"
                            onblur="v28(this.value,this.id);" tabindex="104" style="width:45%" />
                        <input readonly type="text" id="txtNombre2" onkeypress="javascript:return teclearsoloan(event);"
                            onblur="v28(this.value,this.id);" tabindex="105" style="width:45%" />
                    </div>
                    <div class="bloque w10">
                        <input id="txtFechaNac" type="text" class="w80" tabindex="106" /> </div>
                    <select id="cmbSexo" class="bloque w10" tabindex="107">
                        <option value=""></option>
                        <option value="F">FEMENINO</option>
                        <option value="M">MASCULINO</option>
                        <option value="O">OtrO</option>
                    </select>
                </div>
                <div class="btitulos">
                    <label class="bloque w25">Municipio</label>
                    <label class="bloque w30">Dirección</label>
                    <label class="bloque w15">Acompañante</label>
                    <label class="bloque w10">Celular1</label>
                    <label class="bloque w10">Celular2</label>
                    <label class="bloque w10">Telefono</label>
                </div>

                <div class="bestiloImput">

                    <div class="bloque w25">
                        <input type="text" id="txtMunicipio" class="w80" tabindex="108" />
                        <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaMunicT"
                            onclick="traerVentanita(this.id,'','divParaVentanita','txtMunicipio','1')"
                            src="/clinica/utilidades/imagenes/acciones/buscar.png">
                        <div id="divParaVentanita"></div>
                    </div>

                    <input type="text" id="txtDireccionRes" class="bloque w30"
                        onkeypress="javascript:return teclearsoloan(event);" onblur="v28(this.value,this.id);"
                        tabindex="109" />
                    <input type="text" id="txtNomAcompanante" class="bloque w15"
                        onkeypress="javascript:return teclearExcluirCaracter(event);" tabindex="110" />
                    <input type="text" id="txtCelular1" class="bloque w10"
                        onkeypress="javascript:return teclearsoloan(event);" tabindex="111" />
                    <input type="text" id="txtCelular2" class="bloque w10"
                        onkeypress="javascript:return teclearsoloan(event);" tabindex="112" />
                    <input type="text" id="txtTelefonos" class="bloque w10"
                        onkeypress="javascript:return teclearsoloan(event);" tabindex="113" />

                </div>

                <div class="btitulos">
                    <label class="bloque w20">Email</label>
                    <label class="bloque w20">Etnia</label>
                    <label class="bloque w20">Nivel Escolaridad</label>
                    <label class="bloque w10">Estrato</label>
                    <label class="bloque w30">Ocupación</label>
                </div>

                <div class="bestiloImput">
                    <input type="text" id="txtEmail" class="bloque w20" tabindex="108" />
                    <select id="cmbIdEtnia" class="bloque w20 m20" tabindex="114">
                        <option value=""></option>
                        <% resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(2);  
                               ComboVO cmbEtn; 
                               for(int k=0;k<resultaux.size();k++){ 
                                   cmbEtn=(ComboVO)resultaux.get(k);
                            %>
                        <option value="<%= cmbEtn.getId()%>" title="<%= cmbEtn.getTitle()%>">
                            <%= cmbEtn.getDescripcion()%></option>
                        <%}%> 
     </select>
                         <select id="cmbIdNivelEscolaridad" class="bloque w20 m20" tabindex="115" >
                          <option value=""></option>
                            <% resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(4);  
                               ComboVO cmbNi; 
                               for(int k=0;k<resultaux.size();k++){ 
                                   cmbNi=(ComboVO)resultaux.get(k);
                            %>
                        <option value="<%= cmbNi.getId()%>" title="<%= cmbNi.getTitle()%>">
                            <%= cmbNi.getDescripcion()%></option>
                        <%}%> 
                         </select>                         
                        
                         <select  id="cmbIdEstrato" class="bloque w10 m20" tabindex="117" >
                          <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>   
                            <option value="3">3</option>   
                            <option value="4">4</option>   
                            <option value="5">5</option>   
                            <option value="6">6</option>   
                            <option value="7">7</option>
                            <option value="8">Sin Estrato</option>                            
                         </select>                         
                         
                         <div class="bloque w30">
                          <input id="txtIdOcupacion" type="text" tabindex="118" class="w80" onkeypress="llamarAutocomIdDescripcionConDato('txtIdOcupacion',8)" >    
                         <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaOcupT" onclick="traerVentanita(this.id,'','divParaVentanita','txtIdOcupacion','6')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
                         </div>               
    </div>
</div>

  </td>
</tr>
<tr>
        <td align="CENTER">
            <input name="btn_crear_admsion" type="button" class="small button blue" value="MODIFICAR PACIENTE" onclick="modificarCRUD('modificarPaciente')"/> 
        </td>
    </tr>
       <tr>
         <td> 
          <div id="divAcordionInfoIngresoAdicional" style="display:BLOCK">      
                     <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="INFORMACION DE LA ADMISION ADICIONAL" />
                            <jsp:param name="idDiv" value="divInfoIngresoAdicional" />
                            <jsp:param name="pagina" value="infoIngresoAdicional.jsp" />                
                            <jsp:param name="display" value="NONE" />  
                            <jsp:param name="funciones" value="" />
                     </jsp:include> 
                </div> 

           
                <div id="divAcordionInfoIngreso" style="display:BLOCK">      
                     <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="INFORMACION DE LA ADMISION" />
                            <jsp:param name="idDiv" value="divInfoIngreso" />
                            <jsp:param name="pagina" value="infoIngreso.jsp" />                
                            <jsp:param name="display" value="BLOCK" />  
                            <jsp:param name="funciones" value="" />
                     </jsp:include> 
                </div> 
                                          
              
        </td>  
       </tr> 
       <tr>
         <td>   

<div class="btitulos" style="padding: 20px;">
    <div class="bloque w10">
      <input type="button" title="BLI33" class="small button blue" value="LIMPIAR CAMPOS" onclick="limpiarDatosAdmisiones()" />
    </div>
    
    <div class="bloque w15">
        <input title="BT17A" type="button" class="small button blue" value="BUSCAR ADMISIONES" onclick="buscarAGENDA('listAdmisionCuenta');"/> </div>

    <div class="bloque w15">
        <input type="button" class="small button blue" title="BTLL7" value="CREAR ADMISION" onclick="ventanaVerificacionAdmisionFactura()" />
    </div>

    <div class="bloque w15">
        <input type="button" class="small button blue" title="B3TK" value="ELIMINAR ADMISION" onclick="eliminarAdmisionFacturacion()" tabindex="309" />
    </div>

    <div class="bloque w15">
        <input type="button" class="small button blue" title="B5TL" value="MODIFICAR ADMISION" onclick="modificarAdmisionFacturacion()" tabindex="309" />
    </div>

    <div  class="bloque w15">
        <input type="button" title="BLI33" class="small button blue" value="MULTIPLE FACTURA" onclick="modificarCRUD('crearFacturaDesdeAdmision')" />
    </div>

    <div class="bloque w15">
        <input type="button" class="small button blue" value="CARGAR ADJUNTOS" onclick="mostrar('divVentanitaAdjuntos');acordionArchivosAdjuntos();"/>   
    </div>

    <div class="bloque w15">
        <input name="btn_crear_cita" type="button" class="small button blue" value="VER HISTORIA CLINICA" title="BU53" onclick=" asignaAtributo('lblIdAuxiliar', IdSesion(), 0); asignaAtributo('lblIdPaciente', valorAtributoIdAutoCompletar('txtIdBusPaciente'), 0);  asignaAtributo('lblIdAdministradora', valorAtributo('lblIdPlan'), 0);        mostrar('divVentanitaHistoricosAtencion');buscarHC('listDocumentosHistoricosTodos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');" tabindex="309" />
    </div>
</div>

</td>
</tr>
          
             <tr class="titulos" >   
                 <td colspan="8">  
                  <div style="height:200px">
                     <table id="listAdmisionCuenta"  class="scroll" width="100%"></table>  
                  </div>   
                 </td>
             </tr>   
       <tr>    
        <td>




    <div class="btitulos" style="background: white; ">
        <label  id="lblInternado"></label>
        <label class="bloque w10">ADMISIÓN</label>
        <label class="bloque w15">TIPO ADMISIÓN</label>
        <label class="bloque w20">PLAN</label>
        <label class="bloque w10">TIPO REGIMEN</label>
        <label class="bloque w15">TIPO AFILIADO</label>
        <label class="bloque w15">RANGO</label>
        <label class="bloque w15">USUARIO</label>
    </div>
    <div class="bestiloImput">
      <div class="bloque w10"> <label id="lblIdAdmision" > </label> </div>
        <div class="bloque w15"><label id="lblIdTipoAdmision"></label>-<label id="lblNomTipoAdmision"> </label></div>
        <div class="bloque w20"><label id="lblIdPlan"></label>-<label id="lblNomPlan"> </label></div>
        <div class="bloque w10"><label id="lblIdRegimen"></label>-<label id="lblNomRegimen"> </label></div>
        <div class="bloque w15"><label id="lblIdTipoAfiliado"></label>-<label id="lblNomTipoAfiliado"> </label></div>
        <div class="bloque w15"><label id="lblIdRango"></label>-<label id="lblNomRango"> </label></div>   
        <label id="lblUsuarioCrea" class="bloque w15"> </label>
    </div>


                <div id="divAcordionInfoCirugia" style="display:BLOCK">      
                     <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="INFORMACION DE PROCEDIMIENTOS CONtrATADOS A REALIZAR" />
                            <jsp:param name="idDiv" value="divInfoCirugia" />
                            <jsp:param name="pagina" value="infoCirugia.jsp" />                
                            <jsp:param name="display" value="BLOCK" />  
                            <jsp:param name="funciones" value="" />
                     </jsp:include> 
                </div> 

                <div id="divAcordionInfoFacturaInventario" style="display:BLOCK">      
                     <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="DETALLES FACTURA DE INVENTARIO"/>
                            <jsp:param name="idDiv" value="divInfoFacturaInventario"/>
                            <jsp:param name="pagina" value="infoFacturaInventario.jsp"/>                
                            <jsp:param name="display" value="none"/>
                            <jsp:param name="funciones" value="buscarFacturacion('listArticulosDeFactura')"/>
                     </jsp:include> 
                </div>

                 <div id="divAcordionInfoFactura" style="display:BLOCK">      
                     <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="DETALLES FACTURA DEL SERVICIO"/>
                            <jsp:param name="idDiv" value="divInfoFactura"/>
                            <jsp:param name="pagina" value="infoFacturaServicio.jsp"/>                
                            <jsp:param name="display" value="BLOCK"/>
                            <jsp:param name="funciones" value=""/>
                     </jsp:include> 
                </div> 

                <div id="divAcordionInfoFacturaDetalle" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                        <jsp:param name="titulo" value="DETALLES FACTURA" />
                        <jsp:param name="idDiv" value="divInfoFacturaDetalle" />
                        <jsp:param name="pagina" value="infoFacturaDetalle.jsp" />
                        <jsp:param name="display" value="BLOCK" />
                        <jsp:param name="funciones" value="" />
                    </jsp:include>
                </div> 


                <div id="divAcordionInfoFacturaRecibos" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                        <jsp:param name="titulo" value="RECIBOS FACTURA" />
                        <jsp:param name="idDiv" value="divInfoFacturaRecibos" />
                        <jsp:param name="pagina" value="infoFacturaRecibos.jsp" />
                        <jsp:param name="display" value="BLOCK" />
                        <jsp:param name="funciones" value="buscarFacturacion('listRecibosFactura')" />
                    </jsp:include>
                </div>

                <div id="divAcordionCartera" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                        <jsp:param name="titulo" value="PAGOS PACIENTE" />
                        <jsp:param name="idDiv" value="divInfoCartera" />
                        <jsp:param name="pagina" value="crearRecibosFactura.jsp" />
                        <jsp:param name="display" value="BLOCK" />
                        <jsp:param name="funciones" value="" />
                    </jsp:include>
                </div>
        </td>
    </tr>
</table>





<label id="lblUsuarioCrea" style="font-size:10px;"></label>
<label style="font-size:10px;"> Cita futura:</label> 
<label id="lblCitaFutura" align="left" style="font-size:10px;"></label>


 <div id="divVentanitaVerificacion"  style="display:none; z-index:2000; top:1px; left:211px;">
  
      <div class="transParencia" style="z-index:2001; background:#999; filter:alpha(opacity=5);float:left;">
      </div>  
      <div  style="z-index:2002; position:fixed; top:200px; left:100px; width:70%">  
          <table width="80%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
               <tr class="estiloImput" >
                   <td align="left" width="50%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaVerificacion')" /></td>  
                   <td align="right" width="50%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaVerificacion')" /></td>  
               <tr>    
               <tr class="titulos" >
                   <td >Tipo Identificación</td>  
                   <td >Identificación</td>
                   
               <tr>                 
               <tr class="estiloImput" >
                   <td >
						<select size="1" id="cmbTipoIdVerifica" style="width:55%" tabindex="100" >
                           <option value=""></option>                        
                           <option value="CC">Cedula de Ciudadania</option>
                           <option value="CE">Cedula de Extranjeria</option>
                           <option value="MS">Menor sin Identificar</option>
                           <option value="PA">Pasaporte</option>
                           <option value="RC">Registro Civil</option>
                           <option value="TI">Tarjeta de identidad</option>
                           <option value="CD">Carnet Diplomatico</option>
                           <option value="NV">Certificado nacido vivo</option>                           
                           <option value="AS">Adulto sin Identificar</option>                           
                       </select>                   
                   </td>  
                   <td >
                   <input  type="text" id="txtIdentificacionVerifica"  size="20" maxlength="20" onKeyPress="javascript:return teclearsoloTelefono(event);" tabindex="101"/>
                   </td>
               <tr>                                      
               <tr class="estiloImputListaEspera"> 
                   <td >
                        <input name="btn_crear_admsion" type="button" class="small button blue" title="BTN33" value="CREAR ADMISION SIN FACTURA" onclick="crearAdmisionSinFactura()"  />                     
                   </td> 
                   <td >
                        <input  type="button" class="small button blue" title="BTN3W" value="CREAR ADMISION CON FACTURA" onclick="crearAdmisionConFactura()"  />   
                   </td> 

               </tr>
         </table>  
         
         
      </div>     
  </div>

 <div id="divVentanitaAgendaAdmision"  style="display:none; z-index:2000; top:1px; width:600px; left:150px;">
       <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div> 
      <div  style="z-index:2002; position:fixed; top:100px; left:180px; width:75%">  
          <table width="100%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
               <tr class="estiloImput" >
                   <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAgendaAdmision')" /></td>  
                   <td></td>
                   <td></td>
                   <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAgendaAdmision')" /></td>  
               <tr> 
               <tr>
                  <td colspan="4">
                     <TABLE width="100%">
                     <tr class="titulos">      
                         <td width="10%">Sede</td>                                                      
                         <td width="30%">Paciente</td>               
                         <td width="15%">Especialidad</td>  
                         <td width="25%">Profesional</td>  
                         <td width="5%">Exito</td>             
                         <td width="15%">ADMISION</td>                          
                     </tr>  
                     <tr class="estiloImput">
                         <td>
                         	 <select size="1" id="cmbSede" style="width:99%" title="QC65"
                           onfocus="comboDependienteSede('cmbSede','557')">	 
								 <option value=""></option>
					                                                 
					            </select>                        
                         </td>
                         <td><input type="text" size="110"  maxlength="210"  id="txtIdBusPacienteCita" onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPacienteCita',307)"  style="width:95%"  tabindex="99" /> </td>               
                         <td>
                          <select size="1" id="cmbIdEspecialidadCita" style="width:95%"  tabindex="14" onfocus="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidadCita')">	                                        
                              <option value=""></option>
                                
                            					
                          </select> 
                         </td>
                         <td>
                          <select size="1" id="cmbIdProfesionalesCita" style="width:95%"  tabindex="14" onfocus="comboDependienteEmpresa('cmbIdProfesionalesCita','101')">	                                        
                            <option value=""></option>
                                                           
                          </select>                         
                         </td> 
                         <td>
                           <select size="1" id="cmbExito" style="width:90%"  tabindex="14"  >	                                        
                              <option value="S">Si</option>
                              <option value="N">No</option>                                
                           </select>
                         </td>              
                         <td>
                           <select size="1" id="cmbAdmision" style="width:90%"  tabindex="14"  >	   
                           <option value="P">Sin Admision y Pre Factura</option>                                      
                              <option value="N">Sin Admision</option>
                              <option value="S">Con Admision</option>  

                           </select>                         
                         </td>                          
                     </tr> 
                    </TABLE> 
                  </td>
               </tr>                                      
               <tr class="estiloImputListaEspera"> 

                  <td style="text-align: center">
                    <label>EPS: </label> 
                    <input type="text"  id="txtAdministraPaciente"  size="50"  maxlength="60"   onkeypress="llamarAutocompletarEmpresa('txtAdministraPaciente', 184)" style="width:50%"  tabindex="114" /> 
                    <img width="18px" height="18px" align="middle" title="VEN2" id="idLupitaVentanitaAdPaciente" onclick="traerVentanitaEmpresa(this.id,'divParaVentanita', 'txtAdministraPaciente', '24')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 

                </td>
               		<td style="text-align: center">
                    <label>Asistio: </label> 
                    <select size="1" id="cmbAsiste"  tabindex="14"  style="width:40%" >	      
                        <option value="">TODOS</option>
                        <option value="SI">Si</option>
                        <option value="NO">No</option>                                
                    </select>

                </td>
                  <td colspan="1">
                  	 <label>Fecha cita: </label>
                   <input size="10" maxlength="10" id="txtFechaTraerCita" tabindex="120" class="" type="text">
                   </td>                
                   <td colspan="3">
                   <input name="btn_crear_cita" type="button" class="small button blue" value="BUSCAR CITAS" title="B887" onclick="buscarAGENDA('listAgendaAdmisionCuentas');" tabindex="309"  />  
                   </td> 
               </tr>
               <tr class="titulos" >   
                 <td colspan="4">  
                    <table id="listAgendaAdmision" class="scroll" width="100%"></table>  
                 </td>
               </tr>                
         </table>  
         <input type="text" id="txtEstadoFacturaVentanitaAgenda"> 
      </div>     
  </div>
 
 <div id="divVentanitaEditarAgenda"  style="display:none; z-index:2000; top:1px; left:-211px;">
      <div class="transParencia" style="z-index:2003; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
      </div>  
      <div  style="z-index:2004; position:absolute; top:200px; left:60px; width:95%">
          <table width="90%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
               <tr class="estiloImput" >
                   <td align="left" colspan="4" width="15%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditarAgenda')" /></td>                
                   <td align="right"colspan="3" width="15%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditarAgenda')" /></td>  
               <tr>    
               <tr class="titulos" >
                   <td>Prefetencial</td> 
                   <td>Id Cita</td>  
                   <td>Paciente</td>
                   <td>Fecha cita</td> 
                   <td>Estado</td>  
                   <td>Motivo</td>   
                   <td>Clasificacion</td>                                  
               <tr>  
               <tr class="estiloImput">
                   <td>
                       <select id="cmbPreferencial" onchange="modificarCRUD('modificarEstadoPreferencial','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                           <option value="NO">NO</option>
                           <option value="SI">SI</option>
                       </select>
                   </td>
                   <td><label id="lblIdCitaEdit"></label></td>  
                   <td><label id="lblPacienteCitaEdit"></label></td>
                   <td><label id="lblFechaCitaEdit"></label></td> 
                   <td>
                     <select id="cmbEstadoCitaEdit" style="width:90%" tabindex="117" onfocus="limpiarDivEditarJuan('limpiarEstadoMotivoCitaEdit')">
                        <option value=""></option>
                          <%     resultaux.clear();
                                 resultaux=(ArrayList)beanAdmin.combo.cargar(99);	
                                 ComboVO cmbEst; 
                                 for(int k=0;k<resultaux.size();k++){ 
                                       cmbEst=(ComboVO)resultaux.get(k);
                          %>
                        <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>">
                            <%= cmbEst.getId()+"  "+cmbEst.getDescripcion()%></option>
                        <%}%>						
                     </select>                   
                   </td> 
               <td >
                 <select id="cmbMotivoConsultaEdit" style="width:90%" tabindex="118"  onfocus="comboDependiente('cmbMotivoConsultaEdit','cmbEstadoCitaEdit','96');limpiarDivEditarJuan('limpiarEstadoCitaEdit');">
                    <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(106);	
                               ComboVO cmbM; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbM=(ComboVO)resultaux.get(k);
                        %>
                        <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                            <%= cmbM.getId()+"  "+cmbM.getDescripcion()%></option>
                        <%}%>						
                   </select> 
               </td>  

                <td>
                    <select id="cmbMotivoConsultaClaseEdit" style="width:90%" tabindex="121" onfocus="comboDependienteDosCondiciones('cmbMotivoConsultaClaseEdit','cmbMotivoConsultaEdit','txtIdEspecialidad','563')">
                    <option value=""></option>                      
                </select> 
                </td>


               <tr>                                    
               <tr class="estiloImputListaEspera"> 
                   <td colspan="8">

                    <input id="BTN_MODIFICAR_ESTADO_CITA" title="SOLO SI NO TIENE ADMISION" type="button" class="small button blue" value="MODIFICAR ESTADO CITA" onclick="modificarCRUD('modificarEstadoCitaCuenta','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="309"  />
                    <input id="BTN_MODIFICAR_PREFACTURA" type="button" class="small button blue" value="MODIFICAR ESTADO CITA PREFACTURA" onclick="modificarCRUD('modificarEstadoCitaPrefactura');" tabindex="309"  />
                   </td> 
               </tr>
         </table>  
      </div>     
  </div>


  <div id="divVentanitaAdmisionUrgencias"  style="display:none; z-index:2000; top:1px; width:1200px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        <div style="z-index:2002; position:fixed; top:100px; left:180px; width:75%">  
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdmisionUrgencias')" /></td>  
                    <td align="right" colspan="5"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdmisionUrgencias')" /></td>  
                </tr> 
                <tr class="titulos">
                    <td width="15%">TIPO ID</td>
                    <td width="15%">IDENTIFICACION</td>
                    <td width="30%">ID PACIENTE:<label id="lblIdPacienteUrgencias" style="color: red;"></label></td>               
                    <td width="15%">SERVICIO</td>
                    <td width="10">ORDENES</td>       
                    <td width="5%">PREFERENCIAL</td>                  
                </tr>    
                <tr>
                    <td>
                        <select id="cmbTipoIdUrgencias" style="width: 95%;"> 
                            <option value=""></option>
                            <%     
                            resultaux.clear();
                            resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                            for(int k=0;k<resultaux.size();k++){ 
                                    cmbM=(ComboVO)resultaux.get(k);
                            %>
                        <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                            <%=cmbM.getId() + " " + cmbM.getDescripcion()%></option>
                        <%}%>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="txtIdentificacionUrgencias" style="width: 80%;"
                            onKeyPress="javascript:checkKey3(event);" onInput="limpiarCamposURG('limpiarDatosPaciente')"/>
                        <img width="18px" height="18px" align="middle" id="imgLupaPacienteUrgencias"
                            onclick="traerVentanitaPaciente(this.id,'','divParaVentanita','txtIdBusPacienteUrgencias','27')"
                            src="/clinica/utilidades/imagenes/acciones/buscar.png">
                    </td>
                    <td>
                        <input type="text" id="txtIdBusPacienteUrgencias" style="width: 95%;" readonly/>
                    </td>               
                    <td>
                        <select id="cmbIdTipoServicioUrgencias"
                            onfocus="comboDependienteEmpresa('cmbIdTipoServicioUrgencias','163')" onchange="traerInfoServicio()"
                            style="width: 95%;">
                            <option value=""></option>
                        </select>
                    </td>
                    <td>
                        <select style="width: 95%;" id="cmbTieneOrden">
                            <option value="">[ TODO ]</option>
                            <option value="S">SI</option>
                            <option value="N">NO</option>
                        </select>
                    </td>
                    <td>
                        <select style="width: 95%;" id="cmbPreferencialURG">
                            <option value=""></option>
                            <option value="S">SI</option>
                            <option value="N">NO</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table width="100%">
                            <tr>
                                <td style="width: 33%;"><hr></td>
                                <td colspan="4" align="CENTER">
                                    <input type="button" class="small button blue" value="INGRESAR PACIENTE" onclick="verificarNotificacionAntesDeGuardar('ingresarPacienteURG');"/>                         
                                    <input type="button" class="small button blue" value="BUSCAR" onclick="buscarAGENDA('listaPacientesUrgencias', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');"/>  
                                    <input type="button" class="small button blue" value="LIMPIAR" onclick="limpiarCamposURG('limpiarDatosBusquedaURG');"/>  
                                </td>   
                                <td style="width: 33%;"><hr></td>                     
                            </tr>
                                            
                        </table> 
                    </td>
                </tr>
                <tr>
                    <td colspan="6">  
                        <table id="listaPacientesUrgencias" class="scroll" width="100%"></table>  
                    </td>
                </tr>
            </table>
        </div> 
   </div>    
</div>

<div id="divVentanitaHistoricosAtencion" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:180px; width:70%">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left" width="10%">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaHistoricosAtencion')" />
                </td>
                <td width="80%"></td>
                <td align="right" width="10%">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaHistoricosAtencion')" />
                </td>
            </tr>
            <tr class="titulos">
                <td></td>
                <td>HISTORICOS ATENCION</td>
                <td>  
                    IdAuxiliar=<label id="lblIdAuxiliar"></label>
                    IdPaciente=<label id="lblIdPaciente" style="size:1PX"></label>
                    Administradora=<label id="lblIdAdministradora" style="size:1PX"></label>

                    Especialidad<label style="font-size:10px" id="lblIdEspecialidad">1</label>
                </td>
            </tr>
            <tr class="titulos">
                <td colspan="3">
                    <table width="100%" cellpadding="0" cellspacing="0" align="center">
                        <tr class="titulos overs">
                            <td>Folio a crear</td>		
                        </tr>
                        <tr class="estiloImput overs">
                            <td >
                            <label id="lblIdDocumento"></label>
                            <select size="1" id="cmbTipoDocumento" style="width:40%" onFocus="comboCreacionDedocumentos()"
                                title="80">
                            </select>
                            <input type="text" id="txtIdEsdadoDocumento" style="width:1%" disabled="disabled" />
                            <label id="lblNomEstadoDocumento" style="size:1PX"></label>
                            <label class="parpadea text" style="font-size:10px; color:#F00;font-weight:bold"
                                id="lblSemeforoRiesgo"></label>
                            </td>
                        </tr>                        
                        <tr class="estiloImput overs">
                            <td width="100%" >
                            <input id="btnProcedimiento_a" type="button" class="small button blue" value="CREAR FOLIO" title="TT65" onClick="verificarNotificacionAntesDeGuardar('crearFolio')" />
                            <input type="button" onclick="cerrarDocumentClinicoSinImp()" value="FINALIZAR SIN IMPRIMIR" title="btn587" class="small button blue" id="btnFinalizarDocumento_">        
                            <input type="hidden" id="txtIdProfesionalElaboro" />
                            </td>
                        </tr>             
                    </table>
                </td>
            </tr> 

            <tr class="titulos">
                <td colspan="3">
                    <div id="divdatosbasicoscredesa" style="  height:130px; width:100%; overflow-y: scroll;" >
                                    <table id="listDocumentosHistoricos" class="scroll" cellpadding="0" cellspacing="0" align="center">
                                        <tr><th></th></tr>                     
                                    </table>
                    </div> 
                </td>
            </tr>

            <tr class="titulos"></tr>
                <td colspan="3"> 
                        <div id="divAcordionDiagnosticos_" style="display:BLOCK">
                        <jsp:include page="../../hc/hc/divsAcordeon.jsp" flush="FALSE"> 
                        <jsp:param name="titulo" value="Diagn&oacute;stico"/>
                        <jsp:param name="idDiv" value="divDiagnosticos_"/>
                        <jsp:param name="pagina" value="../../hc/hc/diagnosticos.jsp"/>
                        <jsp:param name="display" value="NONE"/>
                        <jsp:param name="funciones" value="buscarHC('listDiagnosticos' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                        </jsp:include>
                        </div>
                </td>
            </tr>                   

            <tr class="titulos">
                <td colspan="3"> 
                    <div id="divAcordionProcedimientos" style="display:BLOCK">
                        <jsp:include page="../../hc/hc/divsAcordeon.jsp" flush="FALSE"> 
                        <jsp:param name="titulo" value="Conducta y tratamiento"/>
                        <jsp:param name="idDiv" value="divProcedimientos" />
                        <jsp:param name="pagina"  value="../../hc/hc/contenidoPlanTratamiento.jsp"/>
                        <jsp:param name="display" value="NONE" />
                        <jsp:param name="funciones" value="cargarTablePlanTratamiento()" />
                        </jsp:include>
                    </div>
                </td>
            </tr>              




        </table>
    </div>
</div>


<div id="divVentanitaAdjuntos" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:180px; width:70%">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
                <td align="right">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="divAcordionAdjuntos" style="display:BLOCK">      
                        <div id="divParaArchivosAdjuntos"></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>


<div id="divCrearPacienteUrgencias"  style="display:none; z-index:2000; top:100px; left:50px; ">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div style="z-index:2004; position:fixed; top:200px;left:200px; width:70%">  
        <table width="70%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divCrearPacienteUrgencias')" /></td>              
                <td align="right" colspan="4"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divCrearPacienteUrgencias')" /></td>  
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="20%">Identificación</td>	                                                                                               
                            <td width="10%">Apellido 1</td>                                 
                            <td width="10%">Apellido 2</td>   
                            <td width="10%">Nombre 1</td> 
                            <td width="10%">Nombre 2</td>
                            <td width="40%">Acompañante</td>                                                               
                        </tr>		
                        <tr class="estiloImput"> 
                            <td>
                                <strong><label id="lblTipoIdCrearPacienteURG"></label></strong>
                                <strong><label id="lblIdCrearPacienteURG"></label></strong>
                            </td>    
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtApellido1Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>                            
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtApellido2Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>                                                                                                                                 
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtNombre1Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>      
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtNombre2Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>   
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtAcompananteUrgencias" style="width:95%" onblur="v28(this.value, this.id);"></td>                            
                        </tr>
                        <tr>
                            <td colspan="6">
                                <table>
                                    <tr class="titulos"> 
                                        <td width="10%">Sexo</td>	
                                        <td width="15%">Fecha Nacimiento</td>
                                        <td width="35%">Telefonos</td>  
                                        <td width="30%">Administradora EPS</td>                                                                                                                                                                     
                                    </tr>		
                                    <tr class="estiloImput"> 
                                        <td>
                                            <select id="cmbSexoUrgencias" style="width:95%">	                                        
                                                <option value=""> </option>  
                                                <option value="F">FEMENINO</option> 
                                                <option value="M">MASCULINO</option>                              
                                                <option value="O">OTRO</option>                                                          
                                            </select>	                   
                                        </td>
                                        <td><input style="width:95%" type="text" id="txtFechaNacimientoUrgencias"/></td>      
                                        <td>
                                            <input type="text"  id="txtTelefonoUrgencias" style="width:30%"  size="50"  maxlength="50"  tabindex="20">
                                            <input type="text"  id="txtCelular1Urgencias" style="width:30%"  size="50"  maxlength="50"  tabindex="20">
                                            <input type="text"  id="txtCelular2Urgencias" style="width:30%"  size="50"  maxlength="50"  tabindex="20">
                                        </td>                                                             
                                        <td><input type="text"  id="txtAdministradoraUrgencias" onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradoraUrgencias', 308)" style="width:95%"/>                                  	 </td>                                                                                                                                                                
                                    </tr>  
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <table width=681.9>
                                    <tr class="titulos"> 
                                        <td width="30%">Municipio</td> 
                                        <td width="30%">Direccion</td>   
                                        <td width="20%">Regimen Afiliacion</td>
                                        <td width="20%">Tipo Regimen</td>
                                    </tr>		
                                    <tr class="estiloImput"> 
                                        <td><input type="text" id="txtMunicipioResidenciaUrgencias"  style="width:85%">
                                            <img width="18px" height="18px" align="middle" title="VEN1" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipioResidenciaUrgencias', '1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                                            <div id="divParaVentanita"></div>                                                    
                                        </td>    
                                        <td><input type="text" id="txtDireccionUrgencias" style="width:95%" onblur="v28(this.value, this.id);" tabindex="20"></td>                            
                                        <td>
                                            <select id="cmbRegimenUrgencias" style="width:95%">
                                                <option value="A">PARTICULAR</option>
                                                <option value="C">CONTRIBUTIVO</option>
                                                <option value="S">SUBSIDIADO</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="cmbTipoRegimenUrgencias" style="width:95%">
                                                <option value="0">OTRO</option>
                                                <option value="1">BENEFICIARIO</option>
                                                <option value="2">COTIZANTE</option>
                                            </select> 
                                        </td>
                                    </tr> 
                                </table> 
                            </td>
                        </tr>
                        <tr class="titulos">
                            <td align="CENTER" colspan="6"> 
                                <input type="button" class="small button blue" value="CREAR PACIENTE" onclick="modificarCRUD('crearPacienteUrgencias');"/>                         
                            </td>
                        </tr>
                    </table> 
                </td>
            </tr>
        </table>  
    </div>     
</div>


<div id="divAsociarCita" style="display:none; z-index:2000; top:100px; left:50px;">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div style="z-index:2004; position:fixed; top:200px;left:100; width:70%">  
        <table width="90%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divAsociarCita')" /></td>              
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divAsociarCita')" /></td>  
            </tr>
            <tr class="titulosCentrados" >
                <td width="100%" colspan="2">Asociar citas a la admision actual</td>
            <tr>
            <tr>
                <td colspan="2"><hr></td>
            </tr>
            <tr class="titulos">
                <td colspan="2">ID ADMSION ACTUAL:<label id="lblIdAdmisionAsociarCita" style="color: red;"></label> / ID FACTURA:<label id="lblIdFacturaAsociarCita"></label>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <tr class="titulosCentrados">
                            <td>CITAS PENDIENTES</td>
                            <td>CITAS CON ADMISION</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" id="citasDisponiblesAsociar"></table>
                            </td>
                            <td>
                                <table width="100%" id="citasAdmision"></table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="CENTER">
                    <!--<input type="button" class="small button blue" value="ASOCIAR CITAS SELECCIONADAS" onclick="verificarNotificacionAntesDeGuardar('asociarCitasAdmision')" style="width: 40%;"/>-->                  
                </td>
                <td>
                  <input type="button" class="small button blue" value="VER FORMATO DE FIRMAS" onclick="imprimirPDFFormatoFirmas()" style="width: 40%;"/>                         
                </td>
            </tr>
        </table>  
    </div>  
</div>

<div id="divmodificarAdmisionPacienteURG" style="display:none; z-index:2000; top:100px; left:50px; ">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div style="z-index:2004; position:fixed; top:200px;left:200px; width:70%">  
        <table width="90%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla" >
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divmodificarAdmisionPacienteURG')" /></td>              
                <td align="right" colspan="5"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divmodificarAdmisionPacienteURG')" /></td>  
            </tr>
            <tr class="titulos">
                <td>ID PACIENTE: <label id="lblIdPacienteModificarURG" style="color: red;"></label></td>
                <td><strong><label id="lblIdentificacionPacienteModificarURG" style="width: 95%;"></label></strong></td>
                <td colspan="2"><strong><label id="lblNombrePacienteModificarURG" style="width: 95%;"></label></strong></td>
                <td>ID ADMSION:<label id="lblIdAdmisionModificarURG"></label> / ID FACTURA:<label id="lblIdFactura"></label>
            </tr>
            <tr>
                <td colspan="6"><hr></td>
            </tr>
            <tr class="titulos">
                <td>SERVICIO</td>
                <td>ADMISION ACTUAL</td>
                <td>PROFESIONAL</td>
                <td>PREFERENCIAL</td>
                <td>ESTADO</td>
                <td></td>
            </tr>
            <tr class="estiloImput">
                <td>
                    <strong><label id="lblIdServicio"></label>:<label id="lblServicio"></label></strong>
                </td>
                <td>
                    <select id="cmbIdTipoAdmisionModificarURG" style="width: 95%;"> 
                        <option value=""></option>
                    </select>
                </td>
                <td>
                    <select id="cmbProfesionalAdmision" style="width: 95%;">
                        <option value=""></option>
                    </select>
                </td>
                <td>
                    <select id="cmbModificarPreferencialURG" style="width: 90%;"> 
                        <option value="S">SI</option>
                        <option value="N">NO</option>
                    </select>
                </td>
                <td>
                    <select style="width: 95%;" id="cmbModificarEstadoAdmisionURG">
                        <option value=""></option>
                        <%     
                        resultaux.clear();
                        resultaux=(ArrayList)beanAdmin.combo.cargar(158);	
                        for(int k=0;k<resultaux.size();k++){ 
                                cmbM=(ComboVO)resultaux.get(k);
                        %>
                        <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                            <%=cmbM.getDescripcion()%></option>
                        <%}%>
                    </select>
                </td>
                <td>
                    <input type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificarServicioPacienteURG');" style="width: 90%;"/>                         
                </td>
            </tr>
            <tr class="titulosCentrados">
                <td colspan="6">Ordenes pendientes por gestionar</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table style="width: 100%;" id="ordenesNoGestionadasURG"></table>
                </td>
            </tr>
            <tr>
                <td><hr></td>
                <td align="CENTER" colspan="4">
                    <input type="button" class="small button blue" value="VER ADMISION" onclick="traerAdmisionPacienteURG()" style="width: 90%;"/>                         
                </td>
                <td><hr></td>
            </tr>
        </table>  
    </div>  
</div>

<input type="hidden" id="txtSoloAdmision" value="SI" /> 
<input type="hidden" id="txtIdEspecialidad" /> 
<input type="hidden" id="txtEsCirugia" /> 
<input type="hidden" id="txtAdmisiones" value="SI" /> 
<input type="hidden" id="txtEstadoAdmisionFactura" /> 

<input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />       
<input type="hidden"  id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden"  id="txtTipoDocumentoVistaPrevia" value="" />                        
<input type="hidden"  id="lblTituloDocumentoVistaPrevia" value=""  />   
<input type="hidden"  id="lblIdAdmisionVistaPrevia" value=""  />    
<input type="hidden" id="txtPrincipalHC" value="NO"/> 
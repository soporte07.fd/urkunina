
<table width="100%">
    <tr class="titulos">
        <td width="50%">Articulo</td>
        <td width="20%">Valor Unitario</td>
        <td width="20%">Cantidad</td>
        <td width="10%"></td>
    </tr>
    <tr class="estiloImput">
        <td>
            <input type="text" style="width: 95%;" id="txtIdArticuloFactura" onfocus="llamarAutocomIdDescripcionParametro('txtIdArticuloFactura', 753, 'lblIdPlanContratacion')" autocomplete="off" onblur="traerValorUnitarioArticuloCuenta()"/>
        </td>
        <td>
            <label id="lblValorUnitarioArticuloFactura" style="width: 95%;"></label>
        </td>
        <td>
            <select id="cmbCantidadArticuloFactura" style="width: 60%;">
                <option value=""></option>  
                <% for(int k=1;k<16;k++){%>
                    <option value="<%= k%>" title="<%= k%>"><%= k%></option>
                <%}%>                    
            </select>
        </td>
        <td>
            <input type="button" style="width: 95%;" class="small button blue" value="ADICIONAR" onclick="modificarCRUD('adicionarArticulosFactura');"/>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <table id="listArticulosDeFactura" class="scroll w100"></table>
        </td>
    </tr>
</table>

<div id="divVentanitaArticuloFactura" class="ventanita">
    <div class="transParencia fondoVentanita">
    </div>
    <div class="fondoVentanitaContenido w40 t200 l250 fondoTabla">
        <div>
            <div class="estiloImput">
                <input name="btn_cerrar" type="button" value="CERRAR" onclick="ocultar('divVentanitaArticuloFactura')" style="float: left; " />
                <input name="btn_cerrar" type="button" value="CERRAR" onclick="ocultar('divVentanitaArticuloFactura')" style=" float: right;" />
            </div>
            <div class="btitulos">
                <label class="bloque w15">TRANSACCION</label>
                <label class="bloque w15">ID ARTICULO</label>
                <label class="bloque w70">ARTICULO</label>
            </div>
            <div class="bestiloImput">
                <label id="lblIdTransaccionArticuloVentanita" class="bloque w15"></label>
                <label id="lblIdArticuloVentanita" class="bloque w15"></label>
                <label id="lblNomArticuloVentanita" class="bloque w70"></label>
            </div>
            <div class="estiloImputListaEspera">
                <input title="BTVA1" type="button" class="small button blue" value="ELIMINAR ARTICULO" onclick="modificarCRUD('eliminarArticulosFactura')" tabindex="309" />
            </div>
        </div>
    </div>
</div>





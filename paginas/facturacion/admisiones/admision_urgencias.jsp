<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>


<table width="1200px" align="center" border="0" cellspacing="0" cellpadding="1" class="fondoTabla">
    <tr>
        <td class="w100">
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="ADMISIONES" />
                </jsp:include>
            </div>
        </td>
    </tr>


    <tr>
        <td>
            <div>
                <div class="btitulos">
                    <label class="bloque w10">ID CITA</label>
                    <label class="bloque w5"></label>
                    <label class="bloque w5"></label>
                    <label class="bloque w20">SERVICIO</label>
                    <label class="bloque w60">PACIENTE</label>
                </div>
                <div class="bestiloImput">
                    <div class="bloque w10">
                        <input type="hidden" id="lblIdAgendaDetalle" />
                        <label id="lblIdCita"></label>
                        <input type="button" class="small button blue" title="ADM57" value="Agenda"
                            onclick="abrirVentanaTraerCitaAlaAdmisionCuenta()" />
                    </div>
                    <div class="bloque w5">
                        <input type="button" class="small button blue" value="Urgencias"
                            onclick="abrirVentanaAdmisionUrgencia()" />
                    </div>
                    <div class="bloque w20">
                        <select id="cmbIdTipoServicio" tabindex="14"
                            onfocus="comboDependienteEmpresa('cmbIdTipoServicio','74')" onchange="traerInfoServicio()"
                            class="bloque w90">
                            <option value=""></option>

                        </select>
                    </div>
                    <div class="bloque w60">
                        <input type="text" value="435-" id="txtIdBusPaciente" oncontextmenu="return false" tabindex="99"
                            class="w85" onfocus="javascript:return document.getElementById('txtApellido1').focus()" />
                        <input name="btn_busAgenda" type="button" class="small button blue" title="ADMI87"
                            value="BUSCAR." onclick="buscarInformacionBasicaPaciente()" />
                    </div>
                </div>
                <div class="btitulos">
                    <label class="bloque w10">Tipo Id</label>
                    <label class="bloque w20">Identificación</label>
                    <label class="bloque w25">Apellidos</label>
                    <label class="bloque w25">Nombres</label>
                    <label class="bloque w10">Fecha Nacimiento</label>
                    <label class="bloque w10">Sexo</label>
                </div>
                <div class="bestiloImput">
                    <div class="bloque w10">
                        <select id="cmbTipoId" class="w90" tabindex="100">
                            <option value="CC">Cedula de Ciudadania</option>
                            <option value="CE">Cedula de Extranjeria</option>
                            <option value="MS">Menor sin Identificar</option>
                            <option value="PA">Pasaporte</option>
                            <option value="RC">Registro Civil</option>
                            <option value="TI">Tarjeta de identidad</option>
                            <option value="CD">Carnet Diplomatico</option>
                            <option value="NV">Certificado nacido vivo</option>
                            <option value="AS">Adulto sin Identificar</option>
                        </select>
                    </div>
                    <div class="bloque w20">
                        <input type="text" id="txtIdentificacion" class="90w"
                            onfocus="javascript:return document.getElementById('txtApellido1').focus()"
                            onKeyPress="javascript:checkKey2(event); return teclearsoloTelefono(event);"
                            oncontextmenu="return false" tabindex="101" />
                        <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaPacienT"
                            onclick="traerVentanita(this.id,'','divParaVentanita','txtIdBusPaciente','7')"
                            src="/clinica/utilidades/imagenes/acciones/buscar.png">
                    </div>
                    <div class="bloque w25">
                        <input readonly type="text" id="txtApellido1"
                            onkeypress="javascript:return teclearsoloan(event);" onblur="v28(this.value,this.id);"
                            tabindex="102" style="width:45%" />
                        <input readonly type="text" id="txtApellido2"
                            onkeypress="javascript:return teclearsoloan(event);" onblur="v28(this.value,this.id);"
                            tabindex="103" style="width:45%" />
                    </div>
                    <div class="bloque w25">
                        <input readonly type="text" id="txtNombre1" onkeypress="javascript:return teclearsoloan(event);"
                            onblur="v28(this.value,this.id);" tabindex="104" style="width:45%" />
                        <input readonly type="text" id="txtNombre2" onkeypress="javascript:return teclearsoloan(event);"
                            onblur="v28(this.value,this.id);" tabindex="105" style="width:45%" />
                    </div>
                    <div class="bloque w10">
                        <input id="txtFechaNac" type="text" class="w80" tabindex="106" /> </div>
                    <select id="cmbSexo" class="bloque w10" tabindex="107">
                        <option value=""></option>
                        <option value="F">FEMENINO</option>
                        <option value="M">MASCULINO</option>
                        <option value="O">OtrO</option>
                    </select>
                </div>
                <div class="btitulos">
                    <label class="bloque w25">Municipio</label>
                    <label class="bloque w30">Dirección</label>
                    <label class="bloque w15">Acompañante</label>
                    <label class="bloque w10">Celular1</label>
                    <label class="bloque w10">Celular2</label>
                    <label class="bloque w10">Telefono</label>
                </div>

                <div class="bestiloImput">

                    <div class="bloque w25">
                        <input type="text" id="txtMunicipio" class="w80" tabindex="108" />
                        <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaMunicT"
                            onclick="traerVentanita(this.id,'','divParaVentanita','txtMunicipio','1')"
                            src="/clinica/utilidades/imagenes/acciones/buscar.png">
                        <div id="divParaVentanita"></div>
                    </div>

                    <input type="text" id="txtDireccionRes" class="bloque w30"
                        onkeypress="javascript:return teclearsoloan(event);" onblur="v28(this.value,this.id);"
                        tabindex="109" />
                    <input type="text" id="txtNomAcompanante" class="bloque w15"
                        onkeypress="javascript:return teclearExcluirCaracter(event);" tabindex="110" />
                    <input type="text" id="txtCelular1" class="bloque w10"
                        onkeypress="javascript:return teclearsoloan(event);" tabindex="111" />
                    <input type="text" id="txtCelular2" class="bloque w10"
                        onkeypress="javascript:return teclearsoloan(event);" tabindex="112" />
                    <input type="text" id="txtTelefonos" class="bloque w10"
                        onkeypress="javascript:return teclearsoloan(event);" tabindex="113" />

                </div>

                <div class="btitulos">
                    <label class="bloque w20">Email</label>
                    <label class="bloque w20">Etnia</label>
                    <label class="bloque w20">Nivel Escolaridad</label>
                    <label class="bloque w10">Estrato</label>
                    <label class="bloque w30">Ocupación</label>
                </div>

                <div class="bestiloImput">
                    <input type="text" id="txtEmail" class="bloque w20" tabindex="108" />
                    <select id="cmbIdEtnia" class="bloque w20 m20" tabindex="114">
                        <option value=""></option>
                        <% resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(2);  
                               ComboVO cmbEtn; 
                               for(int k=0;k<resultaux.size();k++){ 
                                   cmbEtn=(ComboVO)resultaux.get(k);
                            %>
                        <option value="<%= cmbEtn.getId()%>" title="<%= cmbEtn.getTitle()%>">
                            <%= cmbEtn.getDescripcion()%></option>
                        <%}%> 
     </select>
                         <select id="cmbIdNivelEscolaridad" class="bloque w20 m20" tabindex="115" >
                          <option value=""></option>
                            <% resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(4);  
                               ComboVO cmbNi; 
                               for(int k=0;k<resultaux.size();k++){ 
                                   cmbNi=(ComboVO)resultaux.get(k);
                            %>
                        <option value="<%= cmbNi.getId()%>" title="<%= cmbNi.getTitle()%>">
                            <%= cmbNi.getDescripcion()%></option>
                        <%}%> 
                         </select>                         
                        
                         <select  id="cmbIdEstrato" class="bloque w10 m20" tabindex="117" >
                          <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>   
                            <option value="3">3</option>   
                            <option value="4">4</option>   
                            <option value="5">5</option>   
                            <option value="6">6</option>   
                            <option value="7">7</option>
                            <option value="8">Sin Estrato</option>                            
                         </select>                         
                         
                         <div class="bloque w30">
                          <input id="txtIdOcupacion" type="text" tabindex="118" class="w80" onkeypress="llamarAutocomIdDescripcionConDato('txtIdOcupacion',8)" >    
                         <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaOcupT" onclick="traerVentanita(this.id,'','divParaVentanita','txtIdOcupacion','6')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
                         </div>               
    </div>
</div>

  </td>
</tr>
<tr>
        <td align="CENTER">
            <input name="btn_crear_admsion" type="button" class="small button blue" value="MODIFICAR PACIENTE" onclick="modificarCRUD('modificarPaciente')"  />
             <input type="button" class="small button blue" value="CARGAR ADJUNTOS" onclick="mostrar('divVentanitaAdjuntos');acordionArchivosAdjuntos();"  />    
        </td>
    </tr>
       <tr>
         <td> 

          <div id="divAcordionInfoIngresoAdicional" style="display:BLOCK">      
                     <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="INFORMACION DE LA ADMISION ADICIONAL" />
                            <jsp:param name="idDiv" value="divInfoIngresoAdicional" />
                            <jsp:param name="pagina" value="infoIngresoAdicional.jsp" />                
                            <jsp:param name="display" value="NONE" />  
                            <jsp:param name="funciones" value="" />
                     </jsp:include> 
                </div> 

           
                <div id="divAcordionInfoIngreso" style="display:BLOCK">      
                     <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="INFORMACION DE LA ADMISION" />
                            <jsp:param name="idDiv" value="divInfoIngreso" />
                            <jsp:param name="pagina" value="infoIngreso.jsp" />                
                            <jsp:param name="display" value="BLOCK" />  
                            <jsp:param name="funciones" value="" />
                     </jsp:include> 
                </div> 
                                          
              
        </td>  
       </tr> 
       <tr>
         <td>   

<div class="btitulos" style="padding: 20px;">
    <div class="bloque w10">
      <input type="button" title="BLI33" class="small button blue" value="LIMPIAR CAMPOS" onclick="limpiarDatosAdmisiones()" />
    </div>
    
    <div class="bloque w15">
        <input title="BT17A" type="button" class="small button blue" value="BUSCAR ADMISIONES" onclick="buscarAGENDA('listAdmisionCuenta');"/> </div>

    <div class="bloque w15">
        <input type="button" class="small button blue" title="BTLL7" value="CREAR ADMISION" onclick="ventanaVerificacionAdmisionFactura()" />
    </div>

    <div class="bloque w15">
        <input type="button" class="small button blue" title="B3TK" value="ELIMINAR ADMISION" onclick="eliminarAdmisionFacturacion()" tabindex="309" />
    </div>

    <div class="bloque w15">
        <input type="button" class="small button blue" title="B5TL" value="MODIFICAR ADMISION" onclick="modificarAdmisionFacturacion()" tabindex="309" />
    </div>

    <div  class="bloque w15">
        <input type="button" title="BLI33" class="small button blue" value="MULTIPLE FACTURA" onclick="modificarCRUD('crearFacturaDesdeAdmision')" />
    </div>

    <div class="bloque w15">
        <input name="btn_crear_cita" type="button" class="small button blue" value="VER HISTORICOS DE ATENCION" title="BU53" onclick="mostrar('divVentanitaHistoricosAtencion');buscarHC('listDocumentosHistoricosTodos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');" tabindex="309" />
    </div>
</div>

</td>
</tr>
          
             <tr class="titulos" >   
                 <td colspan="8">  
                  <div style="height:200px">
                     <table id="listAdmisionCuenta"  class="scroll" width="100%"></table>  
                  </div>   
                 </td>
             </tr>   
       <tr>    
        <td>



<div>
    <div class="btitulos" style="background: white; ">
        <label class="bloque w10">ADMISIÓN</label>
        <label class="bloque w15">TIPO ADMISIÓN</label>
        <label class="bloque w20">PLAN</label>
        <label class="bloque w10">TIPO REGIMEN</label>
        <label class="bloque w15">TIPO AFILIADO</label>
        <label class="bloque w15">RANGO</label>
        <label class="bloque w15">USUARIO</label>
    </div>
    <div class="bestiloImput">
      <div class="bloque w10"> <label id="lblIdAdmision" > </label> </div>
        <div class="bloque w15"><label id="lblIdTipoAdmision"></label>-<label id="lblNomTipoAdmision"> </label></div>
        <div class="bloque w20"><label id="lblIdPlan"></label>-<label id="lblNomPlan"> </label></div>
        <div class="bloque w10"><label id="lblIdRegimen"></label>-<label id="lblNomRegimen"> </label></div>
        <div class="bloque w15"><label id="lblIdTipoAfiliado"></label>-<label id="lblNomTipoAfiliado"> </label></div>
        <div class="bloque w15"><label id="lblIdRango"></label>-<label id="lblNomRango"> </label></div>   
        <label id="lblUsuarioCrea" class="bloque w15"> </label>
    </div>


    <div id="divAcordionInfoCirugia" style="display:BLOCK">      
                     <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="INFORMACION DE PROCEDIMIENTOS CONtrATADOS A REALIZAR" />
                            <jsp:param name="idDiv" value="divInfoCirugia" />
                            <jsp:param name="pagina" value="infoCirugia.jsp" />                
                            <jsp:param name="display" value="BLOCK" />  
                            <jsp:param name="funciones" value="" />
                     </jsp:include> 
                </div> 

<div id="divAcordionInfoFacturaInventario" style="display:BLOCK">      
                     <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="DETALLES FACTURA DE INVENTARIO"/>
                            <jsp:param name="idDiv" value="divInfoFacturaInventario"/>
                            <jsp:param name="pagina" value="infoFacturaInventario.jsp"/>                
                            <jsp:param name="display" value="none"/>
                            <jsp:param name="funciones" value="buscarFacturacion('listArticulosDeFactura')"/>
                     </jsp:include> 
                </div>

                 <div id="divAcordionInfoFactura" style="display:BLOCK">      
                     <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="DETALLES FACTURA DEL SERVICIO"/>
                            <jsp:param name="idDiv" value="divInfoFactura"/>
                            <jsp:param name="pagina" value="infoFacturaServicio.jsp"/>                
                            <jsp:param name="display" value="BLOCK"/>
                            <jsp:param name="funciones" value=""/>
                     </jsp:include> 
                </div> 

                <div id="divAcordionInfoFacturaDetalle" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                        <jsp:param name="titulo" value="DETALLES FACTURA" />
                        <jsp:param name="idDiv" value="divInfoFacturaDetalle" />
                        <jsp:param name="pagina" value="infoFacturaDetalle.jsp" />
                        <jsp:param name="display" value="BLOCK" />
                        <jsp:param name="funciones" value="" />
                    </jsp:include>
                </div> 


                <div id="divAcordionInfoFacturaRecibos" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                        <jsp:param name="titulo" value="RECIBOS FACTURA" />
                        <jsp:param name="idDiv" value="divInfoFacturaRecibos" />
                        <jsp:param name="pagina" value="infoFacturaRecibos.jsp" />
                        <jsp:param name="display" value="BLOCK" />
                        <jsp:param name="funciones" value="buscarFacturacion('listRecibosFactura')" />
                    </jsp:include>
                </div>




</div>

 </td>
 </tr>

</table>





<label id="lblUsuarioCrea" style="font-size:10px;"></label>
<label style="font-size:10px;"> Cita futura:</label> 
<label id="lblCitaFutura" align="left" style="font-size:10px;"></label>


 <div id="divVentanitaVerificacion"  style="display:none; z-index:2000; top:1px; left:211px;">
  
      <div class="transParencia" style="z-index:2001; background:#999; filter:alpha(opacity=5);float:left;">
      </div>  
      <div  style="z-index:2002; position:fixed; top:200px; left:100px; width:70%">  
          <table width="80%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
               <tr class="estiloImput" >
                   <td align="left" width="50%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaVerificacion')" /></td>  
                   <td align="right" width="50%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaVerificacion')" /></td>  
               <tr>    
               <tr class="titulos" >
                   <td >Tipo Identificación</td>  
                   <td >Identificación</td>
                   
               <tr>                 
               <tr class="estiloImput" >
                   <td >
						<select size="1" id="cmbTipoIdVerifica" style="width:55%" tabindex="100" >
                           <option value=""></option>                        
                           <option value="CC">Cedula de Ciudadania</option>
                           <option value="CE">Cedula de Extranjeria</option>
                           <option value="MS">Menor sin Identificar</option>
                           <option value="PA">Pasaporte</option>
                           <option value="RC">Registro Civil</option>
                           <option value="TI">Tarjeta de identidad</option>
                           <option value="CD">Carnet Diplomatico</option>
                           <option value="NV">Certificado nacido vivo</option>                           
                           <option value="AS">Adulto sin Identificar</option>                           
                       </select>                   
                   </td>  
                   <td >
                   <input  type="text" id="txtIdentificacionVerifica"  size="20" maxlength="20" onKeyPress="javascript:return teclearsoloTelefono(event);" tabindex="101"/>
                   </td>
               <tr>                                      
               <tr class="estiloImputListaEspera"> 
                   <td >
                        <input name="btn_crear_admsion" type="button" class="small button blue" title="BTN33" value="CREAR ADMISION SIN FACTURA" onclick="crearAdmisionSinFactura()"  />                     
                   </td> 
                   <td >
                        <input  type="button" class="small button blue" title="BTN3W" value="CREAR ADMISION CON FACTURA" onclick="crearAdmisionConFactura()"  />   
                   </td> 

               </tr>
         </table>  
         
         
      </div>     
  </div>

 <div id="divVentanitaAgendaAdmision"  style="display:none; z-index:2000; top:1px; width:1200px; left:150px;">
       <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div> 
      <div  style="z-index:2002; position:fixed; top:100px; left:180px; width:75%">  
          <table width="100%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
               <tr class="estiloImput" >
                   <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAgendaAdmision')" /></td>  
                   <td></td>
                   <td></td>
                   <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAgendaAdmision')" /></td>  
               <tr> 
               <tr>
                  <td colspan="4">
                     <TABLE width="100%">
                     <tr class="titulos">      
                         <td width="10%">Sede</td>                                                      
                         <td width="30%">Paciente</td>               
                         <td width="15%">Especialidad</td>  
                         <td width="25%">Profesional</td>  
                         <td width="5%">Exito</td>             
                         <td width="15%">ADMISION</td>                          
                     </tr>  
                     <tr class="estiloImput">
                         <td>
                         	 <select size="1" id="cmbSede" style="width:99%" title="QC65"
                           onfocus="comboDependienteEmpresa('cmbSede','65')">	 
								 <option value=""></option>
					                                                 
					            </select>                        
                         </td>
                         <td><input type="text" size="110"  maxlength="210"  id="txtIdBusPacienteCita" onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPacienteCita',307)"  style="width:95%"  tabindex="99" /> </td>               
                         <td>
                          <select size="1" id="cmbIdEspecialidadCita" style="width:95%"  tabindex="14" onfocus="comboDependienteEmpresa('cmbIdEspecialidadCita','75')">	                                        
                              <option value=""></option>
                                
                            					
                          </select> 
                         </td>
                         <td>
                          <select size="1" id="cmbIdProfesionalesCita" style="width:95%"  tabindex="14" onfocus="comboDependienteEmpresa('cmbIdProfesionalesCita','77')">	                                        
                            <option value=""></option>
                                                           
                          </select>                         
                         </td> 
                         <td>
                           <select size="1" id="cmbExito" style="width:90%"  tabindex="14"  >	                                        
                              <option value="S">Si</option>
                              <option value="N">No</option>                                
                           </select>
                         </td>              
                         <td>
                           <select size="1" id="cmbAdmision" style="width:90%"  tabindex="14"  >	   
                           <option value="P">Sin Admision y Pre Factura</option>                                      
                              <option value="N">Sin Admision</option>
                              <option value="S">Con Admision</option>  

                           </select>                         
                         </td>                          
                     </tr> 
                    </TABLE> 
                  </td>
               </tr>                                      
               <tr class="estiloImputListaEspera"> 

                  <td style="text-align: center">
                    <label>EPS: </label> 
                    <input type="text"  id="txtAdministraPaciente"  size="50"  maxlength="60"   onkeypress="llamarAutocompletarEmpresa('txtAdministraPaciente', 184)" style="width:50%"  tabindex="114" /> 
                    <img width="18px" height="18px" align="middle" title="VEN2" id="idLupitaVentanitaAdPaciente" onclick="traerVentanitaEmpresa(this.id,'divParaVentanita', 'txtAdministraPaciente', '24')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 

                </td>
               		<td style="text-align: center">
                    <label>Asistio: </label> 
                    <select size="1" id="cmbAsiste"  tabindex="14"  style="width:40%" >	      
                        <option value="">TODOS</option>
                        <option value="SI">Si</option>
                        <option value="NO">No</option>                                
                    </select>

                </td>
                  <td colspan="1">
                  	 <label>Fecha cita: </label>
                   <input size="10" maxlength="10" id="txtFechaTraerCita" tabindex="120" class="" type="text">
                   </td>                
                   <td colspan="3">
                   <input name="btn_crear_cita" type="button" class="small button blue" value="BUSCAR CITAS" title="B887" onclick="buscarAGENDA('listAgendaAdmisionCuentas');" tabindex="309"  />  
                   </td> 
               </tr>
               <tr class="titulos" >   
                 <td colspan="4">  
                    <table id="listAgendaAdmision" class="scroll" width="100%"></table>  
                 </td>
               </tr>                
         </table>  
         <input type="text" id="txtEstadoFacturaVentanitaAgenda"> 
      </div>     
  </div>
 
 <div id="divVentanitaEditarAgenda"  style="display:none; z-index:2000; top:1px; left:-211px;">
      <div class="transParencia" style="z-index:2003; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
      </div>  
      <div  style="z-index:2004; position:absolute; top:200px; left:60px; width:95%">
          <table width="90%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
               <tr class="estiloImput" >
                   <td align="left" colspan="4" width="15%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditarAgenda')" /></td>                
                   <td align="right"colspan="3" width="15%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditarAgenda')" /></td>  
               <tr>    
               <tr class="titulos" >
                   <td>Prefetencial</td> 
                   <td>Id Cita</td>  
                   <td>Paciente</td>
                   <td>Fecha cita</td> 
                   <td>Estado</td>  
                   <td>Motivo</td>   
                   <td>Clasificacion</td>                                  
               <tr>  
               <tr class="estiloImput">
                   <td>
                       <select id="cmbPreferencial" onchange="modificarCRUD('modificarEstadoPreferencial','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                           <option value="NO">NO</option>
                           <option value="SI">SI</option>
                       </select>
                   </td>
                   <td><label id="lblIdCitaEdit"></label></td>  
                   <td><label id="lblPacienteCitaEdit"></label></td>
                   <td><label id="lblFechaCitaEdit"></label></td> 
                   <td>
                     <select id="cmbEstadoCitaEdit" style="width:90%" tabindex="117" onfocus="limpiarDivEditarJuan('limpiarEstadoMotivoCitaEdit')">
                        <option value=""></option>
                          <%     resultaux.clear();
                                 resultaux=(ArrayList)beanAdmin.combo.cargar(99);	
                                 ComboVO cmbEst; 
                                 for(int k=0;k<resultaux.size();k++){ 
                                       cmbEst=(ComboVO)resultaux.get(k);
                          %>
                        <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>">
                            <%= cmbEst.getId()+"  "+cmbEst.getDescripcion()%></option>
                        <%}%>						
                     </select>                   
                   </td> 
               <td >
                 <select id="cmbMotivoConsultaEdit" style="width:90%" tabindex="118"  onfocus="comboDependiente('cmbMotivoConsultaEdit','cmbEstadoCitaEdit','96');limpiarDivEditarJuan('limpiarEstadoCitaEdit');">
                    <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(106);	
                               ComboVO cmbM; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbM=(ComboVO)resultaux.get(k);
                        %>
                        <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                            <%= cmbM.getId()+"  "+cmbM.getDescripcion()%></option>
                        <%}%>						
                   </select> 
               </td>  

                <td>
                    <select id="cmbMotivoConsultaClaseEdit" style="width:90%" tabindex="121" onfocus="comboDependienteDosCondiciones('cmbMotivoConsultaClaseEdit','cmbMotivoConsultaEdit','txtIdEspecialidad','563')">
                    <option value=""></option>                      
                </select> 
                </td>


               <tr>                                    
               <tr class="estiloImputListaEspera"> 
                   <td colspan="8">

                    <input id="BTN_MODIFICAR_ESTADO_CITA" title="SOLO SI NO TIENE ADMISION" type="button" class="small button blue" value="MODIFICAR ESTADO CITA" onclick="modificarCRUD('modificarEstadoCitaCuenta','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="309"  />
                    <input id="BTN_MODIFICAR_PREFACTURA" type="button" class="small button blue" value="MODIFICAR ESTADO CITA PREFACTURA" onclick="modificarCRUD('modificarEstadoCitaPrefactura');" tabindex="309"  />
                   </td> 
               </tr>
         </table>  
      </div>     
  </div>


  <div id="divVentanitaAdmisionUrgencias"  style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        <div style="z-index:2002; position:fixed; top:100px; left:180px; width:75%">  
            <table width="80%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
                    <tr class="titulos">
                        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdmisionUrgencias')" /></td>  
                        <td align="right" colspan="5"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdmisionUrgencias')" /></td>  
                    </tr> 
                    <tr class="titulos">
                        <td width="15%">TIPO ID</td>
                        <td width="15%">IDENTIFICA</td>
                        <td width="35%">ID PACIENTE:<label id="lblIdPacienteUrgencias" style="color: red;"></label></td>               
                        <td width="15%">SERVICIO</td>    
                        <td width="10%">ESTADO ADMISION</td>      
                        <td width="10%">PREFERENCIAL</td>                  
                    </tr>    
                    <tr class="estiloImput">
                        <td>
                            <select id="cmbTipoIdUrgencias" style="width: 95%;"> 
                                <option value=""></option>
                                <%     
                                resultaux.clear();
                                resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                                for(int k=0;k<resultaux.size();k++){ 
                                        cmbM=(ComboVO)resultaux.get(k);
                                %>
                                <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                                <%=cmbM.getId() + " " + cmbM.getDescripcion()%></option>
                                <%}%>
                            </select>
                        </td>
                        <td>
                            <input type="text" id="txtIdentificacionUrgencias" width="90%"
                                onKeyPress="javascript:checkKey3(event);" onInput="limpiarCamposURG('limpiarDatosPaciente')"/>
                            <img width="18px" height="18px" align="middle" id="imgLupaPacienteUrgencias"
                                onclick="traerVentanitaUrgencias(this.id,'','divParaVentanita','txtIdBusPacienteUrgencias','7')"
                                src="/clinica/utilidades/imagenes/acciones/buscar.png">
                        </td>
                        <td>
                            <input type="text" id="txtIdBusPacienteUrgencias" class="bloque w90" readonly/>
                        </td>               
                        <td>
                            <select id="cmbIdTipoServicioUrgencias"
                                onfocus="comboDependienteEmpresa('cmbIdTipoServicioUrgencias','157')" onchange="traerInfoServicio()"
                                class="bloque w90">
                                <option value=""></option>
                            </select>
                        </td>
                        <td>
                            <select style="width: 95%;" id="cmbEstadoAdmisionURG">
                                <option value=""></option>
                                <%     
                                resultaux.clear();
                                resultaux=(ArrayList)beanAdmin.combo.cargar(158);	
                                for(int k=0;k<resultaux.size();k++){ 
                                        cmbM=(ComboVO)resultaux.get(k);
                                %>
                                <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                                <%=cmbM.getDescripcion()%></option>
                                <%}%>
                            </select>
                        </td>
                        <td>
                            <select style="width: 80%;" id="cmbPreferencialURG">
                                <option value=""></option>
                                <option value="S">SI</option>
                                <option value="N">NO</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><hr></td>
                        <td colspan="4" align="CENTER">
                            <input type="button" class="small button blue" value="CREAR ADMISION" onclick="modificarCRUD('ingresarPacienteURG');"/>                         
                            <input type="button" class="small button blue" value="BUSCAR" onclick="buscarAGENDA('listaPacientesUrgencias', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');"/>  
                            <input type="button" class="small button blue" value="LIMPIAR" onclick="limpiarCamposURG('limpiarDatosBusquedaURG');"/>  
                        </td>   
                        <td><hr></td>                     
                    </tr>
                    <tr>
                        <td colspan="6">  
                            <table id="listaPacientesUrgencias" class="scroll" width="100%"></table>  
                        </td>
                    </tr>                
            </table>  
            <input type="text" id="txtEstadoFacturaVentanitaAgendas">
        </div> 
   </div>    
</div>



<div id="divVentanitaHistoricosAtencion" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:180px; width:70%">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left" width="10%">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaHistoricosAtencion')" />
                </td>
                <td width="80%"></td>
                <td align="right" width="10%">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaHistoricosAtencion')" />
                </td>
            </tr>
            <tr class="titulos">
                <td></td>
                <td>HISTORICOS ATENCION</td>
                <td></td>
            </tr>
            <tr class="titulos">
                <td colspan="3">
                    <div id="divdatosbasicoscredesa" style="  height:130px; width:100%; overflow-y: scroll;" >
                                    <table id="listDocumentosHistoricos" class="scroll" cellpadding="0" cellspacing="0" align="center">
                                        <tr><th></th></tr>                     
                                    </table>
                                </div> 
                </td>
            </tr>
        </table>
    </div>
</div>


<div id="divVentanitaAdjuntos" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:180px; width:70%">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">

          <tr class="estiloImput">
                <td align="left" width="10%">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
                <td width="80%"></td>
                <td align="right" width="10%">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
            </tr>
 <tr class="estiloImput">
                <td colspan="2">

               <div id="divAcordionAdjuntos" style="display:BLOCK">      
                        <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="Archivos adjuntos" />
                            <jsp:param name="idDiv" value="divArchivosAdjuntos" />
                            <jsp:param name="pagina" value="../../archivo/contenidoAdjuntos.jsp" />                
                            <jsp:param name="display" value="BLOCK" />  
                            <jsp:param name="funciones" value="acordionArchivosAdjuntos()" />
                        </jsp:include> 
                    </div>
         </td>
            </tr>
        </table>
    </div>
</div>


<div id="divCrearPacienteUrgencias"  style="display:none; z-index:2000; top:100px; left:50px; ">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div style="z-index:2004; position:fixed; top:200px;left:200px; width:70%">  
        <table width="70%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERR" onclick="ocultar('divCrearPacienteUrgencias')" /></td>              
                <td align="right" colspan="4"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divCrearPacienteUrgencias')" /></td>  
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="20%">Identificación</td>	                                                                                               
                            <td width="20%">Apellido 1</td>                                 
                            <td width="20%">Apellido 2</td>   
                            <td width="20%">Nombre 1</td> 
                            <td width="20%">Nombre 2</td>                                                               
                        </tr>		
                        <tr class="estiloImput"> 
                            <td>
                                <strong><label id="lblTipoIdCrearPacienteURG"></label></strong>
                                <strong><label id="lblIdCrearPacienteURG"></label></strong>
                            </td>    
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtApellido1Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);" tabindex="20"></td>                            
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtApellido2Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);" tabindex="20"></td>                                                                                                                                 
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtNombre1Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);" tabindex="20"></td>      
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtNombre2Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);" tabindex="20"></td>                            
                        </tr>
                        <tr>
                            <td colspan="5">
                                <table>
                                    <tr class="titulos"> 
                                        <td width="10%">Sexo</td>	
                                        <td width="15%">Fecha Nacimiento</td>
                                        <td width="10%">Menor de edad</td>    
                                        <td width="35%">Telefonos</td>  
                                        <td width="30%">Administradora EPS</td>                                                                                                                                                                     
                                    </tr>		
                                    <tr class="estiloImput"> 
                                        <td>
                                            <select id="cmbSexoUrgencias" style="width:95%">	                                        
                                                <option value=""> </option>  
                                                <option value="F">Mujer</option> 
                                                <option value="M">Hombre</option>                              
                                                <option value="O">Otro</option>                                                          
                                            </select>	                   
                                        </td>     
                                        <td><input style="width:95%" type="text" id="txtFechaNacimientoUrgencias"/></td> 
                                        <td>
                                            Si<input type="radio" name="menorEdad">
                                            No<input type="radio" name="menorEdad">
                                        </td>
                                        <td>
                                            <input type="text"  id="txtTelefonoUrgencias" style="width:30%"  size="50"  maxlength="50"  tabindex="20">
                                            <input type="text"  id="txtCelular1Urgencias" style="width:30%"  size="50"  maxlength="50"  tabindex="20">
                                            <input type="text"  id="txtCelular2Urgencias" style="width:30%"  size="50"  maxlength="50"  tabindex="20">
                                        </td>                                                             
                                        <td><input type="text"  id="txtAdministradoraUrgencias" onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradoraUrgencias', 308)" style="width:95%"/>                                  	 </td>                                                                                                                                                                
                                    </tr>  
                                </table>
                            </td>
                        </tr>	
                        <tr class="titulos"> 
                            <td colspan="2">Municipio</td> 
                            <td colspan="2">Direccion</td>   
                            <td>Correo</td>                            
                        </tr>		
                        <tr class="estiloImput"> 
                            <td colspan="2"><input type="text" id="txtMunicipioResidenciaUrgencias"  style="width:85%">
                                <img width="18px" height="18px" align="middle" title="VEN1" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipioResidenciaUrgencias', '1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                                <div id="divParaVentanita"></div>                                                    
                            </td>    
                            <td colspan="2"><input type="text" id="txtDireccionUrgencias" style="width:95%" onblur="v28(this.value, this.id);" tabindex="20"></td>                            
                            <td>
                                <input type="text" id="txtEmailUrgencias" style="width:95%"/> 
                            </td>                            
                        </tr>  
                        <tr class="titulos">
                            <td align="CENTER" colspan="6"> 
                                <input type="button" class="small button blue" value="CREAR PACIENTE" onclick="modificarCRUD('crearPacienteUrgencias');"/>                         
                            </td>
                        </tr>
                    </table> 
                </td>
            </tr>
        </table>  
    </div>     
</div>


<div id="divmodificarAdmisionPacienteURG"  style="display:none; z-index:2000; top:100px; left:50px; ">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div style="z-index:2004; position:fixed; top:200px;left:200px; width:70%">  
        <table width="50%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla" >
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divmodificarAdmisionPacienteURG')" /></td>              
                <td align="right" colspan="4"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divmodificarAdmisionPacienteURG')" /></td>  
            </tr>
            <tr class="titulos">
                <td >ID PACIENTE: <label id="lblIdPacienteModificarURG" style="color: red;"></label></td>
                <td><strong><label id="lblIdentificacionPacienteModificarURG" style="width: 95%;"></label></strong></td>
                <td colspan="2"><strong><label id="lblNombrePacienteModificarURG" style="width: 95%;"></label></strong></td>
            </tr>
            <tr>
                <td colspan="4"><hr></td>
            </tr>
            <tr class="titulos">
                <td>SERVICIO</td>
                <td>PROFESIONAL</td>
                <td>PREFERENCIAL</td>
                <td>ID ADMSION: <label id="lblIdAdmisionModificarURG"></label></td>
            </tr>
            <tr class="estiloImput">
                <td>
                    <select id="cmbIdTipoServicioModificarURG" style="width: 95%;">
                        <option value=""></option>
                    </select>
                </td>
                <td>
                    <select id="cmbProfesionarOrdena" style="width: 95%;">
                        <option value=""></option>
                    </select>
                </td>
                <td>
                    <select id="cmbModificarPreferencialURG" style="width: 90%;">
                        <option value="S">SI</option>
                        <option value="N">NO</option>
                    </select>
                </td>
                <td>
                    <input type="button" class="small button blue" value="MDOFICAR" onclick="modificarCRUD('modificarServicioPacienteURG');" style="width: 95%;"/>                         
                </td>
            </tr>
            <tr>
                <td><hr></td>
                <td align="CENTER" colspan="2">
                    <input type="button" class="small button blue" value="VER ADMISION" onclick="traerAdmisionPacienteURG()" style="width: 90%;"/>                         
                </td>
                <td><hr></td>
            </tr>
        </table>  
    </div>     
</div>

<input type="hidden" id="txtSoloAdmision" value="SI" /> 
<input type="hidden" id="txtIdEspecialidad" /> 
<input type="hidden" id="txtEsCirugia" /> 
<input type="hidden" id="txtAdmisiones" value="SI" /> 
<input type="hidden" id="txtEstadoAdmisionFactura" /> 

<input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />       
<input type="hidden"  id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden"  id="txtTipoDocumentoVistaPrevia" value="" />                        
<input type="hidden"  id="lblTituloDocumentoVistaPrevia" value=""  />   
<input type="hidden"  id="lblIdAdmisionVistaPrevia" value=""  />    
<input type="hidden" id="txtPrincipalHC" value="NO"/> 
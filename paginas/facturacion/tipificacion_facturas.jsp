<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="1180px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="FACTURAS" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>

                        <div style="overflow:auto;height:500px; width:1180px" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <div id="divBuscar" style="display:block">
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                    <tr class="titulos" align="center">
                                        <td width="15%">ESTADO FACTURA</td>
                                        <td width="10%">FECHA CREA DESDE</td>
                                        <td width="10%">FECHA HASTA</td>
                                        <td width="10%">NUMERO FACTURA</td>
                                        <td width="15%">PLAN</td>
                                        <td width="30%">PACIENTE</td>
                                        <td width="10%">&nbsp;</td>
                                    </tr>



                                    <tr class="estiloImput">

                                        <td>
                                            <select size="1" id="cmbIdEstadoFactura" style="width:80%" tabindex="1">
                                                <option value=""></option>
                                                <% resultaux.clear();
                                                    resultaux = (ArrayList) beanAdmin.combo.cargar(558);
                                                    ComboVO cmbF;
                                                    for (int k = 0; k < resultaux.size(); k++) {
                                                        cmbF = (ComboVO) resultaux.get(k);
                                                %>
                                                <option value="<%= cmbF.getId()%>" title="<%= cmbF.getTitle()%>">
                                                    <%= cmbF.getDescripcion()%></option>
                                                    <%}%>                                 
                                            </select>


                                        </td>

                                        <td><input type="text" id="txtFechaDesdeFactura" tabindex="2" style="width:80%" /></td>
                                        <td><input type="text" id="txtFechaHastaFactura" tabindex="3" style="width:80%" /></td>

                                        <td><input type="text" id="txtBusNumFactura" style="width:80%" tabindex="4" /></td>

                                        <td>

                                            <select size="1" id="cmbIdPlan" style="width:80%" tabindex="128" onfocus="comboDependienteEmpresa('cmbIdPlan', '78')">
                                                <option value=""></option>

                                            </select>  

                                        </td>

                                        <td><input type="text" size="70" maxlength="70" style="width:90%" id="txtIdBusPaciente" tabindex="2" />
                                            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenT" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '7')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                            <div id="divParaVentanita"></div>
                                        </td>
                                        <td>
                                            <input title="BF932" type="button" class="small button blue" value="BUSCAR" onclick="buscarFacturacion('listGrillaFacturas')" />
                                        </td>
                                    </tr>

                                    <tr class="titulos" align="center">
                                        <td colspan="4" >FACTURADORES</td>
                                        <td colspan="3">PROCEDIMIENTOS</td>
                                    </tr>    

                                    <tr class="estiloImput">

                                        <td colspan="4">

                                            <select size="1" id="cmbIdFacturador" style="width:80%" tabindex="128" onfocus="comboDependienteEmpresa('cmbIdFacturador', '82')">
                                                <option value=""></option>

                                            </select>


                                        </td>

                                        <td colspan="3">
                                            <input type="text" id="txtIdProcedimiento" onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimiento', 300)" style="width:90%"/>

                                        </td>

                                    </tr>



                                </table>
                                <table id="listGrillaFacturas" class="scroll"></table>
                            </div>
                        </div>
                        <!-- div contenido-->

                        <div id="divEditar" style="display:block; width:100%">

                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulosCentrados">
                                    <td colspan="3">INFORMACION FACTURA
                                    </td>
                                </tr>
                                <tr>
                                    <td>



                                        <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Id Factura:</td>
                                                            <td width="70%"><label id="lblIdFactura"></label></td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Numero Factura:</td>
                                                            <td width="70%"><label id="lblNumeroFactura"></label></td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Estado Factura:</td>
                                                            <td width="70%"><label id="lblIdEstadoFactura"></label> - <label id="lblEstadoFactura"></label> </td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Total factura:</td>
                                                            <td width="70%"><label id="lblValorTotalFactura"></label></td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Paciente:</td>
                                                            <td width="70%"><label id="lblIdPaciente"></label>-<label id="lblNomPaciente"></label></td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Usuario Elaboro:</td>
                                                            <td>
                                                                <label id="lblIdUsuario"></label>-<label id="lblNomUsuario"></label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                        <table width="100%" style="cursor:pointer">
                                            <tr>
                                                <td width="99%" class="titulos">
                                                    <input title="BF73" type="button" class="small button blue" value="IMPRIMIR"
                                                           onclick="formatoPDFFactura(valorAtributo('lblIdFactura'));" />
                                                    <input title="BF73" type="button" class="small button blue" value="IMPRIMIR VARIAS FACTURAS"
                                                           onclick="imprimirFacturasPDF()" />
                                                    <input title="BF73" type="button" class="small button blue" value="TIPIFICAR FACTURAS"
                                                        onclick="tipificarFacturas(obtenerListaFacturasTabla('listGrillaFacturas'))"/>
                                                </td>
                                                <!--td width="99%" class="titulos">
                                                        <input title="BF73" type="button" class="small button blue" value="IMPRIMIR2"
                                                        onclick="prueba()"/>
                                                </td-->
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>

                        </div>
        </td>
    </tr>
</table>
</td>
</tr>
</table>

<input type="hidden" id="lblIdDoc" />
<input type="hidden" id="lblIdEstadoAuditoria" />
<input type="hidden" id="txtNoDevolucion"/>

<div id="divVentanitaArchivosTipificacion" style="display:none; z-index:2000; top:1px; width:700px; left:150px;">
    <div class="transParencia"
      style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:150px; left:150; width:700">
      <table width="100%" border="2" class="fondoTabla">
        <tr class="estiloImput">
            <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                onclick="ocultar('divVentanitaArchivosTipificacion')"/></td>
            <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                onclick="ocultar('divVentanitaArchivosTipificacion')"/></td>
          <tr>
          <tr class="titulos">
              <td align="center" colspan="2"><input align="center" type="button" class="boton" value="REFRESCAR LISTADO" onclick="buscarHistoria('listArchivosAdjuntosVarios')" /></td>
          </tr>                          
          <tr class="titulos">
              <td colspan="2">                                    
                  <table id="listArchivosTificacion"></table>
              </td>
          </tr>
      </table>
    </div>
  </div>

  <div id="divVentanitaAdjuntos" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:180px; width:70%">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
                <td align="right">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="divAcordionAdjuntos" style="display:BLOCK">      
                        <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                            <jsp:param name="titulo" value="Archivos adjuntos" />
                            <jsp:param name="idDiv" value="divArchivosAdjuntos" />
                            <jsp:param name="pagina" value="../archivo/contenidoAdjuntos.jsp" />                
                            <jsp:param name="display" value="BLOCK" />  
                            <jsp:param name="funciones" value="acordionArchivosAdjuntos()"/>
                        </jsp:include> 
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>

<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
    this.getServletConfig().getServletContext().setAttribute("userCn", beanSession.cn.getConexion());
%>

<table width="1100px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="PLAN DE CONTRATACION" />
        </jsp:include>
      </div>
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" class="fondoTabla">
        <tr class="titulos" align="center">
          <td width="20%">ESTADO</td>
          <td width="70%">PLAN DE CONTRATACION</td>
          <td></td>
        </tr>
        <tr>
          <td><select size="1" id="cmbEstadoBus" style="width:90%" tabindex="2">
              <option value="1">1-ACTIVO</option>
              <option value="0">0-INACTIVO</option>
              <option value="2">2-BORRADOR</option>
            </select>
          </td>
          <td><input type="text" id="txtNomBus" style="width:95%" tabindex="2" /></td>
          <td>
            <input name="btn_buscar" title="bw21" type="button" style="width:95%" class="small button blue" value="BUSCAR"
              onclick="buscarFacturacion('listGrillaPlanes')" />
          </td>
        </tr>
        <tr>
          <td colspan="3">
            <div style="height: 300px;">
              <table id="listGrillaPlanes"></table>
            </div>
          </td>
        </tr>
        <tr class="titulosCentrados">
          <td colspan="3">MANTENIMIENTO DEL PLAN CONTRATACION</td>
        </tr>
        <tr>
          <td colspan="3">
            <table width="100%">
              <td width="25%">ID PLAN:</td>
                <td width="75%"><input type="text" id="txtIdPlan" style="width:5%" tabindex="301" readonly/></td>
              </tr>
              <tr class="estiloImputIzq2">
                <td>DESCRIPCION</td>
                <td><input type="text" id="txtDescPlan" style="width:90%" tabindex="301" /></td>
              </tr>
              <tr class="estiloImputIzq2">
                <td>TERCERO:</td>
                <td><input type="text" id="txtIdAdministradora"
                    onkeypress="llamarAutocomIdDescripcionConDato('txtIdAdministradora',308)" style="width:60%"
                    tabindex="301"/></td>
              </tr>
              <tr class="estiloImputIzq2">
                <td>TIPO REGIMEN:</td>
                <td>
                  <select id="cmbIdTipoRegimen" style="width:40%" tabindex="301">
                    <option value=""></option>
                    <%     resultaux.clear();
                       resultaux=(ArrayList)beanAdmin.combo.cargar(518);	
                       ComboVO cmbtreg; 
                       for(int k=0;k<resultaux.size();k++){ 
                             cmbtreg=(ComboVO)resultaux.get(k);
                    %>
                    <option value="<%= cmbtreg.getId()%>" title="<%= cmbtreg.getTitle()%>">
                      <%= cmbtreg.getId()+"  "+cmbtreg.getDescripcion()%></option>
                    <%}%>						
                  </select>
                </td> 
            </tr>		              
            <tr class="estiloImputIzq2">                         
              <td>TIPO CLIENTE</td>
              <td>              
                  <select id="cmbIdTipoCliente" style="width:40%"  tabindex="301">
                      <option value=""></option>
                        <%     resultaux.clear();
                              resultaux=(ArrayList)beanAdmin.combo.cargar(20);	
                              ComboVO cmb1A; 
                              for(int k=0;k<resultaux.size();k++){ 
                                    cmb1A=(ComboVO)resultaux.get(k);
                        %>
                    <option value="<%= cmb1A.getId()%>" title="<%= cmb1A.getTitle()%>">
                      <%= cmb1A.getId()+"  "+cmb1A.getDescripcion()%></option>
                    <%}%>						
                  </select>
              </td> 
            </tr>	                          
            <tr class="estiloImputIzq2">                         
              <td>NUMERO CONTRATO:</td>
              <td>              
                  <input type="text" id="txtNumeroContrato" style="width:20%" tabindex="301"/>
              </td>                                             
            </tr>
            <tr class="estiloImputIzq2">                         
              <td>FECHA INICIO:</td>              
              <td><input type="text" id="txtFechaInicio" style="width:15%" tabindex="301" placeholder="dd/mm/aaaa"/></td> 
            </tr>
            <tr class="estiloImputIzq2">                         
              <td>FECHA FINAL:</td><td>               
                <input type="text" id="txtFechaFinal" style="width:15%" tabindex="301" placeholder="dd/mm/aaaa"/>
              </td>                                             
            </tr>            
            <tr class="estiloImputIzq2">                         
              <td>MONTO CONTRATO:</td><td>              
              <input type="text" id="txtMontoContrato" style="width:20%" tabindex="301"/>
              </td>                                             
            </tr>                              
            <tr class="estiloImputIzq2">                         
              <td>TIPO PLAN</td><td> 
                    <select id="cmbIdTipoPlan" style="width:40%" tabindex="301" >
                      <option value=""></option>
                        <%     resultaux.clear();
                              resultaux=(ArrayList)beanAdmin.combo.cargar(21);	
                              ComboVO cmb2A; 
                              for(int k=0;k<resultaux.size();k++){ 
                                    cmb2A=(ComboVO)resultaux.get(k);
                        %>
                    <option value="<%= cmb2A.getId()%>" title="<%= cmb2A.getTitle()%>">
                      <%= cmb2A.getDescripcion()%></option>
                    <%}%>						
                  </select>
              </td>                                             
            </tr>
            <tr class="estiloImputIzq2" hidden>                           
                <td>MOSTRAR DESCRIPCION PLAN EN PDF:</td><td>   <select size="1" id="cmbAdmiteRedondeo" style="width:5%" tabindex="301">  
                    <option value="N">NO</option>                              	 
                    <option value="S">SI</option>
                  </select>        
                </td>                                             
            </tr>            
            <tr class="estiloImputIzq2">                           
              <td>FECHA REGISTRO</td>
              <td><label id="lblFechaRegistro"></label></td>                                             
            </tr> 
            <tr class="estiloImputIzq2">                           
              <td>OBSERVACION</td>
              <td><textarea type="text" id="txtObservacion"  size="1000"  maxlength="1000" style="width:95%"  tabindex="301" > </textarea></td>                                             
            </tr>    
            <tr class="estiloImputIzq2" hidden>                         
              <td>SOLICITA AUTORIZACION EN ADMISION</td><td> 
                <select size="1" id="cmbSolicitaAutorizacionAdmision" style="width:10%" tabindex="301" >
                  <option value=""></option>                                	 
                  <option value="1">1-SI</option>
                  <option value="0">0-NO</option>                  
                </select> 
              </td>                                             
            </tr>    
            <tr class="estiloImputIzq2">                           
              <td>ELABORO</td>
              <td><label id="lblIdElaboro"></label></td>                                             
            </tr>   
            <tr class="estiloImputIzq2">                                   
              <td>ESTADO</td><td> 
                <select size="1" id="cmbIdEstadoEdit" style="width:20%" tabindex="301" >
                  <option value="1">1-ACTIVO</option>
                  <option value="0">0-INACTIVO</option>                  
                  <option value="2">2-BORRADOR</option>                                    
                </select>	 
              </td>                                             
            </tr hidden>  
              <tr class="estiloImputIzq2">                                   
                <td>VER EN HISTORIA CLINICA</td><td> 
                  <select size="1" id="cmbVerHc" style="width:10%" tabindex="301" >
                    <option value="S">SI</option>
                    <option value="N">NO</option>                                                     
                  </select>	 
                </td>                                             
              </tr>  
              <tr>
                <td colspan="2" align="center">
                  <input id="btn_limpia" title="btn_lp78"class="small button blue" type="button" value="LIMPIAR"  onclick="limpiarDivEditarJuan('planContratacion')">              
                  <input id="btn_crea" title="btn_cp742" class="small button blue" type="button"  value="CREAR PLAN CONTRATACION"  onclick="modificarCRUD('crearPlanContratacion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                               
                  <input name="btn_modifica" title="btn_mp124" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificaPlanContratacion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  
                  <input name="btn_elimina" title="btn_ep410" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('EliminarPlanContratacion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />                   
                </td>
              </tr>  
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="3">
            <div id="tabsPrincipalPlanes" style="width:98%; height:auto">
              <ul>
                  <li>
                      <a href="#divProcedimientosPlan" onclick="tabActivoPlanesContratacion('divProcedimientosPlan')">
                          <center>PROCEDIMIENTOS</center>
                      </a>
                  </li>
                  <li>
                      <a href="#divArticulosPlan" onclick="tabActivoPlanesContratacion('divArticulosPlan')">
                          <center>ARTICULOS</center>
                      </a>
                  </li>
                  <li>
                      <a href="#divRangos" onclick="tabActivoPlanesContratacion('divRangos')">
                          <center>RANGOS</center>
                      </a>
                  </li>
                  <li>
                      <a href="#divLiquidacion" onclick="tabActivoPlanesContratacion('divLiquidacion')">
                          <center>REGLAS DE LIQUIDACION</center>
                      </a>
                  </li>
                  <li>
                      <a href="#divLiquidacionProcedimientos" onclick="tabActivoPlanesContratacion('divLiquidacionProcedimientos')">
                          <center>LIQUIDACION DE PROCEDIMIENTOS</center>
                      </a>
                  </li>
                  <li>
                      <a href="#divMunicipiosPlan" onclick="tabActivoPlanesContratacion('divMunicipiosPlan')">
                          <center>MUNICIPIOS</center>
                      </a>
                  </li>
                  <li>
                      <a href="#divSedesPlan" onclick="tabActivoPlanesContratacion('divSedesPlan')">
                          <center>SEDES</center>
                      </a>
                  </li>
              </ul>

              <div id="divProcedimientosPlan">
                <table width="100%" cellpadding="0" cellspacing="0"  align="center">
                  <tr>
                    <td>
                      <table width="100%">        
                        <tr class="titulos" hidden>
                          <td align="center" colspan="4">
                            TARIFARIO BASE :
                            <select id="cmbIdTarifario" style="width:15%" tabindex="301">
                              <option value="0" selected></option>
                                <%     resultaux.clear();
                                      resultaux=(ArrayList)beanAdmin.combo.cargar(128);	
                                      ComboVO cmb8; 
                                      for(int k=0;k<resultaux.size();k++){ 
                                            cmb8=(ComboVO)resultaux.get(k);
                                %>
                              <option value="<%= cmb8.getId()%>" title="<%= cmb8.getTitle()%>"><%=cmb8.getDescripcion()%>
                              </option>
                              <%}%>						
                            </select>
                            PORCENTAJE DE INCREMENTO:
                            <input type="text" id="txtPorcentajeIncremento" style="width:5%" tabindex="301"/>%
                            <input id="btn_crea" title="BTi6" class="small button blue" type="button"  value="TRAER DEL TARIFARIO"  onclick="modificarCRUD('traerTarifarioPlanContratacion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="301" />   
                          </td>   
                        </tr>         
                        <tr class="estiloImputIzq2">
                          <td width="23%">PROCEDIMIENTO:</td><td width="70%"><input type="text" id="txtIdProcedimiento" onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimiento',300)" style="width:90%"/></td> 
                        </tr>	
                        <tr class="estiloImputIzq2">
                          <td width="23%">PRECIO:</td><td><input type="text" id="txtPrecioProcedimiento" maxlength="11" style="width:15%" /></td> 
                        </tr>	
                        <tr class="estiloImputIzq2">
                          <td width="23%">CODIGO EXTERNO:</td>
                          <td><input type="text" id="txtCodigoExterno"  maxlength="15" style="width:15%" /></td> 
                        </tr>	
                        <tr class="estiloImputIzq2">
                          <td width="23%">NOMBRE EXTERNO PARA FACTURA:</td>
                          <td>
                            <select size="1" id="cmbNombreExterno" onchange="desactivarNombreExterno()" style="width:15%"  tabindex="2">
                                <option value="N">NO</option>
                                <option value="S">SI</option>                  
                               </select> 
                          </td> 
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="23%">NOMBRE EXTERNO:</td><td width="70%"><input type="text" id="txtNombreExterno" disabled="disabled" style="width:90%"/></td> 
                        </tr> 
                        <tr class="estiloImputIzq2">
                          <td width="23%">CANTIDAD MAXIMA AL MES:</td><td width="70%"><input type="text" id="txtCantidadMaximaMes" maxlength="4" style="width:10%"/></td> 
                        </tr> 
                        <tr class="titulos">
                          <td colspan="2">
                            <table width="100%">
                              <tr>
                                <td >
                                  <input id="btn_limpia" title="BPE6"class="small button blue" type="button" value="LIMPIAR"  onclick="limpiarDivEditarJuan('limpiarProcedimientoPlan')">              
                                  <input id="btn_crea" title="BEE6" class="small button blue" type="button"  value="ADICIONAR"  onclick="modificarCRUD('adicionarProcedimientoPlan','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                               
                                  <input name="btn_modifica" title="bW8" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificarProcedimientoPlan','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  
                                  <input name="btn_elimina" title="bT68" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('eliminarProcedimientoPlan','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />     
                                </td>
                                <td>
                                   <form method="post" enctype="multipart/form-data" action="" target="iframeUpload" id="formExcel" name="formExcel">   
                                     <input id="fileExcel" name="fileExcel" type="file" tabindex="15" size="99%" class="">
                                     <input class="small button blue" type="button"  value="SUBIR TARIFAS"  onclick="comprobarExcelFacturacion()">
                                   </form>  
                                </td>
                              </tr>
                            </table>
                          </td>
            
                        </tr>  
                        <tr class="titulos">
                          <td colspan="2">
                            <div style="height: 200px;">
                              <table id="listGrillaPlanesDetalle"></table>
                              <div id="pager1"></div> 
                            </div>
                          </td>   
                        </tr> 
                       </table> 
                      </td>   
                  </tr>   
                </table> 
                <table width="100%" cellpadding="1" cellspacing="1"  align="center"> 
                  <tr class="titulosCentrados">
                    <td colspan="4">EXCEPCIONES A CANTIDAD MAXIMA DEL PROCEDIMIENTO </td>   
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="23%">CANTIDAD EXCEPCION:</td>
                    <td width="70%"><input type="text" id="txtCantidadExcepcion"/></td> 
                  </tr> 
                  <tr class="estiloImputIzq2">
                    <td width="23%">A&Ntilde;O:</td>
                    <td>
                      <select id="cmbyear"> 
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                      </select>
                    </td>
                  </tr>   
                  <tr class="estiloImputIzq2">
                    <td width="23%">MES:</td><td>
                    <select name="cmbmes" id="cmbmes">
                      <option value="1">Enero</option>
                      <option value="2">Febrero</option>
                      <option value="3">Marzo</option>
                      <option value="4">Abril</option>
                      <option value="5">Mayo</option>
                      <option value="6">Junio</option>
                      <option value="7">Julio</option>
                      <option value="8">Agosto</option>
                      <option value="9">Septiembre</option>
                      <option value="10">Octubre</option>
                      <option value="11">Noviembre</option>
                      <option value="12">Diciembre</option>
                    </select>
                    </td>
                  </tr>                    
                  <tr>
                     <td colspan="2" align="center">
                       <input id="btn_limpia" title="BPE6" class="small button blue" type="button" value="LIMPIAR"  onclick="limpiarDivEditarJuan('limpiarExcepcionesProcedimiento')">              
                       <input id="btn_crea" title="BEE6" class="small button blue" type="button"  value="ADICIONAR"  onclick="modificarCRUD('adicionarExcepcion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                               
                       <input name="btn_modifica" title="bW8" type="button" class="small button blue" value="MODIFICA" onclick="modificarCRUD('modificarExcepcion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  
                       <input name="btn_elimina" title="bT68" type="button" class="small button blue" value="ELIMINA" onclick="modificarCRUD('eliminarExcepcion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />
                       <input id="btn_ver"  title="BY57" class="small button blue" type="button"  value="VER"  onclick="buscarFacturacion('listGrillaPlanExcepcion')">                                 
                     </td>
                  </tr>  
                  <tr class="titulos">
                    <td colspan="4">
                      <div style="height: 200px">
                        <table id="listGrillaPlanExcepcion"></table>          
                      </div>
                    </td>   
                  </tr>       
                </table> 
              </div>

              <div id="divArticulosPlan">
                <table width="100%">       
                  <tr class="estiloImputIzq2">
                    <td width="24%">ART&iacute;CULO:</td>
                    <td><input type="text" style="width:70%" id="txtIdArticulo" maxlength="20" autocomplete="off" class="ac_input"></td>      
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">PRECIO</td>
                    <td><input type="text" tabindex="20" maxlength="11" style="width:15%" id="txtIdPrecioArticulo"></td>         
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">CODIGO EXTERNO</td>
                    <td ><input type="text" tabindex="20" maxlength="11" style="width:15%" id="txt_codExterno"></td>         
                  </tr> 
        
                  <tr class="estiloImputIzq2">
                    <td width="23%">NOMBRE EXTERNO PARA FACTURA:</td>
                    <td>
                      <select size="1" id="cmbNombreExternoArticulo" onchange="desactivarNombreExternoArticulo()" style="width:15%"  tabindex="2">
                          <option value="N">NO</option>
                          <option value="S">SI</option>                  
                         </select> 
                    </td> 
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="23%">NOMBRE EXTERNO:</td><td width="70%"><input type="text" id="txtNombreExternoArticulo" disabled="disabled" style="width:90%"/></td> 
                  </tr> 
                  <tr>
                    <td colspan="2" align="center">
                      <input id="btn_crea" title="BEU8" class="small button blue" type="button"  value="ADICION"  onclick="modificarCRUD('adicionarMedicamentoPlan')">                          
                      <input id="btn_crea" title="BEU7" class="small button blue" type="button"  value="MODIFICAR"  onclick="modificarCRUD('modificarMedicamentoPlan')">                                        
                      <input id="btn_crea" title="BEE7" class="small button blue" type="button"  value="ELIMINA"  onclick="modificarCRUD('eliminarMedicamentoPlan')">                               
                      <input id="btn_ver"  title="BY57" class="small button blue" type="button"  value="VER"  onclick="buscarFacturacion('listGrillaPlanesMedicamentos')">   
                    </td>                        
                  </tr> 
                  <tr class="titulos">
                    <td colspan="2">
                      <div style="height: 200px">
                        <table id="listGrillaPlanesMedicamentos"></table>          
                      </div>
                    </td>   
                  </tr> 
                 </table> 
              </div>

              <div id="divRangos">
                <table width="100%">      
                  <tr class="estiloImputIzq2">
                    <td width="24%">TIPO AFILIADO</td>
                    <td>
                      <select id="cmbIdTipoAfiliado" style="width:25%" tabindex="100" >
                             <option value=""></option>
                              <%     resultaux.clear();
                                     resultaux=(ArrayList)beanAdmin.combo.cargar(305);  
                                     ComboVO cmby8; 
                                     for(int k=0;k<resultaux.size();k++){ 
                                           cmby8=(ComboVO)resultaux.get(k);
                              %>
                                    <option value="<%= cmby8.getId()%>" title="<%= cmby8.getTitle()%>">
                                      <%=cmby8.getDescripcion()%></option>
                                    <%}%>            
                        </select>     
                    </td>         
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">RANGO</td>
                    <td>
                        <select id="cmbIdRango" style="width:30%" tabindex="100" >
                                <%     resultaux.clear();
                                       resultaux=(ArrayList)beanAdmin.combo.cargar(573);  
                                       ComboVO cmbRango; 
                                       for(int k=0;k<resultaux.size();k++){ 
                                             cmbRango=(ComboVO)resultaux.get(k);
                                %>
                                    <option value="<%= cmbRango.getId()%>" title="<%= cmbRango.getTitle()%>">
                                      <%= cmbRango.getDescripcion()%></option>
                                    <%}%>                                                
                        </select>        
                    </td>         
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">COPAGO</td>
                    <td >
                      <input type="text" id="txtCopago" style="width:20%"/>%       
                    </td>         
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">CUOTA MODERADA</td>
                    <td>
                      <input type="text" id="txtCuotaModeradora" style="width:25%"/>   
                    </td>         
                  </tr>               
                  <tr>
                    <td align="center" colspan="2">
                       <input id="btn_crea" title="BEU6" class="small button blue" type="button"  value="ADICION"  onclick="modificarCRUD('adicionarRangoPlan','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                               
                       <input id="btn_crea" title="BEE6" class="small button blue" type="button"  value="ELIMINA"  onclick="modificarCRUD('eliminaRangoPlan','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                               
                       <input id="btn_ver"  title="BY76" class="small button blue" type="button"  value="MODIFICA" onclick="modificarCRUD('modificaRangoPlan','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                             
                       <input id="btn_ver"  title="BY16" class="small button blue" type="button"  value="VER RANGOS"  onclick="buscarFacturacion('listGrillaRangos')">                    
                    </td>                                                
                  </tr>
                  <tr class="titulos">
                    <td colspan="2">
                      <div style="height: 200px">
                        <table id="listGrillaRangos" width="100%" class="scroll"></table>          
                      </div>       
                    </td>   
                  </tr>                   
                 </table> 
              </div>


              <div id="divLiquidacion">
                <table width="100%">       
                  <tr class="estiloImputIzq2">
                    <td width="24%">ID REGLA</td>
                    <td><label id="lblIdRegla"></label></td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">ORDEN PROCEDIMIENTO</td>
                      <td>
                        <select id="cmbOrdenProcedimiento" style="width:25%" tabindex="100" >
                          <option value=""></option>
                          <option value="1">ORDEN 1</option>
                          <option value="2">ORDEN 2</option>
                          <option value="3">ORDEN 3</option>
                        </select>     
                    </td>         
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">PORCENTAJE</td>
                    <td >
                      <input type="text" id="txtPorcentaje" style="width:20%"/>%       
                    </td>         
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">LATERALIDAD</td>
                    <td>
                      <select id="cmbLateralidad" style="width:25%" tabindex="100" >
                      <%     resultaux.clear();
                              resultaux=(ArrayList)beanAdmin.combo.cargar(574);  
                              ComboVO cmbBod; 
                              for(int k=0;k<resultaux.size();k++){ 
                                    cmbBod=(ComboVO)resultaux.get(k);
                      %>
                          <option value="<%= cmbBod.getId()%>" title="<%= cmbBod.getTitle()%>">
                            <%=cmbBod.getDescripcion()%></option>
                          <%} %>
                        </select>
                      </td>
                    </tr>

                    <tr class="estiloImputIzq2">
                      <td width="24%">VIGENTE</td>
                      <td>
                        <select id="cmbVigente" style="width:25%" tabindex="100">
                          <option value=""></option>
                          <option value="S">SI</option>
                          <option value="N">NO</option>
                        </select>
                      </td>
                    </tr>
                    <tr class="titulos">
                      <td width="24%">&nbsp;</td>
                    </tr>

                    <tr>
                      <td colspan="2" align="center">
                        <input id="btn_crea" title="BEU7" class="small button blue" type="button" value="ADICION"
                          onclick="modificarCRUD('adicionarLiquidacion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                        <input id="btn_crea" title="BEE7" class="small button blue" type="button" value="ELIMINA"
                          onclick="modificarCRUD('eliminarLiquidacion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                        <input id="btn_ver" title="BY77" class="small button blue" type="button" value="MODIFICA"
                          onclick="modificarCRUD('modificaLiquidacion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                        <input id="btn_ver" title="BY17" class="small button blue" type="button" value="VER"
                          onclick="buscarFacturacion('listGrillaContratos')">
                      </td>
                    </tr>
                    <tr class="titulos">
                      <td colspan="5">
                        <div style="height: 200px">
                          <table id="listGrillaContratos"></table>
                        </div>
                      </td>
                    </tr> 
                </table>
              </div>

              <div id="divLiquidacionProcedimientos">
                <table width="100%">
                  <tr class="estiloImputIzq2">
                    <td width="24%">ID</td>
                    <td><label id="lblId"></label></td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">PLAN</td>
                    <td><label id="lblPlan"></label></td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">ID PROCEDIMIENTO:</td>
                    <td><input type="text" style="width:70%"
                        onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimientoLiquida',300)"
                        id="txtIdProcedimientoLiquida" maxlength="20" autocomplete="off" class="ac_input"></td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">ORDEN PROCEDIMIENTO</td>
                    <td>
                      <select id="cmbOrdenProcedimientto" style="width:25%" tabindex="100">
                        <option value=""></option>
                        <option value="1">ORDEN 1</option>
                        <option value="2">ORDEN 2</option>
                      </select>
                    </td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">PORCENTAJE</td>
                    <td>
                      <input type="text" id="txtPorcen" style="width:20%" />%
                    </td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">LATERALIDAD</td>
                    <td>
                      <select id="cmbLate" style="width:25%" tabindex="100">
                        <option value="U">UNILATERAL</option>
                        <option value="B">BILATERAL</option>
                      </select>
                    </td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="24%">VIGENTE</td>
                    <td>
                      <select id="cmbVigentte" style="width:25%" tabindex="100">
                        <option value="S">SI</option>
                        <option value="N">NO</option>
                      </select>

                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="center">
                      <input id="btn_crea" title="BEU7" class="small button blue" type="button" value="ADICION"
                        onclick="modificarCRUD('adicionarProcedimientoLiquidacion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                      <input id="btn_crea" title="BEE7" class="small button blue" type="button" value="ELIMINA"
                        onclick="modificarCRUD('eliminarLiquidacionProcedimiento','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                      <input id="btn_ver" title="BY77" class="small button blue" type="button" value="MODIFICA"
                        onclick="modificarCRUD('modificaLiquidacionProcedimiento','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                      <input id="btn_ver" title="BY17" class="small button blue" type="button" value="VER"
                        onclick="buscarFacturacion('listGrillaProcedimientos')"> </td>
                  </tr>
                  <tr class="titulos">
                    <td colspan="5">
                      <div style="height: 200px">
                        <table id="listGrillaProcedimientos"></table>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>

              <div id="divMunicipiosPlan">
                <table width="100%">
                  <tr class="estiloImputIzq2">
                    <td width="24%">PLAN</td>
                    <td>
                      <label id="lblPlanM"></label>
                      <label id="lblMuni"></label>
                    </td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td>MUNICIPIO</td>
                    <td><input type="text" id="txtMunicipio" size="30" maxlength="60"
                        onkeypress="llamarAutocomIdDescripcionConDato('txtMunicipio', 766)" style="width:80%"
                        tabindex="205" />
                      <div id="divParaVentanita"></div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="center">
                      <input id="btn_crea" title="BEU78" class="small button blue" type="button" value="ADICION"
                        onclick="modificarCRUD('adicionarMunicipio','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                      <input id="btn_crea" title="BEE78" class="small button blue" type="button" value="ELIMINA"
                        onclick="modificarCRUD('eliminarMunicipio','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">

                      <input id="btn_modifica" title="BY17P5" class="small button blue" type="button"
                        value="MODIFICA"
                        onclick="modificarCRUD('modificarMunicipioPlan','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">

                      <input id="btn_ver" title="BY175" class="small button blue" type="button" value="VER"
                        onclick="buscarFacturacion('listGrillaMunicipios')"></td>
                  </tr>
                  <tr class="titulos">
                    <td colspan="5">
                      <div style="height: 200px">
                        <table id="listGrillaMunicipios"></table>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>

              <div id="divSedesPlan">
                <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
                  <tr>
                    <td>
                      <table width="100%">                 
                        <tr class="estiloImputIzq2">
                          <td width="30%">SEDE:</td><td width="70%"><input type="text" id="txtIdSede"  onkeypress="llamarAutocomIdDescripcionConDato('txtIdSede',898)" style="width:90%"/></td> 
                        </tr>	
                        <tr>
                           <td colspan="2" align="center">
                             <input id="btn_limpia" title="E55" class="small button blue" type="button" value="LIMPIAR"  onclick="limpiaAtributo('txtIdSede',0);">              
                             <input id="btn_crea" title="ASIG34" class="small button blue" type="button"  value="ASIGNAR SEDE"  onclick="modificarCRUD('asignarPlanSedes')">                               
                             <input name="btn_elimina" title="E57" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('eliminarPlanSedes')">                               
                           </td>
                        </tr>   
                       </table> 
                      </td>   
                  </tr>  
                  <tr class="titulos">
                    <td>
                        <div style="height: 200px">
                          <table id="listSedePlan"></table> 
                        </div> 
                    </td>
                  </tr> 
                </table> 
              </div>
            </div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
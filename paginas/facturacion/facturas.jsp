<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="1180px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="FACTURAS" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>

                        <div style="overflow:auto;height:500px; width:1180px" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <div id="divBuscar" style="display:block">
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                    <tr class="titulos" align="center">
                                        <td width="15%">ESTADO FACTURA</td>
                                        <td width="10%">FECHA CREA DESDE</td>
                                        <td width="10%">FECHA HASTA</td>
                                        <td width="10%">NUMERO FACTURA</td>
                                        <td width="15%">PLAN</td>
                                        <td width="30%">PACIENTE</td>
                                        <td width="10%">&nbsp;</td>
                                    </tr>



                                    <tr class="estiloImput">

                                        <td>
                                            <select size="1" id="cmbIdEstadoFactura" style="width:80%" tabindex="1">
                                                <option value=""></option>
                                                <% resultaux.clear();
                                                    resultaux = (ArrayList) beanAdmin.combo.cargar(558);
                                                    ComboVO cmbF;
                                                    for (int k = 0; k < resultaux.size(); k++) {
                                                        cmbF = (ComboVO) resultaux.get(k);
                                                %>
                                                <option value="<%= cmbF.getId()%>" title="<%= cmbF.getTitle()%>">
                                                    <%= cmbF.getDescripcion()%></option>
                                                <%}%>                                 
                                            </select>


                                        </td>

                                        <td><input type="text" id="txtFechaDesdeFactura" tabindex="2" style="width:80%" /></td>
                                        <td><input type="text" id="txtFechaHastaFactura" tabindex="3" style="width:80%" /></td>

                                        <td><input type="text" id="txtBusNumFactura" style="width:80%" tabindex="4" /></td>

                                        <td>

                                            <select size="1" id="cmbIdPlan" style="width:80%" tabindex="128" onfocus="comboDependienteEmpresa('cmbIdPlan', '78')">
                                                <option value=""></option>

                                            </select>  

                                        </td>

                                        <td><input type="text" size="70" maxlength="70" style="width:90%" id="txtIdBusPaciente" tabindex="2" />
                                            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenT" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '7')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                            <div id="divParaVentanita"></div>
                                        </td>
                                        <td>
                                            <input title="BF932" type="button" class="small button blue" value="BUSCAR" onclick="buscarFacturacion('listGrillaFacturas')" />
                                        </td>
                                    </tr>

                                    <tr class="titulos" align="center">
                                        <td colspan="4" >FACTURADORES</td>
                                        <td colspan="3">PROCEDIMIENTOS</td>
                                    </tr>    

                                    <tr class="estiloImput">

                                        <td colspan="4">

                                            <select size="1" id="cmbIdFacturador" style="width:80%" tabindex="128" onfocus="comboDependienteEmpresa('cmbIdFacturador', '82')">
                                                <option value=""></option>

                                            </select>


                                        </td>

                                        <td colspan="3">
                                            <input type="text" id="txtIdProcedimiento" onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimiento', 300)" style="width:90%"/>

                                        </td>

                                    </tr>



                                </table>
                                <table id="listGrillaFacturas" class="scroll"></table>
                            </div>
                        </div>
                        <!-- div contenido-->

                        <div id="divEditar" style="display:block; width:100%">

                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulosCentrados">
                                    <td colspan="3">INFORMACION FACTURA
                                    </td>
                                </tr>
                                <tr>
                                    <td>



                                        <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Id Factura:</td>
                                                            <td width="70%"><label id="lblIdFactura"></label></td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Id Admision:</td>
                                                            <td width="70%"><label id="lblIdAdmision"></label></td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Numero Factura:</td>
                                                            <td width="70%"><label id="lblNumeroFactura"></label></td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Estado Factura:</td>
                                                            <td width="70%"><label id="lblIdEstadoFactura"></label> - <label id="lblEstadoFactura"></label> </td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Total factura:</td>
                                                            <td width="70%"><label id="lblValorTotalFactura"></label></td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Paciente:</td>
                                                            <td width="70%"><label id="lblIdPaciente"></label>-<label id="lblIdentificacion"></label>-<label id="lblNomPaciente"></label></td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Usuario Elaboro:</td>
                                                            <td>
                                                                <label id="lblIdUsuario"></label>-<label id="lblNomUsuario"></label>
                                                            </td>
                                                        </tr>
                                                        <!--tr   class="estiloImputIzq2">                         
                                          <td>Fecha Admision Factura</td><td>              
                                                                    <input type="text"  id="txtFechaAdmision"  size="10"  maxlength="10"  style="width:10%"  tabindex="114" />
                                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                <input  title="BF77" type="button" value="MODIFICAR" onclick="modificarCRUD('modificarFechaAdmision')"  />                     
                                          </td>                                             
                                        </tr>
                                        <tr   class="estiloImputIzq2">                         
                                          <td>Profesional Admision</td><td>              
                                                <select size="1" id="cmbIdProfesionales" style="width:40%"  tabindex="128" >	                                        
                                                  <option value=""></option>
                                                        <% resultaux.clear();
                                                            resultaux = (ArrayList) beanAdmin.combo.cargar(109);
                                                            ComboVO cmbE;
                                                            for (int k = 0; k < resultaux.size(); k++) {
                                                                cmbE = (ComboVO) resultaux.get(k);
                                                        %>
                                                <option value="<%= cmbE.getId()%>" title="<%= cmbE.getTitle()%>">
                                                    <%= cmbE.getDescripcion()%></option>
                                                <%}%>                                 
                            </select>     
                             id Admision= <label id="lblIdAdmision"></label>
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            <input  title="BF77" type="button" value="MODIFICAR" onclick="modificarCRUD('modificarProfesiAdmision')"  />              
                      </td>                                             
        </tr-->
                                                        <% if (beanSession.usuario.preguntarMenu("10_1")) {%>
                                    <tr class="estiloImputIzq2">
                                        <td>Motivos para anular:</td>
                                        <td>
                                            <select size="1" id="cmbIdMotivoAnulacion" style="width:40%" tabindex="128">
                                                <option value=""></option>
                                                <% resultaux.clear();
                                                                        resultaux = (ArrayList) beanAdmin.combo.cargar(141);
                                                                        ComboVO cmb3;
                                                                        for (int k = 0; k < resultaux.size(); k++) {
                                                                            cmb3 = (ComboVO) resultaux.get(k);
                                                                    %>
                                                <option value="<%= cmb3.getId()%>" title="<%= cmb3.getTitle()%>">
                                                    <%= cmb3.getDescripcion()%></option>
                                                <%}%>                                 
                                                                </select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <input title="BF77" type="button" value="SOLO ANULAR FACTURA" onclick="verificarFacturaAnular('soloAnularFactura')"/>
                                                                <input title="BF78" type="button" value="ANULAR Y CLONAR FACTURA" onclick="verificarFacturaAnular('anularFactura')"/>
                                                                <input title="BF78" type="button" value="ANULAR FACTURA OPTICA" onclick="verificarFacturaAnular('anularFacturaVentas')"/>
                                                            </td>
                                                        </tr>

                                                        <tr class="estiloImputIzq2">
                                                            <td>Motivos para abrir:</td>
                                                            <td>
                                                                <select size="1" id="cmbIdMotivoAbrir" style="width:40%" tabindex="128">	                                        
                                                                    <option value=""></option>
                                                                    <% resultaux.clear();
                                                                        resultaux = (ArrayList) beanAdmin.combo.cargar(559);
                                                                        ComboVO cmb4;
                                                                        for (int k = 0; k < resultaux.size(); k++) {
                                                                            cmb4 = (ComboVO) resultaux.get(k);
                                                                    %>
                                                <option value="<%= cmb4.getId()%>" title="<%= cmb4.getTitle()%>">
                                                    <%= cmb4.getDescripcion()%></option>
                                                <%}%>                                 
                                                                </select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <input title="BF79" type="button" value="ABRIR FACTURA" onclick="modificarCRUD('abrirFactura')"/>

                                                                <input title="BF80" type="button" value="CERRAR FACTURA" onclick="modificarCRUD('cerrarFactura')"/>
                                                            </td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Auditar Facuras</td>
                                                            <td><input title="BF77" type="button" class="small button blue" value="AUDITAR SELECCIONADAS" onclick="guardarYtraerDatoAlListado('auditarFactura')" /></td>
                                                        </tr>
                                                        <%}%>
                                </table>
                    </td>
                </tr>
            </table>

            <table width="100%" style="cursor:pointer">
                <tr>
                    <td width="99%" class="titulos">
                        <input title="BF73" type="button" class="small button blue" value="IMPRIMIR"
                            onclick="formatoPDFFactura(valorAtributo('lblIdFactura'));" />
                        <input title="BF73" type="button" class="small button blue" value="IMPRIMIR VARIAS FACTURAS"
                            onclick="imprimirFacturasPDF()" />
                        <input title="BF73" id="btnTipificacion" type="button" class="small button blue"
                            value="TIPIFICAR FACTURAS"
                            onclick="tipificarFacturas(obtenerListaFacturasTabla('listGrillaFacturas'))" />
                        <div id="loaderTipificacion" hidden>
                            <img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="20px" height="20px">
                            Tipificando facturas <label id="progreso">0</label>%
                        </div>
                    </td>
                    <!--td width="99%" class="titulos">
                                                        <input title="BF73" type="button" class="small button blue" value="IMPRIMIR2"
                                                        onclick="prueba()"/>
                                                </td-->
                </tr>
            </table>

        </td>
    </tr>
</table>

</div>
<!-- divEditar-->
<div id="divEditar" style="display:block; width:100%">
    <table width="100%" cellpadding="0" cellspacing="0" align="center">
        <tr class="titulosCentrados">
            <td colspan="3">EDITAR FACTURA
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr class="titulosListaEspera">
                                    <td>Tipo documento</td>
                                    <td>Documento</td>
                                    <td>N&uacutemero de autorizaci&oacuten</td>
                                    <td>Dx ingreso</td>
                                    <td>Fecha de ingreso</td>
                                    <td>Profesional factura</td>
                                    <td>Fecha de Nacimiento</td>
                                    <td>Municipio</td>
                                </tr>
                                <tr class="estiloImputListaEspera">
                                    <td>
                                        <select id="cmbTipoId" class="w90" tabindex="100">
                                            <option value=""></option>
                                            <option value="CC">Cedula de Ciudadania</option>
                                            <option value="CE">Cedula de Extranjeria</option>
                                            <option value="MS">Menor sin Identificar</option>
                                            <option value="PA">Pasaporte</option>
                                            <option value="RC">Registro Civil</option>
                                            <option value="TI">Tarjeta de identidad</option>
                                            <option value="CD">Carnet Diplomatico</option>
                                            <option value="NV">Certificado nacido vivo</option>
                                            <option value="AS">Adulto sin Identificar</option>
                                        </select>
                                    </td>
                                    <td><input id="txtDocumento" type="text" tabindex="106" readonly />
                                    <td><input id="txtNoAutorizacion" type="text" tabindex="106" />
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="text" id="txtDxIngreso" tabindex="108" readonly />
                                                </td>
                                                <td>
                                                    <img width="18px" height="18px" align="middle" title="VEN52"
                                                        id="idLupitaVentanitaDx"
                                                        onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtDxIngreso', '5')"
                                                        src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                                    <div id="divParaVentanita"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <input type="text" id="txtFechaIngreso" />
                                    </td>
                                    <td>


                                        <select size="1" id="cmbIdProfesionalesFactura"
                                            onfocus="comboDependienteEmpresa('cmbIdProfesionalesFactura', '86')"
                                            style="width:100%">
                                            <option value=""></option>

                                        </select>
                                    </td>
                                    <!--onkeypress="return valida(event)" -->
                                    <input type="hidden" id="txtIdPaciente" />
                                    <td><input id="txtNacimiento" type="text" />
                                    </td>
                                    <td align="CENTER">
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="text" id="txtMunicipio" tabindex="108" read />
                                                </td>
                                                <td>
                                                    <img width="18px" height="18px" align="middle" title="VEN52"
                                                        id="idLupitaVentanitaMunicT"
                                                        onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipio', '1')"
                                                        src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                                    <div id="divParaVentanita"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>
                <table width="100%" style="cursor:pointer">
                    <tr>
                        <td width="99%" class="titulos">
                            <input title="BF77" type="button" class="small button blue" value="MODIFICAR"
                                onclick="modificarCRUD('modificarFactura')" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div id="divDetallesFactura" style="display:block; width:100%">
    <table width="100%" cellpadding="0" cellspacing="0" align="center">
        <tr class="titulosCentrados">
            <td colspan="3">PROCEDIMIENTOS DE FACTURA
            </td>
        </tr>
        <tr>
            <td>
                <table id="listProcedimientosDeFactura" style="width:100%" class="scroll"></table>
            </td>
        </tr>
    </table>
</div>
</td>
</tr>
<% if (beanSession.usuario.preguntarMenu("10_1")) {%>
<tr class="titulosCentrados">
    <td colspan="3">EDITAR FACTURA PGP
    </td>
</tr>
<tr>
    <td>
        <table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr class="titulosListaEspera">

                <td>Numeración Factura:</td>
                <td>Valor No Cubierto:</td>
                <td>Valor Descuento:</td>
                <td></td>

            </tr>
            <tr class="estiloImputListaEspera">
                <td>
                    <select id="cmbNumeracionFactura" style="width:60%">
                        <option value=""></option>
                        <% resultaux.clear();
                                resultaux = (ArrayList) beanAdmin.combo.cargar(89);
                                ComboVO cmbNF;
                                for (int k = 0; k < resultaux.size(); k++) {
                                    cmbNF = (ComboVO) resultaux.get(k);
                            %>
                        <option value="<%= cmbNF.getId()%>" title="<%= cmbNF.getTitle()%>">
                            <%= cmbNF.getDescripcion()%></option><%}%>                                 
                        </select>
                    </td>
                    <td><input id="txtValorNoCubierto" type="text" width="80%" /></td>
                    <td><input id="txtValorDescuento" type="text" width="80%" /></td>
                    <td>
                        <input type="button" class="small button blue" value="MODIFICAR PGP" onclick="modificarCRUD('modificarFacturaPgp')" />
                    </td>
                   <tr>

                </tr>
            </table>
        </td>
    </tr>
    <%}%>

        </table>
    </td>
</tr>
</table>

<input type="hidden" id="lblIdDoc" />
<input type="hidden" id="lblIdEstadoAuditoria" />
<input type="hidden" id="txtNoDevolucion" />

<div id="divVentanitaProcedimCuenta" style="display:none; z-index:2000; top:400px; left:50px; ">

    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2004; position:fixed; top:300px;left:400px; width:70%">
        <table width="70%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaProcedimCuenta')" /></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="5">&nbsp;</td>
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaProcedimCuenta')" /></td>
            </tr>
            <tr class="titulos">
                <td>TRAN.</td>
                <td>PROCEDIMIENTO</td>
                <td> NOMBRE PROCEDIMIENTO</td>
                <td>N&uacute;mero de autorizaci&oacute;n</td>
                <td>Profesional procedimiento</td>
                <td>Ejecutado</td>
            </tr>
            <tr class="estiloImput">
                <td><label id="lblIdTransac"></label></td>
                <td><label id="lblIdProced"></label></td>
                <td><label id="lblNomProced"></label></td>
                <td>
                    <input type="text" id="txtNoAutorizacionProcedimientoEditar" style="width:80%" tabindex="119" />
                </td>
                <td>
                    <select size="1" id="cmbIdProfesionalesFacturaEditar"
                        onfocus="comboDependienteEmpresa('cmbIdProfesionalesFacturaEditar', '86')" style="width:100%">
                        <option value=""></option>

                    </select>
                </td>

                <td>
                    <select id="cmbEjecutadoEditar" style="width:80%" tabindex="123">
                        <option value="N">NO</option>
                        <option value="S">SI</option>
                    </select>
                </td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td colspan="9">
                    <input id="BTN_MO" title="BI101" type="button" class="small button blue"
                        value="MODIFICAR PROCEDIMIENTO"
                        onclick="modificarCRUD('modificarProcedimientoCuenta', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                        tabindex="309" />
                    <input id="BTN_MO" title="BI102" type="button" class="small button blue"
                        value="ELIMINAR PROCEDIMIENTO"
                        onclick="modificarCRUD('eliminarProcedimientoCuenta', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                        tabindex="309" />
                </td>
            </tr>
        </table>
    </div>
</div>

<div id="divVentanitaAdjuntos" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:180px; width:70%">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
                <td align="right">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="divAcordionAdjuntos" style="display:BLOCK">
                        <div id="divParaArchivosAdjuntos"></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
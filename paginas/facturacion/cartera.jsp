<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center" border="0" cellspacing="0" cellpadding="1" class="fondoTabla">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="CARTERA" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td>

            <div>

              <div class="btitulos">
                <label class="bloque w15">Numero Factura</label>
                <label class="bloque w15">Tipo Id</label>
                <label class="bloque w20">Identificación</label>
                <label class="bloque w50">Paciente</label>
              </div>

              <div class="bestiloImput">

                <input type="text" id="txtNumeroFactura" class="style w15" />


                <div class="bloque w15">
                  <select id="cmbTipoId" class="w90" tabindex="100">
                    <option value="CC">Cedula de Ciudadania</option>
                    <option value="CE">Cedula de Extranjeria</option>
                    <option value="MS">Menor sin Identificar</option>
                    <option value="PA">Pasaporte</option>
                    <option value="RC">Registro Civil</option>
                    <option value="TI">Tarjeta de identidad</option>
                    <option value="CD">Carnet Diplomatico</option>
                    <option value="NV">Certificado nacido vivo</option>
                    <option value="AS">Adulto sin Identificar</option>
                  </select>
                </div>
                <div class="bloque w20">
                  <input type="text" id="txtIdentificacion" class="w80"
                    onfocus="javascript:return document.getElementById('txtApellido1').focus()" tabindex="101" />
                  <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaPacienT"
                    onclick="traerVentanita(this.id,'','divParaVentanita','txtIdBusPaciente','7')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                  <div id="divParaVentanita"></div>
                </div>

                <div class="bloque w50">
                  <input type="text" id="txtIdBusPaciente" oncontextmenu="return false" tabindex="99" class="w90"
                    onfocus="javascript:return document.getElementById('txtApellido1').focus()" />
                </div>
              </div>

              <div class="btitulos">
                <label class="bloque w15">Fecha Creación Desde</label>
                <label class="bloque w15">Fecha Creación Hasta</label>
                <label class="bloque w20">Tipo Factura</label>
                <label class="bloque w20">Tipo Pago</label>
              </div>


              <div class="bestiloImput">
                <div class="bloque w15"><input id="txtFechaDesdeFactura" type="text" class="w70" /></div>
                <div class="bloque w15"><input id="txtFechaHastaFactura" type="text" class="w70" /></div>

                <div class="bloque w20">
                  <select id="cmbTipoFactura" class="w90">
                    <option value="FO">OPTICA</option>
                    <option value="FS">SERVICIO</option>
                  </select>
                </div>

                <div class="bloque w20">
                  <select id="cmbTipoPago" class="w80" tabindex="114" title="64">
                    <% resultaux.clear();
         resultaux=(ArrayList)beanAdmin.combo.cargar(64);  
         ComboVO cmbP; 
         for(int k=0;k<resultaux.size();k++){ 
             cmbP=(ComboVO)resultaux.get(k);%>
                    <option value="<%= cmbP.getId()%>" title="<%= cmbP.getTitle()%>">
                      <%= cmbP.getDescripcion()%>
                    </option>
                    <%}%>
</select>
        </div>

    </div>




      <div class="bloque w100" style="text-align: center;">

        <input  title="BF77" type="button" class="small button blue" value="BUSCAR FACTURA" onclick="buscarFacturacion('listGrillaFacturasCartera')"  />  
      <input  title="BF73" type="button" class="small button blue" value="VER FACTURA" onclick="formatoPDFFactura(valorAtributo('lblIdFactura'));"  />  
        


      </div>

        <div style="height:200px">
        <table id="listGrillaFacturasCartera" class="scroll w100"></table> 
      </div>

      <div>
        <label class="bloque w100 titulosCentrados">RECIBOS</label>
      </div>


       <div style="height:200px">
        <table id="listGrillaRecibosCartera" class="scroll w100"></table> 
      </div>


      <div>
        <label class="bloque w100 titulosCentrados">INFORMACION RECIBO CREAR</label>
      </div>

      <div class="btitulos">
        <label class="bloque w10">Id Recibo</label>
        <label class="bloque w10">Id Factura</label>
        <label class="bloque w20">Total Factura</label>
        <label class="bloque w20">Saldo</label>
        <label class="bloque w20">Tipo Pago</label>
        <label class="bloque w20">Estado Factura</label>
      </div>

      <div class="bestiloImput">
                <label id="lblIdRecibo" class="bloque w10"> </label>
                <label id="lblIdFactura" class="bloque w10"> </label>
                <label id="lblTotalFactura" class="bloque w20"> </label>
                <label id="lblSaldoFactura" class="bloque w20"> </label>
                <div class="bloque w20">
                     <label id="lblIdTipoPago"> </label>-
                     <label id="lblTipoPago"> </label>
                </div>
                <div class="bloque w20">
                     <label id="lblIdEstadoFactura"> </label>-
                     <label id="lblEstadoFactura"> </label>
                </div>
      </div>

      <div class="btitulos">
        <label class="bloque w20">Valor</label>
        <label class="bloque w20">Concepto</label>
        <label class="bloque w20">Medio de pago</label>
        <label class="bloque w60">Observación</label>
      </div>


      <div class="bestiloImput">

          <input type="text"  id="txtValorUnidadRecibo" tabindex="114" class="bloque w20"/>


          
        <div class="bloque w20">

         <select size="1" id="cmbIdConceptoRecibo" tabindex="128" class="w80" title="530">                                          
            <option value=""></option>
              <% resultaux.clear();
                 resultaux=(ArrayList)beanAdmin.combo.cargar(530);  
                 ComboVO cmb33; 
                 for(int k=0;k<resultaux.size();k++){ 
                       cmb33=(ComboVO)resultaux.get(k);
              %>
                    <option value="<%= cmb33.getId()%>" title="<%= cmb33.getTitle()%>"><%= cmb33.getDescripcion()%>
                    </option>
                    <%}%>                                 
          </select> 

        </div>
        <div class="bloque w20">
          <select size="1" id="cmbIdMedioPago" class="w80" tabindex="128">	                                        
            <option value=""></option>
              <% resultaux.clear();
                 resultaux=(ArrayList)beanAdmin.combo.cargar(153);	
          
                 for(int k=0;k<resultaux.size();k++){ 
                       cmb33=(ComboVO)resultaux.get(k);
              %>
                    <option value="<%= cmb33.getId()%>" title="<%= cmb33.getTitle()%>"><%= cmb33.getDescripcion()%>
                    </option>
                    <%}%>                                 
          </select>
        </div>

          <input type="text" id="txtObservacionRecibo"  tabindex="2" class="bloque w60"/>  
      </div>

      <div class="bloque w100" style="text-align: center;">
        
        <input  title="BP77" type="button" class="small button blue" value="VER RECIBO" onclick="formatoPDFRecibo(valorAtributo('lblIdRecibo'))"  />  
        <input  title="BF38" type="button" class="small button blue" value="CREAR" onclick="modificarCRUD('crearReciboCartera')"  />   
        <input  title="BF38" type="button" class="small button blue" value="LIMPIAR" onclick="limpiarDivEditarJuan('limpiarRecibosCartera')"  />

      </div>

</div>

   </td>
 </tr> 
 
</table>


<input type="hidden" id="lblRowId" /> 
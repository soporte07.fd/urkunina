<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="DOCUMENTOS TIPIFICACION" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>
                        <div style="overflow:auto;height:600px; width:1050px" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <div id="divBuscar" style="display:block">
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                    <tr class="titulos" align="center">
                                        <td width="60%">NOMBRE</td>
                                        <td width="10%">POS</td>
                                        <td width="10%">APLICA IPS</td>
                                        <td width="10%">VIGENTE</td>
                                    </tr>
                                    <tr class="estiloImput">
                                        <td><input type="text" id="txtNombreDocumento" style="width:90%"/>
                                        </td>
                                        <td>
                                            <select id="cmbPOS" style="width:95%" tabindex="2">
                                                <option value="" selected>[ TODOS ]</option>
                                                <option value="POS">POS</option>
                                                <option value="NOPOS">NO POS</option>
                                                <option value="POS/NOPOS">POS / NO POS</option>
                                            </select>
                                        </td>
                                        <td><select size="1" id="cmbAplicaIPS" style="width:80%" tabindex="2">
                                                <option value="" selected>[ TODOS ]</option>
                                                <option value="SI">SI</option>
                                                <option value="NO">NO</option>
                                                <option value="OPCIONAL">OPCIONAL</option>
                                            </select>
                                        </td>
                                        <td><select size="1" id="cmbVigente" style="width:80%" tabindex="2">
                                                <option value="" selected>[ TODOS ]</option>
                                                <option value="S">SI</option>
                                                <option value="N">NO</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" class="small button blue" value="BUSCAR"
                                                onclick="buscarFacturacion('listGrillaDocumentosTipificacion')" />
                                        </td>
                                    </tr>
                                </table>
                                <table id="listGrillaDocumentosTipificacion" class="scroll"></table>
                            </div>
                        </div><!-- div contenido-->
                        <div id="divEditar" style="display:block; width:100%">

                        </div><!-- divEditar-->
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</table>
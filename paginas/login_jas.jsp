<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "Sgh.Utilidades.*" %>
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
    <meta http-equiv="expires" content="0">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Pragma" CONTENT="no-cache">
    <title>CRISTAL WEB</title>
    <LINK REL="SHORTCUT ICON" HREF="/clinica/utilidades/icono3.ico">

    <link href="/clinica/utilidades/css/estiloClinica.css" rel="stylesheet" type="text/css">
    <link href="/clinica/utilidades/css/extras.css" rel="stylesheet" type="text/css">
    <link href="reportes/estiloPcweb_imp.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/clinica/utilidades/css/menu.css" />
    <link rel="stylesheet" type="text/css" href="/clinica/utilidades/css/jquery.treeview.css" />
    <!--link href="/clinica/utilidades/javascript/themes/basic/grid.css" rel="stylesheet" type="text/css"-->
    <!--link href="/clinica/utilidades/javascript/themes/basic_off/grid.css" rel="stylesheet" type="text/css"-->
    <link href="/clinica/utilidades/css/custom-theme/jquery-ui-1.7.custom.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../utilidades/css/jquery.autocomplete.css" />

    <link href="/clinica/paginas/planAtencion/leaflet/leaflet.css" rel="stylesheet" type="text/css">


    <!--<link rel="stylesheet" type="text/css" href="../utilidades/bootstrap/css/bootstrap.css" />-->

    <link href="/clinica/paginas/hc/saludOral/diente/css/odontograma.css" rel="stylesheet" type="text/css" />
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="/clinica/utilidades/timepicker-1.3.5/jquery.timepicker.css" rel="stylesheet">
    <!--[if lt IE 7]>
<link href="/clinica/utilidades/css/ventan-modal-ie.css" rel="stylesheet" type="text/css">
<![endif]-->

    <!--SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.js"></SCRIPT-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js"></script>
    <script type="text/javascript" src="/clinica/utilidades/timepicker-1.3.5/jquery.timepicker.js"></script>
    <script type="text/javascript" src="/clinica/utilidades/javascript/alert/jquery.alerts.js"></script>

    <script language=javascript src="/clinica/utilidades/jqGrid-4.4.3/jquery.jqGrid.min.js"></script>
    <script language=javascript src="/clinica/utilidades/jqGrid-4.4.3/grid.locale-es.js"></script>
     <link rel="stylesheet" type="text/css" href="/clinica/utilidades/jqGrid/jquery-ui2.css">
       <link rel="stylesheet" type="text/css" href="/clinica/utilidades/jqGrid-3.8.2/css/ui.jqgrid.css">

    <SCRIPT language=javascript src="/clinica/utilidades/javascript/fisheye.js"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.corner.js"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.treeview.js"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/dropShadow.js"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.blockUI.js"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.ui.all.js"></SCRIPT>
    
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/funcionesBotones.js?<%beanSession.getCn().getVersion();%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/funciones.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/funcionesMenu.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesAdicionales.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesJuan.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesAgenda.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesFacturacion.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesEvento.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesHc.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <!--SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesPlatin.js?<%=beanSession.cn.getVersion()%>"></SCRIPT-->
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesReportes.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesSuministros.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>    
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/limpiarCampos.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesRecepcionTecnica.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesSinergiaCC.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/validarcampos.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/subirArchivos.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/js/coda.js"></SCRIPT>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.autocomplete.pack.js"></SCRIPT>
    <!--script language="JavaScript" type="text/javascript" src="/clinica/utilidades/javascript/formatearnum.js"></script-->
    <script language=javascript src="/clinica/utilidades/javascript/cronometro.js?<%=beanSession.cn.getVersion()%>"> </script>
    <script language=javascript src="hc/documentosHistoriaClinica/documentosHistoriaClinica.js?<%=beanSession.cn.getVersion()%>"> </script>
    <!--script language=javascript src="hc/documentosHistoriaClinica/documentosHistoriaClinicaDaniel.js"> </script-->
    <script language=javascript src="/clinica/paginas/referencia/funcionesReferencia.js?<%=beanSession.cn.getVersion()%>"> </script>
    <script language=javascript src="/clinica/paginas/hc/saludOral/odontologia.js?<%=beanSession.cn.getVersion()%>"> </script>
    <!--script language=javascript src="/clinica/utilidades/javascript/OTRAS/piepagi.js?<%=beanSession.cn.getVersion()%>"> </script-->
    <script language=javascript src="/clinica/paginas/parametros/parametros.js?<%=beanSession.cn.getVersion()%>"> </script>
    <script language=javascript src="/clinica/paginas/planAtencion/rutas.js?<%=beanSession.cn.getVersion()%>"> </script>
   
    <script language=javascript src="/clinica/paginas/planAtencion/leaflet/leaflet-src.esm.js?<%=beanSession.cn.getVersion()%>"> </script>
    <script language=javascript src="/clinica/paginas/planAtencion/leaflet/leaflet-src.js?<%=beanSession.cn.getVersion()%>"> </script>
    <script language=javascript src="/clinica/paginas/hc/manejoAdministrativo/encuestas/plantillaEncuesta.js?<%=beanSession.cn.getVersion()%>"> </script>    
    <script language=javascript src="/clinica/paginas/encuesta/encuesta.js?<%=beanSession.cn.getVersion()%>"> </script>    
    <script language=javascript src="/clinica/paginas/encuesta/encuestaValidaciones.js?<%=beanSession.cn.getVersion()%>"> </script>
    <SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesPaciente.js?<%=beanSession.cn.getVersion()%>"></SCRIPT>
    
    
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script language="javascript">
        //onLoad="if ('Navigator' == navigator.appName)


//document.forms[0].reset();//limpiar cache

var varajax;

function ingresar() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajax.open("POST", '/clinica/paginas/principal.jsp?', true);
    varajax.onreadystatechange = llenarcontenedoIngresar;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax.send(valores_a_mandar);
}

function llenarcontenedoIngresar() {
    if (varajax.readyState == 4){
        if (varajax.status == 200){
               document.getElementById("inicial").innerHTML = varajax.responseText;          
                /*  $("#divListado table").remove();
                    contenedorDiv = document.getElementById('divListado');
                    tabla = document.createElement('table');  // se crean los div hijos
                    tabla.id = 'listGrillaPacientes'
                    tabla.className = 'scroll';
                    contenedorDiv.appendChild(tabla);       
                */         
                /*
                alert('1111:'+screen.availWidth)                
                window.resizeTo(screen.availWidth, screen.availHeight); window.moveTo(0,0); 
                alert('2222')

                alert(111)
                document.getElementById('inicial').innerHTML = varajax.responseText;
                alert(222)

                $("#inicial table").remove();
                alert(333)*/

                //document.getElementById('inicial').innerHTML = varajax.responseText;

               // window.resizeTo(120,90);
              //  window.moveTo(50,50) 
                //iniciarObjetosMenu();

                iniciarDiccionarioConNotificaciones(1);
                    $('#dock').Fisheye(
                    {
                        maxWidth: 10,
                        items: 'a',
                        itemsText: 'span',
                        container: '.dock-container',
                        itemWidth: 20,
                        proximity: 20,
                        halign : 'left'
                    }
                );
                
                $("#msgBienvenido").dropShadow({left: 2, top: 2, blur: 1, opacity: 1,color:"WHITE",swap:false});
                $('#divMenuTreeTitle').corner("tear 10px").parent().css('padding', '4px').corner("fray 5px");        
                $("#browser").treeview({
                    toggle: function() {
                        console.log("%s was toggled.", $(this).find(">span").text());
                    }
                });
                //funciones para ocultar y mostrar el menu
                var puedeDesaparecer=false
                $('#disparaMenu').mousemove(function(event) {  setTimeout("",50);
                    if (puedeDesaparecer==false){ 
                        $("#menuDesplegable").animate( { width:"180px" }, { queue:false, duration:30 } );
                        $('#divMenuTreeTitle').show();
                    }
                    puedeDesaparecer=true;
                });
                /*$('#menuDesplegable').mouseover(function(event) {
                $("#menuDesplegable").animate( { width:"0px" }, { queue:false, duration:30 } );
                });*/
                $('#main').mousemove(function(event) {
                    if (puedeDesaparecer==true){ 
                    $("#menuDesplegable").animate( { width:"0px" }, { queue:false, duration:30 } );
                    $('#divMenuTreeTitle').hide();
                    }
                    puedeDesaparecer=false;   
                });
                $('#menuDesplegable').mousemove(function(event) {
                //$("#menuDesplegable").animate( { width:"180px" }, { queue:false, duration:30 } );
                puedeDesaparecer=false
                });
        
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajax.status);
        }
    }
    if (varajax.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}



    function llenarinfo(){
            if(varajax.readyState == 4 ){
                if(varajax.status == 200){


                    }else{
                        alert("Problemas de conexion con servidor: "+varajax.status);
                }
            } 
            if(varajax.readyState == 1){
                document.getElementById('inicial').innerHTML =	crearMensaje();
            }
        }    
    


    function validar(){
        valores_a_mandar="login="+document.getElementById('txtLogin').value+"&password="+document.getElementById('txtPassword').value;
		varajax=crearAjax();
        varajax.open("POST","<%= response.encodeURL("ingresar_xml.jsp")%>",true);
		varajax.onreadystatechange=validarIngreso;
		varajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
		varajax.send(valores_a_mandar); 
    }	
	
    function validarIngreso(){
        if(varajax.readyState == 4 ){
			if(varajax.status == 200){
			  raiz=varajax.responseXML.documentElement;
			  if(raiz.getElementsByTagName('respuesta')[0].firstChild.data=='true'){
			     ingresar();
			  }else{
			     alert("Usuario, contrase�a incorrecta o se encuentra inactivo. \n Favor informar al administrador del sistema.");
			  }
			 }else{
					alert("Problemas de conexion con servidor: "+varajax.status);
			  }
		} 
		if(varajax.readyState == 1){
		    // document.getElementById('inicial').innerHTML =	crearMensaje();
		}
    } 
  
  
   function validar2(){
        valores_a_mandar="login="+document.getElementById('txtLogin').value+"&password="+document.getElementById('txtPassword').value;
		varajax=crearAjax();
        varajax.open("POST","<%= response.encodeURL("ingresar_xml.jsp")%>",true);
		varajax.onreadystatechange=validarIngreso2;
		varajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
		varajax.send(valores_a_mandar); 
      }	
	
  function validarIngreso2(){
     if(varajax.readyState == 4 ){
			if(varajax.status == 200){
			  raiz=varajax.responseXML.documentElement;
			  if(raiz.getElementsByTagName('respuesta')[0].firstChild.data=='true'){
			     ingresar();
			  }else{
			   //  alert("Usuario, contrase�a incorrecta o se encuentra inactivo. \n Favor informar al administrador del sistema.");
			  }
			 }else{
					alert("Problemas de conexion con servidor: "+varajax.status);
			  }
		 } 
		if(varajax.readyState == 1){
		    // document.getElementById('inicial').innerHTML =	crearMensaje();
		}
  }   
  
  
  
  
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
  
//-->
</script>
<style type="text/css">
</style>
</head>

<body onLoad="if ('Navigator' == navigator.appName)
    document.forms[0].reset();">

    <iframe name="iframeUpload" id="iframeUpload" style="border:none; display:none "></iframe>
        <div id="divParaIniciar">
            
        </div>    
    <!--para el calendario-->
    <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
    <iframe name="iframeUpload" id="iframeUpload" style="border:none; display:none "></iframe>
        <div id="divGraficoEstadisticoGral" align="center" title="MENSAJE" style="z-index:100; top:500px; left:200px; position:absolute; display:none">
            <!-- ver renglon 245-->
            <table width="200px" height="200px" border="1" id="tablaGraficoEstadisticoGral">
                <tr>
                    <td>
                        <table width="100%" height="100%" border="1">
                            <tr>
                                <td>ddd</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
</body>

</html>
<script>

function contenedorDivIniciar() { 
    varajax = crearAjax();
    valores_a_mandar = "";
    varajax.open("POST", '/clinica/paginas/contenidoBuscarMenu.jsp?', true);
    varajax.onreadystatechange = llenarcontenedorDivIniciar; 
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax.send(valores_a_mandar);
}

function llenarcontenedorDivIniciar() {  
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {  

            $("#divParaIniciar table").remove();
            contenedorDiv = document.getElementById('divParaIniciar');
            tabla = document.createElement('table');  // se crean los div hijos
            tabla.id = 'idTableMen'            
            contenedorDiv.appendChild(tabla);

            document.getElementById("divParaIniciar").innerHTML = varajax.responseText;
           
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajax.status);
        }
    }
    if (varajax.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}
alert(1)
contenedorDivIniciar()


document.getElementById('txtLogin').focus();
cargarFocos();
</script>
<script type="text/javascript">
var banderadrag = true;

function mover(idReg) { //funcion que permite dar movimineto a los div que tengan como id=drag y como div interno con id=tituloForma
    $('#drag' + idReg).draggable({
        zIndex: 1000,
        ghosting: true,
        opacity: 0.7,
        containment: 'parent',
        handle: '#tituloForma'

    });
}
//validar();

 //setTimeout("cargarMenu('<%= response.encodeURL("planAtencion/geoAreas.jsp")%>', '6-1-3', 'geoAreas', 'geoAreas', 's', 's', 's', 's', 's', 's')", 550);    

//	   setTimeout("cargarMenu('<%= response.encodeURL("calidad/eventoAdverso/reportarEventoAdverso.jsp")%>','4-1-2','reportarEventoAdverso','reportarEventoAdverso','s','s','s','s','s','s')", 550);
//   setTimeout("cargarMenu('<%= response.encodeURL("calidad/eventoAdverso/administrarEventoAdverso.jsp")%>','4-1-2','administrarEventoAdverso','administrarEventoAdverso','s','s','s','s','s','s')", 550);	   

//  setTimeout("cargarMenu('<%= response.encodeURL("reportes/reportes_parametros.jsp")%>','4-1-2','reportes','reportes','s','s','s','s','s','s')", 550);

//	   setTimeout("cargarMenu('<%= response.encodeURL("atusuario/cirugia/listaEspera.jsp")%>','4-1-2','listaEsperaCirugia','listaEsperaCirugia','s','s','s','s','s','s')", 550);
//	   setTimeout("cargarMenu('<%= response.encodeURL("atusuario/cirugia/horarios.jsp")%>','4-1-2','horarios','horarios','s','s','s','s','s','s')", 550);	   
//  setTimeout("cargarMenu('<%= response.encodeURL("atusuario/cirugia/agenda.jsp")%>','4-1-3','agenda','agenda','s','s','s','s','s','s')", 550);	   	   

//	   setTimeout("cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>','4-1-2','listaEspera','listaEspera','s','s','s','s','s','s')", 550);
//	   setTimeout("cargarMenu('<%= response.encodeURL("atusuario/citas/horarios.jsp")%>','4-1-2','horarios','horarios','s','s','s','s','s','s')", 550);	   
	   //setTimeout("cargarMenu('<%= response.encodeURL("atusuario/citas/agenda.jsp")%>','4-1-3','agenda','agenda','s','s','s','s','s','s')", 550);	   	   
//	   setTimeout("cargarMenu('<%= response.encodeURL("archivo/agenda.jsp")%>','4-1-3','agenda','agenda','s','s','s','s','s','s')", 550);	   	   
//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/asistenciaPacientes.jsp")%>','4-1-3','asistenciaPacientes','asistenciaPacientes','s','s','s','s','s','s')", 550);	   	   	   
//setTimeout("cargarMenu('<%= response.encodeURL("reportes/reportes_parametros.jsp")%>', '20-1-1', 'reportes', 'reportes', 's', 's', 's', 's', 's', 's')",550);

//*******     setTimeout("cargarMenu('<%= response.encodeURL("facturacion/admisiones/admision.jsp")%>','4-1-3','admision','admision','s','s','s','s','s','s')", 550);	   	   	   
/*setTimeout("cargarMenu('<%= response.encodeURL("facturacion/admisiones/admisiones.jsp")%>','4-1-3','admision','admision','s','s','s','s','s','s')", 550);	 */

// setTimeout("cargarMenu('<%= response.encodeURL("facturacion/facturasTrabajo.jsp")%>', '6-1-3', 'facturasTrabajo', 'facturasTrabajo', 's', 's', 's', 's', 's', 's')", 550);	 



//        	   	   	   
//setTimeout("cargarMenu('<%= response.encodeURL("facturacion/facturas.jsp")%>','4-1-3','Facturas','Facturas','s','s','s','s','s','s')", 550);
//setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/principalHC.jsp")%>','4-0-1','principalOrdenes','principalOrdenes','s','s','s','s','s','s')", 550);		  
//     setTimeout("cargarMenu('<%= response.encodeURL("referencia/referencia.jsp")%>','4-0-1','referencia','referencia','s','s','s','s','s','s')", 550);		  
//     setTimeout("cargarMenu('<%= response.encodeURL("hc/postConsulta/documentos.jsp")%>','4-0-1','documentos','documentos','s','s','s','s','s','s')", 550);		  

//       setTimeout("cargarMenu('<%= response.encodeURL("calidad/evento/gestionarEventos.jsp")%>','4-1-3','gestionarEvento','gestionarEvento','s','s','s','s','s','s')", 550);	   	   

//       setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/enfermeria/transaccionesLaboratorios/principalTransacciones.jsp")%>','4-0-1','principalOrdenes','principalOrdenes','s','s','s','s','s','s')", 550);		  

//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/enfermeriaDocumentos/principalEnfermeria.jsp")%>','4-0-1','principalOrdenes','principalOrdenes','s','s','s','s','s','s')", 550);		  	 
//     setTimeout("cargarMenu('<%= response.encodeURL("hc/ordenes/principalAuditoria.jsp")%>','4-0-1','ordenes','ordenes','s','s','s','s','s','s')", 550);
//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/suministros/farmacia/principalDispensado.jsp")%>','4-0-1','ordenDispensado','ordenDispensado','s','s','s','s','s','s')", 550);
//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/enfermeria/principalAdministracion.jsp")%>','4-0-1','principalAdministracion','principalAdministracion','s','s','s','s','s','s')", 550);
//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/suministros/bodega/principalBodega.jsp")%>','4-0-1','principalBodega','principalBodega','s','s','s','s','s','s')", 550); 
//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/suministros/articulo/principalArticulo.jsp")%>','4-0-1','principalBodega','principalBodega','s','s','s','s','s','s')", 550); 
//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/suministros/articulo/recepcionTecnica.jsp")%>','4-0-1','recepcionTecnica','recepcionTecnica','s','s','s','s','s','s')", 550); 

//    setTimeout("cargarMenu('<%= response.encodeURL("suministros/transacciones/entradasDeBodega.jsp")%>','4-0-1','entradasDeBodega','entradasDeBodega','s','s','s','s','s','s')", 550); 
//    setTimeout("cargarMenu('<%= response.encodeURL("suministros/transacciones/salidasDeBodega.jsp")%>','4-0-1','salidasDeBodega','salidasDeBodega','s','s','s','s','s','s')", 550); 
//    setTimeout("cargarMenu('<%= response.encodeURL("suministros/transacciones/despachoProgramacionProcedimientos.jsp")%>','4-0-1','despachoProgramacionProcedimientos','despachoProgramacionProcedimientos','s','s','s','s','s','s')", 550); 	
//    setTimeout("cargarMenu('<%= response.encodeURL("suministros/transacciones/solicitudes.jsp")%>','4-0-1','solicitudes','solicitudes','s','s','s','s','s','s')", 550); 	
//    setTimeout("cargarMenu('<%= response.encodeURL("suministros/transacciones/devoluciones.jsp")%>','4-0-1','devoluciones','devoluciones','s','s','s','s','s','s')", 550); 	
//    setTimeout("cargarMenu('<%= response.encodeURL("suministros/transacciones/devolucionCompras.jsp")%>','4-0-1','devolucionCompras','devolucionCompras','s','s','s','s','s','s')", 550); 	
//      setTimeout("cargarMenu('<%= response.encodeURL("suministros/transacciones/trasladoBodegas.jsp")%>','4-0-1','trasladoBodegas','trasladoBodegas','s','s','s','s','s','s')", 550); 

//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/suministros/devoluciones/principalDevoluciones.jsp")%>','4-0-1','principalDevoluciones','principalDevoluciones','s','s','s','s','s','s')", 550);

//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/suministros/stock/principalStock.jsp")%>','4-0-1','principalBodega','principalBodega','s','s','s','s','s','s')", 550); 		

//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/facturacion/tarifarios.jsp")%>','4-0-1','tarifarios','tarifarios','s','s','s','s','s','s')", 550); 		
//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/facturacion/planes.jsp")%>','4-0-1','planes','planes','s','s','s','s','s','s')", 550); 			 
//     setTimeout("cargarMenu('<%= response.encodeURL("suministros/transacciones/despachoProgramacionProcedimientos.jsp")%>','4-0-1','despachoProgramacionProcedimientos','despachoProgramacionProcedimientos','s','s','s','s','s','s')", 550); 			 

//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionAyudaDiagnostica.jsp")%>','4-0-1','procedimientos','procedimientos','s','s','s','s','s','s')", 550);		  
//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionSolicitudes.jsp")%>','4-0-1','procedimientos','procedimientos','s','s','s','s','s','s')", 550);		  

//     setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/hospitalizacion/ubicacionCama.jsp")%>','4-0-1','ubicacionCama','ubicacionCama','s','s','s','s','s','s')", 550);
</script>

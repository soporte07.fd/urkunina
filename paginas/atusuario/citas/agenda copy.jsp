<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->
<%  beanAdmin.setCn(beanSession.getCn());
  ArrayList resultaux=new ArrayList();
%>
<table width="1280px" height="1000px" align="center">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="ASIGNACION DE CITAS" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO  aqui empieza el cambio-->
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table width="100%" height="1000px" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td valign="top">
                        <div style="overflow:auto; height:1100px; width:100%" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <table width="100%">
                                <tr>
                                    <td valign="top" width="90%">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="1">
                                            <tr class="titulos">
                                                <td width="30%">Sede</td>
                                                <td width="30%">Especialidad</td>
                                                <td width="40%">Profesional</td>
                                            </tr>

                                            <tr class="estiloImput">
                                                <td>

                                                    <select size="1" id="cmbSede" style="width:90%" title="GD38"
                                                        onfocus="comboDependienteSede('cmbSede','557')"
                                                        onchange="asignaAtributo('cmbIdEspecialidad', '', 0);asignaAtributo('cmbIdProfesionales', '', 0);cargarTipoCitaSegunEspecialidad('cmbTipoCita', 'cmbIdEspecialidad')">


                                                    </select>
                                                </td>
                                                <td><select id="cmbIdEspecialidad"
                                                        onfocus="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')"
                                                        style="width:90%" tabindex="14"
                                                        onchange="buscarAGENDA('listDias', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                                                        <option value=""></option>


                                                    </select>
                                                </td>
                                                <!--<td>
                                                  <select size="1" id="cmbIdSubEspecialidad"  onfocus="cargarSubEspecialidadDesdeEspecialidad('cmbIdEspecialidad', 'cmbIdSubEspecialidad')" onchange="buscarAGENDA('listDias', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  style="width:70%"  tabindex="117"  >                                        
                                                      <option value=""></option>
                                                  </select>                          
                                              </td> -->
                                                <td><select size="1" id="cmbIdProfesionales" style="width:90%"
                                                        onfocus="cargarProfesionalesDesdeEspecialidadSede(this.id)"
                                                        tabindex="14" onchange="buscarAGENDA('listDias')">
                                                        <option value=""></option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="estiloImput">
                                                <td></td>
                                                <td>
                                                    <img width="15" height="15" style="cursor:pointer"
                                                        src="/clinica/utilidades/imagenes/acciones/izquierda.png"
                                                        onclick="irMesAnteriorAgenda()">&nbsp;
                                                    <label id="lblMes"></label>/<label id="lblAnio"></label>
                                                    <img width="15" height="15" style="cursor:pointer"
                                                        src="/clinica/utilidades/imagenes/acciones/derecha.png"
                                                        onclick="irMesSiguienteAgenda()">&nbsp;
                                                </td>
                                                <td>
                                                    <TABLE>
                                                        <TR>
                                                            <TD width="50%">
                                                                <H3 align="center"><label
                                                                        id="lblFechaSeleccionada"></label></H3>
                                                            </TD>
                                                            <TD width="50%" align="right">
                                                                Exito
                                                                <select size="1" id="cmbExito" style="width:50%;"
                                                                    onchange="buscarAGENDA('listDias', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                                                                    tabindex="14">
                                                                    <option value="S">Si</option>
                                                                    <option value="N">No</option>
                                                                </select>
                                                            </TD>
                                                        </TR>
                                                    </TABLE>
                                                </td>
                                            </tr>
                                            <tr class="titulos">
                                                <td colspan="4">
                                                    <div id="divListAgenda">
                                                        <table id="listAgenda"></table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="12%" valign="top">
                                        <table>
                                            <tr class="estiloImput">
                                                <td>
                                                    <input name="btn_cerrar" class="small button blue" type="button"
                                                        align="right" value="REFRESCAR"
                                                        onclick="buscarAGENDA('listDias', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                                </td>
                                                <td>
                                                    Copiar citas ? <input type="checkbox" id="copiarCitas"
                                                        onchange="reaccionAEvento(this.id)">
                                                </td>
                                            </tr>
                                            <tr id="divOpcionesCopiado" hidden>
                                                <td>
                                                    <hr>
                                                </td>
                                                <td>
                                                    <select id="cmbTipoCopiado" style="width: 90%;"
                                                        onchange="reaccionAEvento(this.id)">
                                                        <option value="1">Seleccion manual</option>
                                                        <option value="2">Frecuencia</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <hr>
                                                </td>
                                            </tr>
                                            <tr class="estiloImput" id="divOpcionesFrecuencia" hidden>
                                                <td>
                                                    <hr>
                                                </td>
                                                <td>
                                                    Cantidad <input type="number" style="width: 10%;"
                                                        id="txtCantidadCitas"> Cada <input type="number"
                                                        style="width: 10%;" id="txtFrecuenciaCitas"> Dia(s)
                                                </td>
                                                <td>
                                                    <hr>
                                                </td>
                                            </tr>
                                            <tr id="divOpcionesCopiado" hidden>
                                                <td></td>
                                                <td>
                                                    <input name="btn_cerrar" class="small button blue" type="button"
                                                        value="Copiar Citas" onclick="copiarCitas()" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="divListadoDias" style="width:100%">
                                                        <table id="listDias" class="scroll"></table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <input type="hidden" id="txtIdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>" />
        </td>
    </tr>
</table>

<div id="divVentanitaAgenda"
    style="position:absolute; display:none; top:200px; left: -5px;  width:90%; height:90px; z-index:100">
    <table width="90%" class="fondoTabla">
        <tr class="estiloImput">
            <td align="left"><input name="btn_cerrar" title="divVentanitaAgenda" type="button" align="right"
                    value="CERRAR" onclick="ocultar('divVentanitaAgenda')" /></td>
            <td align="CENTER" colspan="3"><label id="lblUsuariosDato"></label></td>
            <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                    onclick="ocultar('divVentanitaAgenda')" /></td>
        <tr>
        <tr class="titulosListaEspera">
            <td width="20%">ID CITA.</td>
            <td width="20%">HORA</td>
            <td width="20%">DURACION MINUTOS</td>
            <td width="20%">FECHA</td>
            <td width="20%">CONSULTORIO</td>
        <tr>
        <tr class="estiloImput">
            <td><label id="lblIdAgendaDetalle"></label></td>
            <td><label id="lblHora"></label></td>
            <td><label id="lblDuracion"></label></td>
            <td><label id="lblFechaCita"></label></td>
            <td><label id="lblConsultorio"></label></td>
        <tr>
        <tr class="titulosCentrados" >
            <td width="100%" colspan="5">Informaci&oacute;n del paciente</td>
        <tr>
        <tr class="titulosListaEspera">
            <td>
                <input id="idTraerListaEspera" type="button" class="small button blue" value="Lista Espera"
                    title="btn_96u"
                    onclick="limpiarListadosTotales('listListaEsperaTraer');  abrirVentanaDesdeListaEspera();" />
            </td>
            <td colspan="4">PACIENTE</td>
        <tr>
        <tr class="estiloImput">
            <td>
                Id LE: <label id="lblIdListaEspera"></label>
            </td>
            <td colspan="4">
                <input type="text" size="110" maxlength="210" id="txtIdBusPaciente"
                    onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPaciente', 307)" style="width:50%"
                    tabindex="99" />
                <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaMuni"
                    onclick="traerVentanitaPaciente(this.id,'','divParaVentanita','txtIdBusPaciente','27')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                <input id="btn_busPacienteCita___" type="button" title="55GH_" class="small button blue" value="BUSCAR"
                    onclick="buscarInformacionBasicaPaciente()" />
            </td>
        </tr>
        <tr class="estiloImputListaEspera">
            <td colspan="5">
                <!-- DATOS BASICOS PACIENTE-->
                <table width="100%">
                    <tr class="titulosListaEspera">
                        <td width="15%">Tipo Id</td>
                        <td width="10%">Identificacion</td>
                        <td width="25%">Apellidos</td>
                        <td width="25%">Nombres</td>
                        <td width="15%">Fecha Nacimiento</td>
                        <td width="10%">Sexo</td>
                    </tr>
                    <tr>
                        <td bgcolor="#FD93B8">
                            <img src="/clinica/utilidades/imagenes/acciones/buscar.png" width="16px" height="16px"
                                onclick="guardarYtraerDatoAlListado('buscarIdentificacionPaciente'); limpTablas('listDocumentosHistoricos');ocultar('divArchivosAdjuntos')"
                                align="center" title="Boton buscari" />
                            <select size="1" id="cmbTipoId" style="width:85%" tabindex="100">
                                <option value=""></option>
                                <%     resultaux.clear();
                                     resultaux=(ArrayList)beanAdmin.combo.cargar(105);  
                                     ComboVO cmb105;
                                     for(int k=0;k<resultaux.size();k++){
                                           cmb105=(ComboVO)resultaux.get(k);
                              %>
                                <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                                    <%= cmb105.getId()+"  "+cmb105.getDescripcion()%></option>
                                <%}%>                     
                          </select></td>
                          <!--onkeypress="return valida(event)"  onkeypress="return validaEspacios();"  -->
                      <td  bgcolor="#FD93B8"><input  type="text" id="txtIdentificacion"  style="width:80%"   size="20" maxlength="20"  onKeyPress="javascript:checkKey2(event);" tabindex="101"  />
                      </td>
                      <td>
                          <input type="text" id="txtApellido1" size="30" maxlength="30" onkeypress="return excluirEspeciales(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" tabindex="102" style="width:45%" />
                          <input type="text" id="txtApellido2" size="30" maxlength="30" onkeypress="return excluirEspeciales(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" tabindex="103" style="width:45%" />
                      </td>
                      <td>
                          <input type="text" id="txtNombre1" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" tabindex="104" style="width:45%" />
                          <input type="text" id="txtNombre2" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" tabindex="105" style="width:45%" />
                      </td> 
                      <td><input id="txtFechaNac"  type="text"  size="10"  maxlength="10" tabindex="106" style="width:95%"/> </td>                        
                      <td> <select size="1" id="cmbSexo" style="width:95%" tabindex="107" >                                         
                              <option value=""></option>
                              <%  resultaux.clear();
                                  resultaux=(ArrayList)beanAdmin.combo.cargar(110); 
                                  ComboVO cmbS;
                                  for(int k=0;k<resultaux.size();k++){
                                    cmbS=(ComboVO)resultaux.get(k);
                              %>
                                <option value="<%= cmbS.getId()%>" title="<%= cmbS.getTitle()%>">
                                    <%=cmbS.getDescripcion()%></option>
                                <%}%>                     
                          </select>
                      </td>
                  </tr>
              </table>     
              <!-- FIN DATOS BASICOS -->      
          </td>
        </tr>
        <tr>
            <td colspan="5">
                <table>
                    <tr class="titulosListaEspera" >
                        <td width="20%">Municipio</td>
                        <td width="30%">Direcci&oacute;n</td> 
                        <td width="20%">Acompa&ntilde;ante</td>                  
                        <td width="10%">Tel&eacute;fono</td>
                        <td width="10%">Celular 1</td>                    
                        <td width="10%">Celular 2</td>                 
                    </tr>
                    <tr class="estiloImput">
                        <td><input type="text" id="txtMunicipio" size="60"  maxlength="60"  onfocus="llamarAutocomIdDescripcionConDato('txtMunicipio', 310)" style="width:80%"  tabindex="108"  />
                            <img width="18px" height="18px" align="middle" title="VEN1" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipio', '1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">             
                            <div id="divParaVentanita"></div>                  
                        </td>
                        <td><input type="text" id="txtDireccionRes" size="100" maxlength="100"  style="width:99%" onkeyup="javascript: this.value= this.value.toUpperCase();"  onkeypress="javascript:return teclearsoloan(event);" tabindex="109" /></td> 
                        <td><input type="text" id="txtNomAcompanante"  size="100" maxlength="100"  style="width:95%" onkeyup="javascript: this.value= this.value.toUpperCase();" onkeypress="javascript:return teclearExcluirCaracter(event);"  tabindex="110" /> </td>                  
                        <td>
                            <input type="text" id="txtTelefonos" size="100"  style="width:90%"minlength="7" maxlength="7" onkeypress="javascript:return soloTelefono(event);" tabindex="112"/>                  
                        </td>  
                        <td>
                            <input type="text" id="txtCelular1" size="30"  style="width:90%" minlength="10" maxlength="10" onkeypress="javascript:return soloTelefono(event);" tabindex="111"/>
                        </td>
                        <td>
                            <input type="text" id="txtCelular2" size="30"  style="width:90%" minlength="10" maxlength="10" onkeypress="javascript:return soloTelefono(event);" tabindex="113"/>
                        </td>                                   
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <table>
                    <tr class="titulosListaEspera" >
                        <td>Email</td>
                        <td>Administradora Paciente</td>             
                    </tr>         
                    <tr class="estiloImputListaEspera" >
                        <td><input type="text" id="txtEmail" size="60"  maxlength="60"  style="width:99%" onkeypress="javascript:return email(event);"  tabindex="108"/> </td>
                        <td>
                            <input type="text"  id="txtAdministradoraPaciente"  size="60"  maxlength="60"   onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradoraPaciente', 308)" style="width:90%"  tabindex="114" /> 
                            <img width="18px" height="18px" align="middle" title="VEN2" id="idLupitaVentanitaAdPaciente" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtAdministradoraPaciente', '21')" src="/clinica/utilidades/imagenes/acciones/buscar.png">    
                        </td>             
                    </tr>
                </table>
            </td>
        </tr>
      <tr class="titulos" >
          <td colspan="5"  bgcolor="#CCCCCC" height="5px">
              <% if(beanSession.usuario.preguntarMenu("6_b1") ){%>
                                <input id="btnCrearNuevoPaciente" type="button" class="small button blue"
                                    title="FUNCIONA45" value="Crear nuevo paciente"
                                    onclick="modificarCRUD('crearNuevoPaciente')" />
                                <%}%>
          </td>  
      <tr>
    <tr class="titulosCentrados" >
        <td width="100%" colspan="5">Informaci&oacute;n de cita</td>
    <tr>  
      <tr class="titulosListaEspera">
          <td>Administradora Agenda</td> 
          <td>Tipo R&eacute;gimen - Plan</td>
          <td colspan="3">Plan de Contrataci&oacute;n</td>            
      </tr>         
      <tr class="estiloImputListaEspera" >        
        <td>
            <input type="text" id="txtAdministradora1"
                onchange="limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')"
                onkeypress="llamarAutocompletarEmpresa('txtAdministradora1',184)" style="width: 85%;" tabindex="118" />
            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaAdminRem"
                onclick="traerVentanitaEmpresa(this.id,'divParaVentanita','txtAdministradora1','24');limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
        </td>
        <td>
            <select id="cmbIdTipoRegimen" style="width: 95%;" onclick="limpiarDivEditarJuan('limpiarDatosPlan')"
                onfocus="limpiarDivEditarJuan('limpiarDatosRegimenFactura');
                    cargarComboGRALCondicion2('', '', this.id, 79, valorAtributoIdAutoCompletar('txtAdministradora1'), valorAtributo('cmbSede'))"
                tabindex="117">
                <option value=""></option>
            </select>
        </td>           
        <td colspan="3">
            <label id="lblIdPlanContratacion" hidden></label><label id="lblNomPlanContratacion" hidden></label>     
            <select id="cmbIdPlan" style="width: 70%;"
                onfocus="limpiarDivEditarJuan('limpiarDatosRegimenFactura'); limpiarDivEditarJuan('limpiarDatosPlan')
                         traerPlanesDeContratacionAgenda(this.id, '165', 'txtAdministradora1', 'cmbIdTipoRegimen')"
                onchange="asignaAtributo('lblIdPlanContratacion', valorAtributo(this.id), 0)"
                tabindex="117">
                <option value=""></option>
            </select>     
        </td>                       
      </tr>                            
      <tr class="titulosListaEspera" >
          <td>N&uacute;mero autorizaci&oacute;n</td> 
          <td>Fecha vigencia autorizaci&oacute;n</td>                            
          <td>Fecha que paciente desea la cita</td> 
          <td colspan="2">Instituci&oacute;n</td>                            
      </tr>
      <tr class="estiloImput" >
          <td>
              <input  type="text" id="txtNoAutorizacion"  size="30" maxlength="50"  style="width:85%" onKeyPress="javascript:return teclearExcluirCaracter(event, '%');" tabindex="115"/>
          </td> 
          <td>              
              <input id="txtFechaVigencia"  type="text"  size="10"  maxlength="10"  style="width:60%" tabindex="116" />
          </td>
          <td><input id="txtFechaPacienteCita"  type="text"  size="10"  maxlength="10" style="width:60%" tabindex="117" onblur="verificarNotificacionAntesDeGuardar('verificarFechaPaciente')">
          </td>  
          <td colspan="2"><input type="text" id="txtIPSRemite" size="60"  maxlength="60"   onkeypress="llamarAutocomIdDescripcionConDato('txtIPSRemite', 313)" style="width:90%"  tabindex="118" />                                    
              <img width="18px" height="18px" align="middle" title="VEN2" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIPSRemite', '4')" src="/clinica/utilidades/imagenes/acciones/buscar.png">                            
          </td>                              
      </tr>
    
      <tr>
          <td colspan="5">
              <table width="100%">
                <tr class="titulosListaEspera">           
                    <td width="25%">Tipo:</td>                  
                    <td width="10%">Estado:</td>                           
                    <td width="20%">Via:</td>                           
                    <td width="20%">Clasificación:</td>                           
                    <td width="10%">Sitio</td>
                    <td width="15%">Nro. de Referencia</td> 
                </tr>
                <tr class="estiloImput" >
                    <td>
                        <select size="1" id="cmbTipoCita"  onfocus="cargarTipoCitaSegunEspecialidad(this.id)" onchange="buscarAGENDA('listProcedimientosAgenda')" style="width:90%" tabindex="119" >                                          
                            <option value=""></option>             
                        </select>
                    </td> 
                    <td>
                        <input type="hidden" id="txtEstadoCita" />
                            <select id="cmbEstadoCita" onfocus="limpiarDivEditarJuan('limpiarMotivoAD')" style="width:90%" tabindex="120" >
                            <%     resultaux.clear();
                                    resultaux=(ArrayList)beanAdmin.combo.cargar(107);  
                                    ComboVO cmbEst;
                                    for(int k=0;k<resultaux.size();k++){
                                        cmbEst=(ComboVO)resultaux.get(k);
                            %>
                                            <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>">
                                                <%= cmbEst.getId()+"  "+cmbEst.getDescripcion()%></option>
                                            <%}%>                     
                        </select>
                    </td>
                    <td>
                        <select id="cmbMotivoConsulta" style="width:90%" tabindex="121" onfocus="comboDependiente('cmbMotivoConsulta', 'cmbEstadoCita', '96');limpiarDivEditarJuan('limpiarMotivoClase');">
                            <option value=""></option>
                            <%     resultaux.clear();
                                    resultaux=(ArrayList)beanAdmin.combo.cargar(106);  
                                    ComboVO cmbM;
                                    for(int k=0;k<resultaux.size();k++){
                                        cmbM=(ComboVO)resultaux.get(k);
                            %>
                                            <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                                                <%= cmbM.getId()+"  "+cmbM.getDescripcion()%></option>
                                            <%}%>                     
                        </select>
                    </td>
                    <td>
                        <select id="cmbMotivoConsultaClase" style="width:90%" tabindex="121" onfocus="comboDependienteDosCondiciones('cmbMotivoConsultaClase','cmbMotivoConsulta','cmbIdEspecialidad','563')">
                            <option value=""></option>                    
                        </select>
                    </td>           
                    <td>
                        <select id="cmbIdSitio" style="width:90%" tabindex="123" >
                                <option value=""></option>
                                <%     resultaux.clear();
                                    resultaux=(ArrayList)beanAdmin.combo.cargar(120);
                                    ComboVO cmbQ;
                                    for(int k=0;k<resultaux.size();k++){
                                            cmbQ=(ComboVO)resultaux.get(k);
                                %>
                                        <option value="<%= cmbQ.getId()%>" title="<%= cmbQ.getTitle()%>">
                                            <%= cmbQ.getDescripcion()%></option>
                                        <%}%>
                        </select>     
                    </td>
                    <td>
                        <input type="text" id="txtNoLente" size="60" maxlength="60" style="width:80%" tabindex="119" onkeypress="javascript:return teclearsoloDigitos(event);" class="">
                        <!--<img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaNoLente"  onclick="traerVentanitaCondicion1Autocompletar(this.id, 'txtIdBusPaciente','divParaVentanitaCondicion1','txtNoLente','17')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> -->
                        <div id="divParaVentanitaCondicion1"></div>
                    </td>
                </tr>            
            </table>
          </td>
      </tr>
      <tr class="titulosListaEspera">
        <td colspan="5">Observaciones</td>                   
      </tr>
      <tr class="estiloImput" >
          <td colspan="5"><input type="text" id="txtObservacion"  size="200" maxlength="2000"  style="width:80%"  onKeyPress="javascript:return teclearExcluirCaracter(event, '%');" onkeyup="javascript: this.value= this.value.toUpperCase();"tabindex="122"/>
              <input name="btn_crear_cita" type="button" class="small button blue" title="B777" value="Ver" onclick="mostrar('divVentanitaNotificaciones');buscarAGENDA('listAgendaNotifica')" />
              <input  id="txtObservacionConfirma"  size="200" maxlength="2000" type="hidden" onKeyPress="javascript:return teclearExcluirCaracter(event, '%');" onkeyup="javascript: this.value= this.value.toUpperCase();" tabindex="123"/>
          </td>                                                 
      </tr>  
<tr class="titulos"> 
                      <td width="100%" colspan="5">
                        <div style="overflow:auto; height:100px; width:100%" id="divProcedimientosLE">           
                            <table id="listProcedimientosAgenda" class="scroll"></table>
                        </div>  
                      </td>
</tr>
 <tr class="titulosCentrados" >
                         <td width="100%" colspan="5">Informaci&oacute;n de Procedimientos</td>
                     <tr>
                     <tr class="titulosListaEspera" >
                       <td>Sitio</td>
                       <td colspan="2">Procedimiento CUPS</td>
                       <td colspan="2">Observaciones Procedimiento</td>
                     </tr>
                     <tr class="titulos" >
                         <td>
                           <select id="cmbIdSitioQuirurgico" style="width:80%" tabindex="123" >                             
                                <%     resultaux.clear();
                                       resultaux=(ArrayList)beanAdmin.combo.cargar(120);
                                       ComboVO cmbSQ;
                                       for(int k=0;k<resultaux.size();k++){
                                             cmbSQ=(ComboVO)resultaux.get(k);
                                %>
                                <option value="<%= cmbSQ.getId()%>" title="<%= cmbSQ.getTitle()%>">
                                    <%= cmbSQ.getDescripcion()%></option>
                                <%}%>
                           </select>                          
                         </td>
                         <td colspan="2"><input type="text" id="txtIdProcedimiento"  size="200" maxlength="200"  style="width:98%" onkeypress="llamarAutocomIdDescripcionParametro('txtIdProcedimiento', 420, 'lblIdPlanContratacion')" onkeyup="javascript: this.value= this.value.toUpperCase();" tabindex="124"/>
                         </td>
                         <td colspan="2">                   
                           <input type="text" id="txtObservacionProcedimientoCirugia"  size="200" maxlength="200" onkeyup="javascript: this.value= this.value.toUpperCase();" style="width:75%" tabindex="124"/>                   
                           <input name="btn_crear_cita" type="button" class="small button blue" value="Adici&oacute;n Proc." title="BT722" onclick="modificarCRUD('listCitaCexProcedimientoTraer')" tabindex="309"  />                             
                         </td>
                     </tr>
                 
                     <tr class="camposRepInp" >
                         <td width="100%" colspan="5">PROCEDIMIENTOS</td>
                     <tr>                 
                     <tr class="titulos"> 
                        <td width="100%" colspan="5">
                        <div style="overflow:auto; height:100px; width:100%" id="divCitaCirugiaProcedimientos">           
                            <table id="listCitaCexProcedimientoTraer" class="scroll"></table>
                        </div>  
                        </td>
                     </tr>   
      <tr>
          <td colspan="5"  align="center" >
              <input name="btn_crear_cita" type="button" class="small button blue" title="B55A" value="GUARDAR CITA" onclick="guardarUnaCitaAgenda()" />             
               <input type="hidden" id="txtColumnaIdSitioQuirur" />
           <input type="hidden" id="txtColumnaIdProced" />
           <input type="hidden" id="txtColumnaObservacion" />    
                 
          </td>
      </tr>
      <tr class="titulos" >
          <td colspan="5" >HISTORIAL HISTORIA CLINICA</td>
      <tr>
      <tr class="titulos" > 
          <td colspan="5">
              <table id="listHCPaciente" class="scroll"></table>
          </td>
      </tr>                  
  </table>
</div>


<div id="divVentanitaListaEspera"  style="display:none; z-index:5900; top:1px; left:-251px;">
  <div class="transParencia" style="z-index:1001; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
  </div>
  <div class="cnt" style="z-index:5902; position:absolute; top:100px; left:6px;">
      <table width="100%"  border="1" cellpadding="0" cellspacing="0" class="fondoTabla">
          <tr class="estiloImput" >
              <td align="left"><input name="btn_cerrar" type="button" title="divVentanitaListaEspera" align="right" value="CERRAR." onclick="limpiarListadosTotales('listListaEsperaTraer');ocultar('divVentanitaListaEspera')" /></td>            
              <td align="right" colspan="3"><input name="btn_cerrar" type="button" align="right" value="CERRAR." onclick="limpiarListadosTotales('listListaEsperaTraer');ocultar('divVentanitaListaEspera')" /></td>
          </tr>         
          <tr class="titulosListaEspera">
              <td width="40%">Paciente</td>
              <td width="15%">Dias Espera</td>
              <td width="15%">Estado LE</td>  
              <td width="15%">Con Cita</td>                                                                        
          </tr>   
          <tr class="estiloImput" >
              <td>
                  <input type="text" size="110"  maxlength="210"  id="txtIdBusPacienteLE" onkeyup="javascript: this.value= this.value.toUpperCase();" onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPacienteLE', 307)"  style="width:80%"  tabindex="200" />
              </td>
              <td>
                  <select id="cmbDiasEsperaLE" size="1" style="width:98%"  tabindex="201" >                                         
                      <%   
                                                 resultaux.clear();
                                                 resultaux=(ArrayList)beanAdmin.combo.cargar(115);  
                                                 ComboVO cmbDias;
                                                 for(int k=0;k<resultaux.size();k++){
                                                               cmbDias=(ComboVO)resultaux.get(k);
                      %>
                                <option value="<%= cmbDias.getId()%>" title="<%= cmbDias.getTitle()%>">
                                    <%= cmbDias.getDescripcion()%></option>
                                <%}%>             
              </td>
              <td>
                  <select id="cmbEstadoLE" style="width:80%" tabindex="202" >                                       
                      <%  resultaux.clear();
                          resultaux=(ArrayList)beanAdmin.combo.cargar(114); 
                          ComboVO cmbELE;
                          for(int k=0;k<resultaux.size();k++){
                             cmbELE=(ComboVO)resultaux.get(k);
                      %>
                                <option value="<%= cmbELE.getId()%>" title="<%= cmbELE.getId()%>">
                                    <%= cmbELE.getDescripcion()%></option>
                                <%}%>                     
                  </select>             
              </td>  
              <td>
                  <select id="cmbBusSinCita" style="width:80%" tabindex="203" >
                      <option value="NO">NO</option>                                    
                      <option value="SI">SI</option>
                  </select>                                    
              </td> 
          </tr>                                                         
          <tr class="titulosListaEspera" align="center">
                <td>Administradora</td>
                <td>Grado Necesidad</td>                           
                <td>Rango Edad</td> 
                <td></td>                                                                
          </tr>
          <tr class="estiloImput">
              <td><input type="text"  id="txtAdministradoraLE" size="60" maxlength="60" onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradoraLE', 308)" style="width:80%"  tabindex="205" />
              </td>   
              <td>
                  <select id="cmbNecesidad" style="width:98%" tabindex="206" >                                          
                      <option value=""></option>
                      <%     resultaux.clear();
                             resultaux=(ArrayList)beanAdmin.combo.cargar(113);  
                             ComboVO cmbG;
                             for(int k=0;k<resultaux.size();k++){
                                   cmbG=(ComboVO)resultaux.get(k);
                      %>
                                <option value="<%= cmbG.getId()%>" title="<%= cmbG.getTitle()%>">
                                    <%=cmbG.getDescripcion()%></option>
                                <%}%>                     
                  </select>                          
              </td>   
              <td><select size="1" id="cmbRangosEdad" style="width:90%" tabindex="207" >
                      <%
                                                 resultaux.clear();
                                                 resultaux=(ArrayList)beanAdmin.combo.cargar(116);  
                                                 ComboVO cmbEdad;
                                                 for(int k=0;k<resultaux.size();k++){
                                                               cmbEdad=(ComboVO)resultaux.get(k);
                      %>
                                <option value="<%= cmbEdad.getId()%>" title="<%= cmbEdad.getTitle()%>">
                                    <%= cmbEdad.getDescripcion()%></option>
                                <%}%>
              </td>
              <td>
                  <input type="button" class="small button blue" title="B1588" value="Buscar en LE" onclick="buscarAGENDA('listListaEsperaTraer')" tabindex="207"/>               
              </td>
          </tr>
          <tr class="titulosListaEspera">
              <td colspan="4">Traer de Lista de Espera</td>
          </tr>
          <tr> 
              <td colspan="5">
                  <div id="divContenidoLETraer">        
                      <table align="center" id="listListaEsperaTraer"></table>
                  </div>  
              </td>
          </tr>       
      </table>
  </div>                                        
</div>


<div id="divVentanitaEditarListaEspera"  style="display:none; z-index:2000; top:1px; left:-211px;">
  <div class="transParencia" style="z-index:2001; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
  </div>
  <div  style="z-index:2002; position:absolute; top:180px; left:70px; width:95%">
      <table width="95%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
          <tr class="estiloImput" >
              <td align="left"><input name="btn_cerrar" title="divVentanitaEditarListaEspera" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditarListaEspera')" /></td>
              <td>&nbsp;Id Lista Espera= <label id="lblListaEsperaModificar"></label></td>
              <td>&nbsp;</td>             
              <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditarListaEspera')" /></td>
          <tr>  
          <tr class="estiloImput" >
              <td></td>
              <td>Tel&eacute;fono=&nbsp;<label id="lblTelefono"></label></td>
              <td>PACIENTE=&nbsp;<label id="lblNombrePaciente"></label>&nbsp&nbsp Edad:<label id="lblEdad"></label></td>
              <td>Dias Esperando=&nbsp;<label id="lblDias"></label></td>             
          <tr>                     
          <tr class="titulosListaEspera" >
              <td colspan="2">*Administradora</td>                  
              <td colspan="2">Observaciones</td>                
          <tr>
          <tr class="estiloImputListaEspera" >
              <td colspan="2"><input type="text"  id="txtAdministradora1LE" size="60"  maxlength="60"   onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradora1LE', 308)" style="width:98%"  tabindex="300" />
              </td> 
              <td colspan="2"><input  type="text" id="txtObservacionLE" size="100" maxlength="2000"  style="width:90%"  onKeyPress="javascript:return teclearExcluirCaracter(event, '%');" onkeyup="javascript: this.value= this.value.toUpperCase();"  tabindex="301"/></td>                    
          <tr>
          <tr class="titulosListaEspera">
              <td colspan="4">
                  <table width="100%">
                      <tr>
                          <td width="15%">Especialidad</td>                                  
                          <!--<td width="15%">Sub Especialidad</td>        -->                                                         
                          <td width="15%">Discapacidad F&iacute;sica</td>                       
                          <td width="10%">Embarazo</td>                      
                          <td width="12%">Tipo Cita</td>                
                          <td width="15%">Grado Necesidad</td>                      
                          <td width="10%">Estado</td>                                                                
                      </tr>
                      <tr class="estiloImputListaEspera" >
                          <td width="15%" >
                              <select id="cmbIdEspecialidadLE" style="width:99%"  onfocus="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')"  tabindex="302" onchange="buscarAGENDA('listDias', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                                          
                                  <option value=""></option>
                                 <!--<%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(100);  
                                         ComboVO cmb32_;
                                         for(int k=0;k<resultaux.size();k++){
                                               cmb32_=(ComboVO)resultaux.get(k);
                                  %>
                                <option value="<%= cmb32_.getId()%>" title="<%= cmb32_.getTitle()%>">
                                    <%= cmb32_.getDescripcion()%></option>
                                <%}%>   -->                  
                              </select>
                          </td>
                         <!-- <td>
                              <select id="cmbIdSubEspecialidadLE" style="width:90%"  tabindex="303" >
                                  <option value=""></option>
                                  <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(117);  
                                         ComboVO cmbSLE;
                                         for(int k=0;k<resultaux.size();k++){
                                               cmbSLE=(ComboVO)resultaux.get(k);
                                  %>
                                <option value="<%= cmbSLE.getId()%>" title="<%= cmbSLE.getTitle()%>">
                                    <%= cmbSLE.getDescripcion()%></option>
                                <%}%>                                                                 
                              </select>                    
                          </td>       -->             
                          <td>
                              <select id="cmbDiscapaciFisicaLE"  style="width:40%" tabindex="304">
                                  <option value="N">No</option>                                    
                                  <option value="S">Si</option>
                              </select>                                  
                          </td>                     
                          <td>
                              <select id="cmbEmbarazoLE" style="width:70%" tabindex="305">
                                  <option value="N">No</option>                                    
                                  <option value="S">Si</option>
                              </select>                                  
                          </td>                     
                          <td>
                              <select id="cmbTipoCitaLEM"  style="width:98%" tabindex="306">
                                  <option value="C">Control</option>
                                  <option value="P">Primera Vez</option>                                    
                              </select>                                  
                          </td>
                          <td><select id="cmbNecesidadLE" style="width:80%" tabindex="307">                                         
                                  <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(113);  
                                         ComboVO cmbG_;
                                         for(int k=0;k<resultaux.size();k++){
                                               cmbG_=(ComboVO)resultaux.get(k);
                                  %>
                                <option value="<%= cmbG_.getId()%>" title="<%= cmbG_.getTitle()%>">
                                    <%=cmbG_.getDescripcion()%></option>
                                <%}%>                     
                              </select>
                          </td>
                          <td>
                              <select id="cmbEstadoLEM" style="width:95%" tabindex="308" >
                                  <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(112);  
                                         ComboVO cmbELe;
                                         for(int k=0;k<resultaux.size();k++){
                                               cmbELe=(ComboVO)resultaux.get(k);
                                  %>
                                <option value="<%= cmbELe.getId()%>" title="<%= cmbELe.getTitle()%>">
                                    <%=cmbELe.getDescripcion()%></option>
                                <%}%>                     
                              </select>                                  
                          </td>                                                              
                      </tr>                                        
                  </table>    
              </td>
          </tr>
          <tr class="estiloImputListaEspera">
              <td colspan="4">
                  <input name="btn_crear_cita" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificarListaEspera', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="309"  />
              </td>
          </tr>
      </table>
  </div>   
</div>
<div id="divVentanitaPacienteYaExiste"  style="display:none; z-index:2050; top:1px; left:-391px;">
  <div class="transParencia" style="z-index:2051; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
  </div>
  <div  style="z-index:2052; position:absolute; top:180px; left:1px; width:104%">
      <table width="90%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
          <tr class="estiloImput" >
              <td align="left"><input name="btn_cerrar" title="divVentanitaPacienteYaExiste" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaPacienteYaExiste')" /></td>
              <td>&nbsp;</td>             
              <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaPacienteYaExiste')" /></td>
          <tr>  
          <tr class="estiloImput" >
              <td colspan="2"><label id="lblIdPacienteExiste"></label></td>
              <td colspan="1" align="left"><label style="animation-delay; color:#F00" id="lblLetrero"></label></td>               
          <tr>
          <tr class="titulosCentrados" > 
              <td colspan="2">LISTA ESPERA</td>
              <td colspan="2">CITAS</td>              
          </tr>            
          <tr class="titulos" > 
              <td colspan="4">
                  <div style="overflow:auto;height:180px; width:100%"   id="divContenidoListaEsperaHist"  >           
                      <table id="listHistoricoListaEsperaEnAgenda" class="scroll"></table>
                  </div>  
              </td>
          </tr>               
          <tr class="titulosListaEspera">
              <td colspan="2">
                  <input name="btn_crear_cita" type="button" class="small button blue" value="TRAER" onclick="asignaAtributo('txtIdBusPaciente', valorAtributo('lblIdPacienteExiste'), 0);buscarInformacionBasicaPaciente(); ocultar('divVentanitaPacienteYaExiste')" tabindex="309"  />
              </td>
              <td colspan="1">
                  <input name="btn_crear_cita" type="button" class="small button blue" value="VER HISTORIA CLINICA" title="b8k67" onclick="buscarHC('listDocumentosHistoricosTodos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');" tabindex="309"  />
              </td>
          </tr>
          <tr class="titulos">
              <td colspan="3">                                  
                  <div id="divdatosbasicoscredesa" style="height:130px; width:100%; overflow-y: scroll;">
                      <table id="listDocumentosHistoricos" class="scroll" cellpadding="0" cellspacing="0" align="center">
                          <tr><th></th></tr>                   
                      </table>
                  </div> 
              </td>
          </tr>
          <tr> 
              <td colspan="4">         
                  <table width="1080" align="center"  border="0" cellspacing="0" cellpadding="1"   >
                      <tr class="titulosListaEspera" >
                      <div id="divAcordionAdjuntos" style="display:BLOCK">    
                          <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                              <jsp:param name="titulo" value="Archivos adjuntos" />
                              <jsp:param name="idDiv" value="divArchivosAdjuntos" />
                              <jsp:param name="pagina" value="../../archivo/contenidoAdjuntos.jsp" />              
                              <jsp:param name="display" value="NONE" />
                              <jsp:param name="funciones" value="acordionArchivosAdjuntos()" />
                          </jsp:include>
                      </div>
          </tr>  
      </table>            
      </td>
      </tr> 
      </table>
  </div>   
</div>
<!-------------------------para la vista previa---------------------------- -->
<div id="divVentanitaPdf"  style="display:none; z-index:2050; top:1px; left:190px;">
  <div id="idDivTransparencia" class="transParencia" style="z-index:2054; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
  </div>
  <div id="idDivTransparencia2"  style="z-index:2055; position:absolute; top:5px; left:15px; width:100%">
      <table id="idTablaVentanitaPDF" width="90%" height="90" class="fondoTabla" >
          <tr class="estiloImput" >
              <td align="left"><input name="btn_cerrar" title="divVentanitaPdf" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaPdf')" /></td>
              <td>&nbsp;</td>             
              <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaPdf')" /></td>
          </tr> 
          <tr class="estiloImput">
              <td colspan="3">                                              
                  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
                      <tr>
                          <td>
                              <div id="divParaPaginaVistaPrevia" ></div>   
                          </td>
                      </tr> 
                  </table>
              </td>
          </tr>
      </table>
  </div>   
</div>
<div id="divVentanitaNotificaciones" style="display:none; z-index:7000; top:1px; width:900px; left:150px;">
  <div class="transParencia" style="z-index:7001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
  </div>
  <div style="z-index:7002; position:fixed; top:200px; left:180px; width:70%">
      <table width="90%" height="110" cellpadding="0" cellspacing="0" class="fondoTabla">
          <tr class="estiloImput">
              <td align="left" width="10%">
                  <input name="btn_cerrar" title="divVentanitaNotificaciones" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaNotificaciones')"/>
              </td>
              <td align="right" width="10%">
                  <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaNotificaciones')"/>
              </td>
          </tr>          
          <tr class="titulos">              
              <td colspan="2">
                  <div id="idDivAgendaNotifica" style="width:100%" >
                      <table id="listAgendaNotifica" class="scroll"></table>
                  </div>
              </td>  
          </tr>
            
      </table>
  </div>
</div>
<input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />     
<input type="hidden"  id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden"  id="txtTipoDocumentoVistaPrevia" value="" />                      
<input type="hidden"  id="lblTituloDocumentoVistaPrevia" value=""  />
<input type="hidden"  id="lblIdAdmisionVistaPrevia" value=""  />                 
<!-------------------------fin para la vista previa---------------------------- -->    
 
 
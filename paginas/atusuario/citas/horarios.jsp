<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn());

    ArrayList resultaux = new ArrayList();

%>

<table width="1200px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="AGENDAS" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO  aqui empieza el cambio-->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td valign="top" width="80%">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr class="titulosListaEspera">
                                            <td width="30%">Sede</td>
                                            <td width="30%">Especialidad</td>
                                            <td width="20%">Profesional</td>
                                        </tr>
                                        <tr class="estiloImput">
                                            <td>
                                                <select size="1" id="cmbSede" style="width:90%" title="GD38"
                                                    onfocus="comboDependienteSede('cmbSede','557')"
                                                    ochange="asignaAtributo('cmbIdEspecialidad', '', 0); asignaAtributo('cmbIdProfesionales', '', 0);cargarTipoCitaSegunEspecialidad('cmbTipoCita', 'cmbIdEspecialidad')">
                                                </select>
                                            </td>
                                            <td>
                                                <select size="1" id="cmbIdEspecialidad" style="width:90%" tabindex="14"
                                                    onfocus="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')"
                                                    style="width:80%" tabindex="14">
                                                    <option value=""></option>

                                                </select>
                                            </td>
                                            <td>
                                                <select size="1" id="cmbIdProfesionales" style="width:90%"
                                                    onfocus="cargarProfesionalesDesdeEspecialidadSede(this.id)"
                                                    tabindex="14"
                                                    onchange="limpiaAtributo('cmbHoraInicio',0);limpiaAtributo('cmbMinutoInicio',0);limpiaAtributo('cmbPeriodoInicio',0);limpiaAtributo('cmbDuracionMinutos',0);limpiaAtributo('cmbCuantas',0);limpiaAtributo('cmbHoraFinEliminar',0);limpiaAtributo('cmbMinutoFinEliminar',0);limpiaAtributo('cmbPeriodoFinEliminar',0);limpiaAtributo('cmbIdProfesionalesMov',0);limpiaAtributo('cmbBloqueCita',0);buscarAGENDA('listDiasC');">
                                                    <option value=""></option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%">
                                        <caption class="camposRepInp">Crear una nueva agenda</caption>
                                        <tr class="titulosListaEspera">
                                            <td width="25%"></td>
                                            <td width="10%">Minutos de duraci&oacute;n</td>
                                            <td width="15%">N&uacute;mero de citas</td>
                                            <td width="15%">Citas en bloque</td>
                                            <td width="20%">Consultorio</td>
                                            <td width="20%"></td>
                                        </tr>
                                        <tr class="estiloImput">
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td></td>
                                                        <td class="titulos"><small>Hora de inicio:</small></td>
                                                        <td>
                                                            <select id="cmbHoraInicio" style="width: 90%;">
                                                                <option value=""></option>
                                                                <%
                                                                    for (int i = 101; i < 113; i++) {
                                                                            String[] value = Integer.toString(i).split("");
                                                                            String hour = value[1] + value[2];
            
                                                                %>
                                                                <option value="<%=hour%>"><%=hour%></option>
                                                                <%}
                                                                %>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbMinutoInicio" style="width: 90%;">
                                                                <option value=""></option>
                                                                <%
                                                                    for (int i = 100; i < 160; i++) {
                                                                        String[] value = Integer.toString(i).split("");
                                                                        String minute = value[1] + value[2];
            
                                                                %>
                                                                <option value="<%=minute%>"><%=minute%></option>
                                                                <%}
                                                                %>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbPeriodoInicio" style="width: 90%;">
                                                                <option value=""></option>
                                                                <option value="AM">AM</option>
                                                                <option value="PM">PM</option>
                                                            </select>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td class="titulos">
                                                            <small>Hora de fin:</small>
                                                        </td>
                                                        <td>
                                                            <select id="cmbHoraFinEliminar" style="width: 90%;">
                                                                <option value=""></option>
                                                                <%
                                                                    for (int i = 101; i < 113; i++) {
                                                                        String[] value = Integer.toString(i).split("");
                                                                        String hour = value[1] + value[2];
                                                                %>
                                                                <option value="<%=hour%>"><%=hour%></option>
                                                                <%}%>                                  
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbMinutoFinEliminar" style="width: 90%;">
                                                            	<option value=""></option>
                                                                div              <%
                                                                    for (int i = 100; i < 160; i++) {
                                                                        String[] value = Integer.toString(i).split("");
                                                                        String minute = value[1] + value[2];
                                                                %>
                                                                <option value="<%=minute%>"><%=minute%></option>
                                                                <%}
                                                                %>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbPeriodoFinEliminar" style="width: 90%;">
                                                                <option value=""></option>
                                                                <option value="AM">AM</option>
                                                                <option value="PM">PM</option>
                                                            </select>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="CENTER">
                                                <select size="1" id="cmbDuracionMinutos" style="width: 80%"
                                                    title="Mnibutos de duración" tabindex="14">
                                                    <option value=""></option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="8">8</option>
                                                    <option value="10">10</option>
                                                    <option value="15">15</option>
                                                    <option value="20">20</option>
                                                    <option value="25">25</option>
                                                    <option value="30">30</option>
                                                    <option value="35">35</option>
                                                    <option value="40">40</option>
                                                    <option value="45">45</option>
                                                    <option value="50">50</option>
                                                    <option value="55">55</option>
                                                    <option value="60">60</option>
                                                    <option value="120">2 HORAS</option>
                                                    <option value="180">3 HORAS</option>
                                                    <option value="240">4 HORAS</option>
                                                    <option value="300">5 HORAS</option>
                                                </select>
                                            </td>
                                            <td align="CENTER">
                                                <select size="1" id="cmbCuantas" style="width:80%"
                                                    title="Número de citas" tabindex="14">
                                                    <option value=""></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>
                                                    <option value="24">24</option>
                                                    <option value="25">25</option>
                                                    <option value="26">26</option>
                                                    <option value="27">27</option>
                                                    <option value="28">28</option>
                                                    <option value="29">29</option>
                                                    <option value="30">30</option>
                                                    <option value="31">31</option>
                                                    <option value="32">32</option>
                                                    <option value="33">33</option>
                                                    <option value="34">34</option>
                                                    <option value="35">35</option>
                                                    <option value="36">36</option>
                                                    <option value="37">37</option>
                                                    <option value="38">38</option>
                                                    <option value="39">39</option>
                                                    <option value="40">40</option>
                                                    <option value="41">41</option>
                                                    <option value="42">42</option>
                                                    <option value="43">43</option>
                                                    <option value="44">44</option>
                                                    <option value="45">45</option>
                                                    <option value="46">46</option>
                                                    <option value="47">47</option>
                                                    <option value="48">48</option>
                                                    <option value="49">49</option>
                                                    <option value="50">50</option>
                                                    <option value="51">51</option>
                                                    <option value="52">52</option>
                                                    <option value="53">53</option>
                                                    <option value="54">54</option>
                                                    <option value="55">55</option>
                                                    <option value="56">56</option>
                                                    <option value="57">57</option>
                                                    <option value="58">58</option>
                                                    <option value="59">59</option>
                                                    <option value="60">60</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select size="1" id="cmbBloqueCita" style="width:80%;"
                                                    tabindex="14">
                                                    <option value=""></option>
                                                    <option value="N">No</option>
                                                    <option value="S">Si</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select size="1" id="cmbConsultorio" style="width:100%;"
                                                    onfocus="cargarComboGRALCondicion2('', '', this.id, 312, valorAtributo('cmbIdEspecialidad'), valorAtributo('cmbSede'))"
                                                    tabindex="14">
                                                </select>
                                            </td>
                                            <td align="center"><input name="btn_cerrar" type="button"
                                                    class="small button blue" align="right" value=" Crear Horario"
                                                    title="BT33E" onclick="modificarCRUD('listAgenda');" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%">
                                        <caption class="camposRepInp" id="tituloOpciones">Modificar Agenda
                                        </caption>
                                        <tr id="divOpciones" show>
                                            <td>
                                                <table width="100%">
                                                    <tr class="titulosListaEspera">
                                                        <td align="CENTER">
                                                            <input name="btn_cerrar" class="small button blue"
                                                                type="button"
                                                                title="Permite Eliminar Horario disponible"
                                                                align="right" value="Eliminar disponibles"
                                                                onclick="modificarCRUD('eliminarDisponibles', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                                        </td>
                                                        <td>Fecha Destino</td>
                                                        <td>Profesional destino:</td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td class="titulos"> 
                                                            <input type="text" size="10" maxlength="10"
                                                                title="Fecha Destino" id="txtFechaDestino"
                                                                tabindex="10" />
                                                        </td>
                                                        <td class="titulos">
                                                            <select size="1" id="cmbIdProfesionalesMov"
                                                                style="width:80%"
                                                                onfocus="cargarProfesionalDesdeEspecialidad('cmbIdEspecialidad','cmbIdProfesionalesMov')"
                                                                tabindex="14">
                                                                <option value=""></option>
                                                            </select>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>     
                                                        </td>
                                                        <td align="center">
                                                            <input name="btn_cerrar"
                                                                title="Permite copiar Horario a fecha destino Seleccionada"
                                                                class="small button blue" type="button" align="right"
                                                                value="Copiar horario "
                                                                onclick="modificarCRUD('listAgendaCopiarDia', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                                            <input name="btn_cerrar" class="small button blue"
                                                                title="Permite Mover Agenda a fecha destino seleccionada"
                                                                type="button" align="center" value="Mover Horario"
                                                                onclick="modificarCRUD('MoverAgenda', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                                        </td>
                                                        <td align="center">
                                                            <input name="btn_mover" class="small button blue"
                                                                title="Permite Mover Agenda al profesional Seleccionado"
                                                                type="button" align="right"
                                                                value="Mover agenda a Profesional"
                                                                onclick="modificarCRUD('MoverAgendaporProfesional', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                                        </td>

                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="divListAgenda" style="background-color:White;">
                                        <table id="listAgenda" class="scroll">
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%">
                            <tr class="titulos">
                                <td align="CENTER">
                                    <img width="15" height="15" style="cursor:pointer"
                                        src="/clinica/utilidades/imagenes/acciones/izquierda.png"
                                        onclick="irMesAnteriorHorario(); buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                                    <label id="lblMes"></label>/<label id="lblAnio"></label>
                                    <img width="15" height="15" style="cursor:pointer"
                                        src="/clinica/utilidades/imagenes/acciones/derecha.png"
                                        onclick="irMesSiguienteHorario(); buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                                    <input name="btn_cerrar" type="button" class="small button blue" align="right"
                                        value="Refrescar"
                                        onclick="buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                    Exito:
                                    <select size="1" id="cmbExito" style="width:20%;"
                                        onchange="buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                                        tabindex="14">
                                        <option value="S">Si</option>
                                        <option value="N">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="CENTER">
                                    <label id="lblFechaSeleccionada"></label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <table id="listDiasC" class="scroll"></table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <input type="hidden" id="txtIdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>" />
        </td>
    </tr>
</table>


<div id="divVentanitaAgenda"
    style="position:absolute; display:none; background-color:#E2E1A5; top:200px; left:100px; width:1000px; height:100px; z-index:999">
    <table width="100%" id="CabeceraSublistadoCalendario" border="1" class="fondoTabla">

        <tr class="estiloImput">
            <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                    onclick="ocultar('divVentanitaAgenda')" /></td>
            <td align="CENTER">&nbsp;</td>
            <td align="CENTER">&nbsp;</td>
            <td align="right" colspan="2"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                    onclick="ocultar('divVentanitaAgenda')" /></td>
        </tr>
        <tr class="titulosListaEspera">
            <td align="CENTER" colspan="3">ID HORARIO</td>
            <td align="CENTER" colspan="2">Minutos de duraci&oacute;n</td>
        </tr>
        <tr class="estiloImput">
            <td align="CENTER" colspan="3">
                <strong><label id="lblIdAgendaDetalle" style="color: red;"></label></strong>
            </td>
            <td align="CENTER" colspan="2">
                <strong><label id="lblDuracion"></label></strong>
            </td>
        </tr>
        <tr class="titulosListaEspera">
            <td width="15%">Fecha</td>
            <td width="25%">Hora</td>
            <td width="25%">Profesional</td>
            <td width="20%">Consultorio</td>
            <td width="15%"></td>
        </tr>
        <tr class="titulosListaEspera">
            <td>
                <input type="text" size="10" maxlength="10" title="Fecha Destino Cita" id="txtFechaDestinoCita"
                    tabindex="14" width="80%"/>
            </td>
            <td>
                <select id="cmbHoraInicioDiv" style="width: 30;">
                    <%
                                    for (int i = 101; i < 113; i++) {
                                            String[] value = Integer.toString(i).split("");
                                            String hour = value[1] + value[2];

                                %>
                    <option value="<%=hour%>"><%=hour%></option>
                    <%}
                                %>
                </select>
                <select id="cmbMinutoInicioDiv" style="width: 30;">
                    <%
                                    for (int i = 100; i < 160; i++) {
                                        String[] value = Integer.toString(i).split("");
                                        String minute = value[1] + value[2];

                                %>
                    <option value="<%=minute%>"><%=minute%></option>
                    <%}
                                %>
                </select>
                <select id="cmbPeriodoInicioDiv" style="width: 30;">
                    <option value="AM">AM</option>
                    <option value="PM">PM</option>
                </select>
            </td>
            <td>
                <select size="1" id="cmbIdProfesionalesAg" style="width:100%"
                    onfocus="cargarProfesionalesDesdeEspecialidad(this.id)"
                    tabindex="14" title='Seleccione Profesional para el cambio de cita'>
                    <option value=""></option>
                </select>
            </td>
            <td class="titulos">
                <select size="1" id="cmbConsultorioAg" style="width:80%"
                    onfocus="cargarComboGRALCondicion2('', '', this.id, 312, valorAtributo('cmbIdEspecialidad'), valorAtributo('cmbSede'))"
                    tabindex="14">
                    <option value=""></option>
                </select>
            </td>
            <td>
                <input name="btn_cerrar" class="small button blue" title="Permite Mover cita" type="button"
                    align="right" value="Modificar"
                    onclick="modificarCRUD('MoverCitaPaciente', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
            </td>
        </tr>
        <tr>
            <td colspan="5"><hr></td>
        </tr>
        <tr class="titulosListaEspera">
            <td align="center">
                <input id="btnEliminarCita" type="button" class="small button blue" value="ELIMINAR DISPONIBLE"
                    onclick="verificarNotificacionAntesDeGuardar('eliminarAgendaDetalle');"
                    tabindex="112" />
            </td>
            <td align="center">
                <input id="btnEliminarCita" type="button" class="small button blue" value="DEJAR DISPONIBLE"
                    onclick="verificarNotificacionAntesDeGuardar('dejarDisponibleAgendaDetalle');"
                    tabindex="112"/>
            </td>
            <td colspan="3" align="center">
                Minutos:<select size="1" onfocus="calcularMinutosParaDividir()" id="cmbDuracionMinutosSubDividir" style="width:10%" title="38" tabindex="14">
                    <option value="1">1</option>
                </select>
                <input id="btnSubDividirCita" type="button" class="small button blue" value="SUBDIVIDIR"
                    onclick="modificarCRUD('subDividirAgenda', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                    tabindex="112" />
            </td>
        </tr>
    </table>
</div>
// JavaScript para la programacion de los botones


/** funciones para la accion de busqueda*/



function buscarInformacionPacienteExistenteReferencia(){

	  buscarInformacionBasicaPaciente()
      buscarReferencia('listRemision')
}




function buscarReferencia(arg){
	
	pag ='/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
	  switch (arg){	/*AGENDA INICIO*/
        
			case 'listRemision':  
		     limpiarDivEditarJuan(arg);  
			 
			 if(valorAtributo('lblIdAdmision')!="NO_HC"){ 
				 ancho= ($('#drag'+ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width()-50);			
				 valores_a_mandar=pag;
				 //valores_a_mandar=valores_a_mandar+"?idQuery=367&parametros=";
				 valores_a_mandar=valores_a_mandar+"?idQuery=694&parametros=";
				 add_valores_a_mandar(valorAtributo('lblIdPaciente'));				 
	         }
			 else{
				 ancho= ($('#drag'+ventanaActual.num).find("#tablaPrincipalRemision").width()-50);		
				 valores_a_mandar=pag;
				 //valores_a_mandar=valores_a_mandar+"?idQuery=367&parametros=";
				 valores_a_mandar=valores_a_mandar+"?idQuery=694&parametros=";
				 add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));				 
			 }
			 

			 $('#drag'+ventanaActual.num).find("#"+arg).jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:[ 'contador','ID','ID_TIPO','Tipo','ID_SERVICIO_QUE','Servicio que solicita',
             'ID_SERVICIO_PARA','Servicio para el cual se solicita','Priodidad','NIVEL','ID_TIPO_RESPONSABLE',
             'ID_RESPONSABLE','APELLIDO1','APELLIDO2','NOMBRE1','NOMBRE2','TELEFONO','DIRECCION','DEPARTAMENTO',
             'MUNICIPIO','Informacion','ID_ELABORO','Elaboro','Fecha Elaboro','P1','P2','P3','P4','P5','P6'],			 
             colModel :[ 
               {name:'contador', index:'contador', hidden:true},  
               {name:'ID', index:'ID',hidden:true}, 
			   {name:'ID_TIPO', index:'ID_TIPO',hidden:true},			   			   			   
			   {name:'TIPO', index:'TIPO',width: anchoP(ancho,10)},	
			   {name:'ID_SERVICIO_QUE', index:'ID_SERVICIO_QUE',hidden:true},	
               {name:'SERVICIO_SOLICITA', index:'SERVICIO_SOLICITA',width: anchoP(ancho,18)}, 		
			   {name:'ID_SERVICIO_PARA', index:'ID_SERVICIO_PARA',hidden:true},	
               {name:'SERVICIO_PARA', index:'SERVICIO_PARA',width: anchoP(ancho,18)},
               {name:'PRIORIDAD', index:'PRIORIDAD',hidden:true},
               {name:'NIVEL', index:'NIVEL',hidden:true},
               {name:'ID_TIPO_RESPONSABLE', index:'ID_TIPO_RESPONSABLE',hidden:true},
               {name:'ID_RESPONSABLE', index:'ID_RESPONSABLE',hidden:true},
               {name:'APELLIDO1', index:'APELLIDO1',hidden:true},
               {name:'APELLIDO2', index:'APELLIDO2',hidden:true},
               {name:'NOMBRE1', index:'NOMBRE1',hidden:true},
               {name:'NOMBRE2', index:'NOMBRE2',hidden:true},
               {name:'TELEFONO', index:'TELEFONO',hidden:true},
               {name:'DIRECCION', index:'DIRECCION',hidden:true},
               {name:'DEPARTAMENTO', index:'DEPARTAMENTO',hidden:true},
               {name:'MUNICIPIO', index:'MUNICIPIO',hidden:true},
               {name:'INFORMACION', index:'INFORMACION',width: anchoP(ancho,15)},			   			   
               {name:'ID_ELABORO', index:'ID_ELABORO',hidden:true},
               {name:'ELABORO', index:'ELABORO',width: anchoP(ancho,5)},			   			   			   
               {name:'FECHA_ELABORO', index:'FECHA_ELABORO', width: anchoP(ancho,5)},		
               {name:'P1', index:'P1',hidden:true},
               {name:'P2', index:'P2',hidden:true},
               {name:'P3', index:'P3',hidden:true},
               {name:'P4', index:'P4',hidden:true},
               {name:'P5', index:'P5',hidden:true},
               {name:'P6', index:'P6',hidden:true},

			   ], 
			  onSelectRow: function(rowid) {

				 var datosRow = jQuery('#drag'+ventanaActual.num).find('#'+arg).getRowData(rowid); 					  
				 asignaAtributo('lblIdRemi', datosRow.ID, 0); 
				 asignaAtributo('lblFechaEvento', datosRow.FECHA_ELABORO, 0);				 
				 asignaAtributo('lblIdTipoRemi', datosRow.ID_TIPO, 0);					   					   
				 asignaAtributo('lblNomTipoRemi', datosRow.TIPO, 0);					   					   				 
				 asignaAtributo('lblNombreElementoRemi', datosRow.SERVICIO_SOLICITA, 0);
			     mostrar('divVentanitaRemision') ; 

			     //valores de formulario

			     asignaAtributo('cmbTipoEvento', datosRow.ID_TIPO, 0); 
			     asignaAtributo('cmbServicioSolicita', datosRow.ID_SERVICIO_QUE, 0); 
			     asignaAtributo('cmbServicioParaCual', datosRow.ID_SERVICIO_PARA, 0); 
			     asignaAtributo('cmbPrioridad', datosRow.PRIORIDAD, 0); 
			     asignaAtributo('cmbNivelRemis', datosRow.NIVEL, 0); 
			     asignaAtributo('cmbTipoDocResponsable', datosRow.ID_TIPO_RESPONSABLE, 0); 
			     asignaAtributo('txtDocResponsable', datosRow.ID_RESPONSABLE, 0); 
			     asignaAtributo('txtPrimerApeResponsable', datosRow.APELLIDO1, 0); 
			     asignaAtributo('txtSegundoApeResponsable', datosRow.APELLIDO2, 0); 
			     asignaAtributo('txtPrimerNomResponsable', datosRow.NOMBRE1, 0); 
			     asignaAtributo('txtSegundoNomResponsable', datosRow.NOMBRE2, 0); 
				 asignaAtributo('txtTelefonoResponsable', datosRow.TELEFONO, 0); 
			     asignaAtributo('txtDireccionResponsable', datosRow.DIRECCION, 0); 
			     asignaAtributo('txtDeparResponsable', datosRow.DEPARTAMENTO, 0); 
			     asignaAtributo('txtMunicResponsable', datosRow.MUNICIPIO, 0);
			     asignaAtributo('txt_InformacionRemision', datosRow.INFORMACION, 0); 
			     asignaAtributo('cmbIdProfesionalRemision', datosRow.ID_ELABORO, 0); 
			     asignaAtributo('cmbVerificacionPaciente', datosRow.P1, 0); 
			     asignaAtributo('cmbFormatoDiligenciado', datosRow.P2, 0); 
			     asignaAtributo('cmbResumenHc', datosRow.P3, 0); 
			     asignaAtributo('cmbDocumentoIdentidad', datosRow.P4, 0); 
			     asignaAtributo('cmbImagenDiagnostica', datosRow.P5, 0); 
			     asignaAtributo('cmbPacienteManilla', datosRow.P6, 0); 
			      



				// buscarReferencia('listRemisionTareas')
			  },
			 height: 50, 
			 width: ancho,
         });  
	    $('#drag'+ventanaActual.num).find("#"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;	
		case 'listRemisionTareas':  
		 limpiarDivEditarJuan(arg);  

		 ancho= ($('#drag'+ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width()-50);			
		 valores_a_mandar=pag;
		 valores_a_mandar=valores_a_mandar+"?idQuery=197&parametros=";
		 add_valores_a_mandar(valorAtributo('lblIdRemi'));
		 
		 $('#drag'+ventanaActual.num).find("#"+arg).jqGrid({ 
		 url:valores_a_mandar, 	
		 datatype: 'xml', 
		 mtype: 'GET', 
		 colNames:[ 'contador','ID','Fecha Elaboro','Funcionario Recibe','Cargo','Dependencia','Observacion','Elaboro'],			 
		 colModel :[ 
		   {name:'contador', index:'contador', hidden:true},  
		   {name:'ID', index:'ID',hidden:true}, 
		   {name:'FECHA_ELABORO', index:'FECHA_ELABORO', width: anchoP(ancho,10)},			   
		   {name:'FUNCIONARIO', index:'FUNCIONARIO',width: anchoP(ancho,20)},			   			   			   
		   {name:'CARGO', index:'CARGO',width: anchoP(ancho,20)},		
		   {name:'DEPENDENCIA', index:'DEPENDENCIA',width: anchoP(ancho,20)}, 			   
		   {name:'OBSERVACION', index:'OBSERVACION',width: anchoP(ancho,20)},
		   {name:'elaboro', index:'elaboro',width: anchoP(ancho,10)},			   			   			   
		   ], 
		  onSelectRow: function(rowid) {
			 var datosRow = jQuery('#drag'+ventanaActual.num).find('#'+arg).getRowData(rowid); 					  
			 asignaAtributo('lblIdRemi', datosRow.ID, 0); 
			 asignaAtributo('lblFechaEvento', datosRow.FECHA_ELABORO, 0);				 
			 asignaAtributo('lblIdTipoRemi', datosRow.TIPO, 0);					   					   
			 asignaAtributo('lblNombreElementoRemi', datosRow.SERVICIO_SOLICITA, 0);
			 mostrar('divVentanitaRemision') ; 
		  },
		 height: 400, 
		 width: ancho,
         });  
	    $('#drag'+ventanaActual.num).find("#"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;			
		
	
	 }
}












	
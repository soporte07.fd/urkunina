<%--
-Author: Juan Angel Santacruz
-Date: 8 enero 2008
-
-Description: comentarios de la pagina

--%>
 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Sgh.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Sgh.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Sgh.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<% 	Fecha fecha = new Fecha();
    beanAdmin.setCn(beanSession.getCn()); 
//    beanPersonal.setCn(beanSession.getCn());
    ArrayList resultaux=new ArrayList();
	java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
    java.util.Date date = cal.getTime();
    java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance();
    SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");
%>

<table width="650px"  border="0" cellspacing="0" cellpadding="0"   >
  <tr>
    <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="GESTIONAR " />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="1" cellpadding="0" cellspacing="0" class="fondoTabla">
  <tr>
    <td>  
      <div style="overflow:auto; height:100px" id="divContenidoUsua">      <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
       <!--******************************** inicio de la tabla con los datos d e busqueda ***************************-->	
        <div id="divBuscarUsua"  >  
              <table width="100%" border="0" cellpadding="1" cellspacing="1">
                <tr>
                  <td class="titulos">Por favor digite su contrase&ntilde;a</td>
                </tr>
                <tr>
                
                  <td><center>
                  <!-- <input name="txtContrasenaAdm" id="txtContrasenaAdm" type="password" size="32" maxlength="30" onchange="
                                  if(validarContrasena(this.value)){
                                      ocultar('divcontrasena');
                                      mostrar('contenido');
                                  }else{
                                      alert('Su contraseņa es incorrecta');
                                      this.value='';
                                      this.focus();
                                  }" 
                              tabindex="1" /> -->
                              <input name="txtContrasenaAdm" id="txtContrasenaAdm" type="password" size="32" maxlength="30" onchange="  verificarContrasenaAx('',this.value)	" 
                              tabindex="1" />				
                      </center>
                  </td>
                </tr>
              </table>
      </div>
        <!--********************** fin de la tabla con los datos de busqueda ***********************************************-->
        <!--******************************** inicio de la tabla con los datos del formulario ***************************-->	
        <div id="divEditarUsua" style="display:none"   > 
     <table width="100%" border="0" cellpadding="1" cellspacing="1">
      <tr class="titulos">
        <td colspan="3"><label class="asterisco" style="font-size:12px;">*</label>&nbsp;&nbsp;Identificaci&oacute;n usuario</td>
      </tr>
      <tr>
        <td class="titulos">Tipo identificaci&oacute;n</td>
        <td class="titulos">Identificaci&oacute;n</td>
        <td class="titulos" width="40%">Apellidos y Nombres</td>
      </tr>
      <tr class="estiloImput">
        <td><center>
          <select name="cmbTipoIdUsua"  id="cmbTipoIdUsua" style="width:90%"  tabindex="2" >
                <option value="">[Todo Tipo Documento]</option>
                <%     resultaux.clear();
                       resultaux=(ArrayList)beanAdmin.CargarTipoId();	
                       TipoIdVO tipoidb;
                       for(int k=0;k<resultaux.size();k++){ 
                             tipoidb=(TipoIdVO)resultaux.get(k);
                %>
              <option value="<%= tipoidb.getCodigo()%>"><%= tipoidb.getDescripcion()%></option>
                      <%}%>		
          </select>
        <img src="/Sgh/utilidades/imagenes/acciones/lupa.gif" width="15" height="15" align="absmiddle" /></center>
        </td>
        <td><center> 
            <input name="txtId" id="txtId" type="text" size="22" maxlength="20" onKeyPress="javascript:return teclearsoloTelefono(event);"  />
            <input name="hddId" id="hddId" type="hidden" value=""  />
            <input name="hddTipoIdUsua" id="hddTipoIdUsua" type="hidden" value=""  />
        <img src="/Sgh/utilidades/imagenes/acciones/lupa.gif" width="15" height="15" align="absmiddle" onclick="consultarNombrePersona($('txtId').value,$('cmbTipoIdUsua').options[$('cmbTipoIdUsua').selectedIndex].value,'<%= response.encodeURL("/Sgh/paginas/accionesXml/cargarPersona_xml.jsp") %>');" />		</center>
        </td>
        <td><center><label id="lblNomPersona" style=" font-size:10px; font-family:Arial, Helvetica, sans-serif"></label></center></td>
      </tr>
      <tr>
        <td class="titulos"><label class="asterisco" style="font-size:12px;">*</label>&nbsp;&nbsp;Login</td>
        <td class="titulos"><label class="asterisco" style="font-size:12px;">*</label>&nbsp;&nbsp;Contrase&ntilde;a</td>	
        <td class="titulos">Confirmaci&oacute;n contrase&ntilde;a</td>	
      </tr>
      <tr class="estiloImput">
        <td><center><input name="txtLogin" id="txtLogin" type="text" size="32" maxlength="30" tabindex="4" class="nomayusculas"  /><img src="/Sgh/utilidades/imagenes/acciones/lupa.gif" width="15" height="15" align="absmiddle" /></center></td>
        <td><center><input name="txtContrasena" id="txtContrasena" type="password" size="32" maxlength="30" tabindex="5" class="nomayusculas"  />
        </center></td>	
        <td><center><input name="txtContrasena2" id="txtContrasena2" type="password" size="32" maxlength="30" onchange="" tabindex="6" class="nomayusculas"  />
        </center></td>	
      </tr>  
      <tr>
        <td class="titulos" colspan="2">Rol</td>
        <td class="titulos">Estado</td>	
      </tr>
      <tr class="estiloImput">	
        <td colspan="2"><center>
          <select name="cmbRol" id="cmbRol" tabindex="7">
               <option value="">[Todo Roles]</option>
                <%     resultaux.clear();
                       resultaux=(ArrayList)beanAdmin.cargarRoles();	
                       RolesVO roldb;
                       for(int k=0;k<resultaux.size();k++){ 
                             roldb=(RolesVO)resultaux.get(k);
                %>
              <option value="<%= roldb.getCodRol()%>"><%= roldb.getDescRol()%></option>
                      <%}%>				
          </select>
        </center></td>
        <td><center>
          <select name="cmbEstado" id="cmbEstado" tabindex="8">
            <option value="1" selected="selected">Activo</option>
            <option value="2">Inactivo</option>
          </select>
        </center></td>		
      </tr>
    </table>
        </div>
        <!--********************** fin de la tabla con los datos del formulario ***********************************************-->
      </div><!-- div contenido-->
 	</td>
  </tr>
</table> 
	  <!-- inicio borde esquinas redondas-->
	  <div class='filoInferiorTabla'>&nbsp;</div> 
	  <div id=nifty><B class=rbottom><B class=r4></B><B class=r3></B><B class=r2></B><B class=r1></B></B></DIV>
	  <!-- fin borde esquinas redondas-->
   </td>
</tr> 
</table>




<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->
 
 
<%  beanAdmin.setCn(beanSession.getCn());
   ArrayList resultaux=new ArrayList();
%>
 
<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
 <tr>
   <td>
     <!-- AQUI COMIENZA EL TITULO -->
     <div align="center" id="tituloForma">
       <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
       <jsp:include page="../titulo.jsp" flush="true">
         <jsp:param name="titulo" value="ROLES" />
       </jsp:include>
     </div>
     <!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr>
 <tr>
   <td>
     <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
         <td>
 
           <div style="overflow:auto;height:320px; width:1050px" id="divContenido">
             <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
             <div id="divBuscar" style="display:block">
               <table width="100%" cellpadding="0" cellspacing="0" align="center">
                 <tr class="titulos" align="center">
                   <td width="70%">NOMBRE</td>
                   <td width="10%"></td>
                   <td width="10%">VIGENTE</td>
                   <td width="10%">&nbsp;</td>
                 </tr>
                 <tr class="estiloImput">
                 
                   <td><input type="text" id="txtBusNombre" onkeyup="javascript: this.value= this.value.toUpperCase();" maxlength="50" style="width:90%" tabindex="2" /> </td>
                   <td>
                   </td>
                   <td><select size="1" id="cmbVigenteBus" style="width:80%" tabindex="2">
                       <option value="TRUE">SI</option>
                       <option value="FALSE">NO</option>
                     </select>
                   </td>
                   <td>
                     <input name="btn_BUSCAR" title="BT83t" type="button" class="small button blue" value="BUSCAR"
                       onclick="buscarUsuario('listGrillaPerfiles')" />
                   </td>
                 </tr>
               </table>
               <table id="listGrillaPerfiles" class="scroll"></table>
             </div>
           </div><!-- div contenido-->
 
           <div id="divEditar" style="display:block; width:100%">
 
             <table width="100%" cellpadding="0" cellspacing="0" align="center">
               <tr class="titulosCentrados">
                 <td colspan="3">INFORMACION ROL
               </tr>
               <tr>
                 <td>
 
                   <table width="100%" cellpadding="0" cellspacing="0" align="center">
                     <tr>
                       <td>
                         <table width="100%">
                          
                           <tr class="estiloImputIzq2">
                             <td width="30%"> COGIDO DEL ROL</td>
                             <td width="70%"><input type="text" id="txtId" onkeyup="javascript: this.value= this.value.toUpperCase();" maxlength="3" style="width:20%" /></td>
                           </tr>
                           <tr class="estiloImputIzq2">
                             <td width="30%">NOMBRE</td>
                             <td width="70%"><input type="text" id="txtNombre" onkeyup="javascript: this.value= this.value.toUpperCase();" maxlength="50" style="width:60%" /></td>
                           </tr>
                           <tr class="estiloImputIzq2">
                             <td width="30%">DESCRIPCION</td>
                             <td width="70%"><input type="text" id="txtdefinicion" onkeyup="javascript: this.value= this.value.toUpperCase();" maxlength="100" style="width:60%" /></td>
                           </tr>
                           <tr class="estiloImputIzq2">
                             <td width="30%">VIGENTE</td><td width="70%"><select size="1" id="cmbVigente" style="width:7%" tabindex="2" >                                        
                                 <option value=""></option>
                                 <option value="t">SI</option>
                                 <option value="f">NO</option>                 
                                </select></td>
                           </tr>        
                                       
                         </table>
                       </td>
                     </tr>
                   </table>
 
                   <table width="100%" style="cursor:pointer">
                     <tr>
                       <td width="99%" class="titulos">
                         <input name="btn_limpiar_new" type="button" title="btn_lm784" class="small button blue"
                           value="LIMPIAR " onclick="limpiaAtributo('txtId',0);limpiaAtributo('txtNombre',0);  limpiaAtributo('cmbVigente',0);limpiaAtributo('txtdefinicion',0)" />
                         <input name="btn_CREAR" title="AD75" type="button" class="small button blue" value="CREAR ROL"
                           onclick="modificarCRUDParametros('crearPerfil','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                         <input name="btn_MODIFICAR" title="AD76" type="button" class="small button blue"
                           value="MODIFICAR ROL"
                           onclick="modificarCRUDParametros('modificarPerfil','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
 
                       </td>
                     </tr>
                   </table>
 
                 </td>
               </tr>
             </table>
 
             <table width="100%" cellpadding="0" cellspacing="0" align="center">
               <tr class="titulosCentrados">
                 <td colspan="3">ASIGNACION DE PERMISOS
                 </td>
               </tr>
               <tr>
                 <td>
          
                   <table width="100%" cellpadding="0" cellspacing="0" align="center">
                     <tr>
                       <td>
                         <table width="100%">
                           <tr class="estiloImputIzq2">
                           <td width="30%">OPCIONES DEL MENU</td>
                           <td width="70%"><input  type="text" id="txtIdOpcion"  style="width:60%"      tabindex="100"  />
                               <img width="18px" height="18px" align="middle" title="Seleccione para la busqueda" id="idLupitaVentanitaPerfil" onclick="traerVentanitaPerfiles(this.id, '', 'divParaVentanita', 'txtIdOpcion', '29')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                           </td><div id="divParaVentanita"></div>
                 <td width="5%"> <label id="lblIdGrupo"></label> </td>                
                 </tr>
                 <tr   class="estiloImputIzq2">
           <td class="titulos" colspan ="3">           
           <input name="btn_CREAR" title="btn_ad75" type="button" class="small button blue" value="AGREGAR PERMISO " onclick="modificarCRUD('agregarPermisoPerfil','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  /> 
           <input name="btn_ELIMINAR" title="btn_mo785" type="button" class="small button blue" value="ELIMINAR PERMISO" onclick="modificarCRUD('eliminarPermisoPerfil','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  /> 
           </td>                                   
           </tr>                                                                     
           </table>
           </td>  
           </tr>  
           </table>           
           <table id="listGrillaGrupopciones" class="scroll"></table>             
           </td>
           <label style="display: none;" id="lblIdSede"></label>
           </tr>
           </table>
 
 
             <table width="100%" cellpadding="0" cellspacing="0" align="center">
              
                 <td>
                 </td>
               </tr>
 </table>
</div><!-- divEditar-->
    
 
       </td>
      </tr>
     </table>
 
  </td>
</tr>
</table>


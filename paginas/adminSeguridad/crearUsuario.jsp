<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="Crear usuario" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>

            <div style="overflow:auto;height:320px; width:1050px" id="divContenido">
              <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
              <div id="divBuscar" style="display:block">
                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                  <tr class="titulos" align="center">
                    <td width="10%">IDENTIFICACION </td>
                    <td width="35%">NOMBRE</td>
                    <td width="35%">SEDE PRINCIPAL</td>                    
                    <td width="10%">VIGENTE</td>                    
                  </tr>
                  <tr class="estiloImput">
                    <td><input type="text" id="txtBusId" style="width:90%" tabindex="2" /> </td>
                    <td><input type="text" id="txtBusNombre" style="width:90%" onkeyup="javascript: this.value= this.value.toUpperCase();"  maxlength="80" tabindex="2" /> </td>
                    <td>
                      <select size="1" id="cmbSE" style="width:80%" tabindex="2" title="576" >                                         
                        <option value=""></option>
                            <%     resultaux.clear();
                                    resultaux=(ArrayList)beanAdmin.combo.cargar(963);  
                                    ComboVO cmbSE; 
                                    for(int k=0;k<resultaux.size();k++){ 
                                          cmbSE=(ComboVO)resultaux.get(k);
                             %>
                                       <option value="<%= cmbSE.getId()%>" title="<%= cmbSE.getTitle()%>">
                                         <%=cmbSE.getDescripcion()%></option>
                                       <%} %>
                        </select>
                    </td>                    
                    <td><select size="1" id="cmbVigenteBus" style="width:80%" tabindex="2">
                        <option value="1">SI</option>
                        <option value="0">NO</option>
                      </select>
                    </td>
                    <td>
                      <input name="btn_MODIFICAR" title="BT83t" type="button" class="small button blue" value="BUSCAR"
                        onclick="buscarUsuario('listGrillaUsuarios')" />
                    </td>
                  </tr>
                </table>
                <table id="listGrillaUsuarios" class="scroll"></table>
              </div>
            </div><!-- div contenido-->

            <div id="divEditar" style="display:block; width:100%">

              <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr class="titulosCentrados">
                  <td colspan="3">INFORMACION DEL USUARIO
                  </td>
                </tr>
                <tr>
                  <td>

                    <table width="100%" cellpadding="0" cellspacing="0" align="center">
                      <tr>
                        <td>
                          <table width="100%">
                            <tr class="estiloImputIzq2">
                              <td>TIPO IDENTIFICACION</td>
                              <td width="70%" colspan="2"><select size="1" id="cmbTipoId" style="width:35%" tabindex="2">
                                  <option value=""></option>
                                  <option value="CC">CC - Cedula de Ciudadania</option>
                                  <option value="CE">CE - Cedula de Extranjeria</option>

                                </select></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%"> IDENTIFICACION</td>
                              <td width="70%" colspan="2"><input type="text" id="txtIdentificacion" onkeypress="javascript:return soloTelefono(event);" style="width:30%" /></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%">PRIMER NOMBRE</td>
                              <td width="70%" colspan="2"><input type="text" oninput="llenarNombre1()" onkeypress="return teclearsoloAlfabeto(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtPNombre" style="width:30%" /></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%">SEGUNDO NOMBRE</td>
                              <td width="70%" colspan="2"><input type="text" onkeypress="return teclearsoloAlfabeto(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtSNombre" style="width:30%" /></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%">PRIMER APELLIDO</td>
                              <td width="70%" colspan="2"><input type="text" oninput="llenarApe1()" onkeypress="return teclearsoloAlfabeto(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtPApellido" style="width:30%" /></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%">SEGUNDO APELLIDO</td>
                              <td width="70%" colspan="2"><input type="text" onkeypress="return teclearsoloAlfabeto(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtSApellido" style="width:30%" /></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%">NOMBRE PERSONAL</td>
                              <td width="70%" colspan="2"><input type="text"  onkeypress="return teclearsoloAlfabeto(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtNomPersonal" style="width:40%" /></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%">USUARIO DE SESION</td>
                              <td width="70%" colspan="2"><input type="text" id="txtUSuario" style="width:30%" onkeyup="javascript: this.value= this.value.toUpperCase();"
                                  onkeypress="javascript: return validaEspacio(event,this);" />&nbsp;&nbsp;SE CREARA EL USUARIO CON LA CONTRASE&Ntilde;A 123. EN SU PRIMER INGRESO SE PEDIRA EL CAMBIO DE ESTA.</td> <!-- -->
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%">PROFESION</td>
                              <td width="70%" colspan="2"><select id="cmbIdTipoProfesion" style="width:60%" tabindex="2">
                                  <option value=""></option>
                                  <%     resultaux.clear();
                           resultaux=(ArrayList)beanAdmin.combo.cargar(40);	
                           ComboVO cmbTR; 
                           for(int k=0;k<resultaux.size();k++){ 
                                 cmbTR=(ComboVO)resultaux.get(k);
                    %>
                                  <option value="<%= cmbTR.getId()%>" title="<%= cmbTR.getTitle()%>">
                                    <%=cmbTR.getDescripcion()%></option>
                                  <%}%>						
                 </select></td> 
            </tr>    
           
            <tr   class="estiloImputIzq2">                         
              <td>REGISTRO PROFESIONAL</td><td>              
					<input type="text" id="txtRegistro" style="width:40%" tabindex="2" onkeyup="javascript: this.value= this.value.toUpperCase();"/>
              </td>    
              <td rowspan="5">
                <img id="imgFirmaProfesional" width="180px" height="100px" onerror="this.src='../utilidades/firmas/blanco.png'" >
              </td>                                         
            </tr>                                             
            <tr   class="estiloImputIzq2">                         
              <td>CORREO ELECTRONICO</td><td>              
          <input type="text" id="txtCorreo" style="width:40%" onkeypress="javascript:return email(event);" tabindex="2"/>
              </td>                                             
            </tr>  

          <tr   class="estiloImputIzq2">                         
              <td>TELEFONO</td><td>              
          <input type="text" id="txtPhone" style="width:40%" onkeypress="javascript:return soloTelefono(event);" tabindex="2"/>
              </td>                                             
            </tr>          
                        <tr class="estiloImputIzq2">
              <td width="10%">SEDE PRINCIPAL</td><td width="70%">
                <select size="1" id="cmbSede" style="width:80%" tabindex="2" title="576" >                                         
                   <option value=""></option>
                       <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(963);  
                               ComboVO cmbBod; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbBod=(ComboVO)resultaux.get(k);
                        %>
                                  <option value="<%= cmbBod.getId()%>" title="<%= cmbBod.getTitle()%>">
                                    <%=cmbBod.getDescripcion()%></option>
                                  <%} %>
                                </select>
                              </td>
                            </tr>


                            <tr class="estiloImputIzq2">
                              <td width="30%">TRABAJA ACTUALMENTE</td>
                              <td width="70%"><select size="1" id="cmbEstado" style="width:20%" tabindex="2">

                                  <option value="1">SI</option>
                                  <option value="0">NO</option>
                                </select></td>
                               
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>

                    <table width="100%" style="cursor:pointer">
                      <tr>
                        <td width="99%" class="titulos">
                          <input name="btn_limpiar_new" type="button" title="btn_lm784" class="small button blue"
                            value="LIMPIAR USUARIO" onclick="limpiarDivEditarJuan('limpiarCrearUsuario')" />
                          <input name="btn_CREAR" title="AD75" type="button" class="small button blue" value="CREAR USUARIO"
                            onclick="modificarCRUDParametros('crearUsuario','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                          <input name="btn_MODIFICAR" title="AD76" type="button" class="small button blue"
                            value="MODIFICAR USUARIO"
                            onclick="modificarCRUDParametros('modificarUsuario','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                            <input type="button" class="small button blue"
                            value="SUBIR IMAGEN"
                            onclick="validarImagenFirma()" />

                        </td>

                        <td>
                          <input type="file" id="firma" name="firma" accept="image/png, image/jpeg">
                        </td>
                       
                      </tr>
                    </table>

                  </td>
                </tr>
              </table>

              <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr class="titulosCentrados">
                  <td colspan="3">SEDES SECUNDARIAS</td>
                </tr>
                <tr>
                  <td>
            
                    <table width="100%" cellpadding="0" cellspacing="0" align="center">
                      <tr>
                        <td>
                          <table id="sedes" class="scroll"></table>
                          <table width="100%">                                                        
                            <tr   class="estiloImputIzq2">                                                            
                                <td class="titulos" colspan ="3">            
                                    <input name="btn_CREAR" title="btn_ad75" type="button" class="small button blue" value="AGREGAR SEDE" onclick="agregarSede(obtenerListaSedes('sedes'))"  />  
                                    <input name="btn_MODIFICAR" title="btn_mo785" type="button" class="small button blue" value="ELIMINAR SEDE" onclick="modificarCRUD('eliminarSede','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
                                </td>                                    
                            </tr>                                                       		           
                          </table> 
                        </td>   
                      </tr>   
                    </table>            
                    <table id="sede" class="scroll"></table>              
                  </td>
            <label style="display: none;" id="lblIdSede"></label>
            </tr>
            </table>


              <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr class="titulosCentrados">
                  <td colspan="3">ROLES DE USUARIO
                  </td>
                </tr>
                <tr>
                  <td>

                    <table width="100%" cellpadding="0" cellspacing="0" align="center">
                      <tr>
                        <td>
                          <table width="100%">
                            <tr class="estiloImputIzq2">
                              <td width="10%">ROLES(GRUPO)</td>
                              <td width="70%">
                                <select size="1" id="cmbIdGrupo" style="width:35%" tabindex="2">
                                  <option value=""></option>
                                  <%   resultaux.clear();
                                        resultaux=(ArrayList)beanAdmin.combo.cargar(41);	
                                        ComboVO cmbGR; 
                                        for(int k=0;k<resultaux.size();k++){ 
                                        cmbGR=(ComboVO)resultaux.get(k);
                                   %>
                                  <option value="<%= cmbGR.getId()%>" title="<%= cmbGR.getTitle()%>">
                                    <%=cmbGR.getDescripcion()%></option>
                                  <%}%>
							                  </select>
				                      </td > 
			  <td width="5%"> <label id="lblIdGrupo"></label> </td>
				</tr> 
				<tr   class="estiloImputIzq2"> 
				 <td class="titulos" colspan ="3">
				  <input name="btn_limpiar_new" type="button" title="btn_lm784" class="small button blue" value="LIMPIAR" onclick="limpiarDivEditarJuan('limpiarGrupoUsuario')" />                      
				  <input name="btn_CREAR" title="btn_ad75" type="button" class="small button blue" value="CREAR ROL" onclick="modificarCRUD('crearGrupoUsuario','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
				  <input name="btn_MODIFICAR" title="btn_mo785" type="button" class="small button blue" value="ELIMINAR ROL" onclick="modificarCRUD('eliminarGrupoUsuario','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
				  </td>
				  
				  
				  
				</tr>                                                       		           
			</table> 
		  </td>   
		</tr>   
	  </table>

	  <table id="listGrillaGrupoUsuarios" class="scroll"></table>  
  
	</td></tr>
  </table>
</div><!-- divEditar--> 
      

        </td>
       </tr>
      </table>

   </td>
 </tr> 
 
</table>

<input type="hidden" id="lblFirmaProfesional" /> 
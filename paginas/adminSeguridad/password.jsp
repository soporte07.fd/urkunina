<%--

-Date:15 - 12 - 08
-
-Description: comentarios de la pagina
--%>
<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.Sesion" %>
 <% //@ page import = "Sgh.AdminSeguridad.ControlAdmin" %>
 <%// @ page import = "Sgh.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->

<table width="800px" border="0" cellspacing="0" cellpadding="0" > <!-- en el atributo width se ubica el ancho de la p�gina preferiblemente en pixels -->
  <tr>
    <td>
	  <!-- AQUI COMIENZA EL TITULO -->	 <!-- en value del parametro ubicar el titulo de la pantalla -->
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="CAMBIO DE CONTRASEÑA" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
       <table width="100%" border="1" cellpadding="0" cellspacing="0" class="fondoTabla">
          <tr>
            <td>               
                  <div id="divEditarUsua" style="display:block" >                      
                    <table width="100%" border="0" cellpadding="1" cellspacing="1">                      
                      <tr>
                        <td class="titulos">Tipo identificaci&oacute;n</td>
                        <td class="titulos">Identificaci&oacute;n</td>
                        <td class="titulos" width="40%">Apellidos y Nombres</td>
                      </tr>
                      <tr class="estiloImput">
                        <td><center>
                          <label id="lblTipoId">CC</label>                        
                        </td>
                        <td><center> 
                          <label id="lblIdUsuarioSesion"><%=beanSession.usuario.getIdentificacion()%></label>                                                                                     
                        </td>
                        <td><center>
                          <label><span style="color: #0C3D50; font-size:10px;">&nbsp;<strong><%= beanSession.usuario.getApellido1()+ " " + beanSession.usuario.getNombre1()%></strong></span></label>
                        </td>
                      </tr>
                      <tr>
                        <td class="titulos"><label class="asterisco" style="font-size:12px;">*</label>&nbsp;&nbsp;Login</td>
                        <td class="titulos"><label class="asterisco" style="font-size:12px;">*</label>&nbsp;&nbsp;Contrase&ntilde;a</td>	
                        <td class="titulos">Confirmaci&oacute;n contrase&ntilde;a</td>	
                      </tr>
                      <tr class="estiloImput">
                        <td><center>
                        <label id="lblLoginUsuarioSesion"><%=beanSession.usuario.getLogin()%></label>
                        </center>
                        </td>
                        <td><center><input name="txtContrasena" id="txtContrasena" type="text"  maxlength="30" tabindex="5" class="nomayusculas"  />
                        </center></td>	
                        <td><center><input name="txtContrasena2" id="txtContrasena2" type="password"  maxlength="30" onchange="" tabindex="6" class="nomayusculas"  />
                        </center></td>	
                      </tr>  
                      <tr style="display: none;">
                        <td class="titulos" colspan="2">Rol</td>
                        <td class="titulos">Estado</td>	
                      </tr>
                      <tr class="estiloImput" style="display: none;">	
                        <td colspan="2"><center>
                          <select name="cmbRol" id="cmbRol" tabindex="7">
                            <option value="0" selected="selected"> </option>
                            <option value="1" selected="selected">Gerente</option>
                            <option value="2">Secretaria</option>
                            <option value="3">Empleado</option>
                            <option value="4">Otro</option>
                          </select>
                        </center></td>
                        <td><center>
                          <select name="cmbEstado" id="cmbEstado" tabindex="8">
                            <option value="1" selected="selected">Activo</option>
                            <option value="0">Inactivo</option>
                          </select>
                        </center>
                        </td>		
                      </tr>
                      
                      
                      <tr class="estiloImput">	
                        <td colspan="3">                        
                        <input type="button" class="small button modificar" title="Modificar Contraseña" value="    Modificar " onclick="barraModificar('','<%= response.encodeURL("/clinica/paginas/accionesXml/modificar_xml.jsp")%>')"  tabindex="413" />
                        </td>		
                      </tr>

                      <tr class="estiloImput">
                        <td colspan="3">	
                        <label style="color: crimson;">AL CAMBIAR SU CONTRASE&Ntilde;A POR FAVOR CERRAR EL NAVEGADOR Y VOLVER A INGRESAR CON LA NUEVA CONTRASE&Ntilde;A</label>
                        </td>
                      </tr>
                      
                    </table>                     
                   </div>   
				  <!--********************** fin de la tabla con los datos del formulario ***********************************************-->

               </div>  
	 	    </td>
         </tr>
      </table>
   </td>
  </tr> 
</table>
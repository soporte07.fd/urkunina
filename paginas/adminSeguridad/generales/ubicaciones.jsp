
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Sgh.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Sgh.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Sgh.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<%

   beanAdmin.setCn(beanSession.getCn());
   ArrayList resultaux=new ArrayList();
%> 

<table width="500px" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td>
	<!-- AQUI COMIENZA EL TITULO -->	
     <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="GESTIONAR UBICACIONES GEOGRAFICAS" />
			 </jsp:include>
		 </div>	
    <!-- AQUI TERMINAN EL TITULO -->
	</td>
  </tr>
  <tr>
    <td>
	
	<table width="100%" border="0" cellpadding="1" cellspacing="1" class="fondoTabla">
          <tr> 
            <td width="100%" height="80px" align="center" valign="top"> 
              <table width="100%" border="0" cellpadding="1" cellspacing="1">
			     <tr class="titulos">
			       <td height="21" class="titulos"><div align="center">Paises  </div></td>
		        </tr>
			     <tr class="estiloImput"> 
				  <td width="37%" height="24">
					<select name="cmbPaises" style="width:350px" size="1" id="cmbPaises" tabindex="1" onchange="cargarDepartamentos(this.options[this.selectedIndex].value,'<%= response.encodeURL("/Sgh/paginas/accionesXml/cargarDeptos_xml.jsp") %>');document.getElementById('cmbMunicipio').options.length=1;document.getElementById('cmbBarrios').options.length=1; "> 
				    <option value="00"> - - - - - - - - - - - - Seleccionar Pais - - - - - - - - - - - - </option>
					<%   
						 resultaux.clear();
					     resultaux=(ArrayList)beanAdmin.cargarPais();	
						 PaisVO pais;
						 for(int k=0;k<resultaux.size();k++){ 
						       pais=(PaisVO)resultaux.get(k);
					%>
					<option value="<%= pais.getCod_pais()%>"><%= pais.getNombre_pais()%></option>
					<%}%>
				   </select>
					&nbsp;&nbsp; <img src="/Sgh/utilidades/imagenes/acciones/modificar.png" title="Modificar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo" onclick="javascript:document.getElementById('hddOpcion').value='Pais';modificarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/modificar_xml.jsp")%>');" />		
       			    &nbsp;&nbsp; <img src="/Sgh/utilidades/imagenes/acciones/edit_add.png" title="Adicionar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo" onclick="javascript:document.getElementById('hddOpcion').value='Pais';guardarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/guardar_xml.jsp")%>');" /> 
				    &nbsp;&nbsp; <img src="/Sgh/utilidades/imagenes/acciones/edit_remove.png" title="Eliminar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo"  onclick="javascript:document.getElementById('hddOpcion').value='Pais';eliminarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/eliminar_xml.jsp")%>');"  /></td>
			    </tr>
				 <tr class="titulos">
				   <td height="21" align="left"><div align="center">Departamentos</div></td>
			    </tr>
				 <tr class="estiloImput"> 
				  <td height="24">
				   
				   
				   <select name="cmbDepartamento" style="width:350px" size="1" id="cmbDepartamento" tabindex="2" onchange="cargarCiudades(this.options[this.selectedIndex].value,'<%= response.encodeURL("/Sgh/paginas/accionesXml/cargarMunicipios_xml.jsp") %>');document.getElementById('cmbBarrios').options.length=1;  "> 
				    <option value="00"> - - - - - - Seleccionar Departamento - - - - - - </option>
				  </select>
				  &nbsp;&nbsp; <img src="/Sgh/utilidades/imagenes/acciones/modificar.png" title="Modificar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo" onclick="javascript:document.getElementById('hddOpcion').value='Depto';modificarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/modificar_xml.jsp")%>');" />				   
				&nbsp;&nbsp;	<img src="/Sgh/utilidades/imagenes/acciones/edit_add.png" title="Adicionar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo" onClick="javascript:document.getElementById('hddOpcion').value='Depto';guardarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/guardar_xml.jsp")%>');" />				
				&nbsp;&nbsp;   <img src="/Sgh/utilidades/imagenes/acciones/edit_remove.png" title="Eliminar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo" onclick="javascript:document.getElementById('hddOpcion').value='Depto';eliminarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/eliminar_xml.jsp")%>');" />
				</td>
			     </tr>
				 <tr class="titulos">
				   <td height="21" align="left"><div align="center">Municipios</div></td>
			    </tr>
				 <tr class="estiloImput">
				 <td height="24">
				   
				   <select name="cmbMunicipio" style="width:350px"  size="1" id="cmbMunicipio" tabindex="3" onchange="cargarBarrios(this.options[this.selectedIndex].value,'<%= response.encodeURL("/Sgh/paginas/accionesXml/cargarBarrios_xml.jsp") %>'); "> 
				    <option value="00"> - - - - - - - - - Seleccionar Municipio - - - - - - - - - </option>
				   </select>
				   
				   
			      &nbsp;&nbsp; <img src="/Sgh/utilidades/imagenes/acciones/modificar.png" title="Modificar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo" onclick="javascript:document.getElementById('hddOpcion').value='Municipio';modificarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/modificar_xml.jsp")%>');" />&nbsp;&nbsp; 
				  <img src="/Sgh/utilidades/imagenes/acciones/edit_add.png" title="Adicionar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo" onClick="javascript:document.getElementById('hddOpcion').value='Municipio';guardarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/guardar_xml.jsp")%>');" />
				  &nbsp;&nbsp; <img src="/Sgh/utilidades/imagenes/acciones/edit_remove.png" title="Eliminar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo" onclick="javascript:document.getElementById('hddOpcion').value='Municipio';eliminarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/eliminar_xml.jsp")%>');" /></td>
			     </tr>
				 <tr class="titulos">
				   <td height="21" align="left"><div align="center">Barrios</div></td>
			    </tr>
				 <tr class="estiloImput"> 
				   <td height="25">
				   <select name="cmbBarrios" style="width:350px"  size="1" id="cmbBarrios"  tabindex="4">
				    <option value="00"> - - - - - - - - - Seleccionar Barrio - - - - - - - - - - - </option>
				   </select>
				   &nbsp;&nbsp;  <img src="/Sgh/utilidades/imagenes/acciones/modificar.png" title="Modificar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo" onclick="javascript:document.getElementById('hddOpcion').value='Barrio';modificarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/modificar_xml.jsp")%>');" />
				   &nbsp;&nbsp; <img src="/Sgh/utilidades/imagenes/acciones/edit_add.png" title="Adicionar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo" onClick="javascript:document.getElementById('hddOpcion').value='Barrio';guardarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/guardar_xml.jsp")%>');" />	
				   &nbsp;&nbsp;   <img src="/Sgh/utilidades/imagenes/acciones/edit_remove.png" title="Eliminar" style="cursor:pointer" width="18" height="18" border="0" align="absmiddle" class="manovinculo" onclick="javascript:document.getElementById('hddOpcion').value='Barrio';eliminarJuan('ubicaciones','<%= response.encodeURL("/Sgh/paginas/accionesXml/eliminar_xml.jsp")%>');" /></td>
			     </tr>
              </table>
			</td>
          </tr>
      </table>
	  <!-- inicio borde esquinas redondas-->
	  <div class='filoInferiorTabla'>&nbsp;
	    <input name="hddOpcion" type="hidden" id="hddOpcion" />
	  </div> 
	  <div id=nifty><B class=rbottom><B class=r4></B><B class=r3></B><B class=r2></B><B class=r1></B></B></DIV>
	  <!-- fin borde esquinas redondas-->
	
	</td>
  </tr>
</table>


 

		


<%--
-Author:Angelica Tutalcha
-Date: 14 04 2009
-

--%>
<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

 
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Sgh.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Sgh.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Sgh.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
//    beanPersonal.setCn(beanSession.getCn());
    ArrayList resultaux=new ArrayList();
	java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
    java.util.Date date = cal.getTime();
    java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance();
    SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");

%>

<table width="700px"  border="0" cellspacing="0" cellpadding="0"   >
  <tr>
    <td>
	   <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="GESTIONAR EMPRESAS PRESTADORAS DE SALUD" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="1" cellpadding="0" cellspacing="0" class="fondoTabla">
  <tr>
    <td>
  
   <div style="overflow:auto;" id="divContenido"> <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
       <!--******************************** inicio de la tabla con los datos de busqueda ***************************-->	
       <div id="divBuscar">                 
                <table width="100%" border="0" cellpadding="1" cellspacing="1" >
                    <tr>
                        <td colspan="2" class="titulos" align="center">BUSQUEDA DE EMPRESAS PRESTADORAS DE SALUD</td>
                    </tr>
                    <tr class="titulos">
                        <td width="50%">C&oacute;digo</td>
                        <td width="50%">Nit</td>
                    </tr>
                    <tr class="estiloImput">
                      <td align="center"><input id="txtBusCodigo" name="txtBusCodigo" type="text" style="width:50%" maxlength="5"  tabindex="1" onkeypress="javascript:return teclearsoloan(event);"/></td>
                      <td align="center"><input id="txtBusNit" name="txtBusNit" type="text" style="width:70%"  tabindex="2" onkeypress="javascript:return teclearsoloan(event);"/></td>
                   </tr>
               </table>
                
               <table width="100%" border="0" cellpadding="1" cellspacing="1" >
                <tr class="titulos">
                    <td width="30%">Nombre</td>
                    <td width="70%"><input id="txtBusNombre" name="txtBusNombre" type="text" style="width:70%" maxlength="5"  tabindex="1" onkeypress="javascript:return teclearsoloan(event);"/></td>
                </tr>
                </table>
				   <div id="divListado" align="center" >
                 <table id="list" class="scroll"></table>  
                <div id="pager" class="scroll" style="text-align:center;"></div>
                </div>            
        </div>
       <!--********************** fin de la tabla con los datos de busqueda ***********************************************-->
       <!--******************************** inicio de la tabla con los datos del formulario ***************************-->	
        <div id="divEditar"  style="display:none"> 
     <table width="100%" border="0" cellpadding="1" cellspacing="1" >
	   <tr class="titulos">
	   <td width="25%" >C&oacute;digo&nbsp;</td>
	   <td width="30%" >Nit</td>
       <td width="45%" >Nombre</td>
	   </tr>
	   <tr class="estiloImput">
	   <td align="center"><input id="txtCodigo" name="txtCodigo" type="text" size="10"   tabindex="1" onkeypress="javascript:return teclearsoloan(event);"/></td>
	   <td align="center"><input id="txtNit" name="txtNit" type="text" size="20"   tabindex="2" /></td>
   	   <td align="center"><input id="txtNombre" name="txtNombre" type="text" size="50"   tabindex="2" /></td>
   	 </tr>
     <tr>
     </tr>
   </table>
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >
	   <tr class="titulos">
	   <td width="35%" >Pais</td>
	   <td width="35%" >Departamento</td>
       <td width="30%" >Municipio</td>
	   </tr>
	   <tr class="estiloImput">
	   <td align="center">
	    <select name="cmbPaisRes" size="1" id="cmbPaisRes" style="width:90%"  tabindex="14" onchange="
													cargarDepartamentosIns('00','cmbDeptoRes',this.options[this.selectedIndex].value,'<%= response.encodeURL("/Sgh/paginas/accionesXml/cargarDeptos_xml.jsp") %>');
													document.getElementById('cmbMpioRes').options.length=1;						
											"> 
												<option value="00">[Seleccione un pa&iacute;s]</option>
												<%   resultaux.clear();
													 resultaux=(ArrayList)beanAdmin.cargarPais();	
													 PaisVO paisRes= new PaisVO();
													 for(int k=0;k<resultaux.size();k++){ 
														   paisRes=(PaisVO)resultaux.get(k);
														   if(Constantes.PAIS_DEFECTO.equals(paisRes.getCod_pais())){
														   %>
																<option value="<%= paisRes.getCod_pais()%>" selected="selected"><%= paisRes.getNombre_pais()%></option>
														 <%}else{%>
																<option value="<%= paisRes.getCod_pais()%>"><%= paisRes.getNombre_pais()%></option>
														 <%}
													 }%>
	   </select>
	   </td>
	   <td align="center">
	    <select name="cmbDeptoRes" id="cmbDeptoRes" tabindex="15" style="width:90%" onchange="
											cargarCiudadesIns('00','cmbMpioRes',this.options[this.selectedIndex].value,'<%= response.encodeURL("/Sgh/paginas/accionesXml/cargarMunicipios_xml.jsp") %>');
									">			<option value="00">[Seleccione un departamento]</option>
												<%   resultaux.clear();
													 resultaux=(ArrayList)beanAdmin.cargarDeptosCol();	
													 DeptoVO deptoRes;
													 for(int k=0;k<resultaux.size();k++){ 
														   deptoRes=(DeptoVO)resultaux.get(k);
														   if(Constantes.DEPTO_DEFECTO.equals(deptoRes.getCod_depto())){
														   %>
																<option value="<%= deptoRes.getCod_depto()%>" selected="selected"><%= deptoRes.getNombre_depto()%></option>
														 <%}else{%>
																<option value="<%= deptoRes.getCod_depto()%>"><%= deptoRes.getNombre_depto()%></option>
														 <%}
													 }%>	
  	    </select>
	   </td>
   	   <td align="center">
	   <select name="cmbMpioRes" id="cmbMpioRes" tabindex="16" style="width:90%">
									  <option value="00">[Seleccione un municipio]</option>
									  <%     resultaux.clear();
											 beanAdmin.municipio.setDeptoactual(Constantes.DEPTO_DEFECTO);
											 resultaux=(ArrayList)beanAdmin.cargarMunicipios();	
											 MunicipioVO mpioRes;
											 for(int k=0;k<resultaux.size();k++){ 
												   mpioRes=(MunicipioVO)resultaux.get(k);
												   if(Constantes.MUNICIPIO_DEFECTO.equals(mpioRes.getCod_muni())){
												   %>
														<option value="<%= mpioRes.getCod_muni()%>" selected="selected"><%= mpioRes.getNombre_muni()%></option>
												 <%}else{%>
														<option value="<%= mpioRes.getCod_muni()%>"><%= mpioRes.getNombre_muni()%></option>
												 <%}
											 }%>	
		</select>	   
	   </td>
   	 </tr>
   </table>
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >
	   <tr class="titulos">
	   <td width="25%" >Barrio</td>
	   <td width="50%" >Direccion</td>
	   <td width="25%" >Telefono</td>
	   </tr>
	   <tr class="estiloImput">
	   <td align="center">
	   <select name="cmbBarrioRes" size="1" id="cmbBarrioRes" style="width:90%"  tabindex="36">
													<option value="00">[Seleccione un Barrio]</option>
										  <%     resultaux.clear();
												 beanAdmin.barrio.setMunicipioactual(Constantes.MUNICIPIO_DEFECTO);
												 resultaux=(ArrayList)beanAdmin.cargarBarrios();	
												 BarrioVO barrio;
												 for(int k=0;k<resultaux.size();k++){ 
													   barrio=(BarrioVO)resultaux.get(k);
										  %>
													<option value="<%= barrio.getCod_barr()%>"><%= barrio.getNombre_barr()%></option>
												<%}%>						
 	  </select>	   
	   </td>
	   <td align="center"><input id="txtDir" name="txtDir" type="text" size="40"   tabindex="2" /></td>
   	   <td align="center"><input id="txtTelefono" name="txtTelefono" type="text" size="15"   tabindex="2" /></td>
   	 </tr>
   </table>
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >
	   <tr class="titulos">
	   <td width="25%" >Fax</td>
	   <td width="30%" >Email</td>
       <td width="45%" >Responsable</td>
	   </tr>
	   <tr class="estiloImput">
	   <td align="center"><input id="txtFax" name="txtFax" type="text" size="15"   tabindex="1" onkeypress="javascript:return teclearsoloan(event);"/></td>
	   <td align="center"><input id="txtEmail" name="txtEmail" type="text" size="30"   tabindex="2" /></td>
   	   <td align="center"><input id="txtResponsable" name="txtResponsable" type="text" size="40"   tabindex="2" /></td>
   	 </tr>
   </table>
    </div>
        <!--********************** fin de la tabla con los datos del formulario ***********************************************-->
     </div>
 	</td>
  </tr>
</table> 
	  <!-- inicio borde esquinas redondas-->
	  <div class='filoInferiorTabla'>&nbsp;</div> 
	  <div id=nifty><B class=rbottom><B class=r4></B><B class=r3></B><B class=r2></B><B class=r1></B></B></DIV>
	  <!-- fin borde esquinas redondas-->
   </td>
</tr> 
</table>




 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanEventoA" class="Clinica.GestionarEvento.ControlEventos" scope="session" /> <!-- instanciar bean de session -->
<% 	beanAdmin.setCn(beanSession.getCn()); 
//    beanPersonal.setCn(beanSession.getCn());
    beanEventoA.setCn(beanSession.getCn());
    ArrayList resultaux=new ArrayList();
	java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
    java.util.Date date = cal.getTime();
    java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance();
    SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");
    SimpleDateFormat fdia = new SimpleDateFormat("dd");
    SimpleDateFormat fmes = new SimpleDateFormat("MM");
    SimpleDateFormat fanio = new SimpleDateFormat("yyyy");	
	
	String dia=fdia.format((java.util.Calendar.getInstance(Locale.US)).getTime());
	String mes=fmes.format((java.util.Calendar.getInstance(Locale.US)).getTime());
    String anio=fanio.format((java.util.Calendar.getInstance(Locale.US)).getTime());
    String fechaHoy=dia+"/"+mes+"/"+anio;
	//System.out.println("1. identifica crea histor="+beanSession.usuario.getIdentificacion());

%>


<input type="hidden" id="txt_mes_reporte"  value="<%=mes%>"/>
<table width="1000px"     align="center"   border="0" cellspacing="0" cellpadding="1"   >
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="EVENTO ADVERSO REPORTADOS" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" border="1" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td>

 	 <div style="overflow:auto;height=350px"   id="divContenido"   >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
     <!--******************************** inicio de la tabla con los datos de busqueda ***************************-->	
	  <div id="divBuscar"  > 
              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="cursor:pointer" >
                <tr><td width="99%" class="titulosCentrados">BUSCAR</td></tr>
             </table>            
			<table width="100%" border="0"  cellpadding="0" cellspacing="0"  align="center">
                            <tr class="titulos" align="center">
							  <td width="30%">Proceso (Responsable)</td>
							  <td width="15%">Clase</td>                              
							  <td width="35%">eventos adversos del proceso</td>
							  <td width="20%">Area donde se presenta el evento</td>							  
							</tr>
							<tr class="estiloImput">
							  <td ><select name="cmbBusproceso" size="1" id="cmbBusproceso" style="width:90%"  tabindex="14" >	                                        
                                        <option value="00"></option>
										  <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.proceso.CargarTodosLosProceso();	
												 ProcesoVO proceso;
												 for(int k=0;k<resultaux.size();k++){ 
													   proceso=(ProcesoVO)resultaux.get(k);
										  %>
										<option value="<%= proceso.getCodigo()%>"><%=proceso.getNombre()%></option>
												<%}%>						
 	                             </select>	                   
							  </td>
                              <td ><select name="cmbBusClaseEA" size="1" id="cmbBusClaseEA" style="width:80%"  tabindex="14"  onchange="
												cargarEAHospitalConClase('cmbBusproceso','00','cmbBusEventoProceso',this.options[this.selectedIndex].value,'<%= response.encodeURL("/clinica/paginas/accionesXml/cargarEventosAdversosClase_xml.jsp") %>');
									">	    
                                    <option value="00">[Todas las clases]</option>
										  <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.claseEA.CargarClaseEA();	
												 ClaseEAVO p22;
												 for(int k=0;k<resultaux.size();k++){ 
													   p22=(ClaseEAVO)resultaux.get(k);
										  %>
										<option value="<%= p22.getCodigo()%>"><%= p22.getNombre()%></option>
												<%}%>						
 	                             </select>	
                                </td>                              
							  <td ><select name="cmbBusEventoProceso" size="1" id="cmbBusEventoProceso" style="width:99%"  tabindex="1" >
							    <option value="00"></option>
						      </select></td>
							  <td ><select name="cmbBusUbicacionArea" size="1" id="cmbBusUbicacionArea" style="width:80%"  tabindex="1" >
							    <option value="00">[TODAS LAS AREAS]</option>
							    <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.cargarUbicacionArea();	
												 UbicacionAreaVO ubicArea;
												 for(int k=0;k<resultaux.size();k++){ 
													   ubicArea=(UbicacionAreaVO)resultaux.get(k);
										  %>
							    <option value="<%= ubicArea.getCodigo()%>"><%= ubicArea.getNombre()%></option>
							    <%}%>
							    </select>
                              </td>                
							
				  </tr>
						    <tr class="titulos"> 
                              <td colspan="1">Fecha desde&nbsp;&nbsp;&nbsp;&nbsp; Fecha hasta</td>	                   
							  <td colspan="1">Estado&nbsp;</td>
							  <td colspan="1">Identificaci&oacute;n o Nombre paciente</td>                                
							  <td colspan="1">&nbsp;&nbsp;&nbsp; Id No evento</td>          
							</tr>								
							<tr class="estiloImput">   
                              <td colspan="1"><input type="text" value="<%=fechaHoy%>"  size="15"  maxlength="10"  id="txtBusFechaDesde" name="txtBusFechaDesde" tabindex="14" />
                              <input type="text" value=""  size="15"  maxlength="10"  id="txtBusFechaHasta" name="txtBusFechaHasta" tabindex="14" /></td>	                   
							  <td colspan="1"><select name="cmbBusEstadoEvento" size="1" id="cmbBusEstadoEvento" style="width:90%"  tabindex="1" >
							    <option value="00">[TODOS]</option>
							    <% resultaux.clear();
                                               resultaux=(ArrayList)beanAdmin.cargarEstadoEvento("ISOGCALEVE");	//subtipos plan de accion: preventivo correctivo mejora
                                               NoConformidadVO isoEstBusEvent;
                                               for(int k=0;k<resultaux.size();k++){ 
                                                    isoEstBusEvent=(NoConformidadVO)resultaux.get(k);
                                            %>
							    <option value="<%= isoEstBusEvent.getIsoIdTipoISOEvento()%>"><%= isoEstBusEvent.getIsoDescrTipoISOEvento()%></option>
							    <%}%>
						      </select></td>  
                              <td><input type="text" id="txtBusIdPaciente2" onfocus="llamarAutocomIdDescripcion('txtBusIdPaciente2',15)" value="" size="50"  maxlength="50"   tabindex="14" /> </td> 
							  <td><input type="text" value=""  size="19"  maxlength="20"  id="txtBusIdEvento" name="txtBusIdEvento" onfocus="limpiarFiltros()" tabindex="14" /></td> 
                              </td>         							
                            </tr>	
			</table>
			<div id="divListadoEventosA" style="height:500px; width:100%" >
				 <table id="listadoEventoA" class="scroll"></table>  
				<div id="pager" class="scroll" style="text-align:center;"></div>
			</div>            
			
	  </div>
		  
	  <div id="divEditar"  style="display:none;"  > 
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="cursor:pointer" >
                        <tr><td width="99%" class="titulosCentrados">EDITAR</td></tr>
                     </table>
                           <div style="display:none">
                            <table>
                             <tr>
							  <td><select name="cmbproceso" size="1" id="cmbproceso" style="width:90%"  tabindex="14" >	                                        
                                        <option value="HH03"></option> 	                             </select>	                   
							  </td>
                             </tr>
                            </table>  
                           </div>
				 <table width="100%" border="0"  cellpadding="0" cellspacing="0"  align="center">
                            <tr class="titulos" align="center">
							  <td colspan="2">Paciente</td>    
							  <td colspan="1">Unidad a la que pertenece</td>                                  
							  <td >Clasificaci�n del Evento</td>                                                        
							</tr>   
                            <tr class="estiloInput">                                
                              <td colspan="2" align="center"><input type="text" size="110"  maxlength="210"  id="txtBusIdPaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtBusIdPaciente',307)"  style="width:70%"  tabindex="99" />  
                              </td>
                              <td align="center"><select size="1" id="cmbUnidadPertenece" style="width:80%" title="20" tabindex="14" >	                                        
                                        <option value="00"></option>
                                          <%     resultaux.clear();
                                                 resultaux=(ArrayList)beanAdmin.combo.cargar(28);	
                                                 ComboVO cmb0;
                                                 for(int k=0;k<resultaux.size();k++){ 
                                                       cmb0=(ComboVO)resultaux.get(k);
                                          %>
                                        <option value="<%= cmb0.getId()%>" title="<%= cmb0.getTitle()%>"><%= cmb0.getDescripcion()%></option>
                                                <%}%>						
                                 </select>	                   
                              </td>                              
							  <td align="center"><select size="1" id="cmbClaseEA" style="width:50%"  tabindex="14" >	    
                                    <option value="00"></option>
                                    <option value="co">COMPLICACION</option>                                    
                                    <option value="ea">EVENTO ADVERSO</option>                                                                        
                                    <option value="in">INCIDENTE</option>
                                    <option value="nc">NO CONFORME</option>                                                                                                                                                
 	                             </select>	                   
							  </td>                                                                   
                            </tr> 
                            <tr class="titulos" align="center">
							  <td colspan="3" title="Escoja el tipo de evento a reportar">Listado de Eventos</td>
							  <td width="35%">Area donde se presenta el evento</td>							  
							</tr>
							<tr class="estiloImput">
							  <td colspan="3" >
                              <select name="cmbEAProceso" size="1" id="cmbEAProceso" style="width:90%"  tabindex="1" >
							    <option value="00">[Todos los eventos]</option>
							    <option value="00">DESPACHO ERRONEO</option>
							    <option value="00">SOLICITUD INCORRECTA</option>                                
							    <option value="00">CAIDA</option>                                                                
						      </select></td>
							  <td ><select name="cmbUbicacionArea" size="1" id="cmbUbicacionArea" style="width:70%"  tabindex="1" >
							    <option value="00">COCINA</option>
							    <option value="00">BA�O</option>                                
							    <option value="00">PASILLO</option>                                                                
							    <option value="00">PATIO</option>                                                                                                
						      </select>
                              </td>      
				  </tr>
                  <tr class="titulos"> 
                    <td colspan="3" title="Describa el evento, recuerde dar informaci�n real">Descripci�n del Evento</td>	                   
                    <td colspan="1" align="left">
                        <table  width="100%" border="1">
                           <tr align="center">
                              <td width="100">Fecha que suced�o</td>
                              <td width="70" colspan="2">Hora</td>
                           </tr> 
                        </table>  
                    </td>
                  </tr>								
                  <tr class="estiloImput">   
                    <td colspan="3"> <label  id="lblIdEvento">&nbsp;</label><input type="hidden"  id="lblElaboro"/>
                    <textarea   value=""  cols="75" rows="2"  size="900"  maxlength="900"  id="txtDescripEvento" tabindex="14" /></textarea></td>	                   
                    <td colspan="1" align="center">
                    
                    <table width="100%">
                       <tr class="estiloImput">
                          <td align="left"><input type="text" value=""  size="10"  maxlength="10"  id="txtFechaEvento" tabindex="14" /></td>
                          <td colspan="2" align="left">
                               <select name="cmbHoraEvent" size="1" id="cmbHoraEvent" tabindex="1" >
                                        <option value="--">&nbsp;&nbsp;</option>
                                        <option value="00">00 am</option>
										<option value="01">01 am</option>
										<option value="02">02 am</option>
										<option value="03">03 am</option>
										<option value="04">04 am</option>
										<option value="05">05 am</option>
										<option value="06">06 am</option>
										<option value="07">07 am</option>   
										<option value="08">08 am</option>
										<option value="09">09 am</option>
										<option value="10">10 am</option>
										<option value="11">11 am</option>
										<option value="12">12 pm</option>
										<option value="13">01 pm</option>
										<option value="14">02 pm</option>    
										<option value="15">03 pm</option>   
										<option value="16">04 pm</option>
										<option value="17">05 pm</option>
										<option value="18">06 pm</option>
										<option value="19">07 pm</option>
										<option value="20">08 pm</option>
										<option value="21">09 pm</option>
										<option value="22">10 pm</option>  
										<option value="23">11 pm</option>      
   	                         </select> 
                          </td>
                       </tr> 
                    </table>
                    <input type="hidden" value=""   id="txtEstadoEvent" name="txtEstadoEvent"/>                                        
                    </td>                                                    
                  </tr>	
                  <tr class="titulos"> 
                    <td colspan="4" title="Describa la actividad que usted realiz� en el momento de ocurrir el evento,                       Ejemplo: Llam� al jefe de turno y ayud� a acostar al paciente"> Tratamiento &nbsp;&nbsp;&nbsp; (Lo que se hizo ante el evento)</td>	                   
                  </tr>		
                  <tr class="estiloImput">   
                    <td colspan="4"><label  id="lblIdISORef">&nbsp;</label>
                    <textarea   value="" cols="90" rows="2"  size="900"    maxlength="900"  id="txtTratamiento" name="txtTratamiento" tabindex="14" ></textarea></td>	                   
				  </tr>					
                  <tr>
                    <td colspan="4">
                     <div id="divEditarFases" style="display:none; width:100%; "  >   
                         <table width="100%">
                              <tr class="subTitulos"> 
                                <td colspan="4">CALIDAD - JEFE DE PROCESO</td>	                   
                              </tr>	
                              <tr class="titulos"> 
                                <td colspan="3" width="70%">Concepto a la no conformidad</td>	
                                <td colspan="1">Persona destinatario</td>                                                   
                              </tr>								
                              <tr class="estiloImput">   
                                <td colspan="3">
                                <textarea type="text" value="" id="txtConcepNoConforCalidad" name="txtConcepNoConforCalidad"  style="width:100%"  size="120"  maxlength="200"  tabindex="14" /></textarea>
                                </td>	   
                                <td colspan="1">
                                <select name="cmbResponsaDestinoEvento"  id="cmbResponsaDestinoEvento" style="width:90%"  tabindex="1" >
							    <option value="00">[TODAS LAS PERSONAS]</option>
							    <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.cargarJefeDeProceso();	
												 NoConformidadVO persDest;
												 for(int k=0;k<resultaux.size();k++){ 
													   persDest=(NoConformidadVO)resultaux.get(k);
								%>
							    <option value="<%= persDest.getIsoResponsable()%>"><%= persDest.getIsoResponsableNombre()%></option>
							    <%}%>
							    </select>
                                </td>                                                                                
                              </tr>	
                              <tr class="titulos"> 
                                <td colspan="1">Tipo evento</td>
                                <td colspan="1" title="Evento Padre a quien se le asoci� heredando su plan de acci�n">Relacionado con Evento</td>                                
                                <td colspan="2">Estado</td>                                                   
                              </tr>	
                               <tr class="estiloImput">
                                <td colspan="1"><center><input  type="radio" name="rbtImpactoNoImpacto" id="rbtImpacto" onchange="activarPlanDeaccion(this.id)"  />&nbsp;Impacto&nbsp;&nbsp;&nbsp;<input  type="radio" name="rbtImpactoNoImpacto" id="rbtNoImpacto" checked="checked"  onchange="activarPlanDeaccion(this.id)"/>&nbsp;No Impacto</center>
                                <td colspan="1"><label  id="lblEventoPadreAsocia">&nbsp;</label></td>
                                <td colspan="2"><label  id="lblEstadoEvent">&nbsp;</label></td>
                              </tr>	
                              <tr>
                                <td colspan="4">
                                 <div id="divEditarPlanDeAccion" style="display:none; width:99%;" >
                                  <div id="tabsEventos">
                                      <ul>    
                    	               <li><a href="#divDatosPlanAccion"  title="Datos PLAN DE ACCION"><center>Plan de acci�n</center></a></li> 
                    	               <li><a href="#divDatosPersonalizarTareas" onclick="presentarPersonasPlanDeAccion()"    title="Personas delegadas para cumplir tareas del Plan de acci�n"><center>Personas Tareas</center></a></li>                                                                              
                                       <li><a href="#divDatoAsociarEventos" onclick="presentarEventosAsociarReportar()"   title="Relacionar al plan de acci�n otras no conformidades"><center>Relacionar no conformidades</center></a></li>
                                       <li><a href="#divSeguimiento" onclick="listadoTabla('listPlanAccionSeguimiento');presentarCumplimientoMeta();"   title="Seguimiento al plan de acci�n"><center>Seguimiento</center></a></li>                                     
                                      </ul>
                                     <div id="divDatosPlanAccion"> 
                                      <table  width="100%" border="0" cellpadding="1" cellspacing="1" >
                                       <tr class="titulos"> 
                                            <td colspan="1"  >Id Doc</td>	
                                            <td colspan="1"  >Tipo</td>	
                                            <td colspan="3"  >An�lisis de causas</td>
                                       </tr>                                       
                                       <tr class="estiloImput">
                                            <td colspan="1"><label  id="lblIdEventoDoc"></label></td>	 
                                            <td colspan="1">
                                            <select name="cmbTipoPlanAccion" size="1" id="cmbTipoPlanAccion" style="width:50"  tabindex="1" >
                                             <option value="00">[Escoja tipo plan acci�n]</option>
                                            <%    resultaux.clear();
                                                             resultaux=(ArrayList)beanAdmin.cargarSubTipoEvento("ISOGCALDOC");	//subtipos plan de accion: preventivo correctivo mejora
                                                             NoConformidadVO isoSubTipoEvento;
                                                             for(int k=0;k<resultaux.size();k++){ 
                                                                   isoSubTipoEvento=(NoConformidadVO)resultaux.get(k);
                                            %>
                                             <option value="<%= isoSubTipoEvento.getIsoIdTipoISOEvento()%>"><%= isoSubTipoEvento.getIsoDescrTipoISOEvento()%></option>
                                            <%}%>
                                            </select>                                
                                            </td>   
                                            <td colspan="4"  width="100%"><textarea maxlength="200" type="text" value="" id="txtAnalisisCausas" name="txtAnalisisCausas"  style="width:90%"  size="120"  maxlength="120"   tabindex="14" /></textarea></td>
                                        </tr>  
                                       <tr class="titulos"> 
                                            <td colspan="1"  >Fecha ini</td>	
                                            <td colspan="4"  >Meta Plan de acci�n</td>
                                       </tr>    
                                       <tr class="estiloImput"> 
                                          <td colspan="1"><input type="text" value=""  size="11"  maxlength="10"  id="txtFechaIniPlanAccion" name="txtFechaIniPlanAccion"  tabindex="14" />                                          </td>
                                          <td colspan="4"><input type="text" value="" id="txtMetaPlanAccion" name="txtMetaPlanAccion"  style="width:90%"  size="120"  maxlength="120"   tabindex="14" /></td>                                                                          
                                       </tr>     
                                                                                                     
                                
                                            </table> 
                                               <div id="divdatosbasicoscredesa" style="overflow:scroll;  height:180px; width:100%">
                                                       <table id="listPlanAccion" class="scroll" cellpadding="0" cellspacing="0"></table>
                                                      <!-- <div id="pagerlistPlanAccion" class="scroll" style="overflow:scroll; text-align:center; " >  </div>              -->                                
                                               </div>   

                                     </div><!--fin divDatosPlanAccion--> 
                                     <div id="divDatosPersonalizarTareas">                                        
                                        <table width="90%" >
                                          <tr class="titulos">
                                            <td colspan="2">&nbsp;Personas responsables de las tareas del Plan de acci�n (Quien)</td>
                                          </tr>
                                          <tr>
                                            <td colspan="2" >
                                               <div id="divdatosPersonasPlanAccion" style="overflow:scroll;   height:310px; width:100%">
                                                   <table align="center" id="listPersonasPlanAccion" class="scroll" cellpadding="0" cellspacing="0"></table>
                                               </div>   
                                               <div id="divBuscar4" style="width:100%" >  

                                               </div>  
                                               <div id="divBuscar5" style="width:100%" >  
                                                <table width="100%" >
                                                  <tr class="estiloImput">
                                                    <td width="100%">Esta No Conformidad est� asociada a otra no conformidad (Evento Relacionado <label  id="lblEventoPadreAsocia">&nbsp;</label>), por ello no se permite asociar personas. </td>
                                                  </tr>
                                                </table>
                                               </div>
                                            </td>    
                                          </tr>                                          
                                          
                                        </table>                                     
                                     </div><!-- fin divDatosPersonalizarTareas-->  
                                     <div id="divDatoAsociarEventos"> 
                                       <table width="100%">
                                          <tr>
                                            <td>        
                                                      
                                                      <div id="divdatosbasicoscredesa2"  style="width:100%; height:220px;">
                                                           <table id="listAsociarEventos" class="scroll" cellpadding="0" cellspacing="0"></table>
                                                      <div id="divBuscar2" style="width:100%; display:none" >  

                                                      </div>  
                                                      <div id="divBuscar3" style="width:100%; display:none" >  
                                                       <table width="100%" >
                                                          <tr class="estiloImput">
                                                            <td width="100%">Esta No Conformidad est� asociada a otra no conformidad (Evento Relacionado <label  id="lblEventoPadreAsocia">&nbsp;</label>), por ello no se le permite asociar. </td>
                                                          </tr>
                                                        </table>
                                                      </div>  
                                                </div>                                                
                                            </td>
                                          </tr>
                                        </table>                                     
                                     </div><!-- fin divDatoAsociarEventos--> 
                                     
                                     <div id="divSeguimiento"> 
                                      <table  width="100%" border="0" cellpadding="1" cellspacing="1" > 
                                       <tr class="titulos"> 
                                            <td colspan="1"  >Fecha ini</td>	
                                            <td colspan="4"  >Meta Plan de acci�n</td>
                                       </tr>    
                                       <tr class="estiloImput"> 
                                          <td colspan="1"><label  id="lblFechaIniPlanAccion">&nbsp;</label></td>
                                          <td colspan="4"><label  id="lblMetaPlanAccion">&nbsp;</label></td>                                                                          
                                       </tr>    
                                       <tr class="titulos"> 
                                            <td colspan="1"  >Eficaz</td>	
                                            <td colspan="4"  >Sequimiento a la meta</td>
                                       </tr>    
                                       <tr class="estiloImput"> 
                                          <td colspan="1"><select name="cmbEficaz" id="cmbEficaz"  style=" width:60%" >
                                                           <option value="00">&nbsp;&nbsp;</option>
                                                           <option value="SEG_1">Si</option>
                                                           <option value="SEG_2">No</option>                                                           
                                                          </select>  
                                          </td>                
                                          <td colspan="4"><label  id="lblIdEventoDocSeg"></label>&nbsp;&nbsp;<input type="text" value="" id="txtSeguimMetaPlanAccion" name="txtSeguimMetaPlanAccion"  style="width:80%"  size="120"  maxlength="120"   tabindex="14" /></td>                                                                          
                                       </tr>     
 
                                                                             	
                                       <tr class="titulos" height="13">
                                           <td width="10%">Cumple</td>
                                           <td width="35%">C�mo realizar la acci�n o tarea</td> 
                                           <td width="35%" colspan="2">Seguimiento a la Acci�n</td>                                
                                           <td width="10%">&nbsp;</td>                                                                                                                                                                                                             
                                       </tr>
                                       <tr class="estiloImput" height="7">
                                                       <td><select name="cmbSeguimietoCumple" id="cmbSeguimietoCumple"  style=" width:80%" >
                                                           <option value="00">Escoja</option>
                                                           <option value="Si">Si</option>
                                                           <option value="No">No</option>                                                           
                                                          </select>                               
                                                        </td>
                                                        <td ><textarea id="txtRealizarAcciones" disabled="disabled" name="txtRealizarAcciones" maxlength="285" cols="2"  style="width:99%" ></textarea>                                            
                                                        <td colspan="2">
                                                          <textarea name="txtSeguimiAcciones" maxlength="285" cols="2" id="txtSeguimiAcciones" style="width:99%" ></textarea>
                                                        </td>  
                                                        <td>
                                                              <center>
                                                                    <a href="#" onclick="modifiDatosListasSTGral('listPlanAccionSeguimiento');;" title="Modificar"><img src="/clinica/utilidades/imagenes/acciones/modificar.png" height="18" id="imgUpd" /></a>&nbsp;&nbsp;
                                                              </center>
                                                        </td>                                             
            
                                                       
                                                 </tr>                                                                                                      
                                
                                            </table> 
                                               <div id="divdatosPlanSeguimiento" style="overflow:scroll;  height:180px; width:100%">
                                                       <table id="listPlanAccionSeguimiento" class="scroll" cellpadding="0" cellspacing="0"></table>
                                                      <!-- <div id="pagerlistPlanAccion" class="scroll" style="overflow:scroll; text-align:center; " >  </div>              -->                                
                                               </div>   

                                     </div><!--fin divSeguimiento--> 
                                  
                                  </div>
                                 </div>  <!-- fin divEditarPlanDeAccion-->                  
                                </td>   
                              </tr>                                  
                        </table>  
                     </div>
                    </td>   
                  </tr>    		
								  
			</table>

      </div><!-- divEditar-->	  
	 <!--********************** fin de la tabla con los datos del formulario ***********************************************-->
    </div><!-- div contenido-->
        </td>
       </tr>
      </table>
    
   </td>
 </tr> 
</table>
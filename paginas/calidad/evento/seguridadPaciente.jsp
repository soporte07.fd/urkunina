 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<div id="tabsPrincipalAnalisis" style="width:98%; height:450px">
  <ul>
    <li><a href="#divPaciente" onclick="TabActivoEventos('divPaciente')"><center>PACIENTE</center></a></li>
    <li><a href="#divClasificarEA" onclick="TabActivoEventos('divClasificarEA')"><center>CLASIFICAR</center></a></li>    
    <li><a href="#divAnalisisEA" onclick="TabActivoEventos('divAnalisisEA')"><center>ANALISIS</center></a></li>                    
    <li><a href="#divEspinaPescado" onclick="TabActivoEventos('divEspinaPescado')"><center>ESPINA DE PESCADO</center></a></li>            
    <li><a href="#divPlanAccion" onclick="TabActivoEventos('divPlanAccion')"><center>PLAN ACCION</center></a></li>            
  </ul> 
  
  <div id="divPaciente" style="width:99%"> 
   <jsp:include page="paciente.jsp" flush="true"  />             
  </div> 
  
 <div id="divClasificarEA" style="width:99%"> 
      <table width="100%">
            <tr class="titulos"> 
              <td width="5%">Id</td> 
              <td width="13%">Clase</td> 
              <td width="12%">Tipo Evento</td>
              <td width="15%">Servicio donde ocurri&oacute;</td>
              <td width="30%">Cronolog&iacute;a</td>   
              <td width="25%">Recomendaci&oacute;n</td>                 
            </tr>		
            <tr class="estiloImput"> 
              <td><label id="lblClasifiEA"></label></td>             
              <td>
                   <select  size="1" id="cmbBusClaseEA" style="width:99%"  tabindex="14"  onchange="cargarEAHospitalConClase('cmbBusproceso','00','cmbBusEventoProceso',this.options[this.selectedIndex].value,'')">	    
                    <option value="1">EVENTO ADVERSO</option>
                    <option value="2">INCIDENTE</option>
                    <option value="3">COMPLICACION</option>                                        
                   </select>	                  
              </td>     
              <td>
                   <select size="1" id="cmbTipoEA" style="width:98%" tabindex="201" title="53" >	                                        
                      <option value=""></option>
                      <option value="1">AUTO MEDICACION</option>
                      <option value="2">CAIDA PACIENTE</option>
                      <option value="3">RAM</option>
                      <option value="4">FUGA DE PACIENTE</option>                      
                   </select>                       
              </td> 
              <td>
                    <select size="1" id="cmbIdTipoServicio" style="width:95%" title="76" tabindex="14" >
                            <option value=""></option>                    
                            <% resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(301);	 /*13*/
                               ComboVO cmbt; 
                               for(int k=0;k<resultaux.size();k++){ 
                                   cmbt=(ComboVO)resultaux.get(k);
                            %>
                           <option value="<%= cmbt.getId()%>" title="<%= cmbt.getTitle()%>"><%= cmbt.getDescripcion()%></option>
                            <%}%>                    
                    </select>                            
              </td>                                   
              <td  width="35%" title="Escriba la cronologia"><textarea id="txtCronologia" rows="5"  style="width:95%" tabindex="20" maxlength="2000"></textarea></td>	                                                      
              <td>           	
                <textarea type="text" rows="5"  id="txtRecomendacion" style="width:90%"  size="500"  maxlength="500"  tabindex="200"></textarea>
              </td>
           </tr>                                                                            
            <tr>   
              <td colspan="3" align="center">
                 <input id="btn_crea" title="B7EY" class="small button blue" type="button"  value="Eliminar"  onclick="modificarCRUD('eliminarClasificarEA')">             
              </td> 
              <td colspan="3" align="center">
                 <input id="btn_crea" title="B7EP" class="small button blue" type="button"  value="Adicionar"  onclick="modificarCRUD('listClasificarEA')">             
              </td> 
              
            </tr>                    
            <tr>   
              <td colspan="6">
                 <table align="center" id="listClasificarEA" class="scroll"></table>
              </td> 
            </tr>            
      </table> 
     
 </div> 

 <div id="divAnalisisEA" style="width:99%"> 
   <table width="100%">     
        <tr class="titulos"> 
           <td width="5%">Id</td>	                        
           <td width="35%" title="Escriba la accion insegura">Acci&oacute;n Insegura</td>	                
           <td width="30%" title="Se refiere a la identificación de los factores que contribuyeron a la presencia del evento (Pacientes, Tareas y tecnología, Individuo, Equipo, Ambiente)">Factores Contributivos</td>	        
        </tr>            
        <tr class="titulos"> 
           <td><label id="lblIdAnalisisEA"></label></td>	                                
          <td>
           <textarea id="txtAccionInsegura" rows="4"  style="width:95%" tabindex="20" maxlength="2000"></textarea>
          </td>	                
          <td>
            <select id="cmbFactContributivo" style="width:60%"  tabindex="1">
                    <option value=""></option>                    
                    <% resultaux.clear();
                       resultaux=(ArrayList)beanAdmin.combo.cargar(526);	 /*13*/
                       ComboVO cmb22; 
                       for(int k=0;k<resultaux.size();k++){ 
                           cmb22=(ComboVO)resultaux.get(k);
                    %>
                   <option value="<%= cmb22.getId()%>" title="<%= cmb22.getTitle()%>"><%= cmb22.getDescripcion()%></option>
                    <%}%> 
             </select> 
          </td>
        </tr> 
        <tr>   
          <td colspan="3" align="center">
             <input id="btn_crea" title="BTEY" class="small button blue" type="button"  value="Adicionar"  onclick="modificarCRUD('listAnalisisEA');">             
             <input id="btn_crea" title="BTEY" class="small button blue" type="button"  value="Eliminar"  onclick="modificarCRUD('eliminaAnalisisEA');">                          
          </td> 
        </tr>                    
        <tr>   
          <td colspan="3">
             <table align="center" id="listAnalisisEA" class="scroll"></table>
          </td> 
        </tr>                    
   </table>       
 </div> 

 <div id="divEspinaPescado"> 
    <table  width="100%" border="0" cellpadding="1" cellspacing="1" >
     <tr class="titulos" height="13">
         <td width="10%">Califica</td>     
         <td width="15%">Factor</td>
         <td width="25%">Criterios de causa</td>                                                 
         <td width="35%">An&aacute;lisis / Justificaci&oacute;n</td>
         <td width="15%">&nbsp;</td>                                                                                                                                                                                                             
     </tr>
     <tr class="estiloImput" height="7">
       <td>
          <label id="lblIdEspinaPezEA"></label>
          <select id="cmbCalifica" style=" width:70%" >
            <option value=""></option>    
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>                        
          </select>                                
        </td>     
       <td>
          <select id="cmbFactor" style=" width:87%" onchange="cargarCriterioFactorEA('cmbCriterioCausa','cmbFactor')" >
            <option value=""></option>    
            <option value="1">Mano de obra</option>
            <option value="2">M&eacute;todo</option>
            <option value="3">M&aacute;quina</option>
            <option value="4">Moneda</option>
            <option value="5">Medio ambiente</option>
            <option value="6">Materiales</option>
            <option value="7">Medida</option>
            <option value="8">Management (Pol&iacute;ticas)</option>   
            <option value="9">No aplica</option>                                                                                                
          </select>                                
        </td>
        <td>
            <select id="cmbCriterioCausa"  style=" width:99%" >
             <option value=""></option>
            </select>
        </td>                                              
        <td ><textarea id="txtAnalisisFactor"  maxlength="285" cols="2" style="width:99%" ></textarea>                                            

        <td><input id="btn_crea" title="B76Y" class="small button blue" type="button"  value="Adiciona"  onclick="modificarCRUD('adicionarEspinaPescado','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">    
            <input id="btn_crea" title="B77Y" class="small button blue" type="button"  value="Eliminar"  onclick="modificarCRUD('eliminarEspinaPescado','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                                                                  
        </td>                                                            
     </tr>                                                                                                      

   </table> 
   <div id="divdatosbasicoscredesa" style="overflow:scroll;  height:150px; width:100%">
           <table id="listEspinaPescado" class="scroll" cellpadding="0" cellspacing="0"></table>
   </div>  
</div><!--fin divEspinaPescado--> 
                                         
 <div id="divPlanAccion" style="width:99%">              
   <jsp:include page="planAccion.jsp" flush="true"  />             
 </div>



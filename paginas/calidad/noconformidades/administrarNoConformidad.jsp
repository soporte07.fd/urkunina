 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
//    beanPersonal.setCn(beanSession.getCn());
    ArrayList resultaux=new ArrayList();
	java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
    java.util.Date date = cal.getTime();
    java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance();
    SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");

%>



<table width="1070px"  align="center"   border="0" cellspacing="0" cellpadding="1"   >
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="ADMINISTRAR NO CONFORMIDADES " />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td>

 	 <div style="overflow:auto;height=550px"   id="divContenido"   >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
        <!--******************************** inicio de la tabla con los datos de busqueda ***************************-->	
          <div id="divBuscar"  > 
              <table width="100%" border="0"  cellpadding="0" cellspacing="0"  align="center">
                              <tr class="titulos" align="center">
                                <td width="25%">Proceso(Destino)</td>
                                <td colspan="2">no conformidad del proceso</td>
                                <td width="31%">Area donde se presenta el evento</td>							  
                              </tr>
                              <tr class="estiloImput">
                                <td ><select name="cmbBusproceso" size="1" id="cmbBusproceso" style="width:90%"  tabindex="14" onchange="
                                                  cargarNoConformidades('00','cmbBusNoConformidades',this.options[this.selectedIndex].value,'<%= response.encodeURL("/clinica/paginas/accionesXml/cargarNoConformidades_xml.jsp") %>');
                                                          
                                      ">	                                        
                                          <option value="00">[TODOS LOS PROCESOS]</option>
                                            <%    resultaux.clear();
                                                   resultaux=(ArrayList)beanAdmin.CargarProceso();	
                                                   ProcesoVO proceso;
                                                   for(int k=0;k<resultaux.size();k++){ 
                                                         proceso=(ProcesoVO)resultaux.get(k);
                                            %>
                                          <option value="<%= proceso.getCodigo()%>"><%= proceso.getNombre()%></option>
                                                  <%}%>						
                                   </select>	                   
                                </td>
                                <td colspan="2" ><select name="cmbBusNoConformidades" size="1" id="cmbBusNoConformidades" style="width:90%"  tabindex="1" >
                                  <option value="00">[TODAS LAS NO CONFORMIDADES]</option>
                                </select></td>
                                <td ><select name="cmbBusUbicacionArea" size="1" id="cmbBusUbicacionArea" style="width:90%"  tabindex="1" >
                                  <option value="00">[TODAS LAS AREAS]</option>
                                  <%    resultaux.clear();
                                                   resultaux=(ArrayList)beanAdmin.cargarUbicacionArea();	
                                                   UbicacionAreaVO ubicArea;
                                                   for(int k=0;k<resultaux.size();k++){ 
                                                         ubicArea=(UbicacionAreaVO)resultaux.get(k);
                                            %>
                                  <option value="<%= ubicArea.getCodigo()%>"><%= ubicArea.getNombre()%></option>
                                  <%}%>
                                  </select>
                                </td>                
                              
                    </tr>
                              <tr class="titulos"> 
                                <td colspan="1">
                                      <table width="100%">
                                       <tr>
                                        <td align="center">Fecha Desde</td>                                                                                                                                   
                                        <td align="center">Fecha hasta</td>  
                                       </tr>
                                     </table>                                                      
                                </td>	                   
                                <td width="19%" colspan="1">Estado</td>                              
                                <td colspan="1">Con plan de acci�n</td>                                                            
                                <td width="25%" colspan="1">Id No conformidad</td>                                    
                              </tr>								
                              <tr class="estiloImput">   
                                <td colspan="1">
                                      <table >
                                       <tr>
                                        <td ><input type="text" value=""  size="15"  maxlength="10"  id="txtBusFechaDesde" name="txtBusFechaDesde" tabindex="14" /></td>                                                                                                                                   
                                        <td><input type="text" value=""  size="15"  maxlength="10"  id="txtBusFechaHasta" name="txtBusFechaHasta" tabindex="14" /></td>  
                                       </tr>
                                     </table>  
                                </td>	                   
                                                  
                                <td colspan="1"><select name="cmbBusEstadoEvento" size="1" id="cmbBusEstadoEvento" style="width:80%"  tabindex="1" >
                                  <option value="00">[TODOS]</option>
                                              <% resultaux.clear();
                                                 resultaux=(ArrayList)beanAdmin.cargarEstadoEvento("ISOGCALEVE");	//subtipos plan de accion: preventivo correctivo mejora
                                                 NoConformidadVO isoEstBusEvent;
                                                 for(int k=0;k<resultaux.size();k++){ 
                                                      isoEstBusEvent=(NoConformidadVO)resultaux.get(k);
                                              %>
                                               <option value="<%= isoEstBusEvent.getIsoIdTipoISOEvento()%>"><%= isoEstBusEvent.getIsoDescrTipoISOEvento()%></option>
                                  <%}%>
                                  </select>                               </td> 
                                <td><input id="chk_solo_plan"  name="chk_solo_plan" type="checkbox"  value="" /> </td>  
                                 <td ><center>
                                      <table >
                                      <tr>
                                      <td colspan="1"><input type="text" value=""  size="20"  maxlength="20"  id="txtBusIdEventoNoConf" name="txtBusIdEventoNoConf" onfocus="limpiarFiltros()" tabindex="14" /></td>                                                                                                                                   
                                      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      </td>
                                      </tr>
                                     </table> 
                                 </center>  
                                </td>                                  							
                              </tr>	
              </table>
              <div id="divListado" align="center" >
                  <table id="list" class="scroll"></table>  
                  <div id="pager" class="scroll" style="text-align:center;"></div>
              </div>

          </div>              
        <div id="divEditar" style="display:none; ">   
                     <table width="100%" border="0"  cellpadding="0" cellspacing="0"  align="center">
                                <tr class="titulos" align="center">
                                  <td width="27%">Proceso</td>
                                  <td colspan="2">no conformidad del proceso</td>
                                  <td width="35%">Area donde se presenta el evento</td>							  
                                </tr>
                                <tr class="estiloImput">
                                  <td ><select name="cmbproceso" size="1" id="cmbproceso" style="width:90%"  tabindex="14" onchange="
                                                    cargarNoConformidades('00','cmbNoConformidades',this.options[this.selectedIndex].value,'<%= response.encodeURL("/clinica/paginas/accionesXml/cargarNoConformidadesAdm_xml.jsp") %>');
                                                            
                                        ">	                                        
                                            <option value="00">[TODOS LOS PROCESOS]</option>
                                              <%    resultaux.clear();
                                                     resultaux=(ArrayList)beanAdmin.CargarProceso();	
                                                     ProcesoVO procesoEdit;
                                                     for(int k=0;k<resultaux.size();k++){ 
                                                           procesoEdit=(ProcesoVO)resultaux.get(k);
                                              %>
                                            <option value="<%= procesoEdit.getCodigo()%>"><%= procesoEdit.getNombre()%></option>
                                                    <%}%>						
                                     </select>	                   
                                  </td>
                                  <td colspan="2" ><select name="cmbNoConformidades" size="1" id="cmbNoConformidades" style="width:90%"  tabindex="1" >
                                    <option value="00" >[TODAS LAS NO CONFORMIDADES]</option>
                                    <%    resultaux.clear();
                                                     resultaux=(ArrayList)beanAdmin.cargarNoConformidadTodas();	
                                                     NoConformidadVO noConforEdit;
                                                     for(int k=0;k<resultaux.size();k++){ 
                                                           noConforEdit=(NoConformidadVO)resultaux.get(k);
                                              %>
                                    <option value="<%= noConforEdit.getIsoIdNoConformidad()%>"><%= noConforEdit.getIsoNoConformidadDescripcion()%></option>
                                    <%}%>
                                  </select></td>
                                  <td ><select name="cmbUbicacionArea" size="1" id="cmbUbicacionArea" disabled="disabled" style="width:90%"  tabindex="1" >
                                    <option value="00">[TODAS LAS AREAS]</option>
                                    <%    resultaux.clear();
                                                     resultaux=(ArrayList)beanAdmin.cargarUbicacionArea();	
                                                     UbicacionAreaVO ubicAreaEdit;
                                                     for(int k=0;k<resultaux.size();k++){ 
                                                           ubicAreaEdit=(UbicacionAreaVO)resultaux.get(k);
                                              %>
                                    <option value="<%= ubicAreaEdit.getCodigo()%>"><%= ubicAreaEdit.getNombre()%></option>
                                    <%}%>
                                    </select>
                                  </td>      
                      </tr>
                      <tr class="titulos"> 
                         <td colspan="3" width="50%">
                              <table width="100%">
                                <tr>
                                  <td width="14%">C�digo Evento</td>
                                  <td>Descripci�n del Evento</td>
                                </tr>
                              </table>                         
                         </td>
                         <td colspan="1" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hora&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;min</td>                      
                      </tr>								
                      <tr class="estiloImputIzq">   
                         <td colspan="3">
                          <table width="100%" >
                            <tr>
                              <td width="14%"><label  id="lblIdEvento">&nbsp;</label></td>
                              <td><label  id="lblDescripEvento">&nbsp;</label></td>
                            </tr>
                          </table>                         
                         </td>                          
                        <td  align="center">
                        <input type="text" value=""  size="11"  maxlength="10"  id="txtFechaEvento" name="txtFechaEvento" disabled="disabled" tabindex="14" />&nbsp;
                        <select name="cmbHoraEvent" size="1" id="cmbHoraEvent" style="width:60px"  tabindex="1" disabled="disabled">
                                            <option value="--">&nbsp;&nbsp;</option>
                                            <option value="00">00 am</option>
                                            <option value="01">01 am</option>
                                            <option value="02">02 am</option>
                                            <option value="03">03 am</option>
                                            <option value="04">04 am</option>
                                            <option value="05">05 am</option>
                                            <option value="06">06 am</option>
                                            <option value="07">07 am</option>   
                                            <option value="08">08 am</option>
                                            <option value="09">09 am</option>
                                            <option value="10">10 am</option>
                                            <option value="11">11 am</option>
                                            <option value="12">12 pm</option>
                                            <option value="13">01 pm</option>
                                            <option value="14">02 pm</option>    
                                            <option value="15">03 pm</option>   
                                            <option value="16">04 pm</option>
                                            <option value="17">05 pm</option>
                                            <option value="18">06 pm</option>
                                            <option value="19">07 pm</option>
                                            <option value="20">08 pm</option>
                                            <option value="21">09 pm</option>
                                            <option value="22">10 pm</option>  
                                            <option value="23">11 pm</option>
                         </select>
                        <input type="text" value=""  size="2"  maxlength="2"  id="txtMinutEvent" disabled="disabled" name="txtMinutEvent" tabindex="14" />                      
                        </td>                                                                       
                       
                      </tr>		
                      <tr class="titulos"> 
                         <td colspan="3" width="80%">
                              <table width="100%" >
                                <tr>
                                  <td width="14%">Cod Tratamiento</td>
                                  <td>Descripci�n del Tratamiento</td>
                                </tr>
                              </table>                         
                         </td>
                         <td colspan="1" align="center">Estado</td>                      
                      </tr>	
                      <tr class="estiloImputIzq">   
                         <td colspan="3">
                          <table width="100%">
                            <tr>
                              <td width="14%"><label  id="lblIdISORef">&nbsp;</label></td>
                              <td><label  id="lblDescripEventoPosTrata">&nbsp;</label></td>
                            </tr>
                          </table>                         
                         </td>
                          <td colspan="1" align="center"><label  id="lblEstadoEvent">&nbsp;</label></td>    
                      </tr>		            	                              
                      <tr>
                        <td colspan="4">
                         <div id="divEditarFases" style="display:none; width:100%; "  >   
                             <table width="100%">
                                  <tr class="subTitulos"> 
                                    <td colspan="4">CALIDAD - JEFE DE PROCESO</td>	                   
                                  </tr>	
                                  <tr class="titulos"> 
                                    <td colspan="3" width="70%">Concepto a la no conformidad</td>	
                                    <td colspan="1">Persona responsable</td>                                                   
                                  </tr>								
                                  <tr class="estiloImput">   
                                    <td colspan="3">
                                    <textarea type="text"  value="" id="txtConcepNoConforCalidad" name="txtConcepNoConforCalidad"  style="width:100%"  size="800"  maxlength="800"  tabindex="14" /></textarea>
                                    </td>	   
                                    <td colspan="1">
                                    <select name="cmbResponsaDestinoEvento"  id="cmbResponsaDestinoEvento" style="width:60%"  tabindex="1" >
                                    <option value="00">[Todos Los Jefes de Proceso]</option>
                                    <%    resultaux.clear();
                                                     resultaux=(ArrayList)beanAdmin.cargarJefeDeProceso();	
                                                     NoConformidadVO persDest;
                                                     for(int k=0;k<resultaux.size();k++){ 
                                                           persDest=(NoConformidadVO)resultaux.get(k);
                                    %>
                                    <option value="<%= persDest.getIsoResponsable()%>"><%= persDest.getIsoResponsableNombre()%></option>
                                    <%}%>
                                    </select>
                                    <input value="Asignar" name="btnEnvio" id="btnEnvio" style="width:20%" title="Se refiere a direccionar la no conformidad a un responsable quien ser� el encargado de realizar las acciones pertinentes Ej: jefe de �rea quien fabricar� un pl�n de acci�n" onclick="guardarJuan('administrarNoConformidad','/clinica/paginas/accionesXml/guardar_xml.jsp')" type="button" />                               
                                    
                                    </td>                                                                                
                                  </tr>	
                                  <tr class="titulos"> 
                                    <td colspan="1">Tipo evento</td>
                                    <td colspan="1" title="Evento Padre a quien se le asoci� heredando su plan de acci�n">Relacionado con Evento</td>                                
                                    <td colspan="2">---</td>                                                   
                                  </tr>	
                                   <tr class="estiloImput">
                                    <td colspan="1"><center><input  type="radio" name="rbtImpactoNoImpacto" id="rbtImpacto" onchange="activarPlanDeaccion(this.id)"  />&nbsp;Impacto&nbsp;&nbsp;&nbsp;<input  type="radio" name="rbtImpactoNoImpacto" id="rbtNoImpacto" checked="checked"  onchange="activarPlanDeaccion(this.id)"/>&nbsp;No Impacto</center>
                                    <td colspan="1"><label  id="lblEventoPadreAsocia">&nbsp;</label></td>
                                    <td colspan="2">---</td>
                                  </tr>	
                                  <tr>
                                    <td colspan="4">
                                     <div id="divEditarPlanDeAccion" style="display:none; width:99%;" >
                                      <div id="tabsEventos">
                                          <ul>    
                                           <li><a href="#divDatosPlanAccion"  title="Datos PLAN DE ACCION"><center>Plan de acci�n</center></a></li> 
                                           <li><a href="#divDatosPersonalizarTareas" onclick="presentarPersonasPlanDeAccion()"    title="Personas delegadas para cumplir tareas del Plan de acci�n"><center>Personas Tareas</center></a></li>                                                                              
                                           <li><a href="#divDatoAsociarEventos" onclick="presentarEventosAsociarAdministrar()"   title="Relacionar al plan de acci�n otras no conformidades"><center>Relacionar no conformidades</center></a></li>
                                           <li><a href="#divSeguimiento" onclick="listadoTabla('listPlanAccionSeguimiento');presentarCumplimientoMeta();"   title="Seguimiento al plan de acci�n"><center>Seguimiento</center></a></li>                                     
                                          </ul>
                                         <div id="divDatosPlanAccion"> 
                                          <table  width="100%" border="0" cellpadding="1" cellspacing="1" >
                                           <tr class="titulos"> 
                                                <td colspan="1">Id Doc</td>	
                                                <td colspan="5">Tipo - An�lisis de causas</td>	
                                           </tr>                                       
                                           <tr class="estiloImput">
                                                <td colspan="1"><label  id="lblIdEventoDoc">&nbsp;</label></td>	 
                                                <td colspan="5">
                                                  <table  width="100%">
                                                   <tr class="estiloImput">
                                                    <td width="10%"> 
                                                      <select name="cmbTipoPlanAccion" size="1" id="cmbTipoPlanAccion" style="width:100%"  tabindex="1" >
                                                       <option value="00">[Escoja tipo plan acci�n]</option>
                                                      <%    resultaux.clear();
                                                                       resultaux=(ArrayList)beanAdmin.cargarSubTipoEvento("ISOGCALDOC");	//subtipos plan de accion: preventivo correctivo mejora
                                                                       NoConformidadVO isoSubTipoEvento;
                                                                       for(int k=0;k<resultaux.size();k++){ 
                                                                             isoSubTipoEvento=(NoConformidadVO)resultaux.get(k);
                                                      %>
                                                       <option value="<%= isoSubTipoEvento.getIsoIdTipoISOEvento()%>"><%= isoSubTipoEvento.getIsoDescrTipoISOEvento()%></option>
                                                      <%}%>
                                                      </select>   
                                                      </td>
                                                      <td width="90%">                             
                                                      <textarea type="text" value="" id="txtAnalisisCausas" name="txtAnalisisCausas"  style="width:100%"  size="500"  maxlength="500"  tabindex="14" /></textarea>
                                                      </td>
                                                  </tr>
                                                 </table> 
                                                
                                                </td>   
                                                <td colspan="4"  width="100%"></td>
                                            </tr>  
                                           <tr class="titulos"> 
                                                <td colspan="1"  >Fecha ini</td>	
                                                <td colspan="4"  >Meta Plan de acci�n</td>
                                           </tr>    
                                           <tr class="estiloImput"> 
                                              <td colspan="1"><input type="text" value=""  size="11"  maxlength="10"  id="txtFechaIniPlanAccion" name="txtFechaIniPlanAccion"  tabindex="14" />                                          </td>
                                              <td colspan="4"><input type="text" value="" id="txtMetaPlanAccion" name="txtMetaPlanAccion" onKeyPress="javascript:return teclearExcluirCaracter(event,'%');"   style="width:90%"  size="200"  maxlength="200"   tabindex="14" /></td>                                                                          
                                           </tr>     
                                                                                    
                                           <tr class="titulos" height="13">
                                               <td width="10%">Car�cter(I/E)</td>
                                               <td width="45%">C�mo realizar la acci�n o tarea</td> 
                                               <td width="10%">Cuando</td>                                
                                               <td width="25%">Quien</td>  
                                               <td width="10%">&nbsp;</td>                                                                                                                                                                                                             
                                           </tr>
                                           <tr class="estiloImput" height="7">
                                                           <td>
                                                              <select name="cmbCaracter" id="cmbCaracter"  style=" width:87%" >
                                                                <%    resultaux.clear();
                                                                                 resultaux=(ArrayList)beanAdmin.cargarSubTipoEvento("ISOGCALPLN");	
                                                                                 NoConformidadVO isoSubTipoEvento2;
                                                                                 for(int k=0;k<resultaux.size();k++){ 
                                                                                       isoSubTipoEvento2=(NoConformidadVO)resultaux.get(k);
                                                                %>
                                                                 <option value="<%= isoSubTipoEvento2.getIsoIdTipoISOEvento()%>"><%= isoSubTipoEvento2.getIsoDescrTipoISOEvento()%></option>
                                                                <%}%>
                                                                </select>                                
                                                            </td>
                                                            <td ><textarea name="txtRealizarAcciones" maxlength="285" cols="2" id="txtRealizarAcciones" style="width:99%" ></textarea>                                            
                                                            <td>
                                                              <input type="text" value=""  size="11"  maxlength="10"  id="txtFechaCuando" name="txtFechaCuando" tabindex="14" />
                                                            </td>  
                                                            <td>
                                                                <select name="cmbQuienRealizaAccion" id="cmbQuienRealizaAccion"  style=" width:87%" >
                                                                <option value="00">[ESCOJA ROL RESPONSABLE]</option>
                                                                <%    resultaux.clear();
                                                                                 resultaux=(ArrayList)beanAdmin.cargarRolDestinoEvento();	
                                                                                 NoConformidadVO persRes;
                                                                                 for(int k=0;k<resultaux.size();k++){ 
                                                                                       persRes=(NoConformidadVO)resultaux.get(k);
                                                                          %>
                                                                <option value="<%= persRes.getIsoResponsable()%>"><%= persRes.getIsoResponsableNombre()%></option>
                                                                <%}%>
                                                                </select>
                
                                                            
                                                            </td>                                     
                                                            <td>
                                                                  <center>
                                                                        <a href="#" onclick="agregarElementoListGral('listPlanAccion');" title="Adicionar"><img src="/clinica/utilidades/imagenes/acciones/edit_add.png" id="imgAdd" height="18" /></a>&nbsp;&nbsp;
                                                                        <a href="#" onclick="quitarDatosListasST2('listPlanAccion');" title="Eliminar"><img src="/clinica/utilidades/imagenes/acciones/edit_remove.png" height="18" id="imgDel" /></a>&nbsp;&nbsp;
                                                                  </center>
                                                            </td>                                             
                
                                                           
                                                     </tr>                                                                                                      
                                    
                                                </table> 
                                                   <div id="divdatosbasicoscredesa" style="overflow:scroll;  height:150px; width:100%">
                                                           <table id="listPlanAccion" class="scroll" cellpadding="0" cellspacing="0"></table>
                                                          <!-- <div id="pagerlistPlanAccion" class="scroll" style="overflow:scroll; text-align:center; " >  </div>              -->                                
                                                   </div>  
                                                   <div id="divPlanAccion01" style="width:100%" >  
                                                     <CENTER><input value="Guardar plan de acci�n" name="G" onclick="guardarPestanasListSTV('listPlanAccion','')" type="button" /> </CENTER>                                    
                                                   </div>
                                         </div><!--fin divDatosPlanAccion--> 
                                         <div id="divDatosPersonalizarTareas">                                        
                                            <table width="90%" >
                                              <tr class="titulos">
                                                <td colspan="2">&nbsp;Personas responsables de las tareas del Plan de acci�n (Quien)</td>
                                              </tr>
                                              <tr>
                                                <td colspan="1" align="center"> <select name="cmbPersonaQuienRealizaAccion" id="cmbPersonaQuienRealizaAccion"  style=" width:60%" >
                                                    <option value="00">[ESCOJA RESPONSABLE]</option>
                                                    <%    resultaux.clear();
                                                                     resultaux=(ArrayList)beanAdmin.cargarPersonaDestinoEvento();	
                                                                     NoConformidadVO persRes2;
                                                                     for(int k=0;k<resultaux.size();k++){ 
                                                                           persRes2=(NoConformidadVO)resultaux.get(k);
                                                              %>
                                                    <option value="<%= persRes2.getIsoResponsable()%>"><%= persRes2.getIsoResponsableNombre()%></option>
                                                    <%}%>
                                                    </select>
    
                                                
                                                </td>                                     
                                                <td colspan="1">
                                                      <center>
                                                            <a href="#" onclick="agregarElementoListGral('listPersonasPlanAccion');" title="Adicionar"><img src="/clinica/utilidades/imagenes/acciones/edit_add.png" id="imgAdd" height="18" /></a>&nbsp;&nbsp;
                                                            <a href="#" onclick="quitarDatosListasST2('listPersonasPlanAccion');" title="Eliminar"><img src="/clinica/utilidades/imagenes/acciones/edit_remove.png" height="18" id="imgDel" /></a>&nbsp;&nbsp;
                                                      </center>
                                                </td>    
                                              </tr>
                                              <tr>
                                                <td colspan="2" >
                                                   <div id="divdatosPersonasPlanAccion" style=" height:270px; width:100%">
                                                       <table align="center" id="listPersonasPlanAccion" class="scroll" cellpadding="0" cellspacing="0"></table>
                                                   </div>   
                                                   <div id="divBuscar4" style="width:100%" >  
                                                    <!-- <CENTER><input value="Adiciona Personas del plan de acci�n" name="G" onclick="guardarPestanasListSTV('listPersonasPlanAccion','')" type="button" /> </CENTER>-->                                    
                                                   </div>  
                                                   <div id="divBuscar5" style="width:100%" > <!-- 
                                                    <table width="100%" >
                                                      <tr class="estiloImput">
                                                        <td width="100%">Esta No Conformidad est� asociada a otra no conformidad (Evento Relacionado <label  id="lblEventoPadreAsocia">&nbsp;</label>), o es nueva por ello no se permite asociar personas. </td>
                                                      </tr>
                                                    </table>
                                                    -->
                                                   </div>
                                                </td>    
                                              </tr>                                          
                                              
                                            </table>                                     
                                         </div><!-- fin divDatosPersonalizarTareas-->  
                                         <div id="divDatoAsociarEventos"> 
                                           <table width="100%">
                                              <tr>
                                                <td>        
                                                          <table width="100%" border="0"  cellpadding="0" cellspacing="0"  align="center">
                                                                          <tr class="titulos" align="center">
                                                                            <td width="27%">Proceso</td>
                                                                            <td colspan="2">No conformidad del proceso</td>
                                                                            <td width="35%">Area donde se presenta el evento</td>							  
                                                                          </tr>
                                                                          <tr class="estiloImput">
                                                                            <td >
                                                                            <select name="cmbBusproceso2" size="1" id="cmbBusproceso2" style="width:90%"  tabindex="14" onchange="
                                                                                              cargarNoConformidades('00','cmbBusNoConformidades2',this.options[this.selectedIndex].value,'<%= response.encodeURL("/clinica/paginas/accionesXml/cargarNoConformidades_xml.jsp") %>');
                                                                                                      
                                                                                  ">	                                        
                                                                                      <option value="00">[TODOS LOS PROCESOS]</option>
                                                                                        <%    resultaux.clear();
                                                                                               resultaux=(ArrayList)beanAdmin.CargarProceso();	
                                                                                               ProcesoVO proceso2;
                                                                                               for(int k=0;k<resultaux.size();k++){ 
                                                                                                     proceso2=(ProcesoVO)resultaux.get(k);
                                                                                        %>
                                                                                      <option value="<%= proceso2.getCodigo()%>"><%= proceso2.getNombre()%></option>
                                                                                              <%}%>						
                                                                               </select>	                   
                                                                            </td>
                                                                            <td colspan="2" ><select name="cmbBusNoConformidades2" size="1" id="cmbBusNoConformidades2" style="width:90%"  tabindex="1" >
                                                                              <option value="00">[TODAS LAS NO CONFORMIDADES]</option>
                                                                            </select></td>
                                                                            <td ><select name="cmbBusUbicacionArea2" size="1" id="cmbBusUbicacionArea2" style="width:90%"  tabindex="1" >
                                                                              <option value="">[TODAS LAS AREAS]</option>
                                                                              <%    resultaux.clear();
                                                                                               resultaux=(ArrayList)beanAdmin.cargarUbicacionArea();	
                                                                                               UbicacionAreaVO ubicArea2;
                                                                                               for(int k=0;k<resultaux.size();k++){ 
                                                                                                     ubicArea2=(UbicacionAreaVO)resultaux.get(k);
                                                                                        %>
                                                                              <option value="<%= ubicArea2.getCodigo()%>"><%= ubicArea2.getNombre()%></option>
                                                                              <%}%>
                                                                              </select>
                                                                            </td>                
                                                                          
                                                                </tr>
                                                                          <tr class="titulos"> 
                                                                            <td colspan="1">Fecha desde</td>	                   
                                                                            <td colspan="1">Fecha hasta</td>
                                                                            <td colspan="1">Estado</td>                              
                                                                            <td colspan="1">Buscar no Conformidades </td>                                                            
                                                                          </tr>								
                                                                          <tr class="estiloImput">   
                                                                            <td colspan="1"><input type="text" value=""  size="15"  maxlength="10"  id="txtBusFechaEventoNoConfDesde2" name="txtBusFechaEventoNoConfDesde2" tabindex="14" /></td>	                   
                                                                            <td colspan="1"><input type="text" value=""  size="15"  maxlength="10"  id="txtBusFechaEventoNoConfHasta2" name="txtBusFechaEventoNoConfHasta2" tabindex="14" /></td>                                                    
                                                                            <td colspan="1"><select name="cmbBusEstadoEvento2" size="1" id="cmbBusEstadoEvento2" style="width:60%"  tabindex="1" >
                                                                              <option value="00">[TODOS]</option>
                                                                                          <% resultaux.clear();
                                                                                             resultaux=(ArrayList)beanAdmin.cargarEstadoEvento("ISOGCALEVE");	//subtipos plan de accion: preventivo correctivo mejora
                                                                                             NoConformidadVO isoEstBusEvent2;
                                                                                             for(int k=0;k<resultaux.size();k++){ 
                                                                                                  isoEstBusEvent2=(NoConformidadVO)resultaux.get(k);
                                                                                          %>
                                                                                           <option value="<%= isoEstBusEvent2.getIsoIdTipoISOEvento()%>"><%= isoEstBusEvent2.getIsoDescrTipoISOEvento()%></option>
                                                                              <%}%>
                                                                              </select>                               </td> 
                                                                             <td >
                                                                             <CENTER><input value="Buscar de no impacto" name="G" onclick="buscarJuan('administrarNoConformidadAsociar','/clinica/paginas/accionesXml/buscar_xml.jsp')" type="button" /> </CENTER>                           
                                                                            </td>         							
                                                                          </tr>	
                                                          </table>
                                                          <div id="divListado2" align="center"  >
                                                               <table id="listEventosParaAsociar" class="scroll"></table>
                                                          </div>
                                                          <div id="divdatosbasicoscredesa2"  style="width:100%; height:70px;">
                                                               <table id="listAsociarEventos" class="scroll" cellpadding="0" cellspacing="0"></table>
                                                          <div id="divBotonGuardNCAsociadas" style="width:100%; display:none" >  
                                                            <CENTER><input value="Guardar No Conformidades Asociadas" name="G" onclick="guardarPestanasListSTV('listAsociarEventos','')" type="button" />                                                         </CENTER>
                                                          </div>  
                                                          <div id="divBuscar3" style="width:100%; display:none" >  
                                                           <table width="100%" >
                                                              <tr class="estiloImput">
                                                                <td width="100%">Esta No Conformidad est� asociada a otra no conformidad (Evento Relacionado <label  id="lblEventoPadreAsocia">&nbsp;</label>), por ello no se le permite asociar. </td>
                                                              </tr>
                                                            </table>
                                                          </div>  
                                                    </div>                                                
                                                </td>
                                              </tr>
                                            </table>                                     
                                         </div><!-- fin divDatoAsociarEventos--> 
                                         
                                         <div id="divSeguimiento"> 
                                          <table  width="100%" border="0" cellpadding="1" cellspacing="1" > 
                                           <tr class="titulos"> 
                                                <td colspan="1"  >Fecha ini</td>	
                                                <td colspan="4"  >Meta Plan de acci�n</td>
                                           </tr>    
                                           <tr class="estiloImput"> 
                                              <td colspan="1"><label  id="lblFechaIniPlanAccion">&nbsp;</label></td>
                                              <td colspan="4"><label  id="lblMetaPlanAccion">&nbsp;</label></td>                                                                          
                                           </tr>    
                                           <tr class="titulos"> 
                                                <td colspan="1"  >Eficaz</td>	
                                                <td colspan="4"  >Sequimiento a la meta</td>
                                           </tr>    
                                           <tr class="estiloImput"> 
                                              <td colspan="1"><select name="cmbEficaz" id="cmbEficaz"  style=" width:60%" >
                                                               <option value="SEG_0">&nbsp;&nbsp;</option>
                                                               <option value="SEG_1">Si</option>
                                                               <option value="SEG_2">No</option>                                                           
                                                              </select>  
                                              </td>                
                                              <td colspan="4"><label  id="lblIdEventoDocSeg"></label><input type="text" value="" id="txtSeguimMetaPlanAccion" name="txtSeguimMetaPlanAccion"  style="width:80%"  size="120" onKeyPress="javascript:return teclearExcluirCaracter(event,'%');"  maxlength="120"   tabindex="14" /></td>                                                                          
                                           </tr>     
     
                                                                                    
                                           <tr class="titulos" height="13">
                                               <td width="10%">Cumple</td>
                                               <td width="35%">C�mo realizar la acci�n o tarea</td> 
                                               <td width="35%" colspan="2">Seguimiento a la Acci�n</td>                                
                                               <td width="10%">&nbsp;</td>                                                                                                                                                                                                             
                                           </tr>
                                           <tr class="estiloImput" height="7">
                                                           <td><select name="cmbSeguimietoCumple" id="cmbSeguimietoCumple"  style=" width:80%" >
                                                               <option value="00">Escoja</option>
                                                               <option value="Si">Si</option>
                                                               <option value="No">No</option>                                                           
                                                              </select>                               
                                                            </td>
                                                            <td ><textarea id="txtRealizarAcciones" disabled="disabled" name="txtRealizarAcciones" maxlength="285" cols="2"  style="width:99%" ></textarea>                                            
                                                            <td colspan="2">
                                                              <textarea name="txtSeguimiAcciones" maxlength="285" cols="2" id="txtSeguimiAcciones" style="width:99%" ></textarea>
                                                            </td>  
                                                            <td>
                                                                  <center>
                                                                        <a href="#" onclick="modifiDatosListasSTGral('listPlanAccionSeguimiento');;" title="Modificar"><img src="/clinica/utilidades/imagenes/acciones/modificar.png" height="18" id="imgUpd" /></a>&nbsp;&nbsp;
                                                                  </center>
                                                            </td>                                             
                
                                                           
                                                     </tr>                                                                                                      
                                    
                                                </table> 
                                                   <div id="divdatosPlanSeguimiento" style="overflow:scroll;  height:120px; width:100%">
                                                           <table id="listPlanAccionSeguimiento" class="scroll" cellpadding="0" cellspacing="0"></table>
                                                          <!-- <div id="pagerlistPlanAccion" class="scroll" style="overflow:scroll; text-align:center; " >  </div>              -->                                
                                                   </div>   
                                              <CENTER>
                                              
                                              <%if(beanSession.usuario.getIdentificacion().equals("CC000098399000")){%>
                                               <input  value="Guardar seguimiento al plan de acci�n" name="G" onclick="guardarPestanasListSTV('listPlanAccionSeguimiento','')" type="button" /> 
                                              <%}%>
                                              </CENTER>                                    
                                         </div><!--fin divSeguimiento--> 
                                      
                                      </div>
                                     </div>  <!-- fin divEditarPlanDeAccion-->                  
                                    </td>   
                                  </tr>                                  
                            </table>  
                         </div>
                        </td>   
                      </tr>    		
                                      
                </table>
    
          </div><!-- divEditar-->	  
            <!-- el nombre de este div es sagrado para poder cerrar-->	  
            <div id="divIndicadores" style="display:none;  top:100px; left:207px; position:absolute; color:#B0C6BD; font-weight:bold  "  > 
               <!-- AQUI COMIENZA EL TITULO ESTADISTICO el padre para cerrar es divIndicadores-->	
                 <div align="center" id="tituloForm" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                     <jsp:include page="../../titulo.jsp" flush="true">
                            <jsp:param name="titulo" value="ESTADISTICO NO CONFORMIDADES " />
                     </jsp:include>
                 </div>	
               <!-- AQUI TERMINA EL TITULO ESTADISTICO -->            
           
               <table width="800" class="fondoTabla" >
                 <tr class="estiloImput" > 
                   <td width="100%" >            

                     
                         <table width="100%" id="CabeceralistadoIndicadores" class="scroll" >
                           <tr class="tituloFormaPeque" > 
                               <td width="7%" onMouseOver="mostrar('sublistadoIndicadores')" onMouseOut="ocultar('sublistadoIndicadores')" >Estruct</td> 
                               <td width="40%">Descripci�n Proceso</td> 
                               <td width="10%">Cuantas NC</td> 
                               <td width="10%">Reportadas</td>
                               <td width="10%">Asignadas</td> 
                               <td width="10%">Impacto</td>
                               <td width="10%">Planes</td> 
                               <td width="10%">Planes Cerrad</td>                   
                           <tr>     
                         </table>       
                         <table width="100%" id="listadoIndicadores" class="scroll" bgcolor="#B0C6BD">
                           <tr>
                               <td width="2%"> </td> 
                               <td width="18%"></td> 
                               <td width="70%"></td> 
                               <td width="10%"> </td>
                           <tr> 
                           </table> 
                           <table  width="100%"> 
                           <tr class="estiloImput" > 
                               <td width="4%" align="left" >&nbsp;</td> 
                               <td width="40%" align="right">Totales=&nbsp;</td> 
                               <td width="10%" align="left"><label  id="lblCuantasNC"></label></td> 
                               <td width="10%" align="left"><label  id="lblReportadas"></label></td>
                               <td width="10%" align="left"><label  id="lblAsignadas"></label></td> 
                               <td width="10%" align="left"><label  id="lblImpacto"></label></td>
                               <td width="10%" align="left"><label  id="lblPlanes"></label></td> 
                               <td width="10%" align="left"><label  id="lblPlanesCerrad"></label></td>                   
                           <tr>  
                         </table> 
                 </td>
                <tr>  
              </table>   

            </div>  

       
        <!--********************** fin de la tabla con los datos del formulario ***********************************************-->
       </div><!-- div tabsEventosIndicadores-->
    </div><!-- div contenido-->
    
        </td>
       </tr>
      </table>
      <input type="hidden"  id="IdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>"/>
    
   </td>
 </tr> 
</table>

      <div id="divSublistadoIndicadores"  onmouseover="mostrar('divSublistadoIndicadores')" onMouseOut="ocultar('divSublistadoIndicadores')" style="position:absolute; background-color:#FFF;   display:none;  left:55px; width:800; height:600; z-index:901">
       <table width="800" class="fondoTabla" >
         <tr class="estiloImput" > 
           <td width="100%" >
               <table width="100%"  >
               <tr bgcolor="#B0C6BD" > 
                   <td width="100%" ><label  id="lblIdSubEvento"></label> <label  id="lblTituloCabezaEvento"></label></td> 
               <tr>     
               </table>         
               <table width="100%"  id="CabeceraSublistadoIndicadores" class="scroll" >
               <tr> 
                   <td width="7%" class="label_estadisticoGrafi">Estruct</td> 
                   <td width="40%">Descripci�n Evento</td> 
                   <td width="10%">Cuantas Ev</td> 
                   <td width="10%">Reportadas</td>
                   <td width="10%">Asignadas</td> 
                   <td width="10%">Impacto</td>
                   <td width="10%">Planes</td> 
                   <td width="10%">Planes Cerrad</td>                   
               <tr>     
               </table>       
               <table width="100%" id="sublistadoIndicadores" class="scroll">
               <tr >
                   <td width="7%" >&nbsp;</td> 
                   <td width="40%"></td> 
                   <td width="10%"></td> 
                   <td width="10%"></td>
                   <td width="10%"></td> 
                   <td width="10%"></td>
                   <td width="10%"></td> 
                   <td width="10%"></td> 
               <tr> 
               </table > 
               <table width="100%" class="scroll"> 
               <tr > 
                   <td width="7%" align="left" >&nbsp;</td> 
                   <td width="40%"align="right">Totales=&nbsp;</td> 
                   <td width="10%" align="left"><label  id="lblSubCuantasNC"></label></td> 
                   <td width="10%" align="left"><label  id="lblSubReportadas"></label></td>
                   <td width="10%" align="left"><label  id="lblSubAsignadas"></label></td> 
                   <td width="10%" align="left"><label  id="lblSubImpacto"></label></td>
                   <td width="10%" align="left"><label  id="lblSubPlanes"></label></td> 
                   <td width="10%" align="left"><label  id="lblSubPlanesCerrad"></label></td>                   
               <tr>  
              </table>         
            </td>
           <tr>  
          </table> 
         </div>



     


<%@ page session="true" contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>

<%@ page import="java.io.File" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.util.HashMap" %>

<%@ page import="net.sf.jasperreports.engine.JasperRunManager" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />

<!DOCTYPE HTML>
<% 

        System.out.println("-------IMPRESION VARIAS FACTURAS----------");  
        System.out.println("Conectado a: " + beanSession.cn.getServidor());
        String ruta="";

        Connection conexion = null; 
    
       ruta = "paginas/ireports/facturacion/";
      
       try{
          conexion = beanSession.cn.getConexion();


          String numFacturas = request.getParameter("numFacturas");
          String cadenaJasper = ruta +"FACTURAS.jasper";


          File reportFile = new File(application.getRealPath(cadenaJasper));  
          HashMap<String, Object> parametros = new HashMap<String, Object>(); 

          parametros.put("P1", numFacturas);       

          byte[]  bytes  =  JasperRunManager.runReportToPdf(reportFile.getPath(),  parametros, conexion);  

          response.setContentType("application/pdf");
          response.setContentLength(bytes.length); 

          ServletOutputStream ouputStream = response.getOutputStream(); 
          ouputStream.write(bytes, 0, bytes.length);  

          ouputStream.flush(); 
          ouputStream.close();

   }
       catch(Exception ex){	
          System.out.println("Error Exception en generarFacturas.jsp "+ex.getMessage());
     }
	
        
	
%>
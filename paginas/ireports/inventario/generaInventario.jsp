<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<%@ page import="net.sf.jasperreports.engine.*" %> 
<%@ page import="net.sf.jasperreports.engine.util.*" %>
<%@ page import="net.sf.jasperreports.engine.export.*" %>

<%@ page import="net.sf.jasperreports.view.JasperViewer" %>
<%@ page import="javax.naming.*" %>

<%@ page import="java.io.*" %> 
<%@ page import="java.awt.Font" %> 

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session   contentType="text/html; charset=iso-8859-1"-->

<% 	 
        System.out.println("-----------------IREPORTS-------------------------");	
        System.out.println("Conectado a: " + beanSession.cn.getServidor());  
        
%>

<!DOCTYPE HTML >  

<% 

   		Connection conexion = beanSession.cn.getConexion(); 
       String ruta = "paginas/ireports/inventario/";

       String reporte = request.getParameter("reporte");	
       String cadenaJasper = ruta+reporte+".jasper";	

       int idDoc = Integer.parseInt(request.getParameter("idDoc"));		

       String dir1 = "/opt/tomcat8/webapps/clinica/utilidades/firmas";			
       String dir2 = "/opt/tomcat8/webapps/clinica/utilidades/imagenes/acciones/infoencabezado.png";
	
       File reportFile = new File(application.getRealPath(cadenaJasper));  
       System.out.println("Ruta: "+reportFile.getPath());
	
       HashMap<String, Object> parametros = new HashMap<String, Object>();  
	
       parametros.put("P1", idDoc);  
       parametros.put("dirFirma", dir1);  
       parametros.put("dirLogo", dir2);  
       
       //condición solo para el reporte DOC_FARMACIA_DEV que tiene idPac
       if(request.getParameter("idPac") != null){
            int idPac = Integer.parseInt(request.getParameter("idPac"));	
            parametros.put("P2", idPac);  
        }

       byte[]  bytes  =  JasperRunManager.runReportToPdf(reportFile.getPath(),  parametros, beanSession.cn.getConexion());  

       response.setContentType("application/pdf");
       response.setContentLength(bytes.length); 
	
       ServletOutputStream ouputStream = response.getOutputStream(); 
       ouputStream.write(bytes, 0, bytes.length);  
	
       ouputStream.flush(); 
       ouputStream.close(); 

%>

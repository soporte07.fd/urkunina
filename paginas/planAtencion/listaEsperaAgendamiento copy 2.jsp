<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>


<table width="1150px" height="250px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="AGENDAMIENTO DESDE LISTA DE ESPERA" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>
                        <div style="overflow:auto;height:500px; width:100%" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulos" align="center">
                                    <td width="25%">Especialidad</td>
                                    <td width="25%">Tipo Cita</td>
                                    <td width="15%">Con Cita</td>
                                    <td width="25%">Prestador</td>
                                    <td width="10%"></td>
                                </tr>
                                <tr class="estiloImput">
                                    <td>
                                        <select id="cmbIdEspecialidad" style="width:90%" tabindex="14"
                                            onchange="cargarSubEspecialidadDesdeEspecialidad('cmbIdEspecialidad', 'cmbIdSubEspecialidad');asignaAtributo('cmbTipoCita', '', 0)">
                                            <option value=""></option>
                                            <%     resultaux.clear();
                                                       resultaux=(ArrayList)beanAdmin.combo.cargar(100);	
                                                       ComboVO cmb32; 
                                                       for(int k=0;k<resultaux.size();k++){ 
                                                             cmb32=(ComboVO)resultaux.get(k);
                                                %>
                                            <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>">
                                                <%= cmb32.getDescripcion()%></option>
                                            <%}%>						
                                        </select>	                   
                                    </td>
                                    <td>
                                        <select size="1" id="cmbTipoCita" onfocus="cargarTipoCitaSegunEspecialidad('cmbTipoCita', 'cmbIdEspecialidad')" style="width:90%">     
                                        </select>                                                              
                                    </td> 
                                    <td>
                                        <select id="cmbConCita" style="width:90%">
                                            <option value="N">NO</option>
                                            <option value="S">SI</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="txtPrestadorAsignado" class="w80" tabindex="108">
                                        <img title="VENPRE" id="idLupitaVentanitaPresta" onclick="traerVentanita(this.id,'','divParaVentanita','txtPrestadorAsignado','23')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle">
                                    </td>
                                    <td>
                                        <input type="button" class="small button blue" value="BUSCAR" onclick="buscarRutas('listGrillaListaEsperaRuta')">
                                    </td>
                                </tr>
                            </table>
                            <div style="overflow:auto;height:800px; width:100%">
                                <table id="listGrillaListaEsperaRuta" class="scroll" width="100%"></table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<div id="divVentanitaEditarCita"  style="position:absolute; display:none; background-color:#E2E1A5; top:310px; left:150px; width:900px; height:90px; z-index:999">
    <table width="100%"  id="CabeceraSublistadoCalendario" border="1"  class="fondoTabla">
       
       <tr class="estiloImput" >
           <td colspan="5" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditarCita')" /></td>  
       </tr>     
       <tr class="titulos" >
           <td width="10%">Fecha Cita</td> 
           <td width="20%">Hora</td>
           <td width="20%">Estado</td> 
           <td width="25%">Motivo</td>   
           <td width="25%">Clasificacion</td>          
       </tr>           
       <tr class="estiloImput" >
           <td><input type="text" id="txtFechaCita" style="width:90%"></td>  
           <td>
                                                         
                <select id="cmbHoraCita">
                    <option value=""></option>
                    <% 
                        for(int i=101;i<113;i++){                                       
                            String[] value = Integer.toString(i).split("");
                            String hour = value[1]+value[2];
                                                                
                    %>  
                        <option value="<%=hour%>"><%=hour%></option>      
                    <%}
                    %>                                   
                </select>
                
                <select id="cmbMinutoCita">
                    <option value=""></option>
                    <% 
                        for(int i=100;i<160;i++){                                       
                            String[] value = Integer.toString(i).split("");
                            String minute = value[1]+value[2];
                                                                
                    %>  
                        <option value="<%=minute%>"><%=minute%></option>      
                    <%}
                    %>                                   
                </select>
                
                <select id="cmbPeriodoCita">
                    <option value=""></option>
                    <option value="am">AM</option>
                    <option value="pm">PM</option>
                </select>
           </td>   
           <td>
                <input type="hidden" id="txtEstadoCita" />
                <select id="cmbEstadoCita" onfocus="limpiarDivEditarJuan('limpiarMotivoAD')" style="width:90%" tabindex="120" >
                    <option value=""></option>
                    <%     resultaux.clear();
                           resultaux=(ArrayList)beanAdmin.combo.cargar(107);    
                           ComboVO cmbEst; 
                           for(int k=0;k<resultaux.size();k++){ 
                                 cmbEst=(ComboVO)resultaux.get(k);
                    %>
                                            <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>">
                                                <%= cmbEst.getId()+"  "+cmbEst.getDescripcion()%></option>
                                            <%}%>                       
                </select>
            </td>       
           <td>
                <select id="cmbMotivoConsulta" style="width:90%" tabindex="121" onfocus="comboDependiente('cmbMotivoConsulta', 'cmbEstadoCita', '96');limpiarDivEditarJuan('limpiarMotivoClase');">
                    <option value=""></option>
                    <%     resultaux.clear();
                           resultaux=(ArrayList)beanAdmin.combo.cargar(106);    
                           ComboVO cmbM; 
                           for(int k=0;k<resultaux.size();k++){ 
                                 cmbM=(ComboVO)resultaux.get(k);
                    %>
                                            <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                                                <%= cmbM.getId()+"  "+cmbM.getDescripcion()%></option>
                                            <%}%>                       
                </select> 
            </td>
            <td>
                <select id="cmbMotivoConsultaClase" style="width:90%" tabindex="121" onfocus="comboDependienteDosCondiciones('cmbMotivoConsultaClase','cmbMotivoConsulta','txtIdEspecialidad','563')">
                    <option value=""></option>                      
                </select> 
             </td>  
       </tr>  
       <tr align="CENTER">
           <td colspan="5">
                <input  type="button" class="small button blue" value="GUARDAR" title="B887" onclick="modificarCRUDRutas('modificarCitaListaEsperaRuta');" tabindex="309">
           </td>
       </tr>         
    </table> 
</div>   
<input type="hidden" id="txtIdAgenda">
<input type="hidden" id="txtIdListaEspera">
<input type="hidden" id="txtIdEspecialidad">
<input type="hidden" id="txtIdSubEspecialidad">
<input type="hidden" id="txtTipoCita">
<select  id="cmbIdSubEspecialidad" style="visibility:hidden">	                                        
</select> 


<div id="divVentanitaEventosPaciente"  style="position:absolute; display:none; background-color:#E2E1A5; top:310px; left:150px; width:900px; height:90px; z-index:999">
    <table width="100%" border="1" class="fondoTabla">
        <tr>
            <td>
                <div id="divDepositoContactosPaciente"></div>     
            </td>
        </tr>
    </table>
</div>
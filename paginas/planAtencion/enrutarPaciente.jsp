<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />

<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="1250px" align="center" border="0" >
    <tr>
        <td>
            <div align="center" id="tituloForma">
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="ENRUTAR PACIENTES EN PLAN DE ATENCION"/>
                </jsp:include>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>
                        <table width="100%">
                            <tr class="titulosListaEspera">
                                <td width="20%">TIPO ID</td>
                                <td width="20%">IDENTIFICACION</td>
                                <td width="40%" >PACIENTE</td>
                                <td width="20%" rowspan="4">
                                    <input name="btn_limpiar" type="button" title="FYY1" class="small button blue" value="LIMPIAR" onclick="limpiaAtributo('txtPrestadorAsignadoRuta', 0);limpiaAtributo('txtIdBusPaciente', 0);;limpiaAtributo('cmbTipoId', 0); limpiaAtributo('txtIdentificacion', 0);limpiaAtributo('cmbRuta', 0); limpiaAtributo('cmbPlan', 0); limpiarListadosTotales('listGrillaRutaPaciente')" />
                                    <input name="btn_BUSCAR" title="PTT2" type="button" class="small button blue" value="BUSCAR"onclick="buscarRutas('listGrillaRutaPaciente')" />

                                </td>
                            </tr>
                            <tr class="estiloImput">
                                <td>
                                    <select size="1" id="cmbTipoId" style="width:90%" tabindex="100">
                                        <option value=""></option>
                                        <%     resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(105);
                                            ComboVO cmb105;
                                            for (int k = 0; k < resultaux.size(); k++) {
                                                cmb105 = (ComboVO) resultaux.get(k);
                                        %>
                                        <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                                            <%= cmb105.getId() + "  " + cmb105.getDescripcion()%>
                                        </option>
                                        <%}%>
                                    </select>
                                </td>  

                                <td ><input type="text" id="txtIdentificacion" style="width:90%" size="20" maxlength="20" onKeyPress="javascript:checkKey2(event);" tabindex="101" />
                                </td>
                                <td >
                                    <input type="text" size="110" maxlength="210" id="txtIdBusPaciente" style="width:90%" tabindex="100" />
                                    <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '7');" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                </td>
                                <td></td>
                            </tr>
                            <tr class="titulosListaEspera" align="center">
                                <td>RUTAS</td>
                                <td>PROGRAMAS</td>
                                <td>PRESTADOR ASIGNADO</td>
                                <td></td>
                            </tr>
                            <tr class="estiloImput">
                                <td>
                                    <select size="1" id="cmbPlan" style="width:80%" onchange="cargarRutasAPartirPlan(this.id, 'cmbRuta')">
                                        <option value="" selected="selected"></option>  
                                        <%
                                            resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(841);
                                            ComboVO cmb33;
                                            for (int k = 0; k < resultaux.size(); k++) {
                                                cmb33 = (ComboVO) resultaux.get(k);
                                        %>
                                        <option value="<%= cmb33.getId()%>" title="<%= cmb33.getTitle()%>"><%= cmb33.getDescripcion()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td>
                                    <select size="1" id="cmbRuta" style="width:80%" >                      
                                        <option value=""></option>
                                    </select>
                                </td>   

                                <td>
                                    <input type="text" id="txtPrestadorAsignadoRuta" class="w80"  tabindex="108" style="width:93%" />
                                    <img width="18px" height="18px" align="middle" title="VENRuta" id="idLupitaVentanitaPrestaR" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtPrestadorAsignadoRuta', '23')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                </td>
                                <td></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="height:330px;"  >
                            <table id="listGrillaRutaPaciente"></table>
                        </div>
                        <div id="pager1"></div>

                    </td>
                </tr>

                <tr class="titulosCentrados">
                    <td>PARAMETROS DEL PROGRAMA</td>
                </tr>

                <tr>
                    <td>
                        <div style="height:330px;" >
                            <table id="listGrillaRutaDelPaciente" ></table>
                        </div>
                        <div id="pager2"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>




<div id="divVentanitaRuta"  style="display:none; z-index:9997; top:1px; left:211px;">

    <div class="transParencia" style="z-index:9998; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>   
    <div  id="ventanitaHija" style="z-index:9999; position:fixed; top:200px; left:250px; width:70%">  
        <table width="1000" height="90" class="fondoTabla">
            <tr class="estiloImput" >
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaRuta')" /></td>  
                <td><b>ESCOJA UN PACIENTE</b></td>                   
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaRuta')" /></td>  
            <tr> 
            <tr class="titulos" >
                <td width="20%">CODIGO</td>                            
                <td width="70%">NOMBRE</td>
                <td width="10%">&nbsp;</td>                            
            </tr>
            <tr class="titulos" >
                <td><input type="text" id="txtCodBus"  style="width:90%" tabindex="998"  onkeypress="checkKeyVentanita(event);"/> 
                </td> 
                <td><input type="text" id="txtNomBus"  style="width:90%" onkeyup="this.value = this.value.toUpperCase();" onkeypress="checkKeyVentanita(event);"/> 
                </td> 
                <td>
                    <input name="BTN_BUSCAR" title="BW62" type="button" class="small button blue" value="BUSCAR"  onclick="buscarHistoria('listGrillaPacienteRuta')" />                    
                </td>         
            </tr>           
            <tr class="titulos" >   
                <td colspan="3">  
                    <table id="listGrillaPacienteRuta" class="scroll" width="100%"></table>  
                </td>
            </tr>          
        </table>  
    </div>  
</div>  
<input type="hidden" id="txtIdRutaPaciente" />
<input type="hidden" id="txtIdRuta" />
<input type="hidden" id="txtIdPaciente" />


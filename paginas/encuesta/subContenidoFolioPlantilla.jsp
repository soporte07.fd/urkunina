<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<!DOCTYPE HTML >  
<style type="text/css">
#div2 {
   overflow:scroll;
   height:300px;
   width:100%;
}
#div2 table {
  width:100%;
}

.misbordes {
    border-right: solid #a6c9e2 1px;
    border-bottom: solid #a6c9e2 1px ;
    height:22px;
    font-size: 11px;
    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
  }
  
  .paratrplantilla{
    background-color: white;
    font-size: 11px;
    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
  }

</style> 
<%  


beanAdmin.setCn(beanSession.getCn()); 

ArrayList resulCombo = new ArrayList();

String var = request.getParameter("tipo");
String tipo_folio = request.getParameter("tipo_folio");
String id_evolucion = request.getParameter("id_evolucion");
String bloquear = "";
int contador = 0;

Connection conexion = beanSession.cn.getConexion();
try{

Statement stmt = conexion.createStatement();
String SQL = " ";


    SQL = " SELECT "+
    " pd.orden,"+ 
    " pd.id_plantilla,"+ 
    " pd.descripcion,"+ 
    " pd.referencia,"+ 
    " pd.id_jerarquia, "+
    " pd.id_padre_orden,"+ 
    " pd.resultado_default,"+ 
    " pd.tipo_input,"+ 
    " pd.id_query_combo,"+
    " pd.id_query_ventanita, "+
    " pd.cantdecimales, "+ 
    " pd.ancho,"+
    " pd.altura, "+
    " pd.item,"+
    " pd.vigente,"+
    " pd.edad_meses_desde,"+    
    " pd.maxlenght,"+ 
    " pd.id_formula,"+
    " pd.campo_bloquea,"+  
    " p.nombre,"+ 
    " p.id_tipo,"+ 
    " p.tabla,"+ 
    " COALESCE(formulario.fn_datos_folio_plantilla(pd.referencia,  p.tabla, "+id_evolucion+"::VARCHAR), '') dato,"+ 
    " p.td1_ancho,"+  
    " p.td2_ancho, "+
    " pfo.formula, "+
    " pfo.referencia_resultado, "+
    " (SELECT id_estado FROM hc.evolucion WHERE id="+id_evolucion+" AND tipo = '"+ tipo_folio +"') estado_folio,"+
    " (formulario.fn_numero_formulas_referencia('"+ tipo_folio +"',p.tabla,pd.referencia)) formulas, " +
    " (formulario.fn_validaciones_referencia(p.tabla,pd.referencia)) validaciones "+      
    " FROM "+  
    " formulario.plantilla_detalle pd"+  
    " INNER JOIN formulario.plantilla p ON pd.id_plantilla = p.id"+  
    " INNER JOIN formulario.plantilla_tipo_formulario ptf on p.id = ptf.id_plantilla"+
    " LEFT JOIN formulario.plantilla_formula pfo ON pd.id_formula = pfo.id"+  
    " WHERE pd.vigente='S' AND p.tabla = '"+ var +"' AND ptf.id_tipo_formulario = '"+ tipo_folio +"' ORDER BY pd.orden ";
    
    System.out.println("se ejecutoSQL: " + SQL);


  ResultSet rs = stmt.executeQuery(SQL);

%>   

<div id ="div2" style="background-color: white; ">
<table width="100%"  cellpadding="0" cellspacing="0"  > 
 <!-- <tbody width="100%" class="inputBlanco"> --> 
   
   <%
   while (rs.next()){

    String validacion = "";

    if(rs.getString("validaciones") != null)
      validacion = "validarCampo(this.id,'" + rs.getString("validaciones") + "');";

    String  actualizar = "";

    if(rs.getInt("formulas") > 0)
      actualizar = "actualizarCampoFormula(this.id, this.value);";

    if(rs.getInt("estado_folio") > 0 ){
      bloquear = "disabled";
    }else{ bloquear = " ";}
        if(rs.getString("id_jerarquia").equals("TI")){%>
                  <tr class="paratrplantilla overs" align="left"> 
                     <td class="misbordes" style="border-left: solid #a6c9e2 1px ;" width='<%=rs.getString("td1_ancho")%>' > <h3><%=rs.getString("descripcion")%></h3></td> 
                     <td class="misbordes" width='<%=rs.getString("td2_ancho")%>' > </td>                     
                  </tr>                    
    <%}else if(rs.getString("id_jerarquia").equals("PA")){  %>
                  <tr class="paratrplantilla overs" align="left"> 
                     <td class="misbordes" style="border-left: solid #a6c9e2 1px ;" width='<%=rs.getString("td1_ancho")%>' > <strong><%=rs.getString("descripcion")%></strong></td> 
                     <td class="misbordes" width='<%=rs.getString("td2_ancho")%>' > </td>                                           
                  </tr> 
   <%}else{%>
     <tr class="paratrplantilla overs" align="left">  

             <td  class="misbordes" style="border-left: solid #a6c9e2 1px ;" width='<%=rs.getString("td1_ancho")%>' title='<%= rs.getString("referencia")%>'> <%=rs.getString("descripcion")%>  </td>                    
              <td class="misbordes" width='<%=rs.getString("td2_ancho")%>' > 
              <% if(rs.getString("tipo_input").trim().equals("input")){
                if(rs.getInt("id_formula") > 0){ %> 
                  <input type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width: <%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var %>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar %>" onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>',  this.value );<%= validacion %>" <%= bloquear %> />
                  <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                  <input type="hidden" name="<%= var %>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>                               
              <% } else if (rs.getInt("id_formula") < 1 ){%>
                <input  type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var %>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar %>"  onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>',  this.value );<%= validacion %>" <%= bloquear %> />                             
              <% }
            

              }
              else if(rs.getString("tipo_input").trim().equals("inputl"))
              {%>
                <input  type="text" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' id='txtFormulario' name='<%= var %>' <%= bloquear %> onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>',  this.value );<%= validacion %>"/>
                <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
								onclick="traerVentanitaFuncionesHcPlantilla(this.id, 'actualizarDxIngresoHC', 'divParaVentanita','txtFormulario','<%= rs.getString("id_query_ventanita")%>')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">                
              <%}

              else if(rs.getString("tipo_input").trim().equals("hora"))
              {%>
                <input type="time" step="600" style='width: <%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var %>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar %>" onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>',  this.value );<%= validacion %>" <%= bloquear %> /> 
              <%}

              else if(rs.getString("tipo_input").trim().equals("numeros")){
              if(rs.getInt("id_formula") > 0){ %> 
                <input  type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var %>' onkeypress="javascript:return soloTelefono(event);" value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar %>"  onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>',  this.value );<%= validacion %>" <%= bloquear %> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var %>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>  
              <% } else if (rs.getInt("id_formula") < 1 ){%>
                <input  type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var %>' onkeypress="javascript:return soloTelefono(event);" value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar %>"  onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>',  this.value );<%= validacion %>" <%= bloquear %> />
              <%}
              }
              else if(rs.getString("tipo_input").trim().equals("decimal")){
              if(rs.getInt("id_formula") > 0){ %> 
                <input  type="number" step='<%=rs.getString("cantdecimales")%>' maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' onkeypress="javascript:return NumCheck(event,this.id,'<%=rs.getString("cantdecimales")%>');" name='<%= var %>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar %>"  onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>',  this.value );<%= validacion %>" <%= bloquear %> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var %>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'> 
              <% } else if (rs.getInt("id_formula") < 1 ){%>
                <input  type="number" step='<%=rs.getString("cantdecimales")%>' maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' onkeypress="javascript:return NumCheck(event,this.id,'<%=rs.getString("cantdecimales")%>');" name='<%= var %>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar %>"  onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>',  this.value );<%= validacion %>" <%= bloquear %> />
              <%}
              }
              else if(rs.getString("tipo_input").trim().equals("switchsn")){ 
                String estado = " ";
                String cam_bloquear = "";
                String parametros = "";
                String class_span1 = "";
                String class_span2 = "";
                String activar = "";
                String bloquear_aux = "";

                if(rs.getString("dato").equals("SI") || rs.getString("dato").equals("NO")){
                  class_span1 = "onoffswitch-inner2";
                  class_span2 = "onoffswitch-switch2";
                  activar = "";

                  if(rs.getString("dato").equals("SI"))
                  {
                    estado = "checked";
                  }
                }else{
                  class_span1 = "onoffswitch-inner2-disabled";
                  class_span2 = "onoffswitch-switch2-disabled";
                  if(rs.getInt("estado_folio") == 0){
                    bloquear_aux = bloquear;
                    bloquear = "disabled";
                    activar = "activarSwitchsn('"+ var +"', '" + rs.getString("referencia") + "')";
                  }
                }

                if(rs.getString("campo_bloquea") == null || rs.getString("campo_bloquea").equals("")){
                  cam_bloquear = " ";
                }else{
                  parametros = rs.getString("campo_bloquea");
                  cam_bloquear = "bloquearMisCampos('"+parametros+"','"+var+"', this.checked);";
                }
                %>  

                <div class="onoffswitch2" onclick="<%= activar %>">
                  <% if(rs.getInt("id_formula") > 0){ %>
                    <input type="checkbox" name='<%= var %>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id,this.checked);<%= actualizar %><%= validacion%><%=cam_bloquear%>" <%= estado %>  <%= bloquear %>  >
                    <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                    <input type="hidden" name="<%= var %>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'> 
                  <% } else if (rs.getInt("id_formula") < 1 ){ %>
                    <input type="checkbox" name='<%= var %>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('<%= var %>', '<%= rs.getString("referencia")%>' , this.value);<%= actualizar %><%= validacion%><%=cam_bloquear%>" <%= estado %>  <%= bloquear %>>  
                  <% } %>
                  <label class="onoffswitch-label2" for='<%= "formulario." + var + "." + rs.getString("referencia")%>'>
                      <span class="<%= class_span1 %>" id='<%= var %>-<%= rs.getString("referencia") %>-span1'></span>
                      <span class="<%= class_span2 %>" id='<%= var %>-<%= rs.getString("referencia") %>-span2'></span>
                  </label>
                </div>
                <%
                bloquear = bloquear_aux;
              }

              else if(rs.getString("tipo_input").trim().equals("switchS10")){ 
                String estado = " ";
                String cam_bloquear = "";
                String parametros = "";
                String class_span1 = "";
                String class_span2 = "";
                String activar = "";
                String bloquear_aux = "";

                if(rs.getString("dato").equals("1") || rs.getString("dato").equals("0")){
                  class_span1 = "onoffswitch-inner2";
                  class_span2 = "onoffswitch-switch2";
                  activar = "";

                  if(rs.getString("dato").equals("1"))
                  {
                    estado = "checked";
                  }
                }else{
                  class_span1 = "onoffswitch-inner2-disabled";
                  class_span2 = "onoffswitch-switch2-disabled";
                  if(rs.getInt("estado_folio") == 0){
                    bloquear_aux = bloquear;
                    bloquear = "disabled";
                    activar = "activarSwitchS10('"+ var +"', '" + rs.getString("referencia") + "')";
                  }
                }
                %>  
            

                <div class="onoffswitch2" onclick="<%= activar %>">
                  <% if(rs.getInt("id_formula") > 0){ %>
                    <input type="checkbox" name='<%= var %>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitch10(this.id,this.checked);<%= actualizar %><%= validacion %>" <%= estado %>  <%= bloquear %>  >
                    <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                    <input type="hidden" name="<%= var %>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                  <% } else if (rs.getInt("id_formula") < 1 ){ %>
                    <input type="checkbox" name='<%= var %>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitch10(this.id,this.checked);guardarDatosPlantilla('<%= var %>', '<%= rs.getString("referencia")%>' , this.value);<%= actualizar %><%= validacion %>" <%= estado %>  <%= bloquear %>  >
                  <% } %>
                  <label class="onoffswitch-label2" for='<%= "formulario." + var + "." + rs.getString("referencia")%>'>
                    <span class="<%= class_span1 %>" id='<%= var %>-<%= rs.getString("referencia") %>-span1'></span>
                    <span class="<%= class_span2 %>" id='<%= var %>-<%= rs.getString("referencia") %>-span2'></span>
                  </label>
                </div>
                <%
                bloquear = bloquear_aux;
              }
                
                else if(rs.getString("tipo_input").trim().equals("switch10")){
                  System.out.println("####" + rs.getString("referencia")); 

                  String estado1 = " ";
                  String cam_bloquear = "";
                  String parametros = "";
                  String class_span1 = "";
                  String class_span2 = "";
                  String activar = "";
                  String bloquear_aux = "";

                  if(rs.getString("dato").equals("1") || rs.getString("dato").equals("0")){
                    class_span1 = "onoffswitch-inner3";
                    class_span2 = "onoffswitch-switch3";
                    activar = "";

                    if(rs.getString("dato").equals("1"))
                    {
                      estado1 = "checked";
                    }
                  }else{
                    class_span1 = "onoffswitch-inner2-disabled";
                    class_span2 = "onoffswitch-switch2-disabled";
                    if(rs.getInt("estado_folio") == 0){
                      bloquear_aux = bloquear;
                      bloquear = "disabled";
                      activar = "activarSwitch10('"+ var +"', '" + rs.getString("referencia") + "')";
                    }
                  }
                  %>  
              
        
                  <div class="onoffswitch3" onclick="<%= activar %>">
                    <% if(rs.getInt("id_formula") > 0){ %>
                      <input type="checkbox" name='<%= var %>' class="onoffswitch-checkbox3" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitch10(this.id,this.checked);<%= actualizar %><%= validacion %>" <%= estado1 %>  <%= bloquear %>  >
                      <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                      <input type="hidden" name="<%= var %>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                    <% } else if (rs.getInt("id_formula") < 1 ){ %>
                      <input type="checkbox" name='<%= var %>' class="onoffswitch-checkbox3" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitch10(this.id,this.checked);guardarDatosPlantilla('<%= var %>', '<%= rs.getString("referencia")%>' , this.value);<%= actualizar %><%= validacion %>" <%= estado1 %>  <%= bloquear %>  >
                    <% } %>
                    <label class="onoffswitch-label3" for='<%= "formulario." + var + "." + rs.getString("referencia")%>'>
                      <span class="<%= class_span1 %>" id='<%= var %>-<%= rs.getString("referencia") %>-span1'></span>
                      <span class="<%= class_span2 %>" id='<%= var %>-<%= rs.getString("referencia") %>-span2'></span>
                    </label>
                </div>
        
                  <%
                  bloquear = bloquear_aux;
                } 

                  else if(rs.getString("tipo_input").trim().equals("switchobs")){ 
                    String estado2 = "";
                    String ocultar = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String bloquear_aux = "";
                    System.out.println("#" + rs.getString("dato").trim().length() + "#");

                    if(rs.getString("dato").trim().equals("Normal") || !rs.getString("dato").trim().equals("")){
                      class_span1 = "onoffswitch-inner4";
                      class_span2 = "onoffswitch-switch4";
  
                      if(rs.getString("dato").trim().equals("Normal"))
                      {
                        estado2 = " ";
                        ocultar = "hidden";
                      }else {
                        estado2 = "checked";
                        ocultar = " ";
                      }
                    }else{
                      estado2 = " ";
                      ocultar = "hidden";

                      class_span1 = "onoffswitch-inner2-disabled";
                      class_span2 = "onoffswitch-switch2-disabled";
                      if(rs.getInt("estado_folio") == 0){
                        bloquear_aux = bloquear;
                        bloquear = "disabled";
                        activar = "activarSwitchobs('"+ var +"', '" + rs.getString("referencia") + "')";
                      }
                    }
                    %>  
                
                    <div class="onoffswitch4" style="float: left;" onclick="<%= activar %>">
                      <% if(rs.getInt("id_formula") > 0){ %>
                        <input type="checkbox" name='<%= var %>' class="onoffswitch-checkbox4" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>'  onclick="valorSwitchObservacion(this.id,this.checked);<%= actualizar %><%= validacion %>" <%=estado2%>  <%= bloquear %>  >
                        <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                        <input type="hidden" name="<%= var %>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                      <% } else if (rs.getInt("id_formula") < 1 ){ %>
                        <input type="checkbox" name='<%= var %>' class="onoffswitch-checkbox4" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>'  onclick="valorSwitchObservacion(this.id,this.checked);guardarDatosPlantilla('<%= var %>', '<%= rs.getString("referencia")%>' , this.value);<%= actualizar %><%= validacion %>" <%=estado2%>  <%= bloquear %>  >
                      <% } %>                      
                      <label class="onoffswitch-label4" for='<%= "formulario." + var + "." + rs.getString("referencia")%>'>
                        <span class="<%= class_span1 %>" id='<%= var %>-<%= rs.getString("referencia") %>-span1'></span>
                        <span class="<%= class_span2 %>" id='<%= var %>-<%= rs.getString("referencia") %>-span2'></span>
                      </label>
                  </div>
                  <% bloquear = bloquear_aux; %>
                  <label id='switchobservacion<%= "formulario." + var + "." + rs.getString("referencia")%>' <%= ocultar %> >
                  <div style="float: left; align-items: center;display: flex; width: 60%;font-size: 10px;" > 
                    <label style="padding-left: 10px; padding-right: 10px; ">Hallazgo</label>
                    <% if(rs.getInt("id_formula") > 0){ %>
                      <textarea maxlength='<%=rs.getString("maxlenght")%>' style='width: 100%; text-align: left; border: 1px solid #4297d7;' title='<%= rs.getString("referencia")%>' name='<%= var %>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' onkeypress='return validarKey(event,this.id);' onblur="<%= validacion %>" <%= bloquear %> ><%= rs.getString("dato").trim() %></textarea> 
                      <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                      <input type="hidden" name="<%= var %>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                    <% } else if(rs.getInt("formulas") > 0 ){ %>
                      <textarea maxlength='<%=rs.getString("maxlenght")%>' style='width: 100%; text-align: left; border: 1px solid #4297d7;' title='<%= rs.getString("referencia")%>' name='<%= var %>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>'  onkeypress='return validarKey(event,this.id);' onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);actualizarCampoFormula(this.id, this.value);<%= validacion %>" <%= bloquear %> ><%= rs.getString("dato").trim() %></textarea> 
                    <% } else if (rs.getInt("id_formula") < 1 ){ %>
                      <textarea maxlength='<%=rs.getString("maxlenght")%>' style='width: 100%; text-align: left; border: 1px solid #4297d7;' title='<%= rs.getString("referencia")%>' name='<%= var %>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>'   onkeypress='return validarKey(event,this.id);' onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion %>" <%= bloquear %> ><%= rs.getString("dato").trim() %></textarea>                  
                    <% } 
                  %>
                  </div>
                </label>
                 

          
                    <% } 

                    else if(rs.getString("tipo_input").trim().equals("textadc")){  %>
                      <textarea  style='width: <%=rs.getString("ancho")%>;height:<%=rs.getString("altura")%> ; text-align: left; border: 1px solid #4297d7;' 
                        type="text" title='<%= rs.getString("referencia")%>' id='<%= "txtTexArea" + rs.getString("referencia")%>' 
                        name='<%= rs.getString("referencia")%>' 
                        onkeypress="return validarKey(event,this.id)" placeholder="Ingresar texto..." 
                        onblur="guardarDatosPlantilla('<%= var %>', this.name, this.value);" <%= bloquear %> > <%= rs.getString("dato")%> </textarea>                                               
                  <% 
                  }

                else if(rs.getString("tipo_input").trim().equals("texta")){  %>
                    <textarea  style='width: <%=rs.getString("ancho")%>;height:<%=rs.getString("altura")%>; text-align: left; border: 1px solid #4297d7;' 
                      type="text" title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' 
                      name='<%= rs.getString("referencia")%>' 
                      onkeyup="return validarKey(event,this.id)" placeholder="Ingresar texto..." 
                      onblur="guardarDatosPlantilla('<%= var %>', this.name, this.value);" <%= bloquear %> > <%= rs.getString("dato")%> </textarea>                                               
                <% 
                }else if(rs.getString("tipo_input").trim().equals("combo")){  
                  if(rs.getInt("id_formula") > 0){ %>
                    <select size="1"  style='width: <%=rs.getString("ancho")%>; height: 20px; border: 1px solid #4297d7; border-radius: 5px; background-color: white;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= rs.getString("referencia")%>' value="" onblur="guardarDatosPlantilla('<%= var %>', this.name,  this.value );<%= actualizar %><%= validacion %>" <%= bloquear%> > 
                      <option value=""></option>
                      <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar( Integer.parseInt( rs.getString("id_query_combo")));  
                           ComboVO cmb; 
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(rs.getString("dato").trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>   
                    </select> 
                    <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                    <input type="hidden" name="<%= var %>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                  <% } else if (rs.getInt("id_formula") < 1 ){ %>
                    <select size="1"  style='width: <%=rs.getString("ancho")%>; height: 20px; border: 1px solid #4297d7; border-radius: 5px; background-color: white;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= rs.getString("referencia")%>' value="" onchange="guardarDatosPlantilla('<%= var %>', this.name,  this.value );<%= actualizar %><%= validacion %>" <%= bloquear%> >
                      <option value=""></option>
                      <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar( Integer.parseInt( rs.getString("id_query_combo")));  
                           ComboVO cmb; 
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(rs.getString("dato").trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>   
                     </select> 
                  <% } 
               } else if(rs.getString("tipo_input").trim().equals("comboMed")){  
                %>
                <input type="text" value='<%= rs.getString("dato").trim()%>' id="idMedEvaluacion" hidden>
                   <select size="1"  
                   style='width: <%=rs.getString("ancho")%>; height: 20px; border: 1px solid #4297d7; border-radius: 5px; background-color: white;' 
                   title='<%= rs.getString("referencia")%>' 
                   id="miComboMedEvaluacion"
                   name='<%= rs.getString("referencia")%>' 
                   value='<%= rs.getString("dato").trim()%>'  
                   onclick="comboMed()" onchange ="guardarDatosPlantilla('<%= var %>', this.name,  this.value )" <%= bloquear%> >                                                                   
                   </select>  
               <% 
             }else if(rs.getString("tipo_input").trim().equals("inputFecha")){
                if(rs.getInt("id_formula") > 0){ %>
                  <input type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var %>' value='<%= rs.getString("dato")%>' onchange="<%= actualizar %><%= validacion %>" <%= bloquear %> />
                  <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                  <input type="hidden" name="<%= var %>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                <% } else if (rs.getInt("id_formula") < 1 ){ %>
                  <input type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var %>' value='<%= rs.getString("dato")%>'  onchange="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>',  this.value );<%= actualizar %><%= validacion %>" <%= bloquear %> />   
                <% } 
            }else if(rs.getString("tipo_input").trim().equals("input2")){ %>

              <input  type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>;height:17px; margin-bottom:20px ;border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var %>' value='<%= rs.getString("dato")%>'  onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>',  this.value )" <%= bloquear %> />  

               <% } if(rs.getString("tipo_input").trim().equals("button")){ %>
               <div style="text-align: center; ">  <input type="button" value="GRAFICA" id='<%= "formulario." + var + "." + rs.getString("referencia")%>'  onclick="graficaPlantilla(<%=id_evolucion%>,'<%=rs.getString("referencia")%>')" class="small button blue" <%= bloquear %> >  </div>         
                <%}%>



              </td>  
          
     </tr> 
             
   <% }
   //stmt.close(); 
 }%>
 <!-- </tbody> --> 
<%} 
catch ( Exception e ){
System.out.println(e.getMessage());
}  %>

</table> 

</div>





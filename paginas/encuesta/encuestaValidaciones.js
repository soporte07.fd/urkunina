
//ACTIVAR SWITCH

$("#abc").mousedown(function(e){
    alert("#")
    //1: izquierda, 2: medio/ruleta, 3: derecho
     if(e.which == 3) 
         {
             alert("$")
         }
 });

function activarSwitchsn(tabla, referencia){

    $("#" + tabla + "-" + referencia + "-span1").removeClass("onoffswitch-inner2-disabled");
    $("#" + tabla + "-" + referencia + "-span2").removeClass("onoffswitch-switch2-disabled");

    $("#" + tabla + "-" + referencia + "-span1").addClass("onoffswitch-inner2");
    $("#" + tabla + "-" + referencia + "-span2").addClass("onoffswitch-switch2");

    setTimeout(() => {
        document.getElementById("formulario." + tabla + "." + referencia).disabled = false;
        valorSwitchsn("formulario." + tabla + "." + referencia, document.getElementById("formulario." + tabla + "." + referencia).checked)
        setTimeout(() => {
            guardarDatosPlantilla(tabla, referencia, document.getElementById("formulario." + tabla + "." + referencia).value)
        }, 200);
    }, 200); 
}

function activarSwitchS10(tabla, referencia){

    $("#" + tabla + "-" + referencia + "-span1").removeClass("onoffswitch-inner2-disabled");
    $("#" + tabla + "-" + referencia + "-span2").removeClass("onoffswitch-switch2-disabled");

    $("#" + tabla + "-" + referencia + "-span1").addClass("onoffswitch-inner2");
    $("#" + tabla + "-" + referencia + "-span2").addClass("onoffswitch-switch2");

    setTimeout(() => {
        document.getElementById("formulario." + tabla + "." + referencia).disabled = false;
        valorSwitch10("formulario." + tabla + "." + referencia, document.getElementById("formulario." + tabla + "." + referencia).checked)
        setTimeout(() => {
            guardarDatosPlantilla(tabla, referencia, document.getElementById("formulario." + tabla + "." + referencia).value)
        }, 200);
    }, 200); 
}

function activarSwitch10(tabla, referencia){

    $("#" + tabla + "-" + referencia + "-span1").removeClass("onoffswitch-inner2-disabled");
    $("#" + tabla + "-" + referencia + "-span2").removeClass("onoffswitch-switch2-disabled");

    $("#" + tabla + "-" + referencia + "-span1").addClass("onoffswitch-inner3");
    $("#" + tabla + "-" + referencia + "-span2").addClass("onoffswitch-switch3");

    setTimeout(() => {
        document.getElementById("formulario." + tabla + "." + referencia).disabled = false;
        valorSwitch10("formulario." + tabla + "." + referencia, document.getElementById("formulario." + tabla + "." + referencia).checked)
        setTimeout(() => {
            guardarDatosPlantilla(tabla, referencia, document.getElementById("formulario." + tabla + "." + referencia).value)
        }, 200);
    }, 200); 
}

function activarSwitchobs(tabla, referencia){

    $("#" + tabla + "-" + referencia + "-span1").removeClass("onoffswitch-inner2-disabled");
    $("#" + tabla + "-" + referencia + "-span2").removeClass("onoffswitch-switch2-disabled");

    $("#" + tabla + "-" + referencia + "-span1").addClass("onoffswitch-inner4");
    $("#" + tabla + "-" + referencia + "-span2").addClass("onoffswitch-switch4");

    setTimeout(() => {
        document.getElementById("formulario." + tabla + "." + referencia).disabled = false;
        valorSwitchObservacion("formulario." + tabla + "." + referencia, document.getElementById("formulario." + tabla + "." + referencia).checked)
        setTimeout(() => {
            guardarDatosPlantilla(tabla, referencia, document.getElementById("formulario." + tabla + "." + referencia).value)
        }, 200);
    }, 200); 
}


//  clases para el funcionamiento de la notacion formulas

class Stack {
    constructor() {
        this.items = {}
        this.top = 0
    }

    push(data) {
        this.top++
        this.items[this.top] = data
    }

    pop() {
        let outData
        if (this.top) {
            outData = this.items[this.top]
            delete this.items[this.top]
            this.top--
            return outData
        }
    }

    getSize() {
        return this.top
    }

    isEmpty() {
        return this.top == 0
    }

    peek() {
        return (this.getSize() == 1) ? null : this.items[this.top - 1]
    }

    getTop() {
        return (this.isEmpty()) ? null : this.items[this.top]
    }

    getPos(pos) {
        return (this.isEmpty()) ? null : this.items[pos]
    }

    print() {
        return Object.assign([], this.items).reverse()
    }
}

const isOperator = (operator) => ['(',')',')','*','/','^','+','-','>','<','>=','<=','!','&&','||','==','!=','IF','THEN','ELSE','ENDIF'].includes(operator)

const hightPrecedence = (operator) => (getPrecedence(stack.getTop()) < getPrecedence(operator))

const getPrecedence = (operator) => {
    if ('!^'.includes(operator)) return 8
    if ('*/'.includes(operator)) return 7
    if ('+-'.includes(operator)) return 6
    if ('<><=>='.includes(operator)) return 5
    if ('==!='.includes(operator)) return 4
    if (operator == '&&') return 3
    if (operator == '||') return 2
    if ('IFTHENELSEENDIF('.includes(operator)) return 1
}

const conversor = (parameter) => {
    output = ''
    stack = new Stack()

    parameter.split(' ').map(e => {
        if (isOperator(e)) {
            if (['(','IF'].includes(e)) {
                stack.push(e)
            } else if ([')','THEN','ELSE','ENDIF'].includes(e)) {
                if(e == ')'){
                    while(stack.getTop()!='(' && !stack.isEmpty()) {
                        output += `${stack.pop()} `;
                    }
                    stack.pop();
                }
                else if(e == 'ENDIF'){
                    while(stack.getTop()!='IF' && !stack.isEmpty()) {
                        output += `${stack.pop()} `;
                    }
                    output += `${stack.pop()} `;
                }
            } else {
                if (hightPrecedence(e)) {
                    stack.push(e);
                } else {
                    if (!stack.isEmpty()) 
                        output += `${stack.pop()} `;
                    stack.push(e);
                }
            }
        } else {
            output += `${e} `;
        }
    })

    if (!stack.isEmpty()) {
        stack.print().map(st => {
            if (st != '(') 
                output += `${stack.pop()} `;
            else
                stack.pop();
        });
    }
    return output
}

const evaluator = (expresion_rpn) => {
    stack = new Stack()
    entry = expresion_rpn.split(' ')
    entry.pop()

    entry.map(e => {
        if(!isOperator(e)) {
            if(isNaN(e))
                e = "'"+e+"'";
            stack.push(e)
        } else {
            if(e == 'IF'){
                var ifFalse = stack.pop();
                var ifTrue = stack.pop();
                if(stack.getTop()==null){
                    if(ifTrue)
                        stack.push(ifFalse);
                }else{
                    if(stack.pop())
                        stack.push(ifTrue);
                    else
                        stack.push(ifFalse); 
                }
            }else if(e == '!'){
                aux = !stack.pop();
                stack.push(aux);
            }else{      
                aux = eval(`${stack.peek()} ${ e == '^' ? '**' : e } ${stack.getTop()}`);
                stack.pop();
                stack.pop();
                stack.push(aux);
            }
        }
    })

    return stack.getTop()
}

const setOperation = (event) => {
   

    if (event.key == 'Enter' ) {
        rpn = conversor(event.target.value)
        
        //eso es para evaluar
        //res = evaluator(rpn)

        document.getElementById('main_entry').innerText = event.target.value
        document.getElementById('conversion').innerText = rpn


        //para evaluar 
        //document.getElementById('operation').innerText = res
        event.target.value = ''
    }
}


// fin de las clases para la notacion 


function calFormula(formula) {
    if(document.getElementById('campoResultado'+formula) == null)
        return; 
    camResultado= document.getElementById('campoResultado'+formula).value;
    var tabla = document.getElementById('campoResultado'+formula).getAttribute('name');
    output = ''
    stack = new Stack()
    var formula = document.getElementById('campoFormula'+formula).value;
    var campoVacio = false;
    formula.split(' ').map(e => {
       
        if(e != ' '){ 
            if(!isOperator(e)) { 
                if ($('#listGrillaFormulas td[title='+e+']').closest('tr').find('td:nth-child(2)').html()==e) {
                    var va = 0;
                    va = $('td[title='+e+']').closest('tr').find('td:nth-child(3)').html();
                    if (va == '' || va == '&nbsp;') {
                        campoVacio = true;
                        va = 0;
                        output += `${va} `
                    } else {
                        output += `${va} `
                    }
                }else{
                    output += `${e} `
                }
            }
            else{
                output += `${e} `
            }
        }
    })
    if(campoVacio)
        return;
    fds = evaluator(output)
    console.log(evaluator(output))    
    var campo = 'formulario.'+tabla+'.'+camResultado;
    var campoTest = $(document.getElementById(campo));
    var res = fds;
    if (!isNaN(fds)){
        if (fds % 1 == 0) {
            res =  fds;
        } else {
            var fds2 = parseFloat(Math.round(fds * 100) / 100).toFixed(2);
            res =  fds2;
        }
    }
    else{
        res = fds.replace(/'/g,'');
    }

    if(document.getElementById(campo).type=="text"){
        document.getElementById(campo).value = res;
        campoTest.trigger("keyup");
        campoTest.trigger("blur");                                    
    }
    else if(document.getElementById(campo).type=="checkbox"){
        if(("SI1".includes(res) && !document.getElementById(campo).checked) || ("NO0".includes(res) && document.getElementById(campo).checked)){
            document.getElementById(campo).click();
        }                                   
    }
    else{                              
        document.getElementById(campo).value = res;
        campoTest.trigger("change"); 
    }
    //guardarDatosPlantilla(tabla,camResultado,document.getElementById('formulario.'+tabla+'.'+camResultado).value);
    //actualizarCampoFormula('formulario.'+tabla+'.'+camResultado, document.getElementById('formulario.'+tabla+'.'+camResultado).value);     
}

//Validacion para campos formula
function actualizarCampoFormula(campo, val){
    if($('#listGrillaFormulas td[title='+campo+']').html() == null)
        return;
    $('#listGrillaFormulas td[title='+campo+']').closest('tr').find('td:nth-child(3)').html(val);
    $('#listGrillaFormulas td[title='+campo+']').closest('tr').find('td:nth-child(4)').html().split(',').map(e => {
        if(e != ' '){
            calFormula(e);
        }
    });
}

//realiza los cálculos en los campos de formula al abrir el folio
function calculosIniciales(tipo){
    $('#div_'+tipo+' .campoFormula').each(function(){
        var formula = $(this).attr("id").substring(12,$(this).attr("id").length);
        if(formula != null)
            calFormula(formula);
    });

    /*$('#listGrillaFormulas > tbody > tr').each(function(){
        var campo = $(this).find('td:nth-child(2)').html();
        if(campo != null)
            if(campo.includes(tipo)){
                var valor = $(this).find('td:nth-child(3)').html();
                actualizarCampoFormula(campo,valor);
            }
    });*/
}

//realiza los cálculos en los campos de formula al abrir el folio
function validacionesIniciales(tipo){
    var validaciones = "";
    var validacionActual = "";
    $('#listGrillaValidaciones > tbody > tr').each(function(){
        if($(this).find('td:nth-child(3)').html().includes(tipo)){
            if( validacionActual != $(this).find('td:nth-child(2)').html()){                
                validacionActual = $(this).find('td:nth-child(2)').html();
                validaciones = validaciones + validacionActual + ",";
            }
        }
    });
    if(validaciones != "")
        ejecutarValidaciones(validaciones);
}

function validarCampo(referencia, validaciones){
    if($('#listGrillaValoresValidaciones td[title='+referencia+']').html() == null)
        return;
    var val = document.getElementById(referencia).value;
    $('#listGrillaValoresValidaciones td[title='+referencia+']').closest('tr').find('td:nth-child(3)').html(val);
    ejecutarValidaciones(validaciones);
}

//Ejecuta las validaciones correspondientes a dicho campo
function ejecutarValidaciones(validaciones){    
    validaciones.split(',').map(e => {
        if(e != ""){
            var output = "";
            var validacion = "";
            //$('#listGrillaValidaciones > tbody > tr').each(function(){
            $('.' + e).each(function(){
                var campo = $(this).find('td:nth-child(3)').html();
                if($(this).find('td:nth-child(7)').html() == '1'){
                    validacion = $(this).find('td:nth-child(2)').html();
                    var operando = $(this).find('td:nth-child(4)').html();
                    if(operando == '&gt;')
                        operando = '>';
                    if(operando == '&lt;')
                        operando = '<';                
                    var valor =  $(this).find('td:nth-child(5)').html();
                    var campovalor = $('#listGrillaValoresValidaciones td[title='+campo+']').closest('tr').find('td:nth-child(3)').html();
                    if(output != "")
                        output += ' && ' 
                    output += campovalor + ' ' + operando + ' ' + valor;
                }           
            });
            output = conversor(output);
            var condicion = evaluator(output);
            $('.'+ e).each(function(){
                var codval = $(this).find('td:nth-child(2)').html();
                if(codval != null){
                    if(codval.includes(validacion)){
                        if($(this).find('td:nth-child(7)').html() == '2'){
                            var campo = $(this).find('td:nth-child(3)').html();
                            if(document.getElementById(campo) == null)
                                return;
                            var operando = $(this).find('td:nth-child(4)').html();
                            var valor =  $(this).find('td:nth-child(5)').html();
                            if(operando == 'V'){
                                if(valor == 'DISABLED' && condicion)
                                    document.getElementById(campo).setAttribute('disabled', 'true');
                                else
                                    document.getElementById(campo).removeAttribute('disabled');
                            }
                            else if(operando == '='){
                                if(condicion){                                
                                    if(document.getElementById(campo).type=="text"){
                                        document.getElementById(campo).value = valor;
                                        $(document.getElementById(campo)).trigger("change");
                                        $(document.getElementById(campo)).trigger("blur");                                    
                                    }
                                    else if(document.getElementById(campo).type=="checkbox"){
                                        if(("SI1".includes(valor) && !document.getElementById(campo).checked) || ("NO0".includes(valor) && document.getElementById(campo).checked)){
                                            document.getElementById(campo).click();
                                        }                                   
                                    }
                                    else{                              
                                        document.getElementById(campo).value = valor;
                                        $(document.getElementById(campo)).trigger("change");
                                    }
                                }
                            }
                        }                
                    }
                }            
            });
        }
    });    
}

// valida el tipo de campo por su jerarquia para bloquear los campos que no seran necesarios
function valTipoCampo(){
    var jerarquia = document.getElementById('cmbJerarquia').value;
    
    if (jerarquia == 'TI' || jerarquia == 'PA') {
        $("#cmbTipoEntrada").attr("disabled", 'true');
        $("#txtReferencia").attr("disabled", 'true');
        $("#cmbConsultaCombo").attr("disabled", 'true');
        $("#txtAncho").attr("disabled", 'true');
        $("#txtMaxlenght").attr("disabled", 'true');
        $("#txtValorDefecto").attr("disabled", 'true');

        document.getElementById('cmbTipoEntrada').value = " ";
        document.getElementById('txtReferencia').value = " ";
        document.getElementById('cmbConsultaCombo').value = "0";
        document.getElementById('txtAncho').value = " ";
        document.getElementById('txtMaxlenght').value = " ";
        document.getElementById('txtValorDefecto').value = " ";
    } else if(jerarquia == 'HI'){
        $("#cmbTipoEntrada").removeAttr("disabled");
        $("#txtReferencia").removeAttr("disabled");
        $("#cmbConsultaCombo").removeAttr("disabled");
        $("#txtAncho").removeAttr("disabled");
        $("#txtMaxlenght").removeAttr("disabled");
        $("#txtValorDefecto").removeAttr("disabled");  
    }

}

//bloquea el combo consulta si los parametros de entrada son textarea o input
function validacionTipoEntrada() {
    
    var entrada = document.getElementById('cmbTipoEntrada').value;
    
    if (entrada == 'input' || entrada == 'texta') {
     
        $("#cmbConsultaCombo").attr("disabled", 'true');
        document.getElementById('cmbConsultaCombo').value = "0";

    } else if(entrada == 'combo'){
        $("#cmbConsultaCombo").removeAttr("disabled");
    }
}


//coloca el value y funcion del boton dependiendo si el item de la plantilla tiene o no formula
function valBotonFormula() {
    
    var numFormula = document.getElementById('txtidFormula').value;
    if (numFormula < 1 || numFormula == ' ') {

        document.getElementById('btnFormula').removeAttribute('hidden');
        document.getElementById('btnFormula').value = "CREAR FORMULA";
        document.getElementById('btnFormula').setAttribute('onclick','crearFormula()');
        document.getElementById('btnFormula').setAttribute('class', 'small button blue');
    } else if(numFormula > 0 ){
        
        document.getElementById('btnFormula').removeAttribute('hidden');
        document.getElementById('btnFormula').value = "VER FORMULA";
        document.getElementById('btnFormula').setAttribute('onclick','modificarFormula()');
        document.getElementById('btnFormula').setAttribute('class', 'small button blue');

    }
    
}

//carga los analitos según el laboratorio
function cargarLaboratorioDetalle(comboOrigen, comboDestino){
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 1252, valorAtributo(comboOrigen));
}

//Carga las tablas según el esquema
function cargarTablasEsquema(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 900, valorAtributo(comboOrigen));
}

//Carga los campos según la tabla
function cargarCamposDesdeEsquemaTabla(comboOrigen1, comboOrigen2, comboDestino) {
    cargarComboGRALCondicion2('cmbPadre', '1', comboDestino, 898, valorAtributo(comboOrigen1), valorAtributo(comboOrigen2));
    if(comboDestino != 'cmbIdentificador'){
        $('#chkIdentificador').attr('checked',true);
        $('#chkIdentificador').trigger('change');
        $('#cmbIdentificador').empty().append('<option>Seleccione Identificador</option>');
        $('#txtIdentificador').val('');
    }
}

//Carga los campos según la tabla actual
function cargarCampos(esquema, tabla, comboDestino) {
    cargarComboGRALCondicion2('cmbPadre', '1', comboDestino, 898, esquema, valorAtributo(tabla));
}

//Cambia el identificador para la tabla
function cambiarIdentificador(){
    if($('#chkIdentificador').attr('checked')){
        document.getElementById('cmbIdentificador').setAttribute('hidden','true');    
        document.getElementById('txtIdentificador').setAttribute('hidden','true');
    }
    else{
        document.getElementById('cmbIdentificador').removeAttribute('hidden');
        document.getElementById('txtIdentificador').removeAttribute('hidden');
    }
}

//Cargar campos validaciones
function cargarCampoValidacion(esquema, tabla, campo, resultado){
    var esquema = $("#" + esquema).val();
    var tabla = $("#" + tabla).val();
    var campo = $("#" + campo).val();
    $("#"+resultado).val(esquema + "." + tabla + "." + campo);
}

//hacen visibles los campos para la formula
function crearFormula() {

    limpiarFormula(0)

    document.getElementById('agregarFormula1').removeAttribute('hidden');
    document.getElementById('agregarFormula2').removeAttribute('hidden');
    document.getElementById('agregarFormula3').removeAttribute('hidden');
    document.getElementById('agregarValorFormula').removeAttribute('hidden');
    document.getElementById('separadorFormula').removeAttribute('hidden');
    document.getElementById('campoResultadoFormula').removeAttribute('hidden');
    document.getElementById('construirFormula').removeAttribute('hidden');
    document.getElementById('construirFormula2').removeAttribute('hidden');

    construirFormula

    document.getElementById('btnGuardarFormula').removeAttribute('hidden');
    document.getElementById('btnGuardarFormula').value = "GUARDAR FORMULA";
    document.getElementById('btnGuardarFormula').setAttribute('onclick', 'modificarCRUD("crearFormula", "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp")');
    document.getElementById('btnGuardarFormula').setAttribute('class', 'small button blue');
    

    var campoResultado = document.getElementById('txtReferencia').value;
    document.getElementById('txtCampoResultado').value = campoResultado
    
}


function modificarFormula() {

    limpiarFormula(0)

    document.getElementById('agregarFormula1').removeAttribute('hidden');
    document.getElementById('agregarFormula2').removeAttribute('hidden');
    document.getElementById('agregarFormula3').removeAttribute('hidden');
    document.getElementById('agregarValorFormula').removeAttribute('hidden');
    document.getElementById('separadorFormula').removeAttribute('hidden');
    document.getElementById('campoResultadoFormula').removeAttribute('hidden');
    document.getElementById('construirFormula').removeAttribute('hidden');
    document.getElementById('construirFormula2').removeAttribute('hidden');

    document.getElementById('btnGuardarFormula').removeAttribute('hidden');
    document.getElementById('btnGuardarFormula').value = "MODIFICAR FORMULA";
    document.getElementById('btnGuardarFormula').setAttribute('onclick', 'modificarCRUD("modificarFormula", "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp")');
    document.getElementById('btnGuardarFormula').setAttribute('class', 'small button blue');

    var campoResultado = document.getElementById('txtOcultoCampoResultado').value
    document.getElementById('txtCampoResultado').value = campoResultado
    
    var formula = document.getElementById('txtOcultoFormulaEditar').value;
    document.getElementById('txtFormulaCompleta').value = formula;

    var campos = document.getElementById('txtOcultoCamposFormula').value;
    document.getElementById('txtCamposFormula').value = campos;

    formula.split(' ').map(e => {
        if(e != ' '){

            var valor = `${e}`
            var agregaFormula = document.getElementById('construFormula');
            var newOption = document.createElement('option');
            newOption.value = valor;
            newOption.text = valor;
            if(campos.includes(e))
                newOption.className = "optionFormula optOperando"; 
            else
                newOption.className = "optionFormula";
            agregaFormula.add(newOption, null);

        }
    })
           
}


function limpiarFormula(estado) {
    

    if (estado == 1) {
        document.getElementById('agregarFormula1').setAttribute('hidden','true');
        document.getElementById('agregarFormula2').setAttribute('hidden','true');
        document.getElementById('agregarFormula3').setAttribute('hidden','true');
        document.getElementById('agregarValorFormula').setAttribute('hidden','true');
        document.getElementById('separadorFormula').setAttribute('hidden','true');
        document.getElementById('campoResultadoFormula').setAttribute('hidden','true');
        document.getElementById('construirFormula').setAttribute('hidden','true');
        document.getElementById('construirFormula2').setAttribute('hidden','true');

        document.getElementById('txtCampoResultado').value = "";
        document.getElementById('valorAdicional').value = "";
        document.getElementById('txtFormulaCompleta').value = "";
        document.getElementById('txtCamposFormula').value = "";
        var selec = document.getElementById('construFormula')
        var i;
            for(i = selec.options.length - 1 ; i >= 0 ; i--)
            {
                selec.remove(i);
            }
        
        document.getElementById('btnGuardarFormula').removeAttribute('class', 'small button blue');
        document.getElementById('btnGuardarFormula').setAttribute('hidden', 'true');

        buscarHistoria('listGrillaPlantillaElemento')
        valBotonFormula();

        
    } else {

        document.getElementById('txtCampoResultado').value = "";
        document.getElementById('valorAdicional').value = "";
        document.getElementById('txtFormulaCompleta').value = "";
        var selec = document.getElementById('construFormula')
        var i;
            for(i = selec.options.length - 1 ; i >= 0 ; i--)
            {
                selec.remove(i);
            }

        document.getElementById('btnGuardarFormula').removeAttribute('class', 'small button blue');
        document.getElementById('btnGuardarFormula').setAttribute('hidden', 'true');

        valBotonFormula();
        
    }

    
}


//agrega el laboratorio al campo para construir la formula
function pasarLaboratorioFormula() {
    var campo = document.getElementById('construFormula');
    camposelect = campo.selectedIndex;
    numcampos = campo.options.length;
   
    var operando = document.getElementById('cmbLaboratorioDetalle');
    var agregaFormula = document.getElementById('construFormula');
    var valor = "laboratorio."+document.getElementById('cmbLaboratorio').value+"."+document.getElementById('cmbLaboratorioDetalle').value;
    var newOption = document.createElement('option');
    newOption.value = valor;
    newOption.text = valor;
    newOption.className = "optionFormula optOperando"; 

    if ( numcampos == camposelect+1 ) {
        agregaFormula.add(newOption, null);             
    } else if (camposelect == 0) {
        agregaFormula.prepend(newOption);  
    }else{
        agregaFormula.insertBefore(newOption,agregaFormula.options[camposelect]);         
    }
    sacarvalorsele( );
}

//remplaza el laboratorio seleccionada en el campo de contruccion de formula 
function cambiarOperando() {
    var operador = document.getElementById('cmbLaboratorioDetalle');
    var valor = "laboratorio."+document.getElementById('cmbLaboratorio').value+"."+document.getElementById('cmbLaboratorioDetalle').value;
    var cambiarcampo = document.getElementById('construFormula');
    cambiarcampo =  cambiarcampo.options[cambiarcampo.selectedIndex]
    cambiarcampo.value = valor;
    cambiarcampo.text = valor;
    sacarvalorsele( );
}

 
    //agrega la variable al campo para construir la formula
    function pasarOperando() {
        var campo = document.getElementById('construFormula');
        camposelect = campo.selectedIndex;
        numcampos = campo.options.length;
       
        var operando = document.getElementById('cmbRefCampo');
        var agregaFormula = document.getElementById('construFormula');
        var valor = document.getElementById('cmbEsquema').value+"."+document.getElementById('cmbTabla').value+"."+document.getElementById('cmbRefCampo').value;
        if(!$('#chkIdentificador').attr('checked'))
            if($('#txtIdentificador').val() == '')
                valor += '$'+$('#cmbIdentificador').val();
            else 
                valor += '$'+$('#cmbIdentificador').val()+'.'+$('#txtIdentificador').val();
        var newOption = document.createElement('option');
        newOption.value = valor;
        newOption.text = valor;
        newOption.className = "optionFormula optOperando"; 

        if ( numcampos == camposelect+1 ) {
            agregaFormula.add(newOption, null);             
        } else if (camposelect == 0) {
            agregaFormula.prepend(newOption);  
        }else{
            agregaFormula.insertBefore(newOption,agregaFormula.options[camposelect]);         
        }
        sacarvalorsele( );
    }

    //agrega el simbolo al campo para construir la formula
    function pasarOperadorFormula() {
        var campo = document.getElementById('construFormula');
        camposelect = campo.selectedIndex;
        numcampos = campo.options.length;
        var operador = document.getElementById('cmbOperador');
        var agregaFormula = document.getElementById('construFormula');
        var valor = operador.value;
        var newOption = document.createElement('option');
        newOption.value = valor;
        newOption.text = valor;
        newOption.className = "optionFormula"; 
        
        if ( numcampos == camposelect+1 ) {
            agregaFormula.add(newOption, null);             
        } else if (camposelect == 0) {
            agregaFormula.prepend(newOption);  
        }else{
            agregaFormula.insertBefore(newOption,agregaFormula.options[camposelect]); 
        }
        sacarvalorsele( );
      
    }


    //remplaza la variable seleccionada en el campo de contruccion de formula 
    function cambiarOperando() {
        var operador = document.getElementById('cmbRefCampo');
        var valor = document.getElementById('cmbEsquema').value+"."+document.getElementById('cmbTabla').value+"."+document.getElementById('cmbRefCampo').value;
        if(!$('#chkIdentificador').attr('checked'))
            if($('#txtIdentificador').val() == '')
                valor += '$'+$('#cmbIdentificador').val();
            else 
                valor += '$'+$('#cmbIdentificador').val()+'.'+$('#txtIdentificador').val();
        var cambiarcampo = document.getElementById('construFormula');
        cambiarcampo =  cambiarcampo.options[cambiarcampo.selectedIndex]
        cambiarcampo.value = valor;
        cambiarcampo.text = valor;
        sacarvalorsele( );
    }

    //remplaza la variable seleccionada en el campo de contruccion de formula 
    function cambiarOperador() {

        var operador = document.getElementById('cmbOperador');
        var valor = operador.value;

        var cambiarcampo = document.getElementById('construFormula');
        cambiarcampo =  cambiarcampo.options[cambiarcampo.selectedIndex]
        cambiarcampo.value = valor;
        cambiarcampo.text = valor;
        sacarvalorsele( );  
    }

    function eliminarCampo( ) {

        var campo = document.getElementById('construFormula');
        camposelect = campo.selectedIndex;
        numcampos = campo.options.length;
        if ( camposelect != -1 && camposelect+1 < numcampos  )  {
        
            var eliminarcampo = document.getElementById('construFormula');
            eliminarcampo =  eliminarcampo.options[eliminarcampo.selectedIndex]
            eliminarcampo.remove();
            
        }else{
            var eliminarcampo = document.getElementById('construFormula');
            eliminarcampo =  eliminarcampo.options[numcampos-1]
            eliminarcampo.remove();
        }        
        sacarvalorsele( );
    }

    function adicionarValorFormula() {
        var valorAdicional = document.getElementById('valorAdicional');
        var agregaFormula = document.getElementById('construFormula');
        var valor = valorAdicional.value;
        var newOption = document.createElement('option');
        newOption.value = valor;
        newOption.text = valor;
        newOption.className = "optionFormula"; 

        agregaFormula.add(newOption, null);
        sacarvalorsele( );
        
    }

    function cambiarValor( ) {

        var valorAdicional = document.getElementById('valorAdicional');
        var valor = valorAdicional.value;

        var cambiarcampo = document.getElementById('construFormula');
        cambiarcampo =  cambiarcampo.options[cambiarcampo.selectedIndex]
        cambiarcampo.value = valor;
        cambiarcampo.text = valor;
        sacarvalorsele( );  
    }
    
    function sacarvalorsele( ) {
        var miformula = '';
        var misCampos = '';
        $("#construFormula option").each(function(){
            //alert('opcion '+$(this).text()+' valor '+ $(this).attr('value'))
            var campo = $(this).attr('value');
            miformula += `${campo} `;
            if($(this).hasClass('optOperando') && !misCampos.includes($(this).attr('value')))
                misCampos += campo+",";

        });
        $('#txtCamposFormula').val(misCampos);
        return document.getElementById('txtFormulaCompleta').value = miformula;       
    }



 
function sacarposicion() {
    var posiciones = '';
        $("#construFormula option").each(function(){
            //alert('opcion '+$(this).text()+' valor '+ $(this).attr('value'))
            var campo = $(this).index();
            posiciones += `${campo} `

         });
         alert('pos= ' + posiciones)
    
}
//OCULTA TABS EN CONDUCTA Y TRATAMIENTO PARA FOLIOS DE ENFERMERIA..
function validacionTabsFolios() { /*alert(555)*/
    setTimeout(() => {
    		//alert("Ingresé a tabs")
            if (valorAtributo('lblTipoDocumento')=='ENFE') {
            //alert("INGRESO A DIVS"); 
                console.log("Ingreso a ENFE")            
                if(document.getElementById('idproc')!= null) ocultar('idproc'); ocultar('divProcedimiento');//procedimientos
                if(document.getElementById('idmed')!= null) ocultar('idmed'); //medicamentos
                if(document.getElementById('idconmed')!= null) ocultar('idconmed'); //conciliación medicamentosa
                if(document.getElementById('idrefcon')!= null) ocultar('idrefcon'); //referencia - contrarref
                if(document.getElementById('idcons')!= null) ocultar('idcons'); //Consentimientos
                if(document.getElementById('idadmpac')!= null) ocultar('idadmpac'); //Gestion administrativa    
                if(document.getElementById('idedpac')!= null) mostrar('idedpac'); //educacion paciente
                if(document.getElementById('idimpr')!= null) ocultar('idimpr'); //impresión

           
            }else if (valorAtributo('lblTipoDocumento')=='RGDE') {
            // alert("INGRESO A DIVS"); 
                console.log("Ingreso a RGDE")
                if(document.getElementById('idproc')!= null) ocultar('idproc'); ocultar('divProcedimiento'); //procedimientos
                if(document.getElementById('idmed')!= null) ocultar('idmed'); //medicamentos
                if(document.getElementById('idconmed')!= null) ocultar('idconmed'); //conciliación medicamentosa
                if(document.getElementById('idrefcon')!= null) ocultar('idrefcon'); //referencia - contrarref
                if(document.getElementById('idcons')!= null) ocultar('idcons'); //Consentimientos
                if(document.getElementById('idadmpac')!= null) ocultar('idadmpac'); //Gestion administrativa    
                if(document.getElementById('idedpac')!= null) mostrar('idedpac'); //educacion paciente
                if(document.getElementById('idimpr')!= null) ocultar('idimpr'); //impresión

            }else if (valorAtributo('lblTipoDocumento')=='EVNE') {
                //alert("INGRESO A DIVS"); 
                console.log("Ingreso a EVNE")
                if(document.getElementById('idproc')!= null) ocultar('idproc'); ocultar('divProcedimiento');//procedimientos
                if(document.getElementById('idmed')!= null) ocultar('idmed'); //medicamentos
                if(document.getElementById('idconmed')!= null) ocultar('idconmed'); //conciliación medicamentosa
                if(document.getElementById('idrefcon')!= null) ocultar('idrefcon'); //referencia - contrarref
                if(document.getElementById('idcons')!= null) ocultar('idcons'); //Consentimientos
                if(document.getElementById('idadmpac')!= null) ocultar('idadmpac'); //Gestion administrativa    
                if(document.getElementById('idedpac')!= null) mostrar('idedpac'); //educacion paciente
                if(document.getElementById('idimpr')!= null) ocultar('idimpr'); //impresión

                
            } else if (valorAtributo('lblTipoDocumento')=='EVDT') {
                //alert("Ingreso a EVDT")
                if(document.getElementById('idproc')!= null) ocultar('idproc'); ocultar('divProcedimiento');//procedimientos
                if(document.getElementById('idmed')!= null) ocultar('idmed'); //medicamentos
                if(document.getElementById('idconmed')!= null) ocultar('idconmed'); //conciliación medicamentosa
                if(document.getElementById('idrefcon')!= null) ocultar('idrefcon'); //referencia - contrarref
                if(document.getElementById('idcons')!= null) ocultar('idcons'); //Consentimientos
                if(document.getElementById('idadmpac')!= null) mostrar('idadmpac'); //Gestion administrativa    
                if(document.getElementById('idedpac')!= null) mostrar('idedpac'); //educacion paciente
                if(document.getElementById('idimpr')!= null) ocultar('idimpr'); //impresión
           
            } else if (valorAtributo('lblTipoDocumento')=='MEDG') {
                console.log("Ingreso a MEDG")
                if(document.getElementById('idproc')!= null){ mostrar('idproc'); /*alert(666); *//*ocultar('divProcedimiento');*/}//procedimientos
                if(document.getElementById('idmed')!= null) mostrar('idmed'); //medicamentos
                if(document.getElementById('idconmed')!= null) ocultar('idconmed'); //conciliación medicamentosa
                if(document.getElementById('idrefcon')!= null) mostrar('idrefcon'); //referencia - contrarref
                if(document.getElementById('idcons')!= null) mostrar('idcons'); //Consentimientos
                if(document.getElementById('idadmpac')!= null) mostrar('idadmpac'); //Gestion administrativa    
                if(document.getElementById('idedpac')!= null) mostrar('idedpac'); //educacion paciente
                if(document.getElementById('idimpr')!= null) mostrar('idimpr'); //impresión 
            }else if (valorAtributo('lblTipoDocumento')=='HPSP') {
                console.log("Ingreso a HPSP")
                if(document.getElementById('idproc')!= null){ ocultar('idproc'); /*alert(666); *//*ocultar('divProcedimiento');*/}//procedimientos
                if(document.getElementById('idmed')!= null) ocultar('idmed'); //medicamentos
                if(document.getElementById('idconmed')!= null) ocultar('idconmed'); //conciliación medicamentosa
                if(document.getElementById('idrefcon')!= null) ocultar('idrefcon'); //referencia - contrarref
                if(document.getElementById('idcons')!= null) ocultar('idcons'); //Consentimientos
                if(document.getElementById('idadmpac')!= null) ocultar('idadmpac'); //Gestion administrativa    
                if(document.getElementById('idedpac')!= null) mostrar('idedpac'); //educacion paciente
                if(document.getElementById('idimpr')!= null) ocultar('idimpr'); //impresión
                if(document.getElementById('idIncap')!= null) ocultar('idIncap'); //impresión
            }
    }, 600);
}

function valorSwitch10(id, check) {
  
    if(check == true){
        document.getElementById(id).value = "1";  
    }else{
        document.getElementById(id).value = "0";
    }
}

function valorSwitchsn(ids, checks) {
    if(checks == true){
        document.getElementById(ids).value = "SI";  
    }else{
        document.getElementById(ids).value = "NO";
    }
    
}

function valorSwitchObservacion(ids, checks) {
    rec = ids;
    if(checks == true){
        document.getElementById('switchobservacion'+ids).removeAttribute('hidden');
        document.getElementById(ids).value = "";
        
        
    }else if (checks == false){
        document.getElementById(ids).value = "Normal";
        document.getElementById('switchobservacion'+ids).setAttribute('hidden', 'true');
    }
    
}

function colocarRiesgoEncuesta( riesgo, semaforo, id_diagnostico_conducta, diagnostico, conducta, medio_recepcion) {
    
   
    
    var campoRiesgo = document.getElementById('lblSemaforoRiesgoEncuesta');
    var boton = document.getElementById('btnRecomendaciones');
    var campoSede = document.getElementById('cmbSede1');
    var boton1 = document.getElementById('btnCrearAdmision');
    var campoDiagnostico = document.getElementById('divDiagnostico');
    var campoConducta = document.getElementById('divConducta');

    if(id_diagnostico_conducta == 17 || id_diagnostico_conducta == 13 || id_diagnostico_conducta == 13
        || riesgo == 'BAJO' || riesgo == 'MUY BAJO' || id_diagnostico_conducta == 3){
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue'); 
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoBajoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0) 
         
    }else if(id_diagnostico_conducta == 7 || id_diagnostico_conducta == 9 || id_diagnostico_conducta == 18
        || riesgo == 'MODERADO'){
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue');  
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)   
        
    }
    else if(riesgo == 'MALNUTRICION'){
        campoRiesgo.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoAltoEncuesta');// color rojo
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
    }
    else if(riesgo == 'RIESGO DE MALNUTRICION'){
        campoRiesgo.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoModeradoEncuesta');// color naranja
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
    }
    else if(riesgo == 'ESTADO NUTRICIONAL NORMAL'){
        campoRiesgo.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoBajoEncuesta');// color verde
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
    
    //apgar familiar
    }else if (riesgo=='NORMAL'){
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoBajoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)

    }else if(riesgo=='DISFUNCION LEVE' ){
       
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)

    }
    else if(riesgo== 'DISFUNCION MODERADA'){
        
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoColorTomateEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)

    }
    else if(riesgo== 'DISFUNCION SEVERA'){
         
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoAltoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)

    }
    

    
    
    //NUEVAS REGLAS covid 2.0
    else if(id_diagnostico_conducta == 44){
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue');  
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)

    }else if(id_diagnostico_conducta == 46){
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue');  
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','sinRiesgoColorBlanco');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)
    
    }else if(id_diagnostico_conducta == 41 || riesgo == 'ALTO'){
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue');  
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','sinRiesgoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)

    }else if(id_diagnostico_conducta == 40 || riesgo == 'MODERADO'){
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue');  
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoAltoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)

    }else if(id_diagnostico_conducta == 39 || riesgo == 'LEVE'){
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue');  
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoColorTomateEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)

    }else if(id_diagnostico_conducta == 38){
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue');  
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)

    }else if(id_diagnostico_conducta == 20 || riesgo == 'ALTO'){
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue');  
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','sinRiesgoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)
    
    }else if(id_diagnostico_conducta == 27){
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue');  
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoBajoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)
    
    }else if(id_diagnostico_conducta == 28){
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue');  
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0)
        
    }else if(id_diagnostico_conducta == 29){
            boton.removeAttribute('hidden');
            campoSede.removeAttribute('hidden');
            boton1.removeAttribute('hidden');
            boton.setAttribute('class','small button blue');
            boton1.setAttribute('class','small button blue');  
            campoRiesgo.removeAttribute('hidden');
            campoDiagnostico.removeAttribute('hidden');
            campoConducta.removeAttribute('hidden');
            document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoBajoEncuesta');
            asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
            asignaAtributo('lbldiagnostico',diagnostico , 0)
            asignaAtributo('lblconducta',conducta, 0)

    }else if(id_diagnostico_conducta == 37|| riesgo == 'MODERADO'){
            boton.removeAttribute('hidden');
            campoSede.removeAttribute('hidden');
            boton1.removeAttribute('hidden');
            boton.setAttribute('class','small button blue');
            boton1.setAttribute('class','small button blue');  
            campoRiesgo.removeAttribute('hidden');
            campoDiagnostico.removeAttribute('hidden');
            campoConducta.removeAttribute('hidden');
            document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoAltoEncuesta');
            asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
            asignaAtributo('lbldiagnostico',diagnostico , 0)
            asignaAtributo('lblconducta',conducta, 0)

    }else if(id_diagnostico_conducta == 34){
            boton.removeAttribute('hidden');
            campoSede.removeAttribute('hidden');
            boton1.removeAttribute('hidden');
            boton.setAttribute('class','small button blue');
            boton1.setAttribute('class','small button blue');  
            campoRiesgo.removeAttribute('hidden');
            campoDiagnostico.removeAttribute('hidden');
            campoConducta.removeAttribute('hidden');
            document.getElementById('rowSemaforoRiesgo').setAttribute('class','sinRiesgoEncuesta');
            asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
            asignaAtributo('lbldiagnostico',diagnostico , 0)
            asignaAtributo('lblconducta',conducta, 0)

        }else if(id_diagnostico_conducta == 36|| riesgo == 'LEVE'){
            boton.removeAttribute('hidden');
            campoSede.removeAttribute('hidden');
            boton1.removeAttribute('hidden');
            boton.setAttribute('class','small button blue');
            boton1.setAttribute('class','small button blue');  
            campoRiesgo.removeAttribute('hidden');
            campoDiagnostico.removeAttribute('hidden');
            campoConducta.removeAttribute('hidden');
            document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoColorTomateEncuesta');
            asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
            asignaAtributo('lbldiagnostico',diagnostico , 0)
            asignaAtributo('lblconducta',conducta, 0)

    }else if(id_diagnostico_conducta == 35){
            boton.removeAttribute('hidden');
            campoSede.removeAttribute('hidden');
            boton1.removeAttribute('hidden');
            boton.setAttribute('class','small button blue');
            boton1.setAttribute('class','small button blue');  
            campoRiesgo.removeAttribute('hidden');
            campoDiagnostico.removeAttribute('hidden');
            campoConducta.removeAttribute('hidden');
            document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoModeradoEncuesta');
            asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
            asignaAtributo('lbldiagnostico',diagnostico , 0)
            asignaAtributo('lblconducta',conducta, 0)

    }else if(id_diagnostico_conducta == 42){
            boton.removeAttribute('hidden');
            campoSede.removeAttribute('hidden');
            boton1.removeAttribute('hidden');
            boton.setAttribute('class','small button blue');
            boton1.setAttribute('class','small button blue');  
            campoRiesgo.removeAttribute('hidden');
            campoDiagnostico.removeAttribute('hidden');
            campoConducta.removeAttribute('hidden');
            document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoModeradoEncuesta');
            asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
            asignaAtributo('lbldiagnostico',diagnostico , 0)
            asignaAtributo('lblconducta',conducta, 0)

        }else if(id_diagnostico_conducta == 33){
            boton.removeAttribute('hidden');
            campoSede.removeAttribute('hidden');
            boton1.removeAttribute('hidden');
            boton.setAttribute('class','small button blue');
            boton1.setAttribute('class','small button blue');  
            campoRiesgo.removeAttribute('hidden');
            campoDiagnostico.removeAttribute('hidden');
            campoConducta.removeAttribute('hidden');
            document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoColorTomateEncuesta');
            asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
            asignaAtributo('lbldiagnostico',diagnostico , 0)
            asignaAtributo('lblconducta',conducta, 0)

    }else if(id_diagnostico_conducta == 43){
            boton.removeAttribute('hidden');
            campoSede.removeAttribute('hidden');
            boton1.removeAttribute('hidden');
            boton.setAttribute('class','small button blue');
            boton1.setAttribute('class','small button blue');  
            campoRiesgo.removeAttribute('hidden');
            campoDiagnostico.removeAttribute('hidden');
            campoConducta.removeAttribute('hidden');
            document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoModeradoEncuesta');
            asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
            asignaAtributo('lbldiagnostico',diagnostico , 0)
            asignaAtributo('lblconducta',conducta, 0)
        
    }else if(id_diagnostico_conducta == 24 || id_diagnostico_conducta == 19 || id_diagnostico_conducta == 23 || id_diagnostico_conducta == 21 ||
        id_diagnostico_conducta == 20 || id_diagnostico_conducta == 10 || id_diagnostico_conducta == 16 ||
        id_diagnostico_conducta == 15 || id_diagnostico_conducta == 8 || id_diagnostico_conducta == 25 
        || riesgo == 'ALTO'){
        boton.removeAttribute('hidden'); 
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue'); 
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoAltoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0) 
        }
    else if (id_diagnostico_conducta == 22 || id_diagnostico_conducta == 14 ) {
        boton.removeAttribute('hidden'); 
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class','small button blue');
        boton1.setAttribute('class','small button blue'); 
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class','riesgoColorTomateEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta','Alerta: ' + riesgo +' - '+ semaforo, 0 )
        asignaAtributo('lbldiagnostico',diagnostico , 0)
        asignaAtributo('lblconducta',conducta, 0) 
    }
    else {
        campoConducta.setAttribute('hidden','true');
        campoDiagnostico.setAttribute('hidden','true');
            boton.setAttribute('hidden','true');
            campoSede.setAttribute('hidden','true');
            boton1.setAttribute('hidden','true');
            boton.removeAttribute('class', 'small button blue'); 
            boton1.removeAttribute('class','small button blue');
            campoRiesgo.removeAttribute('hidden');
            document.getElementById('rowSemaforoRiesgo').setAttribute('class','sinRiesgoEncuesta');
            asignaAtributo('lblSemaforoRiesgoEncuesta',' No presenta alerta. No hay ninguna encuesta seleccionada o la encuesta no a sido cerrada', 0 )      
        }
        if (valorAtributo('txtIdMedioRecepcion') == 3 ) {
            boton1.setAttribute('hidden','true');
            campoSede.setAttribute('hidden','true');
            boton1.removeAttribute('class','small button blue');
        }    
}

function formatoRecomendacionTexto() {
    var texto = document.getElementById('paRecomendacionesModal').innerHTML
    var mitexto = '';
    var textoMostar = texto.split('.')
    for (let i = 0; i < textoMostar.length; i++) {
        mitexto += textoMostar[i];  
    }
    document.getElementById('paRecomendacionesModal').innerText = mitexto;

}

 function validarTabsGeneroEspecialidad(divAcordionFolioEspecialidad) {
setTimeout(() => {
    console.log("validando folios de otros antecedentes...");
    if(valorAtributo('lblGeneroPaciente')=='M'){
      //Validacion de edad par elpaciente: oculta plantillas de salud sexual y reproductiva para menores de 12 años
        if(valorAtributo('lblEdadPaciente')<12){
            console.log("menor de 12 años: ");
            console.log(document.getElementById('agom'));
            if(document.getElementById('espetabagom')!= null){document.getElementById('espetabagom').setAttribute('hidden','true');}
            if(document.getElementById('espetabappfih')!= null){document.getElementById('espetabappfih').setAttribute('hidden','true');}
            if(document.getElementById('agom')!= null){ocultar('agom');}
            if(document.getElementById('appfih')!= null){ocultar('appfih');}
            
        } else {
            if(document.getElementById('espetabagom')!= null){document.getElementById('espetabagom').setAttribute('hidden','true');}
            if(document.getElementById('div_agom')!= null){document.getElementById('div_agom').setAttribute('hidden','true');}
        }
     
    }else if(valorAtributo('lblGeneroPaciente')=='F') {
        //Validacion de edad par elpaciente: oculta plantillas de salud sexual y reproductiva para menores de 12 años
        if(valorAtributo('lblEdadPaciente')<12){
            if(document.getElementById('espetabagom')!= null){document.getElementById('espetabagom').setAttribute('hidden','true');}
            if(document.getElementById('espetabappfih')!= null){document.getElementById('espetabappfih').setAttribute('hidden','true');}
            if(document.getElementById('agom')!= null){ocultar('agom');}
            if(document.getElementById('appfih')!= null){ocultar('appfih');}
        } else {
            if(document.getElementById('espetabappfih')!= null){document.getElementById('espetabappfih').setAttribute('hidden','true');}
        }
    }

    }, 600);


    //alert("ingreso a genero :" +valorAtributo('lblGeneroPaciente') + " Especialidad  encontrada: "+document.getElementById('espetabagnut'))
    
} 

function bloquearMisCampos(campos, tabla, checks){

    if(checks == true){
        campos.split(',').map(e => { 
            va = document.getElementById(tabla+','+e).setAttribute('disabled','true');
        })  
    }else{
        campos.split(',').map(e => { 
            va = document.getElementById(tabla+','+e).removeAttribute('disabled');
        }) 
    }
}

function comboMed() {
    var valor = document.getElementById('idMedEvaluacion').value;
    cargarComboGRALCondicion1('',valor,'miComboMedEvaluacion',1021, IdSede()); 
    document.getElementById('miComboMedEvaluacion').removeAttribute('onclick','comboMed()');
}

function validarTabsGenero(){
    //alert("dato guardado de sexo paciente: "+valorAtributo('lblGeneroPaciente')+" Edad paciente: "+valorAtributo('lblEdadPaciente'))
    if(valorAtributo('lblGeneroPaciente')=='F') {
        //alert("ingresa femenino");
        validarGraficasMujer();
        if(document.getElementById('tabpsenf')!= null){document.getElementById('tabpsenf').setAttribute('hidden','true');}
        if(document.getElementById('tabappfih')!= null){document.getElementById('tabappfih').setAttribute('hidden','true');}
        if(valorAtributo('lblEdadPaciente')<12){
            if(document.getElementById('tabcenf')!= null){document.getElementById('tabcenf').setAttribute('hidden','true');}
            if(document.getElementById('tabagom')!= null){document.getElementById('tabagom').setAttribute('hidden','true');}
            if(document.getElementById('agnut')!= null){document.getElementById('agnut').setAttribute('hidden','true');}
        }
        
    }else if(valorAtributo('lblGeneroPaciente')=='M'){
        validarGraficasHombre()
        //alert("valor encontrado: "+document.getElementById('tabagnut'))
        if(document.getElementById('tabcenf')!= null){document.getElementById('tabcenf').setAttribute('hidden','true');}
        if(document.getElementById('tabagom')!= null){document.getElementById('tabagom').setAttribute('hidden','true');}
        if(document.getElementById('tabagnut')!= null){document.getElementById('tabagnut').setAttribute('hidden','true');}
        
        if(valorAtributo('lblEdadPaciente')<12){
            if(document.getElementById('tabpsenf')!= null){document.getElementById('tabpsenf').setAttribute('hidden','true');}
            if(document.getElementById('tabappfih')!= null){document.getElementById('tabappfih').setAttribute('hidden','true');}
        }
    } 

    //tabcydenf-- tabpjenf
     if(valorAtributo('lblEdadPaciente')<18) {
        //alert("ingresa 17...");
        if(document.getElementById('tabpjenf')!= null){document.getElementById('tabpjenf').setAttribute('hidden','true');}
        //
    }else if(valorAtributo('lblEdadPaciente')<28){
        //alert("ingresa 27...");
        if(document.getElementById('tabcydenf')!= null){document.getElementById('tabcydenf').setAttribute('hidden','true');}
        //
    }else if(valorAtributo('lblEdadPaciente')>=28){
        //alert("ingresa 28..+");
        if(document.getElementById('tabcydenf')!= null){document.getElementById('tabcydenf').setAttribute('hidden','true');}
        if(document.getElementById('tabpjenf')!= null){document.getElementById('tabpjenf').setAttribute('hidden','true');}
    }  
    
} 

function validarGraficasMujer() {
    if(document.getElementById('tabgpm02')!= null){
        
        //ocultar tabs para graficas de niño
        //alert("ingresa validacion mujer...");
        ocultar('tabgph02');
        ocultar('tabgph25');
        ocultar('tabgph518');
        
        //Validacion edad para tabs de graficas
         if(valorAtributo('lblEdadPaciente')>= 0 && valorAtributo('lblEdadPaciente')<2) {
         //alert("ingresa 0 - 2...");
         document.getElementById('tabgpm25').setAttribute('hidden','true');
         document.getElementById('tabgpm518').setAttribute('hidden','true');

        }else if(valorAtributo('lblEdadPaciente')>1 && valorAtributo('lblEdadPaciente')<5){
         //alert("ingresa 2 - 5...");
         document.getElementById('tabgpm02').setAttribute('hidden','true');
         document.getElementById('tabgpm518').setAttribute('hidden','true');

        }else if(valorAtributo('lblEdadPaciente')>4 && valorAtributo('lblEdadPaciente')<19){
         //alert("ingresa 5 - 18...");
         document.getElementById('tabgpm02').setAttribute('hidden','true');
         document.getElementById('tabgpm25').setAttribute('hidden','true');
        }else {
            document.getElementById('tabgpm02').setAttribute('hidden','true'); 
            document.getElementById('tabgpm25').setAttribute('hidden','true');
            document.getElementById('tabgpm518').setAttribute('hidden','true');
        }  
    }
}

function errorEncuesta() {//no permitir cerrar encuesta 

    if(valorAtributo('lblIdEncuesta') == '29'){   
        //Switches de la encuesta de covid 2 clínicos
        c9= document.getElementById('cvidd,c9').value;
        c10= document.getElementById('cvidd,c10').value;
        c11= document.getElementById('cvidd,c11').value;
        c12= document.getElementById('cvidd,c12').value;
        c13= document.getElementById('cvidd,c13').value;
        c14= document.getElementById('cvidd,c14').value;
        c15= document.getElementById('cvidd,c15').value;
        //Validar que alguno está en si
        if(c9=='SI' || c10=='SI' || c11=='SI' || c12=='SI' || c13=='SI' || c14=='SI' || c15=='SI'){
            //Switches sobre sintomas en covid 2
            c1= document.getElementById('cvidd,c1').value;
            c2= document.getElementById('cvidd,c2').value;
            c3= document.getElementById('cvidd,c3').value;
            c4= document.getElementById('cvidd,c4').value;
            c5= document.getElementById('cvidd,c5').value;
            c17= document.getElementById('cvidd,c17').value;
            //Validar si está alguno en si
            if(c1=='SI' || c2=='SI' || c3=='SI' || c4=='SI' || c5=='SI' || c17=='SI'){
                modificarMapCRUD('cerrarEncuestaPaciente');  
            }else{

                alert("Recuerde marcar los síntomas");
            }
        }else{
            modificarMapCRUD('cerrarEncuestaPaciente');
        }
    
    }else{
        modificarMapCRUD('cerrarEncuestaPaciente');
    }
    
}

function sinDatoEncuesta() {//Alertar si desea cerrar encuesta con datos NO

    
     if(valorAtributo('lblIdEncuesta') == '29'){   
         //Switches de la encuesta de covid 2 clínicos
         c1= document.getElementById('cvidd,c1').value;
         c2= document.getElementById('cvidd,c2').value;
         c3= document.getElementById('cvidd,c3').value;
         c4= document.getElementById('cvidd,c4').value;
         c17= document.getElementById('cvidd,c17').value;
         c5= document.getElementById('cvidd,c5').value;
         c6= document.getElementById('cvidd,c6').value;
         c7= document.getElementById('cvidd,c7').value;
         c8= document.getElementById('cvidd,c8').value;
         c9= document.getElementById('cvidd,c9').value;
         c10= document.getElementById('cvidd,c10').value;
         c11= document.getElementById('cvidd,c11').value;
         c12= document.getElementById('cvidd,c12').value;
         c13= document.getElementById('cvidd,c13').value;
         c14= document.getElementById('cvidd,c14').value;
         c15= document.getElementById('cvidd,c15').value;
         //Validar que todos esten en no
         if(c1=='NO' && c2=='NO'&& c3=='NO' && c4=='NO' && c17=='NO'&& c5=='NO'&& c6=='NO'&& c7=='NO' && c8=='NO'&&
         c9=='NO' && c10=='NO' && c11=='NO' && c12=='NO' && c13=='NO' && c14=='NO' && c15=='NO')
         {
                        
            var mensaje="\u00BFDesea cerrar la encuesta sin diligenciar la informaci\u00F3n?";
            var opcion = confirm(mensaje);
             if (opcion == true) {
            
                errorEncuesta();
            
	        } else {
            
            }
	                      
             }else{    
                 errorEncuesta();
             }
                     
     }else{
         modificarMapCRUD('cerrarEncuestaPaciente');
     }
 /*if (id_diagnostico_conducta == 3) {
     alert('Datos ingresados no validos, recuerde marcar sintomas');return false;
 }*/
}

function validarGraficasHombre() {
    if(document.getElementById('tabgph02')!= null){
        
        //ocultar tabs para graficas de niña
        //alert("ingresa validacion Hombre...");
        document.getElementById('tabgpm02').setAttribute('hidden','true'); 
        document.getElementById('tabgpm25').setAttribute('hidden','true');
        document.getElementById('tabgpm518').setAttribute('hidden','true');
        
        //Validacion edad para tabs de graficas
         if(valorAtributo('lblEdadPaciente')>= 0 && valorAtributo('lblEdadPaciente')<2) {
         //alert("ingresa 0 - 2...");
         document.getElementById('tabgph25').setAttribute('hidden','true');
         document.getElementById('tabgph518').setAttribute('hidden','true');

        }else if(valorAtributo('lblEdadPaciente')>1 && valorAtributo('lblEdadPaciente')<5){
         //alert("ingresa 2 - 5...");
         document.getElementById('tabgph02').setAttribute('hidden','true');
         document.getElementById('tabgph518').setAttribute('hidden','true');

        }else if(valorAtributo('lblEdadPaciente')>4 && valorAtributo('lblEdadPaciente')<19){
         //alert("ingresa 5 - 18...");
         document.getElementById('tabgph02').setAttribute('hidden','true');
         document.getElementById('tabgph25').setAttribute('hidden','true');
        }else {
            document.getElementById('tabgph02').setAttribute('hidden','true'); 
            document.getElementById('tabgph25').setAttribute('hidden','true');
            document.getElementById('tabgph518').setAttribute('hidden','true');
        }  
        //  
    }
 
}

function calcularCantidadMed(cmbCant,cmbFrecuencia,cmbDias) {
    console.log("ingreso metodo de clculo de medicamentos");
    var dosis = valorAtributo('cmbCant');
    var horas = valorAtributo('cmbFrecuencia');
    var dias = valorAtributo('cmbDias');
    if(dias==101){
        dias= 1;
    }
    var entrga= dosis*(Math.trunc(24/horas))*dias;
  
    asignaAtributo('txtCantidad', entrga, 0);
}

function validarDatosEncuesta() {

    if(valorAtributo('lblIdEncuesta') == '28'){
        console.log("tipo operador: "+typeof(valorAtributo('lblIdEncuesta')));
        if(document.getElementById('cbnut,c4').value=='SI'){
            var c4 = 0;
            }else{
            var c4 = 2;
            }
            var calculado = parseInt(document.getElementById('cbnut,c1').value)+
            parseInt(document.getElementById('cbnut,c2').value)+parseInt(document.getElementById('cbnut,c3').value)
            +parseInt(document.getElementById('cbnut,c5').value)+parseInt(document.getElementById('cbnut,c6').value)
            +parseInt(c4);
            //document.getElementById("c7").value = 'calculado';
            console.log("calculado: "+calculado);
            if(calculado>=0 && calculado<8 ){ document.getElementById("c7").value = 'MALNUTRICION';document.getElementById("c7").style.color = "red";}
            else if(calculado>=8 && calculado<12){ document.getElementById("c7").value = 'RIESGO DE MALNUTRICION'; document.getElementById("c7").style.color = "orange";}
            else if(calculado>=12){ document.getElementById("c7").value = 'ESTADO NUTRICIONAL NORMAL'; document.getElementById("c7").style.color = "green";
                //var el = document.getElementById("some-element");
               /// el.setAttribute("style", "background-color:darkblue;");
            }
            else{ document.getElementById("c7").value = 'Datos insuficientes para el calculo...';}
    }
}

function notificarApp(){


    if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE UN PACIENTE'); return false; }
    if (valorAtributo('lblIdEncuestaPaciente') == '') { alert('SELECCIONE UNA ENCUESTA'); return false; }

    var servicio = 'http://181.48.155.149/hila/manilla/notificar?id_paciente='+valorAtributoIdAutoCompletar('txtIdBusPaciente')
    +'&id_encuesta='+valorAtributo('lblIdEncuestaPaciente');
   

    $.get(servicio, function (data) {
        alert(JSON.stringify(data));
    });

}


function ubicarMapa() {
    cargaPuntoLatitudLongitud('lblIdlongitud', 'lblIdlatitud', 'lblIdArea', valorAtributo('lblIdPaciente'), 842)
}

function ubicarMapaMunicipio() {
    cargaPuntoLatitudLongitud('lblLatitudMuni', 'lblLongitudMuni', 'lblIdArea', valorAtributo('lblIdArea'), 846)
}


function recargarMapa() {    

    limpTablasDiv('idTableContenedorMapaTd')

    
    contenedorDiv=document.getElementById('idTableContenedorMapaTd'); 
    cuerpoDiv=document.createElement('DIV');  // se crean los div hijos pa cada diente
    cuerpoDiv.id='mapaGeoArea'
    cuerpoDiv.style.height='90%';
    cuerpoDiv.style.width='100%';
    contenedorDiv.appendChild(cuerpoDiv);      
}

var mymap;
function centroPuntoDelMapa() { 
    recargarMapa()


    var estiloPopup = { 'maxWidth': '300' }
    var iconoBase = L.Icon.extend({ options: { iconSize: [35, 35], iconAnchor: [35, 35], popupAnchor: [35, 35] } });

    mymap = L.map('mapaGeoArea').setView([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], valorAtributo('lblIdZoom'));
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);

    mymap.on('click', function (e) {
        asignaAtributo('txtLatitudReferencia', e.latlng.lat.toString(), 0);
        asignaAtributo('txtLongitudReferencia', e.latlng.lng.toString(), 0);
    });

    urlIconoCentroPunto = 'https://image.flaticon.com/icons/png/128/291/291236.png'

    L.marker([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], { icon: new iconoBase({ iconUrl: urlIconoCentroPunto }) }).bindPopup('Punto buscado', estiloPopup).addTo(mymap);

    cargaLineasDelArea(valorAtributo('lblIdArea'), 844)
}

function cargaLineasDelArea(condicion1, idQueryCombo) {  //  alert(  condicion1+'::'+idQueryCombo )
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaLineasDelArea(condicion1) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaLineasDelArea(condicion1) {
    
    var iconoBase = L.Icon.extend({ options: { iconSize: [35, 35], iconAnchor: [35, 35], popupAnchor: [35, 35] } });
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            totalRegistros = raiz.getElementsByTagName('id').length

            var polygonPoints = Array.from(Array(totalRegistros), () => new Array(2));

            for (i = 0; i < totalRegistros; i++) {
                latitud = raiz.getElementsByTagName('id')[i].firstChild.data
                longitud = raiz.getElementsByTagName('nom')[i].firstChild.data

                polygonPoints[i][0] = latitud
                polygonPoints[i][1] = longitud

                titulo = 'Orden=' + (i + 1)
                fabricaMarcador(latitud, longitud, titulo, 'Lat=' + latitud + '  lon=' + longitud, mymap)
            }

            L.polygon(
                polygonPoints,
                {
                    weight: 2, /*grosor de la linea*/
                    lineJoin: "miter",
                    color: "blue",
                    fillColor: '#f03',  /* color de llenar fillColor: "none",*/
                    fillOpacity: 0.05
                }
            ).addTo(mymap)

            cargaMarcadoresDelArea(valorAtributo('lblIdArea'), 843) 

        } else {
            alert("Problemas de conexion con servidor: respuestacargaPuntoLatitudLongitud " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}
function fabricaMarcador(latitud, longitud, titulo, textoPopup, mymap) {
    var estiloPopup = { 'maxWidth': '100' }
    urlIconoCentroPunto = 'http://181.48.155.146:8383/clinica/utilidades/imagenes/marcadores/vertice.png'
    var iconoBase = L.Icon.extend({ options: { iconSize: [15, 15], iconAnchor: [15, 15], popupAnchor: [15, 15] } });

    var textoPopup = '<b>' + titulo + '</b><br/>' + textoPopup;

    L.marker([latitud, longitud], { icon: new iconoBase({ iconUrl: urlIconoCentroPunto }) }).bindPopup(textoPopup, estiloPopup).addTo(mymap);
}

function cargaMarcadoresDelArea(condicion1, idQueryCombo) {  //  alert(  condicion1+'::'+idQueryCombo )
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaMarcadoresDelArea(condicion1) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaMarcadoresDelArea(condicion1) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;

            var estiloPopup = { 'maxWidth': '300' }
            var iconoBase = L.Icon.extend({ options: { iconSize: [35, 35], iconAnchor: [35, 35], popupAnchor: [35, 35] } });


            for (i = 0; i < raiz.getElementsByTagName('id').length; i++) {

                marcador = raiz.getElementsByTagName('id')[i].firstChild.data
                marcador_tipo = raiz.getElementsByTagName('nom')[i].firstChild.data

                var m = marcador.split('-_');
                var id_marcador = m[0];
                var latitud = m[1];
                var longitud = m[2];
                var descripcion = m[3];

                var mt = marcador_tipo.split('-_');
                var id_marcador_tipo = mt[0];

                var textoPopup = '<b>' + mt[1] + '</b><br/>' + m[3];
                var urlIcono = mt[2];
                L.marker([latitud, longitud], { icon: new iconoBase({ iconUrl: urlIcono }) }).bindPopup(textoPopup, estiloPopup).addTo(mymap);
            }

        } else {
            alert("Problemas de conexion con servidor: respuestacargaPuntoLatitudLongitud " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}

function cargaPuntoLatitudLongitud(idLabelDestino1, idLabelDestino2, idLabelDestino3, condicion1, idQueryCombo) { 
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaPuntoLatitudLongitud(idLabelDestino1, idLabelDestino2, idLabelDestino3) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaPuntoLatitudLongitud(idLabelDestino1, idLabelDestino2, idLabelDestino3) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino1);
            limpiaAtributo(idLabelDestino2);
            limpiaAtributo(idLabelDestino3);

            asignaAtributo(idLabelDestino1, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(idLabelDestino2, raiz.getElementsByTagName('nom')[0].firstChild.data)
            asignaAtributo(idLabelDestino3, raiz.getElementsByTagName('title')[0].firstChild.data)

            centroPuntoDelMapa()

        } else {
            alert("Problemas de conexion con servidor: respuestacargaPuntoLatitudLongitud " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}

function georeferenciarPaciente() {  //alert((id+':: '+name+' :: '+value )) 

    mostrar('divVentanitaGeoPaciente')

    ubicarMapa()
}

function guardarDatosEncuesta(id, name, value) {  //alert((id+':: '+name+' :: '+value )) 

    varOrden = id
    varIdPlantilla = name
    varResultado = value
     modificarEncuestaCRUD('guardarDatosEncuesta', id, name, value)
}


function guardarDatosPlantilla(id, name, value) {  //alert((id+':: '+name+' :: '+value )) 
    varOrden = id
    varIdPlantilla = name
    varResultado = value
    modificarPlantillaCRUD('guardarDatosPlantilla', id, name, encodeURI(value));
}

function colocaridencuestacrear() {
    var id = document.getElementById('cmbEncuestaParaCrear').value;
    document.getElementById('lblIdEncuestaParaCrear').value = id; 
}

function modificarMapCRUD(arg, pag) {
    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    switch (arg) {
        case 'eliminarArea':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=803&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdArea'));
                add_valores_a_mandar(valorAtributo('lblIdArea'));
                ajaxModificar();
            }
            break;        
        case 'crearArea':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=799&parametros=";
                add_valores_a_mandar(valorAtributo('txtArea'));
                add_valores_a_mandar(valorAtributo('txtOrden'));
                add_valores_a_mandar(valorAtributo('txtLatitud'));
                add_valores_a_mandar(valorAtributo('txtLongitud'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));                
                add_valores_a_mandar(valorAtributo('cmbVigente'))
                add_valores_a_mandar(valorAtributo('txtZoom'))
                add_valores_a_mandar(valorAtributo('txtColor'))
                add_valores_a_mandar(valorAtributo('txtColorOpacidad'))
                add_valores_a_mandar(IdSesion());
                ajaxModificar();
            }
            break;
        case 'adicionarReferencia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=802&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdArea'))
                add_valores_a_mandar(valorAtributo('txtOrdenReferencia'));
                add_valores_a_mandar(valorAtributo('txtLatitudReferencia'));
                add_valores_a_mandar(valorAtributo('txtLongitudReferencia'));
                add_valores_a_mandar(IdSesion());
                ajaxModificar();
            } 
            break;            
        case 'modificarReferencia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=801&parametros=";

                add_valores_a_mandar(valorAtributo('txtOrdenReferencia'));
                add_valores_a_mandar(valorAtributo('txtLatitudReferencia'));
                add_valores_a_mandar(valorAtributo('txtLongitudReferencia'));
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdArea'))
                add_valores_a_mandar(valorAtributo('lblOrdenReferencia'))
                ajaxModificar();
            } 
            break;
        case 'crearEncuestaPaciente': 
       
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=888&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(document.getElementById('lblIdEncuestaParaCrear').value);
                add_valores_a_mandar(valorAtributo('txtIdMedioRecepcion')); // 3 = hiscotria clinica // 2 = linea de frente
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
                add_valores_a_mandar(document.getElementById('lblIdEncuestaParaCrear').value);
                add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
                add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
                add_valores_a_mandar(document.getElementById('lblIdEncuestaParaCrear').value);
                ajaxModificar();
            }
        break;
        case 'cerrarEncuestaPaciente':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=883&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdEncuestaPaciente'));
                ajaxModificar();

            }
        break;

    }
}

function respuestaMapCRUD(arg, xmlraiz) {
   
    if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
        switch (arg) {
            case 'crearArea':
                buscarEncuesta('listMunicipioAreas')
            break; 
            case 'eliminarArea':
                buscarEncuesta('listMunicipioAreas')
            break;             
            case 'adicionarReferencia':
                buscarEncuesta('listAreasRerefencias')
                setTimeout("ubicarMapaMunicipio()", 1000)
            break;            
            case 'modificarReferencia':
                buscarEncuesta('listAreasRerefencias')                
                setTimeout("ubicarMapaMunicipio()", 1000)
            break;
            case 'crearEncuestaPaciente':
                 
                 alert('Encuesta Creada..!');
             //  arg = 'listEncuestaPaciente';ç
             paginaActual='';
                 buscarEncuesta('listEncuestaPaciente');

             
            break;
            case 'cerrarEncuestaPaciente':
               
               alert('Encuesta Cerrada..!');
               paginaActual='';
               //arg = 'listEncuestaPaciente';
               tabsContenidoEncuesta('desdeEncuesta');
               buscarEncuesta('listEncuestaPaciente')
               colocarRiesgoEncuesta('SIN RIESGO','1');
               
            break;
        }
 
    }
    else console.log('NO SE LOGRO GUARDAR, VERIFIQUE CONEXIÓN CON SERVIDOR')

}

function modificarEncuestaCRUD(arg, id, name, value) {
    pagina = '/clinica/paginas/accionesXml/modificarEncuestaCRUD_xml.jsp';
    varajaxMenu.onreadystatechange = respuestamodificarEncuestaCRUD;
    switch (arg) {

        case 'guardarDatosEncuesta':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = '';
                valores_a_mandar = valores_a_mandar + "tipo=" + id; //eno1(tipo)
                valores_a_mandar = valores_a_mandar + "&value=" + value; //holoa
                valores_a_mandar = valores_a_mandar + "&campo=" + name; // c22
                valores_a_mandar = valores_a_mandar + "&id_encuesta_paciente=" + valorAtributo('lblIdEncuestaPaciente'); //111
                ajaxModificar();
              
            }
            break;
    }
}


function modificarPlantillaCRUD(arg, id, name, value) {
    pagina = '/clinica/paginas/accionesXml/modificarPlantillaCRUD_xml.jsp';
    paginaActual = arg;
    switch (arg) {
        case 'guardarDatosPlantilla':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = '';
                valores_a_mandar = valores_a_mandar + "tipo=" + id; //eno1(tipo)
                valores_a_mandar = valores_a_mandar + "&value=" + value; //holoa
                valores_a_mandar = valores_a_mandar + "&campo=" + name; // c22
                valores_a_mandar = valores_a_mandar + "&id_evolucion=" + valorAtributo('lblIdDocumento'); //111
        ajaxModificar();
    }
    break;
    }
}



function guardarIdFormulaPlantillaDetalleCRUD(arg, id_plantilla, referencia_resultado) {
    pagina = '/clinica/paginas/accionesXml/guardarIdFormulaPlantillaDetalle_xml.jsp';
    varajaxMenu.onreadystatechange = respuestaIdFormulaPlantillaDetalleCRUD;
    switch (arg) {

        case 'guardarIdFormula':
            valores_a_mandar = '';
            valores_a_mandar = valores_a_mandar + "id_plantilla=" + id_plantilla;  
            valores_a_mandar = valores_a_mandar + "&referencia_resultado=" + referencia_resultado; 
            ajaxModificar();
    break;
    }
}



function respuestaIdFormulaPlantillaDetalleCRUD(arg, xmlraiz) {
    switch (arg) {
        case 'guardarIdFormula':
           alert('guardado')
        break;

    }
    
}



function respuestamodificarEncuestaCRUD(arg, xmlraiz) {
    switch (arg) {
        case 'guardarDatosEncuesta':
          // alert('guardado')
        break;

    }
    
}

function respuestamodificarPlantillaCRUD(arg, xmlraiz) {
    switch (arg) {
        case 'guardarDatosPlantilla':
           //alert('guardado')
        break;

    }
    
}


function contenedorEncuesta() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/encuesta/encuestaPaciente.jsp?', true);
    varajaxMenu.onreadystatechange = llenarcontenedorEncuesta;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}


function llenarcontenedorEncuesta() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
         
            document.getElementById("divParaContenedorEncuesta").innerHTML = varajaxMenu.responseText;
            buscarEncuesta('listEncuestaPaciente');
    
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function tabsContenidoEncuesta(modo) {  
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if(modo=='desdeEncuesta'){ 
        parametro =  valorAtributo('lblIdEncuesta')
        
        varajaxMenu.open("POST", '/clinica/paginas/encuesta/tabsContenidoEncuesta.jsp?' +'&id_parametro=' + parametro + '&modo='+ modo, true);
        varajaxMenu.onreadystatechange = function () { llenartabsContenidoEncuesta(modo) };
        varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');    
        varajaxMenu.send(valores_a_mandar);
        ///setTimeout("validarTabsGenero()",200); 

    } else if (modo == 'folioEspecialidad') {

        parametro =  valorAtributo('lblTipoDocumento')
        varajaxMenu.open("POST", '/clinica/paginas/encuesta/tabscontenidoFolioEspecialidad.jsp?' +'&id_parametro=' + parametro + '&modo='+ modo, true);
        varajaxMenu.onreadystatechange = function () { llenartabsContenidoEncuesta(modo) };
        varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');    
        varajaxMenu.send(valores_a_mandar);
        ///setTimeout("validarTabsGenero()",200); 
    
    
    
    } else {  
        parametro =  valorAtributo('lblTipoDocumento')
        varajaxMenu.open("POST", '/clinica/paginas/encuesta/tabsDesdeFolio.jsp?' +'&id_parametro=' + parametro + '&modo='+ modo, true);
        varajaxMenu.onreadystatechange = function () { llenartabsContenidoEncuesta(modo) };
        varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');    
        varajaxMenu.send(valores_a_mandar); 
        setTimeout("validarTabsGenero()",200); 
    }
    
    
}

function llenartabsContenidoEncuesta(modo) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
          
            if(modo=='desdeEncuesta'){

                document.getElementById("divParaTabsContenidoEncuesta").innerHTML = varajaxMenu.responseText;
                $("#tabsContenidoEncuestaPaciente").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                tabsSubContenidoEncuesta(valorAtributo('lblIdPrimerTab'), modo);

            } else if (modo == 'folioEspecialidad') {

                document.getElementById("divParaTabsContenidoFolioEspecialidad").innerHTML = varajaxMenu.responseText;
                $("#tabsContenidoEncuestaPaciente33").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                tabsSubContenidoEncuesta(valorAtributo('lblIdPrimerTab33'), modo);

                
            } else{   
                document.getElementById("divParaTabsContenidoFolioPlantilla").innerHTML = varajaxMenu.responseText;
                $("#tabsContenidoEncuestaPaciente22").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                tabsSubContenidoEncuesta(valorAtributo('lblIdPrimerTab22'), modo);
            }
          //
          //aqui va el onclick de primer tab activo        
          //   buscarRiesgoEncuesta('listEncuestaPacienteRiesgo');          
            
        } else {
           alert("125.Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
  /*  if (varajaxMenu.readyState == 1) {
        alert("126.Problemas de conexion con Servidor: Reinicie el aplicativo")
    } */
}

function tabsSubContenidoEncuesta(tipo, modo) { 

        console.log("tipo: " + tipo);
        console.log("modo: " + modo);
        if(document.getElementById("auxFocus") != null)//En las plantillas de especialidad no existe el auxFocus por lo que debe consultarse antes
            document.getElementById("auxFocus").focus();
        varajaxMenu = crearAjax();
        valores_a_mandar = "";
        if(modo == 'desdeEncuesta'){        
            varajaxMenu.open("POST", '/clinica/paginas/encuesta/subContenedorEncuesta.jsp?tipo=' + tipo + '&id_parametro=' + valorAtributo('lblIdEncuesta') + '&id_encuestaPaciente=' + valorAtributo('lblIdEncuestaPaciente'), true);
            varajaxMenu.onreadystatechange = function () { llenartabsSubContenidoEncuesta(tipo) };
            varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');    
            varajaxMenu.send(valores_a_mandar);
        }else{
            varajaxMenu.open("POST", '/clinica/paginas/encuesta/subContenidoFolioPlantilla.jsp?tipo=' + tipo + '&tipo_folio=' + valorAtributo('lblTipoDocumento') + '&id_evolucion=' + valorAtributo('lblIdDocumento'), true); 
            varajaxMenu.onreadystatechange = function () { llenartabsSubContenidoEncuesta(tipo) };
            varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');    
            varajaxMenu.send(valores_a_mandar);
        }
    
    
}

function llenartabsSubContenidoEncuesta(tipo){ 
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {            
            document.getElementById('div_' + tipo).innerHTML = varajaxMenu.responseText;
            calculosIniciales(tipo);
            validacionesIniciales(tipo);
            

        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
    
    if (tipo=="clastrge") {
                //alert("ingreso a clastrge...");
                comboMed();
            }
}

function buscarEncuesta(arg) {
   
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {
        case 'listAreasRerefencias':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=800&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdArea'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Orden', 'latitud', 'longitud'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'Orden', index: 'Orden', width: anchoP(ancho, 20) },
                    { name: 'latitud', index: 'latitud', width: anchoP(ancho, 40) },
                    { name: 'longitud', index: 'longitud', width: anchoP(ancho, 40) },
                ],
                height: 100,
                width: ancho,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtOrdenReferencia', datosRow.Orden, 0);
                    asignaAtributo('lblOrdenReferencia', datosRow.Orden, 0);
                    asignaAtributo('txtLatitudReferencia', datosRow.latitud, 0);
                    asignaAtributo('txtLongitudReferencia', datosRow.longitud, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listMunicipioAreas':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=798&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id_municipio', 'nom_municipio', 'latitud_muni', 'longitud_muni', 'id_area', 'Nombre Area', 'Orden', 'latitud', 'longitud', 'zoom', 'color', 'color_opacidad', 'vigente'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_municipio', index: 'id_municipio', width: anchoP(ancho, 10) },
                    { name: 'nom_municipio', index: 'nom_municipio', width: anchoP(ancho, 10) },
                    { name: 'latitud_muni', index: 'latitud_muni', hidden: true },
                    { name: 'longitud_muni', index: 'longitud_muni', hidden: true },
                    { name: 'id_area', index: 'id_area', width: anchoP(ancho, 5) },
                    { name: 'NombreArea', index: 'Nombre', width: anchoP(ancho, 60) },
                    { name: 'Orden', index: 'Orden', width: anchoP(ancho, 5) },
                    { name: 'latitud', index: 'latitud', width: anchoP(ancho, 10) },
                    { name: 'longitud', index: 'longitud', width: anchoP(ancho, 10) },
                    { name: 'zoom', index: 'zoom', hidden: true },
                    { name: 'color', index: 'color', hidden: true },
                    { name: 'color_opacidad', index: 'color_opacidad', hidden: true },
                    { name: 'vigente', index: 'vigente', hidden: true },
                ],
                height: 120,
                width: ancho,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtMunicipio', datosRow.id_municipio + '-' + datosRow.nom_municipio, 0);
                    asignaAtributo('lblIdArea', datosRow.id_area, 0);
                    asignaAtributo('lblIdlatitud', datosRow.latitud, 0);
                    asignaAtributo('lblIdlongitud', datosRow.longitud, 0);
                    asignaAtributo('txtArea', datosRow.NombreArea, 0);
                    asignaAtributo('txtOrden', datosRow.Orden, 0);
                    asignaAtributo('txtLatitud', datosRow.latitud, 0);
                    asignaAtributo('txtLongitud', datosRow.longitud, 0);
                    asignaAtributo('txtZoom', datosRow.zoom, 0);
                    asignaAtributo('txtColor', datosRow.color, 0);
                    asignaAtributo('txtColorOpacidad', datosRow.color_opacidad, 0);
                    asignaAtributo('cmbVigente', datosRow.vigente, 0);

                    ubicarMapaMunicipio() /// dibuja
                    setTimeout("buscarEncuesta('listAreasRerefencias')", 2000) // grilla referencias

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
            
        case 'listEncuestaPaciente':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=877&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));
            if(valorAtributo('lblTipoDocumento') == '' ){
                var lbltipofolio = 'ASMB';
                asignaAtributo('lblTipoDocumento', lbltipofolio, 0)
                add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
            }
            else{
                add_valores_a_mandar(valorAtributo('lblTipoDocumento'));    
            }
            //alert(valorAtributo('lblTipoDocumento'))
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'Encuesta', 'Diligenciado', 'id_encuesta', 'id_estado_encuesta', 'Grafica', 'Alerta', 'Diagnostico','Conducta' , 'Fecha de cierre', 'recomendacion', 'Estado admision', 'Medio recepcion', 'Abierta por', 'soloRiesgo', 'semaforo', 'id_diagnostico_conducta'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 5) },
                    { name: 'Encuesta', index: 'Encuesta', width: anchoP(ancho, 15) },
                    { name: 'Diligenciado', index: 'Diligenciado', width: anchoP(ancho, 10) },
                    { name: 'id_encuesta', index: 'id_encuesta', hidden: true },
                    { name: 'id_estado_encuesta', index: 'id_estado_encuesta', hidden: true },
                    { name: 'grafica', index: 'grafica', hidden: true },
                    { name: 'riesgo', index: 'riesgo', width: anchoP(ancho, 10) },
                    { name: 'diagnostico', index: 'diagnostico', width: anchoP(ancho, 15) },
                    { name: 'conducta', index: 'conducta', width: anchoP(ancho, 15) },
                    { name: 'fecha_cierre', index: 'fecha_cierre', width: anchoP(ancho, 10) },
                    { name: 'recomendacion', index: 'recomendacion', hidden: true },
                    { name: 'estado_admision', index: 'estado_admision', width: anchoP(ancho, 7) },
                    { name: 'medio_recepcion', index: 'medio_recepcion', width: anchoP(ancho,7) },
                    { name: 'autor', index: 'autor', width: anchoP(ancho, 6) },
                    { name: 'soloRiesgo', index: 'soloRiesgo', hidden: true },
                    { name: 'semaforo', index: 'semaforo', hidden: true },
                    { name: 'id_diagnostico_conducta', index: 'id_diagnostico_conducta', hidden: true },
                  
                ],
                height: 80,
                autowidth: true,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdEncuesta', datosRow.id_encuesta, 0);
                    asignaAtributo('lblIdEncuestaPaciente', datosRow.id, 0);
                    asignaAtributo('lblDiligenciado', datosRow.Diligenciado, 0);
                    asignaAtributo('txtEstadoEncuesta', datosRow.id_estado_encuesta, 0);
                    asignaAtributo('lblIdGrafica', datosRow.grafica, 0);
                    colocarRiesgoEncuesta(datosRow.soloRiesgo, datosRow.semaforo, datosRow.id_diagnostico_conducta, 
                        datosRow.diagnostico, datosRow.conducta, datosRow.medio_recepcion);
                    asignaAtributo('txtRecomendacionEncuesta', datosRow.recomendacion)
                    tabsContenidoEncuesta('desdeEncuesta');
                    //alert("id encuesta: "+valorAtributo('lblIdEncuestaPaciente'));

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrafica':
        
            alert(5454)
        break;

    }
}

function buscarRiesgoEncuesta(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {
        case 'listEncuestaPacienteRiesgo':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=879&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdEncuesta'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id_encuesta_paciente', 'nombre', 'semaforo'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_encuesta_paciente', index: 'id_encuesta_paciente', hidden: true },
                    { name: 'nombre', index: 'nombre', width: anchoP(ancho, 55) },
                    { name: 'semaforo', index: 'Semaforo', width: anchoP(ancho, 35) }
                ],
                height: 80,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid); 
                
                    tabsContenidoEncuesta('desdeEncuesta');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;
    }
}

function TabActivoEncuesta(idTab) {
    asignaAtributo('lblIdConte', idTab, 0);
}
function graficaEncuesta() {
     mostrar('divVentanitaGrafica')
     contenedorGrafica()
}

function contenedorGrafica() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/encuesta/grafica.jsp?'+'idGrafica='+valorAtributo('lblIdGrafica') , true);
    varajaxMenu.onreadystatechange = llenarcontenedorGrafica;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
      
}

function llenarcontenedorGrafica() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
           document.getElementById("contenedorGraf").innerHTML = varajaxMenu.responseText; 
        //    buscarEncuesta('listEncuestaPaciente')

        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function calculoAncho() {
    total= 100;
    ancho1 = valorAtributo('txtAnCol1');
    ancho2= total - ancho1;
    document.getElementById('txtAnCol2').value = ancho2;

}

function ventanitaRecomendacionesEncuesta() {
        varajaxMenu = crearAjax();
        valores_a_mandar = "";
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaRecomendaciones.jsp?'+'&recomendacion='+valorAtributo('txtRecomendacionEncuesta'), true);
        varajaxMenu.onreadystatechange = function (){llenarventanitaRecomendacionesEncuesta()};
        varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxMenu.send(valores_a_mandar);
}

function llenarventanitaRecomendacionesEncuesta() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaventanitaRecomendaciones").innerHTML = varajaxMenu.responseText;
            mostrar('divParaventanitaRecomendaciones');
            mostrar('divVentanitaPuestaRecomendaciones');
            formatoRecomendacionTexto();

        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}




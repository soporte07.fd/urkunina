<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<%  beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1300"  align="center" cellpadding="0" cellspacing="0" >
    <tr>
        <td>
            <div align="center" id="tituloForma" >
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="GESTION AREAS GEOGRAFICAS" />
                </jsp:include>
            </div>  
        </td>
    </tr> 
    <tr>
      <td valign="top">    
        <table width="100%" class="fondoTabla" cellpadding="0" cellspacing="0" >  
          <tr class="titulos">
             <td width="20%">Municipio</td>
             <td width="20%">Nombre de Area</td>
             <td width="5%">Orden</td>  
             <td width="10%">latitud</td>  
             <td width="10%">longitud</td>  
             <td width="5%">zoom</td>  
             <td width="10%">color</td> 
             <td width="10%">color Opacidad</td> 
             <td width="10%">Vigente</td> 
          </tr>     
          <tr class="titulos">
             <td>                   
                <input type="text" id="txtMunicipio" size="60" value="52838-TUQUE"  maxlength="60"  style="width:70%"  tabindex="108"/> 
                 <img width="18px" height="18px" align="middle" title="VEN71" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id,'','divParaVentanita','txtMunicipio','1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                 <div id="divParaVentanita"></div>                    
             </td> 

             <td width="20%">
               <label id="lblIdArea" >0</label>
               <input type="text" id="txtArea"  style="width:80%"   size="100" maxlength="100" tabindex="01"/>
             </td>
             <td ><input type="text" id="txtOrden"  style="width:80%"   size="20" maxlength="20" tabindex="01"/></td>  
             <td ><input type="text" id="txtLatitud"  style="width:80%"   size="20" maxlength="20" tabindex="01"/></td>  
             <td ><input type="text" id="txtLongitud"  style="width:80%"   size="20" maxlength="20" tabindex="01"/></td>  
             <td ><label id="lblIdZoom" >16</label><input type="text" id="txtZoom"  style="width:40%"   size="20" maxlength="20" tabindex="01"/></td>  
             <td ><input type="text" id="txtColor"  style="width:80%"   size="20" maxlength="20" tabindex="01"/></td> 
             <td ><input type="text" id="txtColorOpacidad"  style="width:80%"   size="20" maxlength="20" tabindex="01"/></td> 
             <td >
                  <select size="1" id="cmbVigente" style="width:90%" tabindex="100">
                    <option value="S">SI</option>
                    <option value="N">NO</option>
                  </td>   
             </td> 
          </tr> 


          <tr class="estiloImput">
             <td colspan="9">
                <label id="lblIdlatitud" >0</label>
                <label id="lblIdlongitud" >0</label>
                <input title="GE86" type="button" class="small button blue" value="CREAR AREA" onclick="modificarMapCRUD('crearArea')"/>
                <input title="GE87" type="button" class="small button blue" value="ELIMINAR AREA" onclick="modificarMapCRUD('eliminarArea')"/>
                <input title="GE88" type="button" class="small button blue" value="BUSCAR AREAS" onclick="buscarEncuesta('listMunicipioAreas')"/>
                <!--  <input title="GE88" type="button" class="small button blue" value="UBICAR MAPA" onclick="ubicarMapaMunicipio()"  /> -->
             </td>                    
          </tr>    
          <tr class="titulos">
            <td colspan="5" valign="top" height='285px'>
                <TABLE width="102%"  height='235px' class="fondoTabla" cellpadding="0" cellspacing="0" >  
                  <TR>
                    <TD  width="102%" height='232px' valign="top">
                      <table id="listMunicipioAreas"  width="100%"  class="scroll" cellpadding="0" cellspacing="0" align="center" ></table>              
                    </TD>
                  </TR>
                </TABLE>    

            </td>
            <td colspan="4" valign="top" height='185px'>
               
                <table width="100%" height='170px' class="fondoTabla" cellpadding="0" cellspacing="0" >  
                  <tr class="titulos">
                     <td width="30%"  valign="top">Orden</td>  
                     <td width="30%">latitud</td>  
                     <td width="30%">longitud</td>
                  </tr>
                  <tr class="estiloImput">
                    <td>
                      <label id="lblOrdenReferencia" >0</label> 
                      <input type="text" id="txtOrdenReferencia"  style="width:20%"   maxlength="20" tabindex="01"/>
                    </td>
                    <td><input type="text" id="txtLatitudReferencia"  style="width:80%"   maxlength="20" tabindex="01"/></td>
                    <td><input type="text" id="txtLongitudReferencia"  style="width:80%"   maxlength="20" tabindex="01"/></td>
                  </tr>          
                  <tr class="estiloImput">
                    <td><input title="GE96" type="button" class="small button blue" value="Adicionar" onclick="modificarMapCRUD('adicionarReferencia')"/></td>
                    <td><input title="GE97" type="button" class="small button blue" value="Modificar" onclick="modificarMapCRUD('modificarReferencia')"/></td>
                    <td><input title="GE98" type="button" class="small button blue" value="Recarga" onclick="recargarMapa()"/></td>
                  </tr>    
                  <tr class="titulos">
                     <td colspan="3"  valign="top"  valign="top" height='170px'>  
                        <table id="listAreasRerefencias"  width="100%" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                     </td> 
                  </tr>                                    
                </table>                  


             
            </td>            
          </tr>
          <tr class="titulos">
            <td height="1000px" colspan="9" valign="top" >

                <table id="idTableContenedorMapa" style="height: 90%; width: 100%" class="scroll" cellpadding="0" cellspacing="0" align="center">
                   <tr>
                    <td  valign="top"  style="height: 100%; width: 100%" id="idTableContenedorMapaTd">
                     <div id="mapaGeoArea" style="height: 90%; width: 100%"></div>
                   </td>
                 </tr> 
                </table>              
            </td>                              
          <tr>  
        </table>   

      </td>    
    </tr>        
</table>    




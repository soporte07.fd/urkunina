
function buscarParametros(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {
        case 'listEspecialidadProfesional':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=947" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: [ 'TOTAL','ID_ESPECIALIDAD','ID_PERSONAL','NOMBRE','ID_PROFESION','PROFESION'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL',  hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD',  hidden: true  },
                    { name: 'ID_PERSONAL', index: 'ID_PERSONAL',   hidden: true  },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 20)},
                    { name: 'ID_PROFESION', index: 'ID_PROFESION',  hidden: true  },
                    { name: 'PROFESION', index: 'PROFESION', width: anchoP(ancho, 20) },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdGrupo', datosRow.ID_PERSONAL, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
            
        case 'listIncapacidad':

            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=781" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'Id', 'Id_evolucion', 'id_paciente', 'Numero dias', 'Fecha Inicio', 'Motivo', 'Diagnostico',],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'Id', index: 'ID', hidden: true },
                    { name: 'Id_evolucion', index: 'ID_EVOLUCION', hidden: true },
                    { name: 'id_paciente', index: 'ID_PACIENTE', hidden: true },
                    { name: 'Numero_dias', index: 'NO_DIAS', width: anchoP(ancho, 10) },
                    { name: 'Fecha_Desde', index: 'FECHA_DESDE', width: anchoP(ancho, 10) },
                    // { name: 'Fecha_Hasta', index: 'FECHA_HASTA', width: anchoP(ancho, 10) },
                    { name: 'Motivo', index: 'MOTIVO', width: anchoP(ancho, 10) },
                    { name: 'Diagnostico', index: 'DIAGNOSTICO', width: anchoP(ancho, 10) },
                    // { name: 'Observacion', index: 'OBSERVACION', width: anchoP(ancho, 10) },
                ],
                height: 100,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdIncap', datosRow.Id, 0);
                    asignaAtributo('lblNumerodias', datosRow.Numero_dias, 0);
                    asignaAtributo('lblMotivo', datosRow.Motivo, 0);
                    asignaAtributo('lblFechaI', datosRow.Fecha_Desde, 0);
                    // asignaAtributo('lblFechaF', datosRow.Fecha_Hasta, 0);

                    mostrar('divVentanitaIncapacidad');

                    asignaAtributo('txtDiasIncapacidad', datosRow.Numero_dias, 0);
                    asignaAtributo('txtFechaIncaInicio', datosRow.Fecha_Desde, 0);
                    // asignaAtributo('txtFechaIncaFin', datosRow.Fecha_Hasta, 0);
                    asignaAtributo('txtMotivo', datosRow.Motivo, 0);
                    // asignaAtributo('txtObservaciones', datosRow.Observacion, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaEducacionPac':

            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=933" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'EVOLUCION', 'PACIENTE', 'OBSERVACION'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'EVOLUCION', index: 'ID_EVOLUCION', width: anchoP(ancho, 10) },
                    { name: 'PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 70) },
                ],
                height: 100,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtObservacionesEducacion', datosRow.OBSERVACION, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        // eliminar educacion
        // editar educacion





        case 'listEspecialidad':

            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=896" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'VIGENTE', 'ID_SERVICIO', 'SERVICIO'
                ],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', width: anchoP(ancho, 20) },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true  },
                    { name: 'ID_SERVICIO', index: 'ID_SERVICIO', hidden: true },
                    { name: 'SERVICIO', index: 'SERVICIO', width: anchoP(ancho, 8) },
                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID_ESPECIALIDAD, 1)
                    asignaAtributo('txtNombre', datosRow.ESPECIALIDAD, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    asignaAtributoCombo('cmbServicio', datosRow.ID_SERVICIO, datosRow.SERVICIO)
                    buscarParametros('listSedeEspecialidad')
                    setTimeout(() => {
                        buscarParametros('listEspecialidadProfesional')
                    }, 300);
                   

                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listSedeEspecialidad':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=902" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'ID_SEDE', 'SEDE'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', hidden: true },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', width: anchoP(ancho, 20) },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdSede', datosRow.ID_SEDE + "-" + datosRow.SEDE, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaGrupopciones':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=944" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID_MENU', 'DESCRIPCION', 'OPCION', 'ID_GRUPO'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', width: anchoP(ancho, 5) },
                    { name: 'ID_MENU', index: 'ID_MENU', width: anchoP(ancho, 10) },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 30) },
                    { name: 'OPCION', index: 'OPCION', width: anchoP(ancho, 30) },
                    { name: 'ID_GRUPO', index: 'ID_GRUPO', hidden: true },
                   

                ],
                height: 350,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdGrupo', datosRow.ID_MENU, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        
            case 'listSede':

                ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
    
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=951" + "&parametros=";
                add_valores_a_mandar(valorAtributo('txtBusNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
                add_valores_a_mandar(IdEmpresa());
    
                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['TOTAL', 'ID', 'NOMBRE', 'VIGENTE', 'DIRECCION', 'TELEFONO', 'NOMBRE_ALT', 'DIRECCION2', 'TELEFONO2', 'CELULAR',   'COD_HABILITACION', 'ID_SECCIONAL', 'SECCIONAL', 'SW_HOME'
                    ],
                    colModel: [
                        { name: 'TOTAL', index: 'TOTAL', width: anchoP(ancho, 4)},
                        { name: 'ID', index: 'ID',  hidden: true },
                        { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 65) },
                        { name: 'VIGENTE', index: 'VIGENTE', hidden: true},
                        { name: 'DIRECCION', index: 'DIRECCION', width: anchoP(ancho, 25) },
                        { name: 'TELEFONO', index: 'TELEFONO', hidden: true },
                        { name: 'NOMBRE_ALT', index: 'NOMBRE_ALT', hidden: true },
                        { name: 'DIRECCION2', index: 'DIRECCION2', hidden: true},
                        { name: 'TELEFONO2', index: 'TELEFONO2', hidden: true},
                        { name: 'CELULAR', index: 'CELULAR', hidden: true },
                        { name: 'COD_HABILITACION', index: 'COD_HABILITACION', hidden: true },
                        { name: 'ID_SECCIONAL', index: 'ID_SECCIONAL', hidden: true  },
                        { name: 'NOMBRE SECCIONAL', index: 'SECCIONAL', width: anchoP(ancho, 6) },
                        { name: 'SW_HOME', index: 'SW_HOME', hidden: true  },
                        
                    ],
                    height: 320,
                    width: ancho,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                        asignaAtributo('txtId', datosRow.ID, 0)
                        asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                        asignaAtributo('txtNombre2', datosRow.NOMBRE_ALT, 0)
                        asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                        asignaAtributo('txtDireccion', datosRow.DIRECCION, 0)
                        asignaAtributo('txtTelefono', datosRow.TELEFONO, 0)
                        asignaAtributo('txtNobre2', datosRow.NOMBRE_ALT, 0)
                        asignaAtributo('txtDireccion2', datosRow.DIRECCION2, 0)
                        asignaAtributo('txtTelefono2', datosRow.TELEFONO2, 0)
                        asignaAtributo('txtCelular', datosRow.CELULAR, 0)
                        asignaAtributo('txtCodHabilitacion', datosRow.COD_HABILITACION, 0)
                        asignaAtributo('cmbServicio', datosRow.SW_HOME, 0)
                        asignaAtributo('cmbSeccional', datosRow.ID_SECCIONAL, 0)
                       
    
                    }
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
    
                break;

        case 'listArea':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=916" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbSede1'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL',  'ID','NOMBRE', 'VIGENTE', 'ID_SERVICIO', 'SERVICIO', 'ID_SEDE', 'SEDE'
                ],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', width: anchoP(ancho, 2)},
                    { name: 'ID', index: 'ID',  hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 20) },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true},
                    { name: 'ID_SERVICIO', index: 'ID_SERVICIO', hidden: true },
                    { name: 'SERVICIO', index: 'SERVICIO', width: anchoP(ancho, 20) },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', hidden: true },
                   
                ],
                height: 320,
                width: ancho,
                
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    asignaAtributo('cmbServicio', datosRow.ID_SERVICIO, 0)
                    asignaAtributo('cmbSede', datosRow.ID_SEDE, 0)

                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listGrillaTipoCita':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=714" + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidadBus'));            
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'RECOMENDACIONES', 'VIGENTE', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'TIENE COSTO', 'NUMERO PROCEDIMIENTOS'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 8) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 20) },
                    { name: 'RECOMENDACIONES', index: 'RECOMENDACIONES', hidden: true },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true},
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', width: anchoP(ancho, 10) },
                    { name: 'TIENE_COSTO', index: 'TIENE_COSTO', hidden: true },
                    { name: 'NUMERO_PROCEDIMIENTOS', index: 'NUMERO_PROCEDIMIENTOS', hidden: true },
                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 1)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('txtRecomendaciones', datosRow.RECOMENDACIONES, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    asignaAtributo('cmbIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0)
                    asignaAtributo('cmbCosto', datosRow.TIENE_COSTO, 0)
                    buscarParametros('listGrillaTipoCitaProcedimientos')
                   // setTimeout("buscarParametros('listGrillaTipoCitaProcedimientosPrestador')", 400);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
        break;
        case 'listGrillaExcepcion':

            ancho = 1000;

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=715" + "&parametros=";

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'PROCEDIMIENTO ID', 'NOMBRE', 'ESTADO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_PROCEDIMIENTO', index: 'ID_PROCEDIMIENTO', width: anchoP(ancho, 8) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 20) },
                    { name: 'ESTADO', index: 'ESTADO', hidden: true },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdProcedimientoe', datosRow.ID_PROCEDIMIENTO + '-' + datosRow.NOMBRE, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaTipoCitaProcedimientos':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=720" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID TIPO CITA', 'ID PROCEDIMIENTO', 'NOMBRE PROCEDIMIENTO', 'VIGENTE', 'CUPS',
                    'CUM', 'POS', 'NIVEL'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_TIPO_CITA', index: 'ID_TIPO_CITA', width: anchoP(ancho, 10) },
                    { name: 'ID_PROCEDIMIENTO', index: 'ID_PROCEDIMIENTO', width: anchoP(ancho, 15) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 70) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 10) },
                    { name: 'CUPS', index: 'CUPS', width: anchoP(ancho, 10) },
                    { name: 'CUM', index: 'CUM', hidden: true  },
                    { name: 'POS', index: 'POS', width: anchoP(ancho, 10) },
                    { name: 'NIVEL', index: 'NIVEL', width: anchoP(ancho, 10) },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID_TIPO_CITA, 1)
                    asignaAtributo('txtIdProcedimiento', datosRow.ID_PROCEDIMIENTO + '-' + datosRow.NOMBRE, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        /* Grilla para el listado de habitaciones*/
        case 'listGrillaHabitacion':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=926&parametros=";
            add_valores_a_mandar(valorAtributo('cmbSede1'));
            add_valores_a_mandar(valorAtributo('cmbSede1'));
            add_valores_a_mandar(valorAtributo('cmbIdArea1'));
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID', 'ID_SEDE', 'SEDE', 'HABITACION',  'ID_TIPO', 'TIPO DE HABITACION', 'ID_AREA', 'AREA', 'ID_CENTRO_COSTO', 'CENTRO COSTO', 'VIGENTE'
                ],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL',  hidden: true  },
                    { name: 'ID', index: 'ID', hidden: true  },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', width: anchoP(ancho, 30) },
                    { name: 'HABITACION', index: 'HABITACION', width: anchoP(ancho, 30) },
                    { name: 'ID_TIPO', index: 'TIPO', hidden: true },
                    { name: 'TIPO DE HABITACION', index: 'TIPO DE HABITACION', width: anchoP(ancho, 20) },
                    { name: 'ID_AREA', index: 'ID_AREA', hidden: true },
                    { name: 'AREA', index: 'AREA', width: anchoP(ancho, 20)  },
                    { name: 'ID_CENTRO_COSTO', index: 'ID_CENTRO_COSTO', hidden: true },
                    { name: 'CENTRO COSTO', index: 'CENTRO COSTO', hidden: true },
                    { name: 'VIGENTE', index: 'VIGENTE',hidden: true  },
                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 1)
                    asignaAtributo('txtNombre', datosRow.HABITACION, 0)
                    asignaAtributoCombo('cmbIdArea', datosRow.ID_AREA, datosRow.AREA)
                    asignaAtributoCombo('cmbSedeCrear', datosRow.ID_SEDE, datosRow.SEDE)
                    asignaAtributo('cmbIdTipo', datosRow.ID_TIPO, 0)
                    asignaAtributo('cmbCosto', datosRow.ID_CENTRO_COSTO, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                   // asignaAtributo('cmbIdAdmision', datosRow.ID_ADMISION, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaCama':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=927" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID', 'CAMA', 'ID_ESTADO', 'ESTADO', 'ID_TIPO', 'TIPO DE CAMA', 'ID_HABITACION', 'HABITACION', 'VIGENTE'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL',width: anchoP(ancho, 2)},
                    { name: 'ID', index: 'ID', hidden: true  },
                    { name: 'CAMA', index: 'CAMA', width: anchoP(ancho, 30) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTAD0', hidden: true },
                    { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                    { name: 'TIPO DE CAMA', index: 'TIPO DE CAMA', width: anchoP(ancho, 20) },
                    { name: 'ID_HABITACION', index: 'ID_HABITACION', hidden: true },
                    { name: 'HABITACION', index: 'HABITACION', width: anchoP(ancho, 30) },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true },

                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 1)
                    asignaAtributo('txtNombre', datosRow.CAMA, 0)
                    asignaAtributo('cmbIdEstado', datosRow.ID_ESTADO, 1)
                    asignaAtributo('cmbIdTipo', datosRow.ID_TIPO, 0)
                    asignaAtributo('cmbHabitacion', datosRow.ID_HABITACION, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

         case 'listGrillaProfesion':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=954" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID', 'PROFESION', 'DESCRIPCION',  'ID_TIPO', 'TIPO', 'PREFIJO',  'SW_FOLIO','ID_LAZOS','VIGENTE'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL',width: anchoP(ancho, 5)},
                    { name: 'ID', index: 'ID', hidden: true  },
                    { name: 'PROFESION', index: 'PROFESION', width: anchoP(ancho, 30) },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', hidden: true },
                    { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 10) },
                    { name: 'PREFIJO', index: 'PREFIJO', hidden: true },
                    { name: 'SW_FOLIO', index: 'SW_FOLIO', hidden: true },
                    { name: 'ID_LAZOS', index: 'ID_LAZOS', hidden: true },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true },

                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.PROFESION, 0)
                    asignaAtributo('txtNombre2', datosRow.DESCRIPCION, 0)
                    asignaAtributo('cmbIdEstado', datosRow.ID_TIPO, 0)
                    asignaAtributo('txtPrefijo', datosRow.PREFIJO, 0)
                    asignaAtributo('cmbFolio', datosRow.SW_FOLIO, 0)
                    asignaAtributo('txtCod', datosRow.ID_LAZOS, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listGrillaTipoCitaProcedimientosPrestador':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=856" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID EMPRESA', 'RAZON SOCIAL', 'CODIGO HABILITACION', 'DIRECCION', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_EMPRESA', index: 'ID_EMPRESA', width: anchoP(ancho, 10) },
                    { name: 'RAZON_SOCIAL', index: 'RAZON_SOCIAL', width: anchoP(ancho, 50) },
                    { name: 'CODIGO_HABILITACION', index: 'CODIGO_HABILITACION', width: anchoP(ancho, 20) },
                    { name: 'DIRECCION', index: 'DIRECCION', width: anchoP(ancho, 20) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 20) }

                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtPrestadorTecnologia', datosRow.ID_EMPRESA + '-' + datosRow.RAZON_SOCIAL, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaPiePagina':

            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=731" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'TIPO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 8) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 60) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 8) },
                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbTipo', datosRow.TIPO, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;

        case 'listGrillaCentroCostos':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=740" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'PADRE', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 8) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 60) },
                    { name: 'PADRE', index: 'PADRE', hidden: true },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 8) },


                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbPadre', datosRow.PADRE, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaPlanAtencionRuta':

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=174&parametros=";
            add_valores_a_mandar(valorAtributo('txtNombrePlanAtencion'));
            add_valores_a_mandar(valorAtributo('cmbVigente'));
            add_valores_a_mandar(IdEmpresa());

            var editOptions = {
                editData: {
                    accion: "editarPlanAtencion",
                    idQuery: "194",
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                closeAfterEdit: true,
                recreateForm: true,
                onclickSubmit: function (params) {
                    var id = jQuery("#listGrillaPlanAtencionRuta").jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#listGrillaPlanAtencionRuta").jqGrid('getRowData', id);
                    return { id: ret.id }
                }
            };
            var addOptions = {
                editData: {
                    accion: "crearPlanAtencion",
                    idQuery: "188"
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                recreateForm: true,
                closeAfterAdd: true
            };
            var delOptions = {
                delData: {
                    accion: "eliminarPlanAtencion",
                    idQuery: "195"
                }, afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje];
                },
                onclickSubmit: function (params) {
                    var id = jQuery("#listGrillaPlanAtencionRuta").jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#listGrillaPlanAtencionRuta").jqGrid('getRowData', id);
                    return { idDel: ret.id }
                }
            };

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'VIGENTE'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 10), hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 40), editrules: { required: true }, editable: true, edittype: "text" },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 10), editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: { 'S': 'SI', 'N': 'NO' } } }
                ],
                height: 374,
                autowidth: true,
                caption: "Rutas",
                pager: "#pagerG",
                editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
                /*onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdPlanAtencion', datosRow.ID, 0)
                    asignaAtributo('txtNombrePlanAtencion', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbVigentePlanAtencion', datosRow.VIGENTE, 0)
                },*/
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid').navGrid('#pagerG', {
                del: true, add: true, edit: true, view: true, refresh: true, search: false,
                edittext: "Editar", addtext: "Adicionar", viewtext: "Ver", refreshtext: "Refrescar", deltext: "Eliminar"
            }, editOptions, addOptions, delOptions);
            break;
    }
}


function modificarCRUDParametros(arg, pag) {

    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    if (verificarCamposGuardar(arg))
        switch (arg) {      
            case 'eliminarFolioProcedimiento':
                tipo_cita = $("#listGrillaTiposCita").jqGrid('getGridParam', 'selrow');
                tipo_formulario = $("#listGrillaTiposFormulario").jqGrid('getGridParam', 'selrow');
                procedimiento = $("#listGrillaFolioProcedimientos").jqGrid('getGridParam', 'selrow');

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=817&parametros="; 
                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);             
                add_valores_a_mandar($("#listGrillaTiposCita").getRowData(tipo_cita).ID_TIPO);
                add_valores_a_mandar($("#listGrillaFolioProcedimientos").getRowData(procedimiento).ID_PROCEDIMIENTO);
                ajaxModificar();
                break;

            case 'adicionarFolioProcedimiento':
                tipo_cita = $("#listGrillaTiposCita").jqGrid('getGridParam', 'selrow');
                tipo_formulario = $("#listGrillaTiposFormulario").jqGrid('getGridParam', 'selrow');

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=816&parametros="; 
                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);             
                add_valores_a_mandar($("#listGrillaTiposCita").getRowData(tipo_cita).ID_TIPO);
                add_valores_a_mandar($("#listGrillaTiposCita").getRowData(tipo_cita).ID_TIPO);
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));
                add_valores_a_mandar('S')
                //add_valores_a_mandar(valorAtributo('cmbVigenteFolioProcedimiento'))
                ajaxModificar();
            break;

            case 'adicionarTipoCitaFormulario':
                tipo_formulario = $("#listGrillaTiposFormulario").jqGrid('getGridParam', 'selrow');

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=961&parametros=";            
                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);             
                add_valores_a_mandar(valorAtributo('cmbTipoCita'));       
                ajaxModificar();
            break;

            case 'eliminarTipoCitaFormulario':
                tipo_cita = $("#listGrillaTiposCita").jqGrid('getGridParam', 'selrow');
                tipo_formulario = $("#listGrillaTiposFormulario").jqGrid('getGridParam', 'selrow');

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=962&parametros=";                            
                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);             
                add_valores_a_mandar($("#listGrillaTiposCita").getRowData(tipo_cita).ID_TIPO); 
                
                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);             
                add_valores_a_mandar($("#listGrillaTiposCita").getRowData(tipo_cita).ID_TIPO); 
                ajaxModificar();
            break;

         case "eliminarProfesionalEspecialidad":
            valores_a_mandar = "accion=" + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=949&parametros=";

            add_valores_a_mandar(valorAtributo("txtId"));
            add_valores_a_mandar(valorAtributo("lblIdGrupo"));
            ajaxModificar();
            break;

            case 'adicionarParametro':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=846&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                add_valores_a_mandar(valorAtributo('cmbEsperar'));
                add_valores_a_mandar(valorAtributo('txtOrden'));
                add_valores_a_mandar(valorAtributo('cmbEstado'));
                ajaxModificar();
                break;

            case 'agregarEspProfesional':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1803&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdGrupo'));
               
                ajaxModificar();
                break;

            case 'crearUsuario':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=596&parametros=";
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('cmbTipoId'));
                    add_valores_a_mandar(valorAtributo('txtPNombre'));
                    add_valores_a_mandar(valorAtributo('txtSNombre'));
                    add_valores_a_mandar(valorAtributo('txtPApellido'));
                    add_valores_a_mandar(valorAtributo('txtSApellido'));
                    add_valores_a_mandar(valorAtributo('txtNomPersonal'));
                    add_valores_a_mandar(valorAtributo('cmbIdTipoProfesion'));
                    add_valores_a_mandar(valorAtributo('txtRegistro'));
                    add_valores_a_mandar(valorAtributo('txtPhone'));
                    add_valores_a_mandar(valorAtributo('txtCorreo'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));


                    /* usuario */
                    add_valores_a_mandar(valorAtributo('cmbTipoId'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('txtUSuario'));
                    add_valores_a_mandar(valorAtributo('cmbEstado'));
                    /* sede */
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));
                    ajaxModificar();
                }
                break;
            case 'modificarUsuario':
                if (verificarCamposGuardar(arg)) {

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=597&parametros=";

                    add_valores_a_mandar(valorAtributo('txtPNombre'));
                    add_valores_a_mandar(valorAtributo('txtSNombre'));
                    add_valores_a_mandar(valorAtributo('txtPApellido'));
                    add_valores_a_mandar(valorAtributo('txtSApellido'));
                    add_valores_a_mandar(valorAtributo('txtNomPersonal'));
                    add_valores_a_mandar(valorAtributo('cmbIdTipoProfesion'));
                    add_valores_a_mandar(valorAtributo('txtRegistro'));
                    add_valores_a_mandar(valorAtributo('txtPhone'));
                    add_valores_a_mandar(valorAtributo('txtCorreo'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));

                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('cmbTipoId'));

                    /* usuario */
                    add_valores_a_mandar(valorAtributo('txtUSuario'));
                    add_valores_a_mandar(valorAtributo('cmbEstado'));
                    add_valores_a_mandar(valorAtributo('cmbTipoId'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    /* sede */
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));

                    ajaxModificar();
                }
                break;
            case 'crearTipoCita':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=725&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'))
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('txtRecomendaciones'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbCosto'));
                add_valores_a_mandar(IdEmpresa());

                ajaxModificar();
                break;
            case 'modificarTipoProcedim':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=724&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('txtRecomendaciones'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbCosto'));
                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;
            case 'eliminarTipoCita':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=957&parametros=";                    
                    add_valores_a_mandar(valorAtributo('txtId'))
                    ajaxModificar();
                }
                break;                
            case 'adicionarPrestadorTecnologia':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=857&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorTecnologia'))
                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;
            case 'eliminarPrestadorTecnologia':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=858&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorTecnologia'))
                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;

            case 'crearTipoCitaProcedimiento':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=726&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'))
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'))

                ajaxModificar();
                break;

            case 'eliminarTipoCitaProcedimiento':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=727&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'))
                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;
            case 'modificarPiePagina':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=732&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbTipo'));
                add_valores_a_mandar(valorAtributo('lblId'))
                ajaxModificar();
                break;

            case 'adicionarPiePagina':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=733&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbTipo'));
                add_valores_a_mandar(IdEmpresa());
                ajaxModificar();
                break;
            case 'adicionarCentroCosto':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=741&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                //add_valores_a_mandar(valorAtributo('cmbPadre'));
                ajaxModificar();
                break;
            case 'modificarCentroCosto':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=742&parametros=";

                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbPadre'));
                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;
            case 'eliminarCentroCosto':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=743&parametros=";

                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;
            case 'adicionarProcedimientoE':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=784&parametros=";

                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoe'))
                ajaxModificar();
                break;
            case 'eliminarProcedimientoE':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=785&parametros=";

                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoe'))
                ajaxModificar();
                break;
            case 'crearSede':
             if (verificarCamposGuardar(arg)) {   
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=953&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('txtNombre2'));
                add_valores_a_mandar(valorAtributo('txtDireccion'));
                add_valores_a_mandar(valorAtributo('txtDireccion2'));
                add_valores_a_mandar(valorAtributo('txtTelefono'))
                add_valores_a_mandar(valorAtributo('txtTelefono2'))
                add_valores_a_mandar(valorAtributo('txtCelular'));
                add_valores_a_mandar(valorAtributo('txtCodHabilitacion'));
                add_valores_a_mandar(valorAtributo('cmbSeccional'));
                add_valores_a_mandar(valorAtributo('cmbServicio'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                
               
                ajaxModificar();

            }
                break;
            
                case 'crearProfesion':
                    if (verificarCamposGuardar(arg)) {   
                       valores_a_mandar = 'accion=' + arg;
                       valores_a_mandar = valores_a_mandar + "&idQuery=955&parametros=";
                       add_valores_a_mandar(valorAtributo('txtNombre'));
                       add_valores_a_mandar(valorAtributo('txtNombre2'));
                       add_valores_a_mandar(valorAtributo('cmbIdEstado'));
                       add_valores_a_mandar(valorAtributo('txtPrefijo'));
                       add_valores_a_mandar(valorAtributo('cmbFolio'));
                       add_valores_a_mandar(valorAtributo('txtCod'));
                       add_valores_a_mandar(valorAtributo('cmbVigente'));
                       
                      
                       ajaxModificar();
       
                   }
                       break;
                 case 'modificarProfesion':
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=956&parametros=";
                        add_valores_a_mandar(valorAtributo('txtNombre'));
                       add_valores_a_mandar(valorAtributo('txtNombre2'));
                       add_valores_a_mandar(valorAtributo('cmbIdEstado'));
                       add_valores_a_mandar(valorAtributo('txtPrefijo'));
                       add_valores_a_mandar(valorAtributo('cmbFolio'));
                       add_valores_a_mandar(valorAtributo('txtCod'));
                       add_valores_a_mandar(valorAtributo('cmbVigente'));
                       add_valores_a_mandar(valorAtributo('txtId'));
        
                          ajaxModificar();
                 break;

            case 'crearEspecialidadSede':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=899&parametros=";
               // add_valores_a_mandar(valorAtributo('txtId'))
                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbServicio'));
                add_valores_a_mandar(valorAtributo('txtCodigo'));

                ajaxModificar();
                break;


            case 'crearArea':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=919&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbServicio'));
                add_valores_a_mandar(valorAtributo('cmbSede'));

                ajaxModificar();
                break;

            case 'modificarSede':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=952&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('txtNombre2'));
                add_valores_a_mandar(valorAtributo('txtDireccion'));
                add_valores_a_mandar(valorAtributo('txtDireccion2'));
                add_valores_a_mandar(valorAtributo('txtTelefono'));
                add_valores_a_mandar(valorAtributo('txtTelefono2'));
                add_valores_a_mandar(valorAtributo('txtCelular'));
                add_valores_a_mandar(valorAtributo('txtCodHabilitacion'));
                add_valores_a_mandar(valorAtributo('cmbSeccional'));
                add_valores_a_mandar(valorAtributo('cmbServicio'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('txtId'));
                ajaxModificar();
                break;

             case 'modificarArea':
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=920&parametros=";
                    add_valores_a_mandar(valorAtributo('txtNombre'))
                    add_valores_a_mandar(valorAtributo('cmbVigente'));
                    add_valores_a_mandar(valorAtributo('cmbServicio'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));
                    add_valores_a_mandar(valorAtributo('txtId'));
                    ajaxModificar();
                    break;

            case 'crearHabitacion':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=922&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbIdArea'));
                add_valores_a_mandar(valorAtributo('cmbIdTipo'));
                add_valores_a_mandar(valorAtributo('cmbCosto'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                ajaxModificar();
                break;

            case 'modificarHabitacion':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=923&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbIdArea'));
                add_valores_a_mandar(valorAtributo('cmbIdTipo'));
                add_valores_a_mandar(valorAtributo('cmbCosto'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('txtId'));
                ajaxModificar();
                break;

            case 'crearElemento':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=928&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbIdEstado'));
                add_valores_a_mandar(valorAtributo('cmbIdTipo'));
                add_valores_a_mandar(valorAtributo('cmbHabitacion'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));

                ajaxModificar();
                break;

            case 'crearPerfil':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=942&parametros=";
                    add_valores_a_mandar(valorAtributo('txtId'))
                    add_valores_a_mandar(valorAtributo('txtNombre'))
                    add_valores_a_mandar(valorAtributo('txtdefinicion'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));

                    ajaxModificar();
                }
                break;

            case 'modificarPerfil':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=943&parametros=";
                    add_valores_a_mandar(valorAtributo('txtNombre'))
                    add_valores_a_mandar(valorAtributo('txtdefinicion'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));
                    add_valores_a_mandar(valorAtributo('txtId'));

                    ajaxModificar();
                }
                break;

            case 'modificarElemento':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=929&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbIdEstado'));
                add_valores_a_mandar(valorAtributo('cmbIdTipo'));
                add_valores_a_mandar(valorAtributo('cmbHabitacion'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('txtId'));


                ajaxModificar();
                break;

            case 'inhabilitarElemento':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=921&parametros=";
                    add_valores_a_mandar(valorAtributo('txtId'));


                    ajaxModificar();
                }
                break;

            case 'asignarSedeEspecialidad':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=900&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'))
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdSede'))

                ajaxModificar();
                break;
            case 'ubicarHabitacionArea':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=900&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'))
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtidhabitacion'))
                ajaxModificar();
                break;
            case 'modificarEspecialidad':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=901&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbServicio'));
                add_valores_a_mandar(valorAtributo('txtCodigo'));
                add_valores_a_mandar(valorAtributo('txtId'));
                ajaxModificar();
                break;

            case 'eliminarEspecialidadSede':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=903&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdSede'))
                add_valores_a_mandar(valorAtributo('txtId'));

                ajaxModificar();
                break;
        }
}
function respuestaCRUDParametros(arg, xmlraiz) {

    if (xmlraiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {

        switch (arg) {
            case 'eliminarFolioProcedimiento':
                alert("Elemento eliminado")
                buscarHC('listGrillaFolioProcedimientos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                limpiaAtributo('txtIdProcedimiento', 0)
                asignaAtributo('cmbVigenteFolioProcedimiento', 'S', 0)
                break;

            case 'adicionarFolioProcedimiento':
                alert("Elemento adicionado")
                buscarHC('listGrillaFolioProcedimientos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                limpiaAtributo('txtIdProcedimiento', 0)
                asignaAtributo('cmbVigenteFolioProcedimiento', 'S', 0)
                break;

            case 'adicionarTipoCitaFormulario':
                alert('Elemento adicionado');
                buscarHC('listGrillaTiposCita');
                limpiaAtributo('cmbIdEspecialidad',0);
                limpiaAtributo('cmbTipoCita',0);
            break;

            case 'eliminarTipoCitaFormulario':
                alert('Elemento eliminado');
                buscarHC('listGrillaTiposCita');
                setTimeout(() => {
                    buscarHC('listGrillaFolioProcedimientos');
                }, 300);
                limpiaAtributo('cmbIdEspecialidad',0);
                limpiaAtributo('cmbTipoCita',0);
            break;

            case 'agregarEspProfesional':
                buscarParametros('listEspecialidadProfesional')
                alert('Profesional Agregado Exitosamente')
                break;

            case "eliminarProfesionalEspecialidad":
                buscarParametros('listEspecialidadProfesional');
                alert("Eliminado exitosamente");
                limpiaAtributo('cmbIdGrupo', 0);
                break;


            case 'crearPacientePrograma':
                buscarProgramas('listGrillaParametrosPaciente');
                buscarProgramas('listGrillaProgramaPaciente');
                break;

            case 'adicionarParametro':
                buscarProgramas('listGrillaParametros');
                break;

            case 'crearUsuario':
                buscarUsuario('listGrillaUsuarios')
                buscarUsuario('sede')
                alert('Creado exitosamente');
                limpiarDivEditarJuan('limpiarCrearUsuario');
                break;

            case 'modificarUsuario':
                buscarUsuario('listGrillaUsuarios')
                buscarUsuario('sede')
                alert('Modificado exitosamente');
                limpiarDivEditarJuan('limpiarCrearUsuario');
                break;

            case 'adicionarPrestadorTecnologia':
                limpiaAtributo('txtPrestadorTecnologia', 0)
                buscarParametros('listGrillaTipoCitaProcedimientosPrestador')
                break;
            case 'eliminarPrestadorTecnologia':
                limpiaAtributo('txtPrestadorTecnologia', 0)
                buscarParametros('listGrillaTipoCitaProcedimientosPrestador')
                break;


            case 'crearTipoCita':
                buscarParametros('listGrillaTipoCita')
                alert('Tipo cita procedimiento registrado')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbCosto', 0)
                limpiaAtributo('txtRecomendaciones', 0)
                break;
            case 'crearEspecialidadSede':
                buscarParametros('listEspecialidad')
                alert('Especialidad  registrada')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbServicio', 0)
                break;
            case 'crearProfesion':
                buscarParametros('listGrillaProfesion')
                alert('Profesion  registrada')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('txtNombre2', 0)
                limpiaAtributo('cmbIdEstado', 0)
                limpiaAtributo('txtPrefijo', 0)
                limpiaAtributo('cmbFolio', 0)
                limpiaAtributo('txtPCod', 0)
                limpiaAtributo('cmbVigente', 0)
                
                break;

        
            case 'crearArea':
                buscarParametros('listArea')
                alert('Area  registrada')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbServicio', 0)
                limpiaAtributo('cmbSede', 0)
                break;

            case 'crearSede':
                            buscarParametros('listSede')
                            alert('Sede  registrada')
                            limpiaAtributo('txtNombre', 0)
                            limpiaAtributo('txtNombre2', 0)
                            limpiaAtributo('txtDireccion', 0)
                            limpiaAtributo('txtTelefono', 0)
                            limpiaAtributo('txtTelefono2', 0)
                            limpiaAtributo('txtCelular', 0)
                            limpiaAtributo('cmbVigente', 0)
                            limpiaAtributo('txtCodHabilitacion', 0)
                            limpiaAtributo('cmbSeccional', 0)
                            limpiaAtributo('cmbServicio', 0)
                            break;

            case 'crearHabitacion':
                buscarParametros('listGrillaHabitacion')
                alert('Habitacion  Creada ')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbIdArea', 0)
                limpiaAtributo('cmbIdTipo', 0)
                limpiaAtributo('cmbCosto', 0)
                limpiaAtributo('cmbIdAdmision', 0)
                break;

            case 'modificarHabitacion':
                buscarParametros('listGrillaHabitacion')
                alert('Habitacion  Modificada ')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbIdArea', 0)
                limpiaAtributo('cmbIdTipo', 0)
                limpiaAtributo('cmbCosto', 0)
                limpiaAtributo('cmbIdAdmision', 0)
                break;

            case 'crearElemento':
                buscarParametros('listGrillaCama')
                alert('Elemento  Creado con exito ')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbIdEstado', 0)
                limpiaAtributo('cmbIdTipo', 0)
                limpiaAtributo('cmbHabitacion', 0)
                limpiaAtributo('cmbVigente', 0)

                break;

            case 'crearPerfil':
                buscarUsuario('listGrillaPerfiles');
                alert('Registro exitiso');
                limpiaAtributo('txtId', 0);
                limpiaAtributo('txtNombre', 0);
                limpiaAtributo('txtdefinicion', 0);
                limpiaAtributo('cmbVigente', 0);

                break;

            case 'modificarPerfil':
                buscarUsuario('listGrillaPerfiles');

                alert('Registro modificado con exito');
                limpiaAtributo('txtId', 1);
                limpiaAtributo('txtNombre', 0);
                limpiaAtributo('txtdefinicion', 0);
                limpiaAtributo('cmbVigente', 0);

                break;


            case 'modificarElemento':
                alert('Elemento  Modificado')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbIdEstado', 0)
                limpiaAtributo('cmbIdTipo', 0)
                limpiaAtributo('cmbHabitacion', 0)
                limpiaAtributo('cmbVigente', 0)
                setTimeout(() => {
                    buscarParametros('listGrillaCama')
                }, 100);
                break;


            case 'inhabilitarElemento':
                buscarParametros('listGrillaCama')
                alert(' Elemento  No Habilitado')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbIdEstado', 0)
                limpiaAtributo('cmbIdTipo', 0)
                limpiaAtributo('cmbHabitacion', 0)
                limpiaAtributo('cmbVigente', 0)
                break;


            case 'asignarSedeEspecialidad':
                buscarParametros('listSedeEspecialidad')
                alert('Especialidad  Asignada a Sede')
                limpiaAtributo('txtIdSede', 0)
                break;

            case 'ubicarHabitacionArea':
                buscarParametros('listSedeEspecialidad')
                alert('Se realizo la asignacion de manera exitosa')
                limpiaAtributo('txtidhabitacion', 0)
                break;



            case 'asignarPlanesaSedes':
                buscarParametros('listSedePlan')
                alert('Plan  Asignado a Sede')
                limpiaAtributo('txtIdSede', 0)
                break;
            case 'modificarTipoProcedim':
                buscarParametros('listGrillaTipoCita')
                alert('TIPO DE CITA MODIFICADO CON EXITO!!')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbCosto', 0)
                limpiaAtributo('txtRecomendaciones', 0)
                break;
            case 'eliminarTipoCita':
                buscarParametros('listGrillaTipoCita')
                alert('TIPO DE CITA ELIMINADO CON EXITO!!')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbCosto', 0)
                limpiaAtributo('txtRecomendaciones', 0)
            break;                

            case 'modificarEspecialidad':
                buscarParametros('listEspecialidad')
                alert('Especialidad modificada')
                limpiaAtributo('txtId', 1)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbServicio', 0)
                break;

            case 'modificarArea':
                buscarParametros('listArea')
                alert('Area modificada')
                limpiaAtributo('txtId', 1)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbServicio', 0)
                limpiaAtributo('cmbSede', 0)
                break;
            case 'modificarSede':
                buscarParametros('listSede')
                alert('Sede modificada')
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('txtNombre2', 0)
                limpiaAtributo('txtDireccion', 0)
                limpiaAtributo('txtTelefono', 0)
                limpiaAtributo('txtTelefono2', 0)
                limpiaAtributo('txtCelular', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('txtCodHabilitacion', 0)
                limpiaAtributo('cmbSeccional', 0)
                limpiaAtributo('cmbServicio', 0)
                break;

                case 'modificarProfesion':
                    buscarParametros('listGrillaProfesion')
                    alert('Profesion  Modificada')
                    limpiaAtributo('txtId', 0)
                    limpiaAtributo('txtNombre', 0)
                    limpiaAtributo('txtNombre2', 0)
                    limpiaAtributo('cmbIdEstado', 0)
                    limpiaAtributo('txtPrefijo', 0)
                    limpiaAtributo('cmbFolio', 0)
                    limpiaAtributo('txtCod', 0)
                    limpiaAtributo('cmbVigente', 0)
                break;

            case 'eliminarEspecialidadSede':
                alert('Se ha Eliminado correctamente')
                buscarParametros('listSedeEspecialidad');
                break;

            case 'eliminarhabitacionArea':
                alert('Se ha Eliminado correctamente')
                buscarParametros('listAreaHabitacion');
                break;
            case 'crearTipoCitaProcedimiento':
                buscarParametros('listGrillaTipoCitaProcedimientos')
                alert('Tipo cita procedimiento agregado')
                limpiaAtributo('txtIdProcedimiento', 0)

                break;
            case 'eliminarTipoCitaProcedimiento':
                buscarParametros('listGrillaTipoCitaProcedimientos')
                alert('Tipo cita procedimiento eliminado')
                limpiaAtributo('txtIdProcedimiento', 0)

                break;
            case 'modificarPiePagina':
                buscarParametros('listGrillaPiePagina');
                alert('Pie de pagina modificado')
                limpiaAtributo('lblId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbTipo', 0)

                break;

            case 'adicionarPiePagina':
                buscarParametros('listGrillaPiePagina');
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbTipo', 0)
                alert('Pie de pagina creado')

                break;

            case 'adicionarCentroCosto':
                buscarParametros('listGrillaCentroCostos');
                break;

            case 'modificarCentroCosto':
                alert('Se ha modificado correctamente')
                buscarParametros('listGrillaCentroCostos');
                break;

            case 'eliminarCentroCosto':
                alert('Se ha eliminado correctamente')
                buscarParametros('listGrillaCentroCostos');
                break;

            case 'adicionarProcedimientoE':
                alert('Se ha adicionado correctamente')
                buscarParametros('listGrillaExcepcion');
                break;
            case 'eliminarProcedimientoE':
                alert('Se ha Eliminado correctamente')
                buscarParametros('listGrillaExcepcion');
                break;
        }
    }
}













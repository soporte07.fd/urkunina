<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="CONFIGURACION HABITACION" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO --> 
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>
            <div style="overflow:auto;height:400px; width:100%" id="divContenido">
              <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
              <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr class="titulos" align="center">
                  <td width="30%">SEDE</td>
                  <td width="20%">AREA</td>
                  <td width="40%">NOMBRE HABITACION</td>
                  <td width="10%">VIGENTE</td>
                </tr>
                <tr  class="estiloImput">
                 <td width="30%" > 
                    <select  size="1" id="cmbSede1" style="width:90%" tabindex="2"
                      onfocus="comboDependienteEmpresa('cmbSede1','65')"
                      onchange="asignaAtributo('cmbIdArea1', '', 0);">
                      </select>
                  </td>
                  <td width="30%"><select size="1" id="cmbIdArea1" onfocus="cargarAreaDesdeSede('cmbSede1', 'cmbIdArea1')"style="width:90%" tabindex="2" >                                         
                    <option value=""></option>
                        </select>
                  </td>  
                  <td width="20%"><input type="text" id="txtBusNombre" onkeyup="javascript: this.value= this.value.toUpperCase();" style="width:90%" tabindex="2" />
                  </td> 
                  <td width="10%"><select size="1" id="cmbVigenteBus" style="width:80%" tabindex="2">
                      <option value="S">SI</option>
                      <option value="N">NO</option>
                    </select>
                  </td>
                  <td></td>
                  <td>
                    <input name="btn_BUSCAR" title="Seleccione habitaciones a buscar" type="button" class="small button blue" value="BUSCAR"
                      onclick="buscarParametros('listGrillaHabitacion')" />
                  </td>
                </tr>
              </table>
              <table id="listGrillaHabitacion" class="scroll" width="100%"></table>
            </div>


            <table width="100%" cellpadding="0" cellspacing="0" align="center">
              <tr class="titulosCentrados">
                <td colspan="3">OPCIONES DE  HABITACION
                </td>
              </tr>
              <tr>
                <td>
                  <table width="100%" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                      <td>
                        <input type="hidden" id="txtId" style="width:95%" />
                        <table width="100%">
                           
                         
                          <tr class="estiloImputIzq2">
                            <td width="30%">NOMBRE HABITACION</td>
                            <td width="70%"><input type="text" id="txtNombre" onkeyup="javascript: this.value= this.value.toUpperCase();" style="width:95%" /></td>
                          </tr>
                          <tr class="estiloImputIzq2">
                            <td width="30%">SEDE</td>
                            <td width="70%">
                              <select  size="1" id="cmbSedeCrear" style="width:95%" tabindex="2"
                                onfocus="comboDependienteEmpresa('cmbSedeCrear','65')"
                                onchange="$('#cmbIdArea').empty();">
                              </select>
                            </td>
                          </tr>
                          <tr class="estiloImputIzq2">
                            <td width="30%">AREA</td><td width="70%">
                              <select size="1" id="cmbIdArea" style="width:50%" tabindex="2" onfocus="cargarAreaDesdeSede('cmbSedeCrear', this.id)">                                                  
                              </select>
                            </td> 
                          </tr>  
                          <tr class="estiloImputIzq2">
                            <td width="30%">TIPO</td><td width="70%"><select size="1" id="cmbIdTipo" style="width:50%" tabindex="2" >                                         
                                <option value=""></option>
                                <% resultaux.clear();
                                resultaux=(ArrayList)beanAdmin.combo.cargar(969);   
                                ComboVO cmb8;
                               for(int k=0;k<resultaux.size();k++){
                               cmb8=(ComboVO)resultaux.get(k);
                               %>
                                              <option value="<%= cmb8.getId()%>" title="<%= cmb8.getTitle()%>">
                                                <%= cmb8.getDescripcion()%></option>
                                              <%}%>  
                               </select></td> 
                          </tr>  
                         
              <tr class="estiloImputIzq2">
              <td width="30%">CENTRO COSTO</td><td width="70%"><select size="1" id="cmbCosto" style="width:50%" tabindex="2" >                                         
                  <option value=""></option>
                  <% resultaux.clear();
                  resultaux=(ArrayList)beanAdmin.combo.cargar(964);   
                  ComboVO cmb3;
                 for(int k=0;k<resultaux.size();k++){
                 cmb3=(ComboVO)resultaux.get(k);
                 %>
                                <option value="<%= cmb3.getId()%>" title="<%= cmb3.getTitle()%>">
                                  <%= cmb3.getDescripcion()%></option>
                                <%}%>  
                 </select></td> 
            </tr>   
            <tr class="estiloImputIzq2">
              <td width="30%">VIGENTE</td>
              <td width="70%"><select size="1" id="cmbVigente" style="width:10%" tabindex="2">
                  <option value="S">SI</option>
                  <option value="N">NO</option>
                </select></td>
            </tr>
            <!--
             <tr class="estiloImputIzq2">
                <td width="20%">TIPO DE ADMISION</td>
                <td>
                 <select size="1" id="cmbIdAdmision" style="width:90%" tabindex="14" style="width:20%"  tabindex="14">
                     <option value=""></option>
                                <% resultaux.clear();
                                 resultaux=(ArrayList)beanAdmin.combo.cargar(535);   
                                 ComboVO cmb32;
                                 for(int k=0;k<resultaux.size();k++){
                                 cmb32=(ComboVO)resultaux.get(k);
                                %>
                   <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>">
                   <%= cmb32.getDescripcion()%></option>
                   <%}%>  
                 </select>
              </td>
            </tr>
            -->
        </table> 
      </td>   
    </tr>   
  </table>

  <table width="100%"  style="cursor:pointer" >
      <tr><td width="99%" class="titulos">
      <input name="btn_limpiar_new" type="button" title="Permite Limpiar informacion" class="small button blue" value="LIMPIAR" onclick="limpiaAtributo('txtId',0);limpiaAtributo('txtNombre',0);limpiaAtributo('cmbIdArea',0); limpiaAtributo('cmbIdTipo',0); limpiaAtributo('cmbVigente',0);limpiaAtributo('cmbIdAdmision',0);limpiaAtributo('cmbCosto',0); limpiaAtributo('cmbSedeCrear',0);" />                      
      <input name="btn_CREAR" title="Permite  crear habitaciones" type="button" class="small button blue" value="CREAR HABITACION" onclick="modificarCRUDParametros('crearHabitacion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  /> 
      <input name="btn_MODIFICAR" title="Permite modificar habitaciones" type="button" class="small button blue" value="MODIFICAR HABITACION" onclick="modificarCRUDParametros('modificarHabitacion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
      </td></tr>
  </table>  
  
  
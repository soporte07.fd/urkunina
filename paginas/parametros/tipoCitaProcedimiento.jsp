<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="TIPO DE CITA" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>
            <div style="overflow:auto;height:400px; width:100%" id="divContenido">
              <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
              <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr class="titulos" align="center">
                  <td width="30%">POR ESPECIALIDAD</td>                  
                  <td width="20%">VIGENTE</td>
                </tr>
                <tr class="estiloImput">
                  <td>
                    <select size="1" id="cmbIdEspecialidadBus" style="width:90%" tabindex="14" style="width:50%" tabindex="14">                      
                          <% resultaux.clear();
                              resultaux=(ArrayList)beanAdmin.combo.cargar(125);   
                              ComboVO cmb01;
                            for(int k=0;k<resultaux.size();k++){
                              cmb01=(ComboVO)resultaux.get(k);
                            %>
                      <option value="<%= cmb01.getId()%>" title="<%= cmb01.getTitle()%>"><%= cmb01.getDescripcion()%></option>
                          <%}%>  
                  </select>
                </td>                  
                <td><select size="1" id="cmbVigenteBus" style="width:30%" tabindex="2">
                      <option value="S">SI</option>
                      <option value="N">NO</option>
                    </select>
                </td>
                <td>
                    <input name="btn_BUSCAR" title="PTT1" type="button" class="small button blue" value="BUSCAR"  onclick="buscarParametros('listGrillaTipoCita')"/>
                </td>
                </tr>
              </table>
              <table id="listGrillaTipoCita" class="scroll" width="100%"></table>
            </div>


            <table width="100%" cellpadding="0" cellspacing="0" align="center">
              <tr class="titulosCentrados">
                <td colspan="3">MANTENIMIENTO DEL TIPO DE CITA PROCEDIMIENTO
                </td>
              </tr>
              <tr>
                <td>
                  <table width="100%" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                      <td>
                        <table width="100%">
                          <tr class="estiloImputIzq2">
                            <!-- <td width="30%">ID TIPO DE CITA:</td><td width="70%"> <label id="lblId"></label></td> -->
                            <td width="30%">ID TIPO DE CITA</td>
                            <td width="70%"> <input type="text" id="txtId" style="width:5%" maxlength="2" onkeyup="javascript: this.value= this.value.toUpperCase();" /></td>
                          </tr>
                          <tr class="estiloImputIzq2">
                            <td width="30%">NOMBRE TIPO DE CITA</td>
                            <td width="70%"><input type="text" id="txtNombre" onkeyup="javascript: this.value= this.value.toUpperCase();" style="width:95%" /></td>
                          </tr>
                          <tr class="estiloImputIzq2">
                            <td width="30%">RECOMENDACIONES</td>
                            <td width="70%">
                              <textarea id="txtRecomendaciones" tabindex="101" style="width:95%" maxlength="4000"
                                size="4000" rows="5" type="text"> </textarea>
                            </td>
                          </tr>
                          <tr class="estiloImputIzq2">
                            <td width="20%">ESPECIALIDAD</td>
                            <td>
                              <select size="1" id="cmbIdEspecialidad" style="width:40%" tabindex="14"  tabindex="14">                                  
                                      <% resultaux.clear();
                                          resultaux=(ArrayList)beanAdmin.combo.cargar(125);   
                                          ComboVO cmb32;
                                        for(int k=0;k<resultaux.size();k++){
                                        cmb32=(ComboVO)resultaux.get(k);
                                        %>
                                  <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>"><%= cmb32.getDescripcion()%></option>
                                      <%}%>  
                              </select>
                            </td>
                          </tr>
                          <tr class="estiloImputIzq2">
                            <td width="30%">TIENE COSTO</td><td width="70%"><select size="1" id="cmbCosto" style="width:7%" tabindex="2" >                                                                         
                                <option value="S">SI</option>
                                <option value="N">NO</option>                  
                              </select></td> 
                          </tr> 
                          <tr class="estiloImputIzq2">
                            <td width="30%">VIGENTE</td>
                            <td width="70%"><select size="1" id="cmbVigente" style="width:7%" tabindex="2">                                
                                <option value="S">SI</option>
                                <option value="N">NO</option>
                              </select></td>
                          </tr>                                                                                                  		           
          </table> 
        </td>   
          </tr>   
      </table>

    <table width="100%"  style="cursor:pointer" >
        <tr><td width="99%" class="titulos">
        <input name="btn_limpiar_new" type="button" title="F76" class="small button blue" value="LIMPIAR" onclick="limpiaAtributo('txtId',0);limpiaAtributo('txtNombre',0); ;limpiaAtributo('txtRecomendaciones',0); limpiaAtributo('cmbVigente',0);limpiaAtributo('cmbIdEspecialidad',0);limpiaAtributo('cmbCosto',0);limpiarListadosTotales('listGrillaTipoCitaProcedimientos')" />                      
        <input name="btn_CREAR" title="F77" type="button" class="small button blue" value="CREAR" onclick="modificarCRUDParametros('crearTipoCita','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  /> 
        <input name="btn_MODIFICAR" title="F79" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUDParametros('modificarTipoProcedim','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
        <input name="btn_MODIFICAR" title="F79" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUDParametros('eliminarTipoCita','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
        </td></tr>
    </table>  
  
      <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
        <tr>
          <td>
            <table width="100%">        
              <tr class="titulosCentrados">
                <td colspan="3">ADICIONAR PROCEDIMIENTOS AL TIPO DE CITA
                </td>   
              </tr>            
              <tr class="estiloImputIzq2">
                <td width="30%">PROCEDIMIENTO ASOCIADO</td><td width="70%"><input type="text" id="txtIdProcedimiento" onkeyup="javascript: this.value= this.value.toUpperCase();"  onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimiento',300)" style="width:90%"/></td> 
              </tr>	
              <tr>
                <td colspan="2" align="center">
                  <input id="btn_limpia" title="E55" class="small button blue" type="button" value="LIMPIAR"  onclick="limpiarDivEditarJuan('limpiarProcedimientoPlan')">              
                  <input id="btn_crea" title="E56" class="small button blue" type="button"  value="ADICIONAR PROCEDIMIENTO"  onclick="modificarCRUDParametros('crearTipoCitaProcedimiento')">                               
                  <input name="btn_elimina" title="E57" type="button" class="small button blue" value="ELIMINA" onclick="modificarCRUDParametros('eliminarTipoCitaProcedimiento')">                               
                </td>
              </tr>   
            </table> 
            </td>   
        </tr>   
        <tr>
          <td>
              <table id="listGrillaTipoCitaProcedimientos" class="scroll"></table>  
          </td>
        </tr>
      </table>    

   </td>
 </tr>  
</table>
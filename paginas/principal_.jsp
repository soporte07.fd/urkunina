<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "Sgh.Utilidades.Sesion" %>


<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->

<table width="90%" align="left" border="0" cellspacing="0" cellpadding="0" style="margin-top:1px; z-index:1500">
    <tr>
        <td valign="top"> <input name="ventanasActivas" id="ventanasActivas" type="hidden" class="nomayusculas"
                tabindex="1" value="" size="25" />
            <label id="lblContrasena" style="display: none;"><%=beanSession.usuario.getContrasena()%></label>
            <label id="lblIdentificacion" style="display: none;"><%=beanSession.usuario.getIdentificacion()%></label>
            <label id="lblIdSede" style="display: none;"><%=beanSession.usuario.getIdSede()%></label>
            <jsp:include page="menu.jsp" flush="true" />
            <table id="tablaMain" width="100%" height="100%" align="left" cellpadding="0" cellspacing="0"
                bgcolor="#CCCCCC">
                <tr>
                    <td align="center" valign="top">                        
                        <div id="menuAux"
                            style="width:132px; position:absolute; left:9px; z-index:1;  top: 40px; height: 12px; display:none;">
                                <div style="background:#FFFFFF">
                                    <div
                                        style="background:#FC3;  height:100%; font-family:Arial, Helvetica, sans-serif; font-size:10px; "
                                        align="center">
                                        <label id="lblMenuTreeTitle" onclick="javascript:mostrarMenu();" style="color:#193b56;">MEN&Uacute;</label>
                                    </div>
                                </div>
                                <br />                            
                        </div>
                        <div id="menuDesplegable"
                            style="width:132px; position:absolute; left:9px; z-index:1;  top: 40px; height: 1080px;">
                                <div style="background:#FFFFFF">
                                    <div id="divMenuTreeTitle" onclick="javascript:ocultarMenu();"
                                        style="background:#FC3;  height:100%; font-family:Arial, Helvetica, sans-serif; font-size:10px; "
                                        align="center">
                                        <label id="lblMenuTreeTitle" style="color:#193b56;">MEN&Uacute;</label>
                                    </div>
                                </div>
                                <br />
                            <DIV id="menumenu" style="overflow:auto; height:1777px; top: 40px">
                              <ul id="browser" class="filetree treeview-black" style="font-size:9px"><strong></strong>
                                <% if(beanSession.usuario.preguntarMenu("HIS")){%>
                                                                  
                                        <li class="closed"><span class="folderHistoriaClinica" title="HIS">HISTORIA CLINICAS</span>
                                            <ul>
                                                <% if(beanSession.usuario.preguntarMenu("HIS01")){%>
                                                <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/principalHC.jsp")%>', '4-0-0', 'principalHC', 'principalHC', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="HIS01">Folios Historia Clinica</a></span>
                                                </li>
                                                <%}%>

                                            <% if(beanSession.usuario.preguntarMenu("HIS02")|| beanSession.usuario.preguntarMenu("HISTORIACLINICA") ){%>
                                                <li class="closed"><span class="folder" title="HIS02">PARAMETROS</span>
                                                    <ul>
                                                        <% if(beanSession.usuario.preguntarMenu("HIS03")){%>
                                                            <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/folios.jsp")%>', '4-0-0', 'folios', 'folios', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="HIS03">Tipos de folio</a></span>
                                                            </li>
                                                        <%}%>
                                                        <% if(beanSession.usuario.preguntarMenu("HIS04")){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/diagnostico.jsp")%>', '4-0-0', 'diagnostico', 'diagnostico', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="HIS04">Diagnostico</a></span>
                                                            </li>
                                                        <%}%>
                                                        <% if(beanSession.usuario.preguntarMenu("HIS05")){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/procedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="HIS05">Procedimiento</a></span>
                                                            </li>
                                                        <%}%>
                                                        <% if(beanSession.usuario.preguntarMenu("HIS06")){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaProcedimientosNoPos.jsp")%>', '4-0-0', 'plantillaProcedimientosNoPos', 'plantillaProcedimientosNoPos', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="HIS06">Plantilla Procedimientos
                                                                        NoPos</a></span>
                                                            </li>
                                                        <%}%>
                                                        <% if(beanSession.usuario.preguntarMenu("HIS07")){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaMedicamentosNoPos.jsp")%>', '4-0-0', 'plantillaMedicamentosNoPos', 'plantillaMedicamentosNoPos', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="HIS07">Plantilla Medicamentos
                                                                        NoPos</a></span>
                                                            </li>

                                                            <%}%>
                                                            <% if(beanSession.usuario.preguntarMenu("HIS08")){%>
                                                            <li><span class="file"><a
                                                                onclick="mostrarGrafica()"
                                                                style="cursor:pointer" title="HIS08" >Ejemplo grafica</a></span>
                                                            </li>
                                                            <%}%>
                                                    </ul>
                                                </li>
                                            <%}%>

                                            <% if(beanSession.usuario.preguntarMenu("HIS09") ){%>
                                                <li class="closed"><span class="folder" title="HIS09">REFERENCIA</span>
                                                    <ul>
                                                        <% if(beanSession.usuario.preguntarMenu("HIS10") ){%>
                                                        <li><span class="file">
                                                            <a onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/referencia.jsp")%>',
                                                            '4-0-0', 'referencia', 'referencia', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="HIS10">Referencia Contra Referencia</a>
                                                            </span>
                                                        </li>
                                                        <%}%>
                                                    </ul>
                                                </li>
                                            <%}%>
                                        </ul>
                                    </li>
                             <%}%>
                                        
                              
                                            
                                
                                <% if(beanSession.usuario.preguntarMenu("APD")|| beanSession.usuario.preguntarMenu("HOS")){%>
                                <li class="closed"><span class="folderApoyoDx" title="APD">APOYO DIAGN&Oacute;STICO</span>
                                    <ul>
                                        <% if(beanSession.usuario.preguntarMenu("APD01")){%>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/apoyoDiagnostico.jsp")%>', '4-0-0', 'apoyoDx', 'apoyoDx', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="APD01">Laboratorios y ayudas diagn&oacute;sticas</a></span>
                                        </li>
                                        <%}%>
                                        <% if(beanSession.usuario.preguntarMenu("APD02")){%>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/pruebaLaboratorio.jsp")%>', '4-0-0', 'pruebaLaboratorio', 'pruebaLaboratorio', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="APD02">Pruebas Laboratorio</a></span>
                                        </li>
                                        <%}%>
                                        <% if(beanSession.usuario.preguntarMenu("APD03")){%>
                                           <li><span class="file"><a
                                              onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaLaboratorio.jsp")%>',
                                            '4-0-0', 'plantillaLaboratorio', 'plantillaLaboratorio', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer" title="APD03" >Plantillas Laboratorio</a></span>
                                            </li>
                                        <%}%>
                                    </ul>
                                </li>
                                <%}%>    

                                <% if(beanSession.usuario.preguntarMenu("POS") ){%>
                                    <li class="closed"><span class="folderPostConsulta" title="POS">POST CONSULTA</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("POS01") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/postConsulta/documentos.jsp")%>', '4-0-0', 'documentos', 'documentos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="POSF">Folios</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("POS02") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaProcedimientosNoPos.jsp")%>', '4-0-0', 'plantillaProcedimientosNoPos', 'plantillaProcedimientosNoPos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="POS02" >Plantilla Procedimientos NoPos</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("POS03") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaProcedimientosNoPos.jsp")%>', '4-0-0', 'plantillaProcedimientosNoPos', 'plantillaProcedimientosNoPos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="POS03" >Plantilla Medicamentos NoPos</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("POS04") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionAyudaDiagnostica.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"  title="POS04" >Gestión Ayudas
                                                            Diagnosticas</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("POS05") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionSolicitudes.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="POS05">Gestión Solicitudes
                                                            Intercambio</a></span>
                                                </li>
                                            <%}%>
                                        </ul>
                                    </li>
                                <% }%>

                                <% if(beanSession.usuario.preguntarMenu("HOS") ){%>
                                    <li class="closed"><span
                                            class="folderHospitalizacion" title="HOS">HOSPITALIZACION</span>
                                    <ul>
                                        
                                        <% if(beanSession.usuario.preguntarMenu("HOS01")|| beanSession.usuario.preguntarMenu("HOS") ){%>
                                            <li><span class="file">
                                                <a 
                                                onclick="javascript:cargarMenu('<%= response.encodeURL("hospitalizacion/ubicacionCama.jsp")%>', '4-0-0', 'ubicacionCama', 'ubicacionCama', 's', 's', 's', 's', 's', 's');"
                                                style="cursor:pointer" title="HOS01">Ubicacion Cama
                                                paciente</a></span>
                                            </li>
                                        <% }%>
                                    </ul>
                                </li>
                                <%}%>
                                
                                <% if(beanSession.usuario.preguntarMenu("RUT")){%>
                                    <li class="closed"><span class="folderRutas" title="RUT">RUTAS</span>                                                                                        
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("RUT01") ){%> 
                                                <li class="closed"><span class="folder" title="RUT01">PARAMETROS</span>
                                                <ul>
                                                    <% if(beanSession.usuario.preguntarMenu("RUT02") ){%> 
                                                        <li><span class="file">
                                                            <a onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/planAtencionRuta.jsp")%>', '4-0-0', 'planAtencionRuta', 'planAtencionRuta', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="RUT02">Plan Atencion Rutas</a></span>
                                                        </li>
                                                    <%}%>
                                                    <% if(beanSession.usuario.preguntarMenu("RUT03") ){%> 
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/planParametros.jsp")%>', '4-0-0', 'tipoCitaProcedimiento', 'Gestionar Facturacion', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="RUT03">Ruta de atención</a></span>
                                                        </li>
                                                    <%}%>
                                                </ul> 
                                            <%}%>                                            
                                            <% if(beanSession.usuario.preguntarMenu("RUT04")){%>    
                                                <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/paciente.jsp")%>', '4-0-0', 'paciente', 'paciente', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer"
                                                                title="RUT04">Paciente</a></span>
                                                    </li>
                                            <%}%>

                                                <!--li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/gestionServicio.jsp")%>', '4-0-0', 'gestionServicio', 'gestionServicio', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="Crear contactos Paciente">Gestión del
                                                            Servicio</a></span></li-->
                                                <% if(beanSession.usuario.preguntarMenu("RUT05") ){%>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/cerfificarContactos.jsp")%>', '4-0-0', 'cerfificarContactos', 'cerfificarContactos', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer"
                                                                title="RUT05">Certificar  Contactos</a></span>
                                                    </li>
                                                <%}%>
                                                <% if(beanSession.usuario.preguntarMenu("RUT06") ){%>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/enrutarPaciente.jsp")%>', '4-0-0', 'enrutarPaciente', 'enrutarPaciente', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="RUT06">Enrutar al Paciente</a></span>
                                                    </li>
                                                <%}%>
                                                <% if(beanSession.usuario.preguntarMenu("RUT07") ){%>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/listaEsperaAgendamiento.jsp")%>', '4-0-0', 'listaEsperaAgendamiento', 'listaEsperaAgendamiento', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer"  title="RUT07">Agendas desde lista de espera</a></span>
                                                    </li>
                                                <%}%>
                                     
                                    
                                        <!--li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/gestionPaciente.jsp")%>', '4-0-0', 'rutaPaciente', 'rutaPaciente', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Citas pendientes</a></span>
                                        </li>
                                        <li><span class="file"><a
                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/paciente.jsp")%>', '4-0-0', 'paciente', 'paciente', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer"
                                            title="Usted puede administrar paciente">Paciente</a></span></li-->                                                    
                                            
                                        </ul>
                                        </li>
                                <%}%>  

                                <!--li class="closed"><span class="folder">SEGURIDAD PACIENTE</span>
                                <ul>
                                <li class="closed"><span class="folder">Gestión del riesgo</span>
                                <% if(beanSession.usuario.preguntarMenu("311") ){%> 
                                <ul>			
                                <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/eventoAdverso/reportarEventoAdverso.jsp")%>', '4-0-0', 'reportarEventoAdverso', 'reportarEventoAdverso', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede reportar un evento adverso, como primer actor">Reportar</a></span>
                                </li>
                                </ul>                            
                                <%}%>        
                                <% if(beanSession.usuario.preguntarMenu("312") ){%> 
                                <ul>
                                <li><span  class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/eventoAdverso/administrarEventoAdverso.jsp")%>', '4-0-0', 'administrarEventoAdverso', 'administrarEventoAdverso', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede gestionar los eventos adversos que se reportaron con anterioridad, como segundo actor o como tercer actor">Gestionar</a></span>
                                </li>  
                                </ul>                                                        
                                <%}%>                  
                                </li>   
                                <li class="closed"><span class="folder">Documentos</span>
                                <ul>
                                <li><span class="file"><a onClick="abrirTutorial('/clinica/pdf/tutoriales/tallerPoliticaDeSeguridad2012.pdf')" style="cursor:pointer" title="Usted puede leer el taller de politica de seguridad del paciente 2012">Taller política</a></span>
                                </li>  
                                </ul>                                                        
                                </li>   
                                </ul>           
                                </li-->
                                <% if(beanSession.usuario.preguntarMenu("5") ){%>
                                <!--li class="closed"><span class="folder">ENCUESTAS</span>
                                <ul>
                                <% if(beanSession.usuario.preguntarMenu("511") ){%>                          
                                <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("encuestas/responder.jsp")%>', '4-0-0', 'responder', 'responder', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede responder">Responder</a></span>
                                </li>
                                <% }%>   
                                <% if(beanSession.usuario.preguntarMenu("512") ){%>                          
                                <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("encuestas/gestionar.jsp")%>', '4-0-0', 'gestionar', 'gestionar', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede administrar Proveedores">Gestionar</a></span>
                                </li>
                                <% }%>  
                                </ul>  
                                </li-->
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("CITP") ){%>
                                    <li class="closed"><span class="folderCirugia" title="CITP">CITAS PROCEDIMIENTOS</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("CITP01") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/horarios.jsp")%>', '4-0-0', 'horarios', 'horarios', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="CITP01">Horarios</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("CITP02") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="CITP02">Agenda</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("CITP03") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/listaEspera.jsp")%>', '4-0-0', 'listaEsperaCirugia', 'listaEsperaCirugia', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="CITP03">Lista de
                                                            Espera</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("CITP04") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionProcedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="CITP04">Gestión Procedimientos</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("CITP05") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/principalHC.jsp")%>', '4-0-0', 'principalOrdenes', 'principalOrdenes', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="CITP05">Evolucion
                                                            Cirugia</a></span>
                                                </li>
                                             <%}%>          
                                        </ul>
                                    </li>
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("CITAS") ){%>
                                    <li class="closed"><span class="folderAtUsuario" title="CITAS">ATENCION AL USUARIO</span>
                                        <ul>
                                          
                                        <% if(beanSession.usuario.preguntarMenu("CITASH") ){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/horarios.jsp")%>', '4-0-0', 'horarios', 'horarios', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="CITASH">Horarios</a></span>
                                            </li>
                                        <% }%>
                                        <% if(beanSession.usuario.preguntarMenu("CITASA") ){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="CITASA">Agenda</a></span>
                                            </li>
                                        <% }%> <!--
                                        <% if(beanSession.usuario.preguntarMenu("CITASCH") ){%> 
                                                <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/misCitasHoy.jsp")%>', '4-0-0', 'misCitasHoy', 'misCitasHoy', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="CITASCH">Mis Citas de Hoy</a></span>
                                                </li>
                                         <% }%> -->
                                        <% if(beanSession.usuario.preguntarMenu("CITASLE") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="CITASLE">Lista de
                                                            Espera</a></span>
                                                </li>
                                        <% }%>
                                        <% if(beanSession.usuario.preguntarMenu("CRMCCP")){%>                                            
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/contactosPaciente.jsp")%>', '4-0-0', 'contactosPaciente', 'contactosPaciente', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="CRMCCP">Centro de Contacto</a></span>
                                            </li>
                                        <%}%>                                          
                                            
                                        </ul>
                                    </li>
                                <%}%>
                                <% if(   beanSession.usuario.preguntarMenu("HOMECARE")   ){%>
                                    
                                <li class="closed"><span class="folderAtUsuario" title="HOMECARE">HOME CARE</span>
                                    <ul>
                                        <% if(beanSession.usuario.preguntarMenu("HOMECAREF") ){%>
                                        <li><span class="file"><a
                                            onclick="javascript:cargarMenu('<%= response.encodeURL("homecare/coordinacion.jsp")%>', '4-0-0', 'coordinacionHomecare', 'coordinacionHomecare', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer"
                                            title="HOMECAREF">Programacion terapias</a></span>
                                        </li>   
                                        <%}%>  
                                        <% if(beanSession.usuario.preguntarMenu("HOMECAREA") ){%>                                   
                                        <li ><span class="file"><a
                                            onclick="javascript:cargarMenu('<%= response.encodeURL("homecare/agenda.jsp")%>', '6-1-3', 'agendaHomecare', 'agendaHomecare', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer" title="HOMECAREA">Agenda terapeuta</a></span>
                                        </li>
                                        <%}%>
                                    </ul>
                                </li>
                                <%}%>
                                <% if(beanSession.usuario.preguntarMenu("ORI") ){%>
                                    <li class="closed"><span class="folderOrientacion" title="ORI">ORIENTACION</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("ORIAP") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/asistenciaPacientes.jsp")%>', '4-0-0', 'asistenciaPacientes', 'asistenciaPacientes', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="ORIAP">Asistencia de Pacientes</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("ORIPAR") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/parlante.jsp")%>', '4-0-0', 'parlante', 'parlante', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="ORIPAR">Parlante</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("ORIVIS") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/asistenciaDePacientes.jsp")%>', '4-0-0', 'asitenciaMedia', 'asitenciaMedia', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="ORIVIS">Visitantes</a></span>
                                                </li>
                                            <%}%>
                                        </ul>
                                    </li>
                                    <%}%>
                            
                            
                                <% if(beanSession.usuario.preguntarMenu("FAC") ){%>
                                    <li class="closed"><span class="folderFacturacion" title="FAC">CONTRATACION</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("FACA") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/admisiones.jsp")%>', '4-0-0', 'admisiones', 'admisiones', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="FACA">Admisiones</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("FACCF") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/soloFactura.jsp")%>', '4-0-0', 'soloFactura', 'soloFactura', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="FACCF">Crear Solo Factura</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("CITASLE") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="FACLE">Lista de
                                                            Espera</a></span>
                                                </li>
                                             <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("FACRIPS") ){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/rips.jsp")%>', '4-0-0', 'rips', 'rips', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="FACRIPS">Generar
                                                        RIPS</a></span>
                                            </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("FACP") ){%>
                                            <li class="closed"><span class="folder" title="FACP" >PARAMETROS</span>
                                                <ul>
                                                    <% if(beanSession.usuario.preguntarMenu("FACPP") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/procedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="FACPP">Procedimiento</a></span>
                                                        </li>
                                                    <%}%>
                                                    <% if(beanSession.usuario.preguntarMenu("FACPA") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/articulo.jsp")%>', '4-0-0', 'articulosPlan', 'articulosPlan', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer"
                                                                    title="FACPA">Articulos</a></span>
                                                        </li>
                                                    <%}%>
                                                    <% if(beanSession.usuario.preguntarMenu("FACPME") ){%>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/copago_maximo.jsp")%>', '4-0-0', 'articulosPlan', 'articulosPlan', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer"
                                                                title="FACPME">M&aacuteximo
                                                                evento</a></span>
                                                    </li>
                                                    <%}%>
                                                </ul>
                                            </li>
                                             <% if(beanSession.usuario.preguntarMenu("FACCON") ){%>
                                                    <li class="closed"><span class="folder" title="FACCON">CONTRATACION</span>
                                                        <ul>
                                                            <% if(beanSession.usuario.preguntarMenu("FACCONTAR") ){%>
                                                                <li><span class="file"><a
                                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/tarifarios.jsp")%>', '4-0-0', 'admision', 'admision', 's', 's', 's', 's', 's', 's');"
                                                                            style="cursor:pointer" title="FACCONTAR">Tarifarios</a></span>
                                                                </li>
                                                            <%}%>
                                                            <% if(beanSession.usuario.preguntarMenu("FACCONPCONT") ){%>
                                                                <li><span class="file"><a
                                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/planes.jsp")%>', '4-0-0', 'planes', 'planes', 's', 's', 's', 's', 's', 's');"
                                                                            style="cursor:pointer" title="FACCONPCONT">Planes de Contratación</a></span>
                                                                </li>
                                                            <%}%>   
                                                        </ul>
                                                    </li>
                                                <%}%>
                    						<% if(beanSession.usuario.preguntarMenu("FACTIP") ){%>
                                                    <li class="closed"><span class="folder" title="FACTIP" >TIPIFICACION</span>
                                                        <ul>
                                                            <% if(beanSession.usuario.preguntarMenu("FACTIPDOC") ){%>
                                                                <li><span class="file"><a
                                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/documentos_tipificacion.jsp")%>', '4-0-0', 'documentos', 'documentos', 's', 's', 's', 's', 's', 's');"
                                                                            style="cursor:pointer" title="FACTIPDOC">>Tipificación Documentos</a></span>
                                                                </li>
                                                           <%}%>
                                                            <% if(beanSession.usuario.preguntarMenu("FACTIPGFAC") ){%>
                                                                <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/tipificacion_facturas.jsp")%>', '4-0-0', 'tipificacion_facturas', 'tipificacion_facturas', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="FACTIPGFAC">Tipificacion Facturas</a></span>
                                                                </li>
                                                            <%}%>
                                                        </ul>
                                                    </li>
                                                 <%}%>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("FACF") ){%>
                                                <li class="closed"><span class="folder" title="FACF">FACTURA</span>
                                                    <ul>
                                                        <% if(beanSession.usuario.preguntarMenu("FACFFACT") ){%>
                                                            <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturas.jsp")%>',
                                                                    '4-0-0', 'Facturas', 'Facturas', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="FACFFACT">Facturas</a></span>
                                                            </li>
                                                         <%}%>   
                                                        <!--li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturasAnuladas.jsp")%>', '4-0-0', 'Facturas', 'Facturas', 's', 's', 's', 's', 's', 's');" style="cursor:pointer">Facturas Anuladas</a></span>
                                                        </li-->
                                                        <% if(beanSession.usuario.preguntarMenu("FACFREC") ){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/recibos.jsp")%>', '4-0-0', 'recibos', 'recibos', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="FACFREC" >Recibos</a></span>
                                                            </li>
                                                        <%}%>
                                                        <% if(beanSession.usuario.preguntarMenu("FACFRIPS") ){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/admision.jsp")%>', '4-0-0', 'admision', 'admision', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="FACFRIPS">Rips</a></span>
                                                            </li>
                                                        <%}%>
                                                    </ul>
                                            </ul>
                                            </li>
                                            <%}%>
                                <%}%>
                                
                                
                 <% if(beanSession.usuario.preguntarMenu("VENTAS") ){%>

                                    <li class="closed"><span class="folderVentas" title="VENTAS">VENTAS</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("VENTASFAC") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/salidasVentas.jsp")%>', '4-0-0', 'salidasVentas', 'salidasVentas', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="VENTASFAC">Facturas</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("VENTASTR") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturasTrabajo.jsp")%>', '4-0-0', 'facturasTrabajo', 'facturasTrabajo', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="VENTASTR">Trabajos</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("VENTASCART") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/cartera.jsp")%>', '4-0-0', 'cartera', 'cartera', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="VENTASCART">Cartera</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("VENTASPARAM") ){%>
                                                <li class="closed"><span class="folder"  title="VENTASPARAM">PARAMETROS</span>
                                                    <ul>
                                                        <% if(beanSession.usuario.preguntarMenu("VENTASPARAMFUN") ){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/parametrosTrabajos.jsp")%>', '4-0-0', 'Funcionarios', 'Funcionarios', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="VENTASPARAMFUN">Funcionarios</a></span>
                                                            </li>
                                                          <%}%>
                                                          <% if(beanSession.usuario.preguntarMenu("VENTASPARAMTERC") ){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/terceros/principalTerceros.jsp")%>', '4-0-0', 'Terceros', 'Terceros', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="VENTASPARAMTERC">Terceros</a></span>
                                                            </li>
                                                          <%}%>
                                                    </ul>
                                                </li>
                                             <%}%>

                                        </ul>
                                    </li>
                
                
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("ARCH")){%>
                                    <li class="closed"><span class="folderArchivo" title="ARCH">ARCHIVO</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("CITASLE")){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="CITASLE ">Lista de
                                                        Espera</a></span>
                                            </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("ARCHAGEN")){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("archivo/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="ARCHAGEN ">Agenda Archivo</a></span>
                                            </li>
                                            
                                            <%}%>
                                        </ul>
                                    </li>
                                <%}%>
                                <% if(beanSession.usuario.preguntarMenu("INV") ){%>
                                <li class="closed"><span class="folderFarmacia" title="INV">INVENTARIOS</span>
                                        
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("INVPED") ){%>
                                                <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/pedidos.jsp")%>', '4-0-0', 'Pedidos', 'Pedidos', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="INV">Pedidos</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("INVRESP") ){%>   
                                                <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/solicitudes.jsp")%>', '4-0-0', 'solicitudes', 'solicitudes', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="INVRESP">Respuesta a pedidos</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INVENTBOD") ){%>   
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/entradasDeBodega.jsp")%>', '4-0-0', 'entradasDeBodega', 'entradasDeBodega', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="INVENTBOD">Entradas a
                                                            Bodega </a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("INVSALBOD") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/salidasDeBodega.jsp")%>', '4-0-0', 'salidasDeBodega', 'salidasDeBodega', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="INVSALBOD">Salidas de
                                                            Bodega </a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INVTRASBOD") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/trasladoBodegas.jsp")%>', '4-0-0', 'trasladoBodegas', 'trasladoBodegas', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INVTRASBOD">Traslado entre Bodegas</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INVDESPROC") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/despachoProgramacionProcedimientos.jsp")%>', '4-0-0', 'despachoProgramacionProcedimientos', 'despachoProgramacionProcedimientos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INVDESPROC">Despacho a Procedimientos</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("INVDEV") ){%>
                                                 <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/devoluciones.jsp")%>', '4-0-0', 'devoluciones', 'devoluciones', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INVDEV">Devoluciones</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INVDEVCOM") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/devolucionCompras.jsp")%>', '4-0-0', 'devolucionCompras', 'devolucionCompras', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="">Devoluciones en Compras</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INVDOCHIST") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/documentos/principalDocumentos.jsp")%>', '4-0-0', 'principalDocumentos', 'principalDocumentos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INVDOCHIST">Documentos Historicos</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INVKARD") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/kardex/principalKardex.jsp")%>', '4-0-0', 'principalKardex', 'principalKardex', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INVKARD">Kardex</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INVCODBAR") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/codigoBarras.jsp")%>', '4-0-0', 'codigoBarras', 'codigoBarras', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="">Codigo de Barras</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("INVENT") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/inventario.jsp")%>', '4-0-0', 'inventario', 'inventario', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="INVENT">Hacer
                                                            Inventario</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("INVRECEP") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/recepcionTecnica.jsp")%>', '4-0-0', 'recepcionTecnica', 'recepcionTecnica', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INVRECEP">Recepcion Tecnica</a></span>
                                           
                                             <% }%>
                                             <% if(beanSession.usuario.preguntarMenu("INVPARAM") ){%>
                                                <li class="closed"><span class="folder">PARAMETROS</span>
                                                    <ul>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/principalArticulo.jsp")%>', '4-0-0', 'principalArticulo', 'principalArticulo', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer"
                                                                    title="943-Usted puede ver los articulos">Articulo</a></span>
                                                        </li>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/canastas/plantillaCanastas.jsp")%>', '4-0-0', 'plantillaCanastas', 'plantillaCanastas', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer"
                                                                    title="943-Usted puede ver las canastas">Crear
                                                                    Canastas</a></span>
                                                        </li>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/bodega/crearBodega.jsp")%>', '4-0-0', 'CrearBodega', 'CrearBodega', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer"
                                                                    title="Usted puede crear y modificar bodegas">Crear
                                                                    Bodegas</a></span>
                                                        </li>
                                                    </ul>
                                                </li>
                                            <% }%> 

                                           
                                        </ul>
                                </li>
                               
                                <% }%>
                                
                                <% if(beanSession.usuario.preguntarMenu("CRM") ){%>
                                <li class="closed"><span class="folderGeo">GEO-REFERENCIA</span>
                                    <ul>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("encuesta/geoAreas.jsp")%>', '4-0-0', 'geoAreas', 'geoAreas', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer">Gestión de Areas</a></span></li>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/gestionServicio.jsp")%>', '4-0-0', 'gestionServicio', 'gestionServicio', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer">Asignar Medico Area</a></span>
                                        </li>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/planParametros.jsp")%>', '4-0-0', 'tipoCitaProcedimiento', 'Gestionar Facturacion', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer">Gestion Encuestas</a></span></li>
                                    </ul>
                                </li>
                                <%}%>  

                                <% if(beanSession.usuario.preguntarMenu("INFO") ){%>
                                    <li class="closed"><span class="folderNotificaciones">NOTIFICACIONES</span>
                                        <ul>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/crearNotificacion.jsp")%>', '4-0-0', 'crearNotificacion', 'crearNotificacion', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Crear</a></span>
                                            </li>

                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/leerNotificacion.jsp")%>', '4-0-0', 'leerNotificacion', 'leerNotificacion', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Leer</a></span>
                                            </li>

                                        </ul>
                                    </li>
                                <%}%>
                                    
                                <!--li class="closed"><span class="folder">MI AGENDA</span>
                                <ul>
                                <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/notificacion.jsp")%>',
                                '8-1-3', 'notificacion', 'notificacion', 's', 's', 's', 's', 's', 's');"
                                style="cursor:pointer" >noti</a></span>
                                </li>
                                </ul>
                                </li-->
                                <% if(beanSession.usuario.preguntarMenu("INFO") ){%>
                                    <li class="closed"><span class="folderInformes">INFORMES</span>
                                        <ul>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("reportes/reportes_parametros.jsp")%>', '4-0-0', 'reportes', 'reportes', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede consultar Reportes de todas las areas">Listado de
                                                        Informes</a></span>
                                            </li>
                                            <!-- VERIFICAR notificacion.jsp
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/notificacion.jsp")%>', '4-0-0', 'reportes', 'reportes', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede consultar Reportes de todas las areas">notificar</a></span>
                                            </li>
                                            -->

                                        </ul>
                                    </li>
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("HELP") ){%>
                                    <li class="closed"><span class="folderCalidad" title="HELP" >HELP DESK</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("HELPRE") ){%>
                                            <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/evento/crearEvento.jsp")%>',
                                                    '2-1-3', 'crearEvento', 'crearEvento', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="HELPRE">Reportar
                                                    Evento</a></span>
                                            </li>
                                            <% }%>
                                            <% if(beanSession.usuario.preguntarMenu("HELPGES_EVEN") ){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/evento/gestionarEventos.jsp")%>', '4-0-0', 'gestionarEvento', 'gestionarEvento', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="HELPGES_EVEN">Gestion de
                                                        Evento</a></span></li>
                                            <% }%>
                                        <!--      <li class="closed"><span class="folder">No conformidades</span>
                                        <% //if(beanSession.usuario.preguntarMenu("211") ){%> 
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/reportarNoConformidad.jsp")%>','2-1-1','reportarNoConformidad','reportarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede reportar una No Conformidad con destino a un proceso">Reportar</a></span></li>
                                        </ul>
                                        <%//}%>                           
                                        <% //if(beanSession.usuario.preguntarMenu("212") ){%> 
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/administrarNoConformidad.jsp")%>','2-1-2','administrarNoConformidad','administrarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede Administrar lider de calidad (Clasificar, valorar, asignar y determinar la Gestión)">Personas Procesos</a></span></li>
                                        </ul> 
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/administrarNoConformidad.jsp")%>','2-1-2','administrarNoConformidad','administrarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede Administrar lider de calidad (Clasificar, valorar, asignar y determinar la Gesti�n)">Administrar</a></span></li>
                                        </ul>   
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/administrarNoConformidad.jsp")%>','2-1-2','administrarNoConformidad','administrarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede gestionar una No Conformidad con destino a su proceso, o direccionarla asi como tambi�n generar Planes de acci�n y seguimientos">Gestionar</a></span></li>
                                        </ul>                            
                                        <%//}%>                                                                                  
                                        </li> -->
                                    </ul>
                                        <!--<ul>
                                        <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/planMejora/administrarPlanMejora.jsp")%>','2-1-3','administrarPlanMejora','administrarPlanMejora','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede administrar un  Plan De Mejora">Oportunidad de mejora</a></span></li>
                                        </ul>  
                                        -->
                                        <% //if(beanSession.usuario.preguntarMenu("i19") ){%>
                                        <!--             <UL>              
                                        <li class="closed"><span class="folder">Indicadores</span>
                                        <ul>
                                        <% if(beanSession.usuario.preguntarMenu("i19_1") ){%>                          
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("indicador/fichaTecnica.jsp")%>','19-1-1','fichaTecnica','fichaTecnica','s','s','s','s','s','s');" style="cursor:pointer" title="i19_1:Usted puede gestionar las Ficha Tecnicas de Indicadores">Ficha Tecnica</a></span>
                                        </li>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("indicador/indicador.jsp")%>','19-1-2','indicador','indicador','s','s','s','s','s','s');" style="cursor:pointer" title="i19_1Usted puede gestionar el Indicadores">Gesti�n Indicador</a></span>
                                        </li>
                                        <% }%>  
                                        </ul>  
                                        </li> 
                                        </UL>  -->
                                        <%//}%>
                                </li>
                                <% }%>

                                <% if(beanSession.usuario.preguntarMenu("ADMIN") ){%>
                                    <!-- 1 -->
                                    <li class="closed"><span class="folderAdministracion" title="ADMIN">ADMINISTRACION</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("ADMINSEG") ){%>
                                             <li class="closed"><span class="folder" title="ADMINSEG">Seguridad</span>
                                                <ul>   
                                                    <% if(beanSession.usuario.preguntarMenu("ADMINSEGROL") ){%>
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/perfiles.jsp")%>', '4-0-0', 'perfiles', 'Perfiles de Usuarios', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="ADMINSEGROL">Roles</a></span>
                                                        </li>
                                                    <% }%>

                                                    <% if(beanSession.usuario.preguntarMenu("ADMINSEGGU") ){%>       
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/crearUsuario.jsp")%>', '4-0-0', 'crearUsuario', 'Creacion de Usuarios', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="ADMINSEGGU">Gestionar Usuarios</a></span>
                                                        </li>
                                                    <% }%>
                                                    <% if(beanSession.usuario.preguntarMenu("ADMINSEGUSU") ){%>
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/usuario.jsp")%>', '4-0-0', 'usuario', 'Gestionar Información de Usuarios', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="ADMINSEGUSU">Credencial de Usuario</a></span>
                                                        </li>
                                                    <% }%>
                                                </ul>
                                            </li>
                                        <% }%>
                                        <% if(beanSession.usuario.preguntarMenu("ADMINPARAM") ){%>
                                            <li class="closed"><span class="folder" title="ADMINPARAM">Parametros</span>
                                                <ul>
                                                    <% if(beanSession.usuario.preguntarMenu("ADMINPARAMTCPROC") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/tipoCitaProcedimiento.jsp")%>', '4-0-0', 'tipoCitaProcedimiento', 'Gestionar Facturacion', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="ADMINPARAMTCPROC">Tipo
                                                                    Cita Procedimiento</a></span>
                                                        </li>
                                                    <% }%>
                                                    <% if(beanSession.usuario.preguntarMenu("ADMINPARAMESP") ){%>
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarProfesion.jsp")%>', '4-0-0', 'configuracionArea', 'configuracionArea', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="ADMINPARAMESP" >Profesiones</a></span>
                                                        </li>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/sedeEspecialidades.jsp")%>', '4-0-0', 'sedeEspecialidades', 'sedeEspecialidades', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="ADMINPARAMESP">Especialidades</a></span>
                                                        </li>
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/tipoCitaProcedimiento.jsp")%>', '4-0-0', 'sedeEspecialidades', 'sedeEspecialidades', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="ADMINPARAMESP">Tipos cita</a></span>
                                                        </li>                                                        
                                                    <% }%>
                                                    <% if(beanSession.usuario.preguntarMenu("ADMINPARAMPPAG") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/piePagina.jsp")%>', '4-0-0', 'Pie de Pagina', 'Gestionar Información de xxx', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="ADMINPARAMPPAG">Pie
                                                                    pagina</a></span>
                                                        </li>
                                                    <% }%>
                                                    <% if(beanSession.usuario.preguntarMenu("ADMINPARAMCCOST") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/centroCosto.jsp")%>', '4-0-0', 'Centro de costo', 'Gestionar Información de xxx', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="ADMINPARAMCCOST">Centro
                                                                    de costo</a></span>
                                                        </li>
                                                    <%}%>
                                                    <% if(beanSession.usuario.preguntarMenu("ADMINPARAMPLAN") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaFormulario.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="ADMINPARAMPLAN">Plantillas</a></span>
                                                        </li>
                                                    <%}%>
                                                    <% if(beanSession.usuario.preguntarMenu("ADMINPARAMPFOL") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaFolio.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="ADMINPARAMPFOL">Plantillas Folio</a></span>
                                                        </li>
                                                    <%}%>
                                                    <% if(beanSession.usuario.preguntarMenu("ADMINPARAMPENC") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaEncuesta.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="ADMINPARAMPENC">Plantillas Encuesta</a></span>
                                                        </li>
                                                    <%}%>
                                                    <% if(beanSession.usuario.preguntarMenu("ADMINPARAMPLAB") ){%>
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaLaboratorio.jsp")%>', '4-0-0', 'plantillaLaboratorio', 'plantillaLaboratorio', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="ADMINPARAMPLAB">Plantillas Laboratorio</a></span>
                                                        </li>
                                                    <%}%>
                                                    <% if(beanSession.usuario.preguntarMenu("ADMINPARAMENCTIPFOL") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/encuestaTipoFolio.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer"  title="ADMINPARAMENCTIPFOL">Encuesta tipo folio</a></span>
                                                        </li>
                                                     <%}%>
                                                    
                                                </ul>
                                            </li>
                                            <%}%>
                                            <% if(true){%>
                                                <li class="closed"><span class="folder" title="ADMINUBI">Ubicaciones</span>
                                                    <ul>
                                                        <% if(beanSession.usuario.preguntarMenu("ADMINUBISEDE") ){%>
                                                            <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configuracionSedes.jsp")%>', '4-0-0', 'configuracionArea', 'configuracionArea', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="ADMINUBISEDE" >Sede</a></span>
                                                            </li>                                                            
                                                         <% }%> 
                                                        <% if(beanSession.usuario.preguntarMenu("ADMINUBIAREA") ){%>
                                                            <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configuracionArea.jsp")%>', '4-0-0', 'configuracionArea', 'configuracionArea', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="ADMINUBIAREA" >Area</a></span>
                                                            </li>                                                            
                                                         <% }%>                                                                                                                                                                               
                                                        <% if(beanSession.usuario.preguntarMenu("ADMINUBIHAB") ){%>
                                                       
                                                            <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarHabitacion.jsp")%>', '4-0-0', 'configuracionHabitacion', 'configuracionHabitacion', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="ADMINUBIHAB">Habitacion</a></span>
                                                            </li>                                                            
                                                                                                                                                                                                                                            
                                                        <% }%>
                                                         
                                                        <% if(beanSession.usuario.preguntarMenu("ADMINUBICAMA") ){%>
                                                            <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarElemento.jsp")%>', '4-0-0', 'configuracionElemento', 'configuracionElemento', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="ADMINUBICAMA">Cama</a></span>
                                                            </li>  
                                                        <% }%>                                                         
                                                  </ul>
                                                </li>
                                            <% }%>
                                            
                                        </ul>
                                    </li>
                                <%}%>
                                <li class="closed"><span class="folderCalidad">CONFIGURACION</span>
                                    <ul>
                                        <li><span class="file"><a
                                            onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/password.jsp")%>', '4-0-0', 'password', 'password', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer" title="">Cambio de Contrase&ntilde;a</a></span>
                                        </li>
                                    </ul>
                                </li>

                                
                                    <!-- Desbilitado / validar cierre de sesion con borrado de cche en el navegador -->
                                    <!-- <li class="closed"><span class="folderCierre">Cerrar Sesion</span>
                                        <ul>
                                            <li><span class="file">
                                                <a </span>
                                                <a href="http://190.60.242.160:8383/clinica/paginas/login.jsp" 
                                                onclick='NoBack();'  >Salir del aplicativo</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <script type="text/javascript">
                                
                                    </script> -->
                              

                              </ul>
                             <!--</li> -->
                           </DIV>
                       </div>

                            <% if(beanSession.cn.getNombreBd().equals("clinica")){ %>
                                <div id="main"
                                    style="width:1494px; left:0px; top:50px; height:4000px;  background-image:url(../utilidades/imagenes/menu/fondoProduccion.jpg); background-repeat:no-repeat;   "
                                    align="center"></div>
                                <% }else { %>
                                <div id="main"
                                    style="width:1494px; left:0px; top:50px; height:4000px; background-image:url(../utilidades/imagenes/menu/fondoProduccion.jpg); background-repeat:no-repeat;  "
                                    align="center"></div>
                                <% }%>
                                <DIV
                                    style="top:701px; display:none; left:10px; position:absolute; color:#3C3; font-weight:bold; width:1136px; height:9px; background-color:#8c0050">
                                </DIV>
                                <div class="mensaje" id="mensaje"
                                    style="top:666px; left:7px; position:absolute; color:#FFFFFF; font-weight:bold ">
                                </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<!--
<center><input id="btn_SALIR"  type="button" class="blue buttonini" value="CERRAR SESION" style="width:190px"  onClick="javascript:salir();" tabindex="999"/></center>
-->

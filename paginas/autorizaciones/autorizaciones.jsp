<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1250px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="AUTORIZACIONES" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>                        
                        <div id="tabsAutorizaciones" style="width:98%; height:auto">
                            <ul>
                                <li>
                                    <a href="#divFoliosTipoAdmision" onclick="tabActivoFoliosParametros('divFoliosTipoAdmision')">
                                        <center>LISTADO SOLICITUDES ORDENES</center>
                                    </a>
                                </li>
                                <!--<li>
                                    <a href="#divFoliosProfesion" onclick="tabActivoFoliosParametros('divFoliosProfesion')">
                                        <center>BUSQUEDA SOLICITUDES</center>
                                    </a>
                                </li>-->
                            </ul>

                            <div id="divFoliosTipoAdmision">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr class="estiloImputIzq2">
                                                    <td>SEDE</td>                                    
                                                    <td style="width: 50%;">
                                                        <select id="cmbSede" style="width: 50%;">
                                                            <%     resultaux.clear();
                                                            resultaux=(ArrayList)beanAdmin.combo.cargar(963);	
                                                            ComboVO cmbS; 
                                                            for(int k=0;k<resultaux.size();k++){ 
                                                                    cmbS=(ComboVO)resultaux.get(k);
                                                            %>
                                                        <option value="<%= cmbS.getId()%>"><%= cmbS.getDescripcion()%></option>
                                                        <%}%>
                                                        </select>
                                                    </td>
                                                    <td>ESTADO</td>
                                                    <td style="width: 50%;">
                                                        <select id="cmbOpcion" onchange="mostrarBoton();setTimeout(buscarHC('listGrillaAutorizaciones'),1500);">                                                            
                                                            <option value="RE">REVISION</option>
                                                            <option value="PO">POSPUESTAS</option>
                                                            <option value="ER">ERRORES</option>
                                                            <option value="RA">RADICADAS</option>
                                                            <option value="DE">DEVUELTAS</option>
                                                            <option value="AU">AUTORIZADAS</option>
                                                            <option value="AN">ANULADAS</option>
                                                        </select>
                                                    </td>                                                                                                                                             
                                                </tr>                                                                                                
                                                <tr class="estiloImputIzq2">
                                                    <td>MEDICO</td>                                                                                        
                                                    <td><input type="text" id="txtMedico" maxlength="200"  style="width:50%" onkeypress="llamarAutocomIdDescripcion('txtMedico', 971)" tabindex="124"/>
                                                        <img width="18px" height="18px" align="middle" title="VEN3" id="idLupitaVentanitaProce" onclick="traerVentanita(this.id,'','divParaVentanita','txtMedico','32')" src="/clinica/utilidades/imagenes/acciones/buscar.png">                                                         
                                                    </td>
                                                </tr>
                                                <tr class="estiloImputIzq2">
                                                    <td colspan="4" align="CENTER">
                                                        <input type="button" class="small button blue" value="CONSULTAR"
                                                        onclick="buscarHC('listGrillaAutorizaciones')"/>                                                                                            
                                                        <input type="button" class="small button blue" value="LIMPIAR"
                                                        onclick="limpiaAtributo('cmbOpcion',0);limpiaAtributo('cmbSede',0);limpiaAtributo('txtMedico',0)"/>                                                        
                                                    </td>
                                                </tr>                                                                                                                                                                                              
                                            </table>
                                        </td>                                        
                                    </tr>
                                    <div id="divParaVentanita"></div>
                                    <div id="divParaVentanitaA"></div>
                                    <div id="divNormales">
                                        <tr class="titulos">
                                            <td width="100%">
                                                <table id="listGrillaAutorizaciones"></table>
                                            </td>                                        
                                        </tr>
                                    </div>                                                                        
                                    <tr>
                                        <td>
                                            <input type="button" class="small button blue" value="EDITAR" id="btnVer"
                                            onclick="ventanaEditar('divParaVentanitaA')"/>
                                            <input type="button" class="small button blue" value="VER FOLIO"
                                            onclick="formatoPDFHC()"/>
                                        </td>
                                    </tr>                                    
                                </table>
                                <label hidden id="lblIdDocumento"></label>
                                <label hidden id="lblTipoDocumento"></label>
                                <label hidden id="lblSolicitud"></label>
                                <label hidden id="lblPaciente"></label>
                                <label hidden id="lblMedico"></label>
                                <label hidden id="lblTipoDiagnostico"></label>
                                <label hidden id="lblDiagnosticoP"></label>
                                <label hidden id="lblDiagnosticoR"></label>
                                <label hidden id="lblJustificacion"></label>                                
                            </div>
    
                           <!--<div id="divFoliosProfesion">
                                <table>
                                    <tr class="titulos">
                                        <td>
                                            <table id="listGrillaProfesiones" class="scroll"></table>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">                                
                                    <tr class="estiloImputIzq2" style="width: 90%;">
                                        <td width="25%"">PROFESION</td>
                                        <td align="left" style="width: 90%;">
                                            <select id="cmbProfesion" style="width: 50%;">
                                            <option value="">[ NINGUNO ]</option>
                                            <%     resultaux.clear();
                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(40);	
                                                    ComboVO cmbP; 
                                                    for(int k=0;k<resultaux.size();k++){ 
                                                            cmbP=(ComboVO)resultaux.get(k);
                                                %>
                                            <option value="<%= cmbP.getId()%>"><%= cmbP.getDescripcion()%></option>
                                            <%}%>
                                            </select>
                                        </td>
                                    </tr>                                
                                    <tr class="estiloImputDer">
                                        <td colspan="2" align="CENTER">
                                            <input type="button" class="small button blue" value="ADICIONAR"
                                            onclick="modificarCRUDParametros('adicionarProfesionFormulario')"/>                                    
                                            <input type="button" class="small button blue" value="ELIMINAR"
                                            onclick="modificarCRUDParametros('eliminarProfesionFormulario')"/>                                                                        
                                            <input type="button" class="small button blue" value="LIMPIAR"
                                            onclick="limpiaAtributo('txtProfesion',0);limpiaAtributo('cmbProfesion',0)"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>-->
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<input type="text" style="display: none;" id="txtTipoCita"/>
<input type="text" style="display: none;" id="txtProfesion"/>
<?xls version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Clinica.Presentacion.*" %>
	<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>

	<%@ page import = "java.text.NumberFormat" %>
    <%@ page import = "Sgh.Utilidades.*" %>

    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
        
<rows>
<%
   Fecha fecha = new Fecha();
   beanAdmin.setCn(beanSession.getCn());
//   beanEventoA.setCn(beanSession.getCn());
      

   java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
   java.util.Date date = cal.getTime();
   java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
   SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");	
   NumberFormat nf = NumberFormat.getInstance(Locale.US);
   DecimalFormat df = (DecimalFormat)nf;
   df.applyPattern("###,##0.00");
   ArrayList resultaux=new ArrayList();
   ArrayList resultDocs=new ArrayList();
   if(request.getParameter("accion")!=null ){
    
	//	---------------------------------------------------------------------------------------- buscar ventanas jquery	
	
    if(request.getParameter("accion").equals("administrarPlatin")){

			 if(request.getParameter("idPrograma").equals("00"))
			       beanPlan.platin.setIdPrograma("%");
			 else  beanPlan.platin.setIdPrograma(request.getParameter("idPrograma"));			   
			 
			 if(request.getParameter("tipoPlan").equals("00"))
			       beanPlan.platin.setIdTipoPlan("%");
			 else  beanPlan.platin.setIdTipoPlan(request.getParameter("tipoPlan"));			 

			 beanPlan.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlan.setRows(request.getParameter("rows"));  //limite de filas por paginas

			 
//			 beanPlan.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlan.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlan.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlan.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlan.asignaBuscarPag(request.getParameter("accion"));			

			 //System.out.println("contador"+resultaux.size());
			 PlatinVO parametro=new PlatinVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanPlan.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(PlatinVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[<%= parametro.getContador() %>]]></cell>                            
                                  <cell><![CDATA[<%= parametro.getIdPlatin() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdPrograma() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdPaciente() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNomPaciente() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getEscolaridad() %>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getNomPersonaRemite() %>]]></cell>                                                                                                    
                                  <cell><![CDATA[<%= parametro.getMotivoRemision() %>]]></cell>                                                                                                    
                                  <cell><![CDATA[<%= parametro.getMotivoIngreso() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getDxIntegralInicial() %>]]></cell>   
                                  <cell><![CDATA[<%= parametro.getIdTipoPlan() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDesTipoPlan() %>]]></cell>                                  
                                  
                                  <cell><![CDATA[<%= parametro.getFechaIni() %>]]></cell>                                                                                                                                                                                                                                      
                                  <cell><![CDATA[<%= parametro.getFechaFin() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdResponsableIngreso() %>]]></cell>
                            </row>                         
					<%	
			}														
    }	
    if(request.getParameter("accion").equals("listDxIntegralInicial")){

	         beanPlan.platin.setIdPlatin(request.getParameter("idPlatin"));

			 beanPlan.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlan.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
//			 beanPlan.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlan.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlan.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlan.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlan.asignaBuscarPag(request.getParameter("accion"));			

			 //System.out.println("contador"+resultaux.size());
			 PlatinGralVO parametro=new PlatinGralVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanPlan.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(PlatinGralVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[<%= parametro.getContador() %>]]></cell>                                       
                                  <cell><![CDATA[<%= parametro.getId() %>]]></cell>                                                             
                                  <cell><![CDATA[<%= parametro.getDescripcion() %>]]></cell>    
                                  <cell><![CDATA[<%= parametro.getDescTipoServicio() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getIdElaboro() %>]]></cell>                                                                                                        
                                  <cell><![CDATA[<%= parametro.getNomElaboro() %>]]></cell>                                    
                                  <cell><![CDATA[<%= parametro.getFechaElaboro() %>]]></cell>                                                                                                        
                            </row>                         
					<%	
			}														
    }	
 if(request.getParameter("accion").equals("listSituaActual")){

	         beanPlan.platin.setIdPlatin(request.getParameter("idPlatin"));  
	         beanPlan.platin.setIdAreaDerecho(request.getParameter("idAreaDerecho")); 			 
			 beanPlan.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlan.setRows(request.getParameter("rows"));  //limite de filas por paginas			 
//			 beanPlan.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlan.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlan.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlan.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlan.asignaBuscarPag(request.getParameter("accion"));			

			 PlatinGralVO parametro=new PlatinGralVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanPlan.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(PlatinGralVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[<%= parametro.getContador() %>]]></cell>                                       
                                  <cell><![CDATA[<%= parametro.getId() %>]]></cell>                                                             
                                  <cell><![CDATA[<%= parametro.getDescripcion() %>]]></cell>    
                                  <cell><![CDATA[<%= parametro.getDescTipoServicio() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getIdElaboro() %>]]></cell>                                                                                                        
                                  <cell><![CDATA[<%= parametro.getNomElaboro() %>]]></cell>                                    
                                  <cell><![CDATA[<%= parametro.getFechaElaboro() %>]]></cell>                                                                                                        
                            </row>                         
					<%	
			}														
    }		
 else if(request.getParameter("accion").equals("listObjMeta")){

	         beanPlan.platin.setIdPlatin(request.getParameter("idPlatin"));  
	         beanPlan.platin.setIdAreaDerecho(request.getParameter("idAreaDerecho")); 			 
			 beanPlan.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlan.setRows(request.getParameter("rows"));  //limite de filas por paginas			 
//			 beanPlan.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlan.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlan.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlan.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlan.asignaBuscarPag(request.getParameter("accion"));			

			 PlatinGralVO parametro=new PlatinGralVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanPlan.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(PlatinGralVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[<%= parametro.getContador() %>]]></cell>                                       
                                  <cell><![CDATA[<%= parametro.getId() %>]]></cell>                                                             
                                  <cell><![CDATA[<%= parametro.getObjetivo() %>]]></cell>    
                                  <cell><![CDATA[<%= parametro.getMeta() %>]]></cell>                                      
                                  <cell><![CDATA[<%= parametro.getDescTipoServicio() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getIdElaboro() %>]]></cell>                                                                                                        
                                  <cell><![CDATA[<%= parametro.getNomElaboro() %>]]></cell>                                    
                                  <cell><![CDATA[<%= parametro.getFechaElaboro() %>]]></cell>                                                                                                        
                            </row>                         
					<%	
			}														
    }	
 if(request.getParameter("accion").equals("listActividad")){

	         beanPlan.platin.setIdPlatin(request.getParameter("idPlatin"));  
	         beanPlan.platin.setIdAreaDerecho(request.getParameter("idAreaDerecho")); 			 
			 beanPlan.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlan.setRows(request.getParameter("rows"));  //limite de filas por paginas			 
//			 beanPlan.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlan.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlan.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlan.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlan.asignaBuscarPag(request.getParameter("accion"));			

			 PlatinGralVO parametro=new PlatinGralVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanPlan.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(PlatinGralVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[<%= parametro.getContador() %>]]></cell>                                       
                                  <cell><![CDATA[<%= parametro.getId() %>]]></cell>                                                             
                                  <cell><![CDATA[<%= parametro.getDescripcion() %>]]></cell>    
                                  <cell><![CDATA[<%= parametro.getDescTipoServicio() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getIdElaboro() %>]]></cell>                                                                                                        
                                  <cell><![CDATA[<%= parametro.getNomElaboro() %>]]></cell>                                    
                                  <cell><![CDATA[<%= parametro.getFechaElaboro() %>]]></cell>                                                                                                        
                            </row>                         
					<%	
			}														
    }	
	
	
   else if(request.getParameter("accion").equals("reportarNoConformidad")){	 
			 beanAdmin.noConformidad.setIdProceso(request.getParameter("idProceso"));
			 beanAdmin.noConformidad.setIsoIdNoConformidad(request.getParameter("idNoConformidades"));		
			 if(request.getParameter("AreaEvento").equals("00"))
			   beanAdmin.noConformidad.setIsoNoConformidadArea("%");
			 else
			   beanAdmin.noConformidad.setIsoNoConformidadArea(request.getParameter("AreaEvento"));			   
			 beanAdmin.noConformidad.setFechaEventoDesde(request.getParameter("txtBusFechaEventoNoConfDesde"));
			 beanAdmin.noConformidad.setFechaEventoHasta(request.getParameter("txtBusFechaEventoNoConfHasta"));
			 if(request.getParameter("txtBusIdEventoNoConf").equals(""))// un evento especifico o todos
			    beanAdmin.noConformidad.setIdISO("%");
			 else
   			    beanAdmin.noConformidad.setIdISO(request.getParameter("txtBusIdEventoNoConf").trim());			 
			 //System.out.println("setNoIdElavoro "+beanSession.usuario.getIdentificacion());
			 if(request.getParameter("solo_mis").equals("true"))
			   beanAdmin.noConformidad.setNoIdElavoro(beanSession.usuario.getIdentificacion());	
			 else
			   beanAdmin.noConformidad.setNoIdElavoro("%");		
			 beanAdmin.noConformidad.setEstadoEvento("%");		
			 beanAdmin.setPage(request.getParameter("page"));  // paginas que se pide
			 beanAdmin.setRows(request.getParameter("rows"));  //limite de filas por paginas			 
			 if(request.getParameter("sidx").equals("codigo")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanAdmin.setSidx("2");
			 
			 beanAdmin.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanAdmin.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanAdmin.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanAdmin.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanAdmin.asignaBuscarPag("administrarNoConformidad");					

			 //System.out.println("contador"+resultaux.size());
			 NoConformidadVO parametro=new NoConformidadVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanAdmin.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(NoConformidadVO)resultaux.get(i);
				i++;		
					%> 					
							 <row id = '<%= i%>'> 
                                   <cell><![CDATA[0]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescripProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdISOEvento() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdElavoro() %>]]></cell>         
                                  <cell><![CDATA[<%= parametro.getISOEventoDescripcion() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getISOEventoDesignacion() %>]]></cell>   
                                  <cell><![CDATA[<%= parametro.getIsoResponsable() %>]]></cell>                                                                                                   
                                  <cell><![CDATA[<%= parametro.getIsoIdNoConformidad() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoNoConformidadDescripcion() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdGnUbicacionArea() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdGnUbicacionAreaDescrip() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoFechaInicio() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoHoraInicio() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoMinutoInicio() %>]]></cell>                                                                    
                            </row> 
					<%	
			}														
    }	
	
	//Aqui uno nuevo
}
	
	//System.out.println("4");
%>

 	  
 	 
 </rows>            
<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Clinica.Presentacion.*" %>
	<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>

	<%@ page import = "java.text.NumberFormat" %>
    <%@ page import = "Sgh.Utilidades.*" %>

    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanEventoA" class="Clinica.GestionarEvento.ControlEventos" scope="session" /> <!-- instanciar bean de session -->
   <!-- <jsp:useBean id="beanPlanMejora" class="Clinica.GestionarPlanMejora.ControlPlanMejora" scope="session" /> <!-- instanciar bean de session -->    -->
<rows>
<%
   Fecha fecha = new Fecha();
   beanAdmin.setCn(beanSession.getCn());
  /* beanEventoA.setCn(beanSession.getCn());
   beanPlanMejora.setCn(beanSession.getCn());   */

   java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
   java.util.Date date = cal.getTime();
   java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
   SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");	
   NumberFormat nf = NumberFormat.getInstance(Locale.US);
   DecimalFormat df = (DecimalFormat)nf;
   df.applyPattern("###,##0.00");
   ArrayList resultaux=new ArrayList();
   ArrayList resultDocs=new ArrayList();
   if(request.getParameter("accion")!=null ){
    
	//	---------------------------------------------------------------------------------------- buscar ventanas jquery
/*	
   if(request.getParameter("accion").equals("reportarNoConformidad")){	   	     
			 
			 beanAdmin.noConformidad.setIdProceso(request.getParameter("idProceso"));
			 beanAdmin.noConformidad.setIsoIdNoConformidad(request.getParameter("idNoConformidades"));		
			 if(request.getParameter("AreaEvento").equals("00"))
			   beanAdmin.noConformidad.setIsoNoConformidadArea("%");
			 else
			   beanAdmin.noConformidad.setIsoNoConformidadArea(request.getParameter("AreaEvento"));			   

			 beanAdmin.noConformidad.setFechaEventoDesde(request.getParameter("txtBusFechaEventoNoConfDesde"));
			 beanAdmin.noConformidad.setFechaEventoHasta(request.getParameter("txtBusFechaEventoNoConfHasta"));

			 if(request.getParameter("txtBusIdEventoNoConf").equals(""))// un evento especifico o todos
			    beanAdmin.noConformidad.setIdISO("%");
			 else
   			    beanAdmin.noConformidad.setIdISO(request.getParameter("txtBusIdEventoNoConf").trim());
			 
			 //System.out.println("setNoIdElavoro "+beanSession.usuario.getIdentificacion());
			 if(request.getParameter("solo_mis").equals("true"))
			   beanAdmin.noConformidad.setNoIdElavoro(beanSession.usuario.getIdentificacion());	
			 else
			   beanAdmin.noConformidad.setNoIdElavoro("%");				 
			 
			 beanAdmin.noConformidad.setEstadoEvento("%");	

			 
			 beanAdmin.setPage(request.getParameter("page"));  // paginas que se pide
			 beanAdmin.setRows(request.getParameter("rows"));  //limite de filas por paginas			 
			 if(request.getParameter("sidx").equals("codigo")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanAdmin.setSidx("2");
			 
			 beanAdmin.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanAdmin.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanAdmin.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanAdmin.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanAdmin.asignaBuscarPag("administrarNoConformidad");					

			 //System.out.println("contador"+resultaux.size());
			 NoConformidadVO parametro=new NoConformidadVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanAdmin.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(NoConformidadVO)resultaux.get(i);
				i++;		
					%> 					
							 <row id = '<%= i%>'> 
                                  <cell><![CDATA[<%= parametro.getIdProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescripProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdISOEvento() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdElavoro() %>]]></cell>         
                                  <cell><![CDATA[<%= parametro.getISOEventoDescripcion() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getISOEventoDesignacion() %>]]></cell>   
                                  <cell><![CDATA[<%= parametro.getIsoResponsable() %>]]></cell>                                                                                                   
                                  <cell><![CDATA[<%= parametro.getIsoIdNoConformidad() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoNoConformidadDescripcion() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdGnUbicacionArea() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdGnUbicacionAreaDescrip() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoFechaInicio() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoHoraInicio() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoMinutoInicio() %>]]></cell>                                                                    
                                  <cell><![CDATA[<%= parametro.getEstadoEvento()%>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIsoIdTipoISOEvento()%>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getIsoDescrTipoISOEvento()%>]]></cell>    
                                  <cell><![CDATA[<%= parametro.getIsoFechaIniPlanAccion()%>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getIsoNoConformidadTratam() %>]]></cell> <!--deescrip posible tratamiento-->     
                                  <cell><![CDATA[<%= parametro.getIdISORef() %>]]></cell>               <!--id deescrip posible tratamiento, evento relacionado-->                                    
                                  <cell><![CDATA[<%= parametro.getIdISORefDoc() %>]]></cell>               <!--id cocumento para algunos, evento que si tiene entonces es de IMPACTO ELSE NO IMPACTO-->                                                                      
                                  
                                  <%if(  parametro.getIdISORefDoc().equals("")  ){ %>
                                  <cell><![CDATA[No Impacto]]></cell> 
                                  <%}else{%>
                                  <cell><![CDATA[Impacto]]></cell> 
                                  <%}%>
                                  <cell><![CDATA[<%= parametro.getIsoAnalisisDoc() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoMetasdoc() %>]]></cell>   
                                  <%if(  parametro.getIdISOEventoAsociado()!=null  ){ %>  <!--si esta asociado tiene valor y NOO se pueden asociar otros eventos a su plan de accion-->
                                   <cell><![CDATA[Si]]></cell> 
                                   <cell><![CDATA[<%= parametro.getIdISOEventoAsociado() %>]]></cell> 
                                   <%}else{%>
                                   <cell><![CDATA[No]]></cell> 
                                   <cell><![CDATA[-]]></cell><!-- este es candidato para ser padre y asociarle otras no conformidades-->
                                  <%}%>
                                  
                                  
                            </row> 
                        
					<%	
			}														
    }	
	else if(request.getParameter("accion").equals("administrarNoConformidad")){	      
			 
			 beanAdmin.noConformidad.setIdProceso(request.getParameter("idProceso"));
			 beanAdmin.noConformidad.setIsoIdNoConformidad(request.getParameter("idNoConformidades"));		
			 if(request.getParameter("AreaEvento").equals("00"))
			   beanAdmin.noConformidad.setIsoNoConformidadArea("%");
			 else
			   beanAdmin.noConformidad.setIsoNoConformidadArea(request.getParameter("AreaEvento"));			   
			 beanAdmin.noConformidad.setFechaEventoDesde(request.getParameter("txtBusFechaEventoNoConfDesde"));
			 beanAdmin.noConformidad.setFechaEventoHasta(request.getParameter("txtBusFechaEventoNoConfHasta"));	
			 //beanAdmin.noConformidad.setNoIdElavoro(beanSession.usuario.getIdentificacion());	
			 beanAdmin.noConformidad.setNoIdElavoro("%");	
			 beanAdmin.noConformidad.setEstadoEvento(request.getParameter("EstadoEvento"));	
			 
 			 if(request.getParameter("txtBusIdEventoNoConf").equals(""))// un evento especifico o todos
			    beanAdmin.noConformidad.setIdISO("%");
			 else
   			    beanAdmin.noConformidad.setIdISO(request.getParameter("txtBusIdEventoNoConf").trim());
				

			 beanAdmin.noConformidad.setConPlan(request.getParameter("solo_plan"));	

			 beanAdmin.setPage(request.getParameter("page"));  // paginas que se pide
			 beanAdmin.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
			 if(request.getParameter("sidx").equals("codigo")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanAdmin.setSidx("2");
			 
			 beanAdmin.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanAdmin.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanAdmin.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanAdmin.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanAdmin.asignaBuscarPag(request.getParameter("accion"));	
			 

			 //System.out.println("contador"+resultaux.size());
			 NoConformidadVO parametro=new NoConformidadVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanAdmin.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(NoConformidadVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[<%= parametro.getContador() %>]]></cell>                            
                                  <cell><![CDATA[<%= parametro.getIdProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescripProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdISOEvento() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdElavoro() %>]]></cell>                                  
                                  <cell><![CDATA[<%= parametro.getISOEventoDescripcion() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getISOEventoDesignacion() %>]]></cell>   
                                  <cell><![CDATA[<%= parametro.getIsoResponsable() %>]]></cell>                                                                                                   
                                  <cell><![CDATA[<%= parametro.getIsoIdNoConformidad() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoNoConformidadDescripcion() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdGnUbicacionArea() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdGnUbicacionAreaDescrip() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoFechaInicio() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoHoraInicio() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoMinutoInicio() %>]]></cell>                                                                    
                                  <cell><![CDATA[<%= parametro.getEstadoEvento()%>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIsoIdTipoISOEvento()%>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getIsoDescrTipoISOEvento()%>]]></cell>    
                                  <cell><![CDATA[<%= parametro.getIsoFechaIniPlanAccion()%>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getIsoNoConformidadTratam() %>]]></cell> <!--deescrip posible tratamiento-->     
                                  <cell><![CDATA[<%= parametro.getIdISORef() %>]]></cell>               <!--id deescrip posible tratamiento, evento relacionado-->                                    
                                  <cell><![CDATA[<%= parametro.getIdISORefDoc() %>]]></cell>               <!--id cocumento para algunos, evento que si tiene entonces es de IMPACTO ELSE NO IMPACTO-->                                                                      
                                  
                                  <%if(  parametro.getIdISORefDoc().equals("")  ){ %>
                                  <cell><![CDATA[No Impacto]]></cell> 
                                  <%}else{%>
                                  <cell><![CDATA[Impacto]]></cell> 
                                  <%}%>
                                  <cell><![CDATA[<%= parametro.getIsoAnalisisDoc() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoMetasdoc() %>]]></cell>   
                                  <%if(  parametro.getIdISOEventoAsociado()!=null  ){ %>  <!--si esta asociado tiene valor y NOO se pueden asociar otros eventos a su plan de accion-->
                                   <cell><![CDATA[Si]]></cell> 
                                   <cell><![CDATA[<%= parametro.getIdISOEventoAsociado() %>]]></cell> 
                                   <%}else{%>
                                   <cell><![CDATA[No]]></cell> 
                                   <cell><![CDATA[-]]></cell><!-- este es candidato para ser padre y asociarle otras no conformidades-->
                                  <%}%>
                                  <cell><![CDATA[<%= parametro.getModificable()%>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getGnFechaActualizacion() %>]]></cell>
                            </row> 
                        
					<%	
			}														
    }	
	else if(request.getParameter("accion").equals("administrarNoConformidadAsociar")){	     
			 
			 beanAdmin.noConformidad.setIdProceso(request.getParameter("idProceso"));
			 beanAdmin.noConformidad.setIsoIdNoConformidad(request.getParameter("idNoConformidades"));		
			 beanAdmin.noConformidad.setIsoNoConformidadArea(request.getParameter("AreaEvento"));
			 beanAdmin.noConformidad.setFechaEventoDesde(request.getParameter("txtBusFechaEventoNoConfDesde"));
			 beanAdmin.noConformidad.setFechaEventoHasta(request.getParameter("txtBusFechaEventoNoConfHasta"));	
			// beanAdmin.noConformidad.setNoIdElavoro(beanSession.usuario.getIdentificacion());	
			 beanAdmin.noConformidad.setEstadoEvento(request.getParameter("EstadoEvento"));			 

			 beanAdmin.setPage(request.getParameter("page"));  // paginas que se pide
			 beanAdmin.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
			 if(request.getParameter("sidx").equals("codigo")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanAdmin.setSidx("2");
			 
			 beanAdmin.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanAdmin.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanAdmin.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanAdmin.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanAdmin.asignaBuscarPag(request.getParameter("accion"));					

			 //System.out.println("contador"+resultaux.size());
			 NoConformidadVO parametro=new NoConformidadVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanAdmin.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(NoConformidadVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                              <%if(  parametro.getIdISORefDoc().equals("")  ){ %>
                                  <cell><![CDATA[<%= parametro.getIdProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescripProceso() %>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIdISOEvento() %>]]></cell>                                 
                                  <cell><![CDATA[<%= parametro.getISOEventoDescripcion() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getISOEventoDesignacion() %>]]></cell>   
                                  <cell><![CDATA[<%= parametro.getIsoResponsable() %>]]></cell>                                                                                                   
                                  <cell><![CDATA[<%= parametro.getIsoIdNoConformidad() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoNoConformidadDescripcion() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdGnUbicacionArea() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdGnUbicacionAreaDescrip() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoFechaInicio() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getEstadoEvento()%>]]></cell> 
 
                                   <%}%>                                 
                                  
                            </row> 
                        
					<%	
			}														
    }	
	else if(request.getParameter("accion").equals("listPersonasPlanAccion")){	     
			 beanAdmin.noConformidad.setIdProceso(request.getParameter("IdEvento"));
			 beanAdmin.setPage(request.getParameter("page"));  // paginas que se pide
			 beanAdmin.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
			 if(request.getParameter("sidx").equals("codigo"))
			      beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanAdmin.setSidx("2");
			 
			 beanAdmin.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanAdmin.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanAdmin.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanAdmin.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanAdmin.asignaBuscarPag(request.getParameter("accion"));	
			 //System.out.println("contador"+resultaux.size());
			 NoConformidadVO parametro=new NoConformidadVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanAdmin.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(NoConformidadVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[0]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIsoResponsable()%>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoResponsableNombre() %>]]></cell> 
                            </row> 
                        
					<%	
			}														
    }	

	else if(request.getParameter("accion").equals("listAsociarEventos")){	     
			 beanAdmin.noConformidad.setIdProceso(request.getParameter("IdEvento"));
			 beanAdmin.setPage(request.getParameter("page"));  // paginas que se pide
			 beanAdmin.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
			 if(request.getParameter("sidx").equals("codigo"))
			      beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanAdmin.setSidx("2");
			 
			 beanAdmin.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanAdmin.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanAdmin.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanAdmin.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanAdmin.asignaBuscarPag(request.getParameter("accion"));					

			 //System.out.println("contador"+resultaux.size());
			 NoConformidadVO parametro=new NoConformidadVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanAdmin.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(NoConformidadVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[0]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIdProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescripProceso() %>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIdISOEvento() %>]]></cell>                                 
                                  <cell><![CDATA[<%= parametro.getISOEventoDescripcion() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getISOEventoDesignacion() %>]]></cell>   
                                  <cell><![CDATA[<%= parametro.getIsoResponsable() %>]]></cell>                                                                                                   
                                  <cell><![CDATA[<%= parametro.getIsoIdNoConformidad() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoNoConformidadDescripcion() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdGnUbicacionArea() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNoIdGnUbicacionAreaDescrip() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoFechaInicio() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getEstadoEvento()%>]]></cell> 
                            </row> 
                        
					<%	
			}														
    }	
	
					
	else if(request.getParameter("accion").equals("listPlanAccion")){	      
			 beanAdmin.noConformidad.setIdISORef(request.getParameter("IdEvento"));
			 beanAdmin.setPage(request.getParameter("page"));  // paginas que se pide
			 beanAdmin.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
//			 if(request.getParameter("sidx").equals("IdEvento")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 //beanAdmin.setSidx("1");
			 
			 beanAdmin.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanAdmin.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanAdmin.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanAdmin.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanAdmin.asignaBuscarPag(request.getParameter("accion"));	
			 //System.out.println("contador"+resultaux.size());
			 NoConformidadVO parametro=new NoConformidadVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanAdmin.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(NoConformidadVO)resultaux.get(i);
				i++;		
					%>					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[0]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIdISOEvento() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoIdSubTipoISOEvento() %>]]></cell>   
                                  <cell><![CDATA[<%= parametro.getIsoDescrSubTipoISOEvento() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getISOEventoDescripcion() %>]]></cell>                                                                      
                                  <cell><![CDATA[<%= parametro.getIsoFechaFinal() %>]]></cell>                                  
                                  <cell><![CDATA[<%= parametro.getIsoResponsable() %>]]></cell>                                  
                                  <cell><![CDATA[<%= parametro.getIsoResponsableNombre() %>]]></cell>                                  
                            </row> 
					<%	
			}														
    }	
	
	else if(request.getParameter("accion").equals("listPlanAccionSeguimiento")){	    
			 beanAdmin.noConformidad.setIdISORef(request.getParameter("IdEvento"));
			 beanAdmin.setPage(request.getParameter("page"));  // paginas que se pide
			 beanAdmin.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
//			 if(request.getParameter("sidx").equals("IdEvento")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 //beanAdmin.setSidx("1");			 
			 beanAdmin.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanAdmin.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanAdmin.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanAdmin.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanAdmin.asignaBuscarPag(request.getParameter("accion"));	
			 //System.out.println("contador"+resultaux.size());
			 NoConformidadVO parametro=new NoConformidadVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanAdmin.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(NoConformidadVO)resultaux.get(i);
				i++;		
					%>					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[0]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIdISOEvento() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getEstadoEvento() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getISOEventoDescripcion() %>]]></cell>                                                                      
                                  <cell><![CDATA[<%= parametro.getDescripProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIsoResponsable() %>]]></cell>                                  
                                  <cell><![CDATA[<%= parametro.getIsoResponsableNombre() %>]]></cell>                                  
                            </row> 
					<%	
			}														
    }		
	else if(request.getParameter("accion").equals("listPlanAccionSeguimientoMeta")){
		beanAdmin.noConformidad.setIdISORef(request.getParameter("IdEvento"));
		resultaux=(ArrayList)beanAdmin.asignaBuscarPag("listPlanAccionSeguimientoMeta");	
		NoConformidadVO parametro=new NoConformidadVO();									
		int i=0;
		while(i<resultaux.size()){
				parametro=(NoConformidadVO)resultaux.get(i);
				   i++;					   
			%>
				   <seguimiento> 
                       <IdISOEvento><![CDATA[<%= parametro.getIdISOEvento()%>]]></IdISOEvento>                   
                       <isoIdTipo><![CDATA[<%= parametro.getIsoIdTipoISOEvento()%>]]></isoIdTipo>
                       <isoIdDescripcion><![CDATA[<%= parametro.getISOEventoDescripcion()%>]]></isoIdDescripcion>   
				   </seguimiento>
			<%
		}
	}	
	else if(request.getParameter("accion").equals("datosPlanAccionGral")){ 
		beanPlanMejora.planAccion.setIdEvento(request.getParameter("IdEvento"));
		resultaux=(ArrayList)beanPlanMejora.asignaBuscarPag("datosPlanAccionGral");	 
		PlanAccionVO parametro=new PlanAccionVO();								
		int i=0;
		while(i<resultaux.size()){ 
				parametro=(PlanAccionVO)resultaux.get(i);
				   i++;	
			%>
				   <planGral> 
                       <IdPlanAccion><![CDATA[<%= parametro.getIdPlanAccion()%>]]></IdPlanAccion>                   
                       <ConceptoGral><![CDATA[<%= parametro.getConceptoGral()%>]]></ConceptoGral>
                       <IdResponsable><![CDATA[<%= parametro.getIdResponsable()%>]]></IdResponsable>  
                       <IdTipoPlan><![CDATA[<%= parametro.getIdTipoPlan()%>]]></IdTipoPlan>                           
                       <RiesgoNoHacerlo><![CDATA[<%= parametro.getRiesgoNoHacerlo()%>]]></RiesgoNoHacerlo>   
                       <BeneficiosHacerlo><![CDATA[<%= parametro.getBeneficiosHacerlo()%>]]></BeneficiosHacerlo>    
                       <FechaIniPlanAccion><![CDATA[<%= parametro.getFechaIniPlanAccion()%>]]></FechaIniPlanAccion>                                                  
                       <Meta><![CDATA[<%= parametro.getMeta()%>]]></Meta>                                                  
				   </planGral>
			<%
		}
	}	
	
	else if(request.getParameter("accion").equals("administrarEventoAdverso")){	   
			 if(request.getParameter("idProceso").equals("00"))
			        beanEventoA.eventoAdverso.setIdProceso("%");
			 else   beanEventoA.eventoAdverso.setIdProceso(request.getParameter("idProceso"));	
			 if(request.getParameter("idClase").equals("00"))
			        beanEventoA.eventoAdverso.setClase("%");
			 else   beanEventoA.eventoAdverso.setClase(request.getParameter("idClase"));				 
			 if(request.getParameter("idEventoProceso").equals("00"))
			        beanEventoA.eventoAdverso.setIdEventoProceso("%");
			 else   beanEventoA.eventoAdverso.setIdEventoProceso(request.getParameter("idEventoProceso"));				 
			 if(request.getParameter("idEvento").equals(""))
			        beanEventoA.eventoAdverso.setIdEvento("%");
			 else   beanEventoA.eventoAdverso.setIdEvento(request.getParameter("idEvento"));			   
			 if(request.getParameter("AreaEvento").equals("00"))
			        beanEventoA.eventoAdverso.setIdArea("%");
			 else   beanEventoA.eventoAdverso.setIdArea(request.getParameter("AreaEvento"));			   
			 if(request.getParameter("FechaDesde").equals(""))
			        beanEventoA.eventoAdverso.setFechaDesde("01/01/1900");
			 else   beanEventoA.eventoAdverso.setFechaDesde(request.getParameter("FechaDesde"));			   
			 if(request.getParameter("FechaHasta").equals(""))
			        beanEventoA.eventoAdverso.setFechaHasta("01/01/2030");
			 else   beanEventoA.eventoAdverso.setFechaHasta(request.getParameter("FechaHasta"));			   
			 if(!request.getParameter("idPaciente").equals(""))
			      beanEventoA.eventoAdverso.setIdPaciente(request.getParameter("idPaciente"));
			 else beanEventoA.eventoAdverso.setIdPaciente("%");	
			 if(request.getParameter("EstadoEvento").equals("00"))
			        beanEventoA.eventoAdverso.setIdEstadoEvento("%");
			 else   beanEventoA.eventoAdverso.setIdEstadoEvento(request.getParameter("EstadoEvento"));	
			 if(request.getParameter("IdUnidadPertenece").equals("00"))
			        beanEventoA.eventoAdverso.setIdUnidadPertenece("%");
			 else   beanEventoA.eventoAdverso.setIdUnidadPertenece(request.getParameter("IdUnidadPertenece"));			 
			 
			 
			 
			 //beanEventoA.eventoAdverso.setNoIdElavoro(beanSession.usuario.getIdentificacion());	
//			 beanEventoA.eventoAdverso.setNoIdElavoro("%");	
			 
			 beanEventoA.setPage(request.getParameter("page"));  // paginas que se pide
			 beanEventoA.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 

			 if(request.getParameter("sidx").equals("nomPaciente")) beanEventoA.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanEventoA.setSidx("2");
			 

			 beanEventoA.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanEventoA.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanEventoA.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanEventoA.asignaBuscarPag(request.getParameter("accion"));					

			 EventoAdversoVO parametro=new EventoAdversoVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanAdmin.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(EventoAdversoVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[<%= parametro.getContador() %>]]></cell>   
                                  <cell><![CDATA[<%= parametro.getIdEvento() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescripEvento() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdPaciente() %>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getNomComplPaciente() %>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIdUnidadPertenece() %>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getNomUnidadPertenece() %>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIdProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDesProceso() %>]]></cell>    
                                  <cell><![CDATA[<%= parametro.getClase()%>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getDescClase()%>]]></cell>                               
                                  <cell><![CDATA[<%= parametro.getTratamiento()%>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdEventoProceso() %>]]></cell>   
                                  <cell><![CDATA[<%= parametro.getDesEventoProceso() %>]]></cell>                                                                                                   
                                  <cell><![CDATA[<%= parametro.getIdArea() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNomArea() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getFechaEvento() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdElavoro() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNomElavoro() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdEstadoEvento()%>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getDesEstadoEvento()%>]]></cell>   
                                  
                                  <cell><![CDATA[<%= parametro.getIdElavoroGestion()%>]]></cell>
                                  <cell><![CDATA[<%= parametro.getNomElavoroGestion()%>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdBarreraYDefensa()%>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdAccionInsegura()%>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdAmbito()%>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdPrevenible()%>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdOrganizCultura()%>]]></cell>    
                                  <cell><![CDATA[noImpacto]]></cell> 
                                  <cell><![CDATA[<%= parametro.getConclusiones()%>]]></cell>                                    
                            </row> 
                        
					<%	
			}														
    }	
	
	else if(request.getParameter("accion").equals("listFactContribu")){	   
			 
             beanEventoA.eventoAdverso.setIdEvento(request.getParameter("idEvento"));			   
			 beanEventoA.setPage(request.getParameter("page"));  // paginas que se pide
			 beanEventoA.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
			 if(request.getParameter("sidx").equals("codigo")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanEventoA.setSidx("2");
			 
			 beanEventoA.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanEventoA.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanEventoA.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanEventoA.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanEventoA.asignaBuscarPag(request.getParameter("accion"));					

			 OrigenFactorContributVO parametro=new OrigenFactorContributVO();	
			 int i=0, j=0,ban=1;
			 %>
                        <page><%=beanAdmin.getPagina()%></page> 
                        <total><%=totalpages%></total> 
                        <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(OrigenFactorContributVO)resultaux.get(i);
				i++;
					%> 					
                        <row id = '<%= i%>'> 
                              <cell><![CDATA[00]]></cell>   
                              <cell><![CDATA[<%= parametro.getCodigo() %>]]></cell>   
                              <cell><![CDATA[<%= parametro.getNombre() %>]]></cell>
                        </row> 
					<%	
			}														
    }		
	else if(request.getParameter("accion").equals("listPersonasOportMejora")){	   
			 
             beanEventoA.eventoAdverso.setIdEvento(request.getParameter("idEvento"));			   
			 beanEventoA.setPage(request.getParameter("page"));  // paginas que se pide
			 beanEventoA.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
			 if(request.getParameter("sidx").equals("codigo")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanEventoA.setSidx("2");
			 
			 beanEventoA.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanEventoA.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanEventoA.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanEventoA.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanEventoA.asignaBuscarPag(request.getParameter("accion"));					

			 OportuMejoraVO parametro=new OportuMejoraVO();	
			 int i=0, j=0,ban=1;
			 %>
                        <page><%=beanAdmin.getPagina()%></page> 
                        <total><%=totalpages%></total> 
                        <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(OportuMejoraVO)resultaux.get(i);
				i++;
					%> 					
                        <row id = '<%= i%>'> 
                              <cell><![CDATA[00]]></cell>   
                              <cell><![CDATA[<%= parametro.getDescTarea() %>]]></cell>   
                              <cell><![CDATA[<%= parametro.getFechaTarea() %>]]></cell>
                              <cell><![CDATA[<%= parametro.getIdResponsable() %>]]></cell>   
                              <cell><![CDATA[<%= parametro.getNomResponsable() %>]]></cell>   
                              <cell><![CDATA[<%= parametro.getCostos()%>]]></cell>   
                              <cell><![CDATA[<%= parametro.getHoras()%>]]></cell>                          
                        </row> 
					<%	
			}														
    }		
	else if(request.getParameter("accion").equals("listSeguimientoOportMejora")){	 
			 
             beanEventoA.eventoAdverso.setIdEvento(request.getParameter("idEvento"));			   
			 beanEventoA.setPage(request.getParameter("page"));  // paginas que se pide
			 beanEventoA.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
			 if(request.getParameter("sidx").equals("codigo")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanEventoA.setSidx("2");
			 
			 beanEventoA.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanEventoA.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanEventoA.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanEventoA.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanEventoA.asignaBuscarPag(request.getParameter("accion"));	
			 OportuMejoraVO parametro=new OportuMejoraVO();	
			 int i=0, j=0,ban=1;
			 %>
                        <page><%=beanAdmin.getPagina()%></page> 
                        <total><%=totalpages%></total> 
                        <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(OportuMejoraVO)resultaux.get(i);
				i++;
					%> 					
                        <row id = '<%= i%>'> 
                              <cell><![CDATA[00]]></cell> 
                              <cell><![CDATA[<%= parametro.getIdTarea() %>]]></cell>                                
                              <cell><![CDATA[<%= parametro.getCod_oport_majora() %>]]></cell> 
                              <cell><![CDATA[<%= parametro.getDes_oport_majora() %>]]></cell> 
                              <cell><![CDATA[<%= parametro.getCod_tiemp_ejecucion() %>]]></cell> 
                              <cell><![CDATA[<%= parametro.getDes_tiemp_ejecucion() %>]]></cell>   

                              <cell><![CDATA[<%= parametro.getDescTarea() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getSeguimTarea() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getIdElaboroSeguimTarea()%>]]></cell>
                              <cell><![CDATA[<%= parametro.getNombreComplElaboroSeguimTarea()%>]]></cell>
                        </row> 
					<%	
			}														
    }		
		
	else if(request.getParameter("accion").equals("listNivelResponsabili")){	 
			 
             beanEventoA.eventoAdverso.setIdEvento(request.getParameter("idEvento"));			   
			 beanEventoA.setPage(request.getParameter("page"));  // paginas que se pide
			 beanEventoA.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
			 if(request.getParameter("sidx").equals("codigo")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanEventoA.setSidx("2");
			 
			 beanEventoA.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanEventoA.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanEventoA.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanEventoA.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanEventoA.asignaBuscarPag(request.getParameter("accion"));	
			 NivelResponsabiVO parametro=new NivelResponsabiVO();	
			 int i=0, j=0,ban=1;
			 %>
                        <page><%=beanAdmin.getPagina()%></page> 
                        <total><%=totalpages%></total> 
                        <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(NivelResponsabiVO)resultaux.get(i);
				i++;
					%> 					
                        <row id = '<%= i%>'> 
                               <cell><![CDATA[<%= parametro.getDescripResponsabilidad() %>]]></cell>
                               <cell><![CDATA[<%= parametro.getCodigo() %>]]></cell>
                               <cell><![CDATA[<%= parametro.getNombre() %>]]></cell>
                               <cell><![CDATA[<%= parametro.getIdResponsable() %>]]></cell>
                               <cell><![CDATA[<%= parametro.getNomResponsable() %>]]></cell>
                        </row> 
					<%	
			}														
    }			
	else if(request.getParameter("accion").equals("relacionPlanAccionEvento")){ 

	
		beanPlanMejora.planMejora.setIdEvento(request.getParameter("IdEvento")); 
		resultaux=(ArrayList)beanPlanMejora.asignaBuscarPag(request.getParameter("accion"));	 
		PlanMejoraVO parametro=new PlanMejoraVO();								
		int i=0;
		while(i<resultaux.size()){ 
				parametro=(PlanMejoraVO)resultaux.get(i);
				   i++;	
			%>
				   <planGral> 
                       <IdProceso><![CDATA[<%= parametro.getIdProceso()%>]]></IdProceso>                   
                       <IdEvento><![CDATA[<%= parametro.getIdEvento()%>]]></IdEvento> 
                       <TipoPlan><![CDATA[<%= parametro.getIdTipoPlan()%>]]></TipoPlan>
                       <DescripEvento><![CDATA[<%= parametro.getDescripEvento()%>]]></DescripEvento>  
                       <Riesgo><![CDATA[<%= parametro.getRiesgo()%>]]></Riesgo>                           
                       <Costo><![CDATA[<%= parametro.getCosto()%>]]></Costo>   
                       <Volumen><![CDATA[<%= parametro.getVolumen()%>]]></Volumen>  
                       <priorizacion><![CDATA[<%= parametro.getRiesgo()*parametro.getCosto()*parametro.getVolumen()%>]]></priorizacion>                                                             
				   </planGral>
			<%
		}
	}		
	else if(request.getParameter("accion").equals("relacionEventoOM")){ 
	
		beanPlanMejora.planMejora.setIdEvento(request.getParameter("IdEvento")); 
		beanPlanMejora.planMejora.setTipoEvento(request.getParameter("TipoEvento")); 		
		resultaux=(ArrayList)beanPlanMejora.asignaBuscarPag(request.getParameter("accion"));	 
		PlanMejoraVO parametro=new PlanMejoraVO();								
		int i=0;
		while(i<resultaux.size()){ 
				parametro=(PlanMejoraVO)resultaux.get(i);
				   i++;	
			%>
				   <planGral> 
                       <IdProceso><![CDATA[<%= parametro.getIdProceso()%>]]></IdProceso>                   
                       <IdEvento><![CDATA[<%= parametro.getIdEvento()%>]]></IdEvento> 
                       <TipoPlan><![CDATA[<%= parametro.getIdTipoPlan()%>]]></TipoPlan>
                       <DescripEvento><![CDATA[<%= parametro.getDescripEvento()%>]]></DescripEvento>  
                       <Riesgo><![CDATA[<%= parametro.getRiesgo()%>]]></Riesgo>                           
                       <Costo><![CDATA[<%= parametro.getCosto()%>]]></Costo>   
                       <Volumen><![CDATA[<%= parametro.getVolumen()%>]]></Volumen>  
                       <priorizacion><![CDATA[<%= parametro.getRiesgo()*parametro.getCosto()*parametro.getVolumen()%>]]></priorizacion>                                                             
				   </planGral>
			<%
		}
	}		
/**************************************************************PLAN DE MEJORA*********************************/
	
	/*
	
    else if(request.getParameter("accion").equals("administrarPlanMejora")){
			 if(request.getParameter("idProceso").equals("00"))
			       beanPlanMejora.planMejora.setIdProceso("%");
			 else  beanPlanMejora.planMejora.setIdProceso(request.getParameter("idProceso"));			   
			 
			 if(request.getParameter("tipoPlan").equals("00"))
			       beanPlanMejora.planMejora.setIdTipoPlan("%");
			 else  beanPlanMejora.planMejora.setIdTipoPlan(request.getParameter("tipoPlan"));			 

			 if(request.getParameter("BusAspectoMejorar").equals(""))
			       beanPlanMejora.planMejora.setDescripEvento("%");
			 else  beanPlanMejora.planMejora.setDescripEvento(request.getParameter("BusAspectoMejorar"));
			 
			 beanPlanMejora.planMejora.setFechaEventoDesde(request.getParameter("BusFechaDesde"));
			 beanPlanMejora.planMejora.setFechaEventoHasta(request.getParameter("BusFechaHasta"));	
			 //beanPlanMejora.planMejora.setNoIdElavoro(beanSession.usuario.getIdentificacion());	
			 
			 
			 if(request.getParameter("EstadoEvento").equals("00"))
			       beanPlanMejora.planMejora.setEstadoEvento("%");	
			 else  beanPlanMejora.planMejora.setEstadoEvento(request.getParameter("EstadoEvento"));
			 
 			 if(request.getParameter("BusIdEvento").equals(""))// un evento especifico o todos
			    beanPlanMejora.planMejora.setIdEvento("%");
			 else
   			    beanPlanMejora.planMejora.setIdEvento(request.getParameter("BusIdEvento").trim());
				
			 if(request.getParameter("solo_mis").equals("true"))
			   beanPlanMejora.planMejora.setNoIdElavoro(beanSession.usuario.getIdentificacion());	
			 else
			   beanPlanMejora.planMejora.setNoIdElavoro("%");				


			 beanPlanMejora.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlanMejora.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
			 if(request.getParameter("sidx").equals("DescriProceso")) beanPlanMejora.setSidx("2");  //indice por el campo por el cual se ordena^			 
			 if(request.getParameter("sidx").equals("descEvento")) beanPlanMejora.setSidx("4");  
			 if(request.getParameter("sidx").equals("riesgo")) beanPlanMejora.setSidx("7");  
			 if(request.getParameter("sidx").equals("costo")) beanPlanMejora.setSidx("8");  
			 if(request.getParameter("sidx").equals("volumen")) beanPlanMejora.setSidx("9");  			  
			 if(request.getParameter("sidx").equals("prioriza")) beanPlanMejora.setSidx("5");  
			 if(request.getParameter("sidx").equals("costoTot")) beanPlanMejora.setSidx("6"); 
			 if(request.getParameter("sidx").equals("fechaIniPlanAccion")) beanPlanMejora.setSidx("19"); 			 
			 if(request.getParameter("sidx").equals("modificable")) beanPlanMejora.setSidx("20"); 	//18
			 if(request.getParameter("sidx").equals("porcentCumpl")) beanPlanMejora.setSidx("21");
			 
//			 beanPlanMejora.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlanMejora.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlanMejora.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlanMejora.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlanMejora.asignaBuscarPag(request.getParameter("accion"));			

			 //System.out.println("contador"+resultaux.size());
			 PlanMejoraVO parametro=new PlanMejoraVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanPlanMejora.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(PlanMejoraVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[111]]></cell>  
                                  <cell><![CDATA[No]]></cell>
                                  <cell><![CDATA[M]]></cell>
                                  <cell><![CDATA[<%= parametro.getContador() %>]]></cell>                            
                                  <cell><![CDATA[<%= parametro.getIdProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescripcionProceso() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdEvento() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescripEvento() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getIdTipoPlan() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescrTipoPlan() %>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getRiesgo() %>]]></cell>                                                                                                    
                                  <cell><![CDATA[<%= parametro.getCosto() %>]]></cell>                                                                                                    
                                  <cell><![CDATA[<%= parametro.getVolumen() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getRiesgo()*parametro.getCosto()*parametro.getVolumen()%>]]></cell>                                    
                                  <cell><![CDATA[<%= parametro.getTotCostoPlan() %>]]></cell>   
                                  <cell><![CDATA[<%= parametro.getFechaEventoCreacion() %>]]></cell>                                                                                                                                                                                                                                      
                                  <cell><![CDATA[<%= parametro.getNoIdElavoro() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getIdPlanAccion() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getFechaIniPlanAccion() %>]]></cell>    
                                  <cell><![CDATA[<%= parametro.getPorcentejec() %>]]></cell>                                      
                                  <cell><![CDATA[<%= parametro.getEstadoEvento()%>]]></cell>                                                                  
                                  <cell><![CDATA[<%= parametro.getModificable()%>]]></cell>          
                                  <cell><![CDATA[<%= parametro.getSeguimientoEficaz()%>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getSeguimientoMeta()%>]]></cell>                                                            
                            </row>                         
					<%	
			}														
    }
    else if(request.getParameter("accion").equals("listTareasPlanAccion")){	      
			 beanPlanMejora.planAccion.setIdPlanAccion(request.getParameter("IdEvento"));
			 beanPlanMejora.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlanMejora.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
//			 if(request.getParameter("sidx").equals("IdEvento")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 //beanAdmin.setSidx("1");
			 
			 beanPlanMejora.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlanMejora.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlanMejora.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlanMejora.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlanMejora.asignaBuscarPag(request.getParameter("accion"));	
			 //System.out.println("contador"+resultaux.size());
			 TareaPlanAccionVO parametro=new TareaPlanAccionVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanPlanMejora.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(TareaPlanAccionVO)resultaux.get(i);
				i++;		
					%>					
                    <row id = '<%= i%>'> 
                          <cell><![CDATA[<%= i %>]]></cell>                     
                          <cell><![CDATA[<%= parametro.getTipoTarea() %>]]></cell> 
                          <cell><![CDATA[<%= parametro.getIdTarea() %>]]></cell>
                          <cell><![CDATA[<%= parametro.getCaracter() %>]]></cell>  
                          <cell><![CDATA[<%= parametro.getDescCaracter() %>]]></cell>
                          <cell><![CDATA[<%= parametro.getDescTarea() %>]]></cell>  
                          <cell><![CDATA[<%= parametro.getFechaTarea() %>]]></cell>                                  
                          <cell><![CDATA[<%= parametro.getIdRolResponsable() %>]]></cell>                                  
                          <cell><![CDATA[<%= parametro.getDescRolResponsable() %>]]></cell>  
                          <cell><![CDATA[<%= parametro.getCostosAdd() %>]]></cell>     
                          <cell><![CDATA[<%= parametro.getEstado()%>]]></cell>                                                                                           
                          <cell><![CDATA[<%= parametro.getDescripcionTarea()%>]]></cell>                                                                                                                     
                    </row> 
					<%	
			}														
    }
    else if(request.getParameter("accion").equals("tareasAjenasAsociar")){	      
			 beanPlanMejora.planAccion.setIdPlanAccion(request.getParameter("IdPlanAccion"));
			 beanPlanMejora.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlanMejora.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
//			 if(request.getParameter("sidx").equals("IdEvento")) beanAdmin.setSidx("1");  //indice por el campo por el cual se ordena^
			 //beanAdmin.setSidx("1");
			 
			 beanPlanMejora.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlanMejora.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlanMejora.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlanMejora.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlanMejora.asignaBuscarPag(request.getParameter("accion"));	
			 //System.out.println("contador"+resultaux.size());
			 TareaPlanAccionVO parametro=new TareaPlanAccionVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanPlanMejora.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(TareaPlanAccionVO)resultaux.get(i);
				i++;		
					%>					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[<%= parametro.getIdPlanAccion() %>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getTipoTarea() %>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIdTarea() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getCaracter() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getDescCaracter() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescTarea() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getFechaTarea() %>]]></cell>                                  
                                  <cell><![CDATA[<%= parametro.getIdRolResponsable() %>]]></cell>                                  
                                  <cell><![CDATA[<%= parametro.getDescRolResponsable() %>]]></cell>                                  
                            </row> 
					<%	
			}														
    }	
    else if(request.getParameter("accion").equals("listAsociarEventosTareas")){	      
			 beanPlanMejora.planAccion.setIdPlanAccion(request.getParameter("IdPlanAccion"));
			 beanPlanMejora.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlanMejora.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 beanPlanMejora.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlanMejora.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlanMejora.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlanMejora.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlanMejora.asignaBuscarPag(request.getParameter("accion"));	
			 //System.out.println("contador"+resultaux.size());
			 TareaPlanAccionVO parametro=new TareaPlanAccionVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanPlanMejora.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(TareaPlanAccionVO)resultaux.get(i);
				i++;		
					%>					
                      <row id = '<%= i%>'> 
                            <cell><![CDATA[<%= parametro.getIdPlanAccion() %>]]></cell>                             
                            <cell><![CDATA[<%= parametro.getTipoTarea() %>]]></cell> 
                            <cell><![CDATA[<%= parametro.getIdTarea() %>]]></cell>
                            <cell><![CDATA[<%= parametro.getCaracter() %>]]></cell>  
                            <cell><![CDATA[<%= parametro.getDescCaracter() %>]]></cell>
                            <cell><![CDATA[<%= parametro.getDescTarea() %>]]></cell>  
                            <cell><![CDATA[<%= parametro.getFechaTarea() %>]]></cell>                                  
                            <cell><![CDATA[<%= parametro.getIdRolResponsable() %>]]></cell>                                  
                            <cell><![CDATA[<%= parametro.getDescRolResponsable() %>]]></cell>                                  
                      </row> 
					<%	
			}														
    }		
    else if(request.getParameter("accion").equals("listEspinaPescado")){	  
			 beanPlanMejora.espinaPescado.setIdPlanAccion(request.getParameter("IdEvento"));
			 beanPlanMejora.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlanMejora.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
			 
			 beanPlanMejora.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlanMejora.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlanMejora.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlanMejora.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlanMejora.asignaBuscarPag(request.getParameter("accion"));	
			 //System.out.println("contador"+resultaux.size());
			 EspinaPescadoVO parametro=new EspinaPescadoVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanAdmin.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(EspinaPescadoVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%=i%>'> 
                                  <cell><![CDATA[0]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIdFactor()%>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescFactor()%>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIdCriterio()%>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescCriterio()%>]]></cell> 
                                  <cell><![CDATA[<%= parametro.getDescAnalisisFactor()%>]]></cell>
                            </row> 
                        
					<%	
			}														
    }		
    else if(request.getParameter("accion").equals("listPersonasPlanAccionGral")){	  
			 beanPlanMejora.planAccion.setIdPlanAccion(request.getParameter("IdEvento"));
			 beanPlanMejora.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlanMejora.setRows(request.getParameter("rows"));  //limite de filas por paginas			 
			 
			 beanPlanMejora.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlanMejora.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlanMejora.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlanMejora.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlanMejora.asignaBuscarPag(request.getParameter("accion"));	
			 //System.out.println("contador"+resultaux.size());
			 PersonasPlanAccionVO parametro=new PersonasPlanAccionVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanAdmin.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(PersonasPlanAccionVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[0]]></cell> 
                                  <cell><![CDATA[<%= parametro.getIdPersona()%>]]></cell>
                                  <cell><![CDATA[<%= parametro.getDescPersona() %>]]></cell> 
                            </row> 
                        
					<%	
			}														
    }		
	else if(request.getParameter("accion").equals("listSeguimientoPlanAccion")){	/*planes de acreditacion 
             beanPlanMejora.planAccion.setIdPlanAccion(request.getParameter("lblIdPlanAccion"));			   
			 beanPlanMejora.setPage(request.getParameter("page"));  // paginas que se pide
			 beanPlanMejora.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
			 if(request.getParameter("sidx").equals("codigo")) beanPlanMejora.setSidx("1");  //indice por el campo por el cual se ordena^
			 else beanPlanMejora.setSidx("2");
			 
			 beanPlanMejora.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanPlanMejora.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanPlanMejora.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanPlanMejora.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanPlanMejora.planAccion.buscarListadoSeguimientoPlanAccion();	
			 OportuMejoraVO parametro=new OportuMejoraVO();	
			 int i=0, j=0,ban=1;
			 %>
                        <page><%=beanPlanMejora.getPagina()%></page> 
                        <total><%=totalpages%></total> 
                        <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(OportuMejoraVO)resultaux.get(i);
				i++;
					%> 					
                        <row id = '<%= i%>'> 
                              <cell><![CDATA[<%= parametro.getIdTarea() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getDescTarea() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.getSeguimTarea() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getSeguimientoCumple() %>]]></cell>                                                                                            
                              <cell><![CDATA[<%= parametro.getIdElaboroSeguimTarea()%>]]></cell>
                              <cell><![CDATA[<%= parametro.getNombreComplElaboroSeguimTarea()%>]]></cell>
                        </row> 
					<%	
			}														
    }	
	
	*/
		

	//odontologia fin
	//Aqui uno nuevo
}
	
	//System.out.println("4");
%>

 	  
 	 
 </rows>            
<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Clinica.Presentacion.*" %>
    <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
	<%@ page import = "java.text.NumberFormat" %>
    <%@ page import = "Sgh.Utilidades.*" %>

    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanReportes" class="Clinica.Reportes.ControlReportes" scope="session" /> <!-- instanciar bean de session -->    
<rows>
<%
   Fecha fecha = new Fecha();
   beanAdmin.setCn(beanSession.getCn());
   beanReportes.setCn(beanSession.getCn());   
   java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
   java.util.Date date = cal.getTime();
   java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
   SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");	
   NumberFormat nf = NumberFormat.getInstance(Locale.US);
   DecimalFormat df = (DecimalFormat)nf;
   df.applyPattern("###,##0.00");
   ArrayList resultaux=new ArrayList();
   ArrayList resultDocs=new ArrayList();
   if(request.getParameter("accion")!=null ){
    
	//	---------------------------------------------------------------------------------------- buscar ventanas jquery	
	
    if(request.getParameter("accion").equals("reportes")){
		
			 if(request.getParameter("idProceso").equals("00"))
			       beanReportes.reportes.setIdProceso("%");
			 else  beanReportes.reportes.setIdProceso(request.getParameter("idProceso"));		

			 if(request.getParameter("txtBusNomReporte").equals(""))
			       beanReportes.reportes.setNombre("%");
			 else  beanReportes.reportes.setNombre("%"+request.getParameter("txtBusNomReporte")+"%");			   
			 
			 if(request.getParameter("idTipo").equals("00"))
			       beanReportes.reportes.setIdTipo("%");
			 else  beanReportes.reportes.setIdTipo(request.getParameter("idTipo"));			 

			 beanReportes.setPage(request.getParameter("page"));  // paginas que se pide
			 beanReportes.setRows(request.getParameter("rows"));  //limite de filas por paginas

			 
//			 beanReportes.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanReportes.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 totalcount = (int)beanReportes.asignaContar(request.getParameter("accion"));
			 totalpages = (int)beanReportes.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanReportes.asignaBuscarPag(request.getParameter("accion"));			

			 //System.out.println("contador"+resultaux.size());
			 ReportesVO parametro=new ReportesVO();	
			 int i=0, j=0,ban=1;
			 %>
			                <page><%=beanReportes.getPagina()%></page> 
                            <total><%=totalpages%></total> 
                            <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(ReportesVO)resultaux.get(i);
				i++;
					%> 					
                            <row id = '<%= i%>'> 
                                  <cell><![CDATA[<%= parametro.getId() %>]]></cell>                                  
                                  <cell><![CDATA[<%= parametro.getNomProceso() %>]]></cell>                                                           
                                  <cell><![CDATA[<%= parametro.getNombre() %>]]></cell>
                                  <cell><![CDATA[<%= parametro.getExplicacion() %>]]></cell>                                    
                                  <cell><![CDATA[<%= parametro.getDesTipo() %>]]></cell>  
                                  <cell><![CDATA[<%= parametro.getCantColumnas() %>]]></cell>  
                            </row>                         
					<%	
			}														
    }	
	if(request.getParameter("accion").equals("listadoReporte")){
		beanReportes.reportes.setId(request.getParameter("id"));
        beanReportes.reportes.setFechaDesde(request.getParameter("fechaDesde"));	
        beanReportes.reportes.setFechaHasta(request.getParameter("fechaHasta"));	
	    
		beanReportes.reportes.setCantFiltros(  Integer.parseInt(request.getParameter("cantFiltros") )); 				
        beanReportes.reportes.setParametrosFiltros(request.getParameter("parametrosFiltros")); 		

        
		resultaux=(ArrayList)beanReportes.reportes.buscarDatosDelReporte();	
	   
		ReportesVO parametro=new ReportesVO();									
		int i=0;
		 while(i<resultaux.size()){
				parametro=(ReportesVO)resultaux.get(i);
				   i++;	
			%>
                       <c1><![CDATA[<%=parametro.getC1()%>]]></c1>   

			<%		
			
		}
	}	
	if(request.getParameter("accion").equals("listadoReporteExcel")){
		beanReportes.reportes.setId(request.getParameter("id"));
        beanReportes.reportes.setParametro1(request.getParameter("parametro1"));	
     //   beanReportes.reportes.setFechaHasta(request.getParameter("fechaHasta"));	
        
		resultaux=(ArrayList) beanReportes.reportes.buscarDatosDelReporteParametros();	
	   
		ReportesVO parametro=new ReportesVO();									
		int i=0;
		 while(i<resultaux.size()){
				parametro=(ReportesVO)resultaux.get(i);
				   i++;	
			%>
                       <c1><![CDATA[<%=parametro.getC1()%>]]></c1>   

			<%		
			
		}
	}		

	//Aqui uno nuevo
}
	
	//System.out.println("4");
%>

 	  
 	 
 </rows>            
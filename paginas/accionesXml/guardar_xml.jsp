<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Clinica.Presentacion.*" %>
	<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
	<%@ page import = "java.text.NumberFormat" %>
    <%@ page import = "Sgh.Utilidades.*" %>

    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanEventoA" class="Clinica.GestionarEvento.ControlEventos" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanPlanMejora" class="Clinica.GestionarPlanMejora.ControlPlanMejora" scope="session" /> <!-- instanciar bean de ControlPlanMejora -->
    <jsp:useBean id="beanSuministros" class="Clinica.GestionSuministros.ControlSuministros" scope="session" /> <!-- instanciar bean de ControlSuministros -->        
    <jsp:useBean id="beanIndicador" class="Clinica.Indicadores.ControlIndicador" scope="session" /> <!-- instanciar bean de session -->            
 <raiz>	  	


<% 

   Fecha fecha = new Fecha();
   beanAdmin.setCn(beanSession.getCn());
   beanEventoA.setCn(beanSession.getCn());
   beanPlanMejora.setCn(beanSession.getCn());  
   beanSuministros.setCn(beanSession.getCn());  
   beanIndicador.setCn(beanSession.getCn());   	   


   java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
   java.util.Date date = cal.getTime();
   java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
   SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");	
   NumberFormat nf = NumberFormat.getInstance(Locale.US);
   DecimalFormat df = (DecimalFormat)nf;
   df.applyPattern("###,##0.00");
   ArrayList resultaux=new ArrayList();
   ArrayList resultDocs=new ArrayList();

    boolean resultado=false;   
    if(request.getParameter("accion")!=null ){   
	
	  if(request.getParameter("accion").equals("usuario")){  
		 
		 beanAdmin.usuario.setRolUsuarioSession(beanSession.usuario.getRolUsuarioSession());		 
		 beanAdmin.usuario.setTipoId(request.getParameter("tipoId"));
		 beanAdmin.usuario.setIdentificacion(request.getParameter("id"));  
		 beanAdmin.usuario.setLogin(request.getParameter("login"));
		 beanAdmin.usuario.setContrasena(request.getParameter("contrasena"));  
		 beanAdmin.usuario.setRol(request.getParameter("rol"));
		 beanAdmin.usuario.setEstado(request.getParameter("estado"));  		 
		 resultado=beanAdmin.asignaGuardar(request.getParameter("accion"));
		 
	 }else if( request.getParameter("accion").equals("reportarNoConformidad") ){ 
			 beanAdmin.noConformidad.setIdProceso(request.getParameter("idProceso"));
			 beanAdmin.noConformidad.setIsoIdNoConformidad(request.getParameter("idNoConformidades"));		
			 beanAdmin.noConformidad.setIsoNoConformidadArea(request.getParameter("AreaEvento"));
			 beanAdmin.noConformidad.setISOEventoDescripcion(request.getParameter("DescripEvento"));
			 beanAdmin.noConformidad.setIsoFechaInicio(request.getParameter("FechaEvento"));	
			 beanAdmin.noConformidad.setIsoHoraInicio(request.getParameter("HoraEvent"));
			 beanAdmin.noConformidad.setIsoMinutoInicio(request.getParameter("MinutEvent"));				 
			 beanAdmin.noConformidad.setNoIdElavoro(beanSession.usuario.getIdentificacion());
			 beanAdmin.noConformidad.setIsoNoConformidadPosibleTratam(request.getParameter("DescripEventoPosTrata"));		
			 resultado=beanAdmin.asignaGuardar(request.getParameter("accion"));
	  }
	  else if( request.getParameter("accion").equals("administrarNoConformidad") ){ 
			 beanAdmin.noConformidad.setIdISO(request.getParameter("IdEvento"));
			 beanAdmin.noConformidad.setIsoIdNoConformidad(request.getParameter("idNoConformidades"));				 
			 beanAdmin.noConformidad.setISOEventoDesignacion(request.getParameter("ConcepNoConforCalidad"));	//calidad
			 beanAdmin.noConformidad.setIsoResponsable(request.getParameter("ResponsaDestinoEvento"));
//			 beanAdmin.noConformidad.setIdISORef(request.getParameter("IdEvento"));// el id de la no conformidad
		     beanAdmin.noConformidad.setNoIdElavoro(beanSession.usuario.getIdentificacion());
			 
//			 beanAdmin.noConformidad.setIsoIdNoConformidad(request.getParameter("idNoConformidades"));				 
//			 
			 resultado=beanAdmin.asignaGuardar(request.getParameter("accion"));
	  }
	  
  	  else if(request.getParameter("accion").equals("listPlanAccion")){ 
            beanAdmin.noConformidad.setIdISORef(request.getParameter("IdEventoRef"));
            beanAdmin.noConformidad.setIsoIdTipoISOEvento(request.getParameter("idTipoPlanAccion"));
			beanAdmin.noConformidad.setISOEventoDesignacion(request.getParameter("AnalisisCausas"));
			beanAdmin.noConformidad.setIsoNoConformidadInfoAdicional(request.getParameter("MetaPlanAccion"));
			beanAdmin.noConformidad.setIsoFechaIniPlanAccion(request.getParameter("FechaIniPlanAccion"));
			beanAdmin.noConformidad.setNoIdElavoro(beanSession.usuario.getIdentificacion());
			String[] examenes= request.getParameter("examenes").split(",-");
			String examenes2= request.getParameter("examenes2");
            resultado=beanAdmin.noConformidad.crearDocumentoYlistPlanAccion(examenes,examenes2);	 //examenes2= personas
	  }
  	  else if(request.getParameter("accion").equals("listTareasPlanAccion")){ 

            beanPlanMejora.planAccion.setIdEvento(request.getParameter("IdEvento"));	  
            beanPlanMejora.planAccion.setIdPlanAccion(request.getParameter("lblIdPlanAccion"));
            beanPlanMejora.planAccion.setIdTipoPlan(request.getParameter("idTipoPlanAccion"));			
			beanPlanMejora.planAccion.setRiesgoNoHacerlo(request.getParameter("RiesgoNoHacerlo"));
			beanPlanMejora.planAccion.setBeneficiosHacerlo(request.getParameter("BeneficiosHacerlo"));			
			beanPlanMejora.planAccion.setFechaIniPlanAccion(request.getParameter("FechaIniPlanAccion"));			
			beanPlanMejora.planAccion.setMeta(request.getParameter("MetaPlanAccion"));
			
			beanPlanMejora.planAccion.setIdResponsable(request.getParameter("idResponsable"));			
			beanPlanMejora.planAccion.setConceptoGral(request.getParameter("ConceptoGral"));						
			beanPlanMejora.planAccion.setIdElaboro(beanSession.usuario.getIdentificacion());
			
			
			String[] tareas= request.getParameter("tareas").split(",-");
			String personas= request.getParameter("personas");
			String[] espinaPescado= request.getParameter("espinaPescado").split(",-");						
            resultado=beanPlanMejora.planAccion.crearPlanAccionTareasYPersonas(tareas,personas,espinaPescado);	 //examenes2= personas
	  }	  
  	  else if(request.getParameter("accion").equals("listPlanAccionSeguimiento")){ 
	        beanAdmin.noConformidad.setIdISO(request.getParameter("IdEvento")); // Si es "" es primera vez y sera insert or update
            beanAdmin.noConformidad.setIdISORef(request.getParameter("IdEventoRef"));
            beanAdmin.noConformidad.setIsoIdTipoISOEvento(request.getParameter("Eficaz"));
			beanAdmin.noConformidad.setISOEventoDescripcion(request.getParameter("SeguimMetaPlanAccion"));
			//beanAdmin.noConformidad.setIsoFechaIniPlanAccion(request.getParameter("FechaIniPlanAccion"));
			beanAdmin.noConformidad.setNoIdElavoro(beanSession.usuario.getIdentificacion());
			String[] examenes= request.getParameter("examenes").split(",-");
            resultado=beanAdmin.noConformidad.crearDocumentoYlistPlanAccionSeguimiento(examenes);				
	  }

  	  else if(request.getParameter("accion").equals("listAsociarEventos")){ 
            beanAdmin.noConformidad.setIdISORef(request.getParameter("IdEvento"));// el id de la no conformidad que tiene asociado las siguientes eventos			
			String examenes= request.getParameter("examenes");
			resultado=beanAdmin.noConformidad.crearEventosAsociadosNC(examenes);			
	  }
  	  else if(request.getParameter("accion").equals("listPersonasPlanAccion")){ 
            beanAdmin.noConformidad.setIdISORef(request.getParameter("IdEventoRef"));// el id de la no conformidad que tiene asociado las siguientes eventos			
			String examenes= request.getParameter("examenes");
			resultado=beanAdmin.noConformidad.adicionaPersonasAsociadosDocumento(examenes);			
	  }
  	  else if(request.getParameter("accion").equals("listAsociarEventosTareas")){ 
            beanPlanMejora.planAccion.setIdPlanAccion(request.getParameter("IdPlanAccion"));
			beanPlanMejora.planAccion.setIdElaboro(beanSession.usuario.getIdentificacion());
			String[] tareasAjenas= request.getParameter("tareas").split(",-");
            resultado=beanPlanMejora.planAccion.crearTareasAgenas(tareasAjenas);	
	  }		  
	  /*****************************************************inicio Eventos adversos*/
	  if( request.getParameter("accion").equals("reportarEventoAdverso") ){ 
             beanEventoA.eventoAdverso.setIdPaciente(request.getParameter("idPaciente"));
             beanEventoA.eventoAdverso.setIdUnidadPertenece(request.getParameter("UnidadPertenece"));			 
             beanEventoA.eventoAdverso.setIdProceso(request.getParameter("idProceso"));
			 beanEventoA.eventoAdverso.setIdEventoProceso(request.getParameter("EAProceso"));		
			 beanEventoA.eventoAdverso.setIdArea(request.getParameter("AreaEvento"));
			 beanEventoA.eventoAdverso.setDescripEvento(request.getParameter("DescripEvento"));
			 beanEventoA.eventoAdverso.setFechaEvento(request.getParameter("FechaEvento"));	
			 beanEventoA.eventoAdverso.setTratamiento(request.getParameter("Tratamiento"));					 
/*			 beanEventoA.eventoAdverso.setIsoHoraInicio(request.getParameter("HoraEvent"));
			 beanEventoA.eventoAdverso.setIsoMinutoInicio(request.getParameter("MinutEvent"));				 */
			 beanEventoA.eventoAdverso.setIdElavoro(beanSession.usuario.getIdentificacion());

			 resultado=beanEventoA.asignaGuardar(request.getParameter("accion"));
	  }
	  else if( request.getParameter("accion").equals("administrarEventoAdverso") ){ 
	      /*   beanEventoA.eventoAdverso.setIdProceso(request.getParameter("idProceso"));
			 beanEventoA.eventoAdverso.setIdISO(request.getParameter("IdEvento"));
			 beanEventoA.eventoAdverso.setIsoIdNoConformidad(request.getParameter("idNoConformidades"));				 
		     beanEventoA.eventoAdverso.setNoIdElavoro(beanSession.usuario.getIdentificacion());
			 resultado=beanEventoA.asignaGuardar(request.getParameter("accion"));*/
	  }	  
  	  else if(request.getParameter("accion").equals("listFactContribu")){ 
            beanEventoA.eventoAdverso.setIdEvento(request.getParameter("IdEvento"));
		    beanEventoA.eventoAdverso.setIdBarreraYDefensa(request.getParameter("BarreraYDefensa"));	
		    beanEventoA.eventoAdverso.setIdAccionInsegura(request.getParameter("AccionInsegura"));	
			beanEventoA.eventoAdverso.setIdAmbito(request.getParameter("Ambito"));
		    beanEventoA.eventoAdverso.setIdPrevenible(request.getParameter("Prevenible"));			
		    beanEventoA.eventoAdverso.setIdOrganizCultura(request.getParameter("OrganizCultura"));	
		    beanEventoA.eventoAdverso.setIdElavoro(beanSession.usuario.getIdentificacion());			
			
			String origenFactores= request.getParameter("origenFactores");
			String[] oportMejora= request.getParameter("oportMejora").split(",-");	
			resultado=beanEventoA.eventoAdverso.adicionGestionDelEA(origenFactores,oportMejora);		
	  }
	  else if(request.getParameter("accion").equals("listSeguimientoOportMejora")){ 
            beanEventoA.eventoAdverso.setIdEvento(request.getParameter("IdEvento"));
			beanEventoA.eventoAdverso.setIdElavoro(beanSession.usuario.getIdentificacion());	
			String[] tareaSegui= request.getParameter("tareas").split(",-");
            resultado=beanEventoA.eventoAdverso.seguimientoTareasOportunidadDeMejora(tareaSegui);	
	  }
	  else if(request.getParameter("accion").equals("listNivelResponsabili")){ 
            beanEventoA.eventoAdverso.setIdEvento(request.getParameter("IdEvento"));
            beanEventoA.eventoAdverso.setConclusiones(request.getParameter("Conclusiones"));			
			
			beanEventoA.eventoAdverso.setIdElavoro(beanSession.usuario.getIdentificacion());	
			String[] tareaNivel= request.getParameter("tareas").split(",-");
            resultado=beanEventoA.eventoAdverso.guardarResumenYConclusiones(tareaNivel);	
	  }	  
	  
	  /*****************************************************inicio plan de mejora acreditacion**********************************/	  
	  if( request.getParameter("accion").equals("administrarPlanMejora") ){ 
			 beanPlanMejora.planMejora.setIdProceso(request.getParameter("idProceso"));
			 beanPlanMejora.planMejora.setAspectoMejorar(request.getParameter("AspectoMejorar"));
			 beanPlanMejora.planMejora.setTipo(request.getParameter("TipoPlan"));	
			 beanPlanMejora.planMejora.setRiesgo(Integer.parseInt(request.getParameter("Riesgo")));				
			 beanPlanMejora.planMejora.setCosto(Integer.parseInt(request.getParameter("Costo")));				
			 beanPlanMejora.planMejora.setVolumen(Integer.parseInt(request.getParameter("Volumen")));			 
			 beanPlanMejora.planMejora.setNoIdElavoro(beanSession.usuario.getIdentificacion());
			 resultado=beanPlanMejora.asignaGuardar(request.getParameter("accion"));
	  }
	  else if(request.getParameter("accion").equals("listSeguimientoPlanAccion")){ 
            beanPlanMejora.planAccion.setIdPlanAccion(request.getParameter("IdPlanAccion"));
            beanPlanMejora.planAccion.setSeguimientoEficaz(request.getParameter("seguimientoEficaz"));
            beanPlanMejora.planAccion.setSeguimientoMeta(request.getParameter("seguimientoMeta"));				
			beanPlanMejora.planAccion.setIdElaboro(beanSession.usuario.getIdentificacion());	
			

			
			String[] tareaSegui= request.getParameter("tareas").split(",-");
            resultado=beanPlanMejora.planAccion.seguimientoTareasPlanAccionYMeta(tareaSegui);	
	  }
	  
	  /*****************************************************inicio plan de mejora acreditacion desde evento adverso**********************************/	  
	  else if( request.getParameter("accion").equals("administrarPlanMejoraEA") ){
			 
			 beanPlanMejora.planMejora.setIdEvento(request.getParameter("IdEvento"));
			 beanPlanMejora.planMejora.setIdProceso(request.getParameter("idProceso"));
			 beanPlanMejora.planMejora.setAspectoMejorar(request.getParameter("AspectoMejorar"));
			 beanPlanMejora.planMejora.setTipo(request.getParameter("TipoPlan"));	
			 beanPlanMejora.planMejora.setRiesgo(Integer.parseInt(request.getParameter("Riesgo")));				
			 beanPlanMejora.planMejora.setCosto(Integer.parseInt(request.getParameter("Costo")));				
			 beanPlanMejora.planMejora.setVolumen(Integer.parseInt(request.getParameter("Volumen")));			 
			 beanPlanMejora.planMejora.setNoIdElavoro(beanSession.usuario.getIdentificacion());
			 resultado=beanPlanMejora.asignaGuardar(request.getParameter("accion"));
	  }
	  else if( request.getParameter("accion").equals("relacionEventoOM") ){
			 
			 beanPlanMejora.planMejora.setIdEvento(request.getParameter("IdEvento"));
			 beanPlanMejora.planMejora.setTipoEvento(request.getParameter("TipoEvento"));			 
			 beanPlanMejora.planMejora.setIdProceso(request.getParameter("idProceso"));
			 beanPlanMejora.planMejora.setAspectoMejorar(request.getParameter("AspectoMejorar"));
			 beanPlanMejora.planMejora.setTipo(request.getParameter("TipoPlan"));	
			 beanPlanMejora.planMejora.setRiesgo(Integer.parseInt(request.getParameter("Riesgo")));				
			 beanPlanMejora.planMejora.setCosto(Integer.parseInt(request.getParameter("Costo")));				
			 beanPlanMejora.planMejora.setVolumen(Integer.parseInt(request.getParameter("Volumen")));			 
			 beanPlanMejora.planMejora.setNoIdElavoro(beanSession.usuario.getIdentificacion());
			 resultado=beanPlanMejora.asignaGuardar(request.getParameter("accion"));
	  }	  
	  /*****************************************************inicio suministros **********************************/	  
	  else if( request.getParameter("accion").equals("adminItem") ){ 
			  beanSuministros.item.setNombre(request.getParameter("NomItem").trim());	
			  beanSuministros.item.setCodUnidMedida(request.getParameter("UnidMed"));	
			  beanSuministros.item.setCodServicio(request.getParameter("Servicio"));	
			  beanSuministros.item.setCodGrupo(request.getParameter("GrupoItem"));		
			  beanSuministros.item.setCodClase(request.getParameter("Clase"));	
			  beanSuministros.item.setObservacion(request.getParameter("Observa"));	
			  beanSuministros.item.setEstado(request.getParameter("Estado"));	
			  beanSuministros.item.setIdElaboro(beanSession.usuario.getIdentificacion());			  
              resultado=beanSuministros.asignaGuardar(request.getParameter("accion"));
	  }
	  else if( request.getParameter("accion").equals("valorConteoItem") ){ 

		  beanSuministros.item.setCodigo(request.getParameter("codItem"));	
		  
		  beanSuministros.item.setIdElaboro(beanSession.usuario.getIdentificacion());
		  
		  resultado=beanSuministros.item.agregarConteoItemInventario(Integer.parseInt(request.getParameter("ValorConteo")));
	  }	  
	  
	  /*****************************************************fin suministros **********************************/	 
	  /***************************************************** indicadores **********************************/	  
	 else if( request.getParameter("accion").equals("fichaTecnica") ){ 

//		  beanIndicador.indicadorFicha.setId(Integer.parseInt(request.getParameter("Riesgo")));  cmbProceso
		  beanIndicador.indicadorFicha.setIdTipo(request.getParameter("cmbTipo"));                                                                                                  
		  beanIndicador.indicadorFicha.setNombre(request.getParameter("txtNombre"));
		  beanIndicador.indicadorFicha.setIdProceso(request.getParameter("cmbProceso"));    		  
		  beanIndicador.indicadorFicha.setObjetivo(request.getParameter("txtObjetivo"));                                   
		  beanIndicador.indicadorFicha.setEstado(request.getParameter("cmbEstado")); 
											
		  beanIndicador.indicadorFicha.setIdPeriodicidad(Integer.parseInt(request.getParameter("cmbPeriodicidad"))); 
		  beanIndicador.indicadorFicha.setFechaVigenciaIni(request.getParameter("txtFechaVigenciaIni")); 
		  beanIndicador.indicadorFicha.setDescripcionResponsables(request.getParameter("txtDescripcionResponsable")); 
		  beanIndicador.indicadorFicha.setIdUnidadMedida(Integer.parseInt(request.getParameter("cmbUnidadMedida"))); 
		  beanIndicador.indicadorFicha.setLineaBase(request.getParameter("txtLineaBase")); 
		  beanIndicador.indicadorFicha.setMeta(request.getParameter("txtMeta")); 
		 // beanIndicador.indicadorFicha.setDenominadorRequerido(request.getParameter("accion")); 
		  beanIndicador.indicadorFicha.setDescripcionDenominador(request.getParameter("txtDescripcionDenominador")); 
		  beanIndicador.indicadorFicha.setDescripcionNumerador(request.getParameter("txtDescripcionNumerador")); 
		  beanIndicador.indicadorFicha.setIdOrigenInfo(Integer.parseInt(request.getParameter("cmbOrigenInfo"))); 
		  beanIndicador.indicadorFicha.setInfoAdicional(request.getParameter("txtInfoAdicional")); 
		  //beanIndicador.indicadorFicha.setIdModoComparacion(request.getParameter("accion")); 
		  beanIndicador.indicadorFicha.setIdElaboro(beanSession.usuario.getIdentificacion());  
		  resultado=beanIndicador.asignaCrear(request.getParameter("accion"));
	  }
	  
	  /***************************************************** fin indicadores **********************************/		  
	  
	  

//	  aqui un nuevo
    }	
	
%>

   <respuesta><![CDATA[<%= resultado%>]]></respuesta>
    <accion><![CDATA[<%=request.getParameter("accion")%>]]></accion>		  
 	 
 </raiz>	  	          
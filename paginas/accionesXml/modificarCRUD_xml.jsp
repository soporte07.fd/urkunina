<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Clinica.Presentacion.*" %>
	<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
	<%@ page import = "java.text.NumberFormat" %>
    <%@ page import = "Sgh.Utilidades.*" %>

    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
 <raiz>	  	

<% 
   Fecha fecha = new Fecha();
   beanAdmin.setCn(beanSession.getCn());      

   java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
   java.util.Date date = cal.getTime();
   java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
   SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");	
   NumberFormat nf = NumberFormat.getInstance(Locale.US);
   DecimalFormat df = (DecimalFormat)nf;
   df.applyPattern("###,##0.00");
   ArrayList resultaux=new ArrayList();

   boolean resultado=false;   
   if(request.getParameter("accion")!=null ){   
	  beanAdmin.grilla.setId(request.getParameter("idQuery")); 	  
	  beanAdmin.grilla.setParametros(request.getParameter("parametros")); 
      resultado = beanAdmin.grilla.modificarCRUD();
   }		
%>
   <respuesta><![CDATA[<%= resultado%>]]></respuesta>
   <MsgAlerta><![CDATA[<%= beanSession.cn.getMsgAlerta()%>]]></MsgAlerta>
 </raiz>	  	          
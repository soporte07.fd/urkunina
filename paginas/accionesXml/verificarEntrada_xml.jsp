<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.sql.*,java.math.*,java.util.*" %>
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Sgh.Presentacion.*" %>
	<%@ page import = "Sgh.AdminSeguridad.ControlAdmin" %>
	<%@ page import = "Sgh.GestionPersonal.ControlPersonal" %>
    <%@ page import = "Sgh.GestionAcademica.ControlAcademico" %>

	
    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanAdmin" class="Sgh.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanPersonal" class="Sgh.GestionPersonal.ControlPersonal" scope="session" /> <!-- instanciar bean de control personal -->
	<jsp:useBean id="beanAcademico" class="Sgh.GestionAcademica.ControlAcademico" scope="session" /> <!-- instanciar bean de control academico-->

<%
   beanAcademico.setCn(beanSession.getCn());// con el codigo de la agencia busca sus programas
   boolean result=false;
   beanAcademico.calificaciones.setCodigo(request.getParameter("codigo"));
   beanAcademico.calificaciones.setIdentificacion(request.getParameter("identificacion"));
   result= beanAcademico.verificarDatos();
   

%>

 <raiz>
	  <existe><![CDATA[<%= result %>]]></existe>	  
 </raiz>            
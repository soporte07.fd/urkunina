<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.sql.*,java.math.*,java.util.*" %>
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Clinica.Presentacion.*" %>
	<%@ page import = "Clinica.GestionHistoriaC.ControlHistoriaC" %>
	
    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanHistoriaC" class="Clinica.GestionHistoriaC.ControlHistoriaC" scope="session" /> <!-- instanciar bean de session -->
	
<%
   beanHistoriaC.setCn(beanSession.getCn());// con el codigo del depto busca sus municipios
   ArrayList result=new ArrayList();
   beanHistoriaC.historiaC.setUbicacionArea(request.getParameter("codComboPadre"));
   result=(ArrayList)beanHistoriaC.cargarDatosPaciente();   
%>
 <raiz>
    <% 
 		NoConformidadVO NoConfoVO=new NoConformidadVO();									
		int i=0;
		while(i<result.size()){
		   NoConfoVO=(NoConformidadVO)result.get(i);	
		   i++;	
	 %> 
	 <depto>
 	  <cod><![CDATA[<%= NoConfoVO.getIsoIdNoConformidad() %>]]></cod>
 	  <nom><![CDATA[<%= NoConfoVO.getIsoNoConformidadDescripcion() %>]]></nom>
 	  <defini><![CDATA[<%= NoConfoVO.getIsoNoConformidadDefinicion() %>]]></defini>
 	  <tratam><![CDATA[<%= NoConfoVO.getIsoNoConformidadTratam() %>]]></tratam>            
    </depto>
	<%
        }
	%>
 </raiz>            
<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Clinica.Presentacion.*" %>
	<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
	<%@ page import = "java.text.NumberFormat" %>
    <%@ page import = "Sgh.Utilidades.*" %>

    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<rows>
<%
   Fecha fecha = new Fecha();
   beanAdmin.setCn(beanSession.getCn());
   
   java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
   java.util.Date date = cal.getTime();
   java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
   SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");	
   NumberFormat nf = NumberFormat.getInstance(Locale.US);
   DecimalFormat df = (DecimalFormat)nf;
   df.applyPattern("###,##0.00");
   ArrayList resultaux=new ArrayList();
   ArrayList resultDocs=new ArrayList();
   
   String fechaDesde="", fechaHasta="";
   
   if(request.getParameter("accion")!=null ){

    
   //	---------------------------------------------------------------------------------------- buscar ventanas jquery
	 
	if(request.getParameter("accion").equals("listadoIndicadores")){
		
		
		if(request.getParameter("fechaDesde").equals(""))
			 fechaDesde = "01/01/1900 00:00:00";									 
        else fechaDesde	= request.getParameter("fechaDesde")+" 00:00:00";
		if(request.getParameter("fechaHasta").equals(""))
			 fechaHasta = "01/01/2050 00:00:00";									 
        else fechaHasta	= request.getParameter("fechaHasta")+" 23:59:59";


           beanAdmin.indicador.setFechaDesde(fechaDesde);
           beanAdmin.indicador.setFechaHasta(fechaHasta);
        
		
		if(request.getParameter("ventanaActual").equals("administrarNoConformidad"))
		    resultaux=(ArrayList)beanAdmin.indicador.buscarListadoIndicadores();	
		else if(request.getParameter("ventanaActual").equals("administrarEventoAdverso"))
		    resultaux=(ArrayList)beanAdmin.indicador.buscarListadoIndicadoresEA();	
	   
		IndicadorVO parametro=new IndicadorVO();									
		int i=0;
		 while(i<resultaux.size()){
				parametro=(IndicadorVO)resultaux.get(i);
				   i++;	
			%>
				   <indicador> 
                       <IdISOEvento><![CDATA[<%=parametro.getCodigo()%>]]></IdISOEvento>                   
                       <IdISOEstruct><![CDATA[<%=parametro.getEstructura()%>]]></IdISOEstruct>
                       <IdISOProcesDescrip><![CDATA[<%=parametro.getDescripcion()%>]]></IdISOProcesDescrip>  
                       <ISOCuantas><![CDATA[<%=parametro.getColumna1()%>]]></ISOCuantas>   
                       <reportadas><![CDATA[<%=parametro.getColumna2()%>]]></reportadas>   
                       <asignadas><![CDATA[<%=parametro.getColumna3()%>]]></asignadas>   
                       <cuantosImpacto><![CDATA[<%=parametro.getColumna4()%>]]></cuantosImpacto>   
                       <cuantosPlanes><![CDATA[<%=parametro.getColumna5()%>]]></cuantosPlanes>   
                       <cuantosPlanesCerrad><![CDATA[<%=parametro.getColumna6()%>]]></cuantosPlanesCerrad> 
				   </indicador>
			<%
		}
	}	
	if(request.getParameter("accion").equals("sublistadoIndicadores")){
		 beanAdmin.indicador.setIdProceso(request.getParameter("IdSubEvento"));
		if(request.getParameter("fechaDesde").equals(""))
           beanAdmin.indicador.setFechaDesde("01/01/1900");
		else	
           beanAdmin.indicador.setFechaDesde(request.getParameter("fechaDesde"));
		if(request.getParameter("fechaHasta").equals(""))
           beanAdmin.indicador.setFechaHasta("01/01/2050");
		else	
           beanAdmin.indicador.setFechaHasta(request.getParameter("fechaHasta"));
        
		
		if(request.getParameter("ventanaActual").equals("administrarNoConformidad"))
		    resultaux=(ArrayList)beanAdmin.indicador.buscarListadoSubIndicadores();	
		else if(request.getParameter("ventanaActual").equals("administrarEventoAdverso"))
		    resultaux=(ArrayList)beanAdmin.indicador.buscarListadoSubIndicadoresEA();	
		
	   
		IndicadorVO parametro=new IndicadorVO();									
		int i=0;
		 while(i<resultaux.size()){
				parametro=(IndicadorVO)resultaux.get(i);
				   i++;	
			%>
				   <indicador> 
                       <IdISOEvento><![CDATA[<%=parametro.getCodigo()%>]]></IdISOEvento>                   
                       <IdISOEstruct><![CDATA[<%=parametro.getEstructura()%>]]></IdISOEstruct>
                       <IdISOProcesDescrip><![CDATA[<%=parametro.getDescripcion()%>]]></IdISOProcesDescrip>  
                       <ISOCuantas><![CDATA[<%=parametro.getColumna1()%>]]></ISOCuantas>   
                       <reportadas><![CDATA[<%=parametro.getColumna2()%>]]></reportadas>   
                       <asignadas><![CDATA[<%=parametro.getColumna3()%>]]></asignadas>   
                       <cuantosImpacto><![CDATA[<%=parametro.getColumna4()%>]]></cuantosImpacto>   
                       <cuantosPlanes><![CDATA[<%=parametro.getColumna5()%>]]></cuantosPlanes>   
                       <cuantosPlanesCerrad><![CDATA[<%=parametro.getColumna6()%>]]></cuantosPlanesCerrad> 
				   </indicador>
			<%
		}

	}	
	else if(request.getParameter("accion").equals("antecedentes")){		 // llamado para el jqry para el  listado  
			System.out.println("ya no existe antecedente ahora es tipoAntecedente");
			
	}
	//odontologia fin
	//Aqui uno nuevo
}
	
	//System.out.println("4");
%>
 	 
</rows>            
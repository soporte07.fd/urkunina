<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "Sgh.Utilidades.Sesion" %>


<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->

<table width="90%" align="left" border="0" cellspacing="0" cellpadding="0" style="margin-top:1px; z-index:1500">
    <tr>
        <td valign="top"> <input name="ventanasActivas" id="ventanasActivas" type="hidden" class="nomayusculas"
                tabindex="1" value="" size="25" />
            <label id="lblContrasena" style="display: none;"><%=beanSession.usuario.getContrasena()%></label>
            <label id="lblIdentificacion" style="display: none;"><%=beanSession.usuario.getIdentificacion()%></label>
            <label id="lblIdSede0" style="display: none;"><%=beanSession.usuario.getIdSede()%></label>
            <jsp:include page="menu.jsp" flush="true" />
            <table id="tablaMain" width="100%" height="100%" align="left" cellpadding="0" cellspacing="0"
                bgcolor="#CCCCCC">
                <tr>
                    <td align="center" valign="top">                        
                        <div id="menuAux"
                            style="width:132px; position:absolute; left:9px; z-index:1;  top: 40px; height: 12px; display:none;">
                                <div style="background:#FFFFFF">
                                    <div
                                        style="background:#FC3;  height:100%; font-family:Arial, Helvetica, sans-serif; font-size:10px; "
                                        align="center">
                                        <label id="lblMenuTreeTitle" onclick="javascript:mostrarMenu();" style="color:#193b56;">MEN&Uacute;</label>
                                    </div>
                                </div>
                                <br />                            
                        </div>
                        <div id="menuDesplegable"
                            style="width:132px; position:absolute; left:9px; z-index:1;  top: 40px; height: 1080px;">
                                <div style="background:#FFFFFF">
                                    <div id="divMenuTreeTitle"
                                        style="background:#FC3;  height:100%; font-family:Arial, Helvetica, sans-serif; font-size:20px; "
                                        align="center">
                                        <label id="lblMenuTreeTitle" style="color:#193b56;">MEN&Uacute;</label>
                                    </div>
                                </div>
                                <br />
                            <DIV id="menumenu" style="overflow:auto; height:1777px; top: 40px">
                              <ul id="browser" class="filetree treeview-black" style="font-size:12px"><strong></strong>
                                <% if(beanSession.usuario.preguntarMenu("HIS")){%>
                                                                  
                                
                                <% if(beanSession.usuario.preguntarMenu("PROC") ){%>
                                    <li class="closed"><span class="folderCirugia" title="CITP">CITAS PROCEDIMIENTOS</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("PROC01") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/horarios.jsp")%>', '4-0-0', 'horarios', 'horarios', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="PROC01">Horario Procedimientos</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("PROC02") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="PROC02">Agenda</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("PRO03") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/listaEspera.jsp")%>', '4-0-0', 'listaEsperaCirugia', 'listaEsperaCirugia', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="PRO03">Lista de
                                                            Espera</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("PROC04") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionProcedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="PROC04">Gestión Procedimientos</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("PROC05") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/principalHC.jsp")%>', '4-0-0', 'principalOrdenes', 'principalOrdenes', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="PROC05">Evolucion
                                                            Cirugia</a></span>
                                                </li>
                                             <%}%>          
                                        </ul>
                                    </li>
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("CIT") ){%>
                                    <li class="closed"><span class="folderAtUsuario" title="CIT">ATENCION AL USUARIO</span>
                                        <ul>
                                          
                                        <% if(beanSession.usuario.preguntarMenu("CIT01") ){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/horarios.jsp")%>', '4-0-0', 'horarios', 'horarios', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="CIT01">Agendamiento</a></span>
                                            </li>
                                        <% }%>
                                        <% if(beanSession.usuario.preguntarMenu("CIT02") ){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="CIT02">Asignacion de Citas</a></span>
                                            </li>
                                        <% }%> <!--
                                        <% if(beanSession.usuario.preguntarMenu("CIT03") ){%> 
                                                <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/misCitasHoy.jsp")%>', '4-0-0', 'misCitasHoy', 'misCitasHoy', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="CIT03">Mis Citas de Hoy</a></span>
                                                </li>
                                         <% }%> -->
                                        <% if(beanSession.usuario.preguntarMenu("PRO03") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="PRO03">Lista de
                                                            Espera</a></span>
                                                </li>
                                        <% }%>
                                        <% if(beanSession.usuario.preguntarMenu("CIT04") ){%>                                            
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/contactosPaciente.jsp")%>', '4-0-0', 'contactosPaciente', 'contactosPaciente', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="CIT04">Contacto con participante</a></span>
                                            </li>
                                        <%}%>   
                                        <% if(beanSession.usuario.preguntarMenu("CIT05")){%>    
                                            <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/paciente.jsp")%>', '4-0-0', 'paciente', 'paciente', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="CIT05">Participante</a></span>
                                                </li>
                                        <%}%>                                       
                                            
                                        </ul>
                                    </li>
                                <%}%>
                                <% if(   beanSession.usuario.preguntarMenu("HOM")   ){%>
                                    
                                <li class="closed"><span class="folderAtUsuario" title="HOM">HOME CARE</span>
                                    <ul>
                                        <% if(beanSession.usuario.preguntarMenu("HOM01") ){%>
                                        <li><span class="file"><a
                                            onclick="javascript:cargarMenu('<%= response.encodeURL("homecare/coordinacion.jsp")%>', '4-0-0', 'coordinacionHomecare', 'coordinacionHomecare', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer"
                                            title="HOM01">Programacion terapias</a></span>
                                        </li>   
                                        <%}%>  
                                        <% if(beanSession.usuario.preguntarMenu("HOM02") ){%>                                   
                                        <li ><span class="file"><a
                                            onclick="javascript:cargarMenu('<%= response.encodeURL("homecare/agenda.jsp")%>', '6-1-3', 'agendaHomecare', 'agendaHomecare', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer" title="HOM02">Agenda terapeuta</a></span>
                                        </li>
                                        <%}%>
                                    </ul>
                                </li>
                                <%}%>
                                <% if(beanSession.usuario.preguntarMenu("ORI") ){%>
                                    <li class="closed"><span class="folderOrientacion" title="ORI">ORIENTACION</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("ORI01") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/asistenciaPacientes.jsp")%>', '4-0-0', 'asistenciaPacientes', 'asistenciaPacientes', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="ORI01">Asistencia de Pacientes</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("ORI02") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/parlante.jsp")%>', '4-0-0', 'parlante', 'parlante', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="ORI02">Parlante</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("ORI03") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/asistenciaDePacientes.jsp")%>', '4-0-0', 'asitenciaMedia', 'asitenciaMedia', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="ORI03">Visitantes</a></span>
                                                </li>
                                                <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/turnos.jsp")%>', '4-0-0', 'demo', 'demo', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="ORI02">DEMO</a></span>
                                        </li>
                                            <%}%>
                                        </ul>
                                    </li>
                                    <%}%>
                            
                            
                                <% if(beanSession.usuario.preguntarMenu("FAC") ){%>
                                    <li class="closed"><span class="folderFacturacion" title="FAC">CONTRATACION</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("FAC01") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/admisiones.jsp")%>', '4-0-0', 'admisiones', 'admisiones', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="FAC01">Admisiones</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("FAC02") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/soloFactura.jsp")%>', '4-0-0', 'soloFactura', 'soloFactura', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="FAC02">Crear Solo Factura</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("PRO03") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="PRO03">Lista de
                                                            Espera</a></span>
                                                </li>
                                             <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("FAC03") ){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/rips.jsp")%>', '4-0-0', 'rips', 'rips', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="FAC03">Generar
                                                        RIPS</a></span>
                                            </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("FAC04") ){%>
                                            <li class="closed"><span class="folder" title="FAC04" >PARAMETROS</span>
                                                <ul>
                                                    <% if(beanSession.usuario.preguntarMenu("FAC05") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/procedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="FAC05">Procedimiento</a></span>
                                                        </li>
                                                    <%}%>
                                                    <% if(beanSession.usuario.preguntarMenu("FAC06") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/articulo.jsp")%>', '4-0-0', 'articulosPlan', 'articulosPlan', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer"
                                                                    title="FAC06">Articulos</a></span>
                                                        </li>
                                                    <%}%>
                                                    <% if(beanSession.usuario.preguntarMenu("FAC07") ){%>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/copago_maximo.jsp")%>', '4-0-0', 'articulosPlan', 'articulosPlan', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer"
                                                                title="FAC07">M&aacuteximo
                                                                evento</a></span>
                                                    </li>
                                                    <%}%>
                                                </ul>
                                            </li>
                                             <% if(beanSession.usuario.preguntarMenu("FAC08") ){%>
                                                    <li class="closed"><span class="folder" title="FAC08">CONTRATACION</span>
                                                        <ul>
                                                            <% if(beanSession.usuario.preguntarMenu("FAC09") ){%>
                                                                <li><span class="file"><a
                                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/tarifarios.jsp")%>', '4-0-0', 'admision', 'admision', 's', 's', 's', 's', 's', 's');"
                                                                            style="cursor:pointer" title="FAC09">Tarifarios</a></span>
                                                                </li>
                                                            <%}%>
                                                            <% if(beanSession.usuario.preguntarMenu("FAC10") ){%>
                                                                <li><span class="file"><a
                                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/planes.jsp")%>', '4-0-0', 'planes', 'planes', 's', 's', 's', 's', 's', 's');"
                                                                            style="cursor:pointer" title="FAC10">Planes de Contratación</a></span>
                                                                </li>
                                                            <%}%>   
                                                        </ul>
                                                    </li>
                                                <%}%>
                    						<% if(beanSession.usuario.preguntarMenu("FAC11") ){%>
                                                    <li class="closed"><span class="folder" title="FAC11" >TIPIFICACION</span>
                                                        <ul>
                                                            <% if(beanSession.usuario.preguntarMenu("FAC12") ){%>
                                                                <li><span class="file"><a
                                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/documentos_tipificacion.jsp")%>', '4-0-0', 'documentos', 'documentos', 's', 's', 's', 's', 's', 's');"
                                                                            style="cursor:pointer" title="FAC12">>Tipificación Documentos</a></span>
                                                                </li>
                                                           <%}%>
                                                            <% if(beanSession.usuario.preguntarMenu("FAC13") ){%>
                                                                <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/tipificacion_facturas.jsp")%>', '4-0-0', 'tipificacion_facturas', 'tipificacion_facturas', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="FAC13">Tipificacion Facturas</a></span>
                                                                </li>
                                                            <%}%>
                                                        </ul>
                                                    </li>
                                                 <%}%>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("FAC14") ){%>
                                                <li class="closed"><span class="folder" title="FAC14">FACTURA</span>
                                                    <ul>
                                                        <% if(beanSession.usuario.preguntarMenu("FAC15") ){%>
                                                            <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturas.jsp")%>',
                                                                    '4-0-0', 'Facturas', 'Facturas', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="FAC15">Facturas</a></span>
                                                            </li>
                                                         <%}%>   
                                                        <!--li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturasAnuladas.jsp")%>', '4-0-0', 'Facturas', 'Facturas', 's', 's', 's', 's', 's', 's');" style="cursor:pointer">Facturas Anuladas</a></span>
                                                        </li-->
                                                        <% if(beanSession.usuario.preguntarMenu("FAC16") ){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/recibos.jsp")%>', '4-0-0', 'recibos', 'recibos', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="FAC16" >Recibos</a></span>
                                                            </li>
                                                        <%}%>
                                                        <% if(beanSession.usuario.preguntarMenu("FAC17") ){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/admision.jsp")%>', '4-0-0', 'admision', 'admision', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="FAC17">Rips</a></span>
                                                            </li>
                                                        <%}%>
                                                    </ul>
                                                </li>
                                            <%}%>
                                            </ul>
                                        </li>
                                <%}%>
                                
                                
                 <% if(beanSession.usuario.preguntarMenu("VEN") ){%>

                                    <li class="closed"><span class="folderVentas" title="VEN">VENTAS</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("VEN01") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/salidasVentas.jsp")%>', '4-0-0', 'salidasVentas', 'salidasVentas', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="VEN01">Facturas</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("VEN02") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturasTrabajo.jsp")%>', '4-0-0', 'facturasTrabajo', 'facturasTrabajo', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="VEN02">Trabajos</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("VEN03") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/cartera.jsp")%>', '4-0-0', 'cartera', 'cartera', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="VEN03">Cartera</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("VEN04") ){%>
                                                <li class="closed"><span class="folder"  title="VEN04">PARAMETROS</span>
                                                    <ul>
                                                        <% if(beanSession.usuario.preguntarMenu("VEN05") ){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/parametrosTrabajos.jsp")%>', '4-0-0', 'Funcionarios', 'Funcionarios', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="VEN05">Funcionarios</a></span>
                                                            </li>
                                                          <%}%>
                                                          <% if(beanSession.usuario.preguntarMenu("VEN06") ){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/terceros/principalTerceros.jsp")%>', '4-0-0', 'Terceros', 'Terceros', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer" title="VEN06">Terceros</a></span>
                                                            </li>
                                                          <%}%>
                                                    </ul>
                                                </li>
                                             <%}%>

                                        </ul>
                                    </li>
                
                
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("ARC")){%>
                                    <li class="closed"><span class="folderArchivo" title="ARC">ARCHIVO</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("PRO03")){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="PRO03 ">Lista de
                                                        Espera</a></span>
                                            </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("ARC01")){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("archivo/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="ARC01 ">Agenda Archivo</a></span>
                                            </li>
                                            
                                            <%}%>
                                        </ul>
                                    </li>
                                <%}%>
                                <% if(beanSession.usuario.preguntarMenu("INV") ){%>
                                <li class="closed"><span class="folderFarmacia" title="INV">INVENTARIOS</span>
                                        
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("INV01") ){%>
                                                <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/pedidos.jsp")%>', '4-0-0', 'Pedidos', 'Pedidos', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="INV01">Pedidos</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("INV02") ){%>   
                                                <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/solicitudes.jsp")%>', '4-0-0', 'solicitudes', 'solicitudes', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="INV02">Respuesta a pedidos</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INV03") ){%>   
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/entradasDeBodega.jsp")%>', '4-0-0', 'entradasDeBodega', 'entradasDeBodega', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="INV03">Entradas a
                                                            Bodega </a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("INV04") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/salidasDeBodega.jsp")%>', '4-0-0', 'salidasDeBodega', 'salidasDeBodega', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="INV04">Salidas de
                                                            Bodega </a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INV05") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/trasladoBodegas.jsp")%>', '4-0-0', 'trasladoBodegas', 'trasladoBodegas', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INV05">Traslado entre Bodegas</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INV06") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/despachoProgramacionProcedimientos.jsp")%>', '4-0-0', 'despachoProgramacionProcedimientos', 'despachoProgramacionProcedimientos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INV06">Despacho a Procedimientos</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("INV07") ){%>
                                                 <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/devoluciones.jsp")%>', '4-0-0', 'devoluciones', 'devoluciones', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INV07">Devoluciones</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INV08") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/devolucionCompras.jsp")%>', '4-0-0', 'devolucionCompras', 'devolucionCompras', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INV08">Devoluciones en Compras</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INV09") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/documentos/principalDocumentos.jsp")%>', '4-0-0', 'principalDocumentos', 'principalDocumentos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INV09">Documentos Historicos</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INV10") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/kardex/principalKardex.jsp")%>', '4-0-0', 'principalKardex', 'principalKardex', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INV10">Kardex</a></span>
                                                </li>
                                             <%}%>
                                             <% if(beanSession.usuario.preguntarMenu("INV11") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/codigoBarras.jsp")%>', '4-0-0', 'codigoBarras', 'codigoBarras', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INV11">Codigo de Barras</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("INV12") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/inventario.jsp")%>', '4-0-0', 'inventario', 'inventario', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="INV12">Hacer
                                                            Inventario</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("INV13") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/recepcionTecnica.jsp")%>', '4-0-0', 'recepcionTecnica', 'recepcionTecnica', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="INV13">Recepcion Tecnica</a></span>
                                           
                                             <% }%>
                                             <% if(beanSession.usuario.preguntarMenu("INV14") ){%>
                                                <li class="closed"><span class="folder" title="INV14">PARAMETROS</span>
                                                    <ul>
                                                        <% if(beanSession.usuario.preguntarMenu("INV15") ){%>
                                                            <li><span class="file"><a
                                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/principalArticulo.jsp")%>', '4-0-0', 'principalArticulo', 'principalArticulo', 's', 's', 's', 's', 's', 's');"
                                                                        style="cursor:pointer"
                                                                        title="INV15" title="INV15">Articulo</a></span>
                                                            </li>
                                                        <% }%>
                                                        <% if(beanSession.usuario.preguntarMenu("INV16") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/canastas/plantillaCanastas.jsp")%>', '4-0-0', 'plantillaCanastas', 'plantillaCanastas', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer"
                                                                    title="INV16" >Crear
                                                                    Canastas</a></span>
                                                        </li>
                                                        <% }%>
                                                        <% if(beanSession.usuario.preguntarMenu("INV17") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/bodega/crearBodega.jsp")%>', '4-0-0', 'CrearBodega', 'CrearBodega', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer"
                                                                    title="INV17">Crear
                                                                    Bodegas</a></span>
                                                        </li>
                                                        <% }%>
                                                    </ul>
                                                </li>
                                            <% }%> 

                                           
                                        </ul>
                                </li>
                               
                                <% }%>
                                
                                <% if(beanSession.usuario.preguntarMenu("GEO") ){%>
                                    <li class="closed"><span class="folderGeo" title="GEO" >GEO-REFERENCIA</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("GEO01") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("encuesta/geoAreas.jsp")%>', '4-0-0', 'geoAreas', 'geoAreas', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="GEO01">Gestión de Areas</a></span>
                                                </li>
                                             <%}%> 
                                             <% if(beanSession.usuario.preguntarMenu("GEO02") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/gestionServicio.jsp")%>', '4-0-0', 'gestionServicio', 'gestionServicio', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="GEO02" >Asignar Medico Area</a></span>
                                                </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("GEO03") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/planParametros.jsp")%>', '4-0-0', 'tipoCitaProcedimiento', 'Gestionar Facturacion', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="GEO03" >Gestion Encuestas</a></span>
                                                </li>
                                            <%}%>
                                        </ul>
                                    </li>
                                    <%}%>  
     

                                    <% if(beanSession.usuario.preguntarMenu("NOT") ){%>
                                        <li class="closed"><span class="folderNotificaciones" title="NOT">NOTIFICACIONES</span>
                                            <ul>
                                                <% if(beanSession.usuario.preguntarMenu("NOT01") ){%>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/crearNotificacion.jsp")%>', '4-0-0', 'crearNotificacion', 'crearNotificacion', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="NOT01" >Crear</a></span>
                                                    </li>
                                                <%}%> 
                                                <% if(beanSession.usuario.preguntarMenu("NOT02") ){%> 
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/leerNotificacion.jsp")%>', '4-0-0', 'leerNotificacion', 'leerNotificacion', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="NOT02">Leer</a></span>
                                                    </li>
                                                <%}%> 
    
                                            </ul>
                                        </li>
                                    <%}%>
                                    
                                <!--li class="closed"><span class="folder">MI AGENDA</span>
                                <ul>
                                <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/notificacion.jsp")%>',
                                '8-1-3', 'notificacion', 'notificacion', 's', 's', 's', 's', 's', 's');"
                                style="cursor:pointer" >noti</a></span>
                                </li>
                                </ul>
                                </li-->
                                <li class="closed"><span class="folderHistoriaClinica" title="HIS">SEGUIMIENTO</span>
                                    <ul>
                                        <% if(beanSession.usuario.preguntarMenu("HIS01")){%>
                                        <li><span class="file"><a
                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/principalHC.jsp")%>', '4-0-0', 'principalHC', 'principalHC', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer" title="HIS01">Folios</a></span>
                                        </li>
                                          
                                      
                                        <%}%>                                            
                                </ul>
                            </li>
                     <%}%>
                                
                      
                                    
                        
                        <% if(beanSession.usuario.preguntarMenu("APD")|| beanSession.usuario.preguntarMenu("HOS")){%>
                        <li class="closed"><span class="folderApoyoDx" title="APD">APOYO DIAGN&Oacute;STICO</span>
                            <ul>
                                <% if(beanSession.usuario.preguntarMenu("APD01")){%>
                                <li><span class="file"><a
                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/apoyoDiagnostico.jsp")%>', '4-0-0', 'apoyoDx', 'apoyoDx', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer" title="APD01">Laboratorios y ayudas diagn&oacute;sticas</a></span>
                                </li>
                                <%}%>
                                <% if(beanSession.usuario.preguntarMenu("APD02")){%>
                                <li><span class="file"><a
                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/pruebaLaboratorio.jsp")%>', '4-0-0', 'pruebaLaboratorio', 'pruebaLaboratorio', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer" title="APD02">Pruebas Laboratorio</a></span>
                                </li>
                                <%}%>
                                <% if(beanSession.usuario.preguntarMenu("ADM15")){%>
                                   <li><span class="file"><a
                                      onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaLaboratorio.jsp")%>',
                                    '4-0-0', 'plantillaLaboratorio', 'plantillaLaboratorio', 's', 's', 's', 's', 's', 's');"
                                    style="cursor:pointer" title="ADM15" >Plantillas Laboratorio</a></span>
                                    </li>
                                <%}%>                                                                                    
                            </ul>
                        </li>
                        <%}%>
                        
                        <% if(beanSession.usuario.preguntarMenu("APD")|| beanSession.usuario.preguntarMenu("HOS")){%>
                            <li class="closed"><span class="folderApoyoDx" title="APD">AUTORIZACIONES</span>
                                <ul>
                                    <% if(beanSession.usuario.preguntarMenu("APD01")){%>
                                    <li><span class="file"><a
                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/autorizaciones/autorizaciones.jsp")%>', '4-0-0', 'autorizaciones','autorizaciones', 's', 's', 's', 's', 's', 's');"
                                                style="cursor:pointer" title="APD01">Autorizaciones</a></span>
                                    </li>
                                    <%}%>                                                                                 
                                </ul>
                            </li>
                            <%}%>

                        <% if(beanSession.usuario.preguntarMenu("POS") ){%>
                            <li class="closed"><span class="folderPostConsulta" title="POS">POST CONSULTA</span>
                                <ul>
                                    <% if(beanSession.usuario.preguntarMenu("POS01") ){%>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/postConsulta/documentos.jsp")%>', '4-0-0', 'documentos', 'documentos', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer"
                                                    title="POSF">Folios</a></span>
                                        </li>
                                     <%}%>
                                     <% if(beanSession.usuario.preguntarMenu("POS02") ){%>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaProcedimientosNoPos.jsp")%>', '4-0-0', 'plantillaProcedimientosNoPos', 'plantillaProcedimientosNoPos', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="POS02" >Plantilla Procedimientos NoPos</a></span>
                                        </li>
                                    <%}%>
                                    <% if(beanSession.usuario.preguntarMenu("POS03") ){%>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaProcedimientosNoPos.jsp")%>', '4-0-0', 'plantillaProcedimientosNoPos', 'plantillaProcedimientosNoPos', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="POS03" >Plantilla Medicamentos NoPos</a></span>
                                        </li>
                                    <%}%>
                                    <% if(beanSession.usuario.preguntarMenu("POS04") ){%>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionAyudaDiagnostica.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer"  title="POS04" >Gestión Ayudas
                                                    Diagnosticas</a></span>
                                        </li>
                                     <%}%>
                                     <% if(beanSession.usuario.preguntarMenu("POS05") ){%>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionSolicitudes.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="POS05">Gestión Solicitudes
                                                    Intercambio</a></span>
                                        </li>
                                    <%}%>
                                </ul>
                            </li>
                        <% }%>

                        <% if(beanSession.usuario.preguntarMenu("HOS") ){%>
                            <li class="closed"><span
                                    class="folderHospitalizacion" title="HOS">HOSPITALIZACION</span>
                            <ul>
                                
                                <% if(beanSession.usuario.preguntarMenu("HOS01")|| beanSession.usuario.preguntarMenu("HOS") ){%>
                                    <li><span class="file">
                                        <a 
                                        onclick="javascript:cargarMenu('<%= response.encodeURL("hospitalizacion/ubicacionCama.jsp")%>', '4-0-0', 'ubicacionCama', 'ubicacionCama', 's', 's', 's', 's', 's', 's');"
                                        style="cursor:pointer" title="HOS01">Ubicacion Cama
                                        paciente</a></span>
                                    </li>
                                <% }%>
                            </ul>
                        </li>
                        <%}%>
             

                        <!--li class="closed"><span class="folder">SEGURIDAD PACIENTE</span>
                        <ul>
                        <li class="closed"><span class="folder">Gestión del riesgo</span>
                        <% if(beanSession.usuario.preguntarMenu("311") ){%> 
                        <ul>			
                        <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/eventoAdverso/reportarEventoAdverso.jsp")%>', '4-0-0', 'reportarEventoAdverso', 'reportarEventoAdverso', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede reportar un evento adverso, como primer actor">Reportar</a></span>
                        </li>
                        </ul>                            
                        <%}%>        
                        <% if(beanSession.usuario.preguntarMenu("312") ){%> 
                        <ul>
                        <li><span  class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/eventoAdverso/administrarEventoAdverso.jsp")%>', '4-0-0', 'administrarEventoAdverso', 'administrarEventoAdverso', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede gestionar los eventos adversos que se reportaron con anterioridad, como segundo actor o como tercer actor">Gestionar</a></span>
                        </li>  
                        </ul>                                                        
                        <%}%>                  
                        </li>   
                        <li class="closed"><span class="folder">Documentos</span>
                        <ul>
                        <li><span class="file"><a onClick="abrirTutorial('/clinica/pdf/tutoriales/tallerPoliticaDeSeguridad2012.pdf')" style="cursor:pointer" title="Usted puede leer el taller de politica de seguridad del paciente 2012">Taller política</a></span>
                        </li>  
                        </ul>                                                        
                        </li>   
                        </ul>           
                        </li-->
                        <% if(beanSession.usuario.preguntarMenu("5") ){%>
                        <!--li class="closed"><span class="folder">ENCUESTAS</span>
                        <ul>a
                        <% if(beanSession.usuario.preguntarMenu("511") ){%>                          
                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("encuestas/responder.jsp")%>', '4-0-0', 'responder', 'responder', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede responder">Responder</a></span>
                        </li>
                        <% }%>   
                        <% if(beanSession.usuario.preguntarMenu("512") ){%>                          
                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("encuestas/gestionar.jsp")%>', '4-0-0', 'gestionar', 'gestionar', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede administrar Proveedores">Gestionar</a></span>
                        </li>
                        <% }%>  
                        </ul>  
                        </li-->
                        <%}%>

                                <% if(beanSession.usuario.preguntarMenu("INF") ){%>
                                    <li class="closed"><span class="folderInformes"title="INF">INFORMES</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("INF01") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("reportes/reportes_parametros.jsp")%>', '4-0-0', 'reportes', 'reportes', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="INF01">Listado de
                                                            Informes</a></span>
                                                </li>
                                            <%}%>
                                            <!-- VERIFICAR notificacion.jsp
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/notificacion.jsp")%>', '4-0-0', 'reportes', 'reportes', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede consultar Reportes de todas las areas">notificar</a></span>
                                            </li>
                                            -->

                                        </ul>
                                    </li>
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("HEL") ){%>
                                    <li class="closed"><span class="folderCalidad" title="HEL" >HELP DESK</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("HEL01") ){%>
                                            <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/evento/crearEvento.jsp")%>',
                                                    '2-1-3', 'crearEvento', 'crearEvento', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="HEL01">Reportar
                                                    Evento</a></span>
                                            </li>
                                            <% }%>
                                            <% if(beanSession.usuario.preguntarMenu("HEL02") ){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/evento/gestionarEventos.jsp")%>', '4-0-0', 'gestionarEvento', 'gestionarEvento', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="HEL02">Gestion de
                                                        Evento</a></span></li>
                                            <% }%>
                                        <!--      <li class="closed"><span class="folder">No conformidades</span>
                                        <% //if(beanSession.usuario.preguntarMenu("211") ){%> 
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/reportarNoConformidad.jsp")%>','2-1-1','reportarNoConformidad','reportarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede reportar una No Conformidad con destino a un proceso">Reportar</a></span></li>
                                        </ul>
                                        <%//}%>                           
                                        <% //if(beanSession.usuario.preguntarMenu("212") ){%> 
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/administrarNoConformidad.jsp")%>','2-1-2','administrarNoConformidad','administrarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede Administrar lider de calidad (Clasificar, valorar, asignar y determinar la Gestión)">Personas Procesos</a></span></li>
                                        </ul> 
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/administrarNoConformidad.jsp")%>','2-1-2','administrarNoConformidad','administrarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede Administrar lider de calidad (Clasificar, valorar, asignar y determinar la Gesti�n)">Administrar</a></span></li>
                                        </ul>   
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/administrarNoConformidad.jsp")%>','2-1-2','administrarNoConformidad','administrarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede gestionar una No Conformidad con destino a su proceso, o direccionarla asi como tambi�n generar Planes de acci�n y seguimientos">Gestionar</a></span></li>
                                        </ul>                            
                                        <%//}%>                                                                                  
                                        </li> -->
                                    </ul>
                                        <!--<ul>
                                        <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/planMejora/administrarPlanMejora.jsp")%>','2-1-3','administrarPlanMejora','administrarPlanMejora','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede administrar un  Plan De Mejora">Oportunidad de mejora</a></span></li>
                                        </ul>  
                                        -->
                                        <% //if(beanSession.usuario.preguntarMenu("i19") ){%>
                                        <!--             <UL>              
                                        <li class="closed"><span class="folder">Indicadores</span>
                                        <ul>
                                        <% if(beanSession.usuario.preguntarMenu("i19_1") ){%>                          
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("indicador/fichaTecnica.jsp")%>','19-1-1','fichaTecnica','fichaTecnica','s','s','s','s','s','s');" style="cursor:pointer" title="i19_1:Usted puede gestionar las Ficha Tecnicas de Indicadores">Ficha Tecnica</a></span>
                                        </li>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("indicador/indicador.jsp")%>','19-1-2','indicador','indicador','s','s','s','s','s','s');" style="cursor:pointer" title="i19_1Usted puede gestionar el Indicadores">Gesti�n Indicador</a></span>
                                        </li>
                                        <% }%>  
                                        </ul>  
                                        </li> 
                                        </UL>  -->
                                        <%//}%>
                                </li>
                                <% }%>

                                <% if(beanSession.usuario.preguntarMenu("ADM") ){%>
                                    <!-- 1 -->
                                    <li class="closed"><span class="folderAdministracion" title="ADM">ADMINISTRACION</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("ADM01") ){%>
                                             <li class="closed"><span class="folder" title="ADM01">Seguridad</span>
                                                <ul>   
                                                    <% if(beanSession.usuario.preguntarMenu("ADM02") ){%>
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/perfiles.jsp")%>', '4-0-0', 'perfiles', 'Perfiles de Usuarios', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="ADM02">Roles</a></span>
                                                        </li>
                                                    <% }%>

                                                    <% if(beanSession.usuario.preguntarMenu("ADM03") ){%>       
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/crearUsuario.jsp")%>', '4-0-0', 'crearUsuario', 'Creacion de Usuarios', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="ADM03">Gestionar Usuarios</a></span>
                                                        </li>
                                                    <% }%>
                                                    <% if(beanSession.usuario.preguntarMenu("ADM04") ){%>
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/usuario.jsp")%>', '4-0-0', 'usuario', 'Gestionar Información de Usuarios', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="ADM04">Credencial de Usuario</a></span>
                                                        </li>
                                                    <% }%>
                                                </ul>
                                            </li>
                                        <% }%>
                                        <% if(beanSession.usuario.preguntarMenu("ADM05") ){%>
                                            <li class="closed"><span class="folder" title="ADM05">Parametros</span>
                                                <ul>

                                                    <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/datosApp.jsp")%>', '4-0-0', 'datosApp', 'datosApp', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="ADM07" >Datos app</a></span>
                                                    </li>
                                                    
                                                    <% if(beanSession.usuario.preguntarMenu("ADM07") ){%>
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarProfesion.jsp")%>', '4-0-0', 'configuracionArea', 'configuracionArea', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="ADM07" >Profesiones</a></span>
                                                        </li>
                                                     <% }%>
                                                     <% if(beanSession.usuario.preguntarMenu("ADM08") ){%>

                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/sedeEspecialidades.jsp")%>', '4-0-0', 'sedeEspecialidades', 'sedeEspecialidades', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="ADM08">Especialidades</a></span>
                                                        </li>
                                                     <% }%>
                                                     <% if(beanSession.usuario.preguntarMenu("ADM09") ){%> 
                                                        <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/tipoCitaProcedimiento.jsp")%>', '4-0-0', 'sedeEspecialidades', 'sedeEspecialidades', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="ADM09">Tipos cita</a></span>
                                                        </li>                                                        
                                                    <% }%>
                                                    <% if(beanSession.usuario.preguntarMenu("ADM11") ){%>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/centroCosto.jsp")%>', '4-0-0', 'Centro de costo', 'Gestionar Información de xxx', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer" title="ADM11">Centro
                                                                    de costo</a></span>
                                                        </li>
                                                    <%}%>
                                                    
                                                </ul>
                                            </li>
                                            <%}%>

                                            <% if(true){%>
                                                <li class="closed"><span class="folder" title="ADM17">Ubicaciones</span>
                                                    <ul>
                                                        <% if(beanSession.usuario.preguntarMenu("ADM18") ){%>
                                                            <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configuracionSedes.jsp")%>', '4-0-0', 'configuracionArea', 'configuracionArea', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="ADM18" >Sede</a></span>
                                                            </li>                                                            
                                                         <% }%> 
                                                        <% if(beanSession.usuario.preguntarMenu("ADM19") ){%>
                                                            <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configuracionArea.jsp")%>', '4-0-0', 'configuracionArea', 'configuracionArea', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="ADM19" >Area</a></span>
                                                            </li>                                                            
                                                         <% }%>                                                                                                                                                                               
                                                        <% if(beanSession.usuario.preguntarMenu("ADM20") ){%>
                                                       
                                                            <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarHabitacion.jsp")%>', '4-0-0', 'configuracionHabitacion', 'configuracionHabitacion', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="ADM20">Habitacion</a></span>
                                                            </li>                                                            
                                                                                                                                                                                                                                            
                                                        <% }%>
                                                         
                                                        <% if(beanSession.usuario.preguntarMenu("ADM21") ){%>
                                                            <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarElemento.jsp")%>', '4-0-0', 'configuracionElemento', 'configuracionElemento', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="ADM21">Cama</a></span>
                                                            </li>  
                                                        <% }%>                                                         
                                                  </ul>
                                                </li>
                                            <% }%>
                                             <% if(beanSession.usuario.preguntarMenu("HIS02")|| beanSession.usuario.preguntarMenu("HISTORIACLINICA") ){%>
                                                <li class="closed"><span class="folder" title="HIS02">Seguimiento</span>
                                                    <ul>
                                                        <% if(beanSession.usuario.preguntarMenu("HIS03")){%>
                                                            <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/folios.jsp")%>', '4-0-0', 'folios', 'folios', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="HIS03">Tipos de folio</a></span>
                                                            </li>
                                                        <%}%>                                                     
                                                       
                                        
                                                            
                                                            <% if(beanSession.usuario.preguntarMenu("ADM12") ){%>
                                                                <li><span class="file"><a
                                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaFormulario.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                            style="cursor:pointer" title="ADM12">Plantillas</a></span>
                                                                </li>
                                                            <%}%>
                                                            <% if(beanSession.usuario.preguntarMenu("ADM13") ){%>
                                                                <li><span class="file"><a
                                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaFolio.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                            style="cursor:pointer" title="ADM13">Plantillas Folio</a></span>
                                                                </li>
                                                            <%}%>
                                                            <% if(beanSession.usuario.preguntarMenu("ADM14") ){%>
                                                                <li><span class="file"><a
                                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaEncuesta.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                            style="cursor:pointer" title="ADM14">Plantillas Encuesta</a></span>
                                                                </li>
                                                            <%}%>
                                                         
                                                            <% if(beanSession.usuario.preguntarMenu("ADM16") ){%>
                                                                <li><span class="file"><a
                                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/encuestaTipoFolio.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                            style="cursor:pointer"  title="ADM16">Encuesta tipo folio</a></span>
                                                                </li>
                                                             <%}%>
                                                            
                                                         
                                                    </ul>
                                                </li>
                                            <%}%>
                                        </ul>
                                    </li>
                                <%}%>
                                <li class="closed"><span class="folderCalidad">CONFIGURACION</span>
                                    <ul>
                                        <li><span class="file"><a
                                            onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/password.jsp")%>', '4-0-0', 'password', 'password', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer" title="">Cambio de Contrase&ntilde;a</a></span>
                                        </li>
                                    </ul>
                                </li>

                                
                                    <!-- Desbilitado / validar cierre de sesion con borrado de cche en el navegador -->
                                    <li class="closed"><span class="folderCierre">Cerrar Sesion</span>
                                        <ul>
                                            <li><span class="file">
                                                <a </span>
                                                <a onclick='cerrar_sesion();'>Salir del aplicativo</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <script type="text/javascript">
                                
                                    </script>
                              

                              </ul>
                             <!--</li> -->
                           </DIV>
                       </div>

                            <% if(beanSession.cn.getNombreBd().equals("clinica")){ %>
                                <div id="main"
                                    style="width:1494px; left:150px; top:50px; height:4000px;  background-image:url(../utilidades/imagenes/menu/fondoProduccion.jpg); background-repeat:no-repeat;   "
                                    align="center"></div>
                                <% }else { %>
                                <div id="main"
                                    style="width:1494px; left:150px; top:50px; height:4000px; background-image:url(../utilidades/imagenes/menu/fondoProduccion.jpg); background-repeat:no-repeat;  "
                                    align="center"></div>
                                <% }%>
                                <DIV
                                    style="top:701px; display:none; left:10px; position:absolute; color:#3C3; font-weight:bold; width:1136px; height:9px; background-color:#8c0050">
                                </DIV>
                                <div class="mensaje" id="mensaje"
                                    style="top:666px; left:7px; position:absolute; color:#FFFFFF; font-weight:bold ">
                                </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<!--
<center><input id="btn_SALIR"  type="button" class="blue buttonini" value="CERRAR SESION" style="width:190px"  onClick="javascript:salir();" tabindex="999"/></center>
-->

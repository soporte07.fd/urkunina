<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "Sgh.Utilidades.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->

<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Pragma" CONTENT="no-cache">
	<title>CRISTAL WEB</title>
	<LINK REL="SHORTCUT ICON" HREF="/clinica/utilidades/icono3.ico">

	<link href="/clinica/utilidades/css/estiloClinica.css" rel="stylesheet" type="text/css">
	<link href="/clinica/utilidades/css/extras.css" rel="stylesheet" type="text/css">
	<link href="reportes/estiloPcweb_imp.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="/clinica/utilidades/css/menu.css" />
	<link rel="stylesheet" type="text/css" href="/clinica/utilidades/css/jquery.treeview.css" />
	<!--link href="/clinica/utilidades/javascript/themes/basic/grid.css" rel="stylesheet" type="text/css"-->
	<!--link href="/clinica/utilidades/javascript/themes/basic_off/grid.css" rel="stylesheet" type="text/css"-->
	<link href="/clinica/utilidades/css/custom-theme/jquery-ui-1.7.custom.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../utilidades/css/jquery.autocomplete.css" />

	<link href="/clinica/paginas/planAtencion/leaflet/leaflet.css" rel="stylesheet" type="text/css">


	<!--<link rel="stylesheet" type="text/css" href="../utilidades/bootstrap/css/bootstrap.css" />-->

	<link href="/clinica/paginas/hc/saludOral/diente/css/odontograma.css" rel="stylesheet" type="text/css" />
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	<link href="/clinica/utilidades/timepicker-1.3.5/jquery.timepicker.css" rel="stylesheet">
	<!--[if lt IE 7]>
<link href="/clinica/utilidades/css/ventan-modal-ie.css" rel="stylesheet" type="text/css">
<![endif]-->

	<!--SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.js"></SCRIPT-->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js"></script>
	<script type="text/javascript" src="/clinica/utilidades/timepicker-1.3.5/jquery.timepicker.js"></script>
	<script type="text/javascript" src="/clinica/utilidades/javascript/alert/jquery.alerts.js"></script>

	<script language=javascript src="/clinica/utilidades/jqGrid-4.4.3/jquery.jqGrid.min.js"></script>
	<script language=javascript src="/clinica/utilidades/jqGrid-4.4.3/grid.locale-es.js"></script>
	<link rel="stylesheet" type="text/css" href="/clinica/utilidades/jqGrid/jquery-ui2.css">
	<link rel="stylesheet" type="text/css" href="/clinica/utilidades/jqGrid-3.8.2/css/ui.jqgrid.css">

	<SCRIPT language=javascript src="/clinica/utilidades/javascript/fisheye.js"></SCRIPT>
	<SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.corner.js"></SCRIPT>
	<SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.treeview.js"></SCRIPT>
	<SCRIPT language=javascript src="/clinica/utilidades/javascript/dropShadow.js"></SCRIPT>
	<SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.blockUI.js"></SCRIPT>
	<SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.ui.all.js"></SCRIPT>

	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/funcionesBotones.js?<%beanSession.getVersion();%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/funciones.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/funcionesMenu.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<script language=javascript src="/clinica/paginas/planAtencion/rutas.js?<%=beanSession.getVersion()%>"> </script>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesAdicionales.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesJuan.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesAgenda.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesFacturacion.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesEvento.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesHc.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<!--SCRIPT language=javascript src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesPlatin.js?<%=beanSession.getVersion()%>"></SCRIPT-->
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesReportes.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesSuministros.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/limpiarCampos.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesRecepcionTecnica.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesSinergiaCC.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/validarcampos.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/subirArchivos.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesPaciente.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<SCRIPT language=javascript src="/clinica/utilidades/javascript/js/coda.js"></SCRIPT>
	<SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.autocomplete.pack.js"></SCRIPT>
	<!--script language="JavaScript" type="text/javascript" src="/clinica/utilidades/javascript/formatearnum.js"></script-->
	<script language=javascript
		src="/clinica/utilidades/javascript/cronometro.js?<%=beanSession.getVersion()%>"> </script>
	<script language=javascript
		src="hc/documentosHistoriaClinica/documentosHistoriaClinica.js?<%=beanSession.getVersion()%>"> </script>
	<script language=javascript
		src="/clinica/paginas/referencia/funcionesReferencia.js?<%=beanSession.getVersion()%>"> </script>
	<script language=javascript
		src="/clinica/paginas/hc/saludOral/odontologia.js?<%=beanSession.getVersion()%>"> </script>
	<script language=javascript src="/clinica/paginas/parametros/parametros.js?<%=beanSession.getVersion()%>"> </script>


	<script language=javascript
		src="/clinica/paginas/planAtencion/leaflet/leaflet-src.esm.js?<%=beanSession.getVersion()%>"> </script>
	<script language=javascript
		src="/clinica/paginas/planAtencion/leaflet/leaflet-src.js?<%=beanSession.getVersion()%>"> </script>
	<script language=javascript
		src="/clinica/paginas/hc/manejoAdministrativo/encuestas/plantillaEncuesta.js?<%=beanSession.getVersion()%>"> </script>
	<script language=javascript src="/clinica/paginas/encuesta/encuesta.js?<%=beanSession.getVersion()%>"> </script>
	<script language=javascript
		src="/clinica/paginas/encuesta/encuestaValidaciones.js?<%=beanSession.getVersion()%>"> </script>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/OTRAS/funcionesBotonesPaciente.js?<%=beanSession.getVersion()%>"></SCRIPT>

	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	<SCRIPT language=javascript
		src="/clinica/utilidades/javascript/jquery.ui.timepicker.js?<%=beanSession.getVersion()%>"></SCRIPT>
	<link rel="stylesheet" type="text/css"
		href="/clinica/utilidades/javascript/jquery.ui.timepicker.css?<%=beanSession.getVersion()%>"/>

	<script language="javascript">
		var varajax;
		var bandera = 0;

		$(document).keydown(function (e) { if (e.keyCode == 65 && e.ctrlKey) { e.preventDefault(); } }); 

		tabID = sessionStorage.tabID && sessionStorage.closedLastTab != '2' ? sessionStorage.tabID : sessionStorage.tabID = Math.trunc(Math.random() * 1000000);
		sessionStorage.closedLastTab = '2';

		window.onunload = window.onbeforeunload = function(e){
			sessionStorage.closedLastTab = '1';
			cerrar_sesion();
			return null
		}

		function nobackbutton(){
			// window.location.hash="no-back-button";	
			// window.location.hash="Again-No-back-button" //chrome
			// window.onhashchange=function(){window.location.hash="no-back-button";}
		}
		
		function llenarinfo() {
			if (varajax.readyState == 4) {
				if (varajax.status == 200) {
					document.getElementById('inicial').innerHTML = varajax.responseText;
					window.resizeTo(1200, 900);
					window.moveTo(50, 50)
					//iniciarObjetosMenu();
					iniciarDiccionarioConNotificaciones(1);
					$('#dock').Fisheye(
						{
							maxWidth: 10,
							items: 'a',
							itemsText: 'span',
							container: '.dock-container',
							itemWidth: 20,
							proximity: 20,
							halign: 'left'
						}
					);
					$("#msgBienvenido").dropShadow({ left: 2, top: 2, blur: 1, opacity: 1, color: "WHITE", swap: false });

					$('#divMenuTreeTitle').corner("tear 10px").parent().css('padding', '4px').corner("fray 5px");
					$("#browser").treeview({
						toggle: function () {
							console.log("%s was toggled.", $(this).find(">span").text());
						}
					});
					//funciones para ocultar y mostrar el menu
					var puedeDesaparecer = false
					$('#disparaMenu').mousemove(function (event) {
						setTimeout("", 50);
						if (puedeDesaparecer == false) {
							$("#menuDesplegable").animate({ width: "180px" }, { queue: false, duration: 30 });
							$('#divMenuTreeTitle').show();
						}
						puedeDesaparecer = false;
					});
					$('#main').mousemove(function (event) {
						if (puedeDesaparecer == true) {
							$("#menuDesplegable").animate({ width: "0px" }, { queue: false, duration: 30 });
							$('#divMenuTreeTitle').hide();
						}
						puedeDesaparecer = false;
					});
					$('#menuDesplegable').mousemove(function (event) {
						//$("#menuDesplegable").animate( { width:"180px" }, { queue:false, duration:30 } );
						puedeDesaparecer = false
					});
					//fin funciones mostrar y ocultar menu
					if (document.getElementById('lblContrasena').innerHTML == '123') {
						setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/adminSeguridad/password.jsp")%>', '5-0-0', 'password', 'password', 's', 's', 's', 's', 's', 's')", 2000);
					}
					else {
					}

				} else {
					alert("Problemas de conexion con servidor: " + varajax.status);
				}
			}
			if (varajax.readyState == 1) {
				document.getElementById('inicial').innerHTML = crearMensaje();
			}
			document.getElementById('idTableLogin').setAttribute("hidden", "true");
		}
		
		function principal(){
			//--- INGRESAR
			$.ajax({
				url: "/clinica/paginas/principal.jsp",
				type: "POST",
				beforeSend: function () { 
					document.getElementById('inicial').innerHTML = crearMensaje();
					//document.getElementById('idTableLogin').setAttribute("hidden", "true");
				},
				success: function (data) {
					document.getElementById('inicial').innerHTML = data;
					window.resizeTo(1200, 900);
					window.moveTo(50, 50)

					iniciarDiccionarioConNotificaciones(1);
					$('#dock').Fisheye(
						{
							maxWidth: 10,
							items: 'a',
							itemsText: 'span',
							container: '.dock-container',
							itemWidth: 20,
							proximity: 20,
							halign: 'left'
						}
					);
					$("#msgBienvenido").dropShadow({ left: 2, top: 2, blur: 1, opacity: 1, color: "WHITE", swap: false });

					$('#divMenuTreeTitle').corner("tear 10px").parent().css('padding', '4px').corner("fray 5px");
					$("#browser").treeview({
						toggle: function () {
							console.log("%s was toggled.", $(this).find(">span").text());
						}
					});

					//funciones para ocultar y mostrar el menu
					var puedeDesaparecer = false
					$('#disparaMenu').mousemove(function (event) {
						setTimeout("", 50);
						if (puedeDesaparecer == false) {
							$("#menuDesplegable").animate({ width: "180px" }, { queue: false, duration: 30 });
							$('#divMenuTreeTitle').show();
						}
						puedeDesaparecer = false;
					});
					$('#main').mousemove(function (event) {
						if (puedeDesaparecer == true) {
							$("#menuDesplegable").animate({ width: "0px" }, { queue: false, duration: 30 });
							$('#divMenuTreeTitle').hide();
						}
						puedeDesaparecer = false;
					});
					$('#menuDesplegable').mousemove(function (event) {
						//$("#menuDesplegable").animate( { width:"180px" }, { queue:false, duration:30 } );
						puedeDesaparecer = false
					});
					//fin funciones mostrar y ocultar menu
					if (document.getElementById('lblContrasena').innerHTML == '123') {
						setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/adminSeguridad/password.jsp")%>', '5-0-0', 'password', 'password', 's', 's', 's', 's', 's', 's')", 2000);
					}
					else {
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					alert("Error al cargar la pagina. Por favor verifique su conexión a internet.")
				}
			});
		}

		function login0(){
			$.ajax({
				url: "/clinica/paginas/login0.jsp",
				type: "POST",
				beforeSend: function () {
					document.getElementById('inicial').innerHTML = crearMensaje();
					//document.getElementById('idTableLogin').setAttribute("hidden", "true");
				 },
				success: function (data) {
					document.getElementById('inicial').innerHTML = data;
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					alert(textStatus + "-")
				}
			});
		}

		function ingresar() {
			$.ajax({
				url: "/clinica/paginas/sesion_nav.jsp",
				type: "POST",
				beforeSend: function () { },
				success: function (data) {
					if (data.sesion == tabID) {
						principal();
					} else {
						login0();
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					alert(textStatus + "---------")
				}
			});
		}

		/*function ingresar(idSesion){
			valores_a_mandar="";
			varajax=crearAjax();
			varajax.open("POST","<%= response.encodeURL("principal.jsp")%>",true);
			varajax.onreadystatechange=llenarinfo;
			varajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
			varajax.send(valores_a_mandar); 
		}	*/

		function sedes() {
			valores_a_mandar = "";
			varajax = crearAjax();
			varajax.open("POST", "<%= response.encodeURL("sedes_xml.jsp")%>", true);
			varajax.onreadystatechange = respuestasedes;
			varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
			varajax.send(valores_a_mandar);
		}

		function respuestasedes() {
			if (varajax.readyState == 4) {
				if (varajax.status == 200) {
					raiz = varajax.responseXML.documentElement;
					if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
						ingresar();
						setTimeout("cargarMenu('<%= response.encodeURL("infosedes.jsp")%>', '4-0-0', 'infosedes', 'infosedes', 's', 's', 's', 's', 's', 's')", 1000);

					} else {
						ingresar();
					}
				} else {
					alert("Problemas de conexion con servidor: " + varajax.status);
				}
			}
			if (varajax.readyState == 1) {
			}
		}

		function verificar_disponibilidad_sesion(){
			$.ajax({
				url: "/clinica/paginas/sesion_nav.jsp",
				type: "POST",
				beforeSend: function () { 
				},
				success: function (data) {
					if (true) {
						validar();
					} else {
						login0();
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					alert(textStatus + "-")
				}
			});
		}

		function validar() {
			valores_a_mandar = "login=" + document.getElementById('txtLogin').value + "&password=" + document.getElementById('txtContrasena').value + "&idSesion=" + tabID;
			varajax = crearAjax();
			varajax.open("POST", "<%= response.encodeURL("ingresar_xml.jsp")%>", true);
			varajax.onreadystatechange = validarIngreso;
			varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
			varajax.send(valores_a_mandar);
		}

		function validarIngreso() {
			if (varajax.readyState == 4) {
				if (varajax.status == 200) {
					raiz = varajax.responseXML.documentElement;
					if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
						if (bandera == 0) {
							sedes();
							bandera = 1;
						}
						else {
							//actualizarValidacionSede();
							ingresar();
						}
					} else {
						alert("Usuario, contrasena incorrecta o se encuentra inactivo. \n Favor informar al administrador del sistema.");
					}
				} else {
					alert("Problemas de conexion con servidor: " + varajax.status);
				}
			}
			if (varajax.readyState == 1) {
			}
		}

		function actualizarValidacionSede() {
			$.ajax({
				url: "/clinica/paginas/verificacion_sede.jsp",
				type: "POST",
				beforeSend: function () { },
				success: function (data) { },
				error: function (XMLHttpRequest, textStatus, errorThrown) { }
			});
		}

		function validar2() {
			valores_a_mandar = "login=" + document.getElementById('txtLogin').value + "&password=" + document.getElementById('txtContrasena').value;
			varajax = crearAjax();
			varajax.open("POST", "<%= response.encodeURL("ingresar_xml.jsp")%>", true);
			varajax.onreadystatechange = validarIngreso2;
			varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
			varajax.send(valores_a_mandar);
		}

		function validarIngreso2() {
			if (varajax.readyState == 4) {
				if (varajax.status == 200) {
					raiz = varajax.responseXML.documentElement;
					if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
						ingresar();
					} else {
						//  alert("Usuario, contrase�a incorrecta o se encuentra inactivo. \n Favor informar al administrador del sistema.");
					}
				} else {
					alert("Problemas de conexion con servidor: " + varajax.status);
				}
			}
			if (varajax.readyState == 1) {
				// document.getElementById('inicial').innerHTML =	crearMensaje();
			}
		}

		/*Script del Reloj */
		function actualizaReloj() {
			/* Capturamos la Hora, los minutos y los segundos */
			marcacion = new Date()
			/* Capturamos la Hora */
			Hora = marcacion.getHours()
			/* Capturamos los Minutos */
			Minutos = marcacion.getMinutes()
			/* Capturamos los Segundos */
			Segundos = marcacion.getSeconds()
			/*variable para el ap�strofe de am o pm*/
			dn = ""<!--am-->
			if (Hora > 12) {
			dn = ""<!--am-->
			Hora = Hora <!--Si es formato 12 horas poner Hora= Hora -12--> 
			}
			if (Hora == 0)
			Hora = 12
			/* Si la Hora, los Minutos o los Segundos son Menores o igual a 9, le a�adimos un 0 */
			if (Hora <= 9) Hora = "0" + Hora
			if (Minutos <= 9) Minutos = "0" + Minutos
			if (Segundos <= 9) Segundos = "0" + Segundos
			/* Termina el Script del Reloj */
			
			/*Script de la Fecha */
			
			var Dia = new Array("Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo");
			var Mes = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", 
			"Octubre", "Noviembre", "Diciembre");
			var Mesn = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", 
			"10", "11", "12");
			var Hoy = new Date();
			var Anio = Hoy.getFullYear();
			var Fecha = Dia[Hoy.getDay()] + ", " + Hoy.getDate() + " de " + Mes[Hoy.getMonth()] + " de " + Anio + ". ";
			var Fecha2 = Hoy.getDate() + "/" + Mesn[Hoy.getMonth()] + "/" + Anio + "  ";
			
			
			/* Termina el script de la Fecha */
			
			/* Creamos 2 variables para darle formato a nuestro Script */
			var Script, Script2, Total, Total2
			
			/* En Reloj le indicamos la Hora, los Minutos y los Segundos */
			Script = Fecha + Hora + ":" + Minutos + ":" + Segundos + " " + dn
			
			/* En total Finalizamos el Reloj uniendo las variables */
			Total = Script
			Total2 = Fecha2 + Hora + ":" + Minutos + ":" + Segundos + " " + dn
			
			/* Capturamos una celda para mostrar el Reloj */
			document.getElementById('Fecha_Reloj').innerHTML = Total
			document.getElementById('format').value = Total2
			
			/* Indicamos que nos refresque el Reloj cada 1 segundo */
			setTimeout("actualizaReloj()", 1000)
		}

		document.forms[0].reset();


		function MM_swapImgRestore() { //v3.0
			var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
		}

		function MM_preloadImages() { //v3.0
			var d = document; if (d.images) {
				if (!d.MM_p) d.MM_p = new Array();
				var i, j = d.MM_p.length, a = MM_preloadImages.arguments; for (i = 0; i < a.length; i++)
					if (a[i].indexOf("#") != 0) { d.MM_p[j] = new Image; d.MM_p[j++].src = a[i]; }
			}
		}

		function MM_findObj(n, d) { //v4.01
			var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
				d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
			}
			if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
			for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
			if (!x && d.getElementById) x = d.getElementById(n); return x;
		}

		function MM_swapImage() { //v3.0
			var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2); i += 3)
				if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
		}


		document.getElementById('txtLogin').focus();
		cargarFocos();


		var banderadrag = true;
		function mover(idReg) {//funcion que permite dar movimineto a los div que tengan como id=drag y como div interno con id=tituloForma
			$('#drag' + idReg).draggable(
				{
					zIndex: 1000,
					ghosting: true,
					opacity: 0.7,
					containment: 'parent',
					handle: '#tituloForma'

				}
			);
		}
		//validar();
		function checkKey(key) {
			var unicode
			if (key.charCode) { unicode = key.charCode; }
			else { unicode = key.keyCode; }
			//alert(unicode); // Para saber que codigo de tecla presiono , descomentar

			if (unicode == 13) {
				verificar_disponibilidad_sesion();
			}
		}

	</script>
</head>
<%
if(false){ %>
<body onload="ingresar(); nobackbutton();">
	<div id="inicial"></div>
</body>
<%
} else { %>
<body onLoad="if ('Navigator' == navigator.appName) document.forms[0].reset(); nobackbutton();">
	<iframe name="iframeUpload" id="iframeUpload" style="border:none; display:none "></iframe>
	<div id="inicial">
		<table width="90%" border="1" align="center" id="idTableLogin">
			<tr>
				<td width="50%" align="center">
					<FONT FACE="impact" SIZE=4 color="#336699">PUBLICACIONES</font>
				</td>
				<td width="50%" align="center">&nbsp;</td>
			</tr>
			<tr>
				<td align="center" valign="top">
					<div class="container" style="height:450px; width:auto">
						<iframe src="../../publicaciones/index.html" width="100%" height="100%" align="center"
							class="container_"></iframe>
					</div>
				</td>
				<td width="10%" align="CENTER" valign="top">
					<div class="container">
						<img src="../logo.png" width="230px" height="120px" style="margin-left:7%; margin-top:1%;"
							alt="Clinica">
						<hr />
						<input id="txtLogin" type="text" style="height:20px;" value="" size="25"
							onKeyPress="javascript:return teclearsoloDigitosYMinuscYMayusRayaPiso(event);"
							placeholder="Usuario" tabindex="1" />
						<br /><br />
						<input id="txtContrasena" type="password" style="height:20px" value="" size="25"
							onkeypress="checkKey(event);" placeholder="Contrase&ntilde;a" tabindex="2" />
						<br /><br />
						<center><input id="btn_ENTRAR" type="button" class="blue buttonini" value="INICIAR SESION"
								style="width:190px" onClick="javascript:verificar_disponibilidad_sesion();" tabindex="999" /></center>
						<hr />
						<input type="hidden" disabled="no" id="format" width="70px" />
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
</body>
<%
}
%>
</html>
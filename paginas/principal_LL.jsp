<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "Sgh.Utilidades.Sesion" %>


<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->

<table width="90%" align="left" border="0" cellspacing="0" cellpadding="0" style="margin-top:1px; z-index:1500">
    <tr>
        <td valign="top"> <input name="ventanasActivas" id="ventanasActivas" type="hidden" class="nomayusculas"
                tabindex="1" value="" size="25" />
            <label id="lblContrasena" style="display: none;"><%=beanSession.usuario.getContrasena()%></label>
            <jsp:include page="menu.jsp" flush="true" />
            <table id="tablaMain" width="100%" height="100%" align="left" cellpadding="0" cellspacing="0"
                bgcolor="#CCCCCC">
                <tr>
                    <td align="center" valign="top">
                        <div id="disparaMenu"
                            style=" background-color:#333; width:5px; height:680px; position:absolute; z-index:110; left:3px; top: 70px;  ">
                        </div>
                        <div id="menuDesplegable"
                            style="width:132px; position:absolute; left:9px; z-index:1;  top: 40px; height: 1080px;">
                                <div style="background:#FFFFFF">
                                    <div id="divMenuTreeTitle"
                                        style="background:#FC3;  height:100%; font-family:Arial, Helvetica, sans-serif; font-size:10px; "
                                        align="center">
                                        <label id="lblMenuTreeTitle" style="color:#193b56;">MEN&Uacute;</label>
                                    </div>
                                </div>
                                <br />
                            <DIV style="overflow:auto; height:1777px; top: 40px">
                              <ul id="browser" class="filetree treeview-black" style="font-size:9px"><strong></strong>
                            
                                <% if(beanSession.usuario.preguntarMenu("CCP")){%>
                                        <li class="closed"><span class="folderHistoriaClinica">HISTORIA CLINICA</span>
                                            <ul>
                                                <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/principalHC.jsp")%>', '4-0-0', 'principalHC', 'principalHC', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="Usted puede consultar Reportes de todas las
                                                    areas">Folios Historia Clinica</a></span>
                                                </li>

                                            <% if(beanSession.usuario.preguntarMenu("HC") ){%>
                                                <li class="closed"><span class="folder">PARAMETROS</span>
                                                    <ul>

                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/diagnostico.jsp")%>', '4-0-0', 'diagnostico', 'diagnostico', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer">Diagnostico</a></span>
                                                        </li>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/procedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer">Procedimiento</a></span>
                                                        </li>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaProcedimientosNoPos.jsp")%>', '4-0-0', 'plantillaProcedimientosNoPos', 'plantillaProcedimientosNoPos', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer">Plantilla Procedimientos
                                                                    NoPos</a></span>
                                                        </li>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaMedicamentosNoPos.jsp")%>', '4-0-0', 'plantillaMedicamentosNoPos', 'plantillaMedicamentosNoPos', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer">Plantilla Medicamentos
                                                                    NoPos</a></span>
                                                        </li>
                                                    </ul>
                                                </li>
                                            <%}%>

                                            <% if(beanSession.usuario.preguntarMenu("REF") ){%>
                                                <li class="closed"><span class="folder">REFERENCIA</span>
                                                    <ul>
                                                        <li><span class="file">
                                                            <a onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/referencia.jsp")%>',
                                                            '4-0-0', 'referencia', 'referencia', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="Usted puede consultar Reportes de todas las
                                                            areas">Referencia Contra Referencia</a>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </li>
                                            <%}%>
                                        </ul>
                                    </li>
                                        
                                <%}%>
                                            
                                
                                <% if(beanSession.usuario.preguntarMenu("HOS")){%>
                                    <li class="closed"><span class="folderApoyoDx">APOYO DIAGN&Oacute;STICO</span>
                                        <ul>
                                            <li><span class="file"><a
                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/apoyoDiagnostico.jsp")%>', '4-0-0', 'apoyoDx', 'apoyoDx', 's', 's', 's', 's', 's', 's');"
                                                style="cursor:pointer">Laboratorios y ayudas diagn&oacute;sticas</a></span>
                                            </li>
                                        </ul>
                                    </li>
                                <%}%>    

                                <% if(beanSession.usuario.preguntarMenu("POS") ){%>
                                    <li class="closed"><span class="folderPostConsulta">POST CONSULTA</span>
                                        <ul>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/postConsulta/documentos.jsp")%>', '4-0-0', 'documentos', 'documentos', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede administrar paciente">Folios</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaProcedimientosNoPos.jsp")%>', '4-0-0', 'plantillaProcedimientosNoPos', 'plantillaProcedimientosNoPos', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Plantilla Procedimientos
                                                        NoPos</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaMedicamentosNoPos.jsp")%>', '4-0-0', 'plantillaMedicamentosNoPos', 'plantillaMedicamentosNoPos', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Plantilla Medicamentos
                                                        NoPos</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionAyudaDiagnostica.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Gestión Ayudas
                                                        Diagnosticas</a></span>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionSolicitudes.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Gestión Solicitudes
                                                        Intercambio</a></span>
                                        </ul>
                                    </li>
                                <% }%>

                                <% if(beanSession.usuario.preguntarMenu("HOS") ){%>
                                    <li class="closed"><span
                                            class="folderHospitalizacion">HOSPITALIZACION</span>
                                    <ul>
                                        <% if(beanSession.usuario.preguntarMenu("HC") ){%>
                                            <li class="closed"><span class="folder">PARAMETROS</span>
                                                <ul>
                                                    <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configuracionArea.jsp")%>', '4-0-0', 'configuracionArea', 'configuracionArea', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Area</a></span>
                                                    </li>                                                            
                                                                                                                                                                                                                                    
                                                </ul>

                                                <ul>
                                                    <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarHabitacion.jsp")%>', '4-0-0', 'configuracionHabitacion', 'configuracionHabitacion', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Habitacion</a></span>
                                                    </li>                                                            
                                                                                                                                                                                                                                    
                                                </ul>
                                                <ul>
                                                    <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarElemento.jsp")%>', '4-0-0', 'configuracionElemento', 'configuracionElemento', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Cama</a></span>
                                                    </li>                                                            
                                                                                                                                                                                                                                    
                                                </ul>

                                            </li>
                                        <% }%>
                                        
                                        <% if(beanSession.usuario.preguntarMenu("HOS") ){%>
                                            <li><span class="file">
                                                <a 
                                                onclick="javascript:cargarMenu('<%= response.encodeURL("hospitalizacion/ubicacionCama.jsp")%>', '4-0-0', 'ubicacionCama', 'ubicacionCama', 's', 's', 's', 's', 's', 's');"
                                                style="cursor:pointer">Ubicacion Cama
                                                paciente</a></span>
                                            </li>
                                        <% }%>
                                    </ul>
                                </li>
                                <%}%>
                                
                                <% if(beanSession.usuario.preguntarMenu("CRM")|| beanSession.usuario.preguntarMenu("CCP")){%>
                                    <li class="closed"><span class="folderRutas">RUTAS</span>                                                                                        
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("CRMP") ){%> 
                                                <li class="closed"><span class="folder">PARAMETROS</span>
                                                <ul>
                                                    <li><span class="file">
                                                        <a onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/planAtencionRuta.jsp")%>', '4-0-0', 'planAtencionRuta', 'planAtencionRuta', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Plan Atencion Rutas</a></span>
                                                    </li>
                                                    <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/planParametros.jsp")%>', '4-0-0', 'tipoCitaProcedimiento', 'Gestionar Facturacion', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Crear y ubicar tipos de cita">Ruta de atención</a></span></li>
                                                </ul> 
                                            <%}%>
                                                <% if(beanSession.usuario.preguntarMenu("CCP")){%>                                                
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/contactosPaciente.jsp")%>', '4-0-0', 'contactosPaciente', 'contactosPaciente', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="Crear contactos Paciente">Centro de Contacto</a></span></li>

                                                <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/paciente.jsp")%>', '4-0-0', 'paciente', 'paciente', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer"
                                                                title="Usted puede administrar paciente">Paciente</a></span>
                                                    </li>
                                                <%}%>

                                                <!--li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/gestionServicio.jsp")%>', '4-0-0', 'gestionServicio', 'gestionServicio', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="Crear contactos Paciente">Gestión del
                                                            Servicio</a></span></li-->
                                                <% if(beanSession.usuario.preguntarMenu("CRM") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/cerfificarContactos.jsp")%>', '4-0-0', 'cerfificarContactos', 'cerfificarContactos', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="Crear contactos Paciente">Certificar
                                                            Contactos</a></span></li>
                                               
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/enrutarPaciente.jsp")%>', '4-0-0', 'enrutarPaciente', 'enrutarPaciente', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer">Enrutar al Paciente</a></span>
                                                </li>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/listaEsperaAgendamiento.jsp")%>', '4-0-0', 'listaEsperaAgendamiento', 'listaEsperaAgendamiento', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer">Agendas desde lista de
                                                            espera</a></span>
                                                </li><% if(beanSession.usuario.preguntarMenu("CRM") ){%>
                                                
                                    <%}%> 
                                    
                                        <!--li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/gestionPaciente.jsp")%>', '4-0-0', 'rutaPaciente', 'rutaPaciente', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Citas pendientes</a></span>
                                        </li>
                                        <li><span class="file"><a
                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/paciente.jsp")%>', '4-0-0', 'paciente', 'paciente', 's', 's', 's', 's', 's', 's');"
                                            style="cursor:pointer"
                                            title="Usted puede administrar paciente">Paciente</a></span></li-->                                                    
                                        <% }%>
                                        </ul>
                                        </li>
                                <%}%>  

                                <!--li class="closed"><span class="folder">SEGURIDAD PACIENTE</span>
                                <ul>
                                <li class="closed"><span class="folder">Gestión del riesgo</span>
                                <% if(beanSession.usuario.preguntarMenu("311") ){%> 
                                <ul>			
                                <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/eventoAdverso/reportarEventoAdverso.jsp")%>', '4-0-0', 'reportarEventoAdverso', 'reportarEventoAdverso', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede reportar un evento adverso, como primer actor">Reportar</a></span>
                                </li>
                                </ul>                            
                                <%}%>        
                                <% if(beanSession.usuario.preguntarMenu("312") ){%> 
                                <ul>
                                <li><span  class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/eventoAdverso/administrarEventoAdverso.jsp")%>', '4-0-0', 'administrarEventoAdverso', 'administrarEventoAdverso', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede gestionar los eventos adversos que se reportaron con anterioridad, como segundo actor o como tercer actor">Gestionar</a></span>
                                </li>  
                                </ul>                                                        
                                <%}%>                  
                                </li>   
                                <li class="closed"><span class="folder">Documentos</span>
                                <ul>
                                <li><span class="file"><a onClick="abrirTutorial('/clinica/pdf/tutoriales/tallerPoliticaDeSeguridad2012.pdf')" style="cursor:pointer" title="Usted puede leer el taller de politica de seguridad del paciente 2012">Taller política</a></span>
                                </li>  
                                </ul>                                                        
                                </li>   
                                </ul>           
                                </li-->
                                <% if(beanSession.usuario.preguntarMenu("5") ){%>
                                <!--li class="closed"><span class="folder">ENCUESTAS</span>
                                <ul>
                                <% if(beanSession.usuario.preguntarMenu("511") ){%>                          
                                <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("encuestas/responder.jsp")%>', '4-0-0', 'responder', 'responder', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede responder">Responder</a></span>
                                </li>
                                <% }%>   
                                <% if(beanSession.usuario.preguntarMenu("512") ){%>                          
                                <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("encuestas/gestionar.jsp")%>', '4-0-0', 'gestionar', 'gestionar', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" title="Usted puede administrar Proveedores">Gestionar</a></span>
                                </li>
                                <% }%>  
                                </ul>  
                                </li-->
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("cirugia") ){%>
                                    <li class="closed"><span class="folderCirugia">CITAS PROCEDIMIENTOS</span>
                                        <ul>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/horarios.jsp")%>', '4-0-0', 'horarios', 'horarios', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede consultar los horarios de hoy o de un rango">Horarios</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Agenda</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/listaEspera.jsp")%>', '4-0-0', 'listaEsperaCirugia', 'listaEsperaCirugia', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede consultar las listas de espera ">Lista de
                                                        Espera</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionProcedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Gestión Procedimientos</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/principalHC.jsp")%>', '4-0-0', 'principalOrdenes', 'principalOrdenes', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede realizar Evoluciones y Tratamientos">Evolucion
                                                        Cirugia</a></span>
                                        </ul>
                                    </li>
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("6") ){%>
                                    <li class="closed"><span class="folderAtUsuario" title="ASIGNACION DE CITAS">CITAS</span>
                                        <ul>
                                        <% if(beanSession.usuario.preguntarMenu("611") ){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/horarios.jsp")%>', '4-0-0', 'horarios', 'horarios', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Permite gestionar Horarios">Horarios</a></span>
                                            </li>
                                        <% }%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="Permite asignar agenda ">Agenda</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/misCitasHoy.jsp")%>', '4-0-0', 'misCitasHoy', 'misCitasHoy', 's', 's', 's', 's', 's', 's');"
                                                style="cursor:pointer" title="Permite Consultar Agenda">Mis Citas de Hoy</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede consultar las listas de espera ">Lista de
                                                        Espera</a></span>
                                            </li>
                                            <!--
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/consultaCitas.jsp")%>', '6-1-e3', 'consultaCitasPaciente', 'consultaCitasPaciente', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Consulta Citas del paciente</a></span>
                                            </li>
                                            -->
                                        </ul>
                                    </li>
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("ORI") ){%>
                                    <li class="closed"><span class="folderOrientacion">ORIENTACION</span>
                                        <ul>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/asistenciaPacientes.jsp")%>', '4-0-0', 'asistenciaPacientes', 'asistenciaPacientes', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Asistencia de Pacientes</a></span>
                                            </li>

                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/parlante.jsp")%>', '4-0-0', 'parlante', 'parlante', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Parlante</a></span>
                                            </li>

                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/asistenciaDePacientes.jsp")%>', '4-0-0', 'asitenciaMedia', 'asitenciaMedia', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Visitantes</a></span>
                                            </li>

                                        </ul>
                                    </li>
                                    <%}%>
                            
                            
                                <% if(beanSession.usuario.preguntarMenu("10") ){%>
                                    <li class="closed"><span class="folderFacturacion">FACTURACION</span>
                                        <ul>
                                            <!--li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/admision.jsp")%>', '4-0-0', 'admision', 'admision', 's', 's', 's', 's', 's', 's');" style="cursor:pointer" >Admision</a></span>
                                            </li-->
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/admisiones.jsp")%>', '4-0-0', 'admisiones', 'admisiones', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Admisiones</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/soloFactura.jsp")%>', '4-0-0', 'soloFactura', 'soloFactura', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Crear Solo Factura</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede consultar las listas de espera ">Lista de
                                                        Espera</a></span>
                                            </li>
                                            </li>
                                            <% if(beanSession.usuario.preguntarMenu("10_1") ){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/rips.jsp")%>', '4-0-0', 'rips', 'rips', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="Usted puede generar RIPS">Generar
                                                        RIPS</a></span>
                                            </li>
                                            <%}%>
                                            <% if(beanSession.usuario.preguntarMenu("10_1") ){%>
                                            <li class="closed"><span class="folder">PARAMETROS</span>
                                                <ul>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/procedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer">Procedimiento</a></span>
                                                    </li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/articulo.jsp")%>', '4-0-0', 'articulosPlan', 'articulosPlan', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer"
                                                                title="Usted puede ver los articulos asociados a un plan">Articulos</a></span>
                                                    </li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/copago_maximo.jsp")%>', '4-0-0', 'articulosPlan', 'articulosPlan', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer"
                                                                title="Usted puede ver los articulos asociados a un plan">M&aacuteximo
                                                                evento</a></span>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="closed"><span class="folder">CONTRATACION</span>
                                                <ul>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/tarifarios.jsp")%>', '4-0-0', 'admision', 'admision', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer">Tarifarios</a></span>
                                                    </li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/planes.jsp")%>', '4-0-0', 'planes', 'planes', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer">Planes de Contratación</a></span>
                                                    </li>
                                                </ul>
                                            </li>
                                            <%}%>
                                            <li class="closed"><span class="folder">FACTURA</span>
                                                <ul>
                                                    <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturas.jsp")%>',
                                                            '4-0-0', 'Facturas', 'Facturas', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer">Facturas</a></span>
                                                    </li>
                                                    <!--li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturasAnuladas.jsp")%>', '4-0-0', 'Facturas', 'Facturas', 's', 's', 's', 's', 's', 's');" style="cursor:pointer">Facturas Anuladas</a></span>
                                                    </li-->
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/recibos.jsp")%>', '4-0-0', 'recibos', 'recibos', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer">Recibos</a></span>
                                                    </li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/admision.jsp")%>', '4-0-0', 'admision', 'admision', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer">Rips</a></span>
                                                    </li>
                                                </ul>
                                        </ul>
                                    </li>
                                <%}%>
                                
                                <% if(beanSession.usuario.preguntarMenu("ventas") ){%>

                                    <li class="closed"><span class="folderVentas">VENTAS</span>
                                        <ul>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/salidasVentas.jsp")%>', '4-0-0', 'salidasVentas', 'salidasVentas', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="">Facturas</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturasTrabajo.jsp")%>', '4-0-0', 'facturasTrabajo', 'facturasTrabajo', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Trabajos</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/cartera.jsp")%>', '4-0-0', 'cartera', 'cartera', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Cartera</a></span>
                                            </li>
                                            <li class="closed"><span class="folder">PARAMETROS</span>
                                                <ul>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/parametrosTrabajos.jsp")%>', '4-0-0', 'Funcionarios', 'Funcionarios', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="">Funcionarios</a></span>
                                                    </li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/terceros/principalTerceros.jsp")%>', '4-0-0', 'Terceros', 'Terceros', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="">Terceros</a></span>
                                                    </li>
                                                </ul>
                                            </li>

                                        </ul>
                                    </li>
                
                
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("11")){%>
                                    <li class="closed"><span class="folderArchivo">ARCHIVO</span>
                                        <ul>
                                            <% if(beanSession.usuario.preguntarMenu("11")){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede consultar las listas de espera ">Lista de
                                                        Espera</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("archivo/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Agenda Archivo</a></span>
                                            </li>
                                            <%}%>
                                            <!---<% if(beanSession.usuario.preguntarMenu("CCP") ){%>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/paciente.jsp")%>', '6-1-4', 'paciente', 'paciente', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede administrar paciente">Paciente</a></span>
                                            </li>-->
                                            <%}%>
                                        </ul>
                                    </li>
                                <%}%>

                                <li class="closed"><span class="folderFarmacia" title="CLASIFICACION DE INVENTARIO">INVENTARIOS</span>
                                        
                                        <ul>
                                            <li><span class="file"><a
                                                onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/pedidos.jsp")%>', '4-0-0', 'Pedidos', 'Pedidos', 's', 's', 's', 's', 's', 's');"
                                                style="cursor:pointer" title="Usted puede hacer Pedidos">Pedidos</a></span>
                                            </li>
                                            <% if(beanSession.usuario.preguntarMenu("INV") ){%>

                                            <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/solicitudes.jsp")%>', '4-0-0', 'solicitudes', 'solicitudes', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="Respuestas a sus pedidos">Respuesta a pedidos</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/entradasDeBodega.jsp")%>', '4-0-0', 'entradasDeBodega', 'entradasDeBodega', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="940-Usted puede hacer Transacciones en movimiento">Entradas a
                                                        Bodega </a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/salidasDeBodega.jsp")%>', '4-0-0', 'salidasDeBodega', 'salidasDeBodega', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="940-Usted puede hacer Transacciones en movimiento">Salidas de
                                                        Bodega </a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/trasladoBodegas.jsp")%>', '4-0-0', 'trasladoBodegas', 'trasladoBodegas', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="">Traslado entre Bodegas</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/despachoProgramacionProcedimientos.jsp")%>', '4-0-0', 'despachoProgramacionProcedimientos', 'despachoProgramacionProcedimientos', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="">Despacho a Procedimientos</a></span>
                                            </li>
                                            
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/devoluciones.jsp")%>', '4-0-0', 'devoluciones', 'devoluciones', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="">Devoluciones</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/devolucionCompras.jsp")%>', '4-0-0', 'devolucionCompras', 'devolucionCompras', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="">Devoluciones en Compras</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/documentos/principalDocumentos.jsp")%>', '4-0-0', 'principalDocumentos', 'principalDocumentos', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="">Documentos Historicos</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/kardex/principalKardex.jsp")%>', '4-0-0', 'principalKardex', 'principalKardex', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="">Kardex</a></span>
                                            </li>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/codigoBarras.jsp")%>', '4-0-0', 'codigoBarras', 'codigoBarras', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer" title="">Codigo de Barras</a></span>
                                            </li>
                                            <% if(beanSession.usuario.preguntarMenu("941") ){%>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/inventario.jsp")%>', '4-0-0', 'inventario', 'inventario', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer"
                                                            title="941-Usted puede hacer inventario en movimiento ">Hacer
                                                            Inventario</a></span>
                                                </li>
                                                <li><span class="file"><a
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/recepcionTecnica.jsp")%>', '4-0-0', 'recepcionTecnica', 'recepcionTecnica', 's', 's', 's', 's', 's', 's');"
                                                            style="cursor:pointer" title="">Recepcion Tecnica</a></span>
                                             <% }%>
                                                <li class="closed"><span class="folder">PARAMETROS</span>
                                                    <ul>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/articulo/principalArticulo.jsp")%>', '4-0-0', 'principalArticulo', 'principalArticulo', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer"
                                                                    title="943-Usted puede ver los articulos">Articulo</a></span>
                                                        </li>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/canastas/plantillaCanastas.jsp")%>', '4-0-0', 'plantillaCanastas', 'plantillaCanastas', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer"
                                                                    title="943-Usted puede ver las canastas">Crear
                                                                    Canastas</a></span>
                                                        </li>
                                                        <li><span class="file"><a
                                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/bodega/crearBodega.jsp")%>', '4-0-0', 'CrearBodega', 'CrearBodega', 's', 's', 's', 's', 's', 's');"
                                                                    style="cursor:pointer"
                                                                    title="Usted puede crear y modificar bodegas">Crear
                                                                    Bodegas</a></span>
                                                        </li>
                                                    </ul>
                                                </li>

                                            <% }%>
                                        </ul>
                                </li>
                               
    
                                <% if(beanSession.usuario.preguntarMenu("CRM") ){%>
                                <li class="closed"><span class="folderGeo">GEO-REFERENCIA</span>
                                    <ul>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("encuesta/geoAreas.jsp")%>', '4-0-0', 'geoAreas', 'geoAreas', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer">Gestión de Areas</a></span></li>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/gestionServicio.jsp")%>', '4-0-0', 'gestionServicio', 'gestionServicio', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer">Asignar Medico Area</a></span>
                                        </li>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/planParametros.jsp")%>', '4-0-0', 'tipoCitaProcedimiento', 'Gestionar Facturacion', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer">Gestion Encuestas</a></span></li>
                                    </ul>
                                </li>
                                <%}%>  

                                <% if(beanSession.usuario.preguntarMenu("INFO") ){%>
                                    <li class="closed"><span class="folderNotificaciones">NOTIFICACIONES</span>
                                        <ul>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/crearNotificacion.jsp")%>', '4-0-0', 'crearNotificacion', 'crearNotificacion', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Crear</a></span>
                                            </li>

                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/leerNotificacion.jsp")%>', '4-0-0', 'leerNotificacion', 'leerNotificacion', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer">Leer</a></span>
                                            </li>

                                        </ul>
                                    </li>
                                <%}%>
                                    
                                <!--li class="closed"><span class="folder">MI AGENDA</span>
                                <ul>
                                <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/notificacion.jsp")%>',
                                '8-1-3', 'notificacion', 'notificacion', 's', 's', 's', 's', 's', 's');"
                                style="cursor:pointer" >noti</a></span>
                                </li>
                                </ul>
                                </li-->
                                <% if(beanSession.usuario.preguntarMenu("INFO") ){%>
                                    <li class="closed"><span class="folderInformes">INFORMES</span>
                                        <ul>
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("reportes/reportes_parametros.jsp")%>', '4-0-0', 'reportes', 'reportes', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede consultar Reportes de todas las areas">Listado de
                                                        Informes</a></span>
                                            </li>
                                            <!-- VERIFICAR notificacion.jsp
                                            <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/notificacion.jsp")%>', '4-0-0', 'reportes', 'reportes', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Usted puede consultar Reportes de todas las areas">notificar</a></span>
                                            </li>
                                            -->

                                        </ul>
                                    </li>
                                <%}%>

                                <% if(beanSession.usuario.preguntarMenu("1") ){%>
                                <li class="closed"><span class="folderCalidad">HELP DESK</span>
                                    <ul>
                                        <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/evento/crearEvento.jsp")%>',
                                                '2-1-3', 'crearEvento', 'crearEvento', 's', 's', 's', 's', 's', 's');"
                                                style="cursor:pointer" title="Usted puede crear un evento">Reportar
                                                Evento</a></span>
                                        </li>
                                        <% if(beanSession.usuario.preguntarMenu("GES_EVEN") ){%>
                                        <li><span class="file"><a
                                                    onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/evento/gestionarEventos.jsp")%>', '4-0-0', 'gestionarEvento', 'gestionarEvento', 's', 's', 's', 's', 's', 's');"
                                                    style="cursor:pointer" title="Usted puede Gestionar Eventos">Gestion de
                                                    Evento</a></span></li>
                                        <% }%>
                                        <!--      <li class="closed"><span class="folder">No conformidades</span>
                                        <% //if(beanSession.usuario.preguntarMenu("211") ){%> 
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/reportarNoConformidad.jsp")%>','2-1-1','reportarNoConformidad','reportarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede reportar una No Conformidad con destino a un proceso">Reportar</a></span></li>
                                        </ul>
                                        <%//}%>                           
                                        <% //if(beanSession.usuario.preguntarMenu("212") ){%> 
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/administrarNoConformidad.jsp")%>','2-1-2','administrarNoConformidad','administrarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede Administrar lider de calidad (Clasificar, valorar, asignar y determinar la Gestión)">Personas Procesos</a></span></li>
                                        </ul> 
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/administrarNoConformidad.jsp")%>','2-1-2','administrarNoConformidad','administrarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede Administrar lider de calidad (Clasificar, valorar, asignar y determinar la Gesti�n)">Administrar</a></span></li>
                                        </ul>   
                                        <ul>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/noconformidades/administrarNoConformidad.jsp")%>','2-1-2','administrarNoConformidad','administrarNoConformidad','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede gestionar una No Conformidad con destino a su proceso, o direccionarla asi como tambi�n generar Planes de acci�n y seguimientos">Gestionar</a></span></li>
                                        </ul>                            
                                        <%//}%>                                                                                  
                                        </li> -->
                                    </ul>
                                        <!--<ul>
                                        <li><span class="file"><a onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/planMejora/administrarPlanMejora.jsp")%>','2-1-3','administrarPlanMejora','administrarPlanMejora','s','s','s','s','s','s');" style="cursor:pointer" title="Usted puede administrar un  Plan De Mejora">Oportunidad de mejora</a></span></li>
                                        </ul>  
                                        -->
                                        <% //if(beanSession.usuario.preguntarMenu("i19") ){%>
                                        <!--             <UL>              
                                        <li class="closed"><span class="folder">Indicadores</span>
                                        <ul>
                                        <% if(beanSession.usuario.preguntarMenu("i19_1") ){%>                          
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("indicador/fichaTecnica.jsp")%>','19-1-1','fichaTecnica','fichaTecnica','s','s','s','s','s','s');" style="cursor:pointer" title="i19_1:Usted puede gestionar las Ficha Tecnicas de Indicadores">Ficha Tecnica</a></span>
                                        </li>
                                        <li><span class="file" ><a onclick="javascript:cargarMenu('<%= response.encodeURL("indicador/indicador.jsp")%>','19-1-2','indicador','indicador','s','s','s','s','s','s');" style="cursor:pointer" title="i19_1Usted puede gestionar el Indicadores">Gesti�n Indicador</a></span>
                                        </li>
                                        <% }%>  
                                        </ul>  
                                        </li> 
                                        </UL>  -->
                                        <%//}%>
                                </li>
                                <% }%>

                                <% if(beanSession.usuario.preguntarMenu("1") ){%>
                                    <!-- 1 -->
                                    <li class="closed"><span class="folderAdministracion" title="ADMINISTRADOR DEL SISTEMA">ADMINISTRACION</span>
                                        <ul>
                                            <li class="closed"><span class="folder">Seguridad</span>
                                                <ul>        
                                                    <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/crearUsuario.jsp")%>', '4-0-0', 'crearUsuario', 'Creacion de Usuarios', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Gestionar Usuarios">Gestionar Usuarios</a></span>
                                                    </li>
                                                    <li><span class="file"><a
                                                        onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/usuario.jsp")%>', '4-0-0', 'usuario', 'Gestionar Información de Usuarios', 's', 's', 's', 's', 's', 's');"
                                                        style="cursor:pointer"
                                                        title="Crear Usuarios">Usuarios</a></span>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="closed"><span class="folder">Parametros</span>
                                                <ul>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/tipoCitaProcedimiento.jsp")%>', '4-0-0', 'tipoCitaProcedimiento', 'Gestionar Facturacion', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="Gestionar Usuarios">Tipo
                                                                Cita Procedimiento</a></span></li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/sedeEspecialidades.jsp")%>', '4-0-0', 'sedeEspecialidades', 'sedeEspecialidades', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="Gestionar Usuarios">Sede Especialidades</a></span></li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/piePagina.jsp")%>', '4-0-0', 'Pie de Pagina', 'Gestionar Información de xxx', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="Gestionar Usuarios">Pie
                                                                pagina</a></span></li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/centroCosto.jsp")%>', '4-0-0', 'Centro de costo', 'Gestionar Información de xxx', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer" title="Gestionar Usuarios">Centro
                                                                de costo</a></span></li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaFormulario.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer">Plantillas</a></span>
                                                    </li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaFolio.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer">Plantillas Folio</a></span>
                                                    </li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaEncuesta.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer">Plantillas Encuesta</a></span>
                                                    </li>
                                                    <li><span class="file"><a
                                                                onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/encuestaTipoFolio.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');"
                                                                style="cursor:pointer">Encuesta tipo folio</a></span>
                                                    </li>
                                                    
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                <%}%>

                                
                                    <!-- Desbilitado / validar cierre de sesion con borrado de cche en el navegador -->
                                    <!-- <li class="closed"><span class="folderCierre">Cerrar Sesion</span>
                                        <ul>
                                            <li><span class="file">
                                                <a </span>
                                                <a href="http://190.60.242.160:8383/clinica/paginas/login.jsp" 
                                                onclick='NoBack();'  >Salir del aplicativo</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <script type="text/javascript">
                                
                                    </script> -->
                              

                              </ul>
                             <!--</li> -->
                           </DIV>
                       </div>

                            <% if(beanSession.cn.getNombreBd().equals("clinica")){ %>
                                <div id="main"
                                    style="width:1494px; left:150px; top:50px; height:4000px;  background-image:url(../utilidades/imagenes/menu/fondoProduccion.jpg); background-repeat:no-repeat;   "
                                    align="center"></div>
                                <% }else { %>
                                <div id="main"
                                    style="width:1494px; left:150px; top:50px; height:4000px; background-image:url(../utilidades/imagenes/menu/fondoProduccion.jpg); background-repeat:no-repeat;  "
                                    align="center"></div>
                                <% }%>
                                <DIV
                                    style="top:701px; display:none; left:10px; position:absolute; color:#3C3; font-weight:bold; width:1136px; height:9px; background-color:#8c0050">
                                </DIV>
                                <div class="mensaje" id="mensaje"
                                    style="top:666px; left:7px; position:absolute; color:#FFFFFF; font-weight:bold ">
                                </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<!--
<center><input id="btn_SALIR"  type="button" class="blue buttonini" value="CERRAR SESION" style="width:190px"  onClick="javascript:salir();" tabindex="999"/></center>
-->
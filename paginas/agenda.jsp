 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1200px"  align="center"   border="0" cellspacing="0" cellpadding="1">
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="AGENDA CIRUGIA" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO  aqui empieza el cambio-->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td>
              <div style="overflow:auto; height:1100px; width:100%"   id="divContenido">   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
              
               <table width="100%">  
                 <tr>
                  <td valign="top" width="90%" >             
                   <table width="100%" cellpadding="0" cellspacing="0" border="1">
                     <tr class="titulos">
                         <td width="30%">Especialidad</td> 
                         <td width="30%">Sub Especialidad</td>                
                         <td width="30%">Cirujano</td>                           
                         <td width="10%">&nbsp;</td>                                                    
                     </tr>     
                     <tr class="estiloImput" >
                         <td><select id="cmbIdEspecialidad" style="width:80%"  tabindex="14" onchange="asignaAtributo('cmbIdSubEspecialidad', '', 0);asignaAtributo('cmbIdProfesionales', '', 0);cargarTipoCitaSegunEspecialidad('cmbTipoCita','cmbIdEspecialidad')" >	                                        
                                <option value=""></option>
                                  <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(118);	
                                         ComboVO cmb32; 
                                         for(int k=0;k<resultaux.size();k++){ 
                                               cmb32=(ComboVO)resultaux.get(k);
                                  %>
                                <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>"><%= cmb32.getDescripcion()%></option>
                                              <%}%>						
                             </select>	                   
                         </td>   
                         <td>
                             <select size="1" id="cmbIdSubEspecialidad" style="width:80%"   onfocus="cargarSubEspecialidadDesdeEspecialidad('cmbIdEspecialidad', 'cmbIdSubEspecialidad')" onchange="buscarAGENDA('listDias','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" style="width:40%"  tabindex="117"  >	                                        
                                <option value=""></option>
                             </select>    
                         </td> 
                         <td><select size="1" id="cmbIdProfesionales" style="width:80%"  onfocus="cargarProfesionalesDesdeEspecialidad(this.id)" tabindex="14" onchange="buscarAGENDA('listDias')">	                                        
                             <option value="" ></option>                                  
                             </select>	                   
                         </td>                
                     </tr>  
                     <tr class="estiloImput" >
                         <td>
                             &nbsp;&nbsp;&nbsp;Sala:
							 <select size="1" id="cmbIdSala" style="width:30%"  tabindex="14" >	                                        
                              <option value="1" >Sala 1</option>                                  
                             </select>	 
                             
                             &nbsp; &nbsp;Sede:
                            <select size="1" id="cmbSede" style="width:40%" title="GD38"  onchange="buscarAGENDA('listDias','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" >
					                <% resultaux.clear();
					                   resultaux=(ArrayList)beanAdmin.combo.cargar(557);	
					                   ComboVO cmbSede; 
					                   for(int k=0;k<resultaux.size();k++){ 
					                         cmbSede=(ComboVO)resultaux.get(k);
					                %>
					                <option value="<%= cmbSede.getId()%>" title="<%= cmbSede.getTitle()%>"><%= cmbSede.getDescripcion()%></option>
					                <%}%>                                 
					            </select>                                                           
                         </td>
                         <td>
                            <img width="15" height="15" style="cursor:pointer" src="/clinica/utilidades/imagenes/acciones/izquierda.png" onclick="irMesAnteriorAgenda()">&nbsp;
                                <label id="lblMes"></label>/<label id="lblAnio"></label>
                            <img width="15" height="15" style="cursor:pointer" src="/clinica/utilidades/imagenes/acciones/derecha.png" onclick="irMesSiguienteAgenda()">&nbsp;              
                           
                         </td>
                         <td>
                              <TABLE>
                                <TR>
                                  <TD width="50%">
                                       <H3 align="center"><label id="lblFechaSeleccionada"></label></H3>                                  
                                  </TD>
                                  <TD width="50%" align="right">
                                       Exito
                                       <select size="1" id="cmbExito" style="width:50%;"  onchange="buscarAGENDA('listDias','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="14"  >	                                        
                                          <option value="S">Si</option>
                                          <option value="N">No</option>                                
                                       </select>                                
                                  </TD>                                
                                </TR>
                              </TABLE>
                         </td> 
                         <td>&nbsp;
                         </td>
                     </tr>    
                     
                     <tr class="titulos"  >   
                        <td colspan="3" >
                            <div id="divListAgenda" style="overflow:auto; height:100%; width:100%">
                                <table id="listAgenda" class="scroll"></table>  
                            </div>    
                        </td>
                     </tr> 
                   </table>  
                   </td> 
                   <td width="12%" valign="top">  
                      <input name="btn_cerrar" type="button" align="right" value="CONSULTAR"  onclick="buscarAGENDA('listDias','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"/>                   
                     <div id="divListadoDias" style="width:100%" >
                      <table id="listDias" class="scroll"></table>  
                     </div> 
                   </td>
                 </tr>
               </table>   
                  
              </div> 
         </td>
       </tr>
      </table>
      <input type="hidden" id="txtIdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>"/>    
   </td>
 </tr>  
</table>

  <div id="divVentanitaAgenda" style="position:absolute; display:none; background-color:#E2E1A5; top:100px; left:70px; width:90%; height:90px; z-index:999">
        <table width="100%"    class="fondoTabla">           
           <tr class="estiloImput" >
               <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAgenda')" /></td>  
               <td  align="CENTER" colspan="2" ><label id="lblUsuariosDato"></label></td>
               <td align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAgenda')" /></td>  
           <tr>     
           <tr class="titulos" >
               <td width="25%" >ID AGENDA DETALLE</td>  
               <td width="25%" >HORA</td>                 
               <td width="25%" >DURACION MINUTOS</td>  
               <td width="25%" >FECHA</td>                                   
           <tr>           
           <tr class="estiloImput" >
               <td ><label id="lblIdAgendaDetalle"></label></td>            
               <td ><label id="lblHora"></label></td>
               <td ><label id="lblDuracion"></label></td> 
               <td ><label id="lblFechaCita"></label></td> 
           <tr> 
           <tr class="titulos" >
               <td colspan="1" >
               <img src="/clinica/utilidades/imagenes/flechas/listaEspera.png" width="20px" height="20px" onclick="abrirVentanaDesdeListaEspera();" align="left" title="Boton 2= b611" />               
               Lista Espera</td>             
               <td colspan="3" >PACIENTE</td>  
           <tr>   
           <tr class="estiloImput" >        
               <td colspan="1" >
                     <label id="lblIdListaEspera"></label>
               </td>      
               <td colspan="3" >
                    <input type="text" size="110"  maxlength="210"  id="txtIdBusPaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPaciente',307)"  style="width:70%"  tabindex="99" /> 
                    <input name="btn_busAgenda" type="button" class="small button blue" value="BUSCAR." onclick="buscarInformacionBasicaPaciente()"  />  
               </td>    
           </tr> 
 
           <tr class="estiloImputListaEspera">
             <td colspan="4">
<!-- DATOS BASICOS PACIENTE-->              
               <table width="100%"  cellpadding="0" cellspacing="0" >
                 <tr>
                   <td width="10%"  bgcolor="#FD93B8">Tipo Id</td>
                   <td  bgcolor="#FD93B8">Identificacion</td>
                   <td >Apellidos</td>  
                   <td >Nombres</td>                      
                   <td >Fech.Nacim</td>   
                   <td >Sexo</td>                                            
                 </tr>               
               
                 <tr>
                   <td width="10%" bgcolor="#FD93B8">
                   <img src="/clinica/utilidades/imagenes/acciones/buscar.png" width="20px" height="20px" onclick="guardarYtraerDatoAlListado('buscarIdentificacionPaciente'); limpTablas('listDocumentosHistoricos');ocultar('divArchivosAdjuntos')" align="center" title="B8t7" />                                     
					             <select size="1" id="cmbTipoId" style="width:55%" tabindex="100" >
                       <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                               ComboVO cmb105; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb105=(ComboVO)resultaux.get(k);
                        %>
                      <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>"><%= cmb105.getId()+"  "+cmb105.getDescripcion()%></option>
                                    <%}%>						
                   </select></td> 
                   <td  bgcolor="#FD93B8"><input  type="text" id="txtIdentificacion"  style="width:80%"   size="20" maxlength="20"  onKeyPress="javascript:checkKey2(event);" tabindex="101"  />
                   </td>
                   <td>
                     <input type="text" id="txtApellido1" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);" tabindex="102" style="width:45%" />
                     <input type="text" id="txtApellido2" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);" tabindex="103" style="width:45%" />
                   </td> 
                   <td>
                     <input type="text" id="txtNombre1" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  tabindex="104" style="width:45%" />
                     <input type="text" id="txtNombre2" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);" tabindex="105" style="width:45%" />  
                   </td>   
                   <td><input id="txtFechaNac"  type="text"  size="10"  maxlength="10" tabindex="106" /> </td>                      	 
                   <td> <select size="1" id="cmbSexo" style="width:95%" tabindex="107">
                          <option value=""></option>
                              <%  resultaux.clear();
                                  resultaux=(ArrayList)beanAdmin.combo.cargar(110);	
                                  ComboVO cmbS; 
                                  for(int k=0;k<resultaux.size();k++){ 
                                    cmbS=(ComboVO)resultaux.get(k);
                              %>
                          <option value="<%= cmbS.getId()%>" title="<%= cmbS.getTitle()%>"><%=cmbS.getDescripcion()%></option>
                          <%}%>						
                     </select>
                   </td>
                 </tr>
               </table>       
<!-- FIN DATOS BASICOS -->        
             </td>  
           <tr> 

           <tr class="titulos" >
               <td>Municipio</td> 
               <td>Direcci&oacute;n</td>   
               <td>Acompa&ntilde;ante</td>                    
               <td>Celular*1&nbsp;&nbsp;&nbsp;&nbsp;Tel&eacute;fono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Celular2</td>                                       
           <tr>     
           <tr class="estiloImput" >
               <td><input type="text" id="txtMunicipio" size="60"  maxlength="60"  onfocus="llamarAutocomIdDescripcionConDato('txtMunicipio',310)" style="width:80%"  tabindex="108"  /></td> 
               <td><input type="text" id="txtDireccionRes" size="100" maxlength="100"  style="width:99%"  onkeypress="javascript:return teclearsoloan(event);" tabindex="109" /></td>   
               <td><input type="text" id="txtNomAcompanante"  size="100" maxlength="100"  style="width:95%"  onkeypress="javascript:return teclearExcluirCaracter(event);"  tabindex="110" /> </td>                    
               <td>
                   <input type="text" id="txtCelular1" size="30"  style="width:30%" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  tabindex="111"/>
                   <input type="text" id="txtTelefonos" size="100"  style="width:30%" maxlength="100" onkeypress="javascript:return teclearsoloan(event);"  tabindex="112"/>                    
                   <input type="text" id="txtCelular2" size="30"  style="width:30%" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  tabindex="113"/>
               </td>                                       
           <tr>
           <tr class="titulosListaEspera" >
               <td colspan="2">Email</td> 
               <td colspan="2">&nbsp;</td>                
           </tr>           
           <tr class="estiloImputListaEspera" >
             <td colspan="2"><input type="text" id="txtEmail" size="60"  maxlength="60"  style="width:60%"  tabindex="108"/> </td>                 
             <td colspan="2">&nbsp;</td>              
           </tr>            
           <tr class="titulos" >
               <td colspan="6"  bgcolor="#CCCCCC" height="5px">
               <% if(beanSession.usuario.preguntarMenu("6_b1") ){%>
				 <input id="btnCrearNuevoPaciente" type="button" class="small button blue" title="FUNCIONA45" value="Crear nuevo paciente" onclick="modificarCRUD('crearNuevoPaciente')"  />              
                 <input name="btn_MODIFICAR" class="small button blue" value="MODIFICAR PACIENTE" onclick="modificarCRUD('pacienteModifica');" type="button">               
                <%}%>
               </td>             
           <tr>                     
           <tr class="titulos" >
               <td colspan="2">Administradora</td>                    
               <td>N&uacute;mero autorizaci&oacute;n y Fecha Vig
               <img width="20px" height="20px" align="middle" title="BA32" onclick="modificarCRUD('modificarAutorizacion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" src="/clinica/utilidades/imagenes/acciones/modificar.png">
               </td>    
               <td>Fecha Paciente</td>                  
           </tr>  
           <tr class="estiloImput" >
               <td colspan="2"><input type="text"  id="txtAdministradora1"  size="60"  maxlength="60"   onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradora1',308)" style="width:98%"  tabindex="114" />                                  	 
               </td>   
               <td><input  type="text" id="txtNoAutorizacion"  size="30" maxlength="100"  style="width:40%" onKeyPress="javascript:return teclearExcluirCaracter(event,'%');" tabindex="115"/>
                   <input id="txtFechaVigencia"  type="text"  size="10"  maxlength="10" tabindex="116" />               
               </td>  
               <td><input id="txtFechaPacienteCita"  type="text"  size="10"  maxlength="10" tabindex="116" />
               </td>                    
           </tr> 
           <tr class="titulos" >
               <td colspan="2">Instituci&oacute;n que remite</td>                    
               <td colspan="2">Observaciones</td>   
           </tr>  
           <tr class="estiloImput" >
               <td colspan="2"><input type="text" id="txtIPSRemite"   size="60"  maxlength="60"   onkeypress="llamarAutocomIdDescripcionConDato('txtIPSRemite',313)" style="width:98%"  tabindex="114" />                                  	 
               </td>   
               <td colspan="2"><input type="text" id="txtObservacion"  size="200" maxlength="3000"  style="width:98%"  onKeyPress="javascript:return teclearExcluirCaracter(event,'%');" tabindex="115"/></td>                      
           </tr>              
                           
           <tr class="estiloImput" >
               <td>Tipo:
               <select id="cmbTipoCita"  style="width:65%" onfocus="cargarTipoCitaSegunEspecialidad('cmbTipoCita','cmbIdEspecialidad')" tabindex="116">	                                        
                     <option value=""></option>
               </select>
               </td>
               <td>
               Estado:
               <input type="hidden" id="txtEstadoCita" />               
               <select id="cmbEstadoCita" onfocus="limpiarDivEditarJuan('limpiarMotivoCX')" style="width:70%" tabindex="117" >
                    <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(107);	
                               ComboVO cmbEst; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbEst=(ComboVO)resultaux.get(k);
                        %>
                      <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>"><%= cmbEst.getId()+"  "+cmbEst.getDescripcion()%></option>
                               <%}%>						
                   </select>
               </td>               
               <td width="26%" colspan="2">
                 Motivo:
                 <select id="cmbMotivoConsulta" style="width:50%" tabindex="118"  onfocus="comboDependiente('cmbMotivoConsulta','cmbEstadoCita','96')">
                    <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(106);	
                               ComboVO cmbM; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbM=(ComboVO)resultaux.get(k);
                        %>
                      <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>"><%= cmbM.getId()+"  "+cmbM.getDescripcion()%></option>
                               <%}   %>						
                   </select> 
               </td>                    
           </tr> 
           <tr class="camposRepInp" >
               <td width="100%" colspan="4">Informaci&oacute;n de la cirugia</td> &nbsp;
           <tr>
           <tr class="estiloImput" >
               <td colspan="4">

                   <TABLE width="100%">           
                       <tr class="titulos" >
                           <td width="25%">Lente intraocular </td>                    
                           <td width="25%">Tipo Anestesia</td>   
                           <td width="50%" colspan="2">Anestesiologo</td>
                       </tr>                 
                       <tr class="estiloImput" >
                           <td><input type="text" id="txtNoLente"   size="60"  maxlength="60"  style="width:80%"  tabindex="119" />
                               <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaNoLente"  onclick="traerVentanitaCondicion1Autocompletar(this.id, 'txtIdBusPaciente','divParaVentanitaCondicion1','txtNoLente','17')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
							   <div id="divParaVentanitaCondicion1"></div>
                           </td>  
                           <td>
                             <select id="cmbTipoAnestesia" style="width:80%" tabindex="120" >
                               <option value=""></option>
                                  <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(119);	
                                         ComboVO cmbTA; 
                                         for(int k=0;k<resultaux.size();k++){ 
                                               cmbTA=(ComboVO)resultaux.get(k);
                                  %>
                               <option value="<%= cmbTA.getId()%>" title="<%= cmbTA.getTitle()%>"><%= cmbTA.getDescripcion()%></option>
                                         <%}%>
                             </select>               
                           </td>  
                           <td colspan="2"><input type="text" id="txtAnestesiologo"  size="200" maxlength="200"  style="width:98%"  onkeypress="llamarAutocomIdDescripcionConDato('txtAnestesiologo',298)" tabindex="121"/></td>
                       </tr> 

                       <tr class="titulos" >
                           <td width="35%" colspan="2">Instrumentador</td>
                           <td width="35%" colspan="2">Circulante</td>                                 
                       </tr>                 
                       <tr class="estiloImput" >
                           <td colspan="2"><input type="text" id="txtInstrumentador"  size="200" maxlength="200"  style="width:98%"  onkeypress="llamarAutocomIdDescripcionConDato('txtInstrumentador',298)" tabindex="121"/></td>
                           <td colspan="2"><input type="text" id="txtCirculante"  size="200" maxlength="200"  style="width:98%"  onkeypress="llamarAutocomIdDescripcionConDato('txtCirculante',298)" tabindex="122"/></td>               
                       </tr>                       
                       <tr class="titulosCentrados" >
                           <td width="100%" colspan="4">Informaci&oacute;n de Procedimientos</td> 
                       <tr>
                       <tr class="titulos" >
                         <td colspan="1">Sitio Quirurgico</td>
                         <td colspan="2">Procedimiento CUPS</td>
                         <td colspan="1">Observaciones Procedimiento</td> 
                       </tr> 
                       <tr class="titulos" >
                           <td>
                             <select id="cmbIdSitioQuirurgico" style="width:80%" tabindex="123" >
                               <option value=""></option>
                                  <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(120);	
                                         ComboVO cmbSQ; 
                                         for(int k=0;k<resultaux.size();k++){ 
                                               cmbSQ=(ComboVO)resultaux.get(k);
                                  %>
                               <option value="<%= cmbSQ.getId()%>" title="<%= cmbSQ.getTitle()%>"><%= cmbSQ.getDescripcion()%></option>
                                         <%}%>
                             </select>                            
                           </td>
                           <td colspan="2"><input type="text" id="txtIdProcedimiento"  size="200" maxlength="200"  style="width:98%" onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimiento',324)" tabindex="124"/>
                           </td>
                           <td>                     
                             <input type="text" id="txtObservacionProcedimientoCirugia"  size="200" maxlength="200"  style="width:75%" tabindex="124"/>                     
							 <input name="btn_crear_cita" type="button" class="small button blue" value="Adici&oacute;n Proc." title="BT722" onclick="modificarCRUD('listCitaCirugiaProcedimiento')" tabindex="309"  />                               
                           </td>
                       </tr>  
                       <tr class="camposRepInp" >
                           <td width="100%" colspan="4">PROCEDIMIENTOS CIRUGIA</td> 
                       <tr>                   
                       <tr class="titulos">   
                          <td width="100%" colspan="4">
                          <div style="overflow:auto; height:80px; width:100%" id="divCitaCirugiaProcedimientos">             
                              <table id="listCitaCirugiaProcedimiento" class="scroll"></table>  
                          </div>    
                          </td>
                       </tr>                                     
                   </TABLE>

               </td>
            </tr>              
                         
           <tr>
             <td colspan="4" align="center" >
             <input name="btn_crear_cita" type="button" class="small button blue" title="BT9U7" value="GUARDAR CITA CIRUGIA" onclick="guardarUnaCitaAgenda('crearCitaCirugia')" />                                                      
             <input type="hidden" id="txtColumnaIdSitioQuirur" /> 
             <input type="hidden" id="txtColumnaIdProced" />
             <input type="hidden" id="txtColumnaObservacion" />             
             </td>
           </tr> 
           <tr class="titulos" >   
 	          <td colspan="4">
                  <table id="listHCPaciente" class="scroll"></table>  
              </td>
           </tr>                    
        </table> 
  </div>


 <div id="divVentanitaLenteBiometria"  style="display:none; z-index:3010; top:600px; left:-211px;">  
      <div class="transParencia" style="z-index:3011; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
      </div>  
      <div  style="z-index:3012; position:absolute; top:280px; left:70px; width:95%">  
          <table width="95%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla">
             <tr class="estiloImput" >
                <td align="left"><input name="btn_cerrar" type="button" align="right" title="VR32" value="CERRAR" onclick="ocultar('divVentanitaLenteBiometria')" /></td>  
                <td>&nbsp;</td>               
                <td align="right"><input name="btn_cerrar" type="button" align="right" title="VR32"  value="CERRAR" onclick="ocultar('divVentanitaLenteBiometria')" /></td>  
            <tr>    
            <tr>
              <td colspan="3">
                                       
                <table width="100%" align="center">
                      <tr class="titulos"> 
                        <td>
                           No Lente=
                          <input type="text" id="txt_BIOM_C10" maxlength="60" style="width:70%"/>
                          <% if(beanSession.usuario.preguntarMenu("lente") ){%> 
                          <img src="/clinica/utilidades/imagenes/botones-estandar/Guardar.gif" title="G877" width="20px" height="20px" onclick="modificarCRUD('guardarNoLente');">
                        <% } %> 
                        </td>
                     </tr>  
                     <tr class="estiloImput"> 
                        <td>
                           <input id="btn_limpia" title="BTUU8" class="small button blue" type="button" value="Traer"  onclick="traerTextoPredefinidoBiometria()">              
                        </td>
                        <td colspan="3" align="right">
                           <textarea type="text" id="txt_gestionarPaciente" rows="11"   maxlength="4000" style="width:95%"   tabindex="108" > </textarea>                                                
                        </td>                   
                        <td colspan="1" align="left">
                           <% if(beanSession.usuario.preguntarMenu("lente") ){%> 
                         <input id="btnGestionPaciente" class="small button blue" type="button" title="bt487" value="GESTIONAR" onclick="modificarCRUD('gestionPacienteFolio');"> 
                            <% } %>                                
                        </td>    
                      </tr>     
                </table> 
                                                       
              </td>   
            </tr>   
          </table> 
      <input type="text" id="txtIdEsdadoDocumento" style="width:1%" disabled="disabled" /> 
      <label id="lblNomEstadoDocumento" style="size:1PX"></label>                           
      <label id="lblTipoDocumento"></label>
      <label id="lblDescripcionTipoDocumento"></label>
      <label id="lblIdDocumento"></label> 

<!--
    asignaAtributo('lbl_idFecha',Fecha,estad);  
    asignaAtributo('lblIdAdmision',idAdmision,estad);         
    asignaAtributo('txtIdProfesionalElaboro',idProfesionalElaboro,estad); 
-->      
          
      </div>     
  </div>



  <div id="divVentanitaListaEspera"  style="display:none; z-index:1000; top:1px; left:-211px;">
      <div class="transParencia" style="z-index:1001; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
      </div>  
      <div class="cnt" style="z-index:1002; position:absolute; top:100px; left:60px;">  
       <table width="100%"  border="1" cellpadding="0" cellspacing="0" class="fondoTabla">
           <tr class="estiloImput" >
               <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaListaEspera')" /></td>  
               <td>&nbsp;</td>             
               <td>&nbsp;</td>
               <td>&nbsp;</td>               
               <td align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaListaEspera')" /></td>  
           </tr>           
           <tr class="titulos">
               <td width="40%">Paciente</td> 
               <td width="15%">Dias Espera</td> 
               <td width="15%">Estado LE</td>    
               <td width="15%">Sin Cita</td>                           
               <td width="15%">Tipo Cita</td>                                          
           </tr>     
           <tr class="estiloImput" >
               <td>
                   <input type="text" size="110"  maxlength="210"  id="txtIdBusPacienteLE" onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPacienteLE',307)"  style="width:80%"  tabindex="200" /> 
               </td> 
               <td>
               	   <select id="cmbDiasEsperaLE" size="1" style="width:98%"  tabindex="201" >	                                        
                        <%     
						   resultaux.clear();
						   resultaux=(ArrayList)beanAdmin.combo.cargar(115);	
						   ComboVO cmbDias; 
						   for(int k=0;k<resultaux.size();k++){ 
								 cmbDias=(ComboVO)resultaux.get(k);
                        %>
                      <option value="<%= cmbDias.getId()%>" title="<%= cmbDias.getTitle()%>"><%= cmbDias.getDescripcion()%></option>
                  <%}%>               
               </td>
               <td>
                   <select id="cmbEstadoLE" style="width:80%" tabindex="202" >	                                        
                        <%  resultaux.clear();
                            resultaux=(ArrayList)beanAdmin.combo.cargar(114);	
                            ComboVO cmbELE; 
                            for(int k=0;k<resultaux.size();k++){ 
                               cmbELE=(ComboVO)resultaux.get(k);
                        %>
                        <option value="<%= cmbELE.getId()%>" title="<%= cmbELE.getId()%>"><%= cmbELE.getDescripcion()%></option>
                       <%}%>						
                   </select>               
               </td>	 
               <td>
                   <select id="cmbBusSinCita" style="width:70%" tabindex="203" >
                       <option value="S">Sin cita</option>                                      
                       <option value="C">Con cita</option>
                   </select>               	                   
               </td>    
               <td><select id="cmbTipoCitaLE" style="width:80%" tabindex="204" >	                                        
                     <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(108);	
                               ComboVO cmbTLE; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbTLE=(ComboVO)resultaux.get(k);
                        %>
                      <option value="<%= cmbTLE.getId()%>" title="<%= cmbTLE.getId()%>"><%= cmbTLE.getDescripcion()%></option>
                       <%}%>						
                   </select>
               </td>  
           </tr>                                                           
           <tr class="titulos" align="center" >
               <td>Administradora</td>  
               <td>Grado Necesidad</td>                             
               <td>Profesional Remite</td> 
               <td>Rango Edad</td>                                                         
               <td></td>                
           </tr> 
           <tr class="estiloImput">
               <td><input type="text"  id="txtAdministradoraLE" size="60" maxlength="60" onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradoraLE',308)" style="width:80%"  tabindex="205" />
               </td>     
               <td>
                   <select id="cmbNecesidad" style="width:98%" tabindex="206" >	                                        
                     <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(113);	
                               ComboVO cmbG; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbG=(ComboVO)resultaux.get(k);
                        %>
                      <option value="<%= cmbG.getId()%>" title="<%= cmbG.getTitle()%>"><%=cmbG.getDescripcion()%></option>
                        <%}%>						
                   </select>                            
               </td> 
               <td>
					<select size="1" id="cmbIdProfesionalesLETraer" onfocus="cargarProfesionalesDesdeEspecialidadTraerLE('cmbIdEspecialidad')" style="width:95%"  tabindex="14">	                                        
                       <option value="" ></option>                                  
                    </select>                                    
               </td>     
               <td><select size="1" id="cmbRangosEdad" style="width:90%" tabindex="207" >
                        <% 
						   resultaux.clear();
						   resultaux=(ArrayList)beanAdmin.combo.cargar(116);	
						   ComboVO cmbEdad; 
						   for(int k=0;k<resultaux.size();k++){ 
								 cmbEdad=(ComboVO)resultaux.get(k);
                        %>
                      <option value="<%= cmbEdad.getId()%>" title="<%= cmbEdad.getTitle()%>"><%= cmbEdad.getDescripcion()%></option>
                  <%}%>  
               </td> 

           </tr>  
           <tr class="titulos" align="center" >
               <td colspan="4">Procedimiento</td>  
           </tr>
           <tr class="estiloImput">
             <td colspan="4">
               <input type="text" id="txtIdProcedimientoLE"  size="200" maxlength="200"  style="width:70%" onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimientoLE',324)" tabindex="124"/>
             </td> 
             <td>
               <input  type="button" class="small button blue" title="B7Y2" value="Buscar LE C" onclick="buscarAGENDA('listListaEsperaTraerCirugia')" tabindex="207" />                 
             </td>              
           </tr>         
           <tr class="titulosListaEspera" >
               <td colspan="6" >Traer de Lista de Espera</td> 
           </tr> 
           <tr class="titulos" >   
 	          <td colspan="6">
                 <div id="divContenidoLETraer" style="overflow:auto; height:1000PX; width:100%">          
                  <table align="center" id="listListaEsperaTraerCirugia" class="scroll"></table>  
                </div>    	
              </td>
           </tr>         
        </table>  
      </div>                                          
  </div>  
 

  <div id="divVentanitaEditarListaEspera"  style="display:none; z-index:2000; top:1px; left:-211px;">
      <div class="transParencia" style="z-index:2001; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
      </div>  
      <div  style="z-index:2002; position:absolute; top:180px; left:70px; width:95%">  
          <table width="95%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
               <tr class="estiloImput" >
                   <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditarListaEspera')" /></td>  
                   <td>&nbsp;Id Lista Espera= <label id="lblListaEsperaModificar"></label></td>
                   <td>&nbsp;</td>               
                   <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditarListaEspera')" /></td>  
               <tr>    
               <tr class="estiloImput" >
                   <td></td>  
                   <td>Tel&eacute;fono=&nbsp;<label id="lblTelefono"></label></td>
                   <td>PACIENTE=&nbsp;<label id="lblNombrePaciente"></label>&nbsp&nbsp Edad:<label id="lblEdad"></label></td> 
                   <td>Dias Esperando=&nbsp;<label id="lblDias"></label></td>               
               <tr>                       
               <tr class="titulosListaEspera" >
                   <td colspan="2">*Administradora</td>                    
                   <td colspan="2">Observaciones</td>                  
               <tr> 
               <tr class="estiloImputListaEspera" >
                   <td colspan="2"><input type="text"  id="txtAdministradora1LE" size="60"  maxlength="60"   onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradora1LE',308)" style="width:98%"  tabindex="300" />
                   </td>   
                   <td colspan="2"><input  type="text" id="txtObservacionLE" size="100" maxlength="2000"  style="width:90%"  onKeyPress="javascript:return teclearExcluirCaracter(event,'%');"  tabindex="301"/></td>                      
               <tr>  
               <tr class="titulosListaEspera">
                 <td colspan="4">
                     <table width="100%">
                       <tr>
                         <td width="15%">Especialidad</td>                                    
                         <td width="15%">,</td>                                                                   
                         <td width="13%">No Autorizacion</td>                         
                         <td width="10%">Fecha Vigencia</td>                        
                         <td width="12%">Tipo Cita</td>                  
                         <td width="15%">Grado Necesidad</td>                        
                         <td width="10%">Estado</td>                                                                  
                       </tr>
                       <tr class="estiloImputListaEspera" >
                         <td width="15%" >
,                         </td> 
                         <td>
                           <select id="cmbIdSubEspecialidadLE" style="width:90%"  tabindex="303" >	
                              <option value=""></option>
                                <%     resultaux.clear();
                                       resultaux=(ArrayList)beanAdmin.combo.cargar(117);	
                                       ComboVO cmbSLE; 
                                       for(int k=0;k<resultaux.size();k++){ 
                                             cmbSLE=(ComboVO)resultaux.get(k);
                                %>
                              <option value="<%= cmbSLE.getId()%>" title="<%= cmbSLE.getTitle()%>"><%= cmbSLE.getDescripcion()%></option>
                               <%}%>                                                                   
                           </select>	                   
                         </td>                      
                         <td>
                           <input id="txtNoAutorizacionLE"  type="text"  size="10"  maxlength="10" tabindex="116" />
                         </td>                       
                         <td>
                           <input id="txtFechaVencimientoLE"  type="text"  size="10"  maxlength="10" tabindex="116" />
                         </td>                       
                         <td>
                           <select id="cmbTipoCitaLEM"  style="width:98%" tabindex="306">
                             <option value="C">Control</option>
                             <option value="P">Primera Vez</option>                                      
                         </select>                                  	 
                         </td> 
                         <td><select id="cmbNecesidadLE" style="width:80%" tabindex="307">	                                        
                                  <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(113);	
                                         ComboVO cmbG_; 
                                         for(int k=0;k<resultaux.size();k++){ 
                                               cmbG_=(ComboVO)resultaux.get(k);
                                  %>
                                <option value="<%= cmbG_.getId()%>" title="<%= cmbG_.getTitle()%>"><%=cmbG_.getDescripcion()%></option>
                                  <%}%>						
                             </select> 
                         </td>  
                         <td>
                            <select id="cmbEstadoLEM" style="width:95%" tabindex="308" >
                                  <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(112);	
                                         ComboVO cmbELe; 
                                         for(int k=0;k<resultaux.size();k++){ 
                                               cmbELe=(ComboVO)resultaux.get(k);
                                  %>
                                <option value="<%= cmbELe.getId()%>" title="<%= cmbELe.getTitle()%>"><%=cmbELe.getDescripcion()%></option>
                                  <%}%>						
                            </select>                                 	 
                         </td>                                                                
                       </tr>                                          
                     </table>      
                 </td>
               </tr>  
               <tr class="estiloImputListaEspera"> 
                   <td colspan="4">
                   <input name="btn_crear_cita" type="button" title="by882" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificarListaEspera','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="309"  />  
                   </td> 
               </tr>
         </table>  
      </div>     
  </div>


  <div id="divVentanitaPacienteYaExiste"  style="display:none; z-index:2050; top:1px; left:-211px;">
  
      <div class="transParencia" style="z-index:2051; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
      </div>  
      <div  style="z-index:2052; position:absolute; top:180px; left:10px; width:104%">  
          <table width="90%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
             <tr class="estiloImput" >
                 <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaPacienteYaExiste')" /></td>  
                 <td>&nbsp;</td>               
                 <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaPacienteYaExiste')" /></td>  
             <tr>    
             <tr class="estiloImput" >
                 <td colspan="2"><label id="lblIdPacienteExiste"></label></td>
                 <td colspan="1" align="left"><label style="animation-delay; color:#F00" id="lblLetrero"></label></td>                 
             <tr> 
             <tr class="titulosCentrados" >   
                <td colspan="2">LISTA ESPERA</td>
                <td colspan="2">CITAS</td>                
             </tr>              
             <tr class="titulos" >   
                <td colspan="4">
                <div style="overflow:auto;height=180px; width:100%"   id="divContenidoListaEsperaHist"  >             
                    <table id="listHistoricoListaEsperaEnAgenda" class="scroll"></table>  
                </div>    
                </td>
             </tr>                 
             <tr class="estiloImput"> 
                 <td colspan="2">
                 <input name="btn_crear_cita" type="button" class="small button blue" value="TRAER DATOS DEL PACIENTE" title="B64TT" onclick="asignaAtributo('txtIdBusPaciente',valorAtributo('lblIdPacienteExiste'),0);buscarInformacionBasicaPaciente(); ocultar('divVentanitaPacienteYaExiste')" tabindex="309"  />  
                 </td> 
                 <td colspan="1">
                 <input name="btn_crear_cita" type="button" class="small button blue" value="VER HISTORIA CLINICA." title="b8k62" onclick="buscarHC('listDocumentosHistoricosTodos','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');" tabindex="309"  />  
                 </td>
             </tr>
             <tr class="titulos">
              <td colspan="3"> 
              <!-- PARA MOSTRAR EL LISTADO DE HC  -->
              <div id="divdatosbasicoscredesa" style="  height:130px; width:100%; overflow-y: scroll;" >
                    <table id="listDocumentosHistoricos" class="scroll" cellpadding="0" cellspacing="0" align="center">
                       <tr><th></th></tr>                     
                    </table>
              </div>   
              </td>
            </tr>
            
            
         <tr class="titulos" >   
 	          <td colspan="4">           
                <table width="1080" align="center"  border="0" cellspacing="0" cellpadding="1"   >
                  <tr class="titulosListaEspera" >
                        <div id="divAcordionAdjuntos" style="display:BLOCK">      
                               <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                                      <jsp:param name="titulo" value="Archivos adjuntos" />
                                      <jsp:param name="idDiv" value="divArchivosAdjuntos" />
                                      <jsp:param name="pagina" value="../../archivo/contenidoAdjuntos.jsp" />                
                                      <jsp:param name="display" value="NONE" />  
                                      <jsp:param name="funciones" value="acordionArchivosAdjuntos()" />
                               </jsp:include> 
                        </div>
                  </tr>    
                </table>              
              
              </td>
           </tr>            
            
            
            
         </table>  
      </div>     
  </div>

 


  <div id="divVentanitaEliminarCirugiaProcedimientos"  style="display:none; z-index:2000; top:600px; left:-211px;">  
      <div class="transParencia" style="z-index:2001; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
      </div>  
      <div  style="z-index:2002; position:absolute; top:280px; left:70px; width:95%">  
          <table width="95%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
               <tr class="estiloImput" >
                   <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEliminarCirugiaProcedimientos')" /></td>  
                   <td>&nbsp;</td>               
                   <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEliminarCirugiaProcedimientos')" /></td>  
               <tr>    
               <tr class="titulos" >
                   <td>Sitio quirurgico</td>  
                   <td colspan="2">Procedimiento</td>
               <tr>  
               <tr class="estiloImput" >
                   <td>Id=<label id="lblId"></label> &nbsp;&nbsp;&nbsp; <label id="lblNomSitioQuirurgico"></label></td>  
                   <td colspan="2"><label id="lblIdProcedimiento"></label>-<label id="lblNomProcedimiento"></label></td>
               <tr>        
               <tr class="estiloImput" >
                   <td colspan="3">Observaciones=<input id="txtObservacionProc" type="text" size="150" maxlength="150"  style="width:70%" tabindex="124"/></td>
               <tr>                                                           
                                                                  
               <tr class="estiloImputListaEspera"> 
                   <td colspan="2">
                   <input name="btn_crear_cita" type="button" class="small button blue" value="ELIMINAR PROCEDIMIENTO" onclick="modificarCRUD('eliminarListCirugiaProcedimientos','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="309"  />  
                   </td> 
                   <td colspan="1">
                   <input name="btn_crear_cita" type="button" class="small button blue" title="b77p" value="MODIFICAR" onclick="modificarCRUD('modificarListCirugiaProcedimientos','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="309"  />                     
                   </td> 
               </tr>
         </table>  
      </div>     
  </div>




<!-------------------------para la vista previa---------------------------- -->

<div id="divVentanitaPdf"  style="display:none; z-index:2050; top:1px; left:190px;">

    <div id="idDivTransparencia" class="transParencia" style="z-index:2054; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
    </div>  
    <div id="idDivTransparencia2"  style="z-index:2055; position:absolute; top:5px; left:15px; width:100%">  
          <table id="idTablaVentanitaPDF" width="90%" height="90" class="fondoTabla" >
             <tr class="estiloImput" >
                 <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaPdf')" /></td>  
                 <td>&nbsp;</td>               
                 <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaPdf')" /></td>  
             </tr>   
             <tr class="estiloImput">  
                 <td colspan="3">                                                
                    <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
                      <tr>
                        <td>
                            <div id="divParaPaginaVistaPrevia" ></div>     
                        </td>
                      </tr>   
                    </table>
                 </td>  
             </tr> 
          </table>
    </div>     
</div> 

    <input type="hidden" id="txt_banderaOpcionCirugia" value="OK" />       
    <input type="hidden"  id="txtIdDocumentoVistaPrevia" value="" />
    <input type="hidden"  id="txtTipoDocumentoVistaPrevia" value="" />                        
    <input type="hidden"  id="lblTituloDocumentoVistaPrevia" value=""  />    
    <input type="hidden"  id="lblIdAdmisionVistaPrevia" value=""  />                
<!-------------------------fin para la vista previa---------------------------- -->      

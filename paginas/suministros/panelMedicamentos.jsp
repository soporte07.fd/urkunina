<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />

<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="1150px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <div align="center" id="tituloForma">
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="PANEL MEDICAMENTOS" />
                </jsp:include>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>
                        <div style="overflow:auto;height:700px; width:1150px" id="divContenido">
                            <div id="divBuscar" style="display:block">
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">

                                    <tr class="titulosListaEspera">
                                        <td width="40%">PACIENTE</td>
                                        <td width="10%">FECHA DESDE</td>    
                                        <td width="10%">FECHA HASTA</td>  
                                        <td width="15%">ESTADO</td> 
                                        <td width="25%"></td>
                                    </tr>

                                    <tr class="estiloImput"> 
                                        <td>
                                            <input type="text" id="txtIdPaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtIdPaciente', 307)" style="width:80%"/> 
                                            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaPaciente"
                                                 onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdPaciente', '26')"
                                                 src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                            <div id="divParaVentanita"></div>
                                        </td> 
                                        <td><input type="text" style="width:85%"  id="txtFechaDesdeP"/></td>    
                                        <td><input type="text" style="width:90%"  id="txtFechaHastaP"/></td>  

                                        <td><select size="1" id="cmbEstado" style="width:80%" tabindex="2">
                                                <option value=""></option>
                                                <option value="E">ENVIADO</option>
                                                <option value="N">NO ENVIADO</option>
                                                <option value="R">ERROR</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" class="small button blue" title="BTL4" value="BUSCAR"  onclick="buscarSuministros('listaPanelMedicamentos')"   />                                                                      
                                        </td>                            
                                    </tr>   
                                    
                                </table>
                                <table id="listaPanelMedicamentos" class="scroll"></table>  

                                <table align="center" width="100%">
                                    <tr class="estiloImput">
                                        <td>
                                            <input type="button" class="small button blue" title="BTL4" value="ENVIAR SISTEMA FARMACEUTICO"  onclick="guardarYtraerDatoAlListado('enviarSF')"   />       
                                        </td>
                                    </tr>
                                </table>
                                
                                <table id="listaMedicaciones" class="scroll"></table>  
                            </div>              
                        </div>

                    </td>
                </tr>
            </table>

        </td>
    </tr> 

</table>


<div id="divVentanitaSubirPdf"  style="display:none; z-index:1007; top:1px; left:211px;">
    <div class="transParencia" style="z-index:1008; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>   
    <div  id="ventanitaHija" style="z-index:1009; position:fixed; top:200px; left:160px; width:70%">  
        <table width="1000" height="90" class="fondoTabla">
            <tr class="estiloImput" >
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaSubirPdf')" /></td>  
                <td>&nbsp;</td>                   
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaSubirPdf')" /></td>  
            </tr> 

            <tr>
                <td colspan="3">

                    <div id="tabsSubirDocumentos" style="width:98%; height:auto">
                        <ul>
                            <li> <a href="#divPlantilla" onclick="buscarHC('listLaboratoriosPlantilla', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');tabActivo = 'divPlantilla'""><center>REGISTRAR PLANTILLA</center></a> </li>
                            <li> <a href="#divSubirArchivo" onclick="buscarHistoria('listLaboratoriosPdf');tabActivo = 'divSubirArchivo'""><center>SUBIR ARCHIVO</center></a> </li>


                        </ul>
                        <div id="divPlantilla" style="width:99%"> 
                            <table width="100%" style="background-color: #E8E8E8;">

                                <tr>
                                    <td class="titulos">
                                        <table id="listLaboratoriosPlantilla" ></table>
                                        <div id="pagerPlantilla" ></div>
                                    </td>
                                </tr>

                            </table>
                        </div>

                        <div id="divSubirArchivo" style="width:99%"> 

                            <form method="post" enctype="multipart/form-data" action="" target="iframeUpload" id="formUpc" name="formUpc">   
                                <table width="100%" style="background-color: #E8E8E8;">

                                    <tr class="titulosListaEspera"> 
                                        <td>Escoja el archivo</td>
                                        <td>Observacion</td>
                                        <td> Enviar archivo</td>              
                                    </tr>        
                                    <tr class="estiloImput">          				
                                        <td align="center">
                                            <label>
                                                <input id="fileUpc" name="fileUpc" type="file"  tabindex="15" size="99%"/>  
                                            </label>
                                        </td>

                                        <td>
                                            <textarea id="txtObservacionAdjunto"  style="width:90%"  maxlength="80" tabindex="116" ></textarea>
                                        </td>

                                        <td align="center"><input align="center" type="button" title="BT99P" class="boton" value="S U B I R    A R C H I V O" onclick="comprobarYSubirArchivos('orden_programacion')" /></td>
                                    </tr>

                                    <tr>
                                        <td class="titulos" colspan="3"  >
                                            <table id="listLaboratoriosPdf" ></table>

                                        </td>
                                    </tr>

                                </table> 

                            </form>
                        </div>  


                    </div>

                </td>

            </tr>

        </table>  
    </div>  
</div>                


<div id="divVentanitaArchivosAdjuntos"  style="position:absolute; display:none; top:10px; left:1px; width:600px; height:90px; z-index:2002">
    <div class="transParencia" style="z-index:2003; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
    </div> 
    <div   style="z-index:2004; position:absolute; top:100px; left:150px; width:95%">          
        <table id="idSubVentanita" width="100%" class="fondoTabla">
            <tr class="estiloImput">
                <td colspan="2">Usuario elaboro: <label id="lblUsuarioElaboro"></label></td>
                <td colspan="2" align="right">
                    <input name="btn_cerrar" type="button" value="CERRAR" onclick="cerrarVentanitaArchivosAdjuntos()" /></td>  

            <tr>     
            <tr class="titulos">
                <td width="5%">ID</td>  
                <td width="65%">NOMBRE ARCHIVO</td>
                <td width="30%">&nbsp;</td>                      
            <tr>           
            <tr class="estiloImput" >
            <form method="post" enctype="multipart/form-data" action="" target="iframeUpload" id="formUpcElimina" name="formUpcElimina">   
                <td><label id="lblId"></label></td>            
                <td align="CENTER" onClick="abrirPopupArchivoAdjunto('/docPDF/', 1200, 1200)"><label id="lblNombreElemento"></label><label id="lblImagen"></label>
                    &nbsp;&nbsp;&nbsp;&nbsp;<a><u>Descargar</u></a>
                </td>
                <td align="CENTER">&nbsp;
                    <% if (beanSession.usuario.preguntarMenu("111")) {%>         
                    <input type="button" title="BE08" onclick="javascript:eliminarArchivoAdjunto()" value="ELIMINA" class="small button blue" id="btnProcedimiento">           
                    <!--input type="button" title="BE09" onclick="javascript:limpiarArchivoAdjunto()"  value="LIMPIA"  class="small button blue" id="btnProcedimiento"-->                          
                    <%}%>
                </td>            
                <tr> 
                    </table>  
                    </div>            
                    </div> 



                <input type="hidden" id="lblIdEvolucion"  />  



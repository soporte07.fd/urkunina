 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center"   border="0" cellspacing="0" cellpadding="1"   >
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="Administracion de Plantillas Canastas" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td>

   <div style="overflow:auto;height:350px; width:100%"   id="divContenido" >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
      <div id="divBuscar"  style="display:block"   >
      <table width="100%"   cellpadding="0" cellspacing="0"  align="center">
           <tr class="titulos" align="center">
              <td width="10%">CODIGO</td>                            
              <td width="60%">NOMBRE</td>
              <td width="20%">VIGENTE</td>
              <td width="10%">&nbsp;</td>                            
           </tr>
           <tr class="estiloImput"> 
              <td colspan="1"><input type="text" id="txtCodPlantilla"  style="width:90%"  tabindex="2" /> 
              </td> 
              <td colspan="1"><input type="text" id="txtNomPlantilla"  style="width:90%"  tabindex="2" /> 
              </td> 
              <td><select size="1" id="cmbVigenteBus" style="width:80%" tabindex="2" >	                                        
                  <option value="S">SI</option>
                  <option value="N">NO</option>                  
                 </select>	                   
              </td>     
              <td>
                  
<input title="btn_7pU" type="button" class="small button blue" value="BUSCAR"  onclick="buscarSuministros('listGrillaPlantillaCanasta')"   /> 				  
              </td>         
          </tr>	                          
      </table>
      <table id="listGrillaPlantillaCanasta" class="scroll"></table>  
    </div>              
  </div><!-- div contenido-->

<div id="divEditar" style="display:block; width:100%" >  

  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr class="titulosCentrados">
      <td colspan="3"> ELEMENTO A EDITAR 
      </td>   
    </tr>  
    <tr>
      <td>
         <table width="100%">            
             <tr  class="estiloImputIzq2">
             	<td class="estiloImputDer" width="10%">CODIGO: </td>
             	<td ><label id="lblCodigoCanasta" ></label> </td>
             </tr>
             <tr >
             	<td class="estiloImputDer">NOMBRE: </td>
             	<td ><input id="txtNombreCanasta" style="width:95%"  maxlenght="100" tabindex="2" /></td>
             </tr>
             <tr >
             	<td class="estiloImputDer">DESCRIPCION: </td>
             	<td ><textarea id="txtContenidoCanasta" style="width:95%; border:solid #06C 1px" maxlenght="1000" onkeyup="this.value=this.value.toUpperCase()" tabindex="2" ></textarea></td>
             </tr>  
			<tr >
             	<td class="estiloImputDer">TIPO: </td>
             	<td >
					<select size="1" id="cmbTipoCanasta" style="width:10%" tabindex="2">
						<option value=""></option>   
						<%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(130);	
                               ComboVO cmbCA; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbCA=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbCA.getId()%>" title="<%= cmbCA.getTitle()%>"><%= cmbCA.getId()+"  "+cmbCA.getDescripcion()%></option>
                             <%}%>	
						
					 </select>
				</td>
             </tr>
			<tr >
				<td class="estiloImputDer">VIGENTE</td>
				<td>
					<select size="1" id="cmbVigente" style="width:10%" tabindex="2">                
					  <option value="S">SI</option>
					  <option value="N">NO</option>   				  
					 </select> 
				</td> 
			</tr>			 
        </table> 
      </td>   
    </tr>   
  </table>

  <table width="100%"  style="cursor:pointer" >
      <tr><td width="99%" class="titulos">
      <input name="btn_MODIFICAR" title="btn_cP78" type="button" class="small button blue" value="CREAR" onclick="modificarCRUD('crearPlantillaCanasta','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
      <input name="btn_MODIFICAR" title="btn_cP41" type="button" class="small button blue" value="MODIFCAR" onclick="modificarCRUD('modificarPlantillaCanasta','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
      <input name="btn_MODIFICAR" title="btn_Lp63" type="button" class="small button blue" value="LIMPIAR" onclick="limpiarDivEditarJuan('plantillaCanasta')"  />  
      </td></tr>
  </table>

  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr class="titulosCentrados">
      <td colspan="3">ARTICULOS DE LA CANASTA
      </td>   
    </tr>  
    <tr>
      <td>
        <table id="listGrillaCanastaDetalle" width="100%" class="scroll"></table>          
      </td>   
    </tr>   
  </table>	
  
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr>
      <td>
        <table width="100%">        
          <tr class="titulosCentrados">
            <td colspan="3">ADICIONAR ARTICULO A LA CANASTA
            </td>   
          </tr> 
		  <tr class="estiloImputIzq2">	
			<td>ID:</td>
			<td >
				<label id="lblIdDetalle"> </label>
			</td>
		  </tr>	
          <tr class="estiloImputIzq2">
            <td width="30%">ARTICULO:</td><td width="70%"><input type="text" id="txtIdArticulo" onfocus="llamarAutocomIdDescripcionConDato('txtIdArticulo',93)" style="width:90%"/></td> 
          </tr>	
          <!--tr class="estiloImputIzq2">
            <td>CANTIDAD</td><td><select size="1" id="cmbCantidad" style="width:5%" tabindex="2">
                  <option value=""></option>  
				  <%     resultaux.clear();
                                             resultaux=(ArrayList)beanAdmin.combo.cargar(3);	
                                             ComboVO cmb4;
                                             for(int k=0;k<resultaux.size();k++){ 
                                                   cmb4=(ComboVO)resultaux.get(k);
                                      %>
                                    <option value="<%= cmb4.getId()%>" title="<%= cmb4.getTitle()%>"><%=cmb4.getDescripcion()%></option>
                                            <%}%>
				  
                 </select> </td> 
          </tr-->


          <tr class="estiloImputIzq2">
            <td>CANTIDAD</td><td><select size="1" id="cmbCantidad" style="width:5%" tabindex="2">
                  <option value=""></option>  
          <%     
              for(int i=0;i<51;i++){ 
                        %>
                      <option value="<%= i%>" title="<%= i%>"><%=i%></option>
                              <%}%>
                 </select> </td> 
          </tr>

          <tr class="estiloImputIzq2">
            <td>UNIDAD</td><td>
				<label id="lbl_Unidad"></label>
			</td> 
          </tr>	                        	
          
          <tr>
             <td colspan="2" align="center">
               <input id="btn_limpia" title="BPC6"class="small button blue" type="button" value="LIMPIAR"  onclick="limpiarDivEditarJuan('plantillaCanastaDetalle')">              
               <input id="btn_crea" title="B_ac6" class="small button blue" type="button"  value="ADICIONAR"  onclick="modificarCRUD('adicionarArticuloCanasta','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                                              
               <input name="btn_elimina" title="bTC68" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('eliminarArticuloCanasta','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />                   
             </td>
          </tr>   
         </table> 
        </td>   
    </tr>   
  </table>  
  
</div><!-- divEditar--> 
      

        </td>
       </tr>
      </table>

   </td>
 </tr> 
 
</table>


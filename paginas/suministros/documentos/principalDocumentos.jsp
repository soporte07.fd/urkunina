﻿ <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center"   border="0" cellspacing="0" cellpadding="1"   >
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="Documentos Historicos" /> 
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td>

   <div style="overflow:auto;height:350px; width:100%"   id="divContenido" >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
      <div id="divBuscar"  style="display:block"   >
      <table width="100%"   cellpadding="0" cellspacing="0"  align="center">
           <tr class="titulos" align="center">
              <td width="15%">BODEGA</td>                            
              <td width="10%">NATURALEZA</td>
              <td width="20%">TIPO DOCUMENTO</td>
              <td width="10%">Fecha Desde:</td>                            
              <td width="10%">Fecha Hasta:</td>    
              <td width="10%">N&uacute;mero</td>                                                                                                    
              <td width="10%">Estado</td>                            
              <td width="25%">&nbsp;</td>                            
           </tr>
           <tr class="estiloImput"> 
              <td colspan="1"><select id="cmbIdBodegaBus" style="width:90%" tabindex="100">
                      <option value=""></option>
                       <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(510);	
                               ComboVO cmbBod; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbBod=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbBod.getId()%>" title="<%= cmbBod.getTitle()%>"><%=cmbBod.getDescripcion()%></option>
                             <%} %>                       			
                   </select>
              </td> 
              <td colspan="1"><select id="cmbIdNaturalezaBus" style="width:90%" tabindex="100" >
						<option value="I" >ENTRADA</option>
						<option value="S" >SALIDA</option>
					</select>
              </td> 
              <td><select size="1" id="cmbTipoBus" style="width:60%" tabindex="2" onfocus="comboCargarElementos1('cmbTipoBus',517,'cmbIdNaturalezaBus')" >	                                        
                  <option value=""></option>
                                        
                 </select>	                   
              </td>
			  <td >	<input id="txtFechaDesdeDoc" type="text" tabindex="100" maxlength="10" size="10">
			  </td>
			  <td><input id="txtFechaHastaDoc" type="text" tabindex="100" maxlength="10" size="10">
			  </td>
			  <td>	<input id="txtNumeroDoc" type="text" tabindex="100" maxlength="10" size="10">
			  </td>              
			  <td><select size="1" id="cmbIdEstado" style="width:90%" tabindex="100" >
					  <option value=""></option>
							<%     resultaux.clear();
								   resultaux=(ArrayList)beanAdmin.combo.cargar(135);	
								   ComboVO cmbEst; 
								   for(int k=0;k<resultaux.size();k++){ 
										 cmbEst=(ComboVO)resultaux.get(k);
							%>
						   <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>"><%=cmbEst.getDescripcion()%></option>
								 <%} %>	                                                   
					 </select>
			  </td>
              <td>
                  <input title="8doch" type="button" class="small button blue" value="BUSCAR"  onclick="buscarSuministros('listGrillaDocumentosHistoricos')"   />   
				  <input name="btn_cierra" title="BTR6" type="button" class="small button blue" value="ImprimirDoc" onclick="impresionDocFarmacia();"   />  				  
              </td>         
          </tr>	                          
      </table>
     <table id="listGrillaDocumentosHistoricos" class="scroll"></table>  
    </div>              
  </div><!-- div contenido-->

<div id="divEditar" style="display:block; width:100%" >  

	
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">    
		<tr class="estiloImput">
		                          
			    <td width="8%" align="left">ID DOCUMENTO:</td>
				<td width="5%" align="left"><label id="lblIdDoc"></label></td> 
				<td width="8%" align="right">NATURALEZA:</td>
				<td width="2%" align="left"><label id="lblNaturaleza"></label><label id="lblIdBodega"></label></td>		
				<td width="2%"><label id="lblIdEstado"></label></td>  
				<td width="8%">	.</td>	
				<td width="20%">	.</td>			  
		</tr>  
		<tr class="titulosCentrados">
		    <td colspan="7">Modificar Documento</td> 					
		</tr>
	    <tr class="estiloImput">
		  <td colspan="2" align="right">Fecha de cierre del documento :</td>  
		  <td align="left"> <input type="text" id="txtFechaModifica" tabindex="100" maxlength="10" size="10" /></td>  
		</tr> 
        <tr class="estiloImput">		
			<td colspan="7">
                <input title="Al modificar la fecha del documento, altera el valor unitario promedio" type="button" class="small button blue" value="Modificar"  onclick="modificarCRUD('modificarFechaDocumentoInventario','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')"   />   			    
            </td>  
        </tr>		
		<tr class="titulosCentrados">
		  <td colspan="7">TRANSACCIONES
		  </td>   
		</tr>  
		<tr>
		  <td colspan="7">        
			<table id="listGrillaTransaccionDocumentoHistorico" class="scroll"></table> 	  
		  </td>
		</tr>
   </table>
  
   
</div><!-- divEditar--> 
      

        </td>
       </tr>
      </table>

   </td>
 </tr> 
 
</table>


<!-- VENTANA list grilla hoja de gastos-->		
            <div id="divVentanitaModTransaccion"  style="position:absolute; display:none; background-color:#E2E1A5; top:570px; left:90px; width:980px; height:90px; z-index:999">
                  <table width="100%"  border="1"  class="fondoTabla">           
                     <tr class="estiloImput" >
                         <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaModTransaccion')" /></td>  
                         <td  align="left"><label id="lblIdTr"></label></td>                                                                         
                         <td colspan="4"><label id="lblArt"></label></td>                         
                         <td align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaModTransaccion')" /></td>  
                     </tr>     
                     <tr class="titulos " >
                                              
                         <td width="5%" >Cantidad</td>           
                         <td width="5%" >Existencias en bodegas</td>           
                         <td width="8%" >Valor Unitario</td>           
                         <td width="8%" >Iva</td>           
                         <td width="8%" >Valor Impuesto</td>           
                         <td width="8%" >Lote</td>           
                         <td width="8%" >Fecha Venc.</td>           
                     </tr>           
                     <tr class="estiloImput" >
                        
                        
                         <td >
							<input type ="hidden" width="8%" value="" id="txtTrCantidadAnt" />
							<input type ="text" width="8%" value="" id="txtTrCantidad" />
						 </td> 
						 <td >
							<label id ="lblExistencia" ></label>
						 </td>
                         <td >
							<input type ="text" value="" id="txtVlrTr" />
						 </td> 
						 <td >
							<select id="cmbVlrIva" >	
								<option value="0">0</option> 
								<option value="19">19</option>
							</select >
						 </td> 
                         <td >
							<label id ="lblVlrImpuesto" ></label>
						 </td> 						 
                         <td >
							<input type ="text" value="" id="txtLoteTr" />
							<input type ="hidden" value="" id="txtLoteAnt" />
						 </td> 
						 <td >
							<input type ="text" value="" id="txtFechaTr" />
						 </td> 
                     </tr> 
                     <tr>  
                         <td align="center">
                          <input id="btnConsumoElimina" title="btn_e2110" type="button" class="small button blue" value="Modifica Cantidad" onclick="guardarYtraerDatoAlListado('modificarTransaccionDocumento')" />						  
						 </td>
						 <td> &nbsp; </td>
						 <td>
                          <input id="btnConsumoElimina" title="btn_e2110" type="button" class="small button blue" value="Modifica Valor " onclick="guardarYtraerDatoAlListado('modificarTransaccionDocumento')" />	
						 </td>
						 <td>	
						  <input type ="hidden" id="txtUConsumo" />
						 </td>
						  <td> &nbsp; </td>
						 <td>						 
                          <input id="btnConsumoElimina" title="btn_e2110" type="button" class="small button blue" value="Modifica Lote" onclick="guardarYtraerDatoAlListado('modificarTransaccionDocumento')" />						  						  
                         </td>  
						 <td>  
                          <input id="btnConsumoElimina" title="btn_e2110" type="button" class="small button blue" value="Eliminar Transaccion" onclick="guardarYtraerDatoAlListado('eliminarTransaccionDocumento')" />	
						 </td>						 
						
                        <!--  <td colspan="2" align="center">
                          <input id="btnProcedimientoModifica" title="btn_mo74" type="button" class="small button blue" value="Modificar" onclick="modificarCRUD('modificarListHojaGasto', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="203" /> 
                         </td>  -->                   
                     </tr>                
                  </table> 
             </div>


<input type="hidden" id="txtIdUsuarioSesion" value ="<%=beanSession.usuario.getIdentificacion()%>" />


﻿ <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center"   border="0" cellspacing="0" cellpadding="1"   >
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="Entradas de Bodegas" /> 
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td> 
		  <table width="100%"   cellpadding="0" cellspacing="0"  align="center">
		       <tr class="titulos" align="center">
		          <td width="10%">CODIGO</td>                            
		          <td width="60%">NOMBRE</td>
		          <td width="20%">VIGENTE</td>
		          <td width="10%">&nbsp;</td>                            
		       </tr>
		       <tr class="estiloImput"> 
		          <td colspan="1"><input type="text" id="txtCodDiagnosticoBus"  style="width:90%"  tabindex="2" /> 
		          </td> 
		          <td colspan="1"><input type="text" id="txtNomDiagnosticoBus"  style="width:90%"  tabindex="2" /> 
		          </td> 
		          <td><select size="1" id="cmbVigenteBus" style="width:60%" tabindex="2" >	                                        
		              <option value="1">1- SI</option>
		              <option value="0">0- NO</option>                  
		             </select>	                   
		          </td>     
		          <td>
		              <input title="89docu" type="button" class="small button blue" value="BUSCAR"  onclick="buscarSuministros('listGrillaBodega')"   />                    
		          </td>         
		      </tr>	                          
		  </table>
		  <div id="divBodegaInv" style="height: 120px; width: 100%">		  	
		   <table id="listGrillaBodega" class="scroll"></table>  
		  </div> 
              
  

<div id="divEditar" style="display:block; width:100%" >  

  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr class="titulos">
      <td colspan="4" align="left"><strong>DOCUMENTOS DE INVENTARIO</strong>
      </td>   
    </tr>  
    <tr>
      <td>
         <table width="100%">
            <tr   class="estiloImputIzq2">
              <td width="20%">BODEGA</td>
			  <td width="80%">
				<select id="cmbIdBodega" style="width:30%" tabindex="100" disabled="disabled">
                      <option value=""></option>
                       <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(510);	
                               ComboVO cmbBod; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbBod=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbBod.getId()%>" title="<%= cmbBod.getTitle()%>"><%=cmbBod.getDescripcion()%></option>
                             <%} %>                       			
                   </select>
				   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			  </td> 		  
            </tr>            
            <tr class="estiloImputIzq2">                         
              <td>ID DOCUMENTO INVENTARIO 
			  </td>     
			  <td>
			  	<label id="lblIdDoc"></label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                ESTADO DOCUMENTO: &nbsp;     		<label id="lblIdEstado"></label>
			  </td>                                        
            </tr>	
            <tr  class="estiloImputIzq2">   
              <td>TIPO DE DOCUMENTO</td>
			  <td>  
			  <select id="cmbIdTipoDocumento" style="width:30%"  tabindex="100" onfocus="cargarDocumentosDeBodega('cmbIdBodega','txtNaturaleza')" onblur="verificaTipo()"	/>
                      <option value=""></option>
                       <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(134);	
                               ComboVO cmbDoc; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbDoc=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbDoc.getId()%>" title="<%= cmbDoc.getTitle()%>"><%=cmbDoc.getDescripcion()%></option>
                             <%} %>                       			
                   </select>
				   <input type="hidden" id="txtNaturaleza" value ="I" />
              </td>	                                             
            </tr>
            <tr  class="estiloImputIzq2"> 
             <td >
				ID DOCUMENTO SALIDA
			  </td>		  
			  <td>
				<input type="text" id="txtDocEgreso" style="width:20%" tabindex="100" disabled="disabled" placeholder="Ingrese el id del documento"/>
				<input name="btn_modifica" title="btn_egre" type="button" value="Traer transacciones" onclick="modificarCRUD('traerTrEgresos','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />
			  </td>                                             
            </tr>
            <tr class="estiloImputIzq2">  
				<td>NÚMERO</td>
				<td > <input type="text" id="txtNumero" style="width:10%" tabindex="100" /></td> 
             </tr>  
             <tr class="estiloImputIzq2">                         
				<td >TERCERO</td>
					<td >  
						<input type="text" id="txtIdTercero" style="width:55%" tabindex="100" onfocus="llamarAutocomIdDescripcionConDato('txtIdTercero',510)" />
					</td>			  
             </tr>
             </tr>  
             <tr class="estiloImputIzq2">                         
				<td >FECHA DOCUMENTO</td>
					<td >  
						<input type="text" id="txtFechaDocumento" style="width:10%" tabindex="100" />
					</td>
             </tr>


			 <tr class="estiloImputIzq2">                         
              <td >VALOR FLETE&nbsp;</td>
              <td >
				<input type="text" id="txtValorFlete" style="width:10%" tabindex="100" />
			  </td>
			  
             </tr> 

			 <tr class="estiloImputIzq2">    
			  <td>OBSERVACION&nbsp;&nbsp;
			  </td> 
			  <td>
				<input type="text" id="txtObservacion" style="width:55%" tabindex="100" />
			  </td> 			  
             </tr>  
			 <tr class="estiloImputIzq2">  
			 	<td>BUSQUEDA&nbsp;&nbsp;
				<td>
					<table width="100%">
						<tr class="estiloImputIzq2">  
							<td width="40%" >FECHA DESDE:
								<input id="txtFechaDesdeDoc" type="text" tabindex="100" maxlength="10" size="10">
							</td>
							<td width="40%">FECHA HASTA:
								<input id="txtFechaHastaDoc" type="text" tabindex="100" maxlength="10" size="10">
							</td>
							<td width="20%">
								ESTADO:
								<select size="1" id="cmbIdEstado" style="width:50%" tabindex="100" >
								  <option value=""></option>
										<%     resultaux.clear();
											   resultaux=(ArrayList)beanAdmin.combo.cargar(135);	
											   ComboVO cmbEst; 
											   for(int k=0;k<resultaux.size();k++){ 
													 cmbEst=(ComboVO)resultaux.get(k);
										%>
									   <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>"><%=cmbEst.getDescripcion()%></option>
											 <%} %>	                                                   
								 </select>
							</td>
						</tr>
					</table>
				</td>
				
			 </tr>


			<tr>
               <td colspan="4" align="center">
				 <input id="btn_limpia" title="btn_lp78e"class="small button blue" type="button" value="LIMPIAR"  onclick="limpiarDivEditarJuan('limpCamposDocumento')">              
<!--                 <input id="btn_crea"   title="btn_B70"  class="small button blue" type="button" value="Crear"  onclick="modificarCRUD('crearDocumento','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" > -->
				 <input id="btnProcedimiento_a" title="BG67" type="button" class="small button blue"  value="NUEVO" onclick="guardarYtraerDatoAlListado('existeDocumentoInventarioBodega')"  /> 
                 <input name="btn_modifica" title="btn_b75" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificaDocumento','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  
                 <input name="btn_cierra" title="BTRR5" type="button" class="small button blue" value="CERRAR" onclick="modificarCRUD('cerrarDocumentoBodega','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  
				 <input name="btn_cierra" title="BTRim" type="button" class="small button blue" value="IMPRIMIR" onclick="impresionDocFarmacia();"   />  
				 <input name="btn_modifica" title="btn_b75" type="button" class="small button blue" value="MODIFICAR FECHA" onclick="modificarCRUD('modificaDocumentoFecha','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  
				 <input name="btn_cierra" title="BTTY7" type="button" class="small button blue" value="BUSCAR" onclick="buscarSuministros('listGrillaDocumentosBodega')"   />  				 
               </td>			   
             </tr>                                                                  
        </table>
        <div id="divDocuInv" style="height: 200px; width: 100%">
		 <table id="listGrillaDocumentosBodega" class="scroll"></table>    
		</div> 
      </td>  
    </tr>  
  </table> 
  
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr class="titulos">      
      <td colspan="3" align="left"><strong>TRANSACCIONES DEL DOCUMENTO</strong>	
      </td>   
    </tr>  
    <tr>
      <td>
        <table width="100%">            	
            <tr class="estiloImputIzq2">                         
              <td width="20%">ID TRANSACCIÓN:&nbsp;&nbsp;&nbsp;&nbsp; <label id="lblIdTransaccion"></label></td>			  
			  <td width="80%">NATURALEZA:&nbsp;&nbsp;&nbsp;&nbsp; <label id="lblNaturaleza">I</label></td>
            </tr>	 					             
			<tr class="estiloImputIzq2"> 
				<td>ARTICULO</td>
				<td> 	              	
				  <input type="text" id="txtIdArticulo" onfocus="llamarAutocomIdDescripcionConDato('txtIdArticulo',93)" style="width:60%" tabindex="100"  />								 
				</td>  
			</tr>
			<tr class="estiloImputIzq2">
				<td>LOTE</td>
				<td>
                    <TABLE width="100%">
                    	<TR>
                    		<TD width="25%">
								<input type="text" id="txtLote" style="width:112px" tabindex="100"/>
							</TD>
                    		<TD width="25%">
								ARTICULO SIN LOTE<input id="chkLote" onchange="cambiarLote()" type="checkbox" />
							</TD>							
							<TD width="25%">	
								<div class="optica" style="display:none;"> TIPO-MEDIDA:</div>
							</TD>
							<TD width="25%" aling="left">	
								<div class="optica" style="display:none;">
									<select id="cmbMedida">
										<option value="">[ Seleccionar ]  </option>
										<option value="TR90"> TR90 </option>
										<option value="ACETATO"> ACETATO </option>
										<option value="METALICO"> METALICO </option>
										<option value="PLASTICO"> PLASTICO </option>
										<option value="TITANIO"> TITANIO </option>
									</select>	
								</div>	
							</TD>							
						</TR>	
					</TABLE>
				</td> 
			</tr>	
			<tr class="estiloImputIzq2">							
				<td >FECHA DE VENCIMIENTO</td>
				<td>
                    <input type="text" id="txtFechaVencimiento" style="width:112px" tabindex="100"/>
				</td> 
			</tr>					
			<tr class="estiloImputIzq2">  
				<td >CANTIDAD</td>
				<td ><input type="text" id="txtCantidad" style="width:12%" tabindex="100" onKeyPress="javascript:return soloNumeros(event)" onkeyup="calcularImpuesto('lblValorImpuesto')"   onblur="guardarYtraerDatoAlListado('verificaCantidadInventario');" />
				</td>	
			</tr>
			<tr class="estiloImputIzq2">  
				<td>VALOR UNITARIO (SIN IVA)</td>
				<td><input type="text" id="txtValorU" style="width:12%" onKeyPress="javascript:return soloNumeros(event)" tabindex="100"  onkeyup="calcularImpuesto('lblValorImpuesto')" onblur="guardarYtraerDatoAlListado('verificaValorInventario');" /></td>
			</tr>
			<tr class="estiloImputIzq2">  
				<td>IVA</td>
				<td>
					<select id="cmbIva"  style="width:12%" tabindex="100" onblur="calcularImpuesto('lblValorImpuesto')" >
					<option value=""></option>
					<%     resultaux.clear();
									   resultaux=(ArrayList)beanAdmin.combo.cargar(520);	
									   ComboVO cmbIvaArticulo; 
									   for(int k=0;k<resultaux.size();k++){ 
											 cmbIvaArticulo=(ComboVO)resultaux.get(k);
								%>
					 <option value="<%= cmbIvaArticulo.getId()%>" title="<%= cmbIvaArticulo.getTitle()%>"><%=cmbIvaArticulo.getDescripcion()%></option>
									 <%} %>	
					</select> %
				</td>	
			</tr>
			<tr class="estiloImputIzq2">  
				<td >VALOR IMPUESTO </td>
				<td > &nbsp;&nbsp;$&nbsp;<label id="lblValorImpuesto"></label></td>	
			</tr>            			
            <tr class="estiloImputIzq2">  
				<td >SUB TOTAL</td>
				<td ><label id="lblSubTotal">0</label></td>
				</tr>
				</td>
			</tr>   
			<tr class="estiloImputIzq2">
			<td>TOTAL</td>
				<td><label id="lblTotal">0</label></td>
			</tr>
			<tr class="estiloImputIzq2">
				<td>PRECIO DE VENTA</td>
				<td><input type="text" id="txtPrecioVenta" onKeyPress="javascript:return soloNumeros(event)" style="width:12%" tabindex="100"/></td>
			</tr>
			<tr class="estiloImputIzq2">
				<td>SERIAL
				</td>     						  
				<td><input type="text" id="txtSerial" style="width:12%" tabindex="100" onchange="registrarSerial();"  onpaste="this.onchange();"/>
				</td>
			</tr>                                                                                            
			 
             <tr>
               <td colspan="4" align="center">			     
				 <% if(beanSession.usuario.preguntarMenu("DES") ){%>
					<input type="button" class="small button blue" onClick="mostrar('divVentanitaDescuento'); buscarSuministros('listDescuentosArticulo'); "  value="DESCUENTO ARTICULO"  title="BTdescuento" >                    
				 <%}%>
				<input name="btn_cierra" title="BR79-GENERA CODIGO BARRAS DOCUMENTO" type="button" class="small button blue" value="GENERA CB DOCUMENTO" onclick="impresionBarCode();"   />  
				<input name="btn_cierra" title="BR74-GENERA CODIGO BARRAS TRANSACCION" type="button" class="small button blue" value="GENERA CB TRANSACCION" onclick="impresionBarCodeTr();"   />  
                <input name="btn_modifica" title="BR75-GENERA CODIGO BARRAS AUTOMATICO" type="button" class="small button blue" value="GENERA CB AUTOMATICO" onclick="modificarCRUD('creaSerialesTransaccion')"  />                   				 
				<input id="btn_limpia" title="BR75"  type="button" class="small button blue" value="LIMPIAR"  onclick="limpiarDivEditarJuan('limpCamposTransaccion')">              
                <input id="btn_crea" title="BR76" type="button"  class="small button blue"  value="CREAR"  onclick="verificarSerial()" >                                                
                <input name="btn_modifica" title="BR77" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificaTransaccion')"   />  
                <input name="btn_elimina" title="BR78" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('eliminaTransaccion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />          
               </td>
             </tr>                                                     
        </table>
        <div id="divTransaccionInv">
		  <table id="listGrillaTransaccionDocumento" class="scroll"></table> 
		</div>  	  
	  </td>
	</tr>
   </table>
  
   
</div><!-- divEditar--> 
      

        </td>
       </tr>
      </table>

   </td>
 </tr> 
 
</table>

<input type="hidden" id="txtIdUsuarioSesion" value ="<%=beanSession.usuario.getIdentificacion()%>" />


  <input type="hidden" id="txtIdLote1" value="" />
  <input type="hidden" id="txtIdTipoBodega" value="" />
  <input type="hidden" id="txtIdLoteBan" value="NO" />
  <input type="text" id="txtRespuestaSerial" value="" />
  
  
<div  id="divVentanitaDescuento"  style="display:none; z-index:3050; top:700px; left:100px;">
    <div id="idDivTransparencia_1" class="transParencia" style="z-index:3051; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
    </div>  
    <div id="idDivTransparencia21"  style="z-index:3052; position:absolute; top:400px; left:-5px; width:100%" >  
          <table id="idTablaVentanitaIntercambio" width="93%" height="100px" class="fondoTabla" >
             <tr class="estiloImput" >
                 <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaDescuento')" /></td>  
                 <td colspan="2">
					<label id="lblIdArtDescuento" ></label>-
					<label id="lblArtDescuento" ></label> -
					<label id="lblSerialDescuento" ></label>
				 </td>               
                 <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaDescuento')" /></td>  
             </tr>               
             <tr class="estiloImput">  
                 <td colspan="4" align="center">       
                   <table id="listDescuentosArticulo" class="scroll"></table>    
                 </td>  
             </tr> 
             <tr class="estiloImput">              
               <td colspan="4">                                                                    
				  <table width="100%">
						 <tr class="titulos"> 
						  <td align="right">Descuento:</td>                            
						  <td >
							<input type="text" id="txtDescuentoArt" onKeyPress="javascript:return soloNumeros(event)" style="width:20%" tabindex="100"/>
						  </td> 						  
						 </tr>                       																	
						 <tr class="estiloImput">  
							<td><input id="idBtnSolicitAutoriza"  type="button"  class="small button blue" value="Crear" onClick="modificarCRUD('crearDescuento')" /></td>							
							<td><input id="idBtnSolicitAutoriza"  type="button"  class="small button blue" value="Eliminar" onClick="modificarCRUD('eliminarDescuento')" /></td>                            
						 </tr>                                                        
				  </table>
               </td>
              </tr>                 
          </table>
    </div>     
</div>
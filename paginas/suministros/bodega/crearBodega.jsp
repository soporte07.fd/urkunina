﻿ <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center"   border="0" cellspacing="0" cellpadding="1"   >
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="Crear Bodega" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td>

   <div style="overflow:auto;height:150px; width:100%"   id="divContenido" >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
      <div id="divBuscar"  style="display:block"   >
      <table width="100%"   cellpadding="0" cellspacing="0"  align="center">
           <tr class="titulos" align="center">
              <td width="10%">CODIGO</td>                            
              <td width="60%">NOMBRE</td>
              <td width="20%">VIGENTE</td>
              <td width="10%">&nbsp;</td>                            
           </tr>
           <tr class="estiloImput"> 
              <td colspan="1"><input type="text" id="txtCodDiagnosticoBus"  style="width:90%"  tabindex="2" /> 
              </td> 
              <td colspan="1"><input type="text" id="txtNomDiagnosticoBus"  style="width:90%"  tabindex="2" /> 
              </td> 
              <td><select size="1" id="cmbVigenteBus" style="width:60%" tabindex="2" >	                                        
                  <option value="1">1- SI</option>
                  <option value="0">0- NO</option>                  
                 </select>	                   
              </td>     
              <td>
                  <input title="bw89b" type="button" class="small button blue" value="BUSCAR"  onclick="buscarSuministros('listGrillaBodega')"   />                    
              </td>         
          </tr>	                          
      </table>
     <table id="listGrillaBodega" class="scroll"></table>  
    </div>              
  </div><!-- div contenido-->

<div id="divEditar" style="display:block; width:100%" >  

  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr class="titulosCentrados">
      <td colspan="3">DATOS DE LA BODEGA
      </td>   
    </tr>  
    <tr>
      <td>
         <table width="100%">
            <tr   class="estiloImputIzq2">
              <td width="30%">ID BODEGA:</td><td width="70%">
				<select id="cmbIdBodega" style="width:30%" tabindex="100" >
                      <option value=""></option>
                       <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(510);	
                               ComboVO cmbBod; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbBod=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbBod.getId()%>" title="<%= cmbBod.getTitle()%>"><%=cmbBod.getDescripcion()%></option>
                             <%} %>                       			
                   </select>
			  </td> 
            </tr>	
            <tr   class="estiloImputIzq2">                         
              <td>DESCRIPCION:</td><td>              
					<input type="text" id="txtDescripcion" style="width:70%" tabindex="100"/>
              </td>                                             
            </tr>
			<tr   class="estiloImputIzq2">                         
              <td>EMPRESA:</td><td>              					
					<select id="cmbIdEmpresa" style="width:40%" tabindex="100" >
                      <option value=""></option>
                      <option value="1">FUNDACION OFTALMOLOGIA DE NARIÑO</option>
                      <option value="0">NO</option>                        			
                   </select>
              </td>                                             
            </tr>
						                                     
             <tr   class="estiloImputIzq2">
              <td>CENTRO COSTO:</td><td><select id="cmbIdCentroCosto" style="width:40%" tabindex="100" >
                       <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(133);	
                               ComboVO cmb8; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb8=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmb8.getId()%>" title="<%= cmb8.getTitle()%>"><%=cmb8.getDescripcion()%></option>
                             <%} %>					
                   </select></td> 
            </tr>
			<tr   class="estiloImputIzq2">
              <td width="30%">RESPONSABLE:</td><td width="70%">
			  <select id="cmbIdIdResponsable" style="width:40%"  tabindex="100" >
                       <option value=""></option>
                       	<%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(47);	
                               ComboVO cmb4B; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb4B=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmb4B.getId()%>" title="<%= cmb4B.getTitle()%>"><%= cmb4B.getDescripcion()%></option>
                             <%} %>	                     				
                   </select>
			  </td> 
            </tr>
			<tr   class="estiloImputIzq2">
              <td width="30%">ESTADO VIGENTE:</td><td width="70%">
			  <select id="cmbEstado" style="width:8%" tabindex="100" >
                      <option value=""></option>
                      <option value="1">SI</option>
                      <option value="0">NO</option>                        			
                   </select></td> 
            </tr>
			<tr class="estiloImputIzq2">                           
              <td>RESTITUCION</td><td>  
              	 <select size="1" id="cmbSWRestitucion" style="width:8%" tabindex="100" >
                  <option value=""></option>               
                  <option value="1">SI</option>
                  <option value="0">NO</option>                                                    
                 </select>	
                </td>                                             
             </tr>				                                    
            <tr class="estiloImputIzq2">                         
              <td>AUTORIZACION RECIBIR COMPRAS</td><td> 
	              <input type="text" id="txtAutorizacion" style="width:20%" tabindex="100" />				
               </td>                                             
             </tr>             
			 <tr class="estiloImputIzq2">                           
              <td>RESTRICCION_STOCK</td><td>  
              	 <select size="1" id="cmbSWRestriccionStock" style="width:8%" tabindex="100" >
                  <option value=""></option>               
                  <option value="1">SI</option>
                  <option value="0">NO</option>                                                    
                 </select>	
                </td>                                             
             </tr>			 
             
             <tr class="estiloImputIzq2">                           
              <td>TIPO BODEGA</td><td>  
              	 <select size="1" id="cmbIdBodegaTipo" style="width:20%" tabindex="100" >
                  <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(132);	
                               ComboVO cmbTB; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbTB=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbTB.getId()%>" title="<%= cmbTB.getTitle()%>"><%=cmbTB.getDescripcion()%></option>
                             <%} %>	                                                   
                 </select>	
                </td>                                             
             </tr>
			  
			 <tr class="estiloImputIzq2">                           
              <td>CONSIGNACION</td><td>  
              	 <select size="1" id="cmbSWConsignacion" style="width:8%" tabindex="100" >
                  <option value=""></option>               
                  <option value="1">SI</option>
                  <option value="0">NO</option>                                                    
                 </select>	
                </td>                                             
             </tr>
			 <tr class="estiloImputIzq2">                           
              <td>APROVECHAMIENTO</td><td>  
              	 <select size="1" id="cmbSWAprovechamiento" style="width:8%" tabindex="100" >
                  <option value=""></option>               
                  <option value="1">SI</option>
                  <option value="0">NO</option>                                                    
                 </select>	
                </td>                                             
             </tr>
			 <tr class="estiloImputIzq2">                           
              <td>AFECTA COSTO</td><td>  
              	 <select size="1" id="cmbSWAfectaCosto" style="width:8%" tabindex="100" >
                  <option value=""></option>               
                  <option value="1">SI</option>
                  <option value="0">NO</option>                                                    
                 </select>	
                </td>                                             
             </tr>
			 
			 
			 
             <tr>
               <td colspan="2" align="center">
				 <input id="btn_limpia" title="btn_lp78e"class="small button blue" type="button" value="Limpiar"  onclick="limpiarDivEditarJuan('limpCamposBodega')">              
                 <input id="btn_crea" title="btn_B70" class="small button blue" type="button"  value="Crear"  onclick="modificarCRUD('crearBodega','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" >                               
                 <input name="btn_modifica" title="btn_b75" type="button" class="small button blue" value="Modificar" onclick="modificarCRUD('modificaBodega','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  
<!--                 <input name="btn_elimina" title="btn_el98y" type="button" class="small button blue" value="Eliminar" onclick="modificarCRUD('eliminaArticulo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />                   -->
               </td>
             </tr>                                                     
        </table>
      </td>  
    </tr>  
  </table> 
  
  
   
</div><!-- divEditar--> 
      

        </td>
       </tr>
      </table>

   </td>
 </tr> 
 
</table>


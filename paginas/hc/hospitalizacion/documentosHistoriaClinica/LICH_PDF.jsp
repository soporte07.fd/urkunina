
<table width="100%"  align="center">
<tr>
 <td >
  <table width="100%" align="center">
        <tr class="titulos">
          <td width="100%" colspan="8">USUARIO REINGRESA A CIRUGIA EN UN PERIODO MENOR A 3 MESES POR LA MISMA CAUSA:
             <label id="lbl_LICH_C1" ></label>
          </td>                                           
        </tr>		
        <tr class="titulos">
          <td width="20%">TIEMPO DE ADMISION: </td>                               
          <td class="inputBlanco">
              <label id="lbl_LICH_C2" ></label> MIN
          </td> 
          <td width="20%">TIEMPO DE ANESTESIA: </td>
           <td class="inputBlanco">
              <label id="lbl_LICH_C3"> </label>	MIN		
          </td>
          <td width="20%">TIEMPO DE PROCEDIMIENTO: </td>                
          <td class="inputBlanco">
              <label id="lbl_LICH_C4" ></label> MIN	
          </td>   
          <td width="20%">TIEMPO DE RECUPERACION: </td>                
          <td class="inputBlanco">
              <label id="lbl_LICH_C5" ></label> MIN
          </td> 
        </tr>		
        
  </table> 

  <table width="100%" align="center">
    <tr class="titulos">
      <td width="90%" colspan="2">1-ADMISION A CIRUGIA AMBULATORIA
      </td>
	  <td width="10%" colspan="2">OBSERVACION
      </td>	  
    </tr> 
    <tr class="inputBlanco">
      <td width="80%" class="inputBlancoDer">USUARIO LLEGA 30 MINUTOS ANTES DE LA HORA DE CIRUGIA:</td> 
      <td width="10%" class="inputBlancoIzq2">
         <label id="lbl_LICH_C6" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C7"></label>
	  </td>	 
    </tr>  
    <tr class="inputBlanco">
      <td class="inputBlancoDer">EL NOMBRE Y APELLIDO DE USUARIO CORRESPONDE CON LA PROGRAMACION QUIRURGICA Y CON EL PACIENTE:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C8" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C9" ></label>
	  </td>
    </tr>
    <tr class="inputBlanco">
      <td class="inputBlancoDer">EL NUMERO DE IDENTIFICACION DEL USUARIO CORRESPONDE CON LA PROGRAMACION QUIRURGICA:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C10" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C11" ></label>
	  </td>		
    </tr> 
    <tr class="inputBlanco">
      <td class="inputBlancoDer">EL NOMBRE DEL PROCEDIMIENTO A REALIZAR CORRESPONDE CON LA PROGRAMACION QUIRURGICA:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C12" ></label>
      </td>
      <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C13" ></label>
	  </td> 	  
    </tr>    
    <tr class="inputBlanco">
      <td class="inputBlancoDer">EL SITIO QUIRURGICO CORRESPONDE CON LA PROGRAMACION QUIRURGICA:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C14" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C15" ></label>
	  </td>
    </tr>    
    <tr class="inputBlanco">
      <td class="inputBlancoDer">CONFIRMAR AYUNO (CUANDO CORRESPONDA SUGUN GUIA DE RECOMENDACIONES PREQUIRURGICAS) ESCRIBA LA ULTIMA HORA DE INGESTA DE ALIMENTO O BEBIDA:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C16" ></label>
      </td>       
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C17" ></label>
	  </td>
    </tr>      
    <tr class="inputBlanco">
      <td class="inputBlancoDer">USUARIO SE PRESENTA CON OJOS SIN MAQUILLAJE (VERIFICAR AUSENCIA DE PESTAÑINA):</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C18" ></label>
      </td>  
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C19" ></label>
	  </td>
    </tr>    
    <tr class="inputBlanco">
      <td class="inputBlancoDer">USUARIO SE PRESENTA CON UÑAS CORTAS, LIMPIAS Y SIN ESMALTE:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C20" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C21" ></label>
	  </td>
    </tr>     
    <tr class="inputBlanco">
      <td class="inputBlancoDer">USUARIO SE PRESENTA SIN OBJETOS DE VANIDAD, ARETES, ANILLOS ETC:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C22" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C23" ></label>
	  </td>
    </tr> 
    <tr class="inputBlanco">
      <td class="inputBlanco">NOMBRE DE LOS MEDICAMENTOS QUE ESTA INGIRIENDO EL USUARIO:
        <label id="lbl_LICH_C24" > </label>
      </td> 
    </tr>    
    <tr class="inputBlanco">
      <td class="inputBlancoDer">USUARIO HA INGERIDO MEDICAMENTOS ESTIPULADOS EN LA GUIA DE RECOMENDACIONES PRE QUIRURGICAS:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C25" ></label>
      </td>  
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C26" ></label>
	  </td>
    </tr>   
	 <tr class="inputBlanco">
      <td class="inputBlancoDer">USUARIO ES CONSUMIDOR CRONICO DE ALCOHOL, CIGARRILLO O SUSTANCIAS PSICOACTIVAS:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C98" ></label>
      </td>  
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C99" ></label>
	  </td>
    </tr>
    <tr class="inputBlanco">
      <td class="inputBlancoDer">MANILLA DE IDENTIFICACION CON NOMRE Y APELLIDOS COMPLETOS, DOCUMENTO DE IDENTIFICACION Y NOMBRE DEL PROCEDIMIENTO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C27" ></label>
      </td>   
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C28" ></label>
	  </td>
    </tr>  
    <tr class="titulos">
      <td width="90%" colspan="2">INGRESO A CIRUGIA AMBULATORIA (Circulante)
      </td>  
	<td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr> 
    <tr class="inputBlanco">
      <td class="inputBlancoDer">VESTIMENTA DEL USUARIO ADECUADA:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C29" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C30" ></label>
	  </td>
    </tr>          
    <tr class="inputBlanco">
      <td class="inputBlancoDer">MANILLA DE IDENTIFICACION DEL USUARIO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C31" ></label>
      </td>  
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C32" ></label>
	  </td>
    </tr> 
    <tr class="inputBlanco">
      <td class="inputBlancoDer">CONFIRMACION DEL NOMBRE E IDENTIFICACION DEL USUARIO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C33" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C34" ></label>
	  </td>
    </tr>  
    <tr class="inputBlanco">
      <td class="inputBlancoDer">PROCEDIMIENTO QUIRURGICO CONFIRMADO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C35" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C36" ></label>
	  </td>
    </tr>   
    <tr class="inputBlanco">
      <td class="inputBlancoDer">CONSENTIMIENTO INFORMADO FIRMADO POR EL USUARIO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C37" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C38" ></label>
	  </td>
    </tr>   
    <tr class="inputBlanco">
      <td class="inputBlancoDer">MARCACION DE SITIO OPERATORIO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C39" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C40" ></label>
	  </td>
    </tr> 
    <tr class="inputBlanco">
      <td class="inputBlancoDer">MANILLA ROJA DE ALERTA DE ALERGIAS MEDICAMNETOSAS:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C41" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C42" ></label>
	  </td>
    </tr>         
    <tr class="inputBlanco">
      <td class="inputBlancoDer">CAMILLAS CON BARANDA ARRIBA Y CON FRENO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C43" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C44" ></label>
	  </td>
    </tr>  
    <tr class="inputBlanco">
      <td class="inputBlancoDer">ACCESO  VENOSO PERMEABLE (CUANDO CORRESPONDA):</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C45" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C46" ></label>
	  </td>
    </tr>  
    <tr class="inputBlanco">
      <td class="inputBlancoDer">TOMA Y REGISTROS DE SIGNOS VITALES:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C47" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C48" ></label>
	  </td>
    </tr>    
    <tr class="titulos">
      <td width="90%" colspan="2">2-ANTES DE LA INDUCCION DE LA ANESTESIA O BLOQUEO ANESTESICO (ANESTESIOLOGO, CIRUJANO, CIRCULANTE)
      </td>   
	<td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr>                         
    <tr class="inputBlanco">
      <td class="inputBlancoDer">SE HA COMPLETADO LA COMPROBACION DE LOS APARATOS DE ANESTESIA Y LA MEDICACION ANESTESICA:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C49" ></label>
      </td>  
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C50" ></label>
	  </td>
    </tr>            
	<tr class="inputBlanco">
      <td class="inputBlancoDer">CIRCUITOS:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C51" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C52" ></label>
	  </td>
    </tr> 
	<tr class="inputBlanco">
      <td class="inputBlancoDer">MEDICACION DEL USUARIO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C53" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C54" ></label>
	  </td>
    </tr> 
	<tr class="inputBlanco">
      <td class="inputBlancoDer">REGISTRO ANESTESICO DEL USUARIO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C55" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C56" ></label>
	  </td>
    </tr> 
	<tr class="inputBlanco">
      <td class="inputBlancoDer">EQUIPO DE INTUBACION:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C57" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C58" ></label>
	  </td>
    </tr> 
	<tr class="inputBlanco">
      <td class="inputBlancoDer">EQUIPO PARA ASPIRACION DE VIA AÉREA:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C59" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C60" ></label>
	  </td>
    </tr>
	<tr class="inputBlanco">
      <td class="inputBlancoDer">SISTEMA DE VENTILACION:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C61" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C62" ></label>
	  </td>
    </tr>
	<tr class="inputBlanco">
      <td class="inputBlancoDer">PREMEDICACION ANESTESICA:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C63" ></label>
      </td>     
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C64" ></label>
	  </td>
    </tr>
	<tr class="inputBlanco">
      <td class="inputBlancoDer">EQUIPO DE SUCCION:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C65" ></label>
      </td>    
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C66" ></label>
	  </td>
    </tr>
	<tr class="inputBlanco">
      <td class="inputBlancoDer">SE HA COLOCADO EL PULSOXIMETRO AL PACIENTE Y FUNCIONA ?:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C67" ></label>
      </td>     
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C68" ></label>
	  </td>
    </tr>
	<tr class="inputBlanco">
      <td class="inputBlancoDer">ALERGIAS CONOCIDAS:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C69" ></label>
      </td>    
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C70" ></label>
	  </td>
    </tr>
	<tr class="inputBlanco">
      <td class="inputBlancoDer">VIA AÉREA DIFICIL / RIESGO DE ASPIRACION:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C71" ></label>
      </td>  
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C72" ></label>
	  </td>
    </tr>
	<tr class="titulos">
      <td width="90%" colspan="2">3-ANTES DE LA INCISION
      </td>  
	<td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr>                         
    <tr class="inputBlanco">
      <td class="inputBlancoDer">TODOS LOS MIEMBROS DEL EQUIPO ESTAN PRESENTES:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C73" ></label>
      </td>   
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C74" ></label>
	  </td>
    </tr> 
	<tr class="inputBlanco">
      <td class="inputBlancoDer">CIRUJANO, INSTRUMENTADOR Y CIRCULANTE CONFIRMAN: SU NOMBRE, FUNCION E IDENTIDAD DEL PACIENTE, SITIO OPERATORIO DEMARCADO, PROCEDIMIENTO QUIRURGICO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C75" ></label>
      </td>                                                    
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C76" ></label>
	  </td>
    </tr>
	<tr class="inputBlanco">
      <td class="inputBlancoDer">EL INSTRUMENTADOR VERIFICA QUE TODO EL INSTRUMENTAL Y LOS EQUIPOS A UTILIZAR SE ENCUENTREN FUNCIONANDO CORRECTAMENTE:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C77" ></label>
      </td>   
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C78" ></label>
	  </td>
    </tr>
	<tr class="titulos">
      <td width="90%" colspan="2">PREVISION DE EVENTOS CRITICOS
      </td> 
	  <td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr> 
	<tr class="inputBlanco">
      <td class="inputBlancoDer">EL EQUIPO CONOCE LAS PROBABLES COMPLICACIONES INTRAOPERATORIAS DEL PROCEDIMIENTO :</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C79" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C80" ></label>
	  </td>
    </tr> 
	<tr class="titulos">
      <td width="90%" colspan="2">EQUIPO DE ENFERMERIA E INSTRUMENTADORA
      </td>   
	  <td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr> 
	 <tr class="inputBlanco">
      <td class="inputBlancoDer">EL INSTRUMENTADOR CONFIRMA EL ESTADO DE ESTERILIZACION DE PAQUETES QUIRURGICOS Y DISPOSITIVOS MEDICOS (CON RESULTADOS DE LOS INDICADORES) :</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C81" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C82" ></label>
	  </td>
    </tr> 
	<tr class="inputBlanco">
      <td class="inputBlancoDer">SE RESOLVIERON LAS INQUIETUDES REFERENTES AL INSTRUMENTAL O INSUMOS A UTILIZAR EN EL PROCEDIMIENTO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C83" ></label>
      </td>  
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C84" ></label>
	  </td>
    </tr> 
	<tr class="titulos">
      <td width="90%" colspan="2">4- DURANTE EL PROCEDIMIENTO
      </td>      
	  <td width="10%" colspan="2">OBSERVACION
      </td>	
    </tr> 
	<tr class="inputBlanco">
      <td class="inputBlancoDer">EL CIRCULANTE RECIBE, REALIZA EMBALAJE DE MUESTRAS PARA PATOLOGIA:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C85" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C86" ></label>
	  </td>
    </tr> 
	<tr class="inputBlanco">
      <td class="inputBlancoDer">ETIQUETADO DE LAS MUESTRAS (LECTURA DE LA ETIQUETA EN VOZ ALTA, INCLUIDO EL NOMBRE DEL PACIENTE):</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C87" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C88" ></label>
	  </td>
    </tr>
	<tr class="inputBlanco">
      <td class="inputBlancoDer">EL INSTRUMENTADOR CONFIRMA VERBALMENTE RECUENTO DE MATERIALES E INSTRUMENTAL :</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C89" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C90" ></label>
	  </td>
    </tr>
	<tr class="titulos">
      <td width="90%" colspan="2">5- ANTES DE QUE EL PACIENTE SALGA A RECUPERACION
      </td>
	  <td width="10%" colspan="2">OBSERVACION
      </td>	  
    </tr> 
	<tr class="inputBlanco">
      <td class="inputBlancoDer">NOMBRE DEL PROCEDIMIENTO:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C91" ></label>
      </td>  
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C92" ></label>
	  </td>
    </tr> 
	<tr class="inputBlanco">
      <td class="inputBlancoDer">SE IMPARTIO RECOMENDACIONES PARA EL PERIODO DE RECUPERACION Y/O TRATAMIENTO DEL PACIENTE POR PARTE DE ANESTESIA O CIRUGIA:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C93" ></label>
      </td>
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C94" ></label>
	  </td>
    </tr>
	<tr class="inputBlanco">
      <td class="inputBlancoDer">SE VERIFICA QUE LOS REGISTROS DE ANESTESIA Y DESCRIPCION QUIRURGICA ESTEN DILIGENCIADOS EN SU TOTALIDAD:</td> 
      <td class="inputBlancoIzq2">
         <label id="lbl_LICH_C95" ></label>
      </td> 
	  <td width="10%" class="inputBlancoIzq2">
			<label id="lbl_LICH_C96" ></label>
	  </td>
    </tr> 
  </table>  
   
 </td>   
</tr> 
<tr><td>  
	<table>	
	<tr  >
      <td class="titulos" width="10%">Nota Enfermeria: </td>	      
      <td class="inputBlanco" width="80%" >
        <label id="lbl_LICH_C97"> </label>
      </td>   
    </tr>       
   
   </table> 
</td></tr>    
   <!-- <tr>
    	<td> 
        	<table width="100%" align="center">  
            	 <tr class="titulos" >
                  <td colspan="5">Signos vitales</td>	  
                </tr>               		
                <tr class="titulos">
                  <td width="20%">Frecuencia Cardiaca</td>                               
                  <td width="20%">Frecuencia Respiratoria</td>
                  <td width="20%">Temperatura</td>                
                  <td width="20%">Tension Arterial</td>
                  <td width="20%">Sat. Oxigeno</td>                 
                </tr>		
                <tr class="inputBlanco" style="text-align:center"> 
                  <td>
                      <label id="lbl_LICH_C98"></label> X MIN
                  </td>                
                  <td>
                      <label id="lbl_LICH_C99"></label> X MIN
                  </td>                
                  <td>
                      <label id="lbl_LICH_C100"></label>° Grados
                  </td>                
                  <td>
                      <label id="lbl_LICH_C101"></label> mmHG
                  </td> 
                  <td>
                      <label id="lbl_LICH_C102" ></label>
                  </td>   
                </tr>     
          </table> 
        </td>
    </tr>
	-->
</table>
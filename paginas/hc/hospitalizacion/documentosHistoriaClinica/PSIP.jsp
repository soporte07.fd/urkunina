﻿<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
   <tr class="camposRepInp" >
     <td width="100%">ESTRUCTURA FAMILIAR  -  RELACION FAMILIAR</td> 
  </tr>                     
  <tr class="estiloImput">
     <td>     
         <textarea type="text" rows="6" id="txt_PSIP_C1"  size="4000"  maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumento()"> </textarea>     
     </td> 
  </tr>  
  <tr class="camposRepInp" >
     <td width="100%">CONDUCTA DEL CONSUMO </td> 
  </tr>    
  <tr class="inputBlanco">
    <td width="80%">	 
		<TABLE width="80%"  align="center"> 
		  <TR  class="inputBlanco">				
			 <td> CONSUME SPA:&nbsp;&nbsp;
				<select id="txt_PSIP_C2" style="width:20%" tabindex="119" >
					<option value="SI">SI</option>               
					<option value="NO">NO</option>                                                              
				</select>              
			 </td>			 			 			 
			 <td>  FRECUENCIA:&nbsp;&nbsp;
				<select id="txt_PSIP_C3" style="width:30%" tabindex="119" >
					<option value="DIARIO">DIARIO</option>               
					<option value="SEMANAL">SEMANAL</option>  
					<option value="QUINCENAL">QUINCENAL</option>               
					<option value="MENSUAL">MENSUAL</option> 
					<option value="ANUAL">ANUAL</option>  			
				</select>              
			 </td>	 
			 <td>     OBSERVACION TIPO SUSTANCIA: 
			 </td>	 
			 <td>    
				 <textarea type="text" rows="2" id="txt_PSIP_C4"  size="4000"  maxlength="4000" style="width:90%; border:solid #06C 1px"   tabindex="101" onblur="guardarContenidoDocumento()"> </textarea>     
			 </td>	 		 		
		  </TR>	  
		</TABLE> 
	</td>  
  </tr> 
  <tr class="camposRepInp" >
     <td width="100%">CONDICION EMOCIONAL </td> 
  </tr>   
 <tr class="inputBlanco">
    <td width="100%">	 
	 <TABLE width="80%"  align="center"> 
		<TR class="inputBlanco">
			<td width="20%" > LENGUAJE:&nbsp;&nbsp;  </td>
			<td width="80%" > PRODUCCION:&nbsp;&nbsp;<input align="center" size="10" id="txt_PSIP_C5" type="text" style="width:5%"   value=""  />  
			  FLUIDEZ:&nbsp;&nbsp;<input align="center" id="txt_PSIP_C6" type="text" style="width:5%"  size="10"  value=""  />   
			  COHERENCIA:&nbsp;&nbsp;<input align="center" id="txt_PSIP_C7" type="text" style="width:10%"  size="10"  value=""  />   
		   </td>
		</TR>		 
		<TR  class="inputBlanco">	 
		 <td> ORIENTACION:&nbsp;&nbsp;  </td>
		 <td> TIEMPO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input align="center" id="txt_PSIP_C8" type="text"  size="10" style="width:5%"   value=""  />  
			  LUGAR:&nbsp;&nbsp;&nbsp;&nbsp;<input align="center" id="txt_PSIP_C9" type="text" style="width:5%"   size="10" value=""  />  
			  PERSONA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input align="center" id="txt_PSIP_C10" type="text" style="width:5%"  size="10"   value=""  />   
		 </td>	 
		</TR>		 
		<TR>	 
		 <td>OBSERVACION CONDUCTA EMOCIONAL: 
		 </td>	 
		 <td>    
			 <textarea type="text" rows="2" id="txt_PSIP_C11"  size="4000"  maxlength="4000" style="width:90%; border:solid #06C 1px"   tabindex="101" onblur="guardarContenidoDocumento()"> </textarea>     
		 </td>	 		 		
		</TR>	  
	</TABLE> 
	</td>  	 
  </tr>  
  <tr class="camposRepInp" >
     <td width="100%">CONCEPTO PSICOLÓGICO</td> 
  </tr>                     
  <tr class="estiloImput">
     <td>     
         <textarea type="text" rows="6" id="txt_PSIP_C12"  size="4000"  maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>     
     </td> 
  </tr>  
  
  <tr class="camposRepInp" >
     <td width="100%">OBJETIVO ATENCION PSICOLÓGICA </td> 
  </tr>                     
  <tr class="estiloImput">
     <td>     
         <textarea type="text" rows="6" id="txt_PSIP_C13"  size="4000"  maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>     
     </td> 
  </tr> 
  <tr class="camposRepInp" >
     <td width="100%">OBSERVACION / RETROALIMENTACION</td> 
  </tr>                     
  <tr class="estiloImput">
     <td>     
         <textarea type="text" rows="6" id="txt_PSIP_C14"  size="4000"  maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>     
     </td> 
  </tr>   
</table>  
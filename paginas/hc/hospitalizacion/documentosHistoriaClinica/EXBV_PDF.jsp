


<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="30%" >&nbsp;
       <table width="100%" align="center">
            <tr class="titulos"> 
             <td >Actividades Diarias</td> 
            </tr>
            <tr class="inputBlanco">
             <td>
                 <label id="lbl_EXBV_C1"   > </label>
             </td> 
		   </tr>
       </table>
  </td>
  <td width="30%" >
     <table width="100%" align="center">
            <tr class="titulos"> 
              <td width="20%">LENSOMETRIA</td>                               
              <td width="30%">Valor</td>
              <td width="50%">Adici&oacute;n</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXBV_C2" ></label></td>
              <td>
                 <label id="lbl_EXBV_C3" ></label>             
              </td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXBV_C4"   ></label></td>
              <td>
                 <label id="lbl_EXBV_C5" ></label>              
              </td>                
           </tr>     
            <tr class="inputBlanco"> 
              <td>Observaciones</td>                               
              <td colspan="2"><label id="lbl_EXBV_C6" ></label></td>
           </tr>     
           
      </table> 
    </td>   
</tr>   
</table>  

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos"> 
              <td width="40%">AGUDEZA VISUAL Sin Correcci&oacute;n</td>                               
              <td width="30%">Visi&oacute;n Lejana</td>
              <td width="30%">Visi&oacute;n pr&oacute;xima</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td>
                 <label id="lbl_EXBV_C7" ></label> 
              </td>
              <td>
				  <label id="lbl_EXBV_C8" ></label>
              </td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td>
				  <label id="lbl_EXBV_C9" ></label>
              </td>
              <td>
				  <label id="lbl_EXBV_C10" ></label>        
              </td>                
           </tr> 
            <tr class="inputBlanco"> 
              <td>Pinhole Ojo derecho</td>                               
              <td colspan="2">
				  <label id="lbl_EXBV_C11" ></label>
              </td>
           </tr> 
           <tr class="inputBlanco" >
              <td>Pinhole Ojo Izquierdo</td>
              <td colspan="2">
				  <label id="lbl_EXBV_C12" ></label>           
              </td>             
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos"> 
              <td width="40%">AGUDEZA VISUAL Con Correcci&oacute;n</td>                               
              <td width="30%">Visi&oacute;n Lejana</td>
              <td width="30%">Visi&oacute;n pr&oacute;xima</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td>
			     <label id="lbl_EXBV_C13" ></label>
              </td>
              <td><label id="lbl_EXBV_C14" ></label></td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td>
			     <label id="lbl_EXBV_C15" ></label>          
              </td>
              <td><label id="lbl_EXBV_C16" ></label></td>                
           </tr>  
            <tr class="inputBlanco"> 
              <td>...</td>                               
              <td colspan="2">
               ...
              </td>
           </tr>   
            <tr class="inputBlanco"> 
              <td>...</td>                               
              <td colspan="2">
               ...
              </td>
           </tr>                      
      </table> 
  </td>   
</tr>   
</table> 
 

<table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
  <tr class="titulos" >
     <td width="50%" colspan="2">Examen externo Ojo Derecho</td> 
     <td width="50%" colspan="2">Examen externo Ojo Izquierdo</td> 
  <tr>                     
  <tr class="inputBlanco">
     <td colspan="2">
         <label id="lbl_EXBV_C17" > </label>
     </td>          
     <td colspan="2">     
         <label id="lbl_EXBV_C18" > </label>     
     </td> 
 
  </tr> 
  <tr class="titulos" >
     <td colspan="2">Examen Motor</td> 
     <td colspan="2">Ducciones</td>
  </tr>
    <tr class="inputBlanco" >
     <td colspan="2">
     	<label  id="lbl_EXBV_C19"  ></label>
     </td>
     <td colspan="2">     
         OD:<label  id="lbl_EXBV_C20"  ></label>
         OI:<label  id="lbl_EXBV_C21"  ></label>         
     </td>
  </tr>
  <tr class="titulos" >
     <td>Reflejos pupilares</td> 
     <td>Cover test</td>   
     <td >Vision Color</td> 
     <td >Estereopsis</td>      
  <tr>                     
  <tr class="inputBlanco">
     <td>
         <label  id="lbl_EXBV_C22"  ></label>
     </td>          
     <td>     
         VL:<label  id="lbl_EXBV_C23"  ></label>
         VP:<label  id="lbl_EXBV_C24"  ></label>         
     </td> 
     <td>     
        <label  id="lbl_EXBV_C25"  ></label>              
     </td> 
     <td>
       <label  id="lbl_EXBV_C26"  ></label>
      </td>
  </tr> 
  <tr class="titulos" >
     <td colspan="2">Versiones</td> 
     <td>Oftalmoscopia Ojo Derecho</td> 
     <td>Oftalmoscopia Ojo Izquierdo</td>     
  <tr class="inputBlanco">
     <td colspan="2">     
         <label  id="lbl_EXBV_C27" ></label>
     </td>
     <td>         
         <label  id="lbl_EXBV_C28" ></label>
     </td>
     <td>         
         <label  id="lbl_EXBV_C29" ></label>           
     </td> 
</table>  
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos"> 
              <td width="40%">REFRACCION</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXBV_C30"   ></label> </td>
              <td>
			     <label id="lbl_EXBV_C31" ></label>             
              </td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXBV_C32"   ></label> </td>
              <td>
			     <label id="lbl_EXBV_C33" ></label>             
              </td>                
           </tr>     
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos"> 
              <td width="40%"> QUERATOMETRIA</td>                               
              <td width="30%">Valor</td>
              <td width="30%">&nbsp;</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXBV_C34"   ></label> </td>
              <td>&nbsp;</td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXBV_C35"   ></label> </td>
              <td>&nbsp;</td>                
           </tr>     
      </table> 
  </td>   
</tr> 

<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos"> 
              <td width="40%">SUBJETIVO</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXBV_C36"   ></label> </td>
              <td>
			     <label id="lbl_EXBV_C37" ></label>             
              </td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXBV_C38"   ></label> </td>
              <td>
			     <label id="lbl_EXBV_C39" ></label>            
                            
              </td>                
           </tr> 
            <tr class="inputBlanco"> 
              <td>Adici&oacute;n</td>                               
              <td colspan="2">
                 <label id="lbl_EXBV_C40" ></label>            
               </td>
           </tr>               
            <tr class="inputBlanco"> 
              <td>Distancia Pupilar</td>                               
              <td colspan="2"><label id="lbl_EXBV_C41"  ></label> </td>
           </tr>
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos"> 
              <td width="40%">Ayudas Opticas</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXBV_C42"   ></label> </td>
              <td>
			     <label id="lbl_EXBV_C43" ></label>            
              </td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXBV_C44"   ></label> </td>
              <td>
			     <label id="lbl_EXBV_C45" ></label>           
              </td>                
           </tr> 
           <tr class="inputBlanco"> 
              <td>...</td>                               
              <td colspan="2">
               ...
              </td>
           </tr>   
            <tr class="inputBlanco"> 
              <td>...</td>                               
              <td colspan="2">
               ...
              </td>
           </tr>         
      </table>
  </td>   
</tr>  
 <tr>
 	<td width="50%">
    	<table width="100%" align="center">
            <tr class="titulos"> 
              <td width="40%">RX FINAL</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXBV_C46"   ></label> </td>
              <td><label id="lbl_EXBV_C47"   ></label> </td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXBV_C48"   ></label> </td>
              <td><label id="lbl_EXBV_C49"   ></label> </td>                
           </tr>  
            <tr class="inputBlanco"> 
              <td>Adici&oacute;n</td>                               
              <td colspan="2">
                 <label id="lbl_EXBV_C50" ></label>          
              </td>
           </tr>               
            <tr class="inputBlanco"> 
              <td>Observaciones:</td>                               
              <td colspan="2"> <label id="lbl_EXBV_C51" ></label></td>
           </tr>              
      </table> 
    </td>
 </tr>
</table> 






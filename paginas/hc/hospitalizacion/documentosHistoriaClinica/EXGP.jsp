
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >ANAMNESIS</td>
</tr>
</table>
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="estiloImput">
  <td width="1000%">ANTECEDENTES MEDICOS Y ODONTOLOGICOS GENERALES (SELECCIONE SI / NO EN LA CASILLA CORRESPONDIENTE)</td>
</tr>
</table>
<table width="100%"  align="center">
<tr>
    <td width="25%" >
	    <table width="100%" align="center">
			 <tr class="estiloImput"> 
				<td align="right">1. TRATAMIENTO MEDICO ACTUAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C1" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">2. TOMA DE MEDICAMENTOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C2" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">3. ALERGIAS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C3" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">4. CARDIOPATIAS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C4" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">5. ALTERACION PRESION ARTERIAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C5" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
		</table>   
	</td>		
  			  
  <td width="25%" >
 <table width="100%" align="center">
			<tr class="estiloImput"> 
				<td align="right">6. EMBARAZO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C6" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">7. DIABETES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C7" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">8. HEPATITIS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C8" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">9. IRRADIACIONES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C9" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">10. DISGRASIAS SANGUINEAS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C10" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>            
</table>      
  </td>   
    <td width="25%" >
		<table width="100%" align="center">
             <tr class="estiloImput"> 
				<td align="right">11. FIEBRE REUMATICA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C11" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">12. EMFERMEDADES RENALES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C12" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">13. INMUNOSUPRESION:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C13" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">14. TRANSTORNOS EMOCIONALES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C14" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">15. PATOLOGIA RESPIRATORIA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C15" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
      </table>      
  </td> 
   <td width="25%" >
		<table width="100%" align="center">
             <tr class="estiloImput"> 
				<td align="right">16. TRANSTORNOS GASTRICOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C16" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">17. EPILEPSIA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C17" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">18. CIRUGIAS (INCLUSO ORALES):</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C18" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">19. ENFERMEDADES ORALES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C19" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">20. OTRAS ALTERACIONES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C20" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
		</table>      
  </td> 
</tr>   
</table> 

<table width="100%"  align="center">
<tr>
    <td width="25%" >
	    <table width="100%" align="center">
			<tr class="estiloImput"> 
				<td align="center">TEMPERATURA:</br><input type="text" id="txt_EXGP_C21" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
			</tr>
		</table>   
	</td>		
  			  
  <td width="25%" >
 <table width="100%" align="center">
			<tr class="estiloImput"> 
				<td align="center">PULSACIONES/MINUTO:</br><input type="text" id="txt_EXGP_C22" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
		  </tr>
            
</table>      
  </td>   
    <td width="25%" >
		<table width="100%" align="center">
			<tr class="estiloImput"> 
				<td align="center">PRESION ARTERIAL:</br><input type="text" id="txt_EXGP_C23" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
		  </tr>   			
      </table>      
  </td> 
   <td width="25%" >
		<table width="100%" align="center">
		   <tr class="estiloImput"> 
   				<td align="center">FRECUENCIA RESPIRATORIA:</br><input type="text" id="txt_EXGP_C24" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
		  </tr>  			
      </table>      
  </td> 
</tr>   
</table> 


<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%">ANTECEDENTES</td> 
                <td width="37%">OBSERVACIONES (SEGUN EL NUMERO)</td>                  
                        
           </tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_EXGP_C25"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
                <td>
                    <textarea type="text" id="txt_EXGP_C26"    rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td> 
           </tr>   
      </table> 
  </td>
</tr>   
</table>  
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >EXAMEN ESTOMATOLOGICO</td>
</tr>
</table>
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="estiloImput">
  <td width="1000%">SELECCIONE SI / NO EN LA CASILLA CORRESPONDIENTE</td>
</tr>
</table>

<table width="100%"  align="center">
<tr>
    <td width="40%" >
	    <table width="100%" align="center">
			<tr class="titulos1"> 
                <td width="50%" colspan="4">TEJIDOS BLANDOS</td> 
            </tr>
			<tr class="estiloImput"> 
				<td align="right"></td> 
				<td align="left">NORMAL</td>
				<td align="right"></td> 
				<td align="left">NORMAL</td>  				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">1. LABIO SUPERIOR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C27" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
				<td align="right">8. PALADAR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C28" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>  				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">2. LABIO INFERIOR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C29" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
			<td align="right">9. OROFARINGE:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C30" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td> 				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">3. COMISURAS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C31" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>   
			<td align="right">10. LENGUA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C32" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>  				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">4. MUCOSA ORAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C33" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
				<td align="right">11. PISO DE LA BOCA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C34" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">5. SURCOS YUGALES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C35" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>  
			<td align="right">12. REBORDES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C36" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">6. FRENILLOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C37" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
				<td align="right">13. G. SALIVALES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C38" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">7. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C39" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
				<td align="right">14. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C40" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>  				
            </tr>
		</table>   
	</td>		
  			  
  <td width="33,3%" >
 <table width="100%" align="center">
			<tr class="titulos1">
                <td width="50%" colspan="2">ATM. OCLUSION</td> 
            </tr>
			<tr class="estiloImput"> 
				<td align="right">15. DOLOR MUSCULAR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C41" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">16. DOLOR ARTICULAR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C42" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">17. RUIDO ARTICULAR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C43" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">18. ALT. MOVIMIENTO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C44" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">19. MAL OCLUSIONES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C45" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">20. C. DESARROLLO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C46" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">21. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C47" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>			
</table>      
  </td>   
    <td width="33,3%" >
		<table width="100%" align="center">
			<tr class="titulos1"> 
                <td width="50%" colspan="2">TEJIDOS DENTALES</td> 
            </tr>
             <tr class="estiloImput">
				<td align="right">22. CAMBIO DE FORMA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C48" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">23. CAMBIO TAMA&Ntilde;O:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C49" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">24. CAMBIO NUMERO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C50" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">25. CAMBIO COLOR</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C51" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">26. CAMBIO POSICION:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C52" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">27.  IMPACTADOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C53" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">28. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C54" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
      </table>      
  </td> 
</tr>   
</table> 

<table width="100%"  align="center">
<tr>
    <td width="33,3%" >
	    <table width="100%" align="center">
			<tr class="titulos1"> 
                <td width="50%" colspan="2">EXAMEN PERIODONTAL</td> 
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">29. SANGRADO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C55" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">30. MOVILIDAD:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C56" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">31. RECESIONES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C57" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">32. BOLSA PERIODONTAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C58" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">33. CALCULOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C59" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">34. ABSCESO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C60" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">35. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C61" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
		</table>   
	</td>		
  			  
  <td width="33,3%" >
 <table width="100%" align="center">
				<tr class="titulos1"> 
                <td width="50%" colspan="2">EXAMEN PULPAR</td> 
				</tr>
				 <tr class="estiloImput"> 
				<td align="right">36. ALTERAC. VITALIDAD:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C62" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">37. DOLOR PERCUSION:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C63" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">38. MOVILIDAD DENTAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C64" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">39. SENSIBILIDAD:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C65" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">40. FISTULA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C66" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr> 
			<tr class="estiloImput"> 
				<td align="right">41. DIENTE TRATADO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C67" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">42. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C68" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>			
</table>      
  </td>   
    <td width="33,3%" >
		<table width="100%" align="center">
			<tr class="titulos1"> 
                <td width="50%" colspan="2">HABITOS ORALES</td> 
            </tr>
             <tr class="estiloImput"> 
				<td align="right">43. RESPIRADOR ORAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C69" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">44. SUCCION DIGITAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C70" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">45. LENGUA PROCTATIL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C71" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">46. QUEILOSFAGIA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C72" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">47. FUMADOR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C73" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">48. ONICOFAGIA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C74" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">49. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C75" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
      </table>      
  </td> 
</tr>   
</table> 
<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%">OBSERVACIONES (SEGUN EL NUMERO)</td> 
                <td width="37%">OTROS HALLAZGOS</td>                  
                        
           </tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_EXGP_C76"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
                <td>
                    <textarea type="text" id="txt_EXGP_C77"    rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td> 
           </tr>   
      </table> 
  </td>
</tr>   
</table> 

<table width="100%"  align="center">
<tr>
 <td width="50%" >
  <table width="100%">
           <tr class="titulos1" align="center"> 
                <td width="25%" >EXAMEN DE ACCION PREVENTIVA</td>
                <td width="15%">FRECUENCIA</td>                       
				
           </tr>     
            <tr class="estiloImput"> 
				<td align="right">1. Ha recibido charlas de Hingiene Oral?
				<select size="1" id="txt_EXGP_C78" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>				
				</td> 
				<td align="left">
					<input type="text" id="txt_EXGP_C79" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">2. Practica el cepillado diario?
				<select size="1" id="txt_EXGP_C80" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>				
				</td>
				<td align="left">
					<input type="text" id="txt_EXGP_C81" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">3. Usa la seda dental?
				<select size="1" id="txt_EXGP_C82" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>				
				</td>
				<td align="left">
					<input type="text" id="txt_EXGP_C83" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">4. Usa enguaje bucal?
				<select size="1" id="txt_EXGP_C84" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>				
				</td>
				<td align="left">
					<input type="text" id="txt_EXGP_C85" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">5. Le han aplicado  fl&uacute;or?
				<select size="1" id="txt_EXGP_C86" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>				
				</td>
				<td align="left">
					<input type="text" id="txt_EXGP_C87" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">6. Le han aplicado sellantes?
				<select size="1" id="txt_EXGP_C88" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>				
				</td>
				<td align="left">
					<input type="text" id="txt_EXGP_C89" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr> 
			
      </table> 
  </td>
  
  <td width="50%" >
  <table width="100%" align="left">
           <tr class="titulos1"> 
                <td width="100%">OBSERVACIONES</td>                       
           </tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_EXGP_C90"   rows="10" cols="10000" maxlength="4000" style="width:100%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>   
      </table> 
  </td>
</tr>   
</table> 
<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">   
           <tr class="estiloImput"> 
                <td width="37%" colspan="2" class="titulosTodasMinuscu">EXAMENES COMPLEMENTARIOS</td>                        
           </tr>     
           <tr class="estiloImput"> 
                <td  colspan="2">         
                    <textarea type="text" id="txt_EXGP_C91"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>
  
           
      </table> 
  </td>
</tr>   
</table> 






  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >ANALISIS FACIAL</td>
</tr>
</table>

<table width="100%"  align="center">
<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="2">EXAMEN DE PERFIL</td> 
            </tr>		
			<tr class="titulos1"> 
                <td width="50%" >PERFIL</td> 
                <td width="50%">HIPERTONIA LABIAL</td> 
            </tr>
            <tr class="estiloImput"> 
              <td align="right">RECTO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C1" size="100"  maxlength="20" style="width:50%"/></td>                               
              <td align="right">INFERIOR:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C2" size="100"  maxlength="20" style="width:50%"/></td>                               
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td align="right">CONCAVO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C3" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right">SUPERIOR:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C4" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
           </tr>     
            <tr class="estiloImput"> 
              <td align="right">CONVEXO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C5" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right">FRENILLO SOBRE INSERTADO:<input type="text" id="txt_EXDP_C6" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
           </tr> 
			<tr  class="estiloImput"> 
                <td width="50%" class="titulos1" >POSICION LABIAL</td> 
                <td width="50%" align="right">LATERAL:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C7" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /> </td> 
            </tr>
			<tr class="estiloImput"> 
              <td align="right">NORMAL:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C8" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right">LINGUAL:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C9" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
           </tr>
		   <tr class="estiloImput"> 
              <td align="right">PROTRUSION:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C10" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right">INFERIOR:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C11" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
           </tr>
		   <tr class="estiloImput"> 
              <td align="right">RETUSION:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C12" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right">SUPERIOR:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C13" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
           </tr>
		   	<tr class="titulos1"> 
                <td width="50%">HIPOTONIA LABIAL</td> 
                <td width="50%"> LABIOS EN REPOSO</td> 
            </tr>
			   <tr class="estiloImput"> 
              <td align="right">INFERIOR:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C14" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right">SIN CONTACTO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C15" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
           </tr>
		   <tr class="estiloImput"> 
              <td align="right">SUPERIOR:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C16" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right">CON CONTACTO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C17" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
           </tr>
           
      </table>   
  </td>
  <td width="50%" >
 <table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="2">EXAMEN DE FRENTE</td> 
            </tr>		
			<tr class="titulos1"> 
                <td width="50%" rowspan="2">TIPO DE CARA</td> 
                <td width="50%" rowspan="2">ASIMETRIA DE FRENTE</td> 
            </tr>
            <tr class="estiloImput"> 
              <td align="right"></td>                               
              <td align="right"></td>                               
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td align="right">MESOPROSOPO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C18" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="center" rowspan="2">
			    <select size="1" id="txt_EXDP_C19" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>
			  </td>                               
           </tr>     
            <tr class="estiloImput"> 
              <td align="right">EURIPROSOPO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C20" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right"></td>                               
           </tr> 
			<tr  class="estiloImput"> 
                <td align="right">LEPTOPROSOPO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C21" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /> </td> 
                <td align="right">DERECHO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C22" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /> </td> 
            </tr>
			<tr class="estiloImput"> 
              <td align="center" class="titulos1"  rowspan="3">TIPOS DE SONRISA</td>                               
              <td align="right">IZQUIERDO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C23" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
           </tr>
			<tr class="estiloImput"> 
              <td align="right">TERCIO SUPERIOR:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C24" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right"></td>                               
           </tr>
		   <tr class="estiloImput"> 
              <td align="right">TERCIO MEDIO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C25" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
           </tr>
		   	<tr class="estiloImput"> 
              <td align="right">PAPILAR:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C26" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right">TERCIO INFERIOR:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C27" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
            </tr>
			   <tr class="estiloImput"> 
              <td align="right">GINGIVAL:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C28" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right">PLANO OCLUSAL:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C29" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
           </tr>
		   <tr class="estiloImput"> 
              <td align="right">CORONAL:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C30" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right"></td>                               
           </tr>           
      </table>      
  </td>   
</tr>   
</table>  
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >ANALISIS FUNCIONAL</td>
</tr>
</table>

<table width="100%"  align="center">
<tr>
    <td width="33.3%" >
	    <table width="100%" align="center">
			<tr  class="camposRepInp"> 
				<td width="100%" bgcolor="#c0d3c1" colspan="2">HABITOS</td>
			</tr>
			 <tr class="estiloImput"> 
				<td align="right">ONICOFAGIA:</td> 
				<td align="left">
					<select size="1" id="txt_EXDP_C31" style="width:20%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">RESPIRACION ORAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXDP_C32" style="width:20%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">PRESION:</td> 
				<td align="left">
					<select size="1" id="txt_EXDP_C33" style="width:20%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">SUCCION LABIAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXDP_C34" style="width:20%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">ALTERACIONES FONIATRICAS:</td> 
				<td align="left">
					<select size="1" id="txt_EXDP_C35" style="width:20%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">SUCCION DIGITAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXDP_C36" style="width:20%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">DEGLUCION ATIPICA:</td> 
				<td align="left">
					<select size="1" id="txt_EXDP_C37" style="width:20%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">CON EMPUJE LINGUAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXDP_C38" style="width:20%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right">BRUXISMO:</td> 
				<td align="left">
					<select size="1" id="txt_EXDP_C39" style="width:20%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value=""></option>
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
              <td align="right">DUIRNO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C40" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
              <td align="right">NOCTURNO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C41" size="100"  maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
            </tr>
			

		</table>   
	</td>		
  			  
  <td width="33.3%" >
 <table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="3">PALPITACION ARTICULAR</td> 
            </tr>             
			<tr> 
                 <td>&nbsp;&nbsp;&nbsp; </td> 
            </tr>
			<tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="3">DOLOR ARTICULAR</td> 
            </tr>
			<tr class="estiloImput" > 
					<td align="right" rowspan="2">DERECHO:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="txt_EXDP_C42" size="100"  maxlength="5" style="width:30%" onblur="guardarContenidoDocumento()" /></td>                               
					<td align="right" rowspan="2">IZQUIERDO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C43" size="100"  maxlength="5" style="width:30%" onblur="guardarContenidoDocumento()" /></td>                               
					<td align="right" rowspan="2">LATERAL:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="txt_EXDP_C44" size="100"  maxlength="5" style="width:30%" onblur="guardarContenidoDocumento()" /></td>                               
			</tr>
			<tr class="estiloImput"> 
					<td align="right"></td>                               
					<td align="center"></td>                               
					<td align="center"></td>                               
			</tr>			
				<tr class="estiloImput"> 
					<td align="right"></td>                               
					<td align="center"></td>                               
					<td align="center"></td>                               
			</tr>	
			<tr class="camposRepInp"> 
					<td width="100%" bgcolor="#c0d3c1" align="center" colspan="3">LOCALIZACION</td> </tr>	
			<tr> 
			<tr>
					<td align="center" class="titulos1"></td>                               
					<td align="center" class="titulos1">DERECHO</td>                               
					<td align="center" class="titulos1">IZQUIERDO</td>                               
			</tr>
			<tr class="estiloImput"> 
					<td align="right">EN REPOSO:</td>                               
					<td align="center"><input type="text" id="txt_EXDP_C45" size="100"  maxlength="5" style="width:30%" onblur="guardarContenidoDocumento()" /></td>                               
					<td align="center"><input type="text" id="txt_EXDP_C46" size="100"  maxlength="5" style="width:30%" onblur="guardarContenidoDocumento()" /></td>                               
			</tr>	
			<tr class="estiloImput"> 
					<td align="right">EN APERTURA:</td>                               
					<td align="center"><input type="text" id="txt_EXDP_C47" size="100"  maxlength="5" style="width:30%" onblur="guardarContenidoDocumento()" /></td>                               
					<td align="center"><input type="text" id="txt_EXDP_C48" size="100"  maxlength="5" style="width:30%" onblur="guardarContenidoDocumento()" /></td>                               
			</tr>
			<tr class="estiloImput"> 
					<td align="right">EN MOVIMIENTOS:</td>                               
					<td align="center"><input type="text" id="txt_EXDP_C49" size="100"  maxlength="5" style="width:30%" onblur="guardarContenidoDocumento()" /></td>                               
					<td align="center"><input type="text" id="txt_EXDP_C50" size="100"  maxlength="5" style="width:30%" onblur="guardarContenidoDocumento()" /></td>                               
			</tr>
			<tr class="estiloImput"> 
					<td align="right">DE LATERLIDAD:</td>                               
					<td align="center"><input type="text" id="txt_EXDP_C51" size="100"  maxlength="5" style="width:30%" onblur="guardarContenidoDocumento()" /></td>                               
					<td align="center"><input type="text" id="txt_EXDP_C52" size="100"  maxlength="5" style="width:30%" onblur="guardarContenidoDocumento()" /></td>                               
			</tr>
	  </table>      
  </td>   
    <td width="33.3%" >
		<table width="100%" align="center">
                <tr  class="camposRepInp"> 
					<td width="100%" bgcolor="#c0d3c1" colspan="2">EXAMEN DE ATM</td> 
				</tr>   
				<tr class="estiloImput"> 
					<td align="right">RUIDOS ARTICULARES:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C53" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
				</tr>
				<tr class="estiloImput"> 
					<td align="right">CREPITACIONES:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C54" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
				</tr>
				<tr class="estiloImput"> 
					<td align="right">BILATERAL:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C55" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
				</tr>
				<tr class="estiloImput"> 
					<td align="right">CLIKING:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C56" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
				</tr>
				<tr class="estiloImput"> 
					<td align="right">DERECHO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C57" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
				</tr>
				<tr class="estiloImput"> 
					<td align="right">IZQUIERDO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C58" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
				</tr>
				<tr class="estiloImput"> 
					<td align="right">&nbsp;</td>                               
				</tr>
				<tr  class="camposRepInp"> 
					<td width="100%" bgcolor="#c0d3c1" colspan="2">LOCALIZACION</td> 
				</tr> 
				<tr class="estiloImput"> 
					<td align="right">INICIAL:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C59" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
				</tr>
				<tr class="estiloImput"> 
					<td align="right">INTERMEDIO:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C60" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
				</tr>
				<tr class="estiloImput"> 
					<td align="right">FINAL:&nbsp;&nbsp;<input type="text" id="txt_EXDP_C61" size="100"  maxlength="25" style="width:50%" onblur="guardarContenidoDocumento()" /></td>                               
				</tr>				
				
			
      </table>      
  </td> 
</tr>   
</table> 

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
	<tr class="camposRepInp">
	  <td width="1000%" bgcolor="#c0d3c1" >ANALISIS RADIOGRAFICO</td>
	</tr>
</table>

<table width="100%">
	<tr  bgcolor="#C3FF5A"> 
		<td class="estiloImput" width="25%" rowspan="2">LONGITUD RADICULAR DISMINUIDA</td>
		<td width="5%" align="center" rowspan="2"> 
			<select size="1" id="txt_EXDP_C62" rowspan="2" style="width:70%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
				<option value=""></option>
				<option value="SI">SI</option>
				<option value="NO">NO</option>
			 </select>
		</td>
		<td class="estiloImput" rowspan="2"  width="10%">LOCALIZACION:</td>  
		<td align="center">
			&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;						
			&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			</br>
			s
			<input type="number" id="txt_EXDP_C63"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C64"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C65"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C66"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C67"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C68"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C69"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C70"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C71"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C72"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C73"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C74"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C75"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C76"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C77"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C78"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			</br>
			|&nbsp;
			<input type="number" id="txt_EXDP_C79"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C80"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C81"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C82"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C83"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C84"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C85"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C86"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C87"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C88"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C89"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C90"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C91"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C92"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C93"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C94"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
		</td>
	</tr>
</table> 
<table width="100%">
	<tr  bgcolor="#5AFFA8"> 
		<td class="estiloImput" width="25%" rowspan="2">DIENTES RETENIDOS O IMPACTADOS</td>
		<td width="5%" align="center" rowspan="2"> 
			<select size="1" id="txt_EXDP_C95" rowspan="2" style="width:70%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
				<option value=""></option>
				<option value="SI">SI</option>
				<option value="NO">NO</option>
			 </select>
		</td>
		<td class="estiloImput" rowspan="2"  width="10%">LOCALIZACION:</td>  
		<td align="center">
			&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;						
			&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			</br>
			s
			<input type="number" id="txt_EXDP_C96"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C97"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C98"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C99"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C100"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C101"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C102"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C103"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C104"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C105"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C106"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C107"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C108"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C109"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C110"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C111"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			</br>
			|&nbsp;
			<input type="number" id="txt_EXDP_C112"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C113"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C114"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C115"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C116"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C117"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C118"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C119"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C120"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C121"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C122"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C123"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C124"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C125"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C126"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C127"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
		</td>
	</tr>
</table>

<table width="100%">
	<tr  bgcolor="#FFED5A">
		<td class="estiloImput" width="25%" rowspan="2">DIENTES SUPERNUMERARIOS</td>
		<td width="5%" align="center" rowspan="2"> 
			<select size="1" id="txt_EXDP_C128" rowspan="2" style="width:70%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
				<option value=""></option>
				<option value="SI">SI</option>
				<option value="NO">NO</option>
			 </select>
		</td>
		<td class="estiloImput" rowspan="2"  width="10%">LOCALIZACION:</td>  
		<td align="center">
			&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;						
			&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			</br>
			s
			<input type="number" id="txt_EXDP_C129"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C130"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C131"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C132"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C133"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C134"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C135"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C136"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C137"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C138"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C139"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C140"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C141"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C142"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C143"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C144"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			</br>
			|&nbsp;
			<input type="number" id="txt_EXDP_C145"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C146"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C147"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C148"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C149"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C150"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C151"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C152"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C153"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C154"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C155"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C156"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C157"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C158"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C159"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C160"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
		</td>
	</tr>
</table>  

<table width="100%"  align="center">
	<tr>
		<td width="100%" >
			<table width="100%" align="center">
				<tr class="estiloImput"> 
					<td width="37%" colspan="2" class="titulosTodasMinuscu">OBSERVACIONES</td>                        
				</tr>     
				<tr class="estiloImput"> 
					<td  colspan="2">         
						<textarea type="text" id="txt_EXDP_C161"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
					</td>  
				</tr>   
           
			</table> 
		</td>
	</tr>   	
</table> 

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
	<tr class="camposRepInp">
	  <td width="1000%" bgcolor="#c0d3c1" >ANALISIS DE MODELOS DE ESTUDIOS</td>
	</tr>
</table>
   

<table width="100%"  align="center">
<tr>
    <td width="33.3%" >
	    <table width="100%" align="center">
				<tr class="estiloImput" > 
					<td align="center">DENTICION TEMPORAL:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="txt_EXDP_C162" size="100"  maxlength="5" style="width:10%" onblur="guardarContenidoDocumento()" /></td>                               
			</tr>                            
		</table>   
	</td>		
  			  
  <td width="33.3%" >
 <table width="100%" align="center">
			<tr class="estiloImput" > 
				<td align="center">MIXTA:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="txt_EXDP_C163" size="100"  maxlength="5" style="width:10%" onblur="guardarContenidoDocumento()" /></td>                               
			</tr>
 </table>      
  </td>   
    <td width="33.3%" >
		<table width="100%" align="center">
			<tr class="estiloImput" > 
				<td align="center">PERMANENTE:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="txt_EXDP_C164" size="100"  maxlength="5" style="width:10%" onblur="guardarContenidoDocumento()" /></td>                               
			</tr>
     </table>      
  </td> 
</tr>   
</table> 

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
	<tr class="camposRepInp">
	  <td width="50%" bgcolor="#c0d3c1" >SOBREMORDIDA VERTICAL (OVERVITE)</td>
	  <td width="50%" bgcolor="#c0d3c1" >SOBREMORDIDA HORIZONTAL (OVERVITE)</td>
	</tr>
</table>

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
	<tr class="estiloImput">
		<td width="14.28%">NORMAL</td>
		<td width="14.28%">PROFUNDA</td>
		<td width="14.28%">ABIERTA</td>
		<td width="14.28%">NORMAL</td>
		<td width="14.28%">AUMENTADA</td>			  
	    <td width="14.28%">INVERTIDA</td>
	    <td width="14.28%">BORDE A BORDE</td>
	</tr>
	<tr class="estiloImput" align="center">
		<td width="14.28%"><input type="text" id="txt_EXDP_C165" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
		<td width="14.28%"><input type="text" id="txt_EXDP_C166" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
		<td width="14.28%"><input type="text" id="txt_EXDP_C167" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
		<td width="14.28%"><input type="text" id="txt_EXDP_C168" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
		<td width="14.28%"><input type="text" id="txt_EXDP_C169" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>			  
	    <td width="14.28%"><input type="text" id="txt_EXDP_C170" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
	    <td width="14.28%"><input type="text" id="txt_EXDP_C171" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
	</tr>	
</table>

<table width="100%"  align="center">
<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="2">CLASIFICACION CANINA Y MOLAR</td> 
            </tr>		
			<tr class="titulos1"> 
                <td width="50%" >RELACION DERECHA</td> 
                <td width="50%">RELACION IZQUIERDA</td> 
            </tr>
            <tr class="estiloImput"> 
				<td align="left" width="25%">
					CLASE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					I&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					II&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					III
				</td> 
				<td align="left" width="25%">
					CLASE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					I&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					II&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					III
				</td> 								
			</tr>
			<tr class="estiloImput">
				<td align="left" width="25%">
					MOLAR:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C172" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
					<input type="text" id="txt_EXDP_C173" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C174" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />		
				</td>
				<td align="left" width="25%">
					MOLAR:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C175" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
					<input type="text" id="txt_EXDP_C176" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C177" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />		
				</td>			
			</tr>
			<tr class="estiloImput">
				<td align="left" width="25%">
					CANINA:&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C178" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
					<input type="text" id="txt_EXDP_C179" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C180" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />		
				</td>
				<td align="left" width="25%">
					CANINA:&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C181" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
					<input type="text" id="txt_EXDP_C182" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C183" size="100"  maxlength="5" style="width:15%" onblur="guardarContenidoDocumento()" />		
				</td>			
			</tr>
           
      </table>   
  </td>
  <td width="50%" >
 <table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="3">FORMA DE ARCO</td> 
            </tr>		
			<tr class="titulos1"> 
                <td width="33.3%" rowspan="2">ARCO TRIANGULAR</td> 
                <td width="33.3%" rowspan="2">ARCO CUADRADO</td> 
                <td width="33.3%" rowspan="2">ARCO OVOIDAL</td> 
            </tr>
            <tr class="estiloImput"> 
              <td align="right"></td>                               
              <td align="right"></td>                               
           </tr>                                                                            
            <tr class="estiloImput"> 
     
            <tr class="estiloImput"> 
              <td align="center">
			  	<select size="1" id="txt_EXDP_C184" rowspan="2" style="width:40%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
					<option value=""></option>
					<option value="Superior">Superior</option>
					<option value="Inferior">Inferior</option>
				</select>
			  </td>                               
               <td align="center">
			  	<select size="1" id="txt_EXDP_C185" rowspan="2" style="width:40%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
					<option value=""></option>
					<option value="Superior">Superior</option>
					<option value="Inferior">Inferior</option>
				</select>
			  </td>
				<td align="center">
			  	<select size="1" id="txt_EXDP_C186" rowspan="2" style="width:40%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
					<option value=""></option>
					<option value="Superior">Superior</option>
					<option value="Inferior">Inferior</option>
				</select>
			  </td>			  
           </tr> 		 
      </table>      
  </td>   
</tr>   
</table>  
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >LINEA MEDIA</td>
</tr>
</table>
<table width="100%"  align="center">
<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="3">SUPERIOR</td> 
            </tr>		
			<tr class="titulos1"> 
                <td width="33.3%" >CENTRADA</td> 
                <td width="33.3%" >DERECHA</td> 
                <td width="33.3%" >IZQUIERDA</td> 
            </tr>
			<tr class="estiloImput" align="center">
				<td><input type="text" id="txt_EXDP_C187" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C188" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C189" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
			</tr>
			          
      </table>   
  </td>
  <td width="50%" >
	<table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="3">INFERIOR</td> 
            </tr>		
			<tr class="titulos1"> 
                <td width="33.3%" >CENTRADA</td> 
                <td width="33.3%" >DERECHA</td> 
                <td width="33.3%" >IZQUIERDA</td> 
            </tr>    
            <tr class="estiloImput" align="center">
				<td><input type="text" id="txt_EXDP_C190" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C191" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C192" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
			</tr>

      </table>      
  </td>   
</tr>   
</table>  
 <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
	<tr class="camposRepInp">
	  <td width="1000%" bgcolor="#c0d3c1" >RELACIONES INTERARCO</td>
	</tr>
</table>
<table width="100%"  align="center">
<tr>
  <td width="25%" >
      <table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="3">MORDIDA CRUZADA POSTERIOR</td> 
            </tr>		
			<tr class="titulos1"> 
                <td width="25%">SI</td> 
                <td width="25%">NO</td> 
            </tr>
			<tr class="estiloImput" align="left">
				<td>D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I</td>
				<td></td>
			</tr>
			<tr class="estiloImput" align="left">
				<td><input type="text" id="txt_EXDP_C193" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C194" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C195" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
			</tr>
			<tr class="estiloImput" align="left">
				<td><input type="text" id="txt_EXDP_C196" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C197" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C198" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
			</tr>			          
      </table>   
  </td>
  <td width="25%" >
	<table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="3">MORDIDA CRUZADA ANTERIOR</td> 
            </tr>	
			<tr class="titulos1"> 
                <td width="25%">SI</td> 
                <td width="25%">NO</td> 
            </tr>
			<tr class="estiloImput" align="left">
				<td>D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I</td>
				<td></td>
			</tr>
			<tr class="estiloImput" align="left">
				<td><input type="text" id="txt_EXDP_C199" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C200" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C201" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
			</tr>
			<tr class="estiloImput" align="left">
				<td><input type="text" id="txt_EXDP_C202" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C203" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C204" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
			</tr>
      </table>      
  </td>  
<td width="25%" >
	<table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="3">MORDIDA ABIERTA ANTERIOR</td> 
            </tr>
			<tr class="titulos1"> 
                <td width="25%">SI</td> 
                <td width="25%">NO</td> 
            </tr>
			<tr class="estiloImput" align="left">
				<td>D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I</td>
				<td></td>
			</tr>
			<tr class="estiloImput" align="left">
				<td><input type="text" id="txt_EXDP_C205" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C206" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C207" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
			</tr>
			<tr class="estiloImput" align="left">
				<td><input type="text" id="txt_EXDP_C208" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C209" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C210" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
			</tr>
      </table>      
  </td> 
<td width="25%" >
	<table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="3">MORDIDA ABIERTA POSTERIOR</td> 
            </tr>
			<tr class="titulos1"> 
                <td width="25%">SI</td> 
                <td width="25%">NO</td> 
            </tr>
			<tr class="estiloImput" align="left">
				<td>D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I</td>
				<td></td>
			</tr>
			<tr class="estiloImput" align="left">
				<td><input type="text" id="txt_EXDP_C211" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C212" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C213" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
			</tr>
			<tr class="estiloImput" align="left">
				<td><input type="text" id="txt_EXDP_C214" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="txt_EXDP_C215" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
				<td><input type="text" id="txt_EXDP_C216" size="100"  maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()" /></td>
			</tr>

      </table>      
  </td>   
</tr>   
</table>  

 <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
	<tr class="camposRepInp">
	  <td width="1000%" bgcolor="#c0d3c1" >DIENTES AUSENTES</td>
	</tr>
</table>

<table width="100%">
	<tr  bgcolor="#FEC86F"> 
		<td class="estiloImput" width="15%"></br>SUPERIOR</br></br>INFERIOR</td>
		<td align="left">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;						
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			</br>
			s
			<input type="number" id="txt_EXDP_C217"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C218"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C219"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C220"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C221"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C222"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C223"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C224"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C225"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C226"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C227"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C228"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C229"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C230"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C231"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C232"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			</br>
			|&nbsp;
			<input type="number" id="txt_EXDP_C233"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C234"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C235"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C236"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C237"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C238"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C239"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C240"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C241"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C242"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C243"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C244"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C245"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C246"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C247"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C248"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
		</td>
	</tr>
</table>  

 <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
	<tr class="camposRepInp">
	  <td width="1000%" bgcolor="#c0d3c1" >ROTACIONES</td>
	</tr>
</table>

<table width="100%">
	<tr  bgcolor="#FE6F6F"> 
		<td class="estiloImput" width="15%"></br>SUPERIOR</br></br>INFERIOR</td>
		<td align="left">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;						
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			</br>
			s
			<input type="number" id="txt_EXDP_C249"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C250"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C251"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C252"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C253"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C254"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C255"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C256"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C257"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C258"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C259"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C260"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C261"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C262"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C263"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C264"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			</br>
			|&nbsp;
			<input type="number" id="txt_EXDP_C265"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C266"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C267"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C268"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C269"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C270"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C271"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C272"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C273"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C274"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C275"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C276"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C277"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C278"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C279"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C280"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
		</td>
	</tr>
</table>  


<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
	<tr class="camposRepInp">
	  <td width="1000%" bgcolor="#c0d3c1" >DIENTES EN ERUPCION</td>
	</tr>
</table>

<table width="100%">
	<tr  bgcolor="#6FFE90"> 
		<td class="estiloImput" width="15%"></br>SUPERIOR</br></br>INFERIOR</td>
		<td align="left">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;						
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;
			</br>
			s
			<input type="number" id="txt_EXDP_C281"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C282"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C283"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C284"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C285"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C286"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C287"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C288"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C289"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C290"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C291"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C292"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C293"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C294"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C295"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C296"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			</br>
			|&nbsp;
			<input type="number" id="txt_EXDP_C297"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C298"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C299"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C300"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C301"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C302"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C303"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C304"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C305"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C306"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C307"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C308"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C309"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C310"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C311"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
			<input type="number" id="txt_EXDP_C312"  maxlength="1" min="1" max="8" style="width:5%" onblur="guardarContenidoDocumento()" />
		</td>
	</tr>
	<tr class="estiloImput" align="left"  bgcolor="#6FFE90">
		<td width="15%">API&Ntilde;AMIENTO</td>
		<td align="left">SUPERIOR&nbsp;&nbsp;&nbsp;<input type="text" id="txt_EXDP_C313" size="100"  maxlength="5" style="width:5%" onblur="guardarContenidoDocumento()" />
			INFERIOR&nbsp;&nbsp;&nbsp;<input type="text" id="txt_EXDP_C314" size="100"  maxlength="10" style="width:5%" onblur="guardarContenidoDocumento()" /></td>
	</tr>
	<tr class="estiloImput" align="left"  bgcolor="#6FFE90">
		<td width="15%">ESPACIAMIENTO</td>
		<td align="left">SUPERIOR&nbsp;&nbsp;&nbsp;<input type="text" id="txt_EXDP_C315" size="100"  maxlength="5" style="width:5%" onblur="guardarContenidoDocumento()" />
			INFERIOR&nbsp;&nbsp;&nbsp;<input type="text" id="txt_EXDP_C316" size="100"  maxlength="10" style="width:5%" onblur="guardarContenidoDocumento()" /></td>
	</tr>

</table> 
 

<table width="100%" align="center">
            <tr  class="camposRepInp"> 
                 <td width="100%" bgcolor="#c0d3c1" colspan="3">ANALISIS DENTARIO</td> 
            </tr>		
			<tr class="titulos1"> 
                <td width="33.3%" rowspan="2">NORMAL</td> 
                <td width="33.3%" rowspan="2">MACRODONCIA</td> 
                <td width="33.3%" rowspan="2">MICRODONCIA</td> 
            </tr>
            <tr class="estiloImput"> 
              <td align="right"></td>                               
              <td align="right"></td>                               
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td align="center">
					<input type="text" id="txt_EXDP_C317" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()" />
			  </td>                               
               <td align="center">
					<input type="text" id="txt_EXDP_C318" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()" />
			  </td>
				<td align="center">
					<input type="text" id="txt_EXDP_C319" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()" />
			  </td>			  
           </tr> 		 
      </table>      
  </td>   
</tr>   
</table>

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >DIAGNOSTICO</td>
</tr>
</table>  
  
<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%">FACIAL</td> 
                <td width="37%">ESQUELETICO</td>
				</tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_EXDP_C320"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
                <td>
                    <textarea type="text" id="txt_EXDP_C321"    rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td> 
           </tr>   
           <tr class="estiloImput"> 
                <td width="37%">DENTAL</td> 
                <td width="37%">TEJIDOS BLANDOS</td>                  
                        
           </tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_EXDP_C322"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
                <td>
                    <textarea type="text" id="txt_EXDP_C323"    rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td> 
           </tr>            
      </table> 
  </td>
</tr>   
</table>  









<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="50%" >&nbsp;
  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="inputExcel"> 
              <td width="20%">LENSOMETRIA</td>                               
              <td width="30%">Valor</td>
              <td width="50%">Adici&oacute;n</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label  id="lbl_EXOR_C1" size="100"  ></label></td>
              <td>
                 <label id="lbl_EXOR_C2" style="width:50%;" ></label>            
              </td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label  id="lbl_EXOR_C3" size="100"  maxlength="25" style="width:90%" ></label></td>
              <td>
                 <label size="1" id="lbl_EXOR_C4" style="width:50%;" ></label>              
              </td>                
           </tr>     
            <tr class="inputBlanco"> 
              <td>Observaciones</td>                               
              <td colspan="2"><label  id="lbl_EXOR_C5" size="100"  maxlength="200" style="width:96%" ></label></td>
           </tr>     
           
      </table> 
  </td>   
</tr>   
</table> 

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="inputExcel"> 
              <td width="40%">AGUDEZA VISUAL Sin Correcci&oacute;n</td>                               
              <td width="30%">Visi&oacute;n Lejana</td>
              <td width="30%">Visi&oacute;n pr&oacute;xima</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td>
                 <label size="1" id="lbl_EXOR_C6" style="width:99%;" ></label>
              </td>
              <td>
				  <label size="1" id="lbl_EXOR_C7" style="width:99%;" ></label>
              </td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td>
				  <label size="1" id="lbl_EXOR_C8" style="width:99%;" ></label>
              </td>
              <td>
				  <label size="1" id="lbl_EXOR_C9" style="width:99%;" ></label>               
              </td>                
           </tr> 
            <tr class="inputBlanco"> 
              <td>Pinhole Ojo derecho</td>                               
              <td colspan="2">
				  <label size="1" id="lbl_EXOR_C10" style="width:50%;" ></label>
              </td>
           </tr> 
           <tr class="inputBlanco" >
              <td>Pinhole Ojo Izquierdo</td>
              <td colspan="2">
				  <label size="1" id="lbl_EXOR_C11" style="width:50%;" ></label>         
              </td>             
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="inputExcel"> 
              <td width="40%">AGUDEZA VISUAL Con Correcci&oacute;n</td>                               
              <td width="30%">Visi&oacute;n Lejana</td>
              <td width="30%">Visi&oacute;n pr&oacute;xima</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td>
			     <label size="1" id="lbl_EXOR_C12" style="width:99%;" ></label>
              </td>
              <td><label id="lbl_EXOR_C13" ></label></td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td>
			     <label size="1" id="lbl_EXOR_C14" style="width:99%;" ></label>           
              </td>
              <td><label  id="lbl_EXOR_C15" size="50" ></label></td>                
           </tr>  
             <tr class="inputBlanco" >
		      <td colspan="3">...</td>                               
              
           </tr>  
            <tr class="inputBlanco"> 
              <td rowspan="2" colspan="3" >
					...
              </td>
           </tr>                      
      </table> 
  </td>   
</tr>   
</table> 
 

<table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
  <tr class="inputExcel" >
      <td colspan="4">Examen Motor</td>                                             
  </tr>  
  <tr class="inputExcel" >
     <td width="50%" colspan="2">CUADRO DE MEDIDAS SIN CORRECCION</td> 
     <td width="50%" colspan="2">CUADRO DE MEDIDAS CON CORRECCION</td> 
  <tr>                     
  <tr class="inputBlanco">
     <td colspan="2">
         <label  id="lbl_EXOR_C16"   size="1000" ></label>
     </td>          
     <td colspan="2">     
         <label  id="lbl_EXOR_C17"  size="4000" ></label>     
     </td> 
 
  </tr> 
  <tr class="inputExcel" >
     <td width="20%">.</td>    
     <td >.</td>         
     <td>Ducciones</td> 
     <td>Versiones</td>
  <tr>                     
  <tr class="inputBlanco">
     <td >
		 Ojo Dominante:<label id="lbl_EXOR_C18"  style="width:80%"  ></label>.
         
     </td> 
     <td>
     	Test Color:<label    id="lbl_EXOR_C19"  style="width:20%"   ></label>.       
         Test Binocularidad:<label    id="lbl_EXOR_C20"  style="width:20%"  ></label>.
     </td>
     <td >     
         OD:<label  id="lbl_EXOR_C21"   size="100"  maxlength="100" style="width:95%"></label>
         OI:<label  id="lbl_EXOR_C22"   size="100"  maxlength="100" style="width:95%" ></label>
     </td> 
     <td>     
         OD:<label  id="lbl_EXOR_C23"   size="100"  maxlength="100" style="width:95%"></label>
         OI:<label  id="lbl_EXOR_C24"   size="100"  maxlength="100" style="width:95%"></label>
     </td> 
  </tr> 
  <tr class="inputExcel" >
     <td colspan="2">Amplitud de Acomodacion</td> 
     <td colspan="2">Flexibilidad de Acomodacion</td> 
  <tr class="inputBlanco">
     <td valign="middle">         
         OD:<label  id="lbl_EXOR_C25"   size="1000"  maxlength="2000" style="width:90%" ></label>
     </td>
     <td valign="middle">         
         OI:<label  id="lbl_EXOR_C26"   size="1000"  maxlength="2000" style="width:90%"></label>
     </td> 
     <td>         
         OD:<label  id="lbl_EXOR_C27"   size="1000"  maxlength="2000" style="width:90%" ></label>
     </td>
     <td>         
         OI:<label  id="lbl_EXOR_C28"   size="1000"  maxlength="2000" style="width:90%"></label>
     </td> 
    </tr>
</table>  
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="inputExcel"> 
              <td width="40%">REFRACCION</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label  id="lbl_EXOR_C29" style="width:70%" ></label></td>
              <td>
			     <label size="1" id="lbl_EXOR_C30" style="width:99%;"></label>          
              </td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label  id="lbl_EXOR_C31" style="width:70%" ></label> </td>
              <td>
			     <label size="1" id="lbl_EXOR_C32" style="width:99%;" ></label>   
              </td>                
           </tr>     
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="inputExcel"> 
              <td width="40%"> QUERATOMETRIA</td>                               
              <td width="30%">Valor</td>
              <td width="30%">&nbsp;</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label  id="lbl_EXOR_C33" style="width:70%"></label> </td>
              <td>&nbsp;</td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label  id="lbl_EXOR_C34" style="width:70%"></label></td>
              <td>&nbsp;</td>                
           </tr>     
      </table> 
  </td>   
</tr> 

<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="inputExcel"> 
              <td width="40%">SUBJETIVO</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label  id="lbl_EXOR_C35" style="width:70%" ></label> </td>
              <td>
			     <label size="1" id="lbl_EXOR_C36" style="width:99%;" ></label>         
              </td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label  id="lbl_EXOR_C37" style="width:70%" ></label></td>
              <td>
			     <label size="1" id="lbl_EXOR_C38" style="width:99%;" ></label>        
                            
              </td>                
           </tr> 
            <tr class="inputBlanco"> 
              <td>Adici&oacute;n</td>                               
              <td colspan="2">
                 <label size="1" id="lbl_EXOR_C39" style="width:50%;" ></label>            
               </td>
           </tr>               
            <tr class="inputBlanco"> 
              <td>Distancia Pupilar</td>                               
              <td colspan="2"><label  id="lbl_EXOR_C40" style="width:40%" ></label> </td>
           </tr>
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="inputExcel"> 
              <td width="40%">RX FINAL</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label  id="lbl_EXOR_C41" style="width:70%" ></label> </td>
              <td><label  id="lbl_EXOR_C42" style="width:70%" ></label> </td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label  id="lbl_EXOR_C43" style="width:70%" ></label> </td>
              <td><label  id="lbl_EXOR_C44" style="width:70%"></label> </td>                
           </tr>  
            <tr class="inputBlanco"> 
              <td>Adici&oacute;n</td>                               
              <td colspan="2">
                 <label size="1" id="lbl_EXOR_C45" style="width:50%;"></label>          
              </td>
           </tr>               
            <tr class="inputBlanco"> 
              <td>&nbsp;</td>                               
              <td colspan="2">&nbsp;</td>
           </tr>              
      </table> 
  </td>   
</tr>  
<tr>
<table width="100%">
<tr class="inputExcel"> 
   <td >Punto Pr&oacute;ximo de Convergencia</td>
   <td >FP</td>
   <td >FN</td>
</tr>
<tr class="inputBlanco">
	 <td>     
         Real:<label  id="lbl_EXOR_C46" ></label>
         Luz:<label  id="lbl_EXOR_C47" ></label>
         FR:<label  id="lbl_EXOR_C48" ></label>          
     </td> 
      <td>
        <label id="lbl_EXOR_C49" ></label>
      </td>
      <td>
        <label id="lbl_EXOR_C50"></label>
      </td>
    </tr>  
  </table>  
</tr>
 <tr >
 <td width="50%" colspan="3">
      <table width="100%" align="center"> 
	       <tr class="inputExcel"> 
           	   <td >Reservas</td>               
               <td>Otros</td>               
           </tr>           
           <tr class="inputBlanco"> 
              <td  >
					<label id="lbl_EXOR_C51" > </label>
              </td>             
               <td  >
					<label id="lbl_EXOR_C52" > </label>
              </td>
	      </tr>
     </table>
   </td>
</tr> 
        
</table> 






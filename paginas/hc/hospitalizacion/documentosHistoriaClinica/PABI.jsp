
 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" pageEncoding="UTF-8" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session   contentType="text/html; charset=iso-8859-1"-->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
%>

<table width="100%"  align="center">
  <tr>
    <td >
      <table width="100%" align="center">
            <tr class="titulos"> 
              <td width="20%">EXAMEN CORRECTO</td>                               
              <td width="20%">SITIO DE EXAMEN CORRECTO</td>
              <td width="20%">DATOS DEL PACIENTE CORRECTOS</td>                
              <td width="20%">CANTIDAD DE EXAMENES CORRECTA</td>                
              <td width="20%">RIESGOS POR MEDICAMENTOS </td>                                                
            </tr>		
            <tr class="estiloImput"> 
              <td>
                 <select size="1" id="txt_PABI_C1" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_PABI_C2" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_PABI_C3" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_PABI_C4" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>   
              <td>
                 <select size="1" id="txt_PABI_C5" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="NA" selected="selected">NA</option>                    
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
            </tr>   
            
            <tr class="titulos"> 
              <td width="20%">PREPARACION PREVIA DEL PACIENTE</td>                               
              <td width="20%">DOCUMENTOS NECESARIOS</td>
              <td width="20%">CONSENTIMIENTO INFORMADO</td>                
              <td width="40%" colspan="2">OBSERVACIONES</td>                
            </tr>                                                                                     
  
            <tr class="estiloImput"> 
              <td>
                 <select size="1" id="txt_PABI_C6" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_PABI_C7" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="NA">NA</option>                    
                    <option value="SI" selected="selected">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_PABI_C8" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="NA">NA</option>                    
                    <option value="SI" selected="selected">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td colspan="2"><input type="text" id="txt_PABI_C9" maxlength="500" style="width:96%" onblur="guardarContenidoDocumento()" /></td>
           </tr>  

           
           <tr class="titulosCentrados"> 
              <td colspan="5" align="center">Espacio para Seguimiento
              </td>
           </tr>    
           
           <tr class="estiloImput"> 
              <td colspan="3" align="right">
                 <textarea type="text" id="txt_gestionarPaciente" rows="2"   maxlength="4000" style="width:95%"   tabindex="108"  onkeypress="return validarKey(event,this.id)"> </textarea>                                                
              </td>                   
              <td colspan="1" align="left">
	           <input id="btnGestionPaciente" class="small button blue" type="button" title="bt487" value="GESTIONAR" onclick="modificarCRUD('gestionPacienteFolio', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                                    
              </td>    
            </tr>              
            
      </table> 
    </td>   
  </tr>   
</table>  

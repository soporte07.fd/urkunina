
<table width="100%"   align="center">
  <tr class="estiloImput">
    <td width="30%" class="estiloImputDer">Complicaciones:
    	<select size="1" id="txt_TIEM_C1" style="width:40%;" title="32" onblur="guardarContenidoDocumentoAYD()"  tabindex="14" >	  
			<option value="SI">SI</option>            
            <option value="NO">NO</option>
            <option value="NA">NA</option>                        
         </select>
    </td> 
    <td width="70%" class="estiloImputIzq2"><input type="text" id="txt_TIEM_C2" maxlength="400" style="width:80%" onblur="guardarContenidoDocumentoAYD()"/></td>                                                    
  </tr> 
  <tr class="estiloImput">
    <td class="estiloImputDer">Evento Adverso:
    	<select size="1" id="txt_TIEM_C3" style="width:40%;" title="32" onblur="guardarContenidoDocumentoAYD()"  tabindex="14" >	  
            <option value="SI">SI</option> 
            <option value="NO">NO</option>           
            <option value="NA">NA</option>                        
         </select>
    </td> 
    <td class="estiloImputIzq2"><input type="text" id="txt_TIEM_C4" maxlength="400" style="width:80%" onblur="guardarContenidoDocumentoAYD()"/></td>                                                    
  </tr> 
  <tr class="estiloImput">
    <td class="estiloImputDer">TECNICA USADA: </td> 
    <td class="estiloImputIzq2">
        <select size="1" id="txt_TIEM_C5" style="width:90%;" title="32" onchange="cargarDescripcionLaboratorio(this.value);" onblur="guardarContenidoDocumentoAYD();"  tabindex="14" >	  
			<option value='0'> </option>
			<option value="RESECCION DE CHALAZION">RESECCION DE CHALAZION</option>
         </select>
    </td>                                                    
  </tr>   
  <tr class="estiloImput">
    <td  colspan="2" class="titulosTodasMinuscu">
    DESCRIPCION DE LABORATORIO
    </td> 
  </tr>   
  <tr class="estiloImput">
    <td colspan="2">
	  <textarea type="text" rows="18" id="txt_TIEM_C6"  size="4000"  maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumentoAYD()"> </textarea>    
    </td>                                                    
  </tr>   
  <tr>
    <td  class="titulosTodasMinuscu" >INTERPRETA</td>                                   
    <td id="16"  class="titulosTodasMinuscu" onclick="traerTextoPredefinidoAngio(this.id)">COMENTARIO</td>                                 
  </tr>     
  <tr>
    <td colspan="1" align="center"  class="titulosTodasMinuscu">
              <select size="1" id="txt_TIEM_C8" style="width:60%" title="26" onblur="guardarContenidoDocumentoAYD()"  tabindex="14" >	                                        
                <option value="66863360">MYRIAM  HERNANDEZ</option>                          
              </select>  
    </td>   
    <td colspan="1" align="center">
      <textarea type="text" id="txt_TIEM_C7" rows="2"   maxlength="4000" style="width:95%"   tabindex="108" onblur="guardarContenidoDocumentoAYD()"> </textarea>                                
    </td >  
   
  </tr>   
  
  
</table>  

 
 
<table width="100%">
           <tr class="estiloImput"> 
              <td colspan="1" align="CENTER">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:40%" title="76" tabindex="14" >
                      <option value=""></option>         
                      <option value="2">Tomado</option>
                      <option value="3">Enviado</option>      
                      <option value="5">Enviado urgente</option>                                                
                      <option value="4">Interpretado</option> 
                      <option value="6">Interpretado Urgente</option>  
                      <option value="8">Finalizado Urgente</option>                                                              
                      <option value="7">Cancelado Finalizado</option>
                      <option value="10">Reprogramado Finalizado</option>                                                                                       
                      
                  </select>              
              </td>                   
              <td colspan="1" align="CENTER">
              MOTIVO:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"  tabindex="14" >	
                          <option value="1">NINGUNA</option>                                                                                   
                          <option value="3">ATRIBUIBLE AL PACIENTE</option>  
 						  <option value="4">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>    
              <td colspan="1" align="CENTER">
	           <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO AL FOLIO" onclick="cambioEstadoFolio()">                                    
			  </td> 
           </tr>  
</table>






<table width="100%">
            <tr> 
              <td class="titulos1" width="25%"  align="right">ANALISIS</td>  
			  <td class="titulos1" width="25%"  align="right">RESULTADOS</td>			  
              <td></td>  
			</tr>
			<tr class="camposRepInp">
				<td bgcolor="#c0d3c1" align="right" colspan"2">FISICO QUIMICO</td>
				<td bgcolor="#c0d3c1"></td>
				<td></td>		
			</tr>
			<tr class="estiloImput"> 
              <td align="right">ASPECTO:&nbsp;&nbsp;</td>  
			  <td align="left">
				<select size="1" id="txt_UROC_C1" style="width:48%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="Limpida">Limpida</option>
                    <option value="Ligeramente turbia">Ligeramente turbia</option>
                    <option value="Turvia">Turbia</option>
                 </select> 			  
			  </td>			  
              <td></td>  
			</tr>	
			<tr class="estiloImput"> 
              <td align="right">COLOR:&nbsp;&nbsp;</td>  
			  <td align="left">
				<select size="1" id="txt_UROC_C2" style="width:35;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="Amarilla">Amarilla</option>
                    <option value="Ambar">Ambar</option>
                    <option value="Roja">Roja</option>
                 </select>
			  </td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">GLUCOSA:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C3" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>		
			<tr class="estiloImput"> 
              <td align="right">BILIRRUBINA:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C4" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>	
			<tr class="estiloImput"> 
              <td align="right">CUERPOS CETONICOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C5" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>	
			<tr class="estiloImput"> 
              <td align="right">DENSIDAD:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C6" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">SANGRE:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C7" size="100"  maxlength="40" style="width:50%"/></td>			  
		      <td></td>  
			</tr>		
            <tr class="estiloImput"> 
              <td align="right">PH:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C8" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">PROTEINAS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C9" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">UROBILINOGENO:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C10" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>			
			<tr class="estiloImput"> 
              <td align="right">NITRITOS:&nbsp;&nbsp;</td>  
			  <td align="left">
				<select size="1" id="txt_UROC_C11" style="width:28%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                <option value="Positivo">Positivo</option>   
				<option value="Negativo">Negativo</option>
   				</select> 	  
			  </td>				  
              <td></td>  
			</tr> 
			<tr class="estiloImput"> 
              <td align="right">LEUCOCITOS:&nbsp;&nbsp;</td>  
			  <td align="left">
				<select size="1" id="txt_UROC_C12" style="width:35;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
					<option value="Negativo">Negativo</option>
					<option value="+">+</option>
					<option value="++">++</option>
					<option value="+++">+++</option>
   				</select> 	  
			  </td>				  
              <td></td>  
			</tr> 
			<tr class="camposRepInp">
				<td bgcolor="#c0d3c1" align="right" colspan"2">MICROSCOPICO</td>
				<td bgcolor="#c0d3c1"></td>
				<td></td>		
			</tr>
			<tr class="estiloImput"> 
              <td align="right">BACTERIAS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C13" size="100"  maxlength="100" style="width:100%"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">LEUCOCITOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C14" size="100"  maxlength="100" style="width:100%"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CELULAS EPITELIALES:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C15" size="100"  maxlength="100" style="width:100%"/></td>			  
              <td></td>  
			</tr>		
			<tr class="estiloImput"> 
              <td align="right">HEMATIES:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C16" size="100"  maxlength="100" style="width:100%"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">MOCO:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C17" size="100"  maxlength="100" style="width:100%"/></td>			  
              <td></td>  
			</tr>			
			
</table>  
 
 
  

<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%" colspan="2" class="titulosTodasMinuscu">OBSERVACION</td>                        
           </tr>     
           <tr class="estiloImput"> 
                <td  colspan="2">         
                    <textarea type="text" id="txt_UROC_C18"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr> 
  
           
      </table> 
  </td>
</tr>   
</table>  



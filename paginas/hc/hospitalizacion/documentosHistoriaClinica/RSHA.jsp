

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="100%" colspan="4">PRESCRIPCION DE HEMODIALISIS </td> 
  </tr>   
  <tr class="estiloImput">
     <td>     
		FILTRO
     </td>
	 <td>        
		<select id="txt_RSHA_C1" style="width:30%" onblur="guardarContenidoDocumento();">
			<option value="120">120</option>
			<option value="150">150</option>
			<option value="160">160</option>
			<option value="180">180</option>
			<option value="210">210</option>
		</select> 
	 </td>
	 <td>     
		HEPARINA
     </td>
	 <td>        
		<select id="txt_RSHA_C2" style="width:30%" onblur="guardarContenidoDocumento();">
			<option value="500">500</option>
			<option value="1000">1000</option>
			<option value="2000">2000</option>
			<option value="3000">3000</option>
			<option value="4000">4000</option>
			<option value="5000">5000</option>
			<option value="7000">7000</option>
		</select> 
	 </td>
  </tr>
  <tr class="estiloImput">
     <td>     
		ACCESO VASCULAR
     </td>
	 <td>
        <!--<input type="text" id="txt_RSHA_C3" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/> -->
		<table>
			<tr>
				<td>Fistula AV &nbsp; <input type="text" id="txt_RSHA_C3" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>
				</td>
				<td>
					<table>
						<tr>
							<td rowspan="3">Cateter</td>
							<td>Yugular &nbsp; <input type="text" id="txt_RSHA_C23" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/> </td>
						</tr>
						<tr>							
							<td>Femoral &nbsp; <input type="text" id="txt_RSHA_C24" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>
						</tr>
						<tr>							
							<td>Subclave &nbsp;<input type="text" id="txt_RSHA_C25" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>
						</tr>
					</table>
				</td>
			<tr>
		</table> 
	 </td>
	 <td>     
		ULTRAFILTRACION
     </td>
	 <td>
        <input type="text" id="txt_RSHA_C4" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>
	 </td>
  </tr>
  <tr class="estiloImput">
     <td>     
		VELOCIDAD DE FLUJO
     </td>
	 <td>
        <input type="text" id="txt_RSHA_C5" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>
	 </td>
	 <td>     
		TIEMPO
     </td>
	 <td>
        <input type="text" id="txt_RSHA_C6" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>
	 </td>
  </tr>  
  <tr class="camposRepInp" >
     <td width="100%" colspan="4">PESO </td> 
  </tr> 
  <tr class="estiloImput">
     <td>INICIAL</td>
     <td>FINAL</td>
     <td>ACUMULADO</td>
  </tr>
  <tr class="estiloImput">
     <td><input type="text" id="txt_RSHA_C7" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>&nbsp;kg</td>
     <td><input type="text" id="txt_RSHA_C8" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>&nbsp;kg</td>
     <td><input type="text" id="txt_RSHA_C9" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>
  </tr>
  
</table>  

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="100%">Nota de enfermeria</td> 
  </tr>                     
  <tr class="estiloImput">
     <td>     
         <textarea type="text" rows="5" id="txt_RSHA_C10"  size="4000"  maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumento()"> </textarea>     
     </td> 
  </tr>   
</table>  

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="100%" colspan="4">Laboratorios</td> 
  </tr>
  <tr class="estiloImput">
     <td>HEMATOCRITO</td>
     <td><input type="text" id="txt_RSHA_C11" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
     <td>HEMOGLOBINA</td>
     <td><input type="text" id="txt_RSHA_C12" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
  </tr>
  <tr class="estiloImput">
     <td>NITROGENO UREICO PRE/POS</td>
     <td><input type="text" id="txt_RSHA_C13" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
  </tr>
  <tr class="estiloImput">
     <td>CREATININA</td>
     <td><input type="text" id="txt_RSHA_C14" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
  </tr>
  <tr class="estiloImput">
     <td>SODIO</td>
     <td><input type="text" id="txt_RSHA_C15" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
     <td>POTASIO</td>
     <td><input type="text" id="txt_RSHA_C16" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
  </tr>
  <tr class="estiloImput">
     <td>BICARBONATO</td>
     <td><input type="text" id="txt_RSHA_C17" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
  </tr>
  <tr class="estiloImput">
     <td>ANTIGENO DE SUPERICIE HBS(AG)</td>
     <td><input type="text" id="txt_RSHA_C18" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
  </tr>
  <tr class="estiloImput">
     <td>HEPATITIS C: </td>
     <td><input type="text" id="txt_RSHA_C19" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
  </tr>
  <tr class="estiloImput">
     <td>HIV</td>
     <td><input type="text" id="txt_RSHA_C20" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
  </tr>
  <tr class="estiloImput">
     <td>BALANCE LIQUIDOS</td>
     <td colspan="3"><input type="text" id="txt_RSHA_C21" maxlength="500" style="width:90%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
  </tr>
  <tr class="estiloImput">
     <td>GASTO UNITARIO</td>
     <td><input type="text" id="txt_RSHA_C22" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
  </tr>
	<tr class="estiloImput">
	  <td>TEMPERATURA</td>
      <td><input type="text" id="txt_RSHA_C26" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
	</tr>
	<tr class="estiloImput">
	  <td>CONDUCTIVIDAD</td>
      <td><input type="text" id="txt_RSHA_C27" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
	</tr>
	<tr class="estiloImput">
	  <td>VERIFICACION DE LA MAQUINA</td>
      <td><input type="text" id="txt_RSHA_C28" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
	</tr>
	<tr class="estiloImput">
	  <td>NUMERO DE SESION</td>
      <td><input type="text" id="txt_RSHA_C29" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
	</tr>
</table>
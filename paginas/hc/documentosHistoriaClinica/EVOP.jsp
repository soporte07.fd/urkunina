
<table width="100%"  align="center">
<tr>
  <td width="50%" >&nbsp;
  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos2"> 
              <td width="20%">LENSOMETRIA</td>                               
              <td width="30%">Valor</td>
              <td width="50%">Adici&oacute;n</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td><input type="text" id="txt_EVOP_C1" size="100"  maxlength="25" style="width:90%" /></td>
              <td>
				<input type="text" id="txt_EVOP_C2" size="100"  style="width:50%" onblur="guardarContenidoDocumento()" />
                <!-- <select size="1" id="txt_EVOP_C2" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value=""></option>
                    <option value="+1.00">+1.00</option>
                    <option value="+1.25">+1.25</option>
                    <option value="+1.50">+1.50</option>
                    <option value="+1.75">+1.75</option>
                    <option value="+2.00">+2.00</option>
                    <option value="+2.25">+2.25</option>
                    <option value="+2.50">+2.50</option>
                    <option value="+2.75">+2.75</option>                    
                    <option value="+3.00">+3.00</option>
                 </select>      -->          
              </td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td><input type="text" id="txt_EVOP_C3" size="100"  maxlength="25" style="width:90%" onblur="guardarContenidoDocumento()"  /></td>
              <td>
				<input type="text" id="txt_EVOP_C4" size="100"  style="width:50%" onblur="guardarContenidoDocumento()" />
                <!-- <select size="1" id="txt_EVOP_C4" style="width:50%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="+1.00">+1.00</option>
                    <option value="+1.25">+1.25</option>
                    <option value="+1.50">+1.50</option>
                    <option value="+1.75">+1.75</option>                    
                    <option value="+2.00">+2.00</option>
                    <option value="+2.25">+2.25</option>
                    <option value="+2.50">+2.50</option>
                    <option value="+2.75">+2.75</option>                    
                    <option value="+3.00">+3.00</option>
                 </select>    -->           
              </td>                
           </tr>     
            <tr class="estiloImput"> 
              <td>Observaciones</td>                               
              <td colspan="2"><input type="text" id="txt_EVOP_C5" size="100"  maxlength="200" style="width:96%" onblur="guardarContenidoDocumento()"  /></td>
           </tr>     
           
      </table> 
  </td>   
</tr>   
</table>  

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos1"> 
              <td width="25%">AGUDEZA VISUAL Sin Correcci&oacute;n</td>                               
              <td width="37%">Visi&oacute;n Lejana</td>
              <td width="37%">Visi&oacute;n pr&oacute;xima</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td>
                 <select size="1" id="txt_EVOP_C6" style="width:40%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                   <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>   -->
					<option value="Otro">Otro</option>	
                 </select> 
				 <input type="text"  value="" id="txt_EVOP_C58"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >
              </td>
              <td>
				  <select size="1" id="txt_EVOP_C7" style="width:40%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>                 
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <!--<option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>  -->
					<option value="Otro">Otro</option>	
                 </select> 
				 <input type="text"  value="" id="txt_EVOP_C59"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >
              </td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td>
				  <select size="1" id="txt_EVOP_C8" style="width:40%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                   <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>   -->
					<option value="Otro">Otro</option>		
                 </select> 
				 <input type="text"  value="" id="txt_EVOP_C60"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >
              </td>
              <td>
				  <select size="1" id="txt_EVOP_C9" style="width:40%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>                  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                   <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>    -->
					<option value="Otro">Otro</option>		
                 </select>   
				 <input type="text"  value="" id="txt_EVOP_C61"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >				 
              </td>                
           </tr> 
            <tr class="estiloImput"> 
              <td>Pinhole Ojo derecho</td>                               
              <td colspan="2">
				<input type="text" id="txt_EVOP_C10" size="100"  style="width:50%" onblur="guardarContenidoDocumento()" tabindex="14"/>
				<!--  <select size="1" id="txt_EVOP_C10" style="width:50%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>                    
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select> -->
              </td>
           </tr> 
           <tr class="estiloImput" >
              <td>Pinhole Ojo Izquierdo</td>
              <td colspan="2">
				<input type="text" id="txt_EVOP_C11" size="100"  style="width:50%" onblur="guardarContenidoDocumento()" tabindex="14"/>
				<!--  <select size="1" id="txt_EVOP_C11" style="width:50%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>                   
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select>    -->           
              </td>             
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos2"> 
              <td width="25%">AGUDEZA VISUAL Con Correcci&oacute;n</td>                               
              <td width="37%">Visi&oacute;n Lejana</td>
              <td width="37%">Visi&oacute;n pr&oacute;xima</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td>
			     <select size="1" id="txt_EVOP_C12" style="width:40%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
                    <option value=""></option>                    
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                   <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>    -->
					<option value="Otro">Otro</option>					
                 </select>   
				<input type="text"  value="" id="txt_EVOP_C62"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >	
              </td>
              <td>
				<input type="text"  value="" id="txt_EVOP_C13"  style="width:80%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >	
              <!--	<select size="1" id="txt_EVOP_C13" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
                    <option value=""></option>                    
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>  
					<option value="Otro">Otro</option>					
                 </select>  
				 -->
              </td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td>
			     <select size="1" id="txt_EVOP_C14" style="width:40%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
                    <option value=""></option>                   
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                   <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>    -->
					<option value="Otro">Otro</option>		
                 </select> 
				<input type="text"  value="" id="txt_EVOP_C63"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >		
              </td>
              <td>
				<input type="text"  value="" id="txt_EVOP_C15"  style="width:80%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >	
             <!-- 	<select size="1" id="txt_EVOP_C15" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
                    <option value=""></option>                   
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select>  -->
              </td>                
           </tr>  
            <tr class="estiloImput"> 
              <td>...</td>                               
              <td colspan="2">
               ...
              </td>
           </tr>   
            <tr class="estiloImput"> 
              <td>..</td>                               
              <td colspan="2">
             ..
              </td>
           </tr>                       
      </table> 
  </td>   
</tr>   
</table> 
 

<table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
  <tr class="camposRepInp" >
     <td width="50%" colspan="2">Examen externo Ojo Derecho</td> 
     <td width="50%" colspan="2">Examen externo Ojo Izquierdo</td> 
  <tr>                     
  <tr class="estiloImput">
     <td colspan="2">
         <textarea type="text" id="txt_EVOP_C16"   size="1000"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"   onkeyup="this.value=this.value.toUpperCase()"> </textarea>
     </td>          
     <td colspan="2">     
         <textarea type="text" id="txt_EVOP_C17"  size="4000"  maxlength="4000" style="width:95%"  tabindex="101" onblur="guardarContenidoDocumento()"   onkeyup="this.value=this.value.toUpperCase()"> </textarea>     
     </td> 
 
  </tr> 
  <tr class="camposRepInp" >
     <td>Reflejos pupilares</td> 
     <td>Cover test</td>   
     <td>Punto Pr&oacute;ximo de Convergencia</td> 
     <td>Ducciones</td>
  <tr>                     
  <tr class="estiloImput">
     <td>
         <input type="text"  value="" id="txt_EVOP_C18"  style="width:98%"   size="20"  maxlength="1000" onblur="guardarContenidoDocumento()"  tabindex="200" >
     </td>          
     <td>     
         VL:<input type="text"  value="" id="txt_EVOP_C19"  style="width:40%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >
         VP:<input type="text"  value="" id="txt_EVOP_C20"  style="width:40%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200">         
     </td> 
     <td>     
         Real:<input type="text"  value="" id="txt_EVOP_C21"  style="width:20%"   size="20" maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >
         Luz:<input type="text"  value="" id="txt_EVOP_C22"  style="width:20%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >         
         FR:<input type="text"  value="" id="txt_EVOP_C23"  style="width:20%"   size="20" maxlength="20" onblur="guardarContenidoDocumento()"  tabindex="200" >                  
     </td> 
     <td>     
         OD:<textarea type="text" id="txt_EVOP_C24"   size="100"  maxlength="100" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()" > </textarea>
         OI:<textarea type="text" id="txt_EVOP_C25"   size="100"  maxlength="100" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()" > </textarea>
     </td> 
  </tr> 
  <tr class="camposRepInp" >
     <td colspan="2">Versiones</td> 
     <td>Oftalmoscopia Ojo Derecho</td> 
     <td>Oftalmoscopia Ojo Izquierdo</td>     
  <tr class="estiloImput">
     <td colspan="2">     
         <input type="text"  value="" id="txt_EVOP_C26"  style="width:80%"   size="1000"  maxlength="50" onblur="guardarContenidoDocumento()"  tabindex="200" >                           
     </td>
     <td>         
         <textarea type="text" id="txt_EVOP_C27"   size="1000"  maxlength="2000" style="width:90%" tabindex="100" onblur="guardarContenidoDocumento()" > </textarea>         
     </td>
     <td>         
         <textarea type="text" id="txt_EVOP_C28"   size="1000"  maxlength="2000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()" > </textarea>         
     </td> 
</table>  
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos1"> 
              <td width="40%">REFRACCION</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td><input type="text" id="txt_EVOP_C29" maxlength="25" style="width:70%" onblur="guardarContenidoDocumento()" /> </td>
              <td>
			     <select size="1" id="txt_EVOP_C30" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
                    <option value=""></option>                   
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select>              
              </td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td><input type="text" id="txt_EVOP_C31" maxlength="25"  style="width:70%" onblur="guardarContenidoDocumento()"  /> </td>
              <td>
			     <select size="1" id="txt_EVOP_C32" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
                    <option value=""></option>                   
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select>              
              </td>                
           </tr>     
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos2"> 
              <td width="40%"> QUERATOMETRIA</td>                               
              <td width="30%">Valor</td>
              <td width="30%">&nbsp;</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td><input type="text" id="txt_EVOP_C33" maxlength="25" style="width:70%" onblur="guardarContenidoDocumento()"  /> </td>
              <td>&nbsp;</td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td><input type="text" id="txt_EVOP_C34" maxlength="25" style="width:70%" onblur="guardarContenidoDocumento()"  /> </td>
              <td>&nbsp;</td>                
           </tr>     
      </table> 
  </td>   
</tr> 

<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos1"> 
              <td width="40%">SUBJETIVO</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td><input type="text" id="txt_EVOP_C35" maxlength="25" style="width:70%" onblur="guardarContenidoDocumento()"  /> </td>
              <td>
				<input type="text" id="txt_EVOP_C36" size="100"  style="width:99%" onblur="guardarContenidoDocumento()" tabindex="14" />
			    <!-- <select size="1" id="txt_EVOP_C36" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
                    <option value=""></option>                   
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select>   -->            
              </td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td><input type="text" id="txt_EVOP_C37" style="width:70%" onblur="guardarContenidoDocumento()"  /> </td>
              <td>
				<input type="text" id="txt_EVOP_C38" size="100"  style="width:99%" onblur="guardarContenidoDocumento()" tabindex="14" />
			    <!-- <select size="1" id="txt_EVOP_C38" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
                    <option value=""></option>                   
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select>        -->       
                            
              </td>                
           </tr>
			<tr class="estiloImput"> 
              <td>Agudeza visual binocular</td>                               
              <td colspan="2">
				<input type="text" id="txt_EVOP_C65" size="50"  maxlength="100" style="width:90%" onblur="guardarContenidoDocumento()"  />
			  </td>
           </tr>
            <tr class="estiloImput"> 
              <td>Adici&oacute;n</td>                               
              <td colspan="2">
				<input type="text"  value="" id="txt_EVOP_C39"  style="width:50%"   size="20"  onblur="guardarContenidoDocumento()"  tabindex="200" >	
			   <!-- <select size="1" id="txt_EVOP_C39" style="width:50%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="+1.00">+1.00</option>
                    <option value="+1.25">+1.25</option>
                    <option value="+1.50">+1.50</option>
                    <option value="+1.75">+1.75</option>
                    <option value="+2.00">+2.00</option>
                    <option value="+2.25">+2.25</option>
                    <option value="+2.50">+2.50</option>
                    <option value="+2.75">+2.75</option>                    
                    <option value="+3.00">+3.00</option>
                 </select>   -->            
               </td>
           </tr>               
            <tr class="estiloImput"> 
              <td>Distancia Pupilar</td>                               
              <td colspan="2"><input type="text" id="txt_EVOP_C40" maxlength="40" style="width:40%" onblur="guardarContenidoDocumento()"  /> </td>
           </tr>		   
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos2"> 
              <td width="16%">RX FINAL</td>                               
              <td width="30%">Esfera&nbsp;&nbsp;&nbsp;&nbsp;Cilindro&nbsp;&nbsp;&nbsp;&nbsp;Eje&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td width="27%">Agudeza Visual Lejana</td>                
              <td width="27%">Agudeza Visual Pr&oacute;xima</td>                              
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td>
                  <input type="text" id="txt_EVOP_C41" maxlength="25" style="width:25%" onblur="guardarContenidoDocumento()"  /> &nbsp;&nbsp;
                  <input type="text" id="txt_EVOP_C42" maxlength="25" style="width:25%" onblur="guardarContenidoDocumento()"  /> &nbsp;&nbsp;
                  <input type="text" id="txt_EVOP_C43" maxlength="25" style="width:25%" onblur="guardarContenidoDocumento()"  /> &nbsp;&nbsp;                                   
              </td>
              <td> 
				<input type="text" id="txt_EVOP_C44" size="100"  style="width:99%" onblur="guardarContenidoDocumento()" tabindex="14" />
			    <!-- <select size="1" id="txt_EVOP_C44" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
					<option value=""></option>                     
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>
                    <option value="NO CORRIGE">NO CORRIGE</option>                                                                  
                 </select>  -->
              </td>
              <td> 
				<input type="text" id="txt_EVOP_C45" size="100"  style="width:99%" onblur="guardarContenidoDocumento()" tabindex="14" />
			    <!-- <select size="1" id="txt_EVOP_C45" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
                    <option value=""></option>  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>
					<option value="NO CORRIGE">NO CORRIGE</option>

                 </select>  -->
              </td>                              
           </tr>    
                                                                                   
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td>
                 <input type="text" id="txt_EVOP_C46" maxlength="25" style="width:25%" onblur="guardarContenidoDocumento()"  />  &nbsp;&nbsp;
                 <input type="text" id="txt_EVOP_C47" maxlength="25" style="width:25%" onblur="guardarContenidoDocumento()"  />  &nbsp;&nbsp;
                 <input type="text" id="txt_EVOP_C48" maxlength="25" style="width:25%" onblur="guardarContenidoDocumento()"  />  &nbsp;&nbsp;                                  
              </td>
              <td>
				<input type="text" id="txt_EVOP_C49" size="100"  style="width:99%" onblur="guardarContenidoDocumento()" tabindex="14" />
                 <!-- <select size="1" id="txt_EVOP_C49" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
                    <option value=""></option>  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>
                    <option value="NO CORRIGE">NO CORRIGE</option>                                                      
                 </select>   -->  
              </td>   
              <td>
				<input type="text" id="txt_EVOP_C50" size="100"  style="width:99%" onblur="guardarContenidoDocumento()" tabindex="14" />
                <!--  <select size="1" id="txt_EVOP_C50" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14">	  
                    <option value=""></option>  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>
                    <option value="NO CORRIGE">NO CORRIGE</option>                                                       
                 </select>     -->
              </td>                             
           </tr>  
            <tr class="estiloImput"> 
              <td>Adici&oacute;n</td>                               
              <td colspan="2">
				<input type="text"  value="" id="txt_EVOP_C51"  style="width:50%"   size="20"  onblur="guardarContenidoDocumento()"  tabindex="200" >
                <!-- <select size="1" id="txt_EVOP_C51" style="width:50%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="+1.00">+1.00</option>
                    <option value="+1.25">+1.25</option>
                    <option value="+1.50">+1.50</option>
                    <option value="+1.75">+1.75</option>
                    <option value="+2.00">+2.00</option>
                    <option value="+2.25">+2.25</option>
                    <option value="+2.50">+2.50</option>
                    <option value="+2.75">+2.75</option>
                    <option value="+3.00">+3.00</option>
                 </select>   -->            
              </td>
           </tr>               
            <tr class="estiloImput"> 
              <td>Observaciones:</td>                               
              <td colspan="2">
				<input type="text"  value="" id="txt_EVOP_C64"  style="width:99%"   size="20"  maxlength="800" onblur="guardarContenidoDocumento()"  tabindex="200" >	
			  </td>
           </tr>     
      </table> 
  </td>   
</tr>  
<tr class="estiloImput"> 
  <td colspan="3" bgcolor="#009999">&nbsp;</td>
</tr>


<tr class="estiloImput"> 
  <td width="100%" colspan="2">
   <TABLE width="100%">
      <tr class="titulos"> 
        <td colspan="2">Especificaciones del lente</td>
        <td>Filtro</td>  
        <td>Color</td>    
      </tr>
      <tr class="estiloImput"> 
        <td colspan="2"><input type="text" id="txt_EVOP_C52" maxlength="500" style="width:90%" onblur="guardarContenidoDocumento()"  /> </td>  
        <td><input type="text" id="txt_EVOP_C53"  maxlength="100" style="width:90%" onblur="guardarContenidoDocumento()"  /> </td>    
        <td><input type="text" id="txt_EVOP_C54"  maxlength="100" style="width:90%" onblur="guardarContenidoDocumento()"  /> </td>            
      </tr>
      <tr class="titulos"> 
        <td>Uso</td>  
        <td>control</td>    
        <td colspan="2">Recomendaciones</td>            
      </tr>
      <tr class="estiloImput"> 
        <td><input type="text" id="txt_EVOP_C55" style="width:90%" maxlength="100" onblur="guardarContenidoDocumento()"  /> </td>  
        <td><input type="text" id="txt_EVOP_C56" style="width:90%" maxlength="100" onblur="guardarContenidoDocumento()"  /> </td>    
        <td colspan="2"><input type="text" id="txt_EVOP_C57" maxlength="500" style="width:90%" onblur="guardarContenidoDocumento()"  /> </td>            
      </tr>      
       <tr>  
         <td colspan="4" align="right">
            <input id="btnProcedim_" type="button" class="small button blue" value="Impresion Ireport" onClick="formulaPDFHC();" />
            <input id="btnProcedimientoImprimir" title="btn_o57p" type="button" class="small button blue" value="Imprime formula opt&aacute;lmica" onclick="imprimirAnexoOptalmica()" tabindex="203"  />
         </td>                    
       <tr>       
    </TABLE>    
   </td>
</tr>    
 
</table> 



<TABLE width="100%">
                        <tr class="estiloImput"> 
              <td width="25%" align="center">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:60%" title="76" tabindex="14" onfocus="limpiarDivEditarJuan('limpiarMotivoFolioEdit');" >
                      <option value=""></option>         
                      <option value="7">Cancelado Finalizado</option>
                      <option value="10">Reprogramado Finalizado</option>
                        </select>              
              </td>                   
              <td  width="25%" align="center">
              MOTIVO:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"  tabindex="14" onfocus="limpiaAtributo('cmbMotivoClaseFolioEdit',0)">  
                          <option value=""></option> 
                          <option value="26">ATRIBUIBLE AL PACIENTE</option>  
                          <option value="27">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                   
                        </select>  
              </td>    


              <td width="25%" align="center">
                CLASIFICACION:
                    <select id="cmbMotivoClaseFolioEdit" style="width:60%" tabindex="121" onfocus="comboDependienteDosCondiciones('cmbMotivoClaseFolioEdit','cmbIdMotivoEstadoEdit','lblIdDocumento','565')">
                    <option value=""></option>                      
                </select> 
                </td>

              <td width="25%" align="center">
             <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO FOLIO" onclick="cambioEstadoFolio()">                                    
        
              </td> 
           </tr>  
                </TABLE>


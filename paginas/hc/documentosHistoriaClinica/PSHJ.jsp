<table width="100%"  cellpadding="0" cellspacing="0"  align="center">

    <tr class="camposRepInp">
        <td  bgcolor="#c0d3c1" >DATOS DE IDENTIFICACI&Oacute;N DEL PACIENTE</td> 
    </tr>

    <tr class="estiloImput">

        <td >         
<textarea type="text" id="txt_PSHJ_C1" rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()" 
placeholder="Orientaci&oacute;n sexual:
Estado Civil:
Religi&oacute;n:
Grupo Poblacional:"></textarea>         
        </td>  

    </tr>
    <tr class="camposRepInp"> 
        <td bgcolor="#c0d3c1" >1. EVALUACI&Oacute;N CLINICA PSICOLOGICA</td>
    </tr>
    <tr>
        <td width="100%" >
            <table width="100%" align="center">

                <tr class="camposRepInp"> 
                    <td width="37%">FACTORES DESENCADENANTES</td> 
                    <td width="37%">MANTENEDORES DEL PROBLEMA</td>
                </tr>     
                <tr class="estiloImput"> 
                    <td>         
                        <textarea type="text" id="txt_PSHJ_C2"  placeholder="Hace referencia a los hechos que produjeron el problema, tiempo en el que aparecieron, por que no ha podido solucionar su problema." rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()" ></textarea>         
                    </td>  
                    <td>
                        <textarea type="text" id="txt_PSHJ_C3"  placeholder="Que personas, situaciones o cosas empeoran o mantienen las dificultades."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                    </td> 
                </tr>  
            </table>
        </td>


    <tr class="camposRepInp"> 
        <td >ANTECEDENTES PERSONALES Y FAMILIARES</td>                   

    </tr>     
    <tr class="estiloImput"> 
        <td >         
            <textarea type="text" id="txt_PSHJ_C4"  placeholder="Estados, situaciones o vivencias que la persona o sus familiares han padecido y pueden estar influyendo en la sintomatologia actual. Tener en cuenta la manifestacion de tipos de violencias en la vida familiar, de pareja, laboral o social. Asi mismo, la manifestacion de conducta suicida en la familia, personas significativas o en el mismo paciente."  rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
        </td> 
    </tr>  


    <tr class="camposRepInp"> 
        <td  bgcolor="#c0d3c1" >2.  EXTRUCTURA Y FUNCIONAMIENTO FAMIIAR</td>
    </tr>


    <tr>
        <td width="100%">

            <table width="100%"   align="center">
                <tr class="estiloImput">
                    <td><b>MIEMBRO</b></td>
                    <td><b>PARENTESCO</b></td>
                    <td><b>EDAD</b></td>
                    <td><b>ESCOLARIDAD</b></td>
                    <td><b>OCUPACI&Oacute;N</b></td>

                </tr>
                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHJ_C5" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C6" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C7" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C8" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C9" style="width:80%"/></td>
                </tr>

                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHJ_C10" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C11" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C12" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C13" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C14" style="width:80%"/></td>
                </tr>

                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHJ_C15" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C16" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C17" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C18" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C19" style="width:80%"/></td>
                </tr>


            </table>
        </td>
    </tr>

    <tr class="camposRepInp">
        <td  bgcolor="#c0d3c1" >3. EXAMEN MENTAL</td>
    </tr>     
    <tr class="camposRepInp">
        <td>1. APARIENCIA PERSONAL</td>
    </tr>   

    <tr>
        <td >

            <table width="100%"   align="center">
                <tr class="estiloImput">

                    <td><b>EXPRESI&Oacute;N FACIAL</b></td>
                    <td><b>CONTACTO VISUAL</b></td>
                    <td><b>GESTOS PARTICULARES</b></td>

                </tr>

                <tr class="estiloImput"> 

<td>
<textarea type="text" id="txt_PSHJ_C20"  placeholder="Alerta
Vigilante
Depresiva"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>  
</td>  
<td>
<textarea type="text" id="txt_PSHJ_C21"  placeholder="Directo
Esquivo"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>  
</td> 
<td>
<textarea type="text" id="txt_PSHJ_C22"  placeholder="Tics
Cicatrices
Otros"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>  
</td> 
                </tr>


            </table>

        </td>

    </tr>

    <tr class="camposRepInp">
        <td > 2. FUNCIONES COGNITIVAS</td>
    </tr>  

    <tr>
        <td >
            <table width="100%" align="center">
                <tr class="estiloImput">
                    <td><b>ATENCI&Oacute;N</b></td>
                    <td><b>CONCIENCIA</b></td>
                    <td><b>ORIENTACI&Oacute;N</b></td>
                    <td ><b>MEMORIA</b></td>
                </tr>

                <tr class="estiloImput"> 

<td>
<textarea type="text" id="txt_PSHJ_C23"  placeholder="Normal
Aumentada
Disminuida
Otra"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>  
</td> 
<td>
<textarea type="text" id="txt_PSHJ_C24"  placeholder="Vigilia
Hipervigilia
Somnolencia
Estupor"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>  
</td> 

<td>
<textarea type="text" id="txt_PSHJ_C25"  placeholder="En tiempo, espacio y persona
- Auto psiquico: nombre y apellido, edad, etc.
- Alo psiquica: ciudad, sitio , fecha."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
</td> 

<td>
<textarea type="text" id="txt_PSHJ_C26"  placeholder="Remota:
Reciente:
Inmediata:"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
</td> 

                </tr>
            </table>
        </td>

    </tr>

    <tr>
        <td >
            <table width="100%" align="center">
                <tr class="estiloImput">
                    <td ><b>PENSAMIENTO</b></td>
                    <td ><b>INTELIGENCIA</b></td>
                    <td colspan="2"><b>JUICIO Y RACIOCINIO</b></td>

                </tr>

                <tr class="estiloImput">
                    <td>
<textarea type="text" id="txt_PSHJ_C27"  placeholder="Producci&oacute;n:
Continuidad:
Contenido:"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                    </td>

                    <td>

                        <u>ABSTRACCI&Oacute;N</u><br>
                        <textarea type="text" id="txt_PSHJ_C28"  placeholder="Ej:Hijo de tigre sale pintado. Vale pajaro en mano que 100 volando."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         

                        <br><br>
                        <u>CAPACIDAD DE CALCULO</u><br>
                        <textarea type="text" id="txt_PSHJ_C29"  placeholder="100-3=, 45+12. Mas de 7 errores indican problemas de calculo."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         

                    </td>

                    <td>
                        <u>REALIDAD INTERNA</u><br>
                        <textarea type="text" id="txt_PSHJ_C30"  placeholder="Habla sobre su enfermedad y limitaciones"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                        <br><br>
                        <u>REALIDAD EXTERNA</u><br>
                        <br>
                        <textarea type="text" id="txt_PSHJ_C31"  placeholder="Desconfianza y culpabilidad hacia el exterior"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         

                    </td>

                    <td>
                        <u>PROSPECCI&Oacute;N</u><br>
                        <textarea type="text" id="txt_PSHJ_C32"  placeholder="Tiene visi&oacute;n de objetivos y metas futuras"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                        <br>

                    </td>

                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td >
            <table width="100%">
                <tr class="camposRepInp">
                    <td >3. EVALUACI&Oacute;N EMOCIONAL</td>
                    <td >4. MOTRICIDAD</td>
                    <td colspan="2" >5. LENGUAJE</td>
                </tr>

                <tr class="estiloImput">
<td>
<textarea type="text" id="txt_PSHJ_C33"  placeholder="Alegria
Euforia
Labilidad
Miedo
Irritabilidad
Depresi&oacute;n
Ansiedad
Agresi&oacute;n"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
</td>  

<td>                       
<textarea type="text" id="txt_PSHJ_C34"  placeholder="Normal
Disminuida
Aumentada
Otra:"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
</td>  


<td><br>
<b>COMUNICACI&Oacute;N VERBAL</b><br><br>
<u>CONTENIDO DEL LENGUAJE</u><br>         
<textarea type="text" id="txt_PSHJ_C35"  placeholder="Rico
Pobre
Coordinado
Lento
Rapido"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         

<br><br>
<u>TONO DE VOZ</u><br>         
<textarea type="text" id="txt_PSHJ_C36"  placeholder="Audible
No Audible
Suave 
Fuerte
Variable
Agresivo
Estereotipo
Mon&oacute;tono"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         

</td>  

                    <td>
                        <b>COMUNICACI&Oacute;N NO VERBAL</b><br><br>
                        <u>ALTERACIONES_EMOCIONALES</u><br>
                        <textarea type="text" id="txt_PSHJ_C37"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                        <br><br>
                        <u>ALTERACIONES_ORGANICAS</u><br>    
                        <textarea type="text" id="txt_PSHJ_C38"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         

                    </td>


                </tr>

            </table>
        </td>
    </tr>


    <tr class="camposRepInp">
        <td >6. EVALUACI&Oacute;N DE SENSOPERCEPCI&Oacute;N</td>
    </tr>
    <tr>
        <td >
            <table width="100%" align="center">
                <tr  class="estiloImput">
<td><br><b>ILUSIONES</b>
<textarea type="text" id="txt_PSHJ_C39"  placeholder="Visuales 
Auditivas
Tactiles
Gustativas
Olfativas"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         


</td>
<td>

<br><b>ALUCINACIONES</b>

<br><br>
<textarea type="text" id="txt_PSHJ_C40"  placeholder="Visuales 
Auditivas
Tactiles
Gustativas
Olfativas"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         

</td>

                    <td colspan="2">

                        <br><b>DISTORSION DEL ESQUEMA PERSONAL</b>
                        <br><br>
                        <textarea type="text" id="txt_PSHJ_C41"  placeholder="Despersonalizacion"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>

                    </td>

                </tr>
            </table>
        </td>
    </tr>

    <tr class="camposRepInp">
        <td >7. FUNCIONES SOMATICAS</td>
    </tr>
    <tr>
        <td >
            <table width="100%" align="center">
                <tr class="estiloImput">

                    <td><br><b>SUE&Ntilde;O</b>
                        <br><br>

<textarea type="text" id="txt_PSHJ_C42"  placeholder="Dificultad para conciliar
Dificultad para reconciliar
Despertar temprano
Pesadillas"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>


</td>

<td><br><b>APETITO</b>
<br><br>

<textarea type="text" id="txt_PSHJ_C43"  placeholder="Disminuido
Aumentado
Normal"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>

</td>

                </tr>
            </table>
        </td>
    </tr>
    <tr class="camposRepInp">
        <td>8. INTEGRIDAD COMPORTAMENTAL</td>
    </tr>

    <tr>
        <td >
            <table width="100%" align="center">
                <tr class="estiloImput">
                    <td><b>DESCUIDO DEL AUTOCUIDADO</b></td>
                    <td><b>OLVIDO FRECUENTE DE INTENSIONES</b></td>
                    <td><b>DESINHIBICI&Oacute;N MOTORA</b></td>
                    <td ><b>RUPTURA DE NORMAS SOCIALES</b></td>
                </tr>

                <tr class="estiloImput">

                    <td>
                        <textarea type="text" id="txt_PSHJ_C44"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                    </td>
                    <td>
                        <textarea type="text" id="txt_PSHJ_C45"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                    </td>
                    <td>
                        <textarea type="text" id="txt_PSHJ_C46"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                    </td>
                    <td>
                        <textarea type="text" id="txt_PSHJ_C47"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                    </td>

                </tr>

                <tr class="estiloImput">
                    <td><b>IRRESPONSABILIDAD CON EL DINERO</b></td>
                    <td><b>DESCONFIANZA EXAGERADA</b></td>
                    <td><b>AUTOAGRESI&Oacute;N</b></td>
                    <td ><b>DISMINUCI&Oacute;N DE LA SOCIABILIDAD</b></td>
                </tr>

                <tr class="estiloImput">

                    <td>
                        <textarea type="text" id="txt_PSHJ_C48"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                    </td>
                    <td>
                        <textarea type="text" id="txt_PSHJ_C49"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                    </td>
                    <td>
                        <textarea type="text" id="txt_PSHJ_C50"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                    </td>
                    <td>
                        <textarea type="text" id="txt_PSHJ_C51"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                    </td>

                </tr>

                <tr class="estiloImput">
                    <td><b>AUSENCIA DE SENTIDO DE ENFERMEDAD O RECHAZO AL TRATAMIENTO</b></td>
                </tr>
                <tr class="estiloImput">

                    <td>
                        <textarea type="text" id="txt_PSHJ_C52"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>         
                    </td>
                </tr>

            </table>
        </td>
    </tr>

    <tr class="camposRepInp">
        <td bgcolor="#c0d3c1" >4. EVALUACION DE AREAS DE DESEMPE&Ntilde;O</td>
    </tr>

    <tr>
        <td >
            <table width="100%" align="center">
                <tr class="camposRepInp">
                    <td>1. AREA SOCIAL</td>
                    <td>2. AREA ACADEMICA</td>
                    <td>3. AREA AFECTIVA</td>
                    <td>4. AREA FAMILIAR</td>
                </tr>


                <tr class="estiloImput">
                    <td><textarea type="text" id="txt_PSHJ_C53"  placeholder="caracteristicas de sociabilidad, grupo(s) al que pertenece, tipo de actividades sociales, habilidades sociales, satisfaccion y relaciones interpersonales."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C54"  placeholder="Institucion Educativa, grado, (adaptacion al ambiente escolar, proceso de aprendizaje, satisfaccion y rendimiento),"  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C55"  placeholder="manejo de las relaciones afectivas, manifestaciones de tipos de acercamiento y enamoramiento. Evaluar sentimiento hacia si mismo(a), identificar si hay afecto triste o posibles factores de riesgo para conducta suicida."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C56"  placeholder="Topologia y caracteristicas de los procesos de comunicacion, manifestaciones de afecto y control de conducta al interior del grupo familiar."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                </tr>



                <tr class="camposRepInp">
                    <td>5. AREA PSICOSEXUAL </td>
                    <td>6. AREA DE CONSUMO DE SPA </td>
                    <td>7. AREA IDENTIFICACION, MANEJOY SEGUIMIENTO DE NORMAS Y LIMITES</td>
                    <td>8. AREA IDEAS SUICIDAS Y ACTITUDES FRENTE A LA VIDA</td>

                </tr>

                <tr class="estiloImput">
                    <td><textarea type="text" id="txt_PSHJ_C57"  placeholder="Identidad y orientacion sexual, tipo de experiencias, practicas de auto cuidado, identificar posibles problematicas relacionadas con violencia sexual. Satisfaccion en su vida sexual."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C58"  placeholder="Tipo de sustancias, evolucion, frecuencia, severidad y afectacion."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C59"  placeholder="Caracteristicas del o la joven y actitud frente a las normas e imagenes de autoridad."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C60"  placeholder="Hay episodios de tristeza, pensamientos e ideas de quitarse la vida, encuentra sentido de vida."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                </tr>


            </table>

        </td>
    </tr>

    <tr class="camposRepInp">
        <td bgcolor="#c0d3c1" >5. RED DE APOYO</td>
    </tr>


    <tr>
        <td width="100%">

            <table width="100%"   align="center">
                <tr class="estiloImput">
                    <td><b>MIEMBRO</b></td>
                    <td><b>PARENTESCO</b></td>
                    <td><b>EDAD</b></td>
                    <td><b>ESCOLARIDAD</b></td>
                    <td><b>OCUPACI&Oacute;N</b></td>                    
                    <td><b>TELEFONO</b></td>
                    <td><b>DIRECCI&Oacute;N</b></td>


                </tr>
                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHJ_C61" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C62" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C63" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C64" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C65" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C66" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C67" style="width:80%"/></td>

                </tr>

                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHJ_C68" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C69" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C70" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C71" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C72" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C73" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C74" style="width:80%"/></td>
                </tr>

                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHJ_C75" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C76" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C77" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C78" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C79" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C80" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHJ_C81" style="width:80%"/></td>
                </tr>


            </table>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            <table width="100%" align="center">

                <tr class="camposRepInp">
                    <td bgcolor="#c0d3c1" >6. EXPECTATIVAS FRENTE AL PROCESO DE INTERVENCI&Oacute;N</td>
                    <td bgcolor="#c0d3c1" >7. IMPRESI&Oacute;N DIAGNOSTICA</td>
                </tr>
                <tr class="estiloImput">
                    <td><textarea type="text" id="txt_PSHJ_C82"  placeholder="Que espera del presente proceso."  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C83"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                </tr>

            </table>

        </td>

    </tr>

    <tr class="camposRepInp">
        <td bgcolor="#c0d3c1" >8. PLAN DE MANEJO </td>
    </tr>

    <tr>
        <td >
            <table width="100%" align="center">
                <tr class="camposRepInp">
                    <td>ENFOQUE TERAPEUTICO</td>
                    <td>PLAN DE SEGUIMIENTO</td>
                    <td>TECNICA PSICOTERAPEUTICA A APLICAR </td>
                    <td>PRUEBAS PSICOLOGICAS ADICIONALES </td>
                </tr>


                <tr class="estiloImput">
                    <td><textarea type="text" id="txt_PSHJ_C84"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C85"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C86"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C87"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>

                </tr>

                <tr class="camposRepInp">
                    <td>IDENTIFICAR NIVEL DE RIESGO </td>
                    <td>PSICOEDUCACION Y RECOMENDACIONES</td>
                    <td>REMISION A II NIVEL DE ATENCION U OTROS SERVICIOS </td>
                    <td>ACTIVACION DE RUTAS DE ATENCION ESPECIFICAS </td>
                </tr>


                <tr class="estiloImput">
                    <td><textarea type="text" id="txt_PSHJ_C88"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C89"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C90"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHJ_C91"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea></td>

                </tr>


            </table> 
        </td>
    </tr>
</table>


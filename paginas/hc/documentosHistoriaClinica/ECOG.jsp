
<table width="100%"  align="center">
  <tr>
    <td >
      <table width="100%" align="center">
            <tr class="titulos"> 
              <td width="20%">EXAMEN CORRECTO</td>                               
              <td width="20%">SITIO DE EXAMEN CORRECTO</td>
              <td width="20%">DATOS DEL PACIENTE CORRECTOS</td>                
              <td width="20%">CANTIDAD DE EXAMENES CORRECTA</td>                
              <td width="20%">RIESGOS POR MEDICAMENTOS </td>                                                
            </tr>		
            <tr class="estiloImput"> 
              <td>
                 <select size="1" id="txt_ECOG_C1" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_ECOG_C2" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_ECOG_C3" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_ECOG_C4" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>   
              <td>
                 <select size="1" id="txt_ECOG_C5" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="NA" selected="selected">NA</option>                    
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
            </tr>   
            
            <tr class="titulos"> 
              <td width="20%">PREPARACION PREVIA DEL PACIENTE</td>                               
              <td width="20%">DOCUMENTOS NECESARIOS</td>
              <td width="20%">CONSENTIMIENTO INFORMADO</td>                
              <td width="40%" colspan="2">OBSERVACIONES</td>                
            </tr>                                                                                     
  
            <tr class="estiloImput"> 
              <td>
                 <select size="1" id="txt_ECOG_C6" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_ECOG_C7" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="NA">NA</option>                    
                    <option value="SI" selected="selected">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_ECOG_C8" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
                    <option value="NA">NA</option>                    
                    <option value="SI" selected="selected">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td colspan="2">
      	      <textarea type="text" id="txt_ECOG_C9" rows="6"   maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>                  
              </td>
           </tr>   
 
           <tr class="titulos"> 
              <td>INFORME</td>
              <td>OJO DERECHO</td>
              <td>OJO IZQUIERDO</td>     
              <td>COMENTARIO</td>          
           </tr>  
         
           <tr class="estiloImput"> 
              <td>
      	         <textarea type="text" id="txt_ECOG_C10" rows="6"   maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>                  
              </td>
              <td>
      	         <textarea type="text" id="txt_ECOG_C11" rows="6"   maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>                  
              </td>
              <td>
      	         <textarea type="text" id="txt_ECOG_C12" rows="6"   maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>                  
              </td>
              <td>
      	         <textarea type="text" id="txt_ECOG_C13" rows="6"   maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>                  
              </td>
           </tr>   
           <tr>    
              <td colspan="5">
                
                <TABLE width="100%">
                        <tr class="estiloImput"> 
              <td width="25%" align="center">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:60%" title="76" tabindex="14" onfocus="limpiarDivEditarJuan('limpiarMotivoFolioEdit');">
                      <option value=""></option>                      
                          <option value="2">Tomado</option>
                          <option value="3">Enviado</option>                    
                          <option value="4">Interpretado</option>                                        
                          <option value="5">Enviado urgente</option>   
                          <option value="9">Finalizado Alerta</option>    
                          <option value="7">Cancelado Finalizado</option>   
                          <option value="10">Reprogramado Finalizado</option>
                        </select>              
              </td>                   
              <td  width="25%" align="center">
              MOTIVO:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"  tabindex="14" onfocus="limpiaAtributo('cmbMotivoClaseFolioEdit',0)">  
                         <option value=""></option> 
                          <option value="26">ATRIBUIBLE AL PACIENTE</option>  
                          <option value="27">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                 
                        </select>  
              </td>    


              <td width="25%" align="center">
                CLASIFICACION:
                    <select id="cmbMotivoClaseFolioEdit" style="width:60%" tabindex="121" onfocus="comboDependienteDosCondiciones('cmbMotivoClaseFolioEdit','cmbIdMotivoEstadoEdit','lblIdDocumento','565')">
                    <option value=""></option>                      
                </select> 
                </td>

              <td width="25%" align="center">
             <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO FOLIO" onclick="cambioEstadoFolio()">                                    
        
              </td> 
           </tr>  
                </TABLE>
      </td>
</tr> 
           <tr class="estiloImput"> 
              <td colspan="5" align="CENTER">EB DIAGNOSTICO CIE 10 FAVOR COLOCAR EL DIGNOSTICO MAS APROXIMADO AL RESULTADO Y EN OBSERVACIONES FAVOR DESCRIBIR EL RESULTADO DE LA ECOGRAFIA.
              </td>                   
           </tr>                       
                         
      </table> 
    </td>   
  </tr>   
</table>  

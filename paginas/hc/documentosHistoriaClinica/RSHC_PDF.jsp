

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="titulos" >
     <td width="100%" colspan="4">PRESCRIPCION DE HEMODIALISIS </td> 
  </tr>   
  <tr class="inputBlanco">
     <td>     
		FILTRO
        <label id="lbl_RSHC_C1" ></label>
     </td>
	 <td>
		ANTIGENO
        <label id="lbl_RSHC_C2" ></label>
	 </td>
	 <td>     
		HEPARINA
     </td>
	 <td>
        <label id="lbl_RSHC_C3" ></label>
	 </td>
  </tr>
  <tr class="inputBlanco">
     <td>     
		ACCESO VASCULAR
     </td>
	 <td>
        <label id="lbl_RSHC_C4" ></label>
	 </td>
	 <td>     
		ULTRAFILTRACION
     </td>
	 <td>
        <label id="lbl_RSHC_C5" ></label>
	 </td>
  </tr>
  <tr class="inputBlanco">
     <td>     
		VELOCIDAD DE FLUJO
     </td>
	 <td>
        <label id="lbl_RSHC_C6" ></label>
	 </td>
	 <td>     
		TIEMPO
     </td>
	 <td>
        <label id="lbl_RSHC_C7" ></label>
	 </td>
  </tr>  
  <tr class="titulos" >
     <td width="100%" colspan="4">PESO </td> 
  </tr> 
  <tr class="inputBlanco">
     <td>INICIAL</td>
     <td>FINAL</td>
     <td>SECO</td>
  </tr>
  <tr class="inputBlanco">
     <td><label id="lbl_RSHC_C8" ></label>&nbsp; kg</td>
     <td><label id="lbl_RSHC_C9" ></label>&nbsp; kg</td>
     <td><label id="lbl_RSHC_C10" ></label></td>
  </tr>
  <tr class="inputBlanco">
	<td colspan="2">
		Eritropoyetina  2000 unidades&nbsp;
		<label id="txt_RSHC_C12" ></label>
		&nbsp;&nbsp;		
	</td>
	<td colspan="2">
		HIERRO I.V. &nbsp;
		<label id="txt_RSHC_C14"></label>		
	</td>
  </tr>
</table>  

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="titulos" >
     <td width="100%">Observaciones</td> 
  </tr>                     
  <tr class="inputBlanco">
     <td>     
         <label id="lbl_RSHC_C11"  ></label>  
     </td> 
  </tr>   
</table>  
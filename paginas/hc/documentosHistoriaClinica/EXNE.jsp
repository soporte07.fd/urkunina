<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="100%">EVALUACION</td> 
  </tr>   
   <tr class="estiloImput">
      <td>
        <table width="100%" align="center">                  
          <tr>
            <td width="25%">EXAMEN ENDO Y EXOBUCAL VALORACION ESTATICA &#8211; DINAMICA</td>
            <td width="20%">FORMA</td>
            <td width="20%">MOVILIDAD</td>
            <td width="20%">SENSIBILIDAD</td> 
          </tr>
        </table>   
    </td>
    </tr>
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="25%" align="right"  class="estiloImput">LABIOS</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXNE_C1" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C2" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C3" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                     
                </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="25%" align="right"  class="estiloImput">VELO DEL PALADAR</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXNE_C4" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C5" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C6" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                     
                </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="25%" align="right"  class="estiloImput">MAXILARES &#8211; ATM</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXNE_C7" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C8" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C9" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                     
                </tr>   
          </table> 
        </td>
    </tr> 
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="25%" align="right"  class="estiloImput">PALADAR DURO</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXNE_C10" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C11" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C12" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                     
                </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="25%" align="right"  class="estiloImput">PALADAR BLANDO</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXNE_C13" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C14" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C15" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                     
                </tr>   
          </table> 
        </td>
    </tr>  
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="25%" align="right"  class="estiloImput">ENCIAS</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXNE_C16" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C17" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C18" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                     
                </tr>   
          </table> 
        </td>
    </tr>            
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
  <tr class="camposRepInp">
     <td width="100%">FUNCIONES ALIMENTICIAS</td> 
  </tr> 
  <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="16.6%" rowspan="2">SUCCION:</td>
                  <td width="16.6%">REFLEJO BUSQUEDA</td>
                  <td width="16.6%">REFLEJO LABIAL</td>                
                  <td width="16.6%">REFLEJO MORDEDURA</td>
                  <td width="16.6%">REFLEJO LINGUAL</td>
                  <td width="16.6%">REFLEJO DE SUCCION</td>
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                    <select size="1" id="txt_EXNE_C19" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >  
                       <option value=""></option> 
                        <option value="PRESENTE">PRESENTE</option> 
                        <option value="AUSENTE">AUSENTE</option>           
                    </select>
                  </td>                
                  <td>
                    <select size="1" id="txt_EXNE_C20" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >  
                       <option value=""></option> 
                        <option value="PRESENTE">PRESENTE</option> 
                        <option value="AUSENTE">AUSENTE</option>           
                    </select>   
                  </td>                
                  <td>
                     <select size="1" id="txt_EXNE_C21" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >  
                       <option value=""></option> 
                        <option value="PRESENTE">PRESENTE</option> 
                        <option value="AUSENTE">AUSENTE</option>           
                    </select>
                  </td>                
                  <td>
                     <select size="1" id="txt_EXNE_C22" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                       <option value=""></option> 
                        <option value="PRESENTE">PRESENTE</option> 
                        <option value="AUSENTE">AUSENTE</option>           
                    </select>  
                  </td>
                   <td>
                     <select size="1" id="txt_EXNE_C23" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                       <option value=""></option> 
                        <option value="PRESENTE">PRESENTE</option> 
                        <option value="AUSENTE">AUSENTE</option>           
                    </select>  
                  </td>                  
                </tr>     
          </table> 
        </td>
    </tr> 
    <tr class="estiloImput">
      <td>
        <table width="100%" align="center">                  
          <tr>
            <td width="25%">MUSCULATURA</td>
            <td width="20%">SENSIBILIDAD</td>
            <td width="20%">TONO</td>
            <td width="20%">FUNCION</td> 
          </tr>
        </table>   
    </td>
    </tr>
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="25%" align="right"  class="estiloImput">ORBICULARES</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXNE_C24" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C25" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C26" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                     
                </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="25%" align="right"  class="estiloImput">LINGUALES</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXNE_C27" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C28" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C29" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                     
                </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="25%" align="right"  class="estiloImput">MASETEROS  &#8211; PTERIGOIDEOS</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXNE_C30" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C31" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C32" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                     
                </tr>   
          </table> 
        </td>
    </tr> 
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="25%" align="right"  class="estiloImput">BUCCINADORES</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXNE_C33" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C34" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C35" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                     
                </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="25%" align="right"  class="estiloImput">MENTONIANO</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXNE_C36" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C37" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXNE_C38" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                     
                </tr>   
          </table> 
        </td>
    </tr>  
     <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
    <tr class="camposRepInp">
     <td width="100%">VIA DE ALIMENTACION</td> 
  </tr> 
  <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="25%">LACTANCIA MATERNA</td>
                  <td width="25%">BIBERON</td>
                  <td width="25%">SONDA NASOGASTRICA</td>                
                  <td width="25%">OTROS</td>
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                     <input type="text" id="txt_EXNE_C39" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXNE_C40" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXNE_C41" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <textarea type="text" id="txt_EXNE_C42"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
                   </td>
                  
                </tr>     
          </table> 
        </td>
    </tr>       
  <tr>
    <td>
      <table width="100%" align="center">                   
        <tr class="estiloImput">
          <td width="50%">OBSERVACIONES</td>
        </tr>   
        <tr class="camposRepInp"> 
          <td>
           <textarea type="text" id="txt_EXNE_C43"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
          </td>
         </tr>              
      </table> 
    </td>
  </tr>
  <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="25%">ESFUERZO</td>
                  <td width="25%">RUIDOS</td>
                  <td width="25%">DEFENSA TACTIL</td>                
                  <td width="25%">ACANALA LENGUA</td>
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                      <select size="1" id="txt_EXNE_C44" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select>
                  </td>                
                  <td>
                       <select size="1" id="txt_EXNE_C45" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select> 
                  </td>                
                  <td>
                      <select size="1" id="txt_EXNE_C46" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select> 
                  </td>                
                  <td>
                    <select size="1" id="txt_EXNE_C47" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>     
                   </td>                  
                </tr>     
          </table> 
        </td>
    </tr>  
      <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="25%">SENSIBILIDAD A LA PALPACION</td>
                  <td width="25%">MOV. ANTERO &#8211; POSTERIOR</td>
                  <td width="25%">MOVIMIENTOS ASOCIADOS</td>                
                  <td width="25%">CUALES</td>
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                      <select size="1" id="txt_EXNE_C48" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >   <option value=""></option> 
                        <option value="PRESENTE ">PRESENTE</option> 
                        <option value="AUSENTE">AUSENTE</option>           
                      </select>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXNE_C49" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXNE_C50" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <textarea type="text" id="txt_EXNE_C51"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
                   </td>                  
                </tr>     
          </table> 
        </td>
    </tr>      
   <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr class="camposRepInp" >
     <td width="100%">DEGLUCION</td> 
  </tr>                   
  <tr class="estiloImput">
     <td>     
          <p>REFLEJOS OROFARINGEOS RELACIONADOS CON LA DEGLUCION:</p>
     </td> 
  </tr>  
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="33.3%">PALATAL (PRESENCIA DE ARCADAS AL ESTIMULO)</td>
                  <td width="33.3%">NAUSEOSO (PRESENCIA DE ARCADAS EN LA BASE LINGUAL AL ESTIMULO)</td>
                  <td width="33.3%">LINGUAL (ANTICIPACION A LA ALIMENTACION)</td>
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                     <input type="text" id="txt_EXNE_C52" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXNE_C53" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXNE_C54" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
               </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="100%" colspan="4">VELOCIDAD  SECUENCIA DE LA DEGLUCION</td>
                </tr>  
                <tr class="estiloImput">
                  <td width="25%">NORMAL O CONTINUA</td>
                  <td width="25%">LENTA O DISCONTINUA</td>
                  <td width="25%">RAPIDA O INTERMITENTE</td>
                  <td width="25%">AUSENTE</td>

                </tr>

                <tr class="camposRepInp"> 
                  <td>
                     <select size="1" id="txt_EXNE_C55" style="width:26%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="CONTINUA">CONTINUA</option>           
                      </select>    
                  </td>                
                 <td>
                     <select size="1" id="txt_EXNE_C56" style="width:33%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="LENTA">LENTA</option> 
                        <option value="DISCONTINUA ">DISCONTINUA </option>           
                      </select>    
                  </td>                 
                  <td>
                     <select size="1" id="txt_EXNE_C57" style="width:33%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="RAPIDA ">RAPIDA</option> 
                        <option value="INTERMITENTE">INTERMITENTE</option>           
                      </select>    
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNE_C58" maxlength="20" style="width:50%" onblur="guardarContenidoDocumento()"/>
      
                  </td>               
               </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="100%" colspan="4">COMPORTAMIENTO MUSCULAR</td>
                </tr>  
                <tr class="estiloImput">
                  <td width="25%">COMPETENCIA LABIAL</td>
                  <td width="25%">INTERPOSICION LABIAL</td>
                  <td width="25%">INTERPOSICION LINGUAL</td>
                  <td width="25%">CONTRACCION</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                     <select size="1" id="txt_EXNE_C59" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>
                       <select size="1" id="txt_EXNE_C60" style="width:45%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SELLE LABIAL">SELLE LABIAL</option> 
                        <option value="OCLUSION DENTAL">OCLUSION DENTAL</option>           
                      </select>      
                  </td>                
                 <td>
                   <select size="1" id="txt_EXNE_C61" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SUPERIOR">SUPERIOR</option> 
                        <option value="INFERIOR">INFERIOR</option> 
                        <option value="NINGUNA">NINGUNA</option>                   
                      </select>   
                  </td>                 
                  <td>
                     <input type="text" id="txt_EXNE_C62" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      ORBICULARES:
                      <select size="1" id="txt_EXNE_C63" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select><br><br>
                      MENTONIANA: 
                      <select size="1" id="txt_EXNE_C64" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select> <br><br>
                      BUCCINADORES:
                      <select size="1" id="txt_EXNE_C65" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select><br><br>
                      MASETEROS: 
                      <select size="1" id="txt_EXNE_C66" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>  
                  </td>               
               </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="25%">ELEVACION LARINGEA</td>
                  <td width="25%">MOVIMIENTOS ASOCIADOS</td>
                  <td width="25%">TONICIDAD A LA PALPACION</td>
                  <td width="25%">SENSIBILIDAD</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                     <select size="1" id="txt_EXNE_C67" style="width:35%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >
                        <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="FORZADA">FORZADA</option>     
                        <option value="FORZADA">FORZADA</option>           
                        <option value="NO FORZADA">NO FORZADA</option>
                        <option value="DIFICULTOSA">DIFICULTOSA</option>
                     </select>
                  </td>                
                 <td>
                   <select size="1" id="txt_EXNE_C68" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option> 
                      </select><br>
                    CUALES:     
                     <textarea type="text" id="txt_EXNE_C69"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea> 
                  </td>                 
                  <td>
                      <select size="1" id="txt_EXNE_C70" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="AUMENTADA">AUMENTADA</option> 
                        <option value="DISMINUIDA">DISMINUIDA</option>  
                        <option value="NORMAL">NORMAL</option>           
                      </select>                  
                  </td> 
                  <td>
                       <select size="1" id="txt_EXNE_C71" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="AUMENTADA">AUMENTADA</option> 
                        <option value="DISMINUIDA">DISMINUIDA</option>  
                        <option value="NORMAL">NORMAL</option>           
                      </select>   
                  </td>               
               </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="100%" colspan="6">POSICION QUE FACILITA LA DEGLUCION</td>
                </tr>  
                <tr class="estiloImput">
                  <td width="100%" colspan="6">CABEZA</td>
                </tr>  
                <tr class="estiloImput">
                  <td width="16.6%">ERGUIDA</td>
                  <td width="16.6%">RECOSTADO</td>
                  <td width="16.6%">SEMISENTADO</td>
                  <td width="16.6%">SENTADO</td>
                  <td width="16.6%">ACOSTADO</td>
                  <td width="16.6%">PARADO</td>

                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNE_C72" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNE_C73" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>  
                  </td>                 
                  <td>
                     <input type="text" id="txt_EXNE_C74" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNE_C75" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>  
                   <td>
                      <input type="text" id="txt_EXNE_C76" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>  
                   <td>
                      <input type="text" id="txt_EXNE_C77" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>               
               </tr>     
          </table> 
        </td>
    </tr>
     <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="100%" colspan="4">MANEJO DEL ALIMENTO</td>
                </tr>  
                <tr class="estiloImput">
                  <td width="16.6%">RESIDUOS DESPUES DE DEGLUTIR</td>
                  <td width="16.6%">DERRAME DE ALIMENTO EN COMISURAS </td>
                  <td width="16.6%">ODINOFAGIA</td>
                  <td width="16.6%">REGURGITACION NASAL</td>

                </tr>
                <tr class="camposRepInp"> 
                  <td>
                       <select size="1" id="txt_EXNE_C78" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>  
                      </select>
                  </td>                
                 <td>
                     <select size="1" id="txt_EXNE_C79" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>  
                      </select>
                  </td>                 
                  <td>
                       <select size="1" id="txt_EXNE_C80" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="DURANTE">DURANTE</option> 
                        <option value="DESPUES">DESPUES</option>  
                      </select>
                  </td> 
                  <td>
                        <select size="1" id="txt_EXNE_C81" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>  
                      </select>                
                 </td>                 
               </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
 
                <tr class="estiloImput">
                  <td width="33.3%">RUIDOS AL TRAGAR</td>
                  <td width="33.3%">TOS</td>
                  <td width="33.3%">OTRO</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                       <select size="1" id="txt_EXNE_C82" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>  
                      </select>
                  </td>                
                 <td>
                     <select size="1" id="txt_EXNE_C83" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>  
                      </select>
                  </td>                 
                  <td>
                     <textarea type="text" id="txt_EXNE_C84"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea> 
                  </td>               
               </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="100%" colspan="3">ETAPA COMPROMETIDA</td>
                </tr>  
                <tr class="estiloImput">
                  <td width="33.3%">ORAL</td>
                  <td width="33.3%">OROFARINGEA</td>
                  <td width="33.3%">FARINGEA</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNE_C85" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNE_C86" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNE_C87" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
               </tr>     
          </table> 
        </td>
    </tr>
      <tr>
  <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
  <tr class="camposRepInp">
     <td width="100%">RESPIRACION</td> 
  </tr> 
  <tr>
      <td> 
          <table width="100%" align="center">                   
               <tr class="estiloImput">
                  <td width="50%">RITMO</td>
                  <td width="50%">FRECUENCIA</td>
                </tr>
                <tr class="camposRepInp"> 
                <td>
                    <select size="1" id="txt_EXNE_C88" style="width:14%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >   
                       <option value=""></option> 
                        <option value="LENTO">LENTO</option> 
                        <option value="NORMAL">NORMAL </option>   
                         <option value="NORMAL">RAPIDO</option>                   
                    </select>
                </td>                
                 <td>
                    <select size="1" id="txt_EXNE_C89" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >   
                       <option value=""></option> 
                        <option value="RESPIRATORIA">RESPIRATORIA</option> 
                         <option value="RESP/MIN ">RESP/MIN</option>                   
                    </select>
                 </td>                 
               </tr>     
          </table> 
        </td>
    </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="100%">COMPORTAMIENTO AUDITIVO</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXNE_C90"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
        </table> 
      </td> 
  </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="100%">CONDUCTA HA SEGUIR</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXNE_C91"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
        </table> 
      </td> 
  </tr>
</table>


 
 






<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
  <tr class="camposRepInp" >
     <td width="100%">AYUDAS ORTESICAS</td> 
  </tr>
   <tr>
    <td>
      <table width="100%" align="center">                   
        <tr class="estiloImput">
          <td width="33.3%">USA AYUDAS ORTESICAS?</td>
          <td width="33.3%">DE QUE TIPO?</td>
          <td width="33.3%">AYUDAS DIAGNOSTICAS</td>
        </tr>   
         <tr class="estiloImput">
             <td align="center">
                    <select size="1" id="txt_EXMO_C1" style="width:10%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select>
             </td>            
            <td>
             <textarea type="text" id="txt_EXMO_C2"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
              <td>
               <textarea type="text" id="txt_EXMO_C3"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>      
              </td>
        </tr>  

      </table> 
    </td>
  </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>  
  <tr>
      <td>
        <table width="50%" align="center">                   
          <tr>
            <td width="15%" class="camposRepInp">ACTIVIDAD MONORA</td>
            <td width="35%" class="estiloImput"></td>
          </tr>  
          <tr class="estiloImput">
            <td align="right">CONTROL CEFALICO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXMO_C4" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="ADECUADA">ADECUADA</option> 
                        <option value="INADECUADA">INADECUADA</option>           
                        <option value="NO APLICA">NO APLICA</option>           
                    </select>
               </td>            
          </tr>  
           <tr class="estiloImput">
            <td align="right">CONTROL TRONCULAR:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                 <select size="1" id="txt_EXMO_C5" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="ADECUADA">ADECUADA</option> 
                        <option value="INADECUADA">INADECUADA</option>           
                        <option value="NO APLICA">NO APLICA</option>           
                    </select>
               </td>            
          </tr> 
          <tr class="estiloImput">
            <td align="right">SEDENTE:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                     <select size="1" id="txt_EXMO_C6" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="ADECUADA">ADECUADA</option> 
                        <option value="INADECUADA">INADECUADA</option>           
                        <option value="NO APLICA">NO APLICA</option>           
                    </select>
               </td>            
          </tr>
          <tr class="estiloImput">
            <td align="right">ARRASTRE:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                       <select size="1" id="txt_EXMO_C7" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="ADECUADA">ADECUADA</option> 
                        <option value="INADECUADA">INADECUADA</option>           
                        <option value="NO APLICA">NO APLICA</option>           
                    </select>
               </td>            
          </tr>
           <tr class="estiloImput">
            <td align="right">GATEO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_EXMO_C8" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="ADECUADA">ADECUADA</option> 
                        <option value="INADECUADA">INADECUADA</option>           
                        <option value="NO APLICA">NO APLICA</option>           
                    </select>
               </td>            
          </tr> 
          <tr class="estiloImput">
            <td align="right">BIPEDESTACION:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXMO_C9" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="ADECUADA">ADECUADA</option> 
                        <option value="INADECUADA">INADECUADA</option>           
                        <option value="NO APLICA">NO APLICA</option>           
                    </select>
               </td>            
          </tr> 
           <tr class="estiloImput">
            <td align="right">MARCHA:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                 <select size="1" id="txt_EXMO_C10" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="ADECUADA">ADECUADA</option> 
                        <option value="INADECUADA">INADECUADA</option>           
                        <option value="NO APLICA">NO APLICA</option>           
                    </select>
               </td>            
          </tr>  
       </table>   
  </tr>      
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">RETRACCIONES MUSCULARES</td>
            <td width="50%">USA COMPENSACIONES DURANTE EL MOVIMIENTO?</td>

          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXMO_C11"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>   
            <td align="center">
                     <select size="1" id="txt_EXMO_C12" style="width:9%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select><br><br>
                  CUALES?  
             <textarea type="text" id="txt_EXMO_C13"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>           
        </table> 
      </td> 
  </tr>
</table>


 
 






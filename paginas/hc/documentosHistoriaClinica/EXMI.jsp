<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr class="camposRepInp">
     <td width="100%">CARACTERISTICAS DE LA LESION</td> 
  </tr>
  <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="titulos1">
                  <td width="20%">FECHA DE INICIO DEL DOLOR O LESION</td>
                  <td width="20%">TIPO DE LESION</td>
                  <td width="20%">ATENCION RECIBIDA</td>
                  <td width="20%">DOLOR</td>
                  <td width="20%">HACE CUANTO TIEMPO</td>
                </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXMI_C1" maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                    <textarea type="text" id="txt_EXMI_C2"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
                  <td>
                     <textarea type="text" id="txt_EXMI_C3"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>   
                  </td>                
                    <td>
                     <select size="1" id="txt_EXMI_C4" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>    
                  </td>
                    <td>
              <textarea type="text" id="txt_EXMI_C5"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>    
                  </td>                 
              </tr> 
          </table> 
        </td>
    </tr>   
     <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="titulos1">
                  <td width="20%">DONDE DUELE</td>
                  <td width="20%">COMO DUELE</td>
                  <td width="20%">INTENSIDAD (DE 1 A 10)</td>
                  <td width="20%">FACTOR DESENCADENANTE</td>
                  <td width="20%">POSTURA ANTALGICA</td>
                </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                  <textarea type="text" id="txt_EXMI_C6"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
                  </td>
                  <td>
                    <textarea type="text" id="txt_EXMI_C7"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
                  <td>
                     <textarea type="text" id="txt_EXMI_C8"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>   
                  </td>                
                   <td>
                  <textarea type="text" id="txt_EXMI_C9"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
                  <td>
                     <select size="1" id="txt_EXMI_C10" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>    
                  </td>                
              </tr> 
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="titulos1">
                  <td width="50%">CARACTERISTICAS</td>
                  <td width="50%">DOMINIO</td>
               </tr>
                <tr class="camposRepInp"> 
                  <td>
                     <textarea type="text" id="txt_EXMI_C11"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
                  <td>
                      DERECHO:
                      <input type="text" id="txt_EXMI_C12" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                      <br><br>
                      IZQUIERDO:
                      <input type="text" id="txt_EXMI_C13" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>  
   <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr class="camposRepInp">
     <td width="100%">EXAMEN ARTICULAR</td> 
  </tr> 
  <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="camposRepInp">
                  <td width="50%" colspan="3">CADERA</td>
                  <td width="50%" colspan="3">RODILLA</td>
               </tr>
               <tr class="camposRepInp">
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                </tr>                 
            <tr class="estiloImput"> 
                  <td>
                     FLEXION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMI_C14" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMI_C15" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      FLEXION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMI_C16" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMI_C17" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>
               <tr class="estiloImput"> 
                  <td>
                     EXTENSION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMI_C18" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMI_C19" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      EXTENSION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMI_C20" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMI_C21" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>  
                   <tr class="estiloImput"> 
                  <td>
                     ABDUCCION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMI_C22" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMI_C23" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>

                  </td>
                    <td>
                  </td> 
                    <td>
                  </td>                 
               </tr> 
                       <tr class="estiloImput"> 
                  <td>
                     ADUCCION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMI_C24" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMI_C25" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>

                  </td>
                    <td>
                  </td> 
                    <td>
                  </td>                 
               </tr> 
                   </tr>  
                   <tr class="estiloImput"> 
                  <td>
                     ROTACION EXTERNA
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMI_C26" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMI_C27" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                  </td>
                    <td>
                  </td> 
                    <td>
                  </td>                 
               </tr> 
                     </tr>  
                   <tr class="estiloImput"> 
                  <td>
                    ROTACION INTERNA
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMI_C28" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMI_C29" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                  </td>
                    <td>
                  </td> 
                    <td>
                  </td>                 
               </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="camposRepInp">
                  <td width="50%" colspan="3">TOBILLO</td>
                  <td width="50%" colspan="3">DEDOS</td>
               </tr>
               <tr class="camposRepInp">
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                </tr>                 
            <tr class="estiloImput"> 
                  <td>
                     FLEXION PLANTAR
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMI_C30" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMI_C31" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      FLEXION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMI_C32" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMI_C33" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>
               <tr class="estiloImput"> 
                  <td>
                     DORSIFLEXION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMI_C34" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMI_C35" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      EXTENSION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMI_C36" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMI_C37" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>  
                   <tr class="estiloImput"> 
                  <td>
                EVERSION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMI_C38" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMI_C39" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                  </td>
                    <td>
                  </td> 
                    <td>
                  </td>                 
               </tr> 
                   </tr>  
                   <tr class="estiloImput"> 
                  <td>
                  INVERSION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMI_C40" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMI_C41" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                  </td>
                    <td>
                  </td> 
                    <td>
                  </td>                 
               </tr>                  
          </table> 
        </td>
    </tr>  
    <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">PRESENTA LESION NERVIOSA?  </td>
            <td width="50%">DEFORMIDAD ARTICULAR</td>
          </tr>   
          <tr class="estiloImput"> 
            <td align="center">
                     <select size="1" id="txt_EXMI_C42" style="width:7%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select><br><br>
              TIPO DE LESION
             <textarea type="text" id="txt_EXMI_C43"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>      
            <td align="center">
                     <select size="1" id="txt_EXMI_C44" style="width:7%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select><br><br>
            CARACTERISTICAS
             <textarea type="text" id="txt_EXMI_C45"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>                      
        </table> 
      </td> 
  </tr>
    <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
   <tr class="camposRepInp" >
     <td width="100%">SENSIBILIDAD</td> 
  </tr>
  <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="titulos1">
                  <td width="25%">NORMAL</td>
                  <td width="25%">HIPOESTESIA</td>
                  <td width="25%">HIPERESTESIA</td>
                  <td width="25%">ANESTESIA</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXMI_C46" maxlength="8" style="width:20%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMI_C47" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMI_C48" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXMI_C49" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr> 
      <tr>
      <td>
        <table width="100%" align="center">   
         <tr class="titulos1">
            <td width="50%">AREA</td>
            <td width="50%">FUERZA MUSCULAR</td>
         </tr>                 
         <tr class="camposRepInp"> 
              <td>
             <textarea type="text" id="txt_EXMI_C50"    rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
             <td>
             <textarea type="text" id="txt_EXMI_C51"    rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>  
          </tr>            
        </table> 
      </td> 
  </tr>  
      <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
   <tr class="camposRepInp" >
     <td width="100%">PALPACION</td> 
  </tr>
  <tr>
      <td>
        <table width="100%" align="center">   
         <tr class="titulos1">
            <td width="33.3%">TEJIDOS BLANDOS</td>
            <td width="33.3%">TEJIDOS OSEOS</td>
            <td width="33.3%" rowspan="2">AYUDAS DIAGNOSTICAS</td>
         </tr>                 
          <tr class="titulos1">
            <td width="33.3%">HALLAZGOS</td>
            <td width="33.3%">HALLAZGOS</td>
         </tr>    
      <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXMI_C52"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
            <td>
             <textarea type="text" id="txt_EXMI_C53"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>  
            <td>
             <textarea type="text" id="txt_EXMI_C54"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>    
          </tr>            
        </table> 
      </td> 
  </tr> 
</table>


 
 






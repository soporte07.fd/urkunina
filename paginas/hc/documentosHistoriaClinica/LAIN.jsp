
<table width="100%">
            <tr> 
              <td class="titulos1" width="35%"  align="right">EXAMEN</td>  
			  <td class="titulos1" width="25%"  align="right">RESULTADOS</td>	
			  <td class="titulos1" width="25%"  align="right">VALOR DE REFERENCIA</td>  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CUALIFICACION DE HORMONA GONADOTROPINA CORIONICA HCG:&nbsp;&nbsp;</td>  
			  <td align="center">
                 <textarea type="text" id="txt_LAIN_C1"  rows="2" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
			  </td>			  
              <td align="left"><label>SENSIBILIDAD DE >99.9%</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">SEROLOGIA VDRL:&nbsp;&nbsp;</td>  
			<td align="center">
                 <textarea type="text" id="txt_LAIN_C2"  rows="2" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
			  </td>
             <td align="left"><label>SD BIOLINE HIV-1/2 100%</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">FACTOR REUMATOIDEO  (FR) -ANALISIS SEMICUANTITATIVO:&nbsp;&nbsp;</td>  
			 <td align="center">
                 <textarea type="text" id="txt_LAIN_C3"  rows="2" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
			  </td>
              <td align="left"><label></label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">PROTEINA C REACTIVA (PCR) -ANALISIS SEMICUANTITATIVO:&nbsp;&nbsp;</td>  
			 <td align="center">
                 <textarea type="text" id="txt_LAIN_C4"  rows="2" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
			  </td>
              <td align="left"><label></label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">ANTIESTREPTOLISINA "O" - (ASO):&nbsp;&nbsp;</td>  
			<td align="center">
                 <textarea type="text" id="txt_LAIN_C5"  rows="2" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
			 </td>
              <td align="left"><label></label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">VIRUS DE INMUNODEFICIENCIA HUMANA (VIH -1/2) PRUEBA  CUALITATIVA:&nbsp;&nbsp;</td>  
			<td align="center">
                 <textarea type="text" id="txt_LAIN_C6"  rows="2" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
			  </td>
              <td align="left"><label>SD BIOLINE HIV-1/2 100%</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">ANTIGENO DE SUPERFICIE HEPATITIS B (HBsAG):&nbsp;&nbsp;</td>  
			<td align="center">
                 <textarea type="text" id="txt_LAIN_C7"  rows="2" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
			 </td>
              <td align="left"><label>SENSIBILIDAD RELATIVA 100%</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">SIFILIS- PRUEBA TREPONEMICA (TPHA) - PRUEBA CUALITATIVA:&nbsp;&nbsp;</td>  
			<td align="center">
                 <textarea type="text" id="txt_LAIN_C8"  rows="2" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
			  </td>	
              <td align="left"><label>LA SENSIBILIDAD ES > A 96 %</label></td>  
			</tr>		
			
</table>  
 
 
<table width="100%"  align="center">
	<tr>
	  <td width="100%" >
	  <table width="100%" align="center">
			   <tr class="estiloImput"> 
					<td width="37%" colspan="2" class="titulosTodasMinuscu">OBSERVACION</td>                        
			   </tr>     
			   <tr class="estiloImput"> 
					<td  colspan="2">         
						<textarea type="text" id="txt_LAIN_C9"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
					</td>  
			   </tr> 
	  
			   
		  </table> 
	  </td>
	</tr>   
</table>  



 
<table width="100%"  align="center">
 <tr class="estiloImput"> 
              <td colspan="1" align="right">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:40%" title="76" tabindex="14" >
                      <option value=""></option>         
                      <option value="7">Cancelado Finalizado</option>
                      <option value="10">Reprogramado Finalizado</option>                                                                                       
                      <option value="2">Tomado</option>
					  <option value="11">Para direccion medica</option>                         
                      <option value="3">Enviado</option>      
                      <option value="5">Enviado urgente</option>                                                
                    <!--  <option value="4">Interpretado</option>  -->
                    <!--  <option value="6">Interpretado Urgente</option>  -->
                      <option value="8">Finalizado Urgente</option>                                                              
                  </select>              
              </td>                   
              <td colspan="1" align="left">
              Motivo:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"  tabindex="14" >	
                          <option value="1">NINGUNA</option>                                                                                   
                          <option value="3">ATRIBUIBLE AL PACIENTE</option>  
 						  <option value="4">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>                                         
              <td colspan="1" align="left">
	           <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO FOLIO" onclick="cambioEstadoFolio()">                                    
			  </td> 
           </tr> 
</table> 
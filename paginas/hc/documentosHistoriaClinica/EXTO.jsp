<table width="100%"  cellpadding="0" cellspacing="0"  align="left">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="40%" align="left">
          <tr class="camposRepInp">
            <td colspan="2">TONO MUSCULAR</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="20%">SEGMENTO CORPORAL</td>
            <td width="20%"></td>
          </tr>  
          <tr class="estiloImput">
            <td align="right">CUELLO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXTO_C1" style="width:34%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr>  
           <tr class="estiloImput">
            <td align="right">TRONCO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXTO_C2" style="width:34%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr> 
          <tr class="estiloImput">
            <td align="right">MIEMBRO SUPERIOR DERECHO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXTO_C3" style="width:34%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr>
          <tr class="estiloImput">
            <td align="right">MIEMBRO SUPERIOR IZQUIERDO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_EXTO_C4" style="width:34%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr>
           <tr class="estiloImput">
            <td align="right">MIEMBRO INFERIOR DERECHO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_EXTO_C5" style="width:34%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr> 
          <tr class="estiloImput">
            <td align="right">MIEMBRO INFERIOR IZQUIERDO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXTO_C6" style="width:34%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr> 
           <tr class="estiloImput">
            <td align="right">FUERZA MUSCULAR:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                  <select size="1" id="txt_EXTO_C7" style="width:34%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr> 
       </table>  
      </td>             
  </tr>
</table>


<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">PATRONES FUNCIONALES DE MOVIMIENTO</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="13.5%">PATRONES</td>
            <td width="11.5%">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
            <td width="13.5%">PATRONES</td>
            <td width="11.5">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
          </tr>
          <tr class="estiloImput">
                  <td align="right">
                   MANO CABEZA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C8" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C9" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C10" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  AGARRE A MANO LLENA:
                 </td>
                  <td>
                      <input type="text" id="txt_EXTO_C11" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C12" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C13" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
            <tr class="estiloImput">
                  <td align="right">
                    MANO BOCA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C14" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C15" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C16" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  AGARRE CILINDRICO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C17" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C18" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C19" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                  MANO CUELLO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C20" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C21" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C22" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  ENGANCHE:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C23" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C24" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C25" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right">
                    MANO HOMBRO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C26" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C27" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C28" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    PINZA FINA:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C29" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C30" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C31" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      MANO ESPALDA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C32" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C33" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C34" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    PINZA TRIPODE:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C35" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C36" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C37" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                      MANO PIERNA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C38" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C39" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C40" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    PINZA LATERAL:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C41" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C42" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C43" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                      MANO PIE:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C44" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C45" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C46" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    OPOSICION:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C47" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C48" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C49" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr>
                  <td class="titulos1" colspan="4"> 
                  PATRONES INTEGRALES DE MOVIMIENTO
                  </td>              
                  <td style="text-align: right;" class="estiloImput">
                   ATRAPAR:
                 </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C50" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C51" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C52" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                 <tr class="estiloImput">
                  <td align="right"> 
                      ALCANCE PLANO SUPERIOR:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C53" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C54" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C55" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    SOLTAR VOLUNTARIO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C56" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C57" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C58" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                      ALCANCE PLANO INFERIOR:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C59" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C60" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C61" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    SOLTAR INVOLUNTARIO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C62" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C63" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C64" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      ALCANCE PLANO LATERAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C65" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C66" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C67" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    PATEAR:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C68" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C69" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C70" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
              <tr class="estiloImput">
                  <td align="right"> 
                      ALCANCE PLANO DIAGONAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C71" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C72" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C73" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    LANZAR:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C74" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C75" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C76" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
        </table>   
       </td>        
  </tr>

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">SISTEMA SENSORIAL</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="16.5%">SENSIBILIDAD SUPERFICIAL</td>
            <td width="11.5%">FUNCIONAL (F)</td>
            <td width="11.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
            <td width="16.5%">AUDITIVO</td>
            <td width="11.5">FUNCIONAL (F)</td>
            <td width="11.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
          </tr>
          <tr class="estiloImput">
                  <td align="right">
                   CALOR:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C77" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C78" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C79" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  AGIDENTIFICACION DEL SONIDO:
                 </td>
                  <td>
                      <input type="text" id="txt_EXTO_C80" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C81" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C82" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
            <tr class="estiloImput">
                  <td align="right">
                    FRIO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C83" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C84" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C85" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  DISCRIMINACION DEL SONIDO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C86" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C87" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C88" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                 TOQUE LIGERO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C89" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C90" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C91" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  LOCALIZACION DEL SONIDO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C92" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C93" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C94" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr>
                <td style="text-align: right;" class="estiloImput">
                   ROMO O AGUDO:
                 </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C95" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C96" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C97" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td colspan="4" class="titulos1">
                    OLFATIVO
                  </td>                 
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                     DOLOR AL TACTO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C98" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C99" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C100" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    AGRADABLE:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C101" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C102" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C103" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr>
                  <td class="titulos1" colspan="4"> 
                      SENSIBILIDAD PROFUNDA
                  </td> 
                  <td style="text-align: right;" class="estiloImput">
                    DESAGRADABLE:
                 </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C104" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C105" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C106" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr>
               <td style="text-align: right;" class="estiloImput">
                      BAROGNOSIA:
                 </td>
                  <td  class="estiloImput">
                      <input type="text" id="txt_EXTO_C107" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td  class="estiloImput">
                      <input type="text" id="txt_EXTO_C108" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td  class="estiloImput">
                      <input type="text" id="txt_EXTO_C109" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td class="titulos1" colspan="4">
                    GUSTATIVO
                  </td>                  
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                     ESTEROGNOSIA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C110" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C111" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C112" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    DULCE:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C113" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C114" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C115" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                      GRAFESTESIA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C116" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C117" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C118" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    SALADO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C119" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C120" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C121" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      DIFERENCIACION DE TEXTURAS:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C122" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C123" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C124" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    ACIDO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C125" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C126" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C127" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
          </table>   
       </td>        
  </tr>
</table>




<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">PERCEPCION VISUAL</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="16.5%">ITEM</td>
            <td width="11.5%">FUNCIONAL (F)</td>
            <td width="11.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
            <td width="50%" colspan="4">FIJACION VISUAL</td>
          </tr>
          <tr>
              <td style="text-align: right;" class="estiloImput">
                   SEGUIMIENTO VISUAL:
                 </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C128" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C129" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C130" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                <td class="camposRepInp">ITEM</td>
                <td class="camposRepInp">COLOR</td>
                <td class="camposRepInp">FORMA</td>
                <td class="camposRepInp">TAMA&Ntilde;O</td>
            </tr> 
            <tr class="estiloImput">
                  <td align="right">
                    HORIZONTAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C131" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C132" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C133" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  PAREA:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C134" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C135" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C136" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                 VERTICAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C137" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C138" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C139" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  AGRUPAR:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C140" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C141" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C142" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right">
                   DIAGONAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C143" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C144" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C145" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   IDENTIFICA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C146" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C147" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C148" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                     CIRCULAR:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C149" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C150" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C151" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    NOMINA:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C152" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C153" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C154" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
       <tr class="estiloImput">
                  <td align="right"> 
                     ATENCION VISUAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C155" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C156" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C157" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    FIGURA FONDO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C158" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C159" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C160" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                     DISCRIMINACION VISUAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C161" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C162" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C163" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    COPIA DE DISE&Ntilde;O:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C164" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C165" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C166" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>            
          </table>   
       </td>        
  </tr>
</table>

 
 <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">PERCEPCION TEMPORO ESPACIAL</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="12.5%">ITEM</td>
            <td width="12.5%">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
            <td width="13.5%">ITEM</td>
            <td width="11.5">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
          </tr>
          <tr>
                  <td class="camposRepInp" colspan="4">
                      ESPACIO
                  </td>
                  <td style="text-align: right;" class="estiloImput">
                      NOCHE:
                 </td> 
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C167" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C168" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C169" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
            <tr class="estiloImput">
                  <td align="right">
                    ARRIBA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C170" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C171" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C172" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    MA&Ntilde;ANA:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C173" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C174" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C175" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                  ABAJO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C176" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C177" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C178" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  TARDE:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C179" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C180" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C181" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right">
                    ADELANTE:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C182" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C183" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C184" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    HOY:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C185" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C186" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C187" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      ATRAS:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C188" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C189" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C190" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    AYER:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C191" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C192" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C193" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr class="estiloImput">
                  <td align="right"> 
                      AFUERA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C194" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C195" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C196" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    MES:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C197" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C198" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C199" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr class="estiloImput">
                  <td align="right"> 
                      DENTRO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C200" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C201" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C202" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    DIA:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C203" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C204" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C205" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr>
                  <td class="titulos1" colspan="4"> 
                      TIEMPO
                  </td> 
                  <td style="text-align: right;" class="estiloImput">
                    A&Ntilde;O:
                 </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C206" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C207" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_EXTO_C208" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
              <tr class="estiloImput">
                  <td align="right"> 
                    DIA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C209" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C210" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C211" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    FECHA:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C212" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C213" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C214" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
        </table>   
       </td>        
  </tr>


 <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">ESQUEMA CORPORAL</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="12.5%">ITEM</td>
            <td width="12.5%">EN SI MISMO</td>
            <td width="12.5%">EN OTRA PERSONA</td>
            <td width="12.5%">EN RELACION A OBJETOS</td>
            <td width="12.5%">ITEM</td>
            <td width="12.5%">EN SI MISMO</td>
            <td width="12.5%">EN OTRA PERSONA</td>
            <td width="12.5%">EN RELACION A OBJETOS</td>
          </tr>
          <tr class="camposRepInp" >
                  <td colspan="4">
                      LATERALIDAD
                  </td> 
                  <td colspan="4">
                      ESQUEMA CORPORAL:
                  </td>
            </tr> 
            <tr class="estiloImput">
                  <td align="right">
                    MANO DERECHA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C215" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C216" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C217" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    PARTES FINAS:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C218" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C219" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C220" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                  MANO IZQUIERDA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C221" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C222" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C223" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  PARTES GRUESAS:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C224" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C225" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C226" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right">
                    OJO DERECHO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C227" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C228" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C229" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    ARTICULACIONES:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C230" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C231" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C232" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      OJO IZQUIERDO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C233" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C234" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C235" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    DETALLES:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C236" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C237" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C238" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr class="estiloImput">
                  <td align="right"> 
                      PIE DERECHO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C239" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C240" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C241" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    IMAGEN CORPORAL:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C242" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C243" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C244" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr class="estiloImput">
                  <td align="right"> 
                      PIE IZQUIERDO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C245" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C246" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C247" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  
            </tr>               
        </table>   
       </td>        
  </tr>
</table>




 <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">MOTRICIDAD</td>
          </tr>                    
        <tr class="camposRepInp">
            <td width="12.5%">MOTRICIDAD FINA</td>
            <td width="12.5%">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
            <td width="13.5%">MOTRICIDAD GRUESA</td>
            <td width="11.5%">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
          </tr>
            <tr class="estiloImput">
                  <td align="right">
                    PICADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C248" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C249" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C250" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    DESPLAZAMIENTO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C251" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C252" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C253" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                 RASGADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C254" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C255" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C256" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  EQUILIBRIO ESTATICO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C257" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C258" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C259" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right">
                    BORDEADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C260" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C261" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C262" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    EQUILIBRIO DINAMICO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C263" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C264" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C265" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      RECORTADO CON TIJERAS:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C266" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C267" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C268" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    POSICION SEDENTE
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C269" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C270" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C271" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr class="estiloImput">
                  <td align="right"> 
                      ENCAJADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C272" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C273" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C274" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    POSICION BIPEDA:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C275" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C276" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C277" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                <tr class="estiloImput">
                  <td align="right"> 
                      ENSARTADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C278" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C279" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C280" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   POSICION DE RODILLAS:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C281" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C282" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C283" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                           <tr class="estiloImput">
                  <td align="right"> 
                      BORDADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C284" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C285" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C286" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    SALTO UNILATERAL:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C287" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C288" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C289" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                           <tr class="estiloImput">
                  <td align="right"> 
                      CALCADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C290" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C291" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C292" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   SALTO BILATERAL:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C293" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C294" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C295" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
              <tr class="estiloImput">
                  <td align="right"> 
                      DIBUJADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C296" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C297" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C298" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                      CORRER:
                    </td>
                  <td>
                      <input type="text" id="txt_EXTO_C299" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C300" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C301" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
              <tr class="estiloImput">
                  <td align="right"> 
                      COLOREADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C302" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C303" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C304" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   TRANSPORTE DE PESO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C305" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C306" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C307" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>      
            <tr class="estiloImput">
                  <td align="right"> 
                      UTILIZACION DE PINCEL:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C308" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C309" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C310" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   ALCANZAR:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C311" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C312" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C313" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>      
            <tr class="estiloImput">
                  <td align="right"> 
                  </td> 
                  <td>
                  </td>                
                 <td>
                  </td>                 
                  <td>
                  </td> 
                  <td align="right">
                    EMPUJAR:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C314" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C315" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C316" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
              <tr class="camposRepInp">
                <td colspan="4">PROCESOS COGNITIVOS</td>
                <td colspan="4">ACTIVIDADES DE LA VIDA DIARIA</td>
             </tr>
            <tr class="camposRepInp">
                <td width="12.5%">ITEM</td>
                <td width="12.5%">FUNCIONAL (F)</td>
                <td width="12.5%">SEMIFUNCIONAL (SF)</td>
                <td width="12.5%">NO FUNCIONAL( NF)</td>
                <td width="12.5%">ITEM</td>
                <td width="12.5%">FUNCIONAL (F)</td>
                <td width="12.5%">SEMIFUNCIONAL (SF)</td>
                <td width="12.5%">NO FUNCIONAL( NF)</td>
           </tr>     
             <tr class="estiloImput">
                  <td align="right"> 
                      PERCEPCION:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C317" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C318" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C319" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   VESTIDO:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C320" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C321" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C322" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
             <tr class="estiloImput">
                  <td align="right"> 
                      JUICIO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C323" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C324" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C325" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   ALIMENTACION:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C326" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C327" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C328" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>  
               <tr class="estiloImput">
                  <td align="right"> 
                      RACIOCINIO:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C329" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C330" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C331" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   HIGIENE MAYOR:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C332" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C333" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C334" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
               <tr class="estiloImput">
                  <td align="right"> 
                      ATENCION:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C335" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C336" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C337" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   HIGIENE MENOR:
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C338" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C339" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C340" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>  
               <tr class="estiloImput">
                  <td align="right"> 
                      MEMORIA:
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C341" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C342" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXTO_C343" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                   
               </tr>    
        </table>   
       </td>        
  </tr>
</table>


<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="10">USO DE AYUDAS TECNICAS</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="10">DERECHA</td>
            <td width="11%">UBICACION</td>
            <td width="9%">GRADOS</td>
            <td width="10%">IZQUIERDA</td>
            <td width="10%">N&#186;</td>
            <td width="10%">DERECHA</td>
            <td width="11%">PARTE DEL CUERPO</td>
            <td width="9%">GRADOS</td>
            <td width="10%">IZQUIERDA</td>
            <td width="10%">N&#186;</td>
          </tr>
          <tr class="estiloImput" align="center">
             <td>
                    <select size="1" id="txt_EXTO_C344" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="PASIVO">PASIVO</option> 
                        <option value="ACTIVO">ACTIVO</option>           
                    </select>
               </td>            
                 <td>
                  	 HOMBRO
                  </td>                
                 <td>
                      <input type="text" id="txt_EXTO_C345" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                  	<select size="1" id="txt_EXTO_C346" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="PASIVO">PASIVO</option> 
                        <option value="ACTIVO">ACTIVO</option>           
                    </select>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C347" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                  	 <select size="1" id="txt_EXTO_C348" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="PASIVO">PASIVO</option> 
                        <option value="ACTIVO">ACTIVO</option>           
                    </select>
                  </td>
                  <td>
                  	CUELLO
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C349" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                  	 <select size="1" id="txt_EXTO_C350" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="PASIVO">PASIVO</option> 
                        <option value="ACTIVO">ACTIVO</option>           
                    </select>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C351" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>  
                <tr class="estiloImput" align="center">
             <td>
                    <select size="1" id="txt_EXTO_C352" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="PASIVO">PASIVO</option> 
                        <option value="ACTIVO">ACTIVO</option>           
                    </select>
               </td>            
                 <td>
                  	 FLEXION
                  </td>                
                 <td>
                 	0 - 180
                  </td>                 
                  <td>
                  	<select size="1" id="txt_EXTO_C353" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="PASIVO">PASIVO</option> 
                        <option value="ACTIVO">ACTIVO</option>           
                    </select>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXTO_C354" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                  	 <select size="1" id="txt_EXTO_C355" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="PASIVO">PASIVO</option> 
                        <option value="ACTIVO">ACTIVO</option>           
                    </select>
                  </td>
                  <td>
                  	FLEXION
                  </td>
                  <td>
                  	20
                  </td>
                  <td>
                  	 <select size="1" id="txt_EXTO_C356" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="PASIVO">PASIVO</option> 
                        <option value="ACTIVO">ACTIVO</option>           
                    </select>
                  </td>
                  <td>
                      <input type="text" id="txt_EXTO_C357" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
            <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C358" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                  	EXTENSION
	                  </td>                
	                 <td>
	                 	0 - 60
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C359" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C360" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C361" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	EXTENSION
	                  </td>
	                  <td>
	                  	35
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C362" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C363" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr> 
             <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C364" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                  	ABDUCCION VERTICAL
	                  </td>                
	                 <td>
	                 	0 - 180
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C365" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C366" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C367" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	INCLINACION
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C368" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C369" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C370" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
            <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C371" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                  	ABDUCCION HORIZONTAL
	                  </td>                
	                 <td>
	                 	0 - 90
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C372" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C373" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C374" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	ROTACION
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C375" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C376" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C377" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
                <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C378" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                  	ABDUCCION HORIZONTAL
	                  </td>                
	                 <td>
	                 	0 - 45
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C379" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C380" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C381" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	CADERA
	                  </td>
	                  <td>
	                     <input type="text" id="txt_EXTO_C382" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C383" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C384" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
            <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C385" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                  	ROTACION INTERNA
	                  </td>                
	                 <td>
	                 	0 - 40
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C386" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C387" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C388" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	FLEXION
	                  </td>
	                  <td>
	                  	90
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C389" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C390" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
            <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C391" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                  	ROTACION EXTERNA
	                  </td>                
	                 <td>
	                 	90 - 0
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C392" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C393" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C394" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	EXTENSION
	                  </td>
	                  <td>
	                  	110
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C395" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C396" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
             <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C397" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 CODO
	                  </td>                
	                 <td>
	                      <input type="text" id="txt_EXTO_C398" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C399" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C400" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C401" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	ABDUCCION
	                  </td>
	                  <td>
	                  	50
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C402" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C403" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
            <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C404" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 FLEXION
	                  </td>                
	                 <td>
	                 	0 - 150
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C405" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C406" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C407" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	ADUCCION
	                  </td>
	                  <td>
	                  	20
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C408" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C409" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
              <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C410" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 EXTENSION
	                  </td>                
	                 <td>
	                 	150 - 0
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C411" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C412" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C413" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	ROTACION INTERNA
	                  </td>
	                  <td>
	                  	30 - 35
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C414" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C415" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
                          <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C416" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 SUPINACION
	                  </td>                
	                 <td>
	                 	0 - 80
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C417" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C418" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C419" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	ROTACION EXTERNA
	                  </td>
	                  <td>
	                  	45
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C420" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C421" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
                          <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C422" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 PRONACION
	                  </td>                
	                 <td>
	                 	0 - 80
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C423" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C424" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C425" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	RODILLA
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C426" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C427" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C428" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
                          <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C429" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 MU&Ntilde;ECA
	                  </td>                
	                 <td>
	                      <input type="text" id="txt_EXTO_C430" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C431" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C432" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C433" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                  	FLEXION
	                  </td>
	                  <td>
	                  	90
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C434" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C435" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
                          <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C436" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 FLEXION
	                  </td>                
	                 <td>
	                 	0 - 80
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C437" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C438" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C439" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
							EXTENSION
	                  </td>
	                  <td>
	                  	0
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C440" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C441" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>

                <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C442" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 EXTENSION
	                  </td>                
	                 <td>
	                 	0 - 40
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C443" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C444" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C445" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
						TOBILLO
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C446" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C447" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C448" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
       			 <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C449" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 DESVIACION RADIAL
	                  </td>                
	                 <td>
	                 	0 - 30
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C450" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C451" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C452" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
						DORSIFLEXION
	                  </td>
	                  <td>
	                  	20
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C453" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C454" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
            <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C455" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 DESVIACION CUBICAL
	                  </td>                
	                 <td>
	                 	0 - 20
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C456" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C457" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C458" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
						PLANTIFLEXION
	                  </td>
	                  <td>
	                  	45 - 55
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C459" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C460" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
            <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C461" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 DEDOS
	                  </td>                
	                 <td>
	                      <input type="text" id="txt_EXTO_C462" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C463" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C464" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C465" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
						EVERSION
	                  </td>
	                  <td>
	                  	15
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C466" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C467" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
            <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C468" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 FLEXION
	                  </td>                
	                 <td>
	                 	50 - 60
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C469" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C470" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C471" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
						INVERSION
	                  </td>
	                  <td>
	                  	35
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C472" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C473" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
            <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C474" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 EXTENSION
	                  </td>                
	                 <td>
	                    30
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C475" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C476" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C477" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
						CUELLO
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C478" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C479" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C480" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
                        <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C481" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 FLEXION IF
	                  </td>                
	                 <td>
	                    80
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C482" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C483" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C484" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
						FLEXION
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C485" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C486" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C487" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
                        <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C488" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 EXTENSION IF
	                  </td>                
	                 <td>
	                    30
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C489" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C490" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C491" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
						EXTENSION
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C492" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C493" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C494" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
                        <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C495" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 ABDUCCION HORIZONTAL
	                  </td>                
	                 <td>
	                    60 - 90
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C496" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C497" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C498" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
						INCLINACION
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C499" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C500" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C501" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
                <tr class="estiloImput" align="center">
	                <td>
	                    <select size="1" id="txt_EXTO_C502" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                </td>            
	                 <td>
	                 ADUCCION HORIZONTAL
	                  </td>                
	                 <td>
	                    60 - 90
	                 </td>                 
	                  <td>
	                  	<select size="1" id="txt_EXTO_C503" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td> 
	                  <td>
	                      <input type="text" id="txt_EXTO_C504" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C505" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
						ROTACION
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C506" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C507" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C508" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
            <tr class="estiloImput" align="center">
	                <td colspan="5"></td>
                        <td>
                 <select size="1" id="txt_EXTO_C509" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                          <option value=""></option> 
                          <option value="PASIVO">PASIVO</option> 
                          <option value="ACTIVO">ACTIVO</option>           
                      </select>
                    </td>           
	                <td>
						LATERAL
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C510" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
	                  <td>
	                  	 <select size="1" id="txt_EXTO_C511" style="width:54%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
	                        <option value=""></option> 
	                        <option value="PASIVO">PASIVO</option> 
	                        <option value="ACTIVO">ACTIVO</option>           
	                    </select>
	                  </td>
	                  <td>
	                      <input type="text" id="txt_EXTO_C512" maxlength="15" style="width:85%" onblur="guardarContenidoDocumento()"/>
	                  </td>
            </tr>
        	</table>
        </td>               
  </tr>
</table>


 <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
<tr class="camposRepInp">
     <td width="100%">USO DE AYUDAS TECNICAS:</td> 
  </tr>  
  <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="50%">DESEMPE&Ntilde;O ESCOLAR</td>
                  <td width="50%">ACTIVIDADES PREVOCACIONALES</td>
            </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                      <textarea type="text" id="txt_EXTO_C513"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
                  <td>
                    <textarea type="text" id="txt_EXTO_C514"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
          </tr> 
          </table> 
        </td>
    </tr>
      <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="50%">EXPERIENCIA LABORAL</td>
                  <td width="50%">ACTIVIDADES DE OCIO Y TIEMPO LIBRE</td>
            </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                      <textarea type="text" id="txt_EXTO_C515"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
                  <td>
                    <textarea type="text" id="txt_EXTO_C516"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
          </tr> 
          </table> 
        </td>
    </tr>

</table>


 
 






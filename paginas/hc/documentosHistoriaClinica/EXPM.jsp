<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="50%">DESCRIPCION DEL PACIENTE - EXAMEN MENTAL </td> 
     <td width="50%">ESTRUCTURA FAMILIAR</td> 
  </tr>
  <tr class="camposRepInp">               
    <td>
         <textarea type="text" id="txt_EXPM_C1"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
    </td>  
    <td>
         <textarea type="text" id="txt_EXPM_C2"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
    </td>                   
  </tr>                
      <tr class="camposRepInp" >
     <td width="50%">FACTORES DESENCADENANTES (SITUACIONES DE CONFLICTO-AFECTACION FISICA Y /O PSICOLOGICA)</td> 
     <td width="50%">DESCRIPCION DE RELACIONES FAMILIARES</td> 
  </tr>
  <tr class="camposRepInp">               
    <td>
         <textarea type="text" id="txt_EXPM_C3"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
    </td>  
    <td>
         <textarea type="text" id="txt_EXPM_C4"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
    </td> 
   </tr> 
</table>


 
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr>
      <td width="100%" bgcolor="#c0d3c1">.</td>
    </tr>
    <tr class="camposRepInp" >
       <td width="100%">ANALISIS CONDUCTUAL</td> 
    </tr>
      <tr class="camposRepInp" >
       <td width="100%">A QUE TIEMPO</td> 
    </tr>
     <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="20%">ENDURECIMIENTO DE LA CABEZA</td>
                  <td width="20%">BALBUCEO </td>
                  <td width="20%">DIJO SU PRIMERA PALABRA</td>
                  <td width="20%">DIJO PALABRAS JUNTAS</td>                
                  <td width="20%">GATEO</td>                
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                     <input type="text" id="txt_EXPM_C5" maxlength="30" style="width:73%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXPM_C6" maxlength="30" style="width:73%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXPM_C7" maxlength="30" style="width:73%" onblur="guardarContenidoDocumento()"/>
                  </td>
                    <td>
                      <input type="text" id="txt_EXPM_C8" maxlength="30" style="width:73%" onblur="guardarContenidoDocumento()"/>
                  </td>
                    <td>
                      <input type="text" id="txt_EXPM_C9" maxlength="30" style="width:73%" onblur="guardarContenidoDocumento()"/>
                  </td>                
               </tr>     
          </table> 
        </td>
    </tr>
         <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="20%">CONTROLO ESFINTERES</td>
                  <td width="20%">CAMINO SOLO</td>
                  <td width="20%">COMIO SOLO</td>
                  <td width="20%">SE VISTIO SOLO</td>                
                  <td width="20%">SE BA&Ntilde;O SOLO </td>                
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                     <input type="text" id="txt_EXPM_C10" maxlength="30" style="width:73%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXPM_C11" maxlength="30" style="width:73%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXPM_C12" maxlength="30" style="width:73%" onblur="guardarContenidoDocumento()"/>
                  </td>
                    <td>
                      <input type="text" id="txt_EXPM_C13" maxlength="30" style="width:73%" onblur="guardarContenidoDocumento()"/>
                  </td>
                    <td>
                      <input type="text" id="txt_EXPM_C14" maxlength="30" style="width:73%" onblur="guardarContenidoDocumento()"/>
                  </td>                
               </tr>     
          </table> 
        </td>
    </tr>
</table>

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
   <tr>
      <td width="100%" bgcolor="#c0d3c1" colspan="3">.</td>
    </tr>
      <tr class="camposRepInp" >
         <td width="33.3%">DESCRIPCION DEL PACIENTE - EXAMEN MENTAL </td> 
         <td width="33.3%">ESTRUCTURA FAMILIAR</td> 
         <td width="33.3%">DESCRIPCION DE RELACIONES FAMILIARES</td> 
      </tr>
      <tr class="camposRepInp">               
        <td>
             <textarea type="text" id="txt_EXPM_C15"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
        </td>  
        <td>
             <textarea type="text" id="txt_EXPM_C16"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
        </td>
          <td>
             <textarea type="text" id="txt_EXPM_C17"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
        </td>                   
      </tr>
</table> 


 
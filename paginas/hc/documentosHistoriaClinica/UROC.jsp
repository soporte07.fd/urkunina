
<table width="100%">
            <tr> 
              <td class="titulos1" width="25%"  align="right">ANALISIS</td>  
			  <td class="titulos1" width="25%"  align="right">RESULTADOS</td>			  
              <td></td>  
			</tr>
			<tr class="camposRepInp">
				<td align="right" colspan"2">ESTUDIO URINARIO</td>
				<td></td>
				<td></td>		
			</tr>
			<tr class="estiloImput"> 
              <td align="right">COLOR:&nbsp;&nbsp;</td>  
			  <td align="left">
				<select size="1" id="txt_UROC_C1" style="width:35;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="Amarilla">Amarilla</option>
                    <option value="Ambar">Ambar</option>
                    <option value="Roja">Roja</option>
                 </select>
			  </td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">ASPECTO:&nbsp;&nbsp;</td>  
			  <td align="left">
				<select size="1" id="txt_UROC_C2" style="width:48%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="Limpida">Limpida</option>
                    <option value="Ligeramente turbia">Ligeramente turbia</option>
                    <option value="Turbia">Turbia</option>
                 </select> 			  
			  </td>			  
              <td></td>  
			</tr>	
			<tr class="camposRepInp">
				<td align="right" colspan"2">ANALISIS FISICO-QUIMICO</td>
				<td></td>
				<td></td>		
			</tr>
			<tr class="estiloImput"> 
              <td align="right">DENSIDAD:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C3" size="100"  maxlength="20" style="width:45%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>		
			<tr class="estiloImput"> 
              <td align="right">PH:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C4" size="100"  maxlength="20" style="width:45%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>	
			<tr class="estiloImput"> 
              <td align="right">PROTEINAS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C5" size="100"  maxlength="20" style="width:45%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td></td>  
			</tr>	
			<tr class="estiloImput"> 
              <td align="right">GLUCOSA:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C6" size="100"  maxlength="20" style="width:45%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CUERPOS CETONICOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C7" size="100"  maxlength="20" style="width:45%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
		      <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">BILIRRUBINAS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C8" size="100"  maxlength="20" style="width:45%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
		      <td></td>  
			</tr>				
            <tr class="estiloImput"> 
              <td align="right">SANGRE:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C9" size="100"  maxlength="20" style="width:45%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">NITRITOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C10" size="100"  maxlength="20" style="width:45%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">UROBILINOGENO:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C11" size="100"  maxlength="20" style="width:45%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td></td>  
			</tr>			
			<tr class="estiloImput"> 
              <td align="right">LEUCOCITOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C12" size="100"  maxlength="20" style="width:45%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr> 	
			<tr class="camposRepInp">
				<td align="right" colspan"2">ANALISIS MICROSCOPICO</td>
				<td></td>
				<td></td>		
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CELULAS EPITELIALES:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C13" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CELULAS ALTAS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C14" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">LEUCOCITOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C15" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>		
			<tr class="estiloImput"> 
              <td align="right">ERITROCITOS EUMORFOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C16" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">ERITROCITOS DISMORFOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C17" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>	
			<tr class="estiloImput"> 
              <td align="right">BACTERIAS:&nbsp;&nbsp;</td>  
			  <td align="left">
				<select size="1" id="txt_UROC_C18" style="width:25%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
					<option value=""></option>
					<option value="Escasas">Escasas</option>
                    <option value="+">+</option>
                    <option value="++">++</option>
                    <option value="+++">+++</option>
                 </select> 			  
			  </td>			  
              <td></td>  
			</tr>	
			<tr class="estiloImput"> 
              <td align="right">MOCO:&nbsp;&nbsp;</td>  
			  <td align="left">
				<select size="1" id="txt_UROC_C19" style="width:25%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
					<option value="Ausencia">Ausencia</option>
                    <option value="+">+</option>
                    <option value="++">++</option>
                    <option value="+++">+++</option>
                 </select> 			  
			  </td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CILINDROS GRANULOSOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C20" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CILINDROS LEUCOCITARIOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C21" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CILINDROS ERITROCITARIOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C22" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CILINDROS HIALINOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C23" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CRISTALES URATOS AMORFOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C24" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CRISTALES FOSFATOS TRIPLES:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C25" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CRISTALES OXALATO DE CALCIO:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C26" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CRISTALES FOSFATOS AMORFOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C27" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CRISTALES DE ACIDO URICO:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C28" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">HIFAS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C29" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">LEVADURAS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C30" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">TRICHOMONAS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C31" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">ESPERMATOZOIDES:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C32" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">ACUMULO DE LUCOCITOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_UROC_C33" size="100"  maxlength="45" style="width:100%" onblur="guardarContenidoDocumento()"/></td>			  
              <td></td>  
			</tr>
			
</table>  
 
 
  

<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%" colspan="2" class="titulosTodasMinuscu">OBSERVACION</td>                        
           </tr>     
           <tr class="estiloImput"> 
                <td  colspan="2">         
                    <textarea type="text" id="txt_UROC_C34"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>  
           
      </table> 
  </td>
</tr>   
</table>  

<table width="100%"  align="center">
 <tr class="estiloImput"> 
              <td colspan="1" align="right">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:40%" title="76" tabindex="14" >
                      <option value=""></option>         
                      <option value="7">Cancelado Finalizado</option>
                      <option value="10">Reprogramado Finalizado</option>                                                                                       
                      <option value="2">Tomado</option>
					  <option value="11">Para direccion medica</option>                         
                      <option value="3">Enviado</option>      
                      <option value="5">Enviado urgente</option>                                                
                    <!--  <option value="4">Interpretado</option>  -->
                    <!--  <option value="6">Interpretado Urgente</option>  -->
                      <option value="8">Finalizado Urgente</option>                                                              
                  </select>              
              </td>                   
              <td colspan="1" align="left">
              Motivo:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"  tabindex="14" >	
                          <option value="1">NINGUNA</option>                                                                                   
                          <option value="3">ATRIBUIBLE AL PACIENTE</option>  
 						  <option value="4">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>                                         
              <td colspan="1" align="left">
	           <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO FOLIO" onclick="cambioEstadoFolio()">                                    
			  </td> 
           </tr> 
</table> 


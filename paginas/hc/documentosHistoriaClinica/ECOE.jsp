<table width="100%"  align="center">
			<tr>
				<td width="100%" align="center" colspan="2">

          <div id="divAcordionTomaExamen" style="display:BLOCK">
            <jsp:include page="../../hc/hc/divsAcordeonHg.jsp" flush="FALSE">
              <jsp:param name="titulo" value="TOMA DEL EXAMEN" />
              <jsp:param name="idDiv" value="divTomaExamen"  />
              <jsp:param name="pagina" value="../documentosHistoriaClinica/ECOE_TOMA.jsp" />
              <jsp:param name="display" value="NONE" />              
            </jsp:include>
          </div>
        
          <div id="divAcordionExamen" style="display:BLOCK">
            <jsp:include page="../../hc/hc/divsAcordeonHg.jsp" flush="FALSE">
              <jsp:param name="titulo" value="CONTENIDO DEL EXAMEN" />
              <jsp:param name="idDiv" value="divExamen" />
              <jsp:param name="pagina" value="../documentosHistoriaClinica/ECOE_EXAMEN.jsp" />
              <jsp:param name="display" value="BLOCK" />              
            </jsp:include>
          </div>


				</td>
			</tr>
</table>      

            
<TABLE width="100%">
  <tr class="estiloImput"> 
    <td width="25%" align="center">ESTADO:
        <select size="1" id="cmbIdEstadoFolioEdit" style="width:60%" title="76" tabindex="14" onfocus="limpiarDivEditarJuan('limpiarMotivoFolioEdit');">
            <option value=""></option>                      
                <option value="2">Tomado</option>
                <option value="3">Enviado</option>                    
                <option value="4">Interpretado</option>                                        
                <option value="5">Enviado urgente</option>   
                <option value="9">Finalizado Alerta</option>    
                <option value="7">Cancelado Finalizado</option>   
                <option value="10">Reprogramado Finalizado</option>
        </select>              
    </td>                   
    <td  width="25%" align="center">
    MOTIVO:
              <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"  tabindex="14" onfocus="limpiaAtributo('cmbMotivoClaseFolioEdit',0)">  
               <option value=""></option> 
                <option value="26">ATRIBUIBLE AL PACIENTE</option>  
                <option value="27">ATRIBUIBLE A LA INSTITUCION</option>  
                <option value="20">ORDEN MEDICA</option>                                                 
              </select>  
    </td>  
    <td width="25%" align="center">
      CLASIFICACION:
          <select id="cmbMotivoClaseFolioEdit" style="width:60%" tabindex="121" onfocus="comboDependienteDosCondiciones('cmbMotivoClaseFolioEdit','cmbIdMotivoEstadoEdit','lblIdDocumento','565')">
          <option value=""></option>                      
      </select> 
    </td>
    <td width="25%" align="center">
     <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO FOLIO" onclick="cambioEstadoFolio()">
     <input type="button" value="FINALIZAR SIN IMPRIMIR" title="BT129" class="small button blue" id="btnFinalizarDocumento_"  onclick="cerrarDocumentSinCambioAutor()">                                          
    </td>               
  </tr>  
  <tr class="estiloImput"> 
    <td colspan="5" align="CENTER">EB DIAGNOSTICO CIE 10 FAVOR COLOCAR EL DIGNOSTICO MAS APROXIMADO AL RESULTADO Y EN OBSERVACIONES FAVOR DESCRIBIR EL RESULTADO DE LA ECOERAFIA.
    </td>                   
  </tr> 
</TABLE>
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
   <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
  <tr class="camposRepInp" >
     <td width="100%">CARACTERISTICAS DE LA LESION</td> 
  </tr>
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr >
            <td class="estiloImput" width="15%">FECHA DE INICIO DE LA LESION:</td>
            <td  align="left">
             <input type="text" id="txt_EXLN_C1" maxlength="10" style="width:10%" onblur="guardarContenidoDocumento()"/>
            </td> 
           </tr> 
        </table> 
      </td> 
  </tr>
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">ATENCION RECIBIDA</td>
            <td width="50%">INSPECCION</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLN_C2"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td> 
             <td>
             <textarea type="text" id="txt_EXLN_C3"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>            
        </table> 
      </td> 
  </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
  <tr class="camposRepInp" >
     <td width="100%">AYUDAS ORTESICAS</td> 
  </tr>
   <tr>
    <td>
      <table width="100%" align="center">                   
        <tr class="estiloImput">
          <td width="33.3%">USA AYUDAS ORTESICAS?</td>
          <td width="33.3%">DE QUE TIPO?</td>
          <td width="33.3%">HACE CUANTO TIEMPO?</td>
        </tr>   
         <tr class="estiloImput">
             <td align="center">
                    <select size="1" id="txt_EXLN_C4" style="width:10%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select>
             </td>            
            <td>
             <textarea type="text" id="txt_EXLN_C5"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
              <td>
               <textarea type="text" id="txt_EXLN_C6"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>      
              </td>
        </tr>  

      </table> 
    </td>
  </tr>
    <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
  <tr class="camposRepInp" >
     <td width="100%">OTRAS ALTERACIONES</td> 
  </tr>
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="33.3%">PRESENTA AFASIAS?</td>
            <td width="33.3%">PRESENTA LIMITACIONES DEL MOVIMIENTO?</td>
            <td width="33.3%">TONO MUSCULAR</td>


          </tr>   
          <tr class="estiloImput"> 
            <td align="center">
                     <select size="1" id="txt_EXLN_C7" style="width:10%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select><br><br>
                DE QUE TIPO?
             <textarea type="text" id="txt_EXLN_C8"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>      
            <td align="center">
                     <select size="1" id="txt_EXLN_C9" style="width:10%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select><br><br>
              DE QUE TIPO?
             <textarea type="text" id="txt_EXLN_C10"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
                 <td align="center">
                     <select size="1" id="txt_EXLN_C11" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="AUMENTADO">AUMENTADO</option>   
                        <option value="DISMINUIDO">DISMINUIDO</option>           
                     </select><br><br>
                EN QUE AREAS?
             <textarea type="text" id="txt_EXLN_C12"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>            
        </table> 
      </td> 
  </tr>
    </tr>
    <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
  <tr class="camposRepInp" >
     <td width="100%">REFLEJOS OSTEOMUSCULARES</td> 
  </tr>
  <tr>
      <td>
        <table width="50%" align="center">                   
          <tr>
            <td width="15%" class="camposRepInp">REFLEJO</td>
            <td width="35%" class="estiloImput"></td>
          </tr>  
          <tr class="estiloImput">
            <td align="right">AQUILIANO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXLN_C13" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="AUMENTADO">AUMENTADO</option>           
                        <option value="DISMINUIDO">DISMINUIDO</option>           
                    </select>
               </td>            
          </tr>  
           <tr class="estiloImput">
            <td align="right">BICIPITAL:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXLN_C14" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="AUMENTADO">AUMENTADO</option>           
                        <option value="DISMINUIDO">DISMINUIDO</option>           
                    </select>
               </td>              
          </tr> 
          <tr class="estiloImput">
            <td align="right">PATELAR:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXLN_C15" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="AUMENTADO">AUMENTADO</option>           
                        <option value="DISMINUIDO">DISMINUIDO</option>           
                    </select>
               </td>             
          </tr>
          <tr class="estiloImput">
            <td align="right">PLANTAR:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXLN_C16" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="AUMENTADO">AUMENTADO</option>           
                        <option value="DISMINUIDO">DISMINUIDO</option>           
                    </select>
               </td>            
          </tr>
           <tr class="estiloImput">
            <td align="right">TRICIPITAL:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXLN_C17" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="AUMENTADO">AUMENTADO</option>           
                        <option value="DISMINUIDO">DISMINUIDO</option>           
                    </select>
               </td>             
          </tr> 
       </table>   
  </tr>   
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
  <tr class="camposRepInp" >
     <td width="100%">PALPACION</td> 
  </tr>
    <tr>
      <td>
        <table width="100%" align="center">   
         <tr class="titulos1">
            <td width="33.3%">TEJIDOS BLANDOS</td>
            <td width="33.3%">TEJIDOS OSEOS</td>
            <td width="33.3%" rowspan="2">AYUDAS DIAGNOSTICAS</td>
         </tr>                 
          <tr class="titulos1">
            <td width="33.3%">HALLAZGOS</td>
            <td width="33.3%">HALLAZGOS</td>
         </tr>    
      <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLN_C18"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
            <td>
             <textarea type="text" id="txt_EXLN_C19"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>  
            <td>
             <textarea type="text" id="txt_EXLN_C20"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>    
          </tr>            
        </table> 
      </td> 
  </tr> 
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
   <tr class="camposRepInp" >
     <td width="100%">SENSIBILIDAD</td> 
  </tr>
  <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="titulos1">
                  <td width="25%">NORMAL</td>
                  <td width="25%">HIPOESTESIA</td>
                  <td width="25%">HIPERESTESIA</td>
                  <td width="25%">ANESTESIA</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXLN_C21" maxlength="8" style="width:20%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXLN_C22" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXLN_C23" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXLN_C24" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr> 
      <tr>
      <td>
        <table width="100%" align="center">   
         <tr class="titulos1">
            <td width="100%">AREA</td>
         </tr>                 
         <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLN_C25"    rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>    
          </tr>            
        </table> 
      </td> 
  </tr>  
</table>


 
 







<table width="100%">
            <tr> 
              <td class="titulos1" width="25%"  align="right">ITEM</td>  
			  <td class="titulos1" width="25%"  align="right">RESULTADOS</td>	
			  <td class="titulos1" width="25%"  align="right">VALOR DE REFERENCIA</td>  
              <td></td>  
			</tr>
			<tr class="camposRepInp"> 
              <td align="right">FORMULA ROJA</td>  
			  <td colspan="2"></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">RECUENTO DE ERITROCITOS:&nbsp;&nbsp;</td>  
			  <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="text" id="txt_LAQH_C1" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>M/ul</td>			  
              <td align="left"><label>4,3 - 5,5 M/ul</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">HEMOGLOBINA:&nbsp;&nbsp;</td>  
			  <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="text" id="txt_LAQH_C2" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>gr/dl</td>			  
              <td align="left"><label>11.8 - 16.0 gr/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">HEMATOCRITO:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQH_C3" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>%</td>			  
              <td align="left"><label>37.0 - 50-0 %</label></td>  
			</tr>
			<tr class="camposRepInp"> 
              <td align="right">FORMULA BLANCA</td>  
			  <td colspan="2"></td>			  
              <td></td>  
			</tr>			
			<tr class="estiloImput"> 
              <td align="right">RECUENTO LEUCOCITOS:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQH_C4" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>U&#955;</td>			  
              <td align="left"><label>4.500 - 11.000/ul</label></td>  
			</tr> 
			<tr class="camposRepInp"> 
              <td align="right">FORMULA LEUCOCITARIA</td>  
			  <td colspan="2"></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">NEUTROFILOS:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQH_C5" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>%</td>			  
              <td align="left"><label>40 - 75 %</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">LINFOCITOS:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQH_C6" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>%</td>			  
              <td align="left"><label>20 - 45%</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">MONOCITOS:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQH_C7" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>%</td>			  
              <td align="left"><label>4 - 9 %</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">EOSINOFILOS:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQH_C8" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>%</td>			  
              <td align="left"><label>0 - 1 %</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">BASOFILOS:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQH_C9" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>%</td>			  
              <td align="left"><label>0 - 2 %</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CAYADOS:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQH_C10" size="100"  maxlength="10" style="width:23%" onblur="guardarContenidoDocumento()"/>%</td>			  
              <td align="left"><label>0 - 5 %</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">PLAQUETAS:&nbsp;&nbsp;</td>  
			  <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="txt_LAQH_C11" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mm/3</td>			  
              <td align="left"><label>150,000 - 450.000 mm/3</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">VELOCIDAD DE SEDIMENTACION GLOBULAR (VSG):&nbsp;&nbsp;</td>  
			  <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="text" id="txt_LAQH_C12" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mm/h</td>			  
              <td align="left"><label>HOMBRES: MENOR DE 50 A&Nacute;OS : 0 - 15 mm/h<br>
					     HOMBRES MAYOR DE 50 A&Nacute;OS: 0 - 20 mm/h<br>
						 MUJERES MENOR DE 50 A&Nacute;OS: 0 - 25 mm/h<br>
						 MUJERES MAYOR DE 50 A&Nacute;OS : 0 - 30 mm/h</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">EXTENDIDO DE SANGRE PERIFERICA (ESP):&nbsp;&nbsp;</td>  
			  <td align="center">
                 <textarea type="text" id="txt_LAQH_C13"  rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
			  </td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">RECUENTO DE RETICULOCITOS:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQH_C14" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>%</td>			  
              <td align="left"><label>NEONATOS: HASTA 2.6 %<br>ADULTOS: HASTA 2.0 %</label></td>  
			</tr>
			<tr class="camposRepInp"> 
              <td align="right">HEMOCLASIFICACION</td>  
			  <td colspan="2"></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">GRUPO SANGUINEO:&nbsp;&nbsp;</td>  
			  <td align="center">
			     <select size="1" id="txt_LAQH_C15" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="O">O</option>
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="AB">AB</option>
                 </select>
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RH:&nbsp;&nbsp;

				 <select size="1" id="txt_LAQH_C16" style="width:25%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
                    <option value=""></option>
                    <option value="POSITIVO">POSITIVO</option>
                    <option value="NEGATIVO">NEGATIVO</option>
                 </select>
			  </td>			  
              <td></td>  
			</tr>		
</table>  
 
 
  

<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%" colspan="2" class="titulosTodasMinuscu">OBSERVACION</td>                        
           </tr>     
           <tr class="estiloImput"> 
                <td  colspan="2">         
                    <textarea type="text" id="txt_LAQH_C17"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr> 
  
           
      </table> 
  </td>
</tr>   
</table>  



 
<table width="100%"  align="center">
 <tr class="estiloImput"> 
              <td colspan="1" align="right">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:40%" title="76" tabindex="14" >
                      <option value=""></option>         
                      <option value="7">Cancelado Finalizado</option>
                      <option value="10">Reprogramado Finalizado</option>                                                                                       
                      <option value="2">Tomado</option>
					  <option value="11">Para direccion medica</option>                         
                      <option value="3">Enviado</option>      
                      <option value="5">Enviado urgente</option>                                                
                    <!--  <option value="4">Interpretado</option>  -->
                    <!--  <option value="6">Interpretado Urgente</option>  -->
                      <option value="8">Finalizado Urgente</option>                                                              
                  </select>              
              </td>                   
              <td colspan="1" align="left">
              Motivo:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"  tabindex="14" >	
                          <option value="1">NINGUNA</option>                                                                                   
                          <option value="3">ATRIBUIBLE AL PACIENTE</option>  
 						  <option value="4">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>                                         
              <td colspan="1" align="left">
	           <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO FOLIO" onclick="cambioEstadoFolio()">                                    
			  </td> 
           </tr> 
</table> 
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="100%">AREA: LENGUAJE</td> 
  </tr>   
  <tr class="camposRepInp" >
     <td width="100%">SUBAREA: LENGUAJE ORAL</td> 
  </tr>                   
  <tr class="estiloImput">
     <td>     
          <p>MEDIANTE CONVERSACION DIRIGIDA  Y JUEGO SE OBSERVARA; SE REALIZARA REGISTRO BASADO EN HITOS DE DESARROLLO DE LENGUAJE SEGUN EDAD.</p>
     </td> 
  </tr> 
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
  <tr class="camposRepInp">
     <td width="100%">NIVEL PRAGMATICO</td> 
  </tr> 
  <tr>
    <td>
      <table width="100%" align="center">                   
        <tr class="estiloImput">
          <td width="50%">INTENCIONALIDAD COMUNICATIVA  MEDIANTE USO DE FUNCIONES</td>
          <td width="50%">ORGANIZACION DE DISCURSO</td>
        </tr>   
        <tr class="camposRepInp"> 
          <td>
           <textarea type="text" id="txt_EXLO_C1"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
          </td>             
          <td>
           <textarea type="text" id="txt_EXLO_C2"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"></textarea>      
          </td>
      </table> 
    </td>
  </tr>
   <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr class="camposRepInp" >
     <td width="100%">NIVEL SEMANTICO</td> 
  </tr>                   
  <tr class="estiloImput">
     <td>     
          <p>SE EVALUARA POR MEDIO DE LA COMPRENSION LECTORA, VOCABULARIO POR CATEGORIAS.</p>
     </td> 
  </tr>  
  <tr>
    	<td> 
        	<table width="100%" align="center">                		
                <tr class="estiloImput">
                  <td width="20%" rowspan="2">VOCABULARIO QUE UTILIZA:</td>
                  <td width="20%">RICO</td>
                  <td width="20%">POBRE</td>                
                  <td width="20%">MEDIANO</td>
                  <td width="20%">SOFISTICADO</td>
                </tr>		
                <tr class="camposRepInp"> 
                  <td>
                     <input type="text" id="txt_EXLO_C3" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLO_C4" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLO_C5" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLO_C6" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  
                </tr>     
          </table> 
        </td>
    </tr>  
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="25%" rowspan="2">ALTERACIONES SEMANTICAS:</td>
                  <td width="25%">NEOLOGISMOS</td>
                  <td width="25%">CIRCUNLOQUIOS</td>
                  <td width="25%">PRECEVERACIONES</td>                
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                     <input type="text" id="txt_EXLO_C7" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLO_C8" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLO_C9" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
               </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">DURANTE LA CHARLA LE CUESTA RECUPERAR PALABRAS (DISNOMIAS)</td>
            <td width="50%">DESCRIPCION DE DESARROLLO SEMANTICO</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLO_C10"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
            <td>
             <textarea type="text" id="txt_EXLO_C11"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
        </table> 
      </td> 
  </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr class="camposRepInp" >
     <td width="100%">NIVEL SlNTACTlCO</td> 
  </tr>                   
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">EXTENSION DE FRASES QUE UTILIZA</td>
            <td width="50%">USO DE ARTICULOS SUSTANTIVOS</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLO_C12"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
            <td>
             <textarea type="text" id="txt_EXLO_C13"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
        </table> 
      </td> 
  </tr>
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">CONCORDANCIA DE GENERO Y NUMERO</td>
            <td width="50%">RESPETA SECUENCIA LOGICA (SUJETO A VERBO)</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLO_C14"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
            <td>
             <textarea type="text" id="txt_EXLO_C15"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
        </table> 
      </td> 
  </tr>
  <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="25%" rowspan="2">TIEMPOS VERBALES:</td>
                  <td width="25%">PRESENTE</td>
                  <td width="25%">PASADO</td>
                  <td width="25%">FUTURO</td>                
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                     <input type="text" id="txt_EXLO_C16" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLO_C17" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLO_C18" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
               </tr>     
          </table> 
        </td>
    </tr>
      <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="25%" rowspan="2">ADVERBIOS:</td>
                  <td width="25%">LUGAR </td>
                  <td width="25%">TIEMPO</td>
                  <td width="25%">MODO</td>                
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                     <input type="text" id="txt_EXLO_C19" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLO_C20" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLO_C21" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
               </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">ADJETIVOS</td>
            <td width="50%">PRONOMBRES</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLO_C22"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
            <td>
             <textarea type="text" id="txt_EXLO_C23"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
        </table> 
      </td> 
  </tr>
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">CONJUNCIONES</td>
            <td width="50%">PREPOSICIONES</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLO_C24"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
            <td>
             <textarea type="text" id="txt_EXLO_C25"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
        </table> 
      </td> 
  </tr>
  <tr>
        <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">PRESENCIA DE AGRAMATISMOS</td>
            <td width="50%">OBSERVACIONES</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLO_C26"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
            <td>
             <textarea type="text" id="txt_EXLO_C27"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
        </table> 
      </td> 
  </tr>
 <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
    <tr class="camposRepInp" >
     <td width="100%">NIVEL FONETICO &#8211;   FONOLOGICO </td> 
  </tr>                   
  <tr class="estiloImput">
     <td>     
          <p> ESTRUCTURA DE ORGANOS FONOARTICULADORES</p>
     </td> 
  </tr>  
    <tr class="estiloImput" >
      <td>
        <table width="100%" align="center">                  
          <tr>
            <td width="20%">ORGANO</td>
            <td width="20%">ANATOMIA</td>
            <td width="20%">MOVILIDAD</td>
            <td width="20%">TONO</td>
            <td width="20%">SENSIBILIDAD</td> 
          </tr>
        </table>   
    </td>
    </tr>
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="20%" align="right"  class="estiloImput">LABIOS</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXLO_C28" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C29" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td >                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C30" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>  
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C31" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%">                   
                <tr class="estiloImput">
                  <td width="20%" align="right"  class="estiloImput">LENGUA</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXLO_C32" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C33" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C34" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C35" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                  
                </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="20%"  align="right"  class="estiloImput">MEJILLAS</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXLO_C36" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C37" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C38" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>
                    <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C39" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                   
                </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="20%"  align="right"  class="estiloImput">MAXILARES &#8211;  MASETEROS &#8211;  PTERIGOIDEOS </td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXLO_C40" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C41" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C42" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>   
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C43" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="20%"  align="right"  class="estiloImput">PALADAR BLANDO</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXLO_C44" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C45" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C46" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>    
                    <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C47" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                </tr>   
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr>
                  <td width="20%"  align="right"  class="estiloImput">PALADAR OSEO</td>
                  <td align="center" width="20%">
                     <input type="text" id="txt_EXLO_C48" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C49" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C50" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>  
                    <td align="center" width="20%">
                      <input type="text" id="txt_EXLO_C51" maxlength="15" style="width:30%" onblur="guardarContenidoDocumento()"/>
                  </td>                  
                </tr>   
          </table> 
        </td>
    </tr>
      <tr>
        <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">OBSERVACIONES</td>
            <td width="50%">ADQUISICION DE REPERTORIO FONETICO &#8211; FONOLOGICO</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLO_C52"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
            <td>
             <textarea type="text" id="txt_EXLO_C53"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
        </table> 
      </td> 
  </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
    <tr class="camposRepInp" >
     <td width="100%">OTOSCOPIA</td> 
  </tr>
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">OIDO DERECHO</td>
            <td width="50%">OIDO IZQUIERDO</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLO_C54"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
            <td>
             <textarea type="text" id="txt_EXLO_C55"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
        </table> 
      </td> 
  </tr> 
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="100%">CONDUCTA HA SEGUIR</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXLO_C56"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
        </table> 
      </td> 
  </tr>
</table>


 
 






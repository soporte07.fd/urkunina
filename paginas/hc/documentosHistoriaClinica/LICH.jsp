
<table width="100%"  align="center">
<tr>
 <td >
  <table width="100%" align="center">
        <tr class="estiloImput">
          <td width="100%" colspan="4">USUARIO REINGRESA A CIRUGIA EN UN PERIODO MENOR A 3 MESES POR LA MISMA CAUSA:
             <select size="1" id="txt_LICH_C1" style="width:5%;" title="32"  onblur=" "  tabindex="14" >	  
                <option value="SI">SI</option>
                <option value="NO">NO</option>
             </select>
          </td>                                           
        </tr>		
        <tr class="titulos">
          <td width="20%">TIEMPO DE ADMISION</td>                               
          <td width="20%">TIEMPO DE ANESTESIA</td>
          <td width="20%">TIEMPO DE PROCEDIMIENTO</td>                
          <td width="20%">TIEMPO DE RECUPERACION</td>                
        </tr>		
        <tr class="estiloImput"> 
          <td>
              <input type="text" id="txt_LICH_C2" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>                
          <td>
              <input type="text" id="txt_LICH_C3" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>                
          <td>
              <input type="text" id="txt_LICH_C4" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>                
          <td>
              <input type="text" id="txt_LICH_C5" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>   
        </tr>     
  </table> 

  <table width="100%" align="center">
    <tr class="titulos2">
      <td width="90%" colspan="2">1-ADMISION A CIRUGIA AMBULATORIA
      </td>
	  <td width="10%" colspan="2">OBSERVACION
      </td>	  
    </tr> 
    <tr class="estiloImput">
      <td width="80%" class="estiloImputDer">USUARIO LLEGA 30 MINUTOS ANTES DE LA HORA DE CIRUGIA:</td> 
      <td width="10%" class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C6" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI" >SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
            <option value=""></option>
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C7" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>	 
    </tr>  
    <tr class="estiloImput">
      <td class="estiloImputDer">EL NOMBRE Y APELLIDO DE USUARIO CORRESPONDE CON LA PROGRAMACION QUIRURGICA Y CON EL PACIENTE:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C8" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C9" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
    <tr class="estiloImput">
      <td class="estiloImputDer">EL NUMERO DE IDENTIFICACION DEL USUARIO CORRESPONDE CON LA PROGRAMACION QUIRURGICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C10" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C11" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>		
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImputDer">EL NOMBRE DEL PROCEDIMIENTO A REALIZAR CORRESPONDE CON LA PROGRAMACION QUIRURGICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C12" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
      <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C13" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td> 	  
    </tr>    
    <tr class="estiloImput">
      <td class="estiloImputDer">EL SITIO QUIRURGICO CORRESPONDE CON LA PROGRAMACION QUIRURGICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C14" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>			
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C15" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>    
    <tr class="estiloImput">
      <td class="estiloImputDer">CONFIRMAR AYUNO (CUANDO CORRESPONDA SUGUN GUIA DE RECOMENDACIONES PREQUIRURGICAS) ESCRIBA LA ULTIMA HORA DE INGESTA DE ALIMENTO O BEBIDA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C16" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>       
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C17" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>      
    <tr class="estiloImput">
      <td class="estiloImputDer">USUARIO SE PRESENTA CON OJOS SIN MAQUILLAJE (VERIFICAR AUSENCIA DE PESTAÑINA):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C18" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C19" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>    
    <tr class="estiloImput">
      <td class="estiloImputDer">USUARIO SE PRESENTA CON UÑAS CORTAS, LIMPIAS Y SIN ESMALTE:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C20" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C21" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>     
    <tr class="estiloImput">
      <td class="estiloImputDer">USUARIO SE PRESENTA SIN OBJETOS DE VANIDAD, ARETES, ANILLOS ETC:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C22" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C23" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImput">NOMBRE DE LOS MEDICAMENTOS QUE ESTA INGIRIENDO EL USUARIO:
        <textarea type="text" id="txt_LICH_C24"  size="4000"  maxlength="4000" style="width:50%"   tabindex="101" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();" onkeypress="return validarKey(event,this.id)" > </textarea>
      </td> 
    </tr>    
    <tr class="estiloImput">
      <td class="estiloImputDer">USUARIO HA INGERIDO MEDICAMENTOS ESTIPULADOS EN LA GUIA DE RECOMENDACIONES PRE QUIRURGICAS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C25" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C26" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">USUARIO ES CONSUMIDOR CRONICO DE ALCOHOL, CIGARRILLO O SUSTANCIAS PSICOACTIVAS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C98" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C99" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>	
    <tr class="estiloImput">
      <td class="estiloImputDer">MANILLA DE IDENTIFICACION CON NOMRE Y APELLIDOS COMPLETOS, DOCUMENTO DE IDENTIFICACION Y NOMBRE DEL PROCEDIMIENTO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C27" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>   
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C28" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>  
    <tr class="titulos2">
      <td width="90%" colspan="2">INGRESO A CIRUGIA AMBULATORIA (Circulante)
      </td>  
	<td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImputDer">VESTIMENTA DEL USUARIO ADECUADA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C29" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C30" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>          
    <tr class="estiloImput">
      <td class="estiloImputDer">MANILLA DE IDENTIFICACION DEL USUARIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C31" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C32" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImputDer">CONFIRMACION DEL NOMBRE E IDENTIFICACION DEL USUARIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C33" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C34" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>  
    <tr class="estiloImput">
      <td class="estiloImputDer">PROCEDIMIENTO QUIRURGICO CONFIRMADO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C35" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C36" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>   
    <tr class="estiloImput">
      <td class="estiloImputDer">CONSENTIMIENTO INFORMADO FIRMADO POR EL USUARIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C37" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C38" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>   
    <tr class="estiloImput">
      <td class="estiloImputDer">MARCACION DE SITIO OPERATORIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C39" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C40" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImputDer">MANILLA ROJA DE ALERTA DE ALERGIAS MEDICAMNETOSAS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C41" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C42" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>         
    <tr class="estiloImput">
      <td class="estiloImputDer">CAMILLAS CON BARANDA ARRIBA Y CON FRENO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C43" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C44" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>  
    <tr class="estiloImput">
      <td class="estiloImputDer">ACCESO  VENOSO PERMEABLE (CUANDO CORRESPONDA):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C45" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C46" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>  
    <tr class="estiloImput">
      <td class="estiloImputDer">TOMA Y REGISTROS DE SIGNOS VITALES:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C47" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C48" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>    
    <tr class="titulos2">
      <td width="90%" colspan="2">2-ANTES DE LA INDUCCION DE LA ANESTESIA O BLOQUEO ANESTESICO (ANESTESIOLOGO, CIRUJANO, CIRCULANTE)
      </td>   
	<td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr>                         
    <tr class="estiloImput">
      <td class="estiloImputDer">SE HA COMPLETADO LA COMPROBACION DE LOS APARATOS DE ANESTESIA Y LA MEDICACION ANESTESICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C49" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C50" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>            
	<tr class="estiloImput">
      <td class="estiloImputDer">CIRCUITOS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C51" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C52" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">MEDICACION DEL USUARIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C53" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C54" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">REGISTRO ANESTESICO DEL USUARIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C55" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C56" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">EQUIPO DE INTUBACION:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C57" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C58" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">EQUIPO PARA ASPIRACION DE VIA AEREA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C59" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option> 
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C60" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">SISTEMA DE VENTILACION:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C61" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C62" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">PREMEDICACION ANESTESICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C63" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>     
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C64" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">EQUIPO DE SUCCION:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C65" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>    
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C66" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">SE HA COLOCADO EL PULSOXIMETRO AL PACIENTE Y FUNCIONA ?:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C67" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>     
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C68" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">ALERGIAS CONOCIDAS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C69" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>	
			<option value=""></option>	
         </select>
      </td>    
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C70" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">VIA AÉREA DIFICIL / RIESGO DE ASPIRACION:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C71" style="width:40%;" title="32" onblur="guardarContenidoDocumento()" tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C72" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();" />
	  </td>
    </tr>
	<tr class="titulos2">
      <td width="90%" colspan="2">3-ANTES DE LA INCISION
      </td>  
	<td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr>                         
    <tr class="estiloImput">
      <td class="estiloImputDer">TODOS LOS MIEMBROS DEL EQUIPO ESTAN PRESENTES:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C73" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>   
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C74" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">CIRUJANO, INSTRUMENTADOR Y CIRCULANTE CONFIRMAN: SU NOMBRE, FUNCION E IDENTIDAD DEL PACIENTE, SITIO OPERATORIO DEMARCADO, PROCEDIMIENTO QUIRURGICO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C75" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>                                                    
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C76" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">EL INSTRUMENTADOR VERIFICA QUE TODO EL INSTRUMENTAL Y LOS EQUIPOS A UTILIZAR SE ENCUENTREN FUNCIONANDO CORRECTAMENTE:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C77" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>   
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C78" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
	<tr class="titulos2">
      <td width="90%" colspan="2">PREVISION DE EVENTOS CRITICOS
      </td> 
	  <td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">EL EQUIPO CONOCE LAS PROBABLES COMPLICACIONES INTRAOPERATORIAS DEL PROCEDIMIENTO :</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C79" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C80" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="titulos2">
      <td width="90%" colspan="2">EQUIPO DE ENFERMERIA E INSTRUMENTADORA
      </td>   
	  <td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr> 
	 <tr class="estiloImput">
      <td class="estiloImputDer">EL INSTRUMENTADOR CONFIRMA EL ESTADO DE ESTERILIZACION DE PAQUETES QUIRURGICOS Y DISPOSITIVOS MEDICOS (CON RESULTADOS DE LOS INDICADORES) :</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C81" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C82" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">SE RESOLVIERON LAS INQUIETUDES REFERENTES AL INSTRUMENTAL O INSUMOS A UTILIZAR EN EL PROCEDIMIENTO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C83" style="width:40%;" title="32" onblur=" "  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C84" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="titulos2">
      <td width="90%" colspan="2">4- DURANTE EL PROCEDIMIENTO
      </td>      
	  <td width="10%" colspan="2">OBSERVACION
      </td>	
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">EL CIRCULANTE RECIBE, REALIZA EMBALAJE DE MUESTRAS PARA PATOLOGIA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C85" style="width:40%;" title="32"   tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C86" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">ETIQUETADO DE LAS MUESTRAS (LECTURA DE LA ETIQUETA EN VOZ ALTA, INCLUIDO EL NOMBRE DEL PACIENTE):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C87" style="width:40%;" title="32"  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C88" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();"/>
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">EL INSTRUMENTADOR CONFIRMA VERBALMENTE RECUENTO DE MATERIALES E INSTRUMENTAL :</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C89" style="width:40%;" title="32"  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C90" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();" />
	  </td>
    </tr>
	<tr class="titulos2">
      <td width="90%" colspan="2">5- ANTES DE QUE EL PACIENTE SALGA A RECUPERACION
      </td>
	  <td width="10%" colspan="2">OBSERVACION
      </td>	  
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">ANTES DE QUE EL PACIENTE SALGA DE LA SALA DE CIRUGIA, CIRUJANO Y AUXILIAR DE ENFERMERIA CONFIRMAN PROCEDIMIENTO REALIZADO::</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C91" style="width:40%;" title="32"  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C92" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase(); " />
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">SE IMPARTIO RECOMENDACIONES PARA EL PERIODO DE RECUPERACION Y/O TRATAMIENTO DEL PACIENTE POR PARTE DE ANESTESIA O CIRUGIA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C93" style="width:40%;" title="32"  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option> 
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C94" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase(); "/>
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">SE VERIFICA QUE LOS REGISTROS DE ANESTESIA Y DESCRIPCION QUIRURGICA ESTEN DILIGENCIADOS EN SU TOTALIDAD:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C95" style="width:40%;" title="32"  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C96" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();"/>
	  </td>
    </tr> 
  </table>  
   
 </td>   
</tr>   
	<tr class="titulos2" colspan="3">
      <td width="100%">Enfermeria Observacion:</td>	  
    </tr> 
    <tr class="estiloImput" colspan="3">
      <td class="estiloImput" width="100%">
        <textarea type="text" id="txt_LICH_C97"   maxlength="4000" style="width:50%"   tabindex="101" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"  onkeypress="return validarKey(event,this.id)"> </textarea>
      </td> 
    </tr>   
    <tr>
    	<td colspan="5" align="right">
        	<input type="button" onClick="guardarContenidoDocumento()" value="Guardar" title="btn544" class="small button blue" id="btnGuardarDocumento">                           
        </td>
    </tr>
</table>
<table width="100%">  
  <tr>  
     <td width="100%">    
       <input type="button" onclick="cerrarDocumentClinicoSinImp()" value="FINALIZAR SIN IMPRIMIR" title="btn587" class="small button blue" id="btnFinalizarDocumento_">        
     </td>                                                       
  </tr>   
</table>   
<!-- 
<table width="100%">
           <tr class="estiloImput"> 
            
              <td colspan="1" align="CENTER">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:40%" title="76" tabindex="14" >
                      <option value=""></option>         
                      <option value="7">Cancelado Finalizado</option>
                      <option value="10">Reprogramado Finalizado</option>                                                                                       
                  </select>              
              </td>                   
              <td colspan="1" align="CENTER">
              MOTIVO:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:27%" title="26"  tabindex="14" >	
                          <option value="1">NINGUNA</option>
                          <option value="3">ATRIBUIBLE AL PACIENTE</option>  
 						  <option value="4">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>   
              <td colspan="1" align="CENTER">
	           <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO AL FOLIO" onclick="cambioEstadoFolio()">                                    
			  </td> 
           </tr>  
</table>-->




<table width="100%"  cellpadding="0" cellspacing="0"  align="center" id="idInfoTituloEXAMEN">
  <tr>
   <td width="50%" valign="top">
      <table width="100%" align="center" border="1" cellpadding="0" cellspacing="0">
            <tr class="titulos"> 
              <td width="20%">Agudeza visual</td>                               
              <td width="25%">Sin Correcci&oacute;n</td>
              <td width="10%">Factor</td>              
              <td width="25%">Con Correcci&oacute;n</td>   
   			  <td width="10%">Tipo</td>                           
            </tr>		
            <tr class="inputBlanco"> 
              <td align="right">Ojo derecho</td>                               
              <td align="center"><label id="lbl_EXOF_C1"/></label></td>              
              <td align="center"><label id="lbl_EXOF_C2"/></label></td>
              <td align="center"><label id="lbl_EXOF_C3"/></label></td>              
              <td align="center"><label id="lbl_EXOF_C4"/></label></td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td align="right">Ojo Izquierdo</td>                               
              <td align="center"><label id="lbl_EXOF_C5"/></label></td>              
              <td align="center"><label id="lbl_EXOF_C6"/></label></td>
              <td align="center"><label id="lbl_EXOF_C7"/></label></td>              
              <td align="center"><label id="lbl_EXOF_C8"/></label></td>                
           </tr>     
      </table> 
   </td>  
   <td width="50%" valign="top">
      <table width="100%" align="center" border="1" cellpadding="0" cellspacing="0">           
          <tr>
            <td width="50%" >
                <table width="100%" align="center">
                      <tr class="titulos">  
                        <td width="40%">REFRACCION</td>                               
                        <td width="30%">Valor</td>
                      </tr>		
                      <tr class="inputBlanco">  
                        <td>Ojo derecho</td>                               
                        <td><label id="lbl_EXOF_C46"/></label> </td>
                     </tr>                                                                            
                      <tr class="inputBlanco">  
                        <td>Ojo Izquierdo</td>                               
                        <td><label id="lbl_EXOF_C47"/></label></td>
                     </tr>     
                </table>  
            </td>
            <td width="50%" >
                <table width="100%" align="center">
                      <tr class="titulos">  
                        <td width="40%">QUERATOMETRIA</td>                               
                        <td width="30%">Valor</td>
                        <td width="30%">&nbsp;</td>                
                      </tr>		
                      <tr class="inputBlanco"> 
                        <td>Ojo derecho</td>                               
                        <td><label id="lbl_EXOF_C48"/></label> </td>
                        <td>&nbsp;</td>                
                     </tr>                                                                            
                      <tr class="inputBlanco"> 
                        <td>Ojo Izquierdo</td>                               
                        <td><label id="lbl_EXOF_C49"/></label></td>
                        <td>&nbsp;</td>                
                     </tr> 
                </table> 
            </td>
          </tr> 
          <tr class="inputBlanco"> 
           <td colspan="3" align="left">
            <strong>Observaciones:</strong>&nbsp;&nbsp;<label id="lbl_EXOF_C50"/></label> 
           </td>   
          </tr>            
           
               
      </table> 
   </td>   
  </tr> 


  <tr id="idTrInfoTituloEXAMEN">
   <td width="50%" valign="top">
       <table width="100%" align="center" border="1" cellpadding="0" cellspacing="0">
            <tr class="titulos"> 
              <td width="20%">Balance muscular Ojo derecho</td>                               
              <td width="20%">Balance muscular Ojo izquierdo</td>                                             
            </tr>		
            <tr class="inputBlanco"> 
              <td height="40" align="center">
  					   <table width="30" >
                          <tr class="inputBlanco">
                            <td valign="top" align="left"><label id="lbl_EXOF_C9"/></label></td>
                            <td>&nbsp;</td>
                            <td valign="top" align="right"><label id="lbl_EXOF_C10"/></label></td>
                          </tr>
                          <tr class="inputBlanco">
                            <td align="left"><label id="lbl_EXOF_C11"/></label></td>
                            <td align="center">x</td>
                            <td align="right"><label id="lbl_EXOF_C12"/></label></td>
                          </tr>
                          <tr class="inputBlanco">
                            <td valign="bottom"><label id="lbl_EXOF_C13"/></label></td>
                            <td>&nbsp;</td>
                            <td valign="bottom" align="right"><label id="lbl_EXOF_C14"/></label></td>
                          </tr>
                        </table>                            
              </td>                          
              <td align="center">
  					   <table width="30" >
                          <tr class="inputBlanco">
                            <td valign="top" align="left"><label id="lbl_EXOF_C15"/></label></td>
                            <td>&nbsp;</td>
                            <td valign="top" align="right"><label id="lbl_EXOF_C16"/></label></td>
                          </tr>
                          <tr class="inputBlanco">
                            <td align="left"><label id="lbl_EXOF_C17"/></label></td>
                            <td align="center">x</td>
                            <td align="right"><label id="lbl_EXOF_C18"/></label></td>
                          </tr>
                          <tr class="inputBlanco">
                            <td valign="bottom"><label id="lbl_EXOF_C19"/></label></td>
                            <td>&nbsp;</td>
                            <td valign="bottom" align="right"><label id="lbl_EXOF_C20"/></label></td>
                          </tr>
                        </table> 
              </td>
           </tr>                                                                            
      </table>  
   </td>  
   <td width="50%" valign="top">   
      <TABLE width="100%" cellpadding="0" cellspacing="0">         
        <tr class="inputBlanco" >
           <td width="50%" align="left"><strong>TROPIAS:</strong>&nbsp;&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C21"/></label>&nbsp;&nbsp;&nbsp; <strong>Prismas:</strong>&nbsp;<label id="lbl_EXOF_C22"/></label></td>
           <td width="50%" align="left"><strong>FORIA:</strong>&nbsp;&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C23"/></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Prismas:</strong>&nbsp;<label id="lbl_EXOF_C24"/></label></td>
        </tr>  
        <tr class="inputBlanco" >
           <td colspan="2" align="left"><strong>DOMINANCIA:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C25"/></label> </td>
        </tr> 
        <tr class="inputBlanco" >
           <td colspan="2" align="left"><strong>Observacion Balance Muscular:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C51"/></label> </td>           
        </tr>  
      </TABLE>
   </td>   
  </tr>  
  
  <tr class="titulos">
	<td colspan="2"><strong>EXAMEN EXTERNO</strong></td>
  </tr> 
  <tr>
    <td colspan="2">  
      <TABLE width="100%"  cellpadding="0" cellspacing="0">         
        <tr class="inputBlanco">
           <td width="50%" align="left"><strong>APARATO LAGRIMAL:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C28"/></label></td>
		   <td width="50%" align="left"><strong>PARPADOS:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C27"/></label></td>           
        </tr>  
      </TABLE>
    </td>
  </tr>     
  <tr class="titulos">
	<td colspan="2"><strong>SEGMENTO ANTERIOR</strong></td>
  </tr> 
  <tr>
    <td colspan="2">  
      <TABLE width="100%" cellpadding="0" cellspacing="0">         
        <tr class="inputBlanco" >
           <td width="50%" align="left"><strong>PUPILAS:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C26"/></label></td>
		   <td width="50%" align="left"><strong>CONJUNTIVA:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C29"/></label></td>
		   
        </tr>  
      </TABLE>
    </td>
  </tr>  
  <tr>
    <td colspan="2">  
      <TABLE width="100%" cellpadding="0" cellspacing="0">         
        <tr class="inputBlanco" >
           <td width="50%" align="left"><strong>CORNEA:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C30"/></label></td>
           <td width="50%" align="left"><strong>ESCLEROTICA:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C31"/></label></td>
        </tr>  
      </TABLE>
    </td>
  </tr>  
  <tr>
    <td colspan="2">  
      <TABLE width="100%" cellpadding="0" cellspacing="0">         
        <tr class="inputBlanco" >
           <td width="50%" align="left"><strong>CAMARA ANTERIOR:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C32"/></label></td>
           <td width="50%" align="left"><strong>IRIS:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C33"/></label></td>
        </tr>  
      </TABLE>
    </td>
  </tr>  
  <tr>
    <td colspan="2">  
      <TABLE width="100%" cellpadding="0" cellspacing="0">         
        <tr class="inputBlanco" >
           <td width="50%" align="left"><strong>CRISTALINO:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C34"/></label></td>
           <td width="50%" align="left"><strong>GONIOSCOPIA:</strong>&nbsp;&nbsp;&nbsp; <label id="lbl_EXOF_C35"/></label></td>
        </tr>  
      </TABLE>
    </td>
  </tr>    
  <tr>
    <td colspan="2">  
      <TABLE width="100%" cellpadding="0" cellspacing="0">         
        <tr class="inputBlanco" >
           <td width="50%" align="left"><strong>PRESION INTRAOCULAR OJO DERECHO:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   mmHg:<label id="lbl_EXOF_C36"/></label>&nbsp;&nbsp;&nbsp;&nbsp;Hrs: <label id="lbl_EXOF_C37"/></label></td>
           <td width="50%" align="left"><strong>PRESION INTRAOCULAR OJO IZQUIERDO:</strong>&nbsp;&nbsp;&nbsp;&nbsp; mmHg:<label id="lbl_EXOF_C38"/></label>&nbsp;&nbsp;&nbsp;&nbsp;Hrs: <label id="lbl_EXOF_C39"/></label></td>
        </tr> 
		<tr class="inputBlanco" >
			<td width="100%" colspan="2" >
				<strong>OBSERVACIONES:</strong>&nbsp;<label id="lbl_EXOF_C52"/></label>
			</td>
		</tr>	
      </TABLE>
    </td>
  </tr> 
  <tr class="titulos">
	<td colspan="2"><strong>FONDO DE OJO</strong></td>
  </tr>
  <tr>
    <td colspan="2">  
      <TABLE width="100%" cellpadding="0" cellspacing="0">         
        <tr class="inputBlanco" >
           <td width="50%" align="left"><strong>VITREO:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C40"/></label></td>
           <td width="50%" align="left"><strong>PAPILA (NERVIO OPTICO):</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C41"/></label></td>
        </tr>  
      </TABLE>
    </td>
  </tr>  
  <tr>
    <td colspan="2">  
      <TABLE width="100%" cellpadding="0" cellspacing="0">         
        <tr class="inputBlanco" >
           <td width="50%" align="left"><strong>MACULA:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C42"/></label></td>
           <td width="50%" align="left"><strong>VASOS:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C43"/></label></td>
        </tr>  
      </TABLE>
    </td>
  </tr>
  <tr>
    <td colspan="2">  
      <TABLE width="100%" cellpadding="0" cellspacing="0">         
        <tr class="inputBlanco" >
           <td width="50%" align="left"><strong>RETINA:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C44"/></label></td>
           <td width="50%" align="left"><strong>OBSERVACIONES:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C53"/></label></td>
        </tr>  
      </TABLE>
    </td>
  </tr>
  <tr>
    <td colspan="2">  
      <TABLE width="100%" cellpadding="0" cellspacing="0">         
        <tr class="inputBlanco" >
           <td width="50%" align="left"><strong>EXAMENES COMPLEMENTARIO:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C45"/></label></td>
           <td width="50%" align="left"><strong>CONCEPTO:</strong>&nbsp;&nbsp;&nbsp;<label id="lbl_EXOF_C54"/></label></td>
        </tr>  
      </TABLE>
    </td>
  </tr>        
 
</table>  

           





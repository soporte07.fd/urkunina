
<div id="divVentanitaODONTOGRAMA"  style="display:none; z-index:9997; top:1px; left:211px;">
      <div class="transParencia" style="z-index:9998; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>   
      <div  id="ventanitaHija" style="z-index:9999; position:fixed; top:50px; left:150px; width:70%">  
	  <table width="90%" align="center"   class="fondoTablaAmarillo" >  
      <tr class="estiloImput" >
         <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaODONTOGRAMA')" />
         <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaODONTOGRAMA')" />         
         </td>   
      <tr> 
      <tr>
        <td width="100%" align="center" colspan="2">

		  <div id="divAcordionTratamientoPorDiente" style="display:BLOCK">      
                   <jsp:include page="../../hc/hc/divsAcordeonHg.jsp" flush="FALSE" >
                          <jsp:param name="titulo" value="Tratamiento Por diente" />
                          <jsp:param name="idDiv" value="divTratamientoPorDiente" />
                          <jsp:param name="pagina" value="../saludOral/contenidoTratamientoPorDiente.jsp"/>                
                          <jsp:param name="display" value="BLOCK" />     
                          <jsp:param name="funciones" value="cargarTratamientoPorDiente()"/>
                   </jsp:include> 
          </div>                 
		  <div id="divAcordionTratamientoSeguimiento" style="display:BLOCK">      
                   <jsp:include page="../../hc/hc/divsAcordeonHg.jsp" flush="FALSE" >
                          <jsp:param name="titulo" value="Seguimiento" />
                          <jsp:param name="idDiv" value="divTratamientoSeguimiento" />
                          <jsp:param name="pagina" value="../saludOral/contenidoOdontograma.jsp"/>                
                          <jsp:param name="display" value="NONE"/>     
                          <jsp:param name="funciones" value="cargarOdontograma()" />                                                                                                     
                   </jsp:include> 
          </div>                   
		  <div id="divAcordionTratamientoInicial" style="display:BLOCK">      
                   <jsp:include page="../../hc/hc/divsAcordeonHg.jsp" flush="FALSE" >
                          <jsp:param name="titulo" value="Inicial"/>
                          <jsp:param name="idDiv" value="divTratamientoInicial"/>
                          <jsp:param name="pagina" value="../saludOral/contenidoOdontogramaInicial.jsp"/>                
                          <jsp:param name="display" value="NONE"/>
                          <jsp:param name="funciones" value="cargarOdontogramaInicial()"/>
                   </jsp:include> 
          </div> 
		  <div id="divAcordionIndiceDePlaca" style="display:BLOCK">      
                   <jsp:include page="../../hc/hc/divsAcordeonHg.jsp" flush="FALSE" >
                          <jsp:param name="titulo" value="Indice De Placa"/>
                          <jsp:param name="idDiv" value="divControlDePlaca"/>
                          <jsp:param name="pagina" value="../saludOral/contenidoControlDePlaca.jsp"/>                
                          <jsp:param name="display" value="NONE"/>
                          <jsp:param name="funciones" value="cargarControlDePlaca()"/>
                   </jsp:include> 
          </div>           
          
          </td> 
          </tr> 
   <tr class="estiloImput">
      <td align="CENTER">TRATAMIENTO HISTORICO:
          <select size="1"   tabindex="14" id="cmb_num_trata_historico" style="width:10%;">	
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>              
      </select>
      </td>   
      <td align="CENTER">TIPO TRATAMIENTO= <label id="lblIdTipoTratamiento">-</label>

      </td>      
     </tr> 
          
</table>

</div>
</div>
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  	<tr class="estiloImput">
     <td>
		<input title="B5D1" type="button" class="small button blue" value="MOSTRAR ODONTOGRAMA"  onclick="mostrar('divVentanitaODONTOGRAMA')" />  
	</td>
  	</tr>
</table >




<table width="100%"  cellpadding="0" cellspacing="0"  align="center">    
    
<tr class="camposRepInp">
	

  <td width="1000%" bgcolor="#c0d3c1" >ANAMNESIS</td>
</tr>
</table>
  <table width="100%" cellpadding="0" cellspacing="0"  align="center">
<tr class="estiloImput">
  <td width="1000%">ANTECEDENTES MEDICOS Y ODONTOLOGICOS GENERALES (SELECCIONE SI / NO EN LA CASILLA CORRESPONDIENTE)</td>
</tr>
</table>
<table width="100%"  align="center">
<tr>
    <td width="25%" >
	    <table width="100%" align="center">
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '1. TRATAMIENTO MEDICO ACTUAL:')" >1. TRATAMIENTO MEDICO ACTUAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C1" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '2. TOMA DE MEDICAMENTOS:')" >2. TOMA DE MEDICAMENTOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C2" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
					<option value="NO">NO</option>
					<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '3. ALERGIAS:')">3. ALERGIAS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C3" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
					<option value="NO">NO</option>
					<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '4. CARDIOPATIAS:')">4. CARDIOPATIAS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C4" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	
						<option value="NO">NO</option>				
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '5. ALTERACION PRESION ARTERIAL:')">5. ALTERACION PRESION ARTERIAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C5" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
		</table>   
	</td>		
  			  
  <td width="25%" >
 <table width="100%" align="center">
			<tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '6. EMBARAZO:')">6. EMBARAZO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C6" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '7. DIABETES:')">7. DIABETES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C7" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '8. HEPATITIS:')">8. HEPATITIS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C8" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '9. IRRADIACIONES:')">9. IRRADIACIONES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C9" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '10. DISCRASIAS SANGUINEAS:')">10. DISCRASIAS SANGUINEAS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C10" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>            
</table>      
  </td>   
    <td width="25%" >
		<table width="100%" align="center">
             <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '11. FIEBRE REUMATICA:')">11. FIEBRE REUMATICA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C11" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '12. EMFERMEDADES RENALES:')">12. EMFERMEDADES RENALES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C12" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '13. INMUNOSUPRESION:')">13. INMUNOSUPRESION:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C13" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '14. TRANSTORNOS EMOCIONALES:')">14. TRANSTORNOS EMOCIONALES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C14" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '15. PATOLOGIA RESPIRATORIA:')">15. PATOLOGIA RESPIRATORIA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C15" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
      </table>      
  </td> 
   <td width="25%" >
		<table width="100%" align="center">
             <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '16. TRANSTORNOS GASTRICOS:')">16. TRANSTORNOS GASTRICOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C16" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '17. EPILEPSIA:')">17. EPILEPSIA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C17" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '18. CIRUGIAS (INCLUSO ORALES):')">18. CIRUGIAS (INCLUSO ORALES):</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C18" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '19. ENFERMEDADES ORALES:')">19. ENFERMEDADES ORALES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C19" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C21', '20. OTRAS ALTERACIONES:')">20. OTRAS ALTERACIONES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C20" style="width:95%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
		</table>      
  </td> 
</tr>   
</table> 

<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">   
           <tr class="estiloImput"> 
                <td width="37%" >OBSERVACIONES (SEGUN EL NUMERO)</td>                        
           </tr>     
           <tr class="estiloImput"> 
                <td  colspan="2">         
                    <textarea type="text" id="txt_EXGP_C21"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>
  
           
      </table> 
  </td>
</tr>   
</table> 




  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >EXAMEN ESTOMATOLOGICO</td>
</tr>
</table>
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="estiloImput">
  <td width="1000%">SELECCIONE SI / NO EN LA CASILLA CORRESPONDIENTE</td>
</tr>
</table>

<table width="100%"  align="center">
<tr>
    <td width="40%" >
	    <table width="100%" align="center">
			<tr class="titulos1"> 
                <td width="50%" colspan="4">TEJIDOS BLANDOS</td> 
            </tr>
			<tr class="estiloImput"> 
				<td align="right"></td> 
				<td align="left">NORMAL</td>
				<td align="right"></td> 
				<td align="left">NORMAL</td>  				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '1. LABIO SUPERIOR:')" >1. LABIO SUPERIOR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C22" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '8. PALADAR:')" >8. PALADAR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C23" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>  				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '2. LABIO INFERIOR:')" >2. LABIO INFERIOR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C24" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
			<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '9. OROFARINGE:')">9. OROFARINGE:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C25" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td> 				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '3. COMISURAS:')">3. COMISURAS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C26" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>   
			<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '10. LENGUA:')">10. LENGUA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C27" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>  				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '4. MUCOSA ORAL:')">4. MUCOSA ORAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C28" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '11. PISO DE LA BOCA:')">11. PISO DE LA BOCA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C29" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '5. SURCOS YUGALES:')">5. SURCOS YUGALES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C30" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>  
			<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '12. REBORDES:')">12. REBORDES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C31" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '6. FRENILLOS:')">6. FRENILLOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C32" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '13. G. SALIVALES:')">13. G. SALIVALES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C33" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>				
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '7. OTROS HALLAZGOS:')">7. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C34" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '14. OTROS HALLAZGOS')">14. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C35" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>  				
            </tr>
		</table>   
	</td>		
  			  
  <td width="33,3%" >
 <table width="100%" align="center">
			<tr class="titulos1">
                <td width="50%" colspan="2" >ATM. OCLUSION</td> 
            </tr>
			<tr class="estiloImput"> 
				<td align="right"onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '15. DOLOR MUSCULAR:')">15. DOLOR MUSCULAR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C36" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '16. DOLOR ARTICULAR:')">16. DOLOR ARTICULAR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C37" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '17. RUIDO ARTICULAR:')">17. RUIDO ARTICULAR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C38" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '18. ALT. MOVIMIENTO')">18. ALT. MOVIMIENTO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C39" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '19. MAL OCLUSIONES:')">19. MAL OCLUSIONES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C40" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '20. C. DESARROLLO:')">20. C. DESARROLLO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C41" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '21. OTROS HALLAZGOS:')">21. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C42" style="width:50%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>			
</table>      
  </td>   
    <td width="33,3%" >
		<table width="100%" align="center">
			<tr class="titulos1"> 
                <td width="50%" colspan="2">TEJIDOS DENTALES</td> 
            </tr>
             <tr class="estiloImput">
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '22. CAMBIO DE FORMA:')">22. CAMBIO DE FORMA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C43" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '23. CAMBIO TAMA&Ntilde;O:')">23. CAMBIO TAMA&Ntilde;O:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C44" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '24. CAMBIO NUMERO:')">24. CAMBIO NUMERO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C45" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '25. CAMBIO COLOR')">25. CAMBIO COLOR</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C46" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '26. CAMBIO POSICION:')">26. CAMBIO POSICION:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C47" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '27.  IMPACTADOS:')">27.  IMPACTADOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C48" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '28. OTROS HALLAZGOS:')">28. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C49" style="width:60%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
      </table>      
  </td> 
</tr>   
</table> 

<table width="100%"  align="center">
<tr>
    <td width="33,3%" >
	    <table width="100%" align="center">
			<tr class="titulos1"> 
                <td width="50%" colspan="2">EXAMEN PERIODONTAL</td> 
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '29. SANGRADO:')">29. SANGRADO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C50" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="SI">SI</option>
						<option value="NO">NO</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '30. MOVILIDAD:')">30. MOVILIDAD:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C51" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '31. RECESIONES:')">31. RECESIONES:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C52" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '32. BOLSA PERIODONTAL:')">32. BOLSA PERIODONTAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C53" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '33. CALCULOS:')">33. CALCULOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C54" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '34. ABSCESO:')">34. ABSCESO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C55" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '35. OTROS HALLAZGOS:')">35. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C56" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
		</table>   
	</td>		
  			  
  <td width="33,3%" >
 <table width="100%" align="center">
				<tr class="titulos1"> 
                <td width="50%" colspan="2">EXAMEN PULPAR</td> 
				</tr>
				 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '36. ALTERAC. VITALIDAD:')">36. ALTERAC. VITALIDAD:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C57" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '37. DOLOR PERCUSION:')">37. DOLOR PERCUSION:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C58" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '38. MOVILIDAD DENTAL:')">38. MOVILIDAD DENTAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C59" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '39. SENSIBILIDAD:')">39. SENSIBILIDAD:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C60" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '40. FISTULA:')">40. FISTULA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C61" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr> 
			<tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '41. DIENTE TRATADO:')">41. DIENTE TRATADO:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C62" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '42. OTROS HALLAZGOS')">42. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C63" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>			
</table>      
  </td>   
    <td width="33,3%" >
		<table width="100%" align="center">
			<tr class="titulos1"> 
                <td width="50%" colspan="2">HABITOS ORALES</td> 
            </tr>
             <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '43. RESPIRADOR ORAL:')">43. RESPIRADOR ORAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C64" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '44. SUCCION DIGITAL:')">44. SUCCION DIGITAL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C65" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '45. LENGUA PROCTATIL:')">45. LENGUA PROCTATIL:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C66" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '46. QUEILOSFAGIA:')">46. QUEILOSFAGIA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C67" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			 <tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '47. FUMADOR:')">47. FUMADOR:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C68" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '48. ONICOFAGIA:')">48. ONICOFAGIA:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C69" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right" onclick="envioObservacionesSegunNumero('txt_EXGP_C71', '49. OTROS HALLAZGOS:')">49. OTROS HALLAZGOS:</td> 
				<td align="left">
					<select size="1" id="txt_EXGP_C70" style="width:55%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>
				</td>                               
            </tr>
      </table>      
  </td> 
</tr>   
</table> 
<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%">OBSERVACIONES (SEGUN EL NUMERO)</td> 
                <td width="37%">OTROS HALLAZGOS</td>                  
                        
           </tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_EXGP_C71"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
                <td>
                    <textarea type="text" id="txt_EXGP_C72"    rows="5" cols="50"  maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td> 
           </tr>   
      </table> 
  </td>
</tr>   
</table> 

<table width="100%"  align="center">
<tr>
 <td width="50%" >
  <table width="100%">
           <tr class="titulos1" align="center"> 
                <td width="25%" >EXAMEN DE ACCION PREVENTIVA</td>
                <td width="15%">FRECUENCIA</td>                       
				
           </tr>     
            <tr class="estiloImput"> 
				<td align="right">1. Ha recibido charlas de Hingiene Oral?
				<select size="1" id="txt_EXGP_C73" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>				
				</td> 
				<td align="left">
					<input type="text" id="txt_EXGP_C74" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">2. Practica el cepillado diario?
				<select size="1" id="txt_EXGP_C75" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>				
				</td>
				<td align="left">
					<input type="text" id="txt_EXGP_C76" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">3. Usa la seda dental?
				<select size="1" id="txt_EXGP_C77" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>				
				</td>
				<td align="left">
					<input type="text" id="txt_EXGP_C78" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">4. Usa enguaje bucal?
				<select size="1" id="txt_EXGP_C79" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>				
				</td>
				<td align="left">
					<input type="text" id="txt_EXGP_C80" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">5. Le han aplicado  fl&uacute;or?
				<select size="1" id="txt_EXGP_C81" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>				
				</td>
				<td align="left">
					<input type="text" id="txt_EXGP_C82" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr>
			<tr class="estiloImput"> 
				<td align="right">6. Le han aplicado sellantes?
				<select size="1" id="txt_EXGP_C83" style="width:15%;" title="3" onblur="guardarContenidoDocumento()" tabindex="14" >	  
						<option value="NO">NO</option>
						<option value="SI">SI</option>
				</select>				
				</td>
				<td align="left">
					<input type="text" id="txt_EXGP_C84" size="100"  maxlength="35" style="width:100%" onblur="guardarContenidoDocumento()" />
				</td>                               
            </tr> 
			
      </table> 
  </td>
  
  <td width="50%" >
  <table width="100%" align="left">
           <tr class="titulos1"> 
                <td width="100%">OBSERVACIONES</td>                       
           </tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_EXGP_C85"  placeholder="Higiene oral regular." rows="10" cols="10000" maxlength="4000" style="width:100%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>   
      </table> 
  </td>
</tr>   
</table> 
<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">   
           <tr class="estiloImput"> 
                <td width="37%" colspan="2" class="titulosTodasMinuscu">PLAN DE TRATAMIENTO</td>                        
           </tr>     
           <tr class="estiloImput"> 
                <td  colspan="2">         
                    <textarea type="text" id="txt_EXGP_C86"  rows="12" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>
  
           
      </table> 
  </td>
</tr>   
</table> 






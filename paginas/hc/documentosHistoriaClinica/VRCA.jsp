

<table BORDER="2" width="100%"  cellpadding="0" cellspacing="0"  align="right">
   <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
            <tr class="camposRepInp">
            <td colspan="8">ESCALA DE RIESGO DE CAIDAS J.H.DOWNTON</td>
          </tr>
    <table BORDER="2" align="center">
		<tr>
			<td class="camposRepInp" colspan="2">CAIDAS PREVIAS</td>
      <td class="camposRepInp"></td>
	    <td class="camposRepInp">&nbsp;&nbsp;&nbsp;&nbsp;
              <select size="1" id="txt_VRCA_C1" style="width:34%;" title="32" tabindex="14" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();"> 
                <option value="0">NO</option>  
                <option value="1">SI</option> 
              </select>
            <input value="0" disabled="disabled" type="text"  id="txt_VRCA_C2" style="width:10%"  tabindex="2"/>
      </td>
		</tr><tr></tr><tr></tr><tr></tr><tr></tr>
    <tr>
      <td ROWSPAN=7 class="camposRepInp" colspan="2">MEDICAMENTOS</td>
      <td class="estiloImput" align="right">NINGUNO:</td>
      <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
              <select size="1" id="txt_VRCA_C3" style="width:34%;" title="32" tabindex="14" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" onchange="deshabilitarVRCA()"> 
                  <option value="1">SI</option> 
                  <option value="0">NO</option>  
              </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C4" style="width:10%"  tabindex="2"/>
      </td> 
    </tr>
    <tr> 
      <td class="estiloImput" align="right">TRANQUILIZANTES-SEDANTES:</td>
      <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
          <select size="1" id="txt_VRCA_C5" style="width:34%;" title="32" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();"  tabindex="14" > 
              <option value="0">NO</option> 
              <option value="1">SI</option> 
          </select>
          <input value="0" type="text" id="txt_VRCA_C6" disabled="disabled" style="width:10%"  tabindex="2"/>
      </td>            
    </tr>
          <tr >
            <td class="estiloImput" align="right">DIURETICOS:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_VRCA_C7" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" style="width:34%;" title="32" tabindex="14" > 
                        <option value="0">NO</option> 
                        <option value="1">SI</option> 
                    </select>
                    <input value="0" type="text" id="txt_VRCA_C8" disabled="disabled" style="width:10%"  tabindex="2"/>
               </td>            
          </tr>
          <tr >
            <td class="estiloImput" align="right">HIPOTENSORES (NO DIURETICOS):</td>
             <td  class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_VRCA_C9" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" style="width:34%;" title="32"  tabindex="14" > 
                        <option value="0">NO</option>
                        <option value="1">SI</option> 
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C10" style="width:10%"  tabindex="2"/>
               </td>            
          </tr>

           <tr>
            <td class="estiloImput" align="right">ANTIPARKINSONIANOS:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_VRCA_C11" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" style="width:34%;" title="32" tabindex="14"> 
                       <option value="0">NO</option>
                       <option value="1">SI</option>   
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C12" style="width:10%"  tabindex="2"/>
               </td>            
          </tr> 
          <tr>
            <td class="estiloImput" align="right">ANTIDEPRESIVOS:</td>
             <td  class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_VRCA_C13" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" style="width:34%;" title="32"  tabindex="14" > 
                      <option value="0">NO</option> 
                      <option value="1">SI</option> 
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C14" style="width:10%"  tabindex="2"/>
               </td>            
          </tr> 
           <tr>
            <td class="estiloImput" align="right">OTROS MEDICAMENTOS:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                  <select size="1" id="txt_VRCA_C15" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" style="width:34%;" title="32" tabindex="14"> 
                        <option value="0">NO</option> 
                        <option value="1">SI</option> 
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C16" style="width:10%"  tabindex="2"/>
               </td>        
          </tr><tr></tr><tr></tr><tr></tr><tr></tr>
          <tr>
            <td ROWSPAN=4 class="camposRepInp" colspan="2">DEFICITS SENSITIVO-MOTORES</td>
            <td class="estiloImput" align="right">NINGUNO:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_VRCA_C17" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" style="width:34%;" title="32" tabindex="14" onchange="deshabilitarVRCA1()" > 
                        <option value="1">SI</option>  
                        <option value="0">NO</option>
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C18" style="width:10%"  tabindex="2"/>
               </td> 
          </tr>
           <tr>
            <td class="estiloImput" align="right">ALTERACIONES VISUALES:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_VRCA_C19" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento()" style="width:34%;" title="32"   tabindex="14"  > 
                        <option value="0">NO</option>
                        <option value="1">SI</option> 
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C20" style="width:10%"  tabindex="2"/>
               </td>            
          </tr> 
          <tr>
            <td class="estiloImput" align="right">ALTERACIONES AUDITIVAS:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_VRCA_C21" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" style="width:34%;" title="32" tabindex="14"  > 
                        <option value="0">NO</option>
                        <option value="1">SI</option> 
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C22" style="width:10%"  tabindex="2"/>
               </td>            
          </tr> 

           <tr>
            <td class="estiloImput" align="right">EXTREMIDADES (Ictus...):</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                  <select size="1" id="txt_VRCA_C23" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" style="width:34%;" title="32" tabindex="14" > 
                        <option value="0">NO</option> 
                        <option value="1">SI</option> 
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C24" style="width:10%"  tabindex="2"/>
               </td>          
          </tr><tr></tr><tr></tr><tr></tr><tr></tr>
         <tr>
            <td ROWSPAN=2 class="camposRepInp" colspan="2">ESTADO MENTAL</td>
            <td class="estiloImput" align="right">ORIENTADO:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_VRCA_C25" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" onchange="cambiarCombo()" style="width:34%;" title="32" tabindex="14" > 
                        <option value="1">SI</option> 
                        <option value="0">NO</option>
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C26" style="width:10%"  tabindex="2"/>
               </td> 
          </tr>
           <tr>
            <td class="estiloImput" align="right">CONFUSO:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_VRCA_C27" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" onchange="cambiarCombo1()" style="width:34%;" title="32"   tabindex="14" >
                        <option value="0">NO</option> 
                        <option value="1">SI</option> 
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C28" style="width:10%"  tabindex="2"/>
               </td>            
          </tr><tr></tr><tr></tr><tr></tr><tr></tr>      
        <td ROWSPAN=5 class="camposRepInp" colspan="2">DEAMBULACION</td>
            <td class="estiloImput" align="right">NORMAL:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_VRCA_C29" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();"  style="width:34%;" title="32" tabindex="14"> 
                        <option value="1">SI</option> 
                        <option value="0">NO</option>
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C30" style="width:10%"  tabindex="2"/>
               </td> 
          </tr>
                     <tr>
            <td class="estiloImput" align="right">SEGURA:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_VRCA_C31" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();"   style="width:34%;" title="32" tabindex="14" > 
                        <option value="1">SI</option> 
                        <option value="0">NO</option>           
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C32" style="width:10%"  tabindex="2"/>
               </td>            
          </tr>
           <tr>
            <td class="estiloImput" align="right">SEGURA CON AYUDA:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_VRCA_C33" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" onchange="cambiarComboDeambulacion()" style="width:34%;" title="32" tabindex="14" > 
                        <option value="0">NO</option>           
                        <option value="1">SI</option> 
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C34" style="width:10%"  tabindex="2"/>
               </td>            
          </tr>
          <tr>
            <td class="estiloImput" align="right">INSEGURA CON AYUDA/SIN AYUDA:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_VRCA_C35" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" onchange="cambiarComboDeambulacion()" style="width:34%;" title="32" tabindex="14"> 
                        <option value="0">NO</option> 
                        <option value="1">SI</option>          
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C36" style="width:10%"  tabindex="2"/>
               </td>            
          </tr>
          <tr>
            <td class="estiloImput" align="right">IMPOSIBLE:</td>
             <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_VRCA_C37" onclick ="total();imprimirTotalcombo();guardarContenidoDocumento();" onchange="cambiarComboDeambulacion()" style="width:34%;" title="32"   tabindex="14" > 
                        <option value="0">NO</option> 
                        <option value="1">SI</option>           
                    </select>
                    <input value="0" disabled="disabled" type="text" id="txt_VRCA_C38"  style="width:10%"  tabindex="2"/>
               </td>
          <table align="center">
          <tr>
            <td class="estiloImput" ><strong>VALORACION TOTAL:</strong></td>
            <td><strong>&nbsp;<input align="center" type="text" id="txt_VRCA_C39" disabled="disabled" style="WIDTH: 30px; HEIGHT: 20px" /></strong>
     
            </td>
            
              <td><input value="0" disabled="disabled" type="text" id="txt_VRCA_C40"  style="WIDTH: 200px; HEIGHT: 15px"  tabindex="2"/></td>
          </tr>  
          </table>             
          </tr> 
                   
       </table>  
  </tr>
</table>

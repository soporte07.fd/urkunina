<table width="100%" align="center">
    <tr class="titulos"> 
      <td width="20%">EXAMEN CORRECTO</td>                               
      <td width="20%">SITIO DE EXAMEN CORRECTO</td>
      <td width="20%">DATOS DEL PACIENTE CORRECTOS</td>                
      <td width="20%">CANTIDAD DE EXAMENES CORRECTA</td>                
      <td width="20%">RIESGOS POR MEDICAMENTOS </td>                                                
    </tr>		
    <tr class="estiloImput"> 
      <td>
         <select size="1" id="txt_ECOE_C1" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
         </select>               
      </td>                
      <td>
         <select size="1" id="txt_ECOE_C2" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
         </select>               
      </td>                
      <td>
         <select size="1" id="txt_ECOE_C3" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
         </select>               
      </td>                
      <td>
         <select size="1" id="txt_ECOE_C4" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
         </select>               
      </td>   
      <td>
         <select size="1" id="txt_ECOE_C5" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
            <option value="NA" selected="selected">NA</option>                    
            <option value="SI">SI</option>
            <option value="NO">NO</option>
         </select>               
      </td>                
    </tr>   
    
    <tr class="titulos"> 
      <td width="20%">PREPARACION PREVIA DEL PACIENTE</td>                               
      <td width="20%">DOCUMENTOS NECESARIOS</td>
      <td width="20%">CONSENTIMIENTO INFORMADO</td>                
      <td width="40%" colspan="2">OBSERVACIONES</td>                
    </tr>                                                                                     

    <tr class="estiloImput"> 
      <td>
         <select size="1" id="txt_ECOE_C6" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
         </select>               
      </td>                
      <td>
         <select size="1" id="txt_ECOE_C7" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
            <option value="NA">NA</option>                    
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
         </select>               
      </td>                
      <td>
         <select size="1" id="txt_ECOE_C8" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >	  
            <option value="NA">NA</option>                    
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
         </select>               
      </td>                
      <td colspan="2">
	      <textarea type="text" id="txt_ECOE_C9" rows="6"   maxlength="4000" style="width:95%"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>                  
      </td>
   </tr>          
</table> 

<table width="100%"  cellpadding="0" cellspacing="0"  align="left">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
  <tr class="camposRepInp" >
     <td width="100%">CARACTERISTICAS DE LA LESION</td> 
  </tr>
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr >
            <td class="titulos1" width="15%">FECHA DE INICIO DE LA LESION:</td>
            <td  align="left">
             <input type="text" id="txt_EXPF_C1" maxlength="10" style="width:10%" onblur="guardarContenidoDocumento()"/>
            </td> 
           </tr> 
        </table> 
      </td> 
  </tr>
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="titulos1">
            <td width="100%">LIMITACIONES QUE PRODUCE</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXPF_C2"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
        </table> 
      </td> 
  </tr>
  <tr>
    <td width="100%" bgcolor="#c0d3c1">.</td>
  </tr>  
  <tr>
      <td>
        <table width="50%" align="left">
          <tr class="camposRepInp">
            <td colspan="2">EXAMEN MUSCULAR</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="30%">MUSCULO</td>
            <td width="25%"></td>
          </tr>  
          <tr class="estiloImput">
            <td align="right">OCCIPITOFRONTAL:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C3" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>  
           <tr class="estiloImput">
            <td align="right">SUPERCILIAR:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C4" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr> 
          <tr class="estiloImput">
            <td align="right">PIRAMIDAL DE LA NARIZ:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C5" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>
          <tr class="estiloImput">
            <td align="right">NASAL:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C6" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>
           <tr class="estiloImput">
            <td align="right">ORBICULAR DE LOS PARPADOS:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C7" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr> 
          <tr class="estiloImput">
            <td align="right">ELEVADOR PARPADO SUPERIOR:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C8" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr> 
           <tr class="estiloImput">
            <td align="right">RECTO SUPERIOR DERECHO Y OBLICUO MENOR IZQUIERDO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C9" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>  
           <tr class="estiloImput">
            <td align="right">OBLICUO MAYOR DERECHO Y RECTO INFERIOR IZQUIERDO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C10" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>   
             <tr class="estiloImput">
            <td align="right">ORBICULAR DE LOS LABIOS:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C11" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>   
             <tr class="estiloImput">
            <td align="right">CIGOMATICO MENOR:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C12" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>   
             <tr class="estiloImput">
            <td align="right">CANINO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C13" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>
          <tr class="estiloImput">
            <td align="right">CIGOMATICO MAYOR:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C14" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>  
          <tr class="estiloImput">
            <td align="right">RISORIO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C15" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>  
          <tr class="estiloImput">
            <td align="right">BUCCINADOR:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C16" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>   
          <tr class="estiloImput">
            <td align="right">CUADRADO DE LA BARBA:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C17" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr> 
         <tr class="estiloImput">
            <td align="right">PLATISMA:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C18" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>    
          <tr class="estiloImput">
            <td align="right">TEMPORAL, MACETERO Y PTERIGOIDEO INTERNO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C19" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>    
          <tr class="estiloImput">
            <td align="right">PTERIGOIDEO EXTERNO E INTERNO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C20" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>  
         <tr class="estiloImput">
            <td align="right">DIGASTRICO Y MUSCULOS SUPRA HIOIDEOS:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_EXPF_C21" style="width:28%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="BUENA">BUENA</option> 
                        <option value="REGULAR">REGULAR</option>           
                        <option value="MALA">MALA</option>           
                    </select>
               </td>            
          </tr>                             
        </table> 
      </td> 
  </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
   <tr class="camposRepInp" >
     <td width="100%">SENSIBILIDAD</td> 
  </tr>
    <tr>
      <td>
        <table width="100%" align="center">   
         <tr class="titulos1">
            <td width="100%">PRESENTA ALTERACION DE LA SENSIBILIDAD?</td>
         </tr>                 
         <tr class="camposRepInp"> 
            <td>
               <select size="1" id="txt_EXPF_C22" style="width:4%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select> 
            </td>    
          </tr>            
        </td>
        </table> 
      </td> 
  </tr>
  <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="titulos1">
                  <td width="25%">NORMAL</td>
                  <td width="25%">HIPOESTESIA</td>
                  <td width="25%">HIPERESTESIA</td>
                  <td width="25%">ANESTESIA</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXPF_C23" maxlength="8" style="width:20%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXPF_C24" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXPF_C25" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXPF_C26" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr> 
      <tr>
      <td>
        <table width="100%" align="center">   
         <tr class="titulos1">
            <td width="100%">AREA</td>
         </tr>                 
         <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXPF_C27"    rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
            </td>    
          </tr>            
        </table> 
      </td> 
  </tr>
</table>


 
 






<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="100%">LENGUAJE LECTO-ESCRlTO</td> 
  <tr>                     
  <tr class="estiloImput">
     <td>     
          <p>SE EVALUARA MEDIANTE LA LECTURA DE UN TEXTO NARRATIVO Y/O INFORMATIVO.</p>
     </td> 
  </tr> 
  <tr>
  <td width="1000%" bgcolor="#c0d3c1" >.</td>
  <tr>
  <tr class="camposRepInp">
     <td width="100%">LECTURA</td> 
  </tr>                     
  <tr>
    	<td> 
        	<table width="100%" align="center">                		
                <tr class="estiloImput">
                  <td width="20%" rowspan="2">TIPO LECTOR:</td>
                  <td width="20%">SUBSLIABICO</td>
                  <td width="20%">VACILANTE</td>                
                  <td width="20%">CORRIENTE</td>
                  <td width="20%">EXPRESIVO</td>
                </tr>		
                <tr class="camposRepInp"> 
                  <td>
                     <input type="text" id="txt_EXLL_C1" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLL_C2" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLL_C3" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLL_C4" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  
                </tr>     
          </table> 
        </td>
    </tr>  
    <tr>
      <td>
        <table width="100%" align="center">                   
                <tr>
                  <td width="25%" class="estiloImput">USO DE SIGNOS DE PUNTUACION:</td>
                  <td width="25%" align="center" class="camposRepInp">
                    <select size="1" id="txt_EXLL_C5" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="14" >   
                       <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select>

                  </td>
                  <td width="25%" class="estiloImput">MOVIMIENTOS ASOCIADOS:</td>
                  <td width="25%" class="camposRepInp">                    
                     <textarea type="text" id="txt_EXLL_C6"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
                  </td>
                </tr>   
          </table> 
         </td> 
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="20%" rowspan="2">ERRORES ESPECIFICOS:</td>
                  <td width="20%">OMISIONES</td>
                  <td width="20%">SUSTITUCIONES</td>                
                  <td width="20%">ADICIONES</td>
                  <td width="20%">OTROS:</td>
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                     <input type="text" id="txt_EXLL_C7" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLL_C8" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLL_C9" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                     <textarea type="text" id="txt_EXLL_C10"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
                  </td>                  
                </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="25%" rowspan="2">NIVEL DE COMPRENSION:</td>
                  <td width="25%">LITERAL</td>
                  <td width="25%">INFERENCIAL</td>
                  <td width="25%">CRITICO INTERTEXTUAL</td>                
                </tr>   
                <tr class="camposRepInp"> 
                  <td>
                     <input type="text" id="txt_EXLL_C11" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLL_C12" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                  <td>
                      <input type="text" id="txt_EXLL_C13" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
               </tr>     
          </table> 
        </td>
    </tr>  
    <tr>
        <td width="1000%" bgcolor="#c0d3c1" >.</td>
    </tr>
    <tr class="camposRepInp">
     <td width="100%">ESCRITURA</td> 
    <tr> 
      <td class="estiloImput">
        <p>SE EVALUARA MEDIANTE PRODUCCION TEXTUAL.</p>
      </td>
    <tr>
      <table width="100%" align="center">                   
        <tr class="estiloImput">
          <td>EMPLEO DE ESPACIO Y RENGLON</td>
        </tr>   
        <tr class="camposRepInp"> 
          <td>
           <textarea type="text" id="txt_EXLL_C14"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
          </td>  
        </table>           
       </tr>  
        <tr>
          <table width="100%" align="center">                   
            <tr class="estiloImput">
              <td width="50%">PRENSION</td>
              <td width="50%">PRESION</td>
            </tr>   
            <tr class="camposRepInp"> 
              <td>
               <textarea type="text" id="txt_EXLL_C15"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
              </td>             
              <td>
               <textarea type="text" id="txt_EXLL_C16"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
              </td>
         </table> 
      </tr>  
      <tr>
          <table width="100%" align="center">                   
            <tr class="estiloImput">
              <td width="50%">DIRECCIONALIDAD DE LOS TRAZOS</td>
              <td width="50%">TAMA&Ntilde;O DE LA LETRA</td>
            </tr>   
            <tr class="camposRepInp"> 
              <td>
               <textarea type="text" id="txt_EXLL_C17"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
              </td>             
              <td>
               <textarea type="text" id="txt_EXLL_C18"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
              </td>
         </table> 
      </tr>  
      <tr>
          <table width="100%" align="center">                   
            <tr class="estiloImput">
              <td width="50%">COHESION</td>
              <td width="50%">COHERENCIA</td>
            </tr>   
            <tr class="camposRepInp"> 
              <td>
               <textarea type="text" id="txt_EXLL_C19"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
              </td>             
              <td>
               <textarea type="text" id="txt_EXLL_C20"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
              </td>
         </table> 
      </tr>
     <tr>
      <table width="100%" align="center">                   
        <tr class="estiloImput">
          <td>ERRORES ESPECIFICOS</td>
        </tr>   
        <tr class="camposRepInp"> 
          <td>
           <textarea type="text" id="txt_EXLL_C21"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
          </td>  
        </table>           
       </tr>
         <tr>
        <td width="1000%" bgcolor="#c0d3c1" >.</td>
    </tr>
      <tr>
      <table width="100%" align="center">                   
        <tr class="camposRepInp">
          <td>CONDUCTA A SEGUIR</td>
        </tr>   
        <tr class="camposRepInp"> 
          <td>
           <textarea type="text" id="txt_EXLL_C22"   rows="5" cols="50" maxlength="4000" style="width:95%" tabindex="100" onblur="guardarContenidoDocumento()"> </textarea>      
          </td>  
        </table>           
       </tr> 
</table>


 
 






<table width="100%"   align="center">
  <tr class="camposRepInp">
    <td bgcolor="#c0d3c1" colspan="3"></td> 	
  </tr>
 </table>

<table style="padding-right: 0px;" width="50%" align="left">
  <tr class="camposRepInp">
  	<td bgcolor="#c0d3c1" colspan="3">OBSERVACION GENERAL </td>
  </tr> 
   <tr align="center" class="titulos"> 
   <td style="width: 20%;padding-top: 10px;"><strong>1. EDEMA:</td></strong>
   </tr>

   <tr align="center" class="estiloImput"> 
    <td><select id="txt_OSTT_C1" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="101" >	  
    <option value=""></option>
    <option value="SI">SI</option> 
    <option value="NO">NO</option> 	
     </select></td> </tr>

   <tr  align="center" class="titulos">
    <td style="width: 80%;padding-top:10px;" ><strong> LOCALIZACION:</td></strong>
   </tr>	
    
   <tr align="center" >
    <td>
    	<textarea type="text" id="txt_OSTT_C2" maxlength="500" style="width:95%;height: 50px;"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
    </td>                                                    
  </tr> 

  <tr class="titulos"> 
    <td style="width: 20%;padding-top: 10px;" ><strong>2. DOLOR:</td></strong>
    </tr>

  	<tr align="center" class="estiloImput"> 
    	<td><select  id="txt_OSTT_C3" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="101" >	  
      <option value=""></option>
      <option value="SI">SI</option> 
      <option value="NO">NO</option> 	
       </select></td></tr>
    
   <tr align="center" class="titulos">
    <td style="width: 80%;padding-top:10px;"><strong>LOCALIZACION:</td></strong>
   </tr>
   <tr>
    <td align="center">
    	<textarea type="text" id="txt_OSTT_C4"   maxlength="500" style="width:95%;height: 50px;"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
    </td>                                                    
  </tr>

  <tr class="titulos"> 
    <td style="width: 20%;padding-top: 10px;" ><strong>2.1 TIPO DE DOLOR:</td></strong>
     </tr>
  
     <tr align=" center"class="estiloImput"> 
      <td><select size="1" id="txt_OSTT_C5" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="101" >	  
          <option value=""></option>
          <option value="QUEMANTE">QUEMANTE</option>
          <option value="SORDO">SORDO</option>
          <option value="PUNZANTE">PUNZANTE</option>
          <option value="HORMIGUEANTE">HORMIGUEANTE</option>
          <option value="IRRADIADO">IRRADIADO</option>
          <option value="OTROS">OTROS</option> 	
         </select></td></tr>

  <tr align="center" class="titulos">
    <td style="width: 80%;padding-top: 10px;" ><strong>LOCALIZACION:</td></strong>
  </tr>
  <tr>
    <td align="center">
   	<textarea type="text" id="txt_OSTT_C6"   maxlength="500" style="width:95%;height: 50px;"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
    </td>                                                    
  </tr> 

  <tr class="titulos" align="center"> 
    <td  style="width: 20%;padding-top: 10px;" ><strong>2.2 ¿HACE CUANTO TIEMPO DUELE?:</td></strong></tr>

    <tr>
      <td align="center" style="padding-top: 10px;">
        <textarea type="text" id="txt_OSTT_C7" maxlength="500" style="width:95%;height: 50px;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
      </td>
    </tr>

    <tr class="titulos"> 
      <td align="center" style="width: 20%;padding-top: 10px;"><strong>2.3 CALIFICACION DEL DOLOR SEGUN E.V.A:</td></strong></tr>
  
      <tr align="center" class="estiloImput"> 
        <td><select  id="txt_OSTT_C8" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="101" >	  
        <option value=""></option>
        <option value="01">01</option> 
        <option value="02">02</option> 	
        <option value="03">03</option> 
        <option value="04">04</option>
        <option value="05">05</option> 
        <option value="06">06</option>
        <option value="07">07</option> 
        <option value="08">08</option>
        <option value="09">09</option> 
        <option value="10">10</option>
         </select></td></tr>

      <tr class="titulos"> 
        <td style="width: 20%;padding-top: 10px;" ><strong>2.4 FACTOR DESENCADENANTE DEL DOLOR:</td></strong>
        </tr>    
      <tr class="estiloImput"> 
        <td align="center" ><select  id="txt_OSTT_C9" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="101" >	  
            <option value=""></option>
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            </select></td></tr>
       
       <tr class="titulos">
        <td style="width: 80%;padding-top: 10px;" ><strong>DESCRIBA:</td></strong>
       </tr>     
        <tr>
        <td align="center">
        <textarea type="text" id="txt_OSTT_C10"   maxlength="500" style="width:95%;height: 50px;"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
        </td>                                                    
      </tr> 
</table>

<table width="50%"   align="center">
   <tr class="camposRepInp"> 
    <td bgcolor="#c0d3c1"  colspan="3" >PIEL Y FANERAS</td></tr> 

   <tr class="titulos"><td colspan="2" align="center" style="width: 20%;padding-top: 60px;"><strong>3. PIEL Y FANERAS:</td></strong></tr>
    <tr>
      <td align="center" colspan="2">
        <textarea type="text" id="txt_OSTT_C11" maxlength="500" style="width:90%;height: 50px;;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
      </td>
    </tr>
 
  <tr class="titulos"> 
      <td style="width: 20%;padding-top: 10px;" ><strong>4. DEFORMIDAD ARTICULAR:</td></strong>
      </tr>
  
  <tr>
    <tr class="estiloImput"> 
      <td  ><select  id="txt_OSTT_C12" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="101" >	  
          <option value=""></option>
          <option value="SI">SI</option>
          <option value="NO">NO</option>
          </select></td></tr>

   <tr class="titulos">
    <td style="width: 80%;padding-top: 10px;" ><strong>DESCRIBA:</td></strong>
   </tr>
  <tr>
   <td align="center" colspan="2">
    <textarea type="text" id="txt_OSTT_C13" maxlength="500" style="width:90%; height: 50px;"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
      </td>                                                    
    </tr> 

    <tr class="titulos"> 
      <td style="width: 20%;padding-top: 10px;" ><strong>5. ALTERACIONES DE LA SENSIBILIDAD:</td></strong>
      </tr>
  
      <tr class="estiloImput"> 
      <td><select  id="txt_OSTT_C14" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="101" >	  
          <option value=""></option>
          <option value="SI">SI</option>
          <option value="NO">NO</option>
          </select></td>
          </tr>
      
      <tr class="titulos">  
        <td style="width: 80%;padding-top: 10px;" ><strong>DESCRIBA:</td></strong>
      </tr>
      
     <tr>
      <td align="center" colspan="2">
        <textarea type="text" id="txt_OSTT_C15" maxlength="500" style="width:90%;height: 50px;"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
      </td>                                                    
    </tr> 

    <tr class="titulos"> 
      <td style="width: 20%;padding-top: 10px;" ><strong>6. RETRACCIONES MUSCULARES:</td></strong>
      </tr>
  
      <tr class="estiloImput"> 
      <td><select  id="txt_OSTT_C16" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="101" >	  
          <option value=""></option>
          <option value="SI">SI</option>
          <option value="NO">NO</option>
          </select></td></tr>
          
      <tr class="titulos">
        <td style="width: 80%;padding-top: 10px;" ><strong>DESCRIBA:</td></strong>
      </tr>
      
     <tr>
      <td align="center" colspan="2">
        <textarea type="text" id="txt_OSTT_C17" maxlength="500" style="width: 90%;height: 50px;"   tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
      </td>                                                    
    </tr> 
    
  </tabla>

    <table  width="95%" align="center">
      <tr class="camposRepInp">
        <td bgcolor="#c0d3c1" colspan="3">7. A.M.A </td>
      </tr> 
    <tr class="titulos"> 
      <td align="center" colspan="2" style="width: 20%;padding-top: 10px;" ><strong>DESCRIPCION: </td></strong></tr>
  
      <tr>
        <td align="center" colspan="2">
          <textarea type="text" id="txt_OSTT_C18" maxlength="500" style="width:1070px;padding-right: 50px;height: 41px;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
        </td>
      </tr>
      </table>

      <table  width="95%" align="center">
        <tr class="camposRepInp">
          <td bgcolor="#c0d3c1" colspan="3">8. FUERZA MUSCULAR </td>
        </tr> 

      <tr class="titulos"> 
        <td  align="center" colspan="2"  style="width: 20%;padding-top: 10px;" ><Strong>DESCRIPCION: </td></strong></tr>
    
        <tr>
          <td align="center" colspan="2">
            <textarea type="text" id="txt_OSTT_C19" maxlength="500" style="width:1070px;padding-right: 50px;height: 41px;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
          </td>
        </tr> 
        
        <tr class="titulos"> 
          <td style="width: 20%;padding-top: 10px;" ><strong>8.1 MUSCULOS FACIALES:</td></strong>
          </tr>
      
          <tr class="estiloImput"> 
          <td><select size="1" id="txt_OSTT_C20" style="width:15%;" title="32" onblur="guardarContenidoDocumento()"  tabindex="101" >	  
              <option value=""></option>
              <option value="FRONTAL">FRONTAL</option>
              <option value="PIRAMIDAL">PIRAMIDAL</option>
              <option value="ORBICULAR DE OJOS">ORBICULAR DE OJOS</option>
              <option value="ELEVADOR LABIO SUPERIOR Y ALA DE NARIZ">ELEVADOR LABIO SUPERIOR Y ALA DE NARIZ</option>
              <option value="RISORIO">RISORIO</option>
              <option value="CIGOMATICO">CIGOMATICO</option>
              <option value="MASETERO">MASETERO</option>
              <option value="ORBICULAR DE LABIOS">ORBICULAR DE LABIOS</option>
              <option value="BUCCIONADOR">BUCCIONADOR</option>
              <option value="CUADRADO DE BARBA">CUADRADO DE BARBA</option>
              <option value="CUTANEO DEL CUELLO">CUTANEO DEL CUELLO</option>
              </select></td></tr>
           
          <tr class="titulos">
            <td style="width: 80%;padding-top: 10px;" ><strong>DESCRIBA:</td></strong>
          </tr>    
          <tr>
          <td  align="center" colspan="2">
            <textarea type="text" id="txt_OSTT_C21" maxlength="500" style="width: 1070px; padding-right: 50px; height: 41px;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
          </td>                                                    
        </tr> 
</table>

<table width="95%"   align="center">
  <tr class="camposRepInp"> 
   <td bgcolor="#c0d3c1"  colspan="3" >EVALUACION POSTURAL</td></tr>
       

        <tr class="titulos"> 
          <td align="center" style="width: 20%;padding-top: 10px;" ><strong>9.1 VISTA ANTERIOR: </td></strong></tr>
      
          <tr>
            <td class="estiloImputIzq2" colspan="2">
              <textarea type="text" id="txt_OSTT_C22" maxlength="500" style="width:100%;padding-right: -16px;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
            </td>
          </tr>

          <tr class="titulos"> 
            <td align="center" style="width: 20%;padding-top: 10px;" ><strong>9.2 VISTA LATERAL:</td></strong></tr>
        
            <tr>
              <td class="estiloImputIzq2" colspan="2">
                <textarea type="text" id="txt_OSTT_C23" maxlength="500" style="width:100%;padding-right: -16px;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
              </td>
            </tr>

            <tr class="titulos"> 
              <td align="center" style="width: 20%;padding-top: 10px;" ><strong>9.3 VISTA POSTERIOR:</td></strong></tr>
          
              <tr>
                <td class="estiloImputIzq2" colspan="2">
                  <textarea type="text" id="txt_OSTT_C24" maxlength="500" style="width:100%;padding-right: -16px;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
                </td>
              </tr>
              </table>

              <table width="95%"   align="center">
                <tr class="camposRepInp"> 
                 <td bgcolor="#c0d3c1"  colspan="3" >EVALUACION DE LA MARCHA</td></tr>

                 <tr class="titulos"> 
                <td style="width: 20%;padding-top: 10px;" ><strong>DESCRIPCION:</td></strong></tr>
            
                <tr>
                  <td class="estiloImputIzq2" colspan="2">
                    <textarea type="text" id="txt_OSTT_C25" maxlength="500" style="width:100%;padding-right: -16px;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
                  </td>
                </tr>
                <tr class="titulos"> 
                  <td style="width: 20%;padding-top: 10px;" ><strong>11. AYUDAS ORTESICAS:</td></strong></tr>
              
                  <tr>
                    <td class="estiloImputIzq2" colspan="2">
                      <textarea type="text" id="txt_OSTT_C26" maxlength="500"style="width:100%;padding-right: -16px;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
                    </td>
                  </tr>
                  </table>

                  <table width="95%"   align="center">
                    <tr class="camposRepInp"> 
                     <td bgcolor="#c0d3c1"  colspan="3" >PLAN DE TRATAMIENTO</td></tr>
                     <tr>
                      <td class="estiloImputIzq2" colspan="2">
                        <textarea type="text" id="txt_OSTT_C27" maxlength="500" style="width:100%;padding-right: -16px;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
                      </td>
                    </tr>
                    </table>


                   <table width="95%"   align="center">
                   <tr class="camposRepInp"> 
                   <td bgcolor="#c0d3c1"  colspan="3" >OBSERVACIONES</td></tr>
                   

                  <tr class="estiloImput">
                  <td  class="estiloImputIzq2" colspan="2">
    	            <textarea type="text" id="txt_OSTT_C28" size="4000" maxlength="500" style="width: 1074px; height: 73px;" tabindex="101" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
                  </td> 
                  </tr>          
                 </table>  

 
 






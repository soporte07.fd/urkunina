
<div id="divVentanitaODONTOGRAMA" style="display:block; z-index:19997; top:1px; left:55px; width: 1190px">
	
	<div id="ventanitaHija" style="z-index:9999; position:absolute; top:0px; left:50px; width:1190px;">
		<table width="1190px" align="center" class="fondoTablaAmarillo">
			<tr>
				<td width="100%" align="center" colspan="2">

					<div id="divAcordionTratamientoPorDiente" style="display:BLOCK">
						<jsp:include page="../../hc/hc/divsAcordeonHg.jsp" flush="FALSE">
							<jsp:param name="titulo" value="Descripción del Tratamiento" />
							<jsp:param name="idDiv" value="divTratamientoPorDiente" />
							<jsp:param name="pagina" value="../saludOral/contenidoTratamientoPorDiente.jsp" />
							<jsp:param name="display" value="BLOCK" />
							<jsp:param name="funciones" value="cargarTratamientoPorDiente()" />
						</jsp:include>
					</div>
					<div id="divAcordionTratamientoSeguimiento" style="display:BLOCK">
						<jsp:include page="../../hc/hc/divsAcordeonHg.jsp" flush="FALSE">
							<jsp:param name="titulo" value="Seguimiento" />
							<jsp:param name="idDiv" value="divTratamientoSeguimiento" />
							<jsp:param name="pagina" value="../saludOral/contenidoOdontograma.jsp" />
							<jsp:param name="display" value="NONE" />
							<jsp:param name="funciones" value="ocultar('divControlDePlaca');cargarOdontograma()" />
						</jsp:include>
					</div>
					<div id="divAcordionTratamientoInicial" style="display:BLOCK">
						<jsp:include page="../../hc/hc/divsAcordeonHg.jsp" flush="FALSE">
							<jsp:param name="titulo" value="Odontograma inicial" />
							<jsp:param name="idDiv" value="divTratamientoInicial" />
							<jsp:param name="pagina" value="../saludOral/contenidoOdontogramaInicial.jsp" />
							<jsp:param name="display" value="NONE" />
							<jsp:param name="funciones" value="cargarOdontogramaInicial()" />
						</jsp:include>
					</div>
					<div id="divAcordionIndiceDePlaca" style="display:BLOCK">
						<jsp:include page="../../hc/hc/divsAcordeonHg.jsp" flush="FALSE">
							<jsp:param name="titulo" value="Indice De Placa" />
							<jsp:param name="idDiv" value="divControlDePlaca" />
							<jsp:param name="pagina" value="../saludOral/contenidoControlDePlaca.jsp" />
							<jsp:param name="display" value="NONE" />
							<jsp:param name="funciones" value="cargarControlDePlaca()" />
						</jsp:include>
					</div>
				</td>
        <td align="right" valign="top" >
          <img id="imgCerrar" src="/clinica/utilidades/imagenes/acciones/cerrarVentana.png" title="Cerrar"  onclick="javascript:ocultar('divVentanitaODONTOGRAMA')" width="15" height="15"> 
        </td>        
			</tr>
		</table>

	</div>
</div>

<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="TIPOS FOLIOS DE HISTORIA CLINICA" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>
                        <div style="overflow:auto; width:1050px" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulos" align="center">
                                    <td width="20%">CODIGO</td>
                                    <td width="75%">NOMBRE</td>
                                    <td></td>
                                </tr>
                                <tr class="estiloImput">
                                    <td><input type="text" id="txtIDFormulario"
                                            onkeyup="javascript: this.value= this.value.toUpperCase();" maxlength="4"
                                            style="width:40%"/>
                                    <td><input type="text" id="txtNombreFormulario"
                                            onkeyup="javascript: this.value= this.value.toUpperCase();"
                                            style="width:75%" />
                                    </td>
                                    <td>
                                        <input type="button" class="small button blue" value="BUSCAR"
                                            onclick="buscarHC('listGrillaTiposFormulario', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')" />
                                    </td>
                                </tr>
                            </table>
                            <table id="listGrillaTiposFormulario" class="scroll"></table>
                        </div><!-- div contenido-->

                        <table width="100%">
                            <tr class="estiloImputDer">
                                <td colspan="2" align="CENTER"> .
                                </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                                <td>ID FORMULARIO: </td>
                                <td align="LEFT">
                                    <label id=lblIdFormulario></label>
                                </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                                <td width="25%">NOMBRE FORMULARIO: </td>
                                <td align="LEFT">
                                    <label id="lblNombreFormulario"></label>
                                </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                                <td>ARCHIVO DE TIPIFICACION: </td>
                                <td align="LEFT">
                                    <select id="cmbDocumentoTipificacion">
                                        <option value="">[ Ninguno ]</option>
                                        <%     resultaux.clear();
                                                resultaux=(ArrayList)beanAdmin.combo.cargar(193);	
                                                ComboVO cmbTB; 
                                                for(int k=0;k<resultaux.size();k++){ 
                                                        cmbTB=(ComboVO)resultaux.get(k);
                                            %>
                                        <option value="<%= cmbTB.getId()%>"><%= cmbTB.getDescripcion()%></option>
                                        <%}%>
                                    </select>
                                </td>
                            </tr>
                            <tr class="estiloImputDer">
                                <td colspan="2" align="CENTER">
                                    <input type="button" class="small button blue" value="MODIFICAR"
                                    onclick="modificarCRUD('modificarDocumentoTipificacionFormulario')"/>
                                </td>
                            </tr>
                        </table>

                        <div id="tabsParametrosFolio" style="width:98%; height:auto">
                            <ul>
                                <li>
                                    <a href="#divFoliosTipoAdmision" onclick="tabActivoFoliosParametros('divFoliosTipoAdmision')">
                                        <center>TIPOS DE ADMISION</center>
                                    </a>
                                </li>
                                <li>
                                    <a href="#divFoliosProfesion" onclick="tabActivoFoliosParametros('divFoliosProfesion')">
                                        <center>PROFESIONES</center>
                                    </a>
                                </li>
                            </ul>

                            <div id="divFoliosTipoAdmision">
                                <table width="100%">
                                    <tr class="titulos">
                                        <td width="50%">
                                            <table id="listGrillaTiposCita"></table>
                                        </td>
                                        <td>
                                            <table id="listGrillaFolioProcedimientos"></table>
                                        </td width="50%">
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr class="estiloImputIzq2">
                                                    <td>ESPECIALIDAD</td>
                                                    <td align="LEFT">
                                                        <select id="cmbIdEspecialidad" style="width: 60%;">
                                                            <option value="">[ NINGUNO ]</option>
                                                                <%  resultaux.clear();
                                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(123);	
                                                                    ComboVO cmbES; 
                                                                    for(int k=0;k<resultaux.size();k++){ 
                                                                            cmbES=(ComboVO)resultaux.get(k);
                                                                %>
                                                            <option value="<%= cmbES.getId()%>"><%= cmbES.getDescripcion()%></option>
                                                            <%}%>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="estiloImputIzq2">
                                                    <td>TIPO CITA</td>                                    
                                                    <td align="left">
                                                        <select style="width: 60%;" id="cmbTipoCita" onfocus="cargarTipoCitaSegunEspecialidad(this.id)" style="width:90%">     
                                                    </select>
                                                    </td>
                                                </tr>
                                                <!--<tr class="estiloImputIzq2">
                                                    <td>VIGENTE</td>                                    
                                                    <td align="left">
                                                        <select name="" id="cmbVigenteFolioTipoAdmision">
                                                            <option value="TRUE">SI</option>
                                                            <option value="FALSE">NO</option>
                                                        </select>  
                                                    </select>
                                                    </td>
                                                </tr>-->
                                                <tr class="estiloImputDer">
                                                    <td colspan="2" align="CENTER">
                                                        <input type="button" class="small button blue" value="ADICIONAR"
                                                        onclick="modificarCRUDParametros('adicionarTipoCitaFormulario')"/>                                    
                                                        <input type="button" class="small button blue" value="ELIMINAR"
                                                        onclick="modificarCRUDParametros('eliminarTipoCitaFormulario')"/>                                                                        
                                                        <input type="button" class="small button blue" value="LIMPIAR"
                                                        onclick="limpiarDivEditarJuan('limpiarTipoCitaFormulario');limpiaAtributo('txtTipoCita',0)"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="100%">
                                                <tr class="estiloImputIzq2">
                                                    <td width="20%">PROCEDIMIENTO</td>
                                                    <td align="LEFT">
                                                        <input type="text" id="txtIdProcedimiento" onkeyup="javascript: this.value= this.value.toUpperCase();"  onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimiento',300)" style="width:90%"/>
                                                    </td>
                                                </tr>
                                                <!--<tr class="estiloImputIzq2">
                                                    <td>TIPO CITA</td>                                    
                                                    <td align="left">
                                                        <select name="" id="cmbVigenteFolioProcedimiento">
                                                            <option value="S">SI</option>
                                                            <option value="N">NO</option>
                                                        </select> 
                                                    </td>
                                                </tr>-->
                                                <tr class="estiloImputDer">
                                                    <td colspan="2" align="CENTER">
                                                        <input type="button" class="small button blue" value="ADICIONAR"
                                                        onclick="modificarCRUDParametros('adicionarFolioProcedimiento')"/>                                    
                                                        <input type="button" class="small button blue" value="ELIMINAR"
                                                        onclick="modificarCRUDParametros('eliminarFolioProcedimiento')"/>                                                                        
                                                        <input type="button" class="small button blue" value="LIMPIAR"
                                                        onclick="limpiaAtributo('txtIdProcedimiento', 0)"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
    
                                    </tr>
                                </table>
                            </div>
    
                            <div id="divFoliosProfesion">
                                <table>
                                    <tr class="titulos">
                                        <td>
                                            <table id="listGrillaProfesiones" class="scroll"></table>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">                                
                                    <tr class="estiloImputIzq2" style="width: 90%;">
                                        <td width="25%"">PROFESION</td>
                                        <td align="left" style="width: 90%;">
                                            <select id="cmbProfesion" style="width: 50%;">
                                            <option value="">[ NINGUNO ]</option>
                                            <%     resultaux.clear();
                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(40);	
                                                    ComboVO cmbP; 
                                                    for(int k=0;k<resultaux.size();k++){ 
                                                            cmbP=(ComboVO)resultaux.get(k);
                                                %>
                                            <option value="<%= cmbP.getId()%>"><%= cmbP.getDescripcion()%></option>
                                            <%}%>
                                            </select>
                                        </td>
                                    </tr>                                
                                    <tr class="estiloImputDer">
                                        <td colspan="2" align="CENTER">
                                            <input type="button" class="small button blue" value="ADICIONAR"
                                            onclick="modificarCRUDParametros('adicionarProfesionFormulario')"/>                                    
                                            <input type="button" class="small button blue" value="ELIMINAR"
                                            onclick="modificarCRUDParametros('eliminarProfesionFormulario')"/>                                                                        
                                            <input type="button" class="small button blue" value="LIMPIAR"
                                            onclick="limpiaAtributo('txtProfesion',0);limpiaAtributo('cmbProfesion',0)"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<input type="text" style="display: none;" id="txtTipoCita"/>
<input type="text" style="display: none;" id="txtProfesion"/>
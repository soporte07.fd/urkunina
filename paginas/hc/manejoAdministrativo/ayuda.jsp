    <div id="estilosAyuda">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Patua+One&display=swap" rel="stylesheet">
    <style>
        .contenedor{
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        text-align: center;
        justify-content: space-around;
        align-content: center;
        /* font-family: 'Abril Fatface', cursive; */
        font-family: 'Patua One', cursive;
    }
    .opcion{
        background-color: rgb(228,228,228);
        color: #333;
        border: 1px solid rgb(39, 138, 116);
        margin: 10px;
        padding: 10px;
        border-radius: 8px;
        width: 330px;
        height: 400px;
        playVid();
    }
    .opcion video{
        width: 290px;
        height: 250px;
    }
    .opcion:hover{
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 350px;
        height: 430px;
        box-shadow: 10px 10px rgba(28, 68, 68, 0.4);
        margin: 5px;
        padding: 5px;
        font-size: 20px;
    }
    .opcion video:hover{
        width: 390px;
        height: 290px;
    }
    a{
        text-decoration: none;
        color: darkslategray;
        text-shadow: rgb(239, 243, 243);
    }
    </style>
</div>

<table width="100%" align="center"   border="0" cellspacing="0" cellpadding="1"  >
    <tr>
      <td >
         <!-- AQUI COMIENZA EL TITULO -->	
            <div align="center"  ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                       <jsp:param name="titulo" value="AYUDA" /> 
                </jsp:include>
            </div>	
           <!-- AQUI TERMINA EL TITULO -->
      </td>
    </tr> 
    <tr>
     <td>
        <table >
          <tr>
           <td>
    
      <div>   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
       <div >
        <h1 id="texAyuda" style="text-align: center; font-family: 'Patua One', cursive;">PANEL DE AYUDA AUDIO VISUAL</h1> 
  
    <div id="divAyuda" class="contenedor">
        <div class="opcion" >
            <h3>Introducci&oacuten y logueo</h3>
            <!-- <a href="https://drive.google.com/drive/folders/1IPplRiALYKynd7IGbItdJZIa2mfj6Atd">Descargar Manul PDF</a>          -->
            <video id="myVideo" src="../../../../publicaciones/media/videos/0. Introduccion y logueo.mp4" poster="../../../../publicaciones/media/logoCB.png" controls ></video>
        </div>

        <div class="opcion" >
            <h3>Centro de contactos</h3>
            <!-- <a href="https://drive.google.com/drive/folders/1IPplRiALYKynd7IGbItdJZIa2mfj6Atd">Descargar Manul PDF</a>          -->
            <video id="myVideo" src="../../../../publicaciones/media/videos/1. Centro de contactos.mp4" preload="none" poster="../../../../publicaciones/media/logoCB.png" controls></video>
        </div>

        <div class="opcion" >
            <h3>Creaci&oacuten de Pacientes nuevos por Centro Contacto</h3>
            <!-- <a href="https://drive.google.com/drive/folders/1IPplRiALYKynd7IGbItdJZIa2mfj6Atd">Descargar Manul PDF</a>          -->
            <video src="../../../../publicaciones/media/videos/1.1 Creacion de Pacientes nuevos por Centro Contacto.mp4" preload="none" poster="../../../../publicaciones/media/logoCB.png" controls></video>
        </div>

        <div class="opcion" >
            <h3>Historia Clinica</h3>
            <!-- <a href="https://drive.google.com/drive/folders/1IPplRiALYKynd7IGbItdJZIa2mfj6Atd">Descargar Manul PDF</a>          -->
            <video src="../../../../publicaciones/media/videos/2.  Historia Clinica.mp4" preload="none" poster="../../../../publicaciones/media/logoCB.png" controls></video>
        </div>

        <div class="opcion" >
            <h3>Facturaci&oacuten</h3>
            <!-- <a href="https://drive.google.com/drive/folders/1IPplRiALYKynd7IGbItdJZIa2mfj6Atd">Descargar Manul PDF</a>          -->
            <video src="../../../../publicaciones/media/videos/3. Facturacion.mp4"  preload="none" poster="../../../../publicaciones/media/logoCB.png" controls></video>
        </div>

        <div class="opcion" >
            <h3>App de encuesta</h3>
            <!-- <a href="https://drive.google.com/drive/folders/1IPplRiALYKynd7IGbItdJZIa2mfj6Atd">Descargar Manul PDF</a>          -->
            <video src="../../../../publicaciones/media/videos/4. App de encuesta.mp4" preload="none" poster="../../../../publicaciones/media/logoCB.png" controls></video>
        </div>

    </div>

       </div>
        
      
    
    
     </div><!-- div contenido-->
    
    
         
    
           </td>
          </tr>
         </table>
    
      </td>
    </tr> 
    
    </table>
        
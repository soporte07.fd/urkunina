<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="100%" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td colspan="4">
      <table width="100%">
        <tr class="titulos">
          <td width="10%">Tipo</td>
          <td width="20%">Grupo</td>
          <td width="5%">Tiene</td>
          <td width="5%">Controlada</td>
          <td width="30%">Observaciones</td>
          <td width="2%">&nbsp;</td>
        </tr>
        <tr class="estiloImput">
          <td>
            <select size="1" id="cmbAntecedenteTipo"
              onchange="cargarAntecedentesGrupo(this.id);cargaDatosAntecedentes(this.id)"
              onfocus="cargarAntecendetesFolio(this.id)" style="width:90%"
              tabindex="14" title="53">              
                 </select>                       
              </td>
              <td>
                 <select size="1" id="cmbAntecedenteGrupo" style="width:85%" title="52" tabindex="14">	
   					<option value=""></option>                                                         
                 </select>                                         
              </td>
              <td>
                 <select size="1" id="cmbTiene" style="width:80%" title="54" tabindex="14">
   					<option value="SI">SI</option>
   					<option value="NO">NO</option>    
                 </select>               
              </td> 
              <td>
                 <select size="1" id="cmbControlada" style="width:80%" title="54" tabindex="14">
   					<option value="SI">SI</option>
   					<option value="NO">NO</option>                    
                 </select>               
              </td>              
              <td><input type="text"  value="" id="txtAntecedenteObservaciones"  style="width:90%"  size="200"  maxlength="200"  tabindex="20" onkeypress="return validarKey(event,this.id)" ></td>                    
              <td><input id="btnProcedimiento"  type="button" class="small button blue" value="Adici&oacute;n" onclick="modificarCRUD('listAntecedentes','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="350" /></td>
           </tr>                                                                            
      </table> 
                    
      
      <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
        <tr class="titulos">
          <td>                                    
          <div id="divdatosbasicoscredesa" style="  height:90px; width:100%;  ">
                <table id="listAntecedentes" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
          </div>   
          </td>
        </tr>
      </table>                    
  </td>   
</tr>   
</table>     


  <div id="divVentanitaAntecedentes"  style="position:absolute; display:none; background-color:#E2E1A5; top:310px; left:150px; width:900px; height:90px; z-index:999">
        <table width="100%"  id="CabeceraSublistadoCalendario" border="1"  class="fondoTabla">
           
           <tr class="estiloImput" >
               <td colspan="6" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAntecedentes')" /></td>  
           </tr>     
           <tr class="titulos" >
               <td colspan="2">ID ANTECEDENTE</td>  
               <td colspan="2" >NOMBRE ANTECEDENTE</td>   
               <td colspan="2" >ID EVOLUCION</td>          
           </tr>           
           <tr class="estiloImput" >
               <td colspan="2"><label id="lblId"></label></td>            
               <td colspan="2"><label id="lblNombreElemento"></label></td> 
               <td colspan="2"><label id="lblIdEvolucionAntecedente"></label></td>
           </tr> 
           <tr>
             <td colspan="6"><hr></td>
           </tr>
           <tr class="titulos">
             <td width="20%">TIPO</td>
             <td width="20%">GRUPO</td>
             <td width="10%">SI/NO</td>
             <td width="10%">CONTROLADA</td>
             <td width="30%">
               OBSERVACIONES               
               <img title="VER52" onclick="copiarContenido('txtAntecedenteObservacionesEditar')" src="/clinica/utilidades/imagenes/acciones/Modificar.gif" width="18px" height="18px" align="middle">
             </td>
             <td></td>
           </tr>
           <tr>
             <td>
                <select id="cmbAntecedenteTipoEditar"
                onchange="cargarAntecedentesGrupo(this.id);cargaDatosAntecedentes(this.id)" style="width:90%"
                onfocus="cargarAntecendetesFolio(this.id)"
                tabindex="14" title="53">                
                   </select> 
             </td>
             <td>
                <select  id="cmbAntecedenteGrupoEditar" style="width:85%" title="52" tabindex="14">	
                    <option value=""></option>                                                         
                </select> 
             </td>
             <td>
                <select id="cmbTieneEditar" style="width:95%" title="54" tabindex="14">
                  <option value="" selected></option>
                  <option value="SI">SI</option>
                  <option value="NO">NO</option>    
                </select>               
             </td> 
             <td>
                <select  id="cmbControladaEditar" style="width:95%" title="54" tabindex="14">
                  <option value="" selected></option>
                  <option value="SI">SI</option>
                  <option value="NO">NO</option>                    
                </select>               
             </td> 
             <td>
               <textarea id="txtAntecedenteObservacionesEditar"  style="width:90%" onkeypress="return validarKey(event,this.id)" rows="10"></textarea>
              </td> 
             <td>
                <input id="btnModificarDx" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificarAntecedente', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  tabindex="112"  />  
             </td> 
           </tr>
           <tr>  
               <td colspan="6" align="center">
                <input id="btnEliminarDx" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('eliminarAntecedentes', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  tabindex="112"  />  
              </td>     
           </tr>                
        </table> 
   </div>   
   
   <div id="dialog"></div>
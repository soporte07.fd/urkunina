
<table width="100%" height="300px" cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td valign="top">                          
       <table width="100%">
          <tr   class="estiloImputIzq2"> 
            <td width="25%">BODEGA:&nbsp;&nbsp;
                <select size="1" id="cmbIdBodega" style="width:70%" tabindex="2" onfocus="comboBodegasConsumos(this.id)" onchange="buscarHistoria('listHojaGastosBodegas')">
                  <option value=""></option> 
                </select>                
            </td>                      
            <td>ID DOCUMENTO:<label id="lblIdDoc"></label> </td>
            <td> ESTADO=<label id="lblIdEstado"></label></td>                                             
            <td>TIPO:</td>
            <td width="28%"><select id="cmbIdTipoDocumento" style="width:80%" tabindex="100">
                    <option value="21">EGRESO POR BODEGA DE CONSUMO</option>
                </select>
                <input type="hidden" id="txtNaturaleza" value ="S" />
            </td>			
          </tr>
          <tr class="titulos">
             <td colspan="5" align="center">			   
			   <input class="small button blue" value="Buscar Documento" title="BTT7" type="button" onclick="buscarSuministros('listGrillaDocumentosBodegaConsumo')"   />  
               <input class="small button blue" value="Crear Documento"  title="BG61"  type="button" onclick="guardarYtraerDatoAlListado('nuevoDocumentoInventarioBodegaConsumo')" />               
               <input class="small button blue" value="Cerrar Documento" title="BT955" type="button" onClick="modificarCRUD('cerrarDocumentoBodegaConsumo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  /> 
			   
             </td>
          </tr>                                               
      </table>
      <table id="listGrillaDocumentosBodegaConsumo" class="scroll"></table>    
      <table id="listGrillaElementosBodegaConsumo" class="scroll"></table>    
    </td>  
  </tr>  
</table> 

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="titulosCentrados">
    <td colspan="3">TRANSACCIONES.
<!--		<input type="button" value="+" title="btn_de23" onClick=""/> -->
    </td>   
  </tr>  
  <tr>
    <td>
      <table width="100%">            	
          <tr   class="estiloImputIzq2">    
			<td><div style="display:none;">
				<table><tr>
				<td width="33%" align="right">ID TRANSACCION:<label id="lblIdTransaccion"></label></td>
				<td width="33%">NATURALEZA:<label id="lblNaturaleza">S</label></td> 
				<td width="33%" colspan="2"></td>            
				</tr></table>
				</div>
			</td>
          </tr>								                                                
                                                   
           <tr class="estiloImputIzq2"> 
              <td colspan="4" align="center">
                  <table  width="100%"  cellpadding="0"   align="center">
                      <tr class="estiloImputIzq2"> 
                          <td width="8%" >Articulo: </td>
                          <td width="20%" colspan="3"> 	              	
                             <input type="text" id="txtIdArticulo"  style="width:90%" tabindex="100"  />	
                             <img width="18px" height="18px" align="middle" src="/clinica/utilidades/imagenes/acciones/buscar.png" onclick="traerVentanitaArticulo('txtIdArticulo','11')" title="VEN234">	
                             <div id="divParaVentanitaArticulo"></div>		  
                          </td>  
                          <td width="7%">LOTE: </td>
                          <td width="7%">
                              <select id="cmbIdLote" style="width:95%" tabindex="100" onfocus="cargarLote()" onblur="traerExistenciaLote()">
                                  <option value=""></option>
                              </select>
							  <input type="hidden" id="txtFV" />
                          </td> 
                          <td width="7%">Existencias: </td>
                          <td width="7%"><label id="lblExistencias">0</label></td> 
						  <td width="7%">Cantidad</td>
                          <td width="7%"><input type="text" id="txtCantidad" style="width:90%" tabindex="100" onKeyPress="javascript:return soloNumeros(event)" onkeyup="calcularImpuestoSalida()"/></td>
						  <td width="7%">Serial :</td>
					      <td width="10%"><input type="text" id="txtSerial" style="width:70%" tabindex="100"/></td>
                      </tr>
                      <tr><td colspan="10">
					  <div style="display:none;">
					  <table>
                      <tr class="estiloImputIzq2">  
                          <td width="10%">Valor Unitario:</td>
                          <td width="7%"><label id="lblValorUnitario">0</label></td>
                          <td width="10%">IVA VENTA:</td>
                          <td width="12%"><label id="lblIva">0</label>
                          </td>
                          <td width="12%">Impuesto IVA $:</td>
                          <td> <label id="lblValorImpuesto"></label>
                          </td>
                          
                      </tr>
                      
                      <tr class="estiloImputIzq2" >  
                          <td colspan="2">SubTotal $:</td>
                          <td colspan="2"><label id="lblSubTotal">0</label></td>
                          <td colspan="2">Total $:</td>
                          <td colspan="2"> <label id="lblTotal">0</label></td>
                      </tr>
					  </table>
					  </div>
					  
					  </td></tr>
                  </table>
              </td>
              
                                                        
           </tr>			 		  				
           
           <tr>
             <td colspan="4" align="center">
               <input id="btn_limpia" title="BTP8"class="small button blue" type="button" value="Limpiar"  onclick="limpiarDivEditarJuan('limpCamposTransaccion')">              
               <input id="btn_crea" title="BTP90" class="small button blue" type="button"  value="Crear"  onclick="modificarCRUD('crearTransaccion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" >                               
               <input name="btn_modifica" title="BTP97" type="button" class="small button blue" value="Modificar" onclick="modificarCRUD('modificaTransaccion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  
               <input name="btn_elimina" title="BTP93" type="button" class="small button blue" value="Eliminar" onclick="modificarCRUD('eliminaTransaccion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />          
             </td>
           </tr>                                                     
      </table>
      <table id="listGrillaTransaccionDocumento" class="scroll"></table> 	  
    </td>
  </tr>
 </table>
<input type="hidden" id="txtBanderaDevolucion" value ="NO" />
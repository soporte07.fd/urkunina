

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td>                          
       <table width="100%">
          <tr   class="estiloImputIzq2"> 
            <td>
                <select id="cmbIdBodega" style="width:80%" tabindex="100">
                    <option value="1">Bodega Pricipal</option>
                </select>
            </td>                      
            <td>ID DOCUMENTO:<label id="lblIdDoc"></label> </td>
            <td> ESTADO=<label id="lblIdEstado"></label></td>                                             
            <td>TIPO:</td>
            <td><select id="cmbIdTipoDocumento" style="width:80%" tabindex="100">
                    <option value="7">EGRESO POR CONSUMO</option>
                </select>
                <input type="hidden" id="txtNaturaleza" value ="S" />
            </td>			  
          </tr>
          <tr class="titulos">
             <td colspan="5" align="center">
               <input class="small button blue" value="Crear Documento" title="BG65"  type="button" onclick="guardarYtraerDatoAlListado('nuevoDocumentoInventarioProgramacion')" >
               <input class="small button blue" value="Cerrar"  title="BT955" type="button" onClick="guardarYtraerDatoAlListado('verificaTransaccionesInventario')" >                 
             </td>
          </tr>                                               
      </table>
      <table id="listGrillaDocumentosBodegaProgramacion" class="scroll"></table>    
    </td>  
  </tr>  
</table> 

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="titulosCentrados">
    <td colspan="3">CREAR TRANSACCION
    </td>   
  </tr>  
  <tr>
    <td>
      <table width="100%">            	
          <tr   class="estiloImputIzq2">                         
            <td width="8%" align="right">ID TRANSACCION:</td>
            <td width="8%">  
                  <label id="lblIdTransaccion"></label>			  					
            </td> 
            <td width="8%" align="right">NATURALEZA:</td>
            <td width="8%">  
                  <label id="lblNaturaleza"></label>			  					
            </td>                                             
          </tr>								                                                
                                                   
           <tr class="estiloImputIzq2"> 
              <td colspan="4" align="center">
                  <table  width="100%"  cellpadding="0"   align="center">
                      <tr class="estiloImputIzq2"> 
                          <td width="8%" >Articulo: </td>
                          <td width="20%" colspan="3"> 	              	
                            <input type="text" id="txtIdArticulo" onfocus="llamarAutocomIdDescripcionConDato('txtIdArticulo',95)" style="width:95%" tabindex="100" onblur="traerValorUnitario()" />				  
                          </td>  
                          <td width="7%">LOTE: </td>
                          <td width="7%">
                              <select id="cmbIdLote" style="width:95%" tabindex="100" onfocus="cargarLote()" onblur="traerExistenciaLote()">
                                  <option value=""></option>
                              </select>
                          </td> 
                          <td width="7%">Existencias: </td>
                          <td width="7%"><label id="lblExistencias">0</label></td> 
                      </tr>
                      
                      <tr class="estiloImputIzq2">  
                          <td>Valor Unitario:</td>
                          <td width="7%"><label id="lblValorUnitario">0</label></td>
                          <td width="7%">IVA VENTA:</td>
                          <td width="12%">
                              <select id="cmbIva" tabindex="100" style="width:30%" onfocus="cargarIva()"  >
                                  <option value=""></option>
                                  
                              </select> %
                          </td>
                          <td>Impuesto IVA $:</td>
                          <td> <label id="lblValorImpuesto"></label>
                          </td>
                          <td>Cantidad</td>
                          <td><input type="text" id="txtCantidad" style="width:90%" tabindex="100" onKeyPress="javascript:return teclearsoloNumerosYPunto(event)" onblur="calcularImpuestoSalida()"/></td>
                      </tr>
                      
                      <tr class="estiloImputIzq2" >  
                          <td colspan="2">SubTotal $:</td>
                          <td colspan="2"><label id="lblSubTotal">0</label></td>
                          <td colspan="2">Total $:</td>
                          <td colspan="2"> <label id="lblTotal">0</label></td>
                      </tr>
                  </table>
              </td>
              
                                                        
           </tr>			 		  				
           
           <tr>
             <td colspan="4" align="center">
               <input id="btn_limpia" title="btn_78TR"class="small button blue" type="button" value="Limpiar"  onclick="limpiarDivEditarJuan('limpCamposTransaccion')">              
               <input id="btn_crea" title="btn_TR70" class="small button blue" type="button"  value="Crear"  onclick="modificarCRUD('crearTransaccion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" >                               
               <input name="btn_modifica" title="btn_TR75" type="button" class="small button blue" value="Modificar" onclick="modificarCRUD('modificaTransaccion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  
               <input name="btn_elimina" title="btn_tr8" type="button" class="small button blue" value="Eliminar" onclick="modificarCRUD('eliminaTransaccion','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />          
             </td>
           </tr>                                                     
      </table>
      <table id="listGrillaTransaccionDocumento" class="scroll"></table> 	  
    </td>
  </tr>
 </table>



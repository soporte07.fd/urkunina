<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>
<!--
<label id="lblNomAdministradora2" style="font-size:11px; color:#F00" disabled="disabled"></label>
<img src="/clinica/utilidades/imagenes/acciones/search.png" title="IMGp88-cargarProcedimientosHistoricos"
     onclick="cargarProcedimientosHistoricos()" width="14px" height="14px"/>
-->
<div id="tabsPrincipalAdministrativo" style="width:98%; height:auto">
    <div id="divParaVentanitaCondicion1"></div>
    <ul>                
        <li id="idcons">
            <a  href="#divConsentimientos" onclick="TabActivoConductaTratamiento('divConsentimientos')">
                <center>CONSENTIMIENTOS</center>
            </a>
        </li>
        <li id="idadmpac">
            <a href="#divAdministracionPaciente" onclick="TabActivoConductaTratamiento('divAdministracionPaciente')">
                <center>GESTI&Oacute;N ADMINISTRATIVA</center>
            </a>
        </li>
        <!--<li>
            <a href="#divIncapacidad" onclick="TabActivoConductaTratamiento('listIncapacidad'); buscarParametros('listIncapacidad');">
                <center>INCAPACIDAD</center>
            </a>
        </li>-->
    </ul>
    
    <div id="divConsentimientos">
        <table width="100%">
            <tr class="titulosListaEspera">                
                <td align="CENTER">
                    Tipo
                    <select style="width:40%" id="cmbTipoConsentimiento">
                        <option value=""></option>
                        <%     resultaux.clear();
                            resultaux = (ArrayList) beanAdmin.combo.cargar(151);
                            ComboVO opt;
                            for (int k = 0; k < resultaux.size(); k++) {
                                opt = (ComboVO) resultaux.get(k);
                        %>
                        <option value="<%= opt.getId()%>" title="<%= opt.getTitle()%>">
                            <%= opt.getDescripcion()%></option>
                            <%}%>
                    </select>
                </td>
                <td>
                    <input type="button" class="small button blue" id="btnCrearConsentimiento" value="CREAR" onclick="modificarCRUD('crearConsentimiento')" />
                </td>
            </tr>
            <tr class="titulos">
                <td colspan="2">
                    <table id="listConsentimientosPaciente"></table>
                </td>
            </tr>
        </table>
        
        <div id="disentimiento" style="display: none;">
        <table width="100%" style="background-color: #E8E8E8;">
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px" width="30%">
                    1. NOMBRE DE PROCEDIMIENTO(S) NO APROBADO(S):
                </td>
                <td>
                    <textarea type="text" id="txtTexto1" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
            </tr>
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px">
                    2. OBJETIVOS DE PROCEDIMIENTO(S) NO APROBADO(S):
                </td>
                <td>
                    <textarea type="text" id="txtTexto2" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
            </tr>
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px">
                    3. DESCRIPCIÓN DE PROCEDIMIENTO(S) NO APROBADO(S):
                </td>
                <td>
                    <textarea type="text" id="txtTexto3" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
            </tr>
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px">
                    4. BENEFICIOS DE PROCEDIMIENTO(S) NO APROBADO(S):
                </td>
                <td>
                    <textarea type="text" id="txtTexto4" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')"></textarea>
                </td>
            </tr>
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px">
                    5. RIESGOS DE PROCEDIMIENTO(S) NO APROBADO(S):
                </td>
                <td>
                    <textarea type="text" id="txtTexto5" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
            </tr>
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px">
                    6. CONSECUENCIA DE NO ACEPTAR EL/LOS PROCEDIMIENTO(S):
                </td>
                <td>
                    <textarea type="text" id="txtTexto6" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
            </tr>
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px">
                    7. RAZONES DE NEGACI&OtildeN:
                </td>
                <td>
                    <textarea type="text" id="txtTexto7" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
                <td><input hidden id="txtIdCon"/></td>
            </tr>
        </table>        
    </div>   
    </div>

    <div id="divAdministracionPaciente">
        <table width="100%" style="background-color: #E8E8E8;">
            <tr class="titulosListaEspera">
                <td width="15%">Indicacion Medica</td>
                <td width="15%" id="elementoAltaMedica" hidden>Tipo de Alta</td>
                <td width="20%">Observaciones</td>
                <td width="5%"></td>
                <td width="10%" align="left">Ver Ordenes Hist&oacute;ricas</td>
            </tr>
            <tr class="estiloImput">
                <td>
                    <select id="cmbOrdenenesAdmisionURG" style="width: 95%;" onchange="reaccionAEvento(this.id);">
                        <option></option>
                    </select>
                </td>
                <td id="elementoAltaMedica" hidden>
                    <select id="cmbTipoAlta" style="width: 95%;" onchange="reaccionAEvento(this.id)">
                    </select>
                </td>
                <td>
                    <textarea id="txtObservacionOrdenURG" placeholder="Observaciones" style="width: 95%;" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
                <td align="CNETER">
                    <input type="button" class="small button blue" value="ENVIAR" onclick="modificarCRUD('enviarOrdenMedicaURG')"/>
                </td>
                <td> 
                    <label>
                        <div class="onoffswitch">
                            <input type="checkbox" checked class="onoffswitch-checkbox" id="sw_historicos" onchange="buscarHC('listOrdenesPacienteURG', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')">
                            <label class="onoffswitch-label" for="sw_historicos">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </label>

                </td>
            </tr>
            <tr id="trInfoMuerte" hidden>
                <td colspan="5">
                    <table width="100%">
                        <tr class="titulosListaEspera">
                            <td width="50%">Causa B&aacute;sica de Muerte</td>
                            <td width="50%">Fecha y Hora de Muerte</td>
                        </tr>
                        <tr class="estiloImput">
                            <td>
                                <input type="text" id="txtIdDxMuerte" style="width: 90%;" readonly>
                                <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
                                     onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdDxMuerte', '5')"
                                     src="/clinica/utilidades/imagenes/acciones/buscar.png">
                            </td>
                            <td>
                                <table width="100%">
                                    <tr class="titulos">
                                        <td width="15%">Fecha y Hora Auto.</td>
                                        <td width="35%">Fecha</td>
                                        <td width="35%">Hora</td>
                                    </tr>
                                    <tr>
                                        <td align="CENTER">
                                            <label>
                                                <div class="onoffswitch">
                                                    <input type="checkbox" class="onoffswitch-checkbox" id="sw_fecha_actual" onchange="reaccionAEvento(this.id)">
                                                    <label class="onoffswitch-label" for="sw_fecha_actual">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </label>
                                        </td>
                                        <td align="center">
                                            <input type="text" id="txtFechaMuerte">
                                        </td>
                                        <td align="center">
                                            <input type="number" min="1" max="12" id="txtHoraMuerte" style="width: 30%;">
                                            <input type="number" min="0" max="59" id="txtMinutoMuerte" style="width: 30%;">
                                            <select id="cmbPeriodoMuerte" style="width: 20%;">
                                                <option value=""></option>
                                                <option value="AM">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="titulos">
                <td colspan="5">
                    <table id="listOrdenesPacienteURG" width="100%"></table>
                </td>
            </tr>
        </table>
    </div>   
</div>
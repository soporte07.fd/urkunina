<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> 
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="100%" cellpadding="0" cellspacing="0" align="center">

    <tr class="titulosListaEspera">
        <td>Tipo</td>                  
        <td>Admisiones</td>                  
        <td>Estado</td>
        <td>Trazabilidad</td>
        <td></td>                  
    </tr>

    <tr class="estiloImput">
        <td>
            <select size="1" id="cmbClaseProcedimiento" style="width:50%" onchange="buscarHistoria('listOrdenes')">
                <option value=""></option>
                <%     resultaux.clear();
                    resultaux = (ArrayList) beanAdmin.combo.cargar(8);
                    ComboVO cmbCla;
                    for (int k = 0; k < resultaux.size(); k++) {
                        cmbCla = (ComboVO) resultaux.get(k);%>
                <option value="<%= cmbCla.getId()%>"><%=cmbCla.getId() + " " + cmbCla.getDescripcion()%>  </option>  <%}%>						
            </select>
        </td>
        <td >
            <select id="cmbAdmisionesOrdenes" style="width:50%" onchange="buscarHistoria('listOrdenes')" >
                <option value="0">Admision Actual</option>
                <option value="1">Admisiones Anteriores</option>                                                        
            </select> 
        </td>

        <td>
            <select size="1" id="cmbEstadoOrdenes" style="width:50%" onchange="buscarHistoria('listOrdenes')">
                <option value=""></option>
                <%     resultaux.clear();
                    resultaux = (ArrayList) beanAdmin.combo.cargar(1);
                    ComboVO cmbEst;
                    for (int k = 0; k < resultaux.size(); k++) {
                        cmbEst = (ComboVO) resultaux.get(k);%>
                <option value="<%= cmbEst.getId()%>"><%=cmbEst.getId() + " " + cmbEst.getDescripcion()%>  </option>  <%}%>						
            </select>
        </td>
        <td >
            <select id="cmbTrazabilidad" style="width:50%" onchange="buscarHistoria('listOrdenes')" >
                <option value=""></option>
                <option value="S">Con Trazabilidad</option>
                <option value="N">Sin Trazabilidad</option>                                                        
            </select> 
        </td>
        <td>
            <input type="button" class="small button blue" value="BUSCAR"  onclick="buscarHistoria('listOrdenes')"   />     
        </td>
    </tr>

    <tr>
        <td colspan="5">
            <table width="100%" id="idTablaProcedimientosDetalle" align="center">
                <tr class="titulos">
                    <td>
                        <table id="listOrdenes" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</table>


<div id="tabsPrincipalOrdenes" style="width:98%; height: auto; ">
    <ul>
        <li>
            <a href="#divOrdenesSubidas" onclick="TabActivoConductaTratamiento('divOrdenesSubidas')">
                <center>DETALLE DE LOS RESULTADOS</center>
            </a>
        </li> 
        <!--li>
            <a href="#divInterpretacion" onclick="TabActivoConductaTratamiento('divInterpretacion')">
                <center>INTERPRETACIONES HISTORICAS</center>
            </a>
        </li--> 
        <li>
            <a href="#divNuevaInterpretacion" onclick="TabActivoConductaTratamiento('divNuevaInterpretacion')">
                <center>HACER UNA INTERPRETACION</center>
            </a>
        </li> 

        <li>
            <a href="#divResultadosExternos" onclick="TabActivoConductaTratamiento('divResultadosExternos')">
                <center>INGRESAR RESULTADOS EXTERNOS</center>
            </a>
        </li> 
    </ul>

    <div id="divOrdenesSubidas" >
        <table width="100%" style="background-color: #E8E8E8;">
            <tr>
                <td class="titulos">
                    <table id="listOrdenesArchivo" ></table>
                </td>
            </tr>
        </table>        
    </div> 

    <!--div id="divInterpretacion">
        <table width="100%" style="background-color: #E8E8E8;">
           
        </table>  
    </div-->

    <div id="divNuevaInterpretacion">
        
        <table width="100%" style="background-color: #E8E8E8;">
            <tr class="titulosListaEspera">
                <td width="5%">ID ORDEN</td>     
                <td width="8%">FECHA ORDENADO</td>
                <td width="10%">PROFESIONAL ORDENA</td>                
                <td width="14%">EMPRESA QUE REALIZA EL EXAMEN</td>                
                <td width="8%">ESTADO</td>                 
            </tr>
            <tr class="estiloImput">                
                <td><label id="lblIdOrden"></label></td>
                <td><label id="lblFechaOrden"></label></td>               
                <td><label id="lblProfesionalOrden"></label></td>               
                <td><label id="lblEmpresaOrden"></label></td>               
                <td><label id="lblIdEstadoOrden"></label>-<label id="lblNomEstadoOrden"></label></td>
                
            </tr>

            <tr class="estiloImput" >
                <td colspan="6"><textarea id="txtInterpretacion" style="width:95%;font-size: 14px;" maxlength="5000" tabindex="200" rows="6" onkeypress="return validarKey(event, this.id)" onblur="v28(this.value, this.id);" ></textarea></td> 
            </tr>

<tr class="estiloImput">
    <td colspan="6">
        <input id="btnAdicionarInterpretacion" type="button" class="small button blue" value="INTERPRETAR" onclick="modificarCRUD('interpretarProcedimiento')"/>
        <input id="btnModificarInterpretacion" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificarInterpretacionProcedimiento')"/>
    </td>
</tr>

<tr >
    <td class="titulos" colspan="6">
        <table id="listInterpretaciones" ></table>
    </td>
</tr>

</table>

    </div>

    <div id="divResultadosExternos">
        <div id="divParaVentanitaResultadosExternos"></div>
        <table width="100%" >
            <tr class="titulosListaEspera">
                <td>ESPECIALIDAD</td>
                <td></td>
            </tr>
            <tr class="estiloImput">
                <td><input id="txtIdProcedimientoResultado"
                           onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimientoResultado', 646)"
                           style="width:90%" >
                    <img width="18px" height="18px" align="middle" 
                         id="idLupitaVentanitaResultadosExternos"
                         onclick="traerVentanita(this.id, '', 'divParaVentanitaResultadosExternos', 'txtIdProcedimientoResultado', '30')"
                         src="/clinica/utilidades/imagenes/acciones/buscar.png">
                </td>
                <td>
                    <input type="button" class="small button blue" value="ADICIONAR" onclick="modificarCRUD('insertarProcedimientoResultadosExternos')" />
                </td>
            </tr>
            <tr>
                <td class="titulos" colspan="2">
                    <table id="listaProcedimientoResultadosExternos" ></table>
                </td>
            </tr>
            <tr>
                <td class="titulos" colspan="2">
                    <table id="listaAnalitosResultadosExternos" ></table>
                </td>
            </tr>
            
        </table>
    </div>
</div>

<input type="hidden" id="txtIdProcedimientoProgramacion" /> 
<input type="hidden" id="txtIdResultadoProgramacion" /> 


<div id="divVentanitaResultadoPlantilla"  style="display:none; z-index:1007; top:1px; left:211px;">
    <div class="transParencia" style="z-index:1008; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
       </div>   
     <div  id="ventanitaHija" style="z-index:1009; position:fixed; top:200px; left:160px; width:70%">  
         <table width="1000" height="90" class="fondoTabla">
          <tr class="estiloImput" >
              <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaResultadoPlantilla')" /></td>  
              <td>&nbsp;</td>                   
              <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaResultadoPlantilla')" /></td>  
          </tr> 
          
             <tr>
                <td colspan="3" class="titulos">
                        <table id="listResultadoPlantilla" ></table>
                        <div id="pagerPlantilla" ></div>
                    </td>
            </tr>
        </table>

    
</div>  
</div> 
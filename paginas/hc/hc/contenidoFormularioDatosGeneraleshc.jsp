<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->
<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<div id="divMotivoEnfermedad">
	<table width="100%" cellpadding="0" cellspacing="0" align="center">
		<tr class="titulos">
			<td>Motivo de la Consulta:</td>
			<td id="lblEnfermedadActual">Enfermedad actual</td>
			<td></td>
		</tr>
		<tr class="estiloImput">
			<td width="30%">
				<textarea type="text" rows="5" id="txtMotivoConsulta" size="4000" maxlength="4000" style="width:95%"
					tabindex="101" onblur="v28(this.value,this.id); modificarCRUD('motivoEnfermedad'); "
					onkeypress="return validarKey(event,this.id)"> </textarea>
			</td>
			<td width="30%">
				<textarea type="text" rows="5" id="txtEnfermedadActual" size="4000" maxlength="4000" style="width:95%"
					tabindex="101" onblur="v28(this.value,this.id); modificarCRUD('motivoEnfermedad'); "
					onkeypress="return validarKey(event,this.id)"> </textarea>
			</td>
			<td width="30%">
				<table width="100%" align="center">
					<tr class="titulos">
						<td width="20%" align="right">Medico remite:</td>
						<td width="80%" align="left">
							<input type="text" size="1" id="input_med" style="width:90%" title="medico que remite"
							onblur="modificarCRUD('motivoEnfermedad','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')"
							tabindex="14">
						</td>
					</tr>

					<tr class="titulos">
						<td colspan="2">
							<table><tr>
								<td width="80%" align="right">s&iacute;ntomatico piel</td>
								<td width="20%" align="left">
									<select size="1" id="cmb_SintomaticoPiel" style="width:90%" title="32"
										onblur="modificarCRUD('motivoEnfermedad','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')"
										tabindex="14">
										<option value="-" selected=""> </option>
										<option value="NO">NO</option>
										<option value="SI">SI</option>
									</select>
								</td>
							</tr>
							<tr class="titulos">
								<td align="right">s&iacute;ntomatico respiratorio</td>
								<td align="left">
									<select size="1" id="cmb_SintomaticoRespira" style="width:90%" title="32"
										onblur="modificarCRUD('motivoEnfermedad','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')"
										tabindex="14">
										<option value="-" selected=""> </option>
										<option value="NO">NO</option>
										<option value="SI">SI</option>
									</select>
								</td>
							</tr></table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<hr>
			</td>
		</tr>
		<tr style="background-color: whitesmoke;">
			<td colspan="3">
				<table width="100%">
					<tr class="titulos">
						<th width="30%">Diagn&oacute;stico de Ingreso</th>
						<th width="30%">Causa Externa</th>
						<th width="30%">Finalidad</th> 
					</tr>
					<tr class="estiloImput">
						<td>
							<input type="text" value="" id="txtIdDxIngreso" style="width: 90%;" readonly>
							<img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
								onclick="traerVentanitaFuncionesHc(this.id, 'actualizarDxIngresoHC', 'divParaVentanita','txtIdDxIngreso','5')"
								src="/clinica/utilidades/imagenes/acciones/buscar.png">
						</td>
						<td>
							<select id="cmbCausaExterna" style="width: 95%;"
								onblur="modificarCRUD('actualizarCausaExternaHc')">
								<option value=""></option>
                                    <%     resultaux.clear();
                                           resultaux=(ArrayList)beanAdmin.combo.cargar(162);	
                                           ComboVO cmbLa; 
                                           for(int k=0;k<resultaux.size();k++){ 
                                                 cmbLa=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbLa.getId()%>" title="<%= cmbLa.getTitle()%>">
                                        <%= cmbLa.getDescripcion()%></option>
                                <%}%>
							</select>
						</td>
						<td>
							<select id="cmbFinalidadConsulta" style="width: 95%;"
								onblur="modificarCRUD('actualizarFinalidadHc')">
								<option value=""></option>
                                    <%     resultaux.clear();
                                           resultaux=(ArrayList)beanAdmin.combo.cargar(609);	
                                           for(int k=0;k<resultaux.size();k++){ 
                                                 cmbLa=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbLa.getId()%>" title="<%= cmbLa.getTitle()%>">
                                        <%= cmbLa.getDescripcion()%></option>
                                <%}%>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<!--      <input type="hidden"  id="txtVistaPrevia" value="NO" width="70px" /> -->
<input type="hidden" id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden" id="txtTipoDocumentoVistaPrevia" value="" />
<input type="hidden" id="lblTituloDocumentoVistaPrevia" value="" />
<input type="hidden" id="lblIdAdmisionVistaPrevia" value="" />
<input type="hidden" id="lblEstadoDocumentoVistaPrevia" value="" />
 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
    

<% 	beanAdmin.setCn(beanSession.getCn()); 
     
    ArrayList resultaux=new ArrayList();
%>

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td colspan="4">
      <table width="100%">
            <tr class="titulos"> 
              <td width="5%">Sistolica</td>                               
              <td width="5%">Diastolica</td>
              <td width="6%">F. Cardiaca</td>                               
              <td width="6%">F. Respiratoria</td>
              <td width="8%">Temperatura</td>                                                                 
              <td width="15%">Saturacion de Oxigeno</td>                                                                 
              <td width="20%" >Hora Minuto - Monitoreo</td>                                                       
              <td width="30%" >Observaciones</td>                                                       
            </tr>		
            <tr class="estiloImput"> 
              <td><input type="text" id="txtMSistolica" style="width:30px"   onKeyPress="javascript:return teclearsoloDigitos(event); "  size="4"  maxlength="4"  tabindex="501"></td>     
              <td><input type="text" id="txtMDiastolica" style="width:30px"   onKeyPress="javascript:return teclearsoloDigitos(event); " size="4"  maxlength="4"  tabindex="502"></td>     
              <td><input type="text" id="txtFCardiaca" style="width:30px"   onKeyPress="javascript:return teclearsoloDigitos(event); " size="4"  maxlength="4"  tabindex="504"></td>     
              <td><input type="text" id="txtFRespiracion" style="width:30px"   onKeyPress="javascript:return teclearsoloDigitos(event); " size="4"  maxlength="4"  tabindex="504"></td>     
              <td><input type="text" id="txtTemperaturaMH" style="width:30px"  onKeyPress="javascript:return teclearsoloFloat(event); "  size="5"  maxlength="4"  tabindex="505">&#176;C</td>                  
              <td><input type="text" id="txtSaturacionDeOxigeno" style="width:30px"  onKeyPress="javascript:return teclearsoloFloat(event); "  size="5"  maxlength="4"  tabindex="506">%</td>                  
              <td>
                <select size="1" id="cmbHoraMHH" style="width:20%" title="52" tabindex="506">	                                        
                      <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(3);	
                               ComboVO cmbhora;
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbhora=(ComboVO)resultaux.get(k);
                        %>
                      <option value="<%= cmbhora.getId()%>" title="<%= cmbhora.getTitle()%>"><%=cmbhora.getDescripcion()%></option>
                              <%}%>    
                   </select>  
				   &nbsp; :       
					<select size="1" id="cmbMinutoMHH" style="width:20%" title="38" tabindex="507" >	                                        
							  <%     resultaux.clear();
									 resultaux=(ArrayList)beanAdmin.combo.cargar(103);	
									 ComboVO cmb1;
									 for(int k=0;k<resultaux.size();k++){ 
										   cmb1=(ComboVO)resultaux.get(k);
							  %>
							<option value="<%= cmb1.getId()%>" title="<%= cmb1.getTitle()%>"><%=cmb1.getDescripcion()%></option>
									<%}%>						
					 </select>				   
              </td>             
              <td>              	
                <textarea id="txtMHObservacion" style="width:90%" tabindex="508" maxlength="2000"></textarea>
              </td>  
              <td><input id="btnProcedimiento" title="btn_amhh"  type="button" class="small button blue" value="Adici&oacute;n" onclick="modificarCRUD('crearMHH','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="509" /></td>
           </tr>                                                                            
      </table> 
                    
      
      <table  width="100%" align="center" >                          
        <tr class="titulos">
          <td>                                    
                <table id="listMonitorizacionHH" class="scroll" align="center"></table>
          </td>
        </tr>
      </table>                    
  </td>   
</tr>   
</table>     


  <div id="divVentanitaMHH"  style="position:absolute; display:none; background-color:#E2E1A5; top:450px; left:150px; width:600px; height:90px; z-index:999">
        <table width="100%"  border="1"  class="fondoTabla">
           
           <tr class="estiloImput" >
               <td colspan="3" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaMHH')" /></td>  
           <tr>     
           <tr class="titulos" >
               <td width="15%" >ID </td>  
               <td width="15%" >ID EVO</td>                 
               <td width="70%" >NOMBRE</td>           
           <tr>           
           <tr class="estiloImput" >
               <td ><label id="lblIdMHH"></label></td>            
               <td ><label id="lblIdElemento"></label></td>
               <td ><label id="lblNombreElemento"></label></td> 
           <tr> 
           <tr>  
               <td colspan="3" align="center"><div id="divBotonGuardar" style="width:80%; display:block" > 
                <input id="btnEliminarMHH" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('eliminarMHHListado', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  tabindex="112"  />  
               </div></td>     
           <tr>                
        </table> 
   </div>            

     
     <table width="100%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
       <tr class="titulosIzquierda" >
           <td colspan="3">PROCEDIMIENTOS NO POS</td>               
       </tr>       
       <tr class="estiloImput">  
           <td colspan="3">                                                
              <table width="100%" >
                <tr class="titulos">
                  <td colspan="3">RESUMEN DE LA ENFERMEDAD ACTUAL</td>
				  <td colspan="1">TRATAMIENTO</td>                  
                </tr>                                  
                <tr class="estiloImput" >
                  <td colspan="3">
                  <textarea type="text" id="txt_ResumenHC"  size="4000"  maxlength="4000" style="width:95%" tabindex="100" > </textarea>
                  </td>
                  <td colspan="1">
                     <select id="txt_Tratamiento" style="width:30%" title="32" tabindex="14">	  
                        <option value="AMBULATORIO">AMBULATORIO</option>
                        <option value="HOSPITALARIO">HOSPITALARIO</option>
                        <option value="URGENTE">URGENTE</option>                                                                                                                        
                     </select>                  
                  </td>
                </tr>  
                <tr class="titulos">
                  <td width="50%" colspan="4">*NO EXISTE ALTERNATIVA EN EL POS:
                  </td>
                </tr>                  
                <tr class="estiloImput" >
                  <td colspan="4">
                  <input type="text" id="txt_AlternativaPos" style="width:90%;" maxlength="500"/>
                  </td>
                </tr>  

 
                <tr class="titulos">
                  <td width="50%" colspan="4">*POSIBILIDAD TERAPEUTICA POS:
                  </td>
                </tr>                  
                <tr class="estiloImput" >
                  <td colspan="4">
                  <input type="text" id="txt_PosibilidadTerapeutica" style="width:90%;" maxlength="100"/>
                  </td>
                </tr>

  
                <tr class="titulos">
                  <td width="50%" colspan="4">*MOTIVO PARA NO UTILIZARLA:
                  </td>
                </tr>                  
                <tr class="estiloImput" >
                  <td colspan="4">
                  <input type="text" id="txt_MotivoUso" style="width:90%;"  maxlength="100"/>
                  </td>
                </tr>                
                
                
                              
                <tr class="titulos">
                  <td width="50%" colspan="4">DIAGNOSTICO(NOMBRE Y CODIGO CIE-10)</td>                                    
                </tr> 
                <tr class="estiloImput">
                  <td colspan="4"><input type="text" id="txt_DiagnostProcPos" style="width:90%;"  maxlength="4000"/></td>
                </tr>  
                
                <tr class="titulos">
                  <td width="100%" colspan="4">CRITERIOS QUE JUSTIFICAN LA PRESENTE SOLICITUD
                  </td>
                </tr>    
                              
                <tr class="estiloImput" >
                  <td colspan="4">
                  1- EXISTE RIESGO INMINENTE PARA LA VIDA Y LA SALUD DEL PACIENTE :
                     <select size="1" id="txt_pregunta1" style="width:5%" title="32"  tabindex="14">	  
                        <option value="SI" selected="selected">SI</option>
                        <option value="NO">NO</option>                        
                     </select>                  
                  </td>
                </tr>  
                <tr class="estiloImput" >
                  <td colspan="4">
                  2- SE HA AGOTADO LAS POSIBILIDADES TERAPEUTICAS EN EL POS:
                     <select size="1" id="txt_pregunta2" style="width:5%" title="32"  tabindex="14">	  
                        <option value="SI" selected="selected">SI</option>
                        <option value="NO">NO</option>                        
                     </select>                  
                  </td>
                </tr>
                                      
                <tr class="titulos">
                  <td width="100%" colspan="4">Justificacion
                  </td>
                </tr> 
                
                <tr class="estiloImput" >
                  <td colspan="4">
                  <textarea type="text" id="txt_JustificacionPos"  maxlength="4000" style="width:95%" tabindex="100" > </textarea>
                  </td>
                </tr>  
              </table>
           </td>  
       </tr> 
       <tr class="estiloImput">  
           <td colspan="3">              
           <input id="btnProcedimiento"  type="button" class="small button blue" title="nopos888" value="Adicionar Elemento" onclick="modificarCRUD('listProcedimientoConNoPOS','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="203" />              
           </td>
       </tr>
      </td>               
    </table>
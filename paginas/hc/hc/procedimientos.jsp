<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>
<!--
<label id="lblNomAdministradora2" style="font-size:11px; color:#F00" disabled="disabled"></label>
<img src="/clinica/utilidades/imagenes/acciones/search.png" title="IMGp88-cargarProcedimientosHistoricos"
     onclick="cargarProcedimientosHistoricos()" width="14px" height="14px"/>
-->
<div id="tabsPrincipalConductaYTratamiento" style="width:98%; height:auto">
    <div id="divParaVentanitaCondicion1"></div>
    <ul>
        <% if(beanSession.usuario.preguntarMenu("CONDUC")){%>
        <!--
            <li id="idproc">
            <a href="#divProcedimiento" onclick="TabActivoConductaTratamiento('divProcedimiento')">
                <center>PROCEDIMIENTOS</center>
            </a>
        </li>
        <li id="idSolicitudes">
            <a href="#divSolicitudes" onclick="TabActivoConductaTratamiento('divSolicitudes')">
                <center>SOLICITUDES</center>
            </a>
        </li>    
            <!--li>
            <a href="#divAyudasDiagnosticas" onclick="TabActivoConductaTratamiento('divAyudasDiagnosticas')">
                <center>AYUDAS DIAGNOSTICAS</center>
            </a>
        </li>
        <li>
            <a href="#divTerapias" onclick="TabActivoConductaTratamiento('divTerapias')">
                <center>TERAPIAS INTERCONSULTA</center>
            </a>
        </li>
        <li>
            <a href="#divLaboratorioClinico" onclick="TabActivoConductaTratamiento('divLaboratorioClinico')">
                <center>LABORATORIO</center>
            </a>
        </li-->
<!--
        <li id="idmed">
            <a href="#divMedicacion" onclick="TabActivoConductaTratamiento('divMedicacion')">
                <center>MEDICAMENTOS</center>
            </a>
        </li>
        <li id="idconmed">
            <a href="#divConciliacionMedicamento" onclick="TabActivoConductaTratamiento('divConciliacionMedicamento')">
                <center>CONCILIACION MEDICAMENTO</center>
            </a>
        </li>
        <li id="idrefcon">
            <a href="#divReferenciaYContrareferencia" onclick="TabActivoConductaTratamiento('listRemision')">
                <center>REFERENCIA</center>
            </a>
        </li>-->
        <!-- <li>
            <a href="#divControl" onclick="TabActivoConductaTratamiento('divControl')">
                <center>CITAS</center>
            </a>
        </li>-->
        <!--<li id="idIncap">
            <a href="#divIncapacidad" onclick="TabActivoConductaTratamiento('listIncapacidad'); buscarParametros('listIncapacidad');">
                <center>INCAPACIDAD</center>
            </a>
        </li>-->
        <!--<li id="idcons">
            <a  href="#divConsentimientos" onclick="TabActivoConductaTratamiento('divConsentimientos')">
                <center>CONSENTIMIENTOS</center>
            </a>
        </li>-->
        <li id="idadmpac">
            <a href="#divAdministracionPaciente" onclick="TabActivoConductaTratamiento('divAdministracionPaciente')">
                <center>GESTI&Oacute;N ADMINISTRATIVA</center>
            </a>
        </li>
        <%}%>
        <li id="idedpac">
            <a href="#divEducacionPaciente" onclick="TabActivoConductaTratamiento('divEducacionPaciente'); buscarParametros('listGrillaEducacionPac');">
                <center>RECOMENDACIONES</center>
            </a>
        </li> 
        <% if(beanSession.usuario.preguntarMenu("CONDUC")){%>
        <!--<li id="idimpr">
            <a href="#divImpresion" onclick="TabActivoConductaTratamiento('divImpresion')">
                <center>IMPRESI&Oacute;N</center>
            </a>
        </li>-->
        <%}%>        
    </ul>

    <% if(beanSession.usuario.preguntarMenu("CONDUC")){%>





    <div id="divAdministracionPaciente">
        <table width="100%" style="background-color: #E8E8E8;">
            <tr class="titulosListaEspera">
                <td width="15%">Ordenes de Servicios</td>
                <td width="15%">Tipo de Alta</td>
                <td width="20%">Observaciones</td>
                <td width="5%"></td>
                <td width="10%" align="left">Ver Ordenes Hist&oacute;ricas</td>
            </tr>
            <tr class="estiloImput">
                <td>
                    <select id="cmbOrdenenesAdmisionURG" style="width: 95%;" onchange="reaccionAEvento(this.id);">
                        <option></option>
                    </select>
                </td>
                <td>
                    <select id="cmbTipoAlta" style="width: 95%;" onchange="reaccionAEvento(this.id)">
                    </select>
                </td>
                <td>
                    <textarea id="txtObservacionOrdenURG" placeholder="Observaciones" style="width: 95%;" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
                <td align="left">
                    <input type="button" class="small button blue" value="ENVIAR" onclick="modificarCRUD('enviarOrdenMedicaURG')"/>
                </td>
                <td> 
                    <label>
                        <div class="onoffswitch">
                            <input type="checkbox" checked class="onoffswitch-checkbox" id="sw_historicos" onchange="buscarHC('listOrdenesPacienteURG', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')">
                            <label class="onoffswitch-label" for="sw_historicos">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </label>

                </td>
            </tr>
            <tr id="trInfoMuerte" hidden>
                <td colspan="5">
                    <table width="100%">
                        <tr class="titulosListaEspera">
                            <td width="50%">Causa B&aacute;sica de Muerte</td>
                            <td width="50%">Fecha y Hora de Muerte</td>
                        </tr>
                        <tr class="estiloImput">
                            <td>
                                <input type="text" id="txtIdDxMuerte" style="width: 90%;" readonly>
                                <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
                                     onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdDxMuerte', '5')"
                                     src="/clinica/utilidades/imagenes/acciones/buscar.png">
                            </td>
                            <td>
                                <table width="100%">
                                    <tr class="titulos">
                                        <td width="15%">Fecha y Hora Auto.</td>
                                        <td width="35%">Fecha</td>
                                        <td width="35%">Hora</td>
                                    </tr>
                                    <tr>
                                        <td align="CENTER">
                                            <label>
                                                <div class="onoffswitch">
                                                    <input type="checkbox" class="onoffswitch-checkbox" id="sw_fecha_actual" onchange="reaccionAEvento(this.id)">
                                                    <label class="onoffswitch-label" for="sw_fecha_actual">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </label>
                                        </td>
                                        <td align="center">
                                            <input type="text" id="txtFechaMuerte">
                                        </td>
                                        <td align="center">
                                            <input type="number" min="1" max="12" id="txtHoraMuerte" style="width: 30%;">
                                            <input type="number" min="0" max="59" id="txtMinutoMuerte" style="width: 30%;">
                                            <select id="cmbPeriodoMuerte" style="width: 20%;">
                                                <option value=""></option>
                                                <option value="AM">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="titulos">
                <td colspan="5">
                    <table id="listOrdenesPacienteURG" width="100%"></table>
                </td>
            </tr>
        </table>
    </div>


    

    <!--<div id="divControl" style="width:99%; height:1100px">
        <table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td height="1100px">
                    <div id="divParaPaginaListaEspera" style="height:1100px"></div>
                </td>
            </tr>
        </table>
    </div>-->


    <div id="divVentanitaMedicamentosNoPos" style="display:none; z-index:2055; top:1px; left:190px;">
        <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60"> </div>
        <div style="z-index:2057; position:absolute; top:50px; left:15px; width:100%">
            <table width="100%" class="fondoTabla" align="center">
                <tr class="estiloImput">
                    <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaMedicamentosNoPos')" /></td>
                    <td>&nbsp;</td>
                    <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaMedicamentosNoPos')" /></td>
                </tr>
                <tr>
                    <td width="100%" colspan="3">
                        <div id="divParaPaginaNoPos"></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="divVentanitaEditProcedimientos" style="position:absolute; display:none; top:0px; left:0px; width:600px; height:90px; z-index:2055">
        <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60"></div>
        <div style="z-index:2057; position:absolute; top:100px; left:200px; height:auto; width:800px">

            <table width="100%" class="fondoTabla">

                <tr class="estiloImput">
                    <td colspan="2" align="left"><input type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditProcedimientos')" /></td>
                    <td colspan="2" align="right"><input type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditProcedimientos')" /></td>
                <tr>

                <tr class="titulosListaEspera">
                    <td width="10%">ID: <label id="lblIdProcedimientoEditar"></label></td>
                    <td width="15%">CUPS: <label id="lblCupsProcedimientoEditar"></label></td>
                    <td width="45%"> <label id="lblProcedimientoEditar"></label> </td>
                    <td width="30%">CLASE: <label id="lblProcedimientoClaseEditar"></label></td>
                </tr>


                <tr class="titulosListaEspera">
                    <td>CANTIDAD</td>
                    <td>GRADO NECESIDAD</td>
                    <td>INDICACI&Oacute;N</td>
                    <td>DIAGNOSTICO ASOCIADO</td>
                </tr>

                <tr class="estiloImput">
                    <td>
                        <select id="cmbCantidadProcedimientoEditar" style="width:90%">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                        </select>
                    </td>

                        <td>
                            <select id="cmbNecesidadProcedimientoEditar" style="width:90%" >
                                <option value=""></option>
                                <%resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(113);
                                    ComboVO cmbProE;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                        cmbProE = (ComboVO) resultaux.get(k);%>
                                <option value="<%= cmbProE.getId()%>" title="<%= cmbProE.getTitle()%>"><%=cmbProE.getDescripcion()%></option><%}%>           
                            </select> 
                        </td>

                    <td>
                        <textarea id="txtIndicacionProcedimientoEditar" style="width:90%" maxlength="5000" tabindex="200" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"></textarea>
                    </td>
                    <td>
                        <select id="cmbProcedimientoDiagnotiscoEditar" style="width:95%; overflow-y: auto;" tabindex="123" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');">
                            <option value=""></option>
                        </select>

                    </td>

                </tr>

                <tr>
                    <td colspan="4" align="center">
<input id="btnTratamientoEditar" type="button" class="small button blue" value="EDITAR" onclick="modificarCRUD('listProcedimientosDetalleEditar');" />
<input type="button" onclick="modificarCRUD('listProcedimientosDetalleEliminar');" value="ELIMINAR" title="ELIMI002" class="small button blue" id="btnProcedimientoEliminar">
<input id="btnProcedimientoImprimir" type="button" class="small button blue" value="IMPRIMIR ORDEN NOPOS" onclick="imprimirAnexoNoPosProcedimientos()" />

                    </td>
                </tr>

            </table>

        </div>
    </div>


   

    <div id="divVentanitaProcedimientosHistoricos" style="position:absolute; display:none; background-color:#E2E1A5; top:1px; left:5px; width:900px; height:180px; z-index:999">
        <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60"> </div>
        <div style="z-index:2057; position:absolute; top:300px; left:50px; height:1000; width:100%">
            <table width="100%" class="fondoTabla">
                <tr class="estiloImput">
                    <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaProcedimientosHistoricos')" /></td>
                    <td align="center">HISTORICOS DE ORDENES DE PLAN DE TRATAMIENTO</td>
                    <td colspan="1" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaProcedimientosHistoricos')" /></td>
                <tr>
                <tr class="titulos">
                    <td colspan="3">
                        <table id="listProcedimientosHistoricos" class="scroll" align="center"></table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="divVentanitaMedicacion" style="display:none; top:350px; left:350px; z-index:999">
        <div class="transParencia" style="z-index:2000; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>
        <div style="z-index:2001; position:fixed; top:200px; left:400px; width:45%">
            <table width="100%" border="1" class="fondoTabla">
                <tr class="estiloImput">
                    <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaMedicacion')" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="1" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaMedicacion')" /></td>
                <tr>
                <tr class="titulos ">
                    <td width="15%">ID Orden</td>
                    <td width="15%" colspan="2">ID Evolucion</td>
                    <td width="70%">NOMBRE</td>
                <tr>
                <tr class="estiloImput">
                    <td><label id="lblId"></label></td>
                    <td><label id="lblIdEvolucion"></label></td>
                    <td><label id="lblIdElemento"></label></td>
                    <td><label id="lblNombreElemento"></label></td>
                <tr>
                <tr>
                    <td colspan="4" align="center">

                        <input id="btnProcedimiento" type="button" class="small button blue" value="Elimina" onclick="modificarCRUD('eliminarMedicacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />

                        <input id="btnModificarMedicamento" title="UD75" type="button" class="small button blue" value="Editar" onclick="ocultar('divVentanitaMedicacion');mostrar('divVentanitaEditMedicamentos');" tabindex="203" />

                        <input id="btnProcedimientoImprimir" title="UJ74" type="button" class="small button blue" value="Imprime justificaci&oacute;n" onclick="imprimirAnexoNoPos()" tabindex="203" />

                <tr>
            </table>
        </div>
    </div>



    <div id="divVentanitaActivarMedicacion" style="display:none; top:300px; left:200px; z-index:999">
        <div class="transParencia" style="z-index:2000; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>
        <div style="z-index:2001; position:fixed; top:150px; left:300px; width:45%">

            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../suministros/articulo/articuloOrdenMedica.jsp" flush="true">
                    <jsp:param name="titulo" value="ADMISIONES" />
                </jsp:include>
            </div>

        </div>
    </div>



    <div id="divVentanitaEditMedicamentos" style="display:none;  top:350px; left:200px; z-index:999">
        <div class="transParencia" style="z-index:2056; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>
        <div style="z-index:2057; position:fixed; top:200px; left:400px; width:60%">

            <table width="100%" class="fondoTabla">
                <tr class="estiloImput">
                    <td colspan="1" align="left">
                        <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditMedicamentos')" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="1" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditMedicamentos')" /></td>
                <tr>
                <tr class="titulos">
                    <td align="center"><b>Id Orden: </b><label id="lblIdEditar"></label> </td>
                    <td align="center"><b>Id Articulo: </b><label id="lblIdElementoEditar"></label> </td>

                    <td colspan="2">
                        <b>Articulo: </b><label id="lblNombreElementoEditar"></label></td>
                </tr>
                <tr>
                    <td colspan="4"><br/></td>
                </tr>
                <tr align="center">
                    <td width="25%">Via</td>
                    <td width="25%">Forma Farmaceutica</td>
                    <td width="25%">Dosis</td>
                    <td width="25%">Frecuencia(Horas)</td>
                </tr>

                <tr>

                    <td align="center">
                        <select id="cmbIdViaEditar" style="width:90%" tabindex="201" onFocus="cargarElementosDeMedicamentoEditar('lblIdElementoEditar')">	 
                            <option value=""></option>
                        </select>
                    </td>
                    <td align="center">
                        <select size="1" id="cmbUnidadEditar" style="width:85%" onFocus="comboFormaFarmaceuticaEditar()" tabindex="202">
                            <option value=""></option>
                        </select>
                    </td>
                    <td align="center">
                        <select size="1" id="cmbCantEditar" title="CantidadDosis" style="width:50%" tabindex="202">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option> 
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="8">8</option>                   
                            <option value="10">10</option>                     
                        </select>
                    </td>

                    <td align="center">
                        <select size="1" id="cmbFrecuenciaEditar" style="width:50%" tabindex="202">
                            <option value="2">Cada 2</option> 
                            <option value="3">Cada 3</option>                
                            <option value="4">Cada 4</option>
                            <option value="6">Cada 6</option>
                            <option value="8">Cada 8</option> 
                            <option value="10">Cada 10</option>                    
                            <option value="12">Cada 12</option>
                            <option value="15">Cada 15</option>                   
                            <option value="24">Cada 24</option>
                            <option value="0">Unica</option>                   
                        </select>
                    </td>

                </tr>


                <tr align="center">
                    <td>Duracion tratamiento (Dias)</td>
                    <td>Cantidad</td>
                    <td colspan="2">Indicaciones</td>
                </tr>

                <tr>

                    <td align="center">
                        <select id="cmbDiasEditar" style="width:80%" tabindex="202">
                            <option value="1">Por 1</option>
                            <option value="2">Por 2</option>
                            <option value="3">Por 3</option> 
                            <option value="4">Por 4</option>
                            <option value="5">Por 5</option>
                            <option value="6">Por 6</option>    
                            <option value="7">Por 7</option>    
                            <option value="8">Por 8</option>
                            <option value="9">Por 9</option>
                            <option value="10">Por 10</option>
                            <option value="12">Por 12</option> 
                            <option value="14">Por 14</option>
                            <option value="15">Por 15</option> 
                            <option value="20">Por 20</option>                                     
                            <option value="30">Por 30</option>
                            <option value="60">Por 60</option> 
                            <option value="90">Por 90</option>     
                            <option value="120">Por 120</option>                                           
                            <option value="150">Por 150</option>                                                              
                            <option value="180">Por 180</option>                        
                            <option value="0">Unica</option>  
                            <option value="101">Permanente</option>                                                                                                                  
                        </select>
                    </td>
                    <td align="center">
                        <input type="text" value="" id="txtCantidadEditar" style="width:20%" size="20" onkeypress="javascript:return soloNumeros(event)" onblur="v28(this.value, this.id);" />

                        <select size="1" id="cmbUnidadFarmaceuticaEditar" style="width:70%" tabindex="202">
                            <option value="">[Unidad_farmaceutica]</option>                     
                            <%     resultaux.clear();
                                resultaux = (ArrayList) beanAdmin.combo.cargar(42);
                                ComboVO cmbUnFarm2;
                                for (int k = 0; k < resultaux.size(); k++) {
                                    cmbUnFarm2 = (ComboVO) resultaux.get(k);
                            %>
                            <option value="<%= cmbUnFarm2.getId()%>" title="<%= cmbUnFarm2.getTitle()%>">
                                <%= cmbUnFarm2.getDescripcion()%></option>
                                <%}%>

                        </select>
                    </td>
                    <td colspan="2" align="center">
                        <input type="text" value="" id="txtIndicacionesEditar" style="width:80%" size="20" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)" />
                    </td>

                </tr>

                <tr>
                    <td colspan="4"><br/></td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <input id="btnTratamientoEditar" type="button" class="small button blue" title="bot174" value="Editar" onclick="modificarCRUD('listMedicamentosEditar', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" tabindex="203" />

                    </td>
                </tr>

            </table>

        </div>
    </div>
    <%}%>

    <div id="divEducacionPaciente">
        <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
            <tr>
                <td>
                    <!-- <input type="hidden" id="txtIdClaseLaboratorio" value="4" />     -->
                    <table width="100%">
                             
                        <tr class="estiloImput">   
                            <td>
                            <textarea id="txtObservacionesEducacion" rows="9" style="width:90%; border:1px solid #4297d7;"  maxlength="18000"  tabindex="100" onblur="v28(this.value,this.id);" onkeypress="return validarKey(event,this.id)" ></textarea>
                            </td>   
                        </tr>                         
                    </table> 
                    <table width="100%" align="center">
                        <tr class="estiloImput"> 
                            <td>
                                <input id="btnEducacionPac" align="center" type="button" class="small button blue" title="Permite adjuntar las recomendaciones en la historia clinica" value="Crear"  onclick="modificarCRUD('agregarEducacionPaciente');" tabindex="203" />  
                                <input id="btnLimpiar" align="center" type="button" class="small button blue" title="limpia la caja de texto a ingresar" value="Limpiar"  onclick="limpiaAtributo('txtObservacionesEducacion', 0);" tabindex="203" />                     
                            </td>   
                        </tr>                         
                    </table> 
                    <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
                        <tr class="titulos">
                            <td>                                    
                                <table id="listGrillaEducacionPac" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                            </td>
                        </tr>
                    </table>                                                              

                </td>   
            </tr>   
        </table> 

    </div>
</div>

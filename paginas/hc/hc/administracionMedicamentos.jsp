<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
     
    ArrayList resultaux=new ArrayList();

%>

<table width="100%">
  <tr class="titulos">
    <td width="5%">&nbsp;</td>
    <td width="20%">Art&iacute;culo&nbsp;&nbsp;&nbsp;&nbsp; </td>
    <td width="10%">Via</td>
    <td width="10%">Forma Farm</td>
    <td width="7%">Dosis</td>
    <td width="18%">Horas / Minutos</td>
    <!--td width="10%">sITIO</td-->
    <td width="20%">
      Indicaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input id="btnProcedimiento" type="button" class="small button blue" value="ADICIONAR" title="btn_adm741"
        onclick="modificarCRUD('AdicionarAdmMedicamentos','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
    </td>
    <td width="20%" align="right">
      <img src="/clinica/utilidades/imagenes/acciones/buscar.png" onclick="administracionMedicamentos();" width="16"
        height="16" align="left">
    </td>
  </tr>
  <tr class="estiloImput">
    <td align="center" colspan="2">
      <input type="text" id="txtIdMedicamento" style="width:98%"
        onkeypress="llamarAutocomIdDescripcionConDato('txtIdMedicamento',37)" maxlength="20" tabindex="200">
    </td>
    <td align="center">
      <select id="cmbIdViaMed" style="width:90%" tabindex="201"
        onFocus="cargarElementosDeMedicamentoAdm('txtIdMedicamento')">
        <option value=""></option>
      </select>
    </td>
    <td align="center">
      <select size="1" id="cmbUnidadAdm" style="width:90%" onFocus="comboFormaFarmaceuticaAdm()" tabindex="202">
        <option value=""></option>
      </select>
    </td>
    <td align="center">
      <select size="1" id="cmbCantAdm" title="CantidadDosis" style="width:50%" tabindex="202">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="8">8</option>
        <option value="10">10</option>
      </select>
    </td>
    <td align="center">

      <select size="1" id="cmbHoraInicioM" style="width:18%" title="38" tabindex="14">
        <option value=""></option>
        <% 
                for(int i=101;i<113;i++){                                       
                    String[] value = Integer.toString(i).split("");
                    String hour = value[1]+value[2];
                                                        
            %>
        <option value="<%=hour%>"><%=hour%></option>
        <%}
            %>
      </select>
      &nbsp;
      <select size="1" id="cmbMinutoInicioM" style="width:18%" title="38" tabindex="14">
        <option value=""></option>
        <% 
                for(int i=100;i<160;i++){                                       
                    String[] value = Integer.toString(i).split("");
                    String minute = value[1]+value[2];
                                                        
            %>
        <option value="<%=minute%>"><%=minute%></option>
        <%}
            %>
      </select>
      <select id="cmbPeriodo">
        <option value="AM">AM</option>
        <option value="PM">PM</option>
      </select>
    </td>
    <!--td align="center">
      <select id="cmbSitioAdm" style="width:80%" tabindex="202">
        <option value=""></option>
        <option value="1">Derecha</option>
        <option value="2">Izquierda</option>
        <option value="10">No Aplica</option>

      </select>
    </td-->
    <td align="center">
      <input type="text" value="" id="txtIndicaciones" style="width:98%" size="20" />
    </td>
  </tr>
</table>

<table width="100%" align="center">
  <tr class="titulos">
    <td>
      <table id="listAdministracionMedicacion" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
    </td>
  </tr>
</table>

<div id="divVentanitaMedicacion"
  style="position:absolute; display:none; background-color:#E2E1A5; top:310px; left:150px; width:600px; height:90px; z-index:999">
  <table width="100%" border="1" class="fondoTabla">
    <tr class="estiloImput">
      <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
          onclick="ocultar('divVentanitaMedicacion')" /></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td colspan="1" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
          onclick="ocultar('divVentanitaMedicacion')" /></td>
    <tr>
    <tr class="titulos ">
      <td width="15%">ID Evo</td>
      <td width="15%">ID Med</td>
      <td width="70%" colspan="2">NOMBRE</td>
    <tr>
    <tr class="estiloImput">
      <td><label id="lblIdEvolucion"></label></td>
      <!--<td ><label id="lblId"></label></td>            -->
      <td><label id="lblIdElemento"></label></td>
      <td><label id="lblNombreElemento"></label></td>
    <tr>
    <tr>
      <td colspan="2" align="center">
        <input id="btnProcedimiento" title="btn_e7810" type="button" class="small button blue" value="Elimina"
          onclick="modificarCRUD('eliminarAdmiMedicacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
      </td>
      <td colspan="2" align="center">
        <!-- <input id="btnProcedimientoImprimir" title="btn_gf74" type="button" class="small button blue" value="Imprime justificaci&oacute;n" onclick="imprimirAnexoNoPos()" tabindex="203" /> -->
      </td>
    <tr>
  </table>
</div>
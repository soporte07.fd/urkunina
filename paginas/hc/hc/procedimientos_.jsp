<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/clinica/utilidades/imagenes/acciones/lupa_roja.gif" title="IMG55" onclick="cargarProcedimientosHistoricos()" width="14px" height="14px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label id="lblNomAdministradora2" style="font-size:11px; color:#F00" disabled="disabled"></label>
<div id="tabsPrincipalConductaYTratamiento" style="width:98%; height:450px">
    <ul>
        <li><a href="#divProcedimiento" onclick="TabActivoConductaTratamiento('divProcedimiento')"><center>PROCEDIMIENTOS</center></a></li>
        <li><a href="#divAyudasDiagnosticas" onclick="TabActivoConductaTratamiento('divAyudasDiagnosticas')" ><center>AYUDAS DIAGNOSTICAS</center></a></li>
        <li><a href="#divMedicacion" onclick="TabActivoConductaTratamiento('divMedicacion')"><center>MEDICAMENTOS</center></a></li>
        <li><a href="#divConciliacionMedicamento" onclick="TabActivoConductaTratamiento('divConciliacionMedicamento')"><center>CONCILIACION MEDICAMENTO</center></a></li>    
        <li><a href="#divTerapias" onclick="TabActivoConductaTratamiento('divTerapias')"><center>TERAPIAS INTERCONSULTA</center></a></li>          
        <li><a href="#divLaboratorioClinico" onclick="TabActivoConductaTratamiento('divLaboratorioClinico')"><center>LABORATORIO</center></a></li>      
        <li><a href="#divReferenciaYContrareferencia" onclick="TabActivoConductaTratamiento('listRemision')"><center>REFERENCIA CONTRA REFERENCIA</center></a></li>
        <li><a href="#divControl" onclick="TabActivoConductaTratamiento('divControl')"><center>CONTROL</center></a></li>    
    </ul> 

    <div id="divProcedimiento" style="width:99%"> 
        <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
            <tr>
                <td >
                    <!-- ini justificacion anexo 3***********************************************************************************************-->          
                    <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
                        <input type="hidden" id="txtIdClase" value="1" />                 
                    </table>  
                    <!-- fin justificacion anexo 3***********************************************************************************************-->        
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="40%">Nombre del Procedimientoo</td> 
                            <td width="7%">Cantidad</td>
                            <td width="15%">Sitio</td>
                            <td width="20%">Indicaci&oacute;n</td>   
                            <td width="20%" align="left" colspan="2">Diagnostico Asociado</td>                                                       
                        </tr>		
                        <tr class="estiloImput"> 
                            <td><input type="text"  value="" id="txtIdProcedimiento" onkeypress="llamarAutocompletarParametro1('txtIdProcedimiento', 642, 'lblIdAdministradora')" style="width:90%"  size="200"  maxlength="200"  tabindex="200">
                                <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaProcmto"  onclick="traerVentanitaCondicion1(this.id, 'lblIdAdministradora', 'divParaVentanitaCondicion1', 'txtIdProcedimiento', '12')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
                            </td>     
                            <td>
                                <select size="1" id="cmbCantidad" style="width:60%" tabindex="201" title="53" >	                                        
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>                       
                            </td> 
                            <td>
                                <select id="cmbIdSitioQuirurgico" style="width:80%" tabindex="123" >
                                    <option value=""></option>
                                    <%     resultaux.clear();
                                           resultaux=(ArrayList)beanAdmin.combo.cargar(120);	
                                           ComboVO cmbSQ; 
                                           for(int k=0;k<resultaux.size();k++){ 
                                                 cmbSQ=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbSQ.getId()%>" title="<%= cmbSQ.getTitle()%>"><%= cmbSQ.getDescripcion()%></option>
                                    <%}%>
                                </select>                            
                            </td>                                   
                            <td>           	
                                <textarea type="text"  value="" id="txtIndicacion" style="width:90%"  size="500"  maxlength="5000"  tabindex="200" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"></textarea></td>                                               
                            <td>
                                <select id="cmbIdDxRelacionadoP" style="width:80%" tabindex="123" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');" >
                                    <option value=""></option>
                                </select> 
                            </td>    
                            <td>
                                <input id="btnProcedimiento"  type="button" class="small button blue" title="JAVA32" value="Adicion"  onclick="guardarYtraerDatoAlListado('buscarSiEsNoPosProcedim')" tabindex="203"   /> 
                            </td>    
                        </tr>                                                                            

                    </table> 
                    <table  width="100%" id="idTablaProcedimientosDetalle" align="center" >                          
                        <tr class="titulos">
                            <td>                                    
                                <table id="listProcedimientosDetalle" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                            </td>
                        </tr>
                    </table>    
                </td>   
            </tr>   
        </table>                 
    </div> 

    <div id="divAyudasDiagnosticas"  style="width:99%">    
        <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
            <tr>
                <td>
                    <input type="hidden" id="txtIdClaseAD" value="2" /> 
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="40%">Nombre de la Ayuda Diagn&oacute;stica</td> 
                            <td width="7%">Cantidad</td> 
                            <td width="15">Sitio</td>                               
                            <td width="20%">Indicaci&oacute;n</td>              
                            <td width="20%" align="left" colspan="2">Diagnostico Asociado</td>     
                        </tr>		
                        <tr class="estiloImput"> 
                            <td><input type="text"   id="txtIdAyudaDiagnostica" onkeypress="llamarAutocompletarParametro1('txtIdAyudaDiagnostica', 643, 'lblIdAdministradora')" style="width:90%"  size="200"  maxlength="200"  tabindex="200"  >  <!--onblur="llamarIndicacionAdx()preguntar a daniel-->
                                <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaAdx"  onclick="traerVentanitaCondicion1(this.id, 'lblIdAdministradora', 'divParaVentanitaCondicion1', 'txtIdAyudaDiagnostica', '19')" src="/clinica/utilidades/imagenes/acciones/buscar.png">                      
                            </td>     
                            <td>
                                <select size="1" id="cmbCantidadAD" style="width:60%" tabindex="201" title="53" onblur="setIndicacion(); cargarSitioADX()">	                                        
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>  
                                    <option value="4">4</option>                                                  
                                </select>                       
                            </td> 
                            <td>
                                <select id="cmbIdSitioQuirurgicoAD" style="width:80%" tabindex="123" onblur="cargarCantidad()">
                                    <option value=""></option>
                                    <%     resultaux.clear();
                                           resultaux=(ArrayList)beanAdmin.combo.cargar(120);	
                                           ComboVO cmbAD; 
                                           for(int k=0;k<resultaux.size();k++){ 
                                                 cmbAD=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbAD.getId()%>" title="<%= cmbAD.getTitle()%>"><%= cmbAD.getDescripcion()%></option>
                                    <%}%>
                                </select>                            
                            </td>                                   
                            <td><input type="text"  value="" id="txtIndicacionAD" style="width:90%"  size="500"  maxlength="5000"  tabindex="200" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"  ></td>       
                            <td>
                                <select id="cmbIdDxRelacionadoA" style="width:80%" tabindex="123" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');" >
                                    <option value=""></option>
                                </select> 
                            </td> 	
                            <td>
                                <input id="btnProcedimiento"  type="button" class="small button blue" title="adicion322d" value="Adicion"  onclick="guardarYtraerDatoAlListado('buscarSiEsNoPosAyudaDiagnostica')" tabindex="203" />                      
                            </td>

                        </tr>                                                                            
                    </table> 
                    <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
                        <tr class="titulos">
                            <td>                                    
                                <table id="listAyudasDiagnosticasDetalle" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                            </td>
                        </tr>
                    </table>    
                </td>   
            </tr>   
        </table>                 
    </div> 
    <!-- MEdicamentos  -->
    <div id="divMedicacion">    
        <table width="100%" >
            <tr class="titulos">              
                <td width="25%">Art&iacute;culo&nbsp;&nbsp;&nbsp;&nbsp; </td>   
                <td width="10%">Via</td>  
                <td width="5%">Forma Farm</td>                
                <td width="10%">Dosis</td> 			  
                <td width="10%">&nbsp;</td> 			  
            </tr>
            <tr>
                <td align="center" >
                    <input type="text" id="txtIdArticulo"  style="width:98%" onfocus="llamarAutocomIdDescripcionConDato('txtIdArticulo', 37)"   maxlength="20" tabindex="200">
                </td>
                <td align="center" >
                    <select  id="cmbIdVia" style="width:90%" tabindex="201" onFocus="cargarElementosDeMedicamento('txtIdArticulo')">	 
                        <option value=""></option>
                    </select> 
                </td> 
                <td align="center" >          
                    <select size="1" id="cmbUnidad" style="width:85%" onFocus="comboFormaFarmaceutica()"  tabindex="202">
                        <option value=""></option>
                    </select>                              
                </td>                                            
                <td align="center" >          
                    <select size="1" id="cmbCant" title="CantidadDosis" style="width:50%"  tabindex="202">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option> 
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="8">8</option>                   
                        <option value="10">10</option>                     
                    </select>                              
                </td>
                <td width="10%">&nbsp;</td> 			  	
            </tr>		
            <tr class="titulos"> 

                <td width="10%">Frecuencia(horas)</td>  
                <td width="10%">Duracion tratamiento (Dias)</td>
                <td width="12%">Cantidad </td> 
                <td width="20%">Indicaciones </td> 
                <td>
                    <input id="btnProcedimiento"  type="button" class="small button blue" value="ADICIONAR" title="btn_adm784" onclick="guardarYtraerDatoAlListado('buscarSiEsNoPos')" />
                </td>	
            </tr>
            <tr>  

                <td align="center">          
                    <select size="1" id="cmbFrecuencia" style="width:50%"  tabindex="202">
                        <option value="2">Cada 2</option> 
                        <option value="3">Cada 3</option>                
                        <option value="4">Cada 4</option>
                        <option value="6">Cada 6</option>
                        <option value="8">Cada 8</option> 
                        <option value="10">Cada 10</option>                    
                        <option value="12">Cada 12</option>
                        <option value="15">Cada 15</option>                   
                        <option value="24">Cada 24</option>
                        <option value="0">Unica</option>                   
                    </select>                               
                </td>
                <td align="center">          
                    <select id="cmbDias" style="width:80%"  tabindex="202">
                        <option value="1">Por 1</option>
                        <option value="2">Por 2</option>
                        <option value="3">Por 3</option> 
                        <option value="4">Por 4</option>
                        <option value="5">Por 5</option>
                        <option value="6">Por 6</option>    
                        <option value="7">Por 7</option>    
                        <option value="8">Por 8</option>
                        <option value="9">Por 9</option>
                        <option value="10">Por 10</option>
                        <option value="12">Por 12</option> 
                        <option value="14">Por 14</option>
                        <option value="15">Por 15</option> 
                        <option value="20">Por 20</option>                                     
                        <option value="30">Por 30</option>
                        <option value="60">Por 60</option> 
                        <option value="90">Por 90</option>     
                        <option value="120">Por 120</option>                                           
                        <option value="150">Por 150</option>                                                              
                        <option value="180">Por 180</option>                        
                        <option value="0">Unica</option>  
                        <option value="101">Permanente</option>                                                                                                                  
                    </select>                               
                </td>               
                <td align="center">          
                    <input type="text"  value="" id="txtCantidad"  style="width:20%" size="20" onkeypress="javascript:return soloNumeros(event)"  onblur="v28(this.value, this.id);"/>                               
                    <select size="1" id="cmbUnidadFarmaceutica" style="width:70%"  tabindex="202">
                        <option value="">[Unidad_farmaceuticaa]</option>                     
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(42);	
                               ComboVO cmbUnFarm; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbUnFarm=(ComboVO)resultaux.get(k);
                        %>
                        <option value="<%= cmbUnFarm.getId()%>" title="<%= cmbUnFarm.getTitle()%>"><%= cmbUnFarm.getDescripcion()%></option>
                        <%}%>

                    </select>                                 
                </td>
                <td align="center">          
                    <input type="text"  value="" id="txtIndicaciones"  style="width:98%" size="20" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)" />                               
                </td>
                <td></td>	
            </tr>

            <!-- grilla -->
            <tr class="titulos">
                <td colspan="8">                                    
                    <table id="listMedicacion" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                </td>
            </tr> 
        </table> 
        <div id="divVentanitaMedicacion"  style="position:absolute; display:none; background-color:#E2E1A5; top:900px; left:150px; width:600px; height:90px; z-index:999">
            <table width="100%"  border="1"  class="fondoTabla">           
                <tr class="estiloImput" >
                    <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaMedicacion')" /></td>  
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="1" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaMedicacion')" /></td>  
                <tr>     
                <tr class="titulos " >
                    <td width="15%" >ID Orden</td>  
                    <td width="15%" >ID Evolucion</td> 
                    <td width="70%" colspan="2">NOMBRE</td>           
                <tr>           
                <tr class="estiloImput" >
                    <td ><label id="lblId"></label></td> 
                    <td ><label id="lblIdEvolucion"></label></td>            
                    <td ><label id="lblIdElemento"></label></td>
                    <td ><label id="lblNombreElemento"></label></td> 
                <tr> 
                <tr>  
                    <td colspan="2" align="center">
                        <input id="btnProcedimiento" type="button" class="small button blue" value="Elimina" onclick="modificarCRUD('eliminarMedicacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />  
                    </td>     
                    <td colspan="2" align="center">
                        <input id="btnProcedimientoImprimir" title="btn_gf74" type="button" class="small button blue" value="Imprime justificaci&oacute;n" onclick="imprimirAnexoNoPos()" tabindex="203" />
                    </td>                    
                <tr>                
            </table> 
        </div>                             
    </div>
    <div id="divConciliacionMedicamento">    
        <table width="100%" >
            <tr class="titulos"> 
                <td width="50%">Antecedentes Farmacologicos</td>            
                <td width="50%">Medicamentos Ordenados</td>                                           
            </tr>          
            <tr class="titulos">
                <td>                                    
                    <table id="listAntFarmacologicos" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                </td>
                <td>                                    
                    <table id="listMedicacionOrdenada" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                </td>
            </tr> 
        </table> 

        <table width="100%" >
            <tr class="titulos">
                <td width="30%" valign="top" >EXISTE INTERACCION MEDICAMENTOSA? 
                </td>
                <td width="60%">DESCRIPCION
                </td>
                <td width="10%">&nbsp;
                </td>            
            </tr>
            <tr class="estiloImput">
                <td> 
                    <select id="cmbExiste" style="width:20%" onchange="selectConciliacionMedicamentosa()" tabindex="303">
                        <option value=""></option>
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                    </select>
                </td>
                <td>
                    <textarea id="txtConciliacionDescripcion" style="width:95%" tabindex="303" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"> </textarea>                
                </td>
                <td>
                    <input id="btnProcedimiento" type="button" title="BT66R" class="small button blue" value="Guardar" onclick="modificarCRUD('guardaConciliacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />  
                </td>
            </tr>        
        </table>	
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>                  
    </div>

    <div id="divTerapias"  style="width:99%">    
        <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
            <tr>
                <td>
                    <input type="hidden" id="txtIdClaseTerapia" value="5" />  
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="40%">Nombre del Terapia</td> 
                            <td width="7%">Cantidad</td> 
                            <td width="15%">Sitio</td>                               
                            <td width="20%">Indicaci&oacute;n</td>              
                            <td width="20%" align="left" colspan="2">Diagnostico Asociado</td>                                                          
                        </tr>		
                        <tr class="estiloImput"> 
                            <td><input type="text"  value="" id="txtIdTerapia" onkeypress="llamarAutocompletarParametro1('txtIdTerapia', 644, 'lblIdAdministradora')" style="width:90%"  size="200"  maxlength="200"  tabindex="200">
                                <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaTera"  onclick="traerVentanitaCondicion1(this.id, 'lblIdAdministradora', 'divParaVentanitaCondicion1', 'txtIdTerapia', '20')" src="/clinica/utilidades/imagenes/acciones/buscar.png">                                           
                            </td>     
                            <td>
                                <select size="1" id="cmbCantidadTerapia" style="width:60%" tabindex="201" title="53" onfocus="cargarSitioTer()">	                                        
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>                       
                            </td>   
                            <td>
                                <select id="cmbIdSitioQuirurgicoTerapia" style="width:80%" tabindex="123" >
                                    <option value=""></option>
                                    <%     resultaux.clear();
                                           resultaux=(ArrayList)beanAdmin.combo.cargar(120);	
                                           ComboVO cmbTe; 
                                           for(int k=0;k<resultaux.size();k++){ 
                                                 cmbTe=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbTe.getId()%>" title="<%= cmbTe.getTitle()%>"><%= cmbTe.getDescripcion()%></option>
                                    <%}%>
                                </select>                            
                            </td>                  
                            <td><input type="text"  value="" id="txtIndicacionTerapia" style="width:90%"  size="500"  maxlength="500"  tabindex="200" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"></td>  
                            <td>
                                <select id="cmbIdDxRelacionadoT" style="width:80%" tabindex="123" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');" >
                                    <option value=""></option>
                                </select> 	
                            </td>	 
                            <td>
                                <input id="btnProcedimiento"  type="button" class="small button blue" title="adicion322Terap" value="Adicion"  onclick="guardarYtraerDatoAlListado('buscarSiEsNoPosTerapia')" tabindex="203" />                                            
                            </td>

                        </tr>                                                                            
                    </table> 
                    <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
                        <tr class="titulos">
                            <td>                                    
                                <table id="listTerapiaDetalle" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                            </td>
                        </tr>
                    </table>    
                </td>   
            </tr>   
        </table>
    </div>  

    <div id="divLaboratorioClinico"  style="width:99%">    
        <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
            <tr>
                <td>
                    <input type="hidden" id="txtIdClaseLaboratorio" value="4" />    
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="40%">Nombre del Laboratorio</td> 
                            <td width="7%">Cantidad</td>
                            <td width="15%">Sitio</td>                                
                            <td width="20%">Indicaci&oacute;n</td>              
                            <td width="20%" align="left" colspan="2">Diagnostico Asociado</td>                                                           
                        </tr>		
                        <tr class="estiloImput"> 
                            <td><input type="text"  value="" id="txtIdLaboratorio" onkeypress="llamarAutocomIdDescripcionConDato('txtIdLaboratorio', 11)" style="width:90%"  size="200"  maxlength="200"  tabindex="200">
                                <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaLabo"  onclick="traerVentanitaCondicion1(this.id, 'lblIdAdministradora', 'divParaVentanitaCondicion1', 'txtIdLaboratorio', '15')" src="/clinica/utilidades/imagenes/acciones/buscar.png">                                                                
                            </td>     
                            <td>
                                <select size="1" id="cmbCantidadLaboratorio" style="width:60%" tabindex="201" title="53" onfocus="cargarSitioLab()">	                                        
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>                       
                            </td> 
                            <td>
                                <select id="cmbIdSitioQuirurgicoLaboratorio" style="width:80%" tabindex="123" >
                                    <option value=""></option>
                                    <%     resultaux.clear();
                                           resultaux=(ArrayList)beanAdmin.combo.cargar(120);	
                                           ComboVO cmbLa; 
                                           for(int k=0;k<resultaux.size();k++){ 
                                                 cmbLa=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbLa.getId()%>" title="<%= cmbLa.getTitle()%>"><%= cmbLa.getDescripcion()%></option>
                                    <%}%>
                                </select>                            
                            </td>                                  
                            <td><input type="text"  value="" id="txtIndicacionLaboratorio" style="width:90%"  size="500"  maxlength="500"  tabindex="200" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"></td>   
                            <td> <select id="cmbIdDxRelacionadoL" style="width:80%" tabindex="123" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');" >
                                    <option value=""></option>
                                </select> 	
                            </td>	 
                            <td>
                                <input id="btnProcedimiento"  type="button" class="small button blue" title="adicion322" value="Adicion"  onclick="guardarYtraerDatoAlListado('buscarSiEsNoPosLaboratorio')" tabindex="203" />                                            
                            </td>

                        </tr>                                                                            
                    </table> 
                    <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
                        <tr class="titulos">
                            <td>                                    
                                <table id="listLaboratoriosDetalle" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                            </td>
                        </tr>
                    </table>    
                </td>   
            </tr>   
        </table>                        
    </div>

    <div id="divReferenciaYContrareferencia">    
        <div id="divAcordionReferenc" style="display:BLOCK">      
            <jsp:include page="divsAcordeonFormulario.jsp" flush="FALSE" >
                <jsp:param name="titulo" value="REGISTRO DE REFERENCIA O CONTRAREFERENCIA " />
                <jsp:param name="idDiv" value="divReferenc" />
                <jsp:param name="pagina" value="referenciaYcontrareferencia.jsp"/>                
                <jsp:param name="display" value="BLOCK" />  
            </jsp:include> 
        </div>                   
    </div> 

    <div id="divParaVentanitaCondicion1"></div>

    <table width="100%" >
        <tr>
            <td colspan="9" bgcolor="#c0d3c1">OPCIONES PARA IMPRIMIR PLAN DE TRATAMIENTO</td>
        </tr>
        <tr> 
            <td colspan="2" align="right">
            </td>
            <td colspan="1" align="center">
                <select id="txt_MD_PRO" style="width:50%;" title="32"  tabindex="14" >	  
                    <option value="Medicamentos">Medicamentos</option>
                    <option value="Procedimientos">Procedimientos diagnosticos y terapeuticos</option>                                    
                </select>           
            </td>
            <td colspan="1" align="right">Clase:
                <select size="1" id="txt_ES_POS" style="width:40%;" title="32"  tabindex="14" >	  
                    <option value="">TODOS</option>
                    <option value="POS">POS</option>
                    <option value="NOPOS">NO POS</option>                                    
                </select>  
            </td>           
            <td colspan="2" align="right">
                <input id="btnProcedimientoImprimir"  type="button" class="small button blue" value="Imprimir"  title="btn_iju"  onclick="imprimirFormulaMedica()" tabindex="203" />                                  
            </td> 
            <td colspan="1" align="center">
                <input id="btnFormulaOftalmica_"  type="button" class="small button blue" value="Imp Formula Oft." title="btn_gfo" onClick="formulaPDFHC();" tabindex="203" />   
            </td>         
            <td colspan="1" align="right">
                <input id="btnProcedimientoImprimir_" title="btn_u48p"  type="button" class="small button blue" value="Imprime Anexo 3 para Procedimientos POS" onclick="imprimirAnexo3Pdf()" tabindex="203" />
            </td>
        <tr>
    </table>  

    <div id="divControl" style="width:99%; height:1100px"> 
        <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
            <tr>
                <td height="1100px" >
                    <div id="divParaPaginaListaEspera" style="height:1100px" ></div>     
                </td>   
            </tr>   
        </table>                 
    </div> 




    <div id="divVentanitaMedicamentosNoPos"  style="display:none; z-index:2055; top:1px; left:190px;">

        <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">  </div>  
        <div  style="z-index:2057; position:absolute; top:520px; left:15px; width:100%">  
            <table width="100%" align="center">
                <tr class="estiloImput" >
                    <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaMedicamentosNoPos')" /></td>  
                    <td>&nbsp;</td>             
                    <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaMedicamentosNoPos')" /></td>  
                </tr>   
                <tr>
                    <td width="100%" colspan="3">
                        <div id="divParaPaginaNoPos" ></div>     
                    </td>
                </tr>   
            </table>
        </div>     
    </div>   






    <div id="divVentanitaEditTratamientos"  style="position:absolute; display:none; background-color:#E2E1A5; top:350px; left:5px; width:600px; height:90px; z-index:999">
        <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">  </div>  
        <div  style="z-index:2057; position:absolute; top:300px; left:50px; height:1000; width:100%"> 

            <table width="100%"  class="fondoTabla">
                <tr class="titulos"> 
                    <td width="40%">Nombre del Procedimientoo</td> 
                    <td width="7%">Cantidad</td>
                    <td width="15%">Sitio</td>
                    <td width="20%">Indicaci&oacute;n</td>   
                    <td width="20%" align="left" colspan="2">Diagnostico Asociado</td>                                                       
                </tr>		
                <tr class="estiloImput"> 
                    <td><input type="text"  value="" id="txtIdProcedimiento" onkeypress="llamarAutocompletarParametro1('txtIdProcedimiento', 642, 'lblIdAdministradora')" style="width:90%"  size="200"  maxlength="200"  tabindex="200"> 
                    </td>     
                    <td>
                        <select size="1" id="cmbCantidad" style="width:60%" tabindex="201" title="53" >	                                        
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>                       
                    </td> 
                    <td>
                        <select id="cmbIdSitioQuirurgico" style="width:80%" tabindex="123" >
                            <option value=""></option>
                            <%     resultaux.clear();
                                   resultaux=(ArrayList)beanAdmin.combo.cargar(120);	
                                   ComboVO cmbSQ; 
                                   for(int k=0;k<resultaux.size();k++){ 
                                         cmbSQ=(ComboVO)resultaux.get(k);
                            %>
                            <option value="<%= cmbSQ.getId()%>" title="<%= cmbSQ.getTitle()%>"><%= cmbSQ.getDescripcion()%></option>
                            <%}%>
                        </select>                            
                    </td>                                   
                    <td>           	
                        <textarea type="text"  value="" id="txtIndicacion" style="width:90%"  size="500"  maxlength="5000"  tabindex="200" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"></textarea></td>                                               
                    <td>
                        <select id="cmbIdDxRelacionadoP" style="width:80%" tabindex="123" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');" >
                            <option value=""></option>
                        </select> 
                    </td>    
                    <td>
                        <input id="btnProcedimiento"  type="button" class="small button blue" title="JAVA32" value="Adicion"  onclick="guardarYtraerDatoAlListado('buscarSiEsNoPosProcedim')" tabindex="203"   /> 
                    </td>    
                </tr>                                                                            

            </table> 



        </div>
    </div>
</div>






<div id="divVentanitaProcedimientosHistoricos"  style="position:absolute; display:none; background-color:#E2E1A5; top:1px; left:5px; width:900px; height:180px; z-index:999">
    <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">  </div>  
    <div  style="z-index:2057; position:absolute; top:300px; left:50px; height:1000; width:100%">      
        <table width="100%"  class="fondoTabla">           
            <tr class="estiloImput" >
                <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaProcedimientosHistoricos')" /></td>  
                <td align="center">HISTORICOS DE ORDENES DE PLAN DE TRATAMIENTO</td>
                <td colspan="1" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaProcedimientosHistoricos')" /></td>  
            <tr>     
            <tr class="titulos">
                <td colspan="3">                                    
                    <table id="listProcedimientosHistoricos" class="scroll" align="center"></table>
                </td>
            </tr>                
        </table> 
    </div>     
</div>


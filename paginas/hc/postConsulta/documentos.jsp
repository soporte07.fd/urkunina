 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
    

<% 	beanAdmin.setCn(beanSession.getCn()); 
     
    ArrayList resultaux=new ArrayList();
%>

<table width="1300px" align="center" class="fondoTabla">
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="FOLIOS" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" height="500">
       <tr>
        <td>
            <table width="100%" >
               <tr class="titulos" align="center">
                  <td colspan="2">Paciente</td> 
                  <td width="20%">Fecha Desde</td>    
                  <td width="15%">Fecha Hasta</td>  
                  <td width="15%">Estado Folio</td>
               </tr>
               <tr class="estiloImput"> 
                  <td   bgcolor="#FD93B8">
			         		  <select size="1" id="cmbTipoId" style="width:40%" tabindex="100" >
                      <option value="CC">Cedula de Ciudadania</option>
                      <option value="CE">Cedula de Extranjeria</option>
                      <option value="MS">Menor sin Identificar</option>
                      <option value="PA">Pasaporte</option>
                      <option value="RC">Registro Civil</option>
                      <option value="TI">Tarjeta de identidad</option>
                      <option value="CD">Carnet Diplomatico</option>
                      <option value="NV">Certificado nacido vivo</option>
                      <option value="AS">Adulto sin Identificar</option>					
                    </select>
                   <input  type="text" id="txtIdentificacion"  placeholder="N&uacute;mero de Documento"  size="21" maxlength="20"  onKeyPress="javascript:checkKey2(event);" tabindex="101"  />	
                  </td>
                  <td>
	                  <input type="text"   maxlength="70"  id="txtIdBusPaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPaciente',307)"  style="width:99%"  tabindex="99" />
                  </td>                   
                  <td ><input type="text" value=""  size="10"  maxlength="10"  id="txtDocFechaDesde"  tabindex="14" /></td>    
                  <td ><input type="text" value=""  size="10"  maxlength="10"  id="txtDocFechaHasta"  tabindex="14" /></td>                              
                  <td >
                      <select size="1" id="cmbIdEstado" style="width:80%"  tabindex="14">	
                       <option value="">TODAS</option>                                                                                    
                       <option value="1">Finalizado</option>                                             
                       <option value="0">Abierto</option>
                       <option value="12">Pre Finalizado</option> 
                       <option value="7">Cancelado finalizado</option>
                       <option value="8">Finalizado urgente</option>
                       <option value="10">Reprogramado finalizado</option>                                              
                      </select>                        
                  </td> 
              </tr>	 
               <tr class="titulos"> 
                   <td width="20%">Sede</td>
                   <td width="25%">Especialidad</td> 
                   <td width="20%">Profesional</td> 
                   <td width="20%">Auditado</td>
               </tr>     
               <tr class="estiloImput" >
                  <td>
                    <select size="1" id="cmbIdSedeBus" style="width:60%" title="GD38" onfocus="comboDependienteSede('cmbIdSedeBus','557')" ochange="asignaAtributo('cmbIdEspecialidadBus', '', 0); asignaAtributo('cmbIdProfesionales', '', 0);">
                    </select>
                  </td>          
                  <td><select size="1" id="cmbIdEspecialidadBus" style="width:60%" onfocus="cargarEspecialidadDesdeSede('cmbIdSedeBus', 'cmbIdEspecialidadBus')" tabindex="14" onchange="cargarProfesionalesDesdeEspecialidadDocumentos(this.id)" >	                                        
                          <option value=""></option>                           					
                       </select>	                   
                  </td>                   
                  <td><select size="1" id="cmbIdProfesionales" onfocus="cargarProfesionalesDesdeEspecialidad(this.id)" style="width:95%"  tabindex="14">	                                        
                       <option value="" ></option>                                             
                       </select>	                   
                  </td> 
                  <td>
                      <select  id="cmbIdAuditado" style="width:70%"  tabindex="14">	                                        
                       <option value="">TODAS</option>                      
                       <option value="NO">NO</option>
                       <option value="SI">SI</option>    
                       <option value="ER">ERROR</option>      
                       <option value="EC">ERROR CORREGIDO</option>                                                                                     
                      </select>  
                  </td>                   
                  <td>
                       <input name="btn_busDoc" title="BT56E" type="button" class="small button blue" value="BUSCAR FOLIOS" onclick="buscarHC('listDocumentosEvolucion','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');"   />                   
                  </td>               
               </tr>                                       
              <tr>
                <td colspan="5" id="idContenedorGrillaDoc" width="100%" height="400">
                  <table id="listDocumentosEvolucion" class="scroll"></table>            
                </td>
              </tr>
            </table>
     
        </td>
       </tr>

      <tr class="titulosCentrados">
        <TD>
          <TABLE width="100%">
                  <td><label id="lblIdDocumento"></label>&nbsp;&nbsp;&nbsp;<label id="lblTipoDocumento"></label>-<label id="lblDescripcionTipoDocumento"></label>::<label id="lblEspecialidad"></label>  
                  </td>             
                  <td>ESTADO DEL FOLIO= <label id="lblIdEstadoFolio"></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ADMISION=<label id="lblIdAdmision"></label></td>                    
                  <input type="hidden" id="txtIdEsdadoDocumento" value="0" />
                  <label id="lblIdEspecialidad"></label> 
                  <label id="lblIdAuxiliar"></label>                                   
          </table>          
        </TD>
      </tr>       

       <tr class="titulos">
         <td>          
          <input name="btn_busDocu" type="button" class="small button blue" value="VISTA PREVIA" title="b8uu" onClick="formatoPDFHC();"/>          
          <input type="button" class="small button blue" onClick="guardarYtraerDatoAlListado('elementosDelFolio')" value="FINALIZAR"  title="BT95">          
          <input id="btnAuditar"  type="button" class="small button blue" value="AUDITAR" title="BU8R" onClick="modificarCRUD('auditarFolio')"/>                                                      
          <input type="button" value="FINALIZAR POS CONSULTA" title="BT129" class="small button blue" id="btnFinalizarDocumento_"  onclick="finalizaFolioConFecha()">                                                         
         </td>
       </tr>
       <tr>
         <td align="center" colspan="4">&nbsp;
         </td>
       </tr>
       <tr>
         <td align="center" colspan="4">
	   <% if(beanSession.usuario.preguntarMenu("POS") ){%>         
           <TABLE width="100%">
             <TR class="titulos">
               <TD width="20%">AUDITORIA</TD>
               <TD width="60%" align="center" colspan="2">OBSERVACION AUDITORIA</TD>         
               <TD width="20%" align="center" colspan="1">&nbsp;</TD>                
             </TR>               
             <TR  class="estiloImput" >
               <TD width="20%">           
                   <select id="cmbAccionAuditoria" style="width:70%" title="32">	  
                      <option value="SI">SI</option>
                      <option value="NO">NO</option> 
                      <option value="ER">ERROR</option>
                      <option value="EC">ERROR CORREGIDO</option>
                   </select>  
               </TD>
               <TD width="60%" align="center" colspan="2">
                   <input type="text"  id="txtAuditado"  size="100"  maxlength="100"  style="width:90%" />         
               </TD>                  
             </TR>
           </TABLE>          
	   <%} if(beanSession.usuario.preguntarMenu("b611D") ){%>         
           <TABLE width="100%">
             <TR class="titulos">
               <TD width="20%">ACCION AL FOLIO</TD>
               <TD width="60%" align="center" colspan="2">MOTIVO DE LA ACCION</TD>         
               <TD width="20%" align="center" colspan="1">&nbsp;</TD>                
             </TR>               
             <TR  class="estiloImput" >
               <TD width="20%">           
                   <select id="cmbAccionFolio" style="width:70%" title="32">	  
                      <option value="0">ABRIR</option>
                      <option value="1">FINALIZAR</option>                    
                   </select>  
               </TD>
               <TD width="60%" align="center" colspan="2">
                   <input type="text"  id="txtMotivoAccion"  size="100"  maxlength="400"  style="width:90%" />         
               </TD>         
               <TD width="20%" align="center" colspan="1">                      
                   <input id="btnabrirDireMedica"  type="button" class="small button blue" value="EJECUTAR DIRECCION MEDICA" title="BP54R" onClick="modificarCRUD('accionesAlEstadoFolio')"/>                                   
               </TD>                
             </TR>
           </TABLE>                  
       <%} %>           
         </td>
       </tr> 
       <p></p>
       <tr>       
        <td>  
           <table  width="100%">
             <tr class="titulos"> 
              <td>ID_PACIENTE</td>
              <td>IDENTIFICACION</td>
              <td>NOMBRE PACIENTE</td>
              <td>PROFESIONAL</td>                        
             </tr>
             <tr class="estiloImput">
              <td><label id="lblIdPaciente"></label></td>
              <td><label id="lblIdentificacion"></label></td>
              <td><label id="lblNombrePaciente"></label></td>
              <td>PROFESIONAL</td>            
              <td><label hidden="true" id="lblIdentificacionPaciente"></label></td>
             </tr>       
             <tr>
             <tr class="estiloImput">
                      <td colspan="2"><label id="lblAdministradora"></label></td>                                     
                      <td colspan="2"><label id="lblIdPlan"></label>-<label id="lblPlan"></label></td>                                     
             </tr>	
             <tr> 
              <TD colspan="4" >
                  <div id="divAcordionProcedimientos" style="display:BLOCK">
                    <jsp:include page="../hc/divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Conducta y tratamiento" />
                      <jsp:param name="idDiv" value="divProcedimientos" />
                      <jsp:param name="pagina" value="../hc/contenidoPlanTratamiento.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="cargarTablePlanTratamiento()" />
                    </jsp:include>
                  </div>

                  <div id="divAcordionDireccionamiento" style="display:BLOCK">
                    <jsp:include page="../hc/divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Intercambio y Direccionamiento"/>
                      <jsp:param name="idDiv" value="divDireccionamiento"/>
                      <jsp:param name="pagina" value="../hc/contenidoDireccionamiento.jsp"/>
                      <jsp:param name="display" value="NONE"/>
                      <jsp:param name="funciones" value="cargarDireccionamiento()"/>
                    </jsp:include>
                  </div>                  
              </TD>  
            </tr>	                         
            </table>   
        </td>
       </tr>       
      </table>       	  
	  <!-- -->
		<table width="100%" id="historicosAtencion">
                     <div id="divdatosbasicoscredesa" style="  height:50px; width:100%;  ">                       
                      <tr class="camposRepInp" >
                         <td colspan="4">HISTORICOS DE ATENCION</td> 
                      </tr>                      
                     <tr class="titulos" >   
                          <td colspan="4">
                              <!-- PARA MOSTRAR EL LISTADO DE HC  -->
                              <div id="divdatosbasicoscredesa" style="  height:130px; width:100%; overflow-y: scroll;" >
                                    <table id="listDocumentosHistoricos" class="scroll" cellpadding="0" cellspacing="0" align="center">
                                       <tr><th></th></tr>                     
                                    </table>
                              </div>                                  
                          </td>
                      </tr>           
                    </div> 
                  </table>
   </td>
 </tr>  
</table>

<!-------------------------para la vista previa---------------------------- -->

<div id="divVentanitaPdf"  style="display:none; z-index:2050; top:1px; left:190px;">
    <div id="idDivTransparencia" class="transParencia" style="z-index:2054; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
    </div>  
    <div id="idDivTransparencia2"  style="z-index:2055; position:absolute; top:5px; left:15px; width:100%">  
          <table id="idTablaVentanitaPDF" width="90%" height="90" class="fondoTabla" >
             <tr class="estiloImput" >
                 <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaPdf')" /></td>  
                 <td>&nbsp;</td>               
                 <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaPdf')" /></td>  
             </tr>   
             <tr class="estiloImput">  
                 <td colspan="3">                                                
                    <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
                      <tr>
                        <td>
                            <div id="divParaPaginaVistaPrevia" ></div>     
                        </td>
                      </tr>   
                    </table>
                 </td>  
             </tr> 
          </table>
    </div>     
</div> 



    <input type="hidden" id="txtPrincipalHC" value="" />  
    <input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />       
    <input type="hidden"  id="txtIdDocumentoVistaPrevia" value="" />
    <input type="hidden"  id="txtTipoDocumentoVistaPrevia" value="" />                        
    <input type="hidden"  id="lblTituloDocumentoVistaPrevia" value=""  />      
    <input type="hidden"  id="lblIdAdmisionVistaPrevia" value=""  />          
<!-------------------------fin para la vista previa---------------------------- -->      
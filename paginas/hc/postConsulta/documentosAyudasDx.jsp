 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
    

<% 	beanAdmin.setCn(beanSession.getCn()); 
     
    ArrayList resultaux=new ArrayList();
%>

<table width="1400px" align="center" class="fondoTabla">
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="FOLIOS AYUDAS DIAGNOSTICAS" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" height="500">
       <tr>
        <td>

            <table width="100%" >
               <tr class="titulos" align="center">
                  <td colspan="2" >Paciente</td> 
                  <td width="20%">Fecha Desde</td>    
                  <td width="15%">Fecha Hasta</td>  
                  <td width="15%">Estado Doc</td>                              
               </tr>
               <tr class="estiloImput"> 
                  <td   bgcolor="#FD93B8">
					<select size="1" id="cmbTipoId" style="width:15%" tabindex="100" >
                       <option value="">[ Tipo Documento ]</option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                               ComboVO cmb105; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb105=(ComboVO)resultaux.get(k);
                        %>
                      <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>"><%= cmb105.getId()+"  "+cmb105.getDescripcion()%></option>
                                    <%}%>						
                   </select>
                   <input  type="text" id="txtIdentificacion"  placeholder="N&uacute;mero de Documento"  size="21" maxlength="20"  onKeyPress="javascript:checkKey2(event);" tabindex="101"  />	
                  </td>
                  <td>
	                  <input type="text"   maxlength="70"  id="txtIdBusPaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPaciente',307)"  style="width:99%"  tabindex="99" />
                  </td> 
                  
                  <td ><input type="text" value=""  size="10"  maxlength="10"  id="txtDocFechaDesde"  tabindex="14" /></td>    
                  <td ><input type="text" value=""  size="10"  maxlength="10"  id="txtDocFechaHasta"  tabindex="14" /></td>                              
                  <td >
                      <select size="1" id="cmbIdEstado" style="width:80%"  tabindex="14">	                                        
                       <option value="1">Finalizado</option>                                             
                       <option value="0">Abierto</option>
                      </select>                        
                  </td> 
              </tr>	 
               <tr class="titulos">
                   <td width="25%">Especialidad</td> 
                   <td width="20%">Sub Especialidad</td>                
                   <td width="20%">Profesional</td> 
                   <td width="20%"></td>
               </tr>     
               <tr class="estiloImput" >
                   <td><select size="1" id="cmbIdEspecialidadBus" style="width:80%"  tabindex="14" onchange="cargarProfesionalesDesdeEspecialidad(this.id)" >	                                        
                          <option value=""></option>
                            <%     resultaux.clear();
                                   resultaux=(ArrayList)beanAdmin.combo.cargar(100);	
                                   ComboVO cmb32; 
                                   for(int k=0;k<resultaux.size();k++){ 
                                         cmb32=(ComboVO)resultaux.get(k);
                            %>
                          <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>"><%= cmb32.getDescripcion()%></option>
                                        <%}%>						
                       </select>	                   
                   </td>
                   <td>
                       <select size="1" id="cmbIdSubEspecialidadBus" style="width:80%"   onfocus="cargarSubEspecialidadDesdeEspecialidad('cmbIdEspecialidadBus', 'cmbIdSubEspecialidadBus')" onchange="buscarAGENDA('listDias','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" style="width:40%"  tabindex="117"  >	                                        
                          <option value=""></option>
                       </select>   
                   </td> 
                   <td><select size="1" id="cmbIdProfesionales" style="width:95%"  tabindex="14">	                                        
                       <option value="" ></option>  
                            <% resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(126);	
                               ComboVO cmb2; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb2=(ComboVO)resultaux.get(k);
                            %>
                         <option value="<%= cmb2.getId()%>" title="<%= cmb2.getTitle()%>"><%= cmb2.getDescripcion()%></option>
                                        <%}%>                 
                       </select>	                   
                   </td> 
                   <td>
                       <input name="btn_busDoc" title="btn_bu45" type="button" class="small button blue" value="BUSCAR DOC" onclick="buscarHC('listDocumentosEvolucion','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');"   />                   
                   </td>               
               </tr>                                       
              <tr>
                <td colspan="5" id="idContenedorGrillaDoc" width="100%" height="400">
                  <table id="listDocumentosEvolucion" class="scroll"></table>            
                </td>
              </tr>
            </table>
     
        </td>
       </tr>
       <tr class="titulosCentrados">
         <td>Información del documento 
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input name="btn_busDocu" type="button" value="Vista previa" title="b8uu" onClick="formatoPDFHC()"/>
          <!--<input name="btn_busDocu" type="button" value="Vista previa" title="b8uu" onClick=" vistaPreviaDocumentosPostconsulta()"/> -->
          <input id="btnProcedim_" type="button"  value="Desbloqueo" title="BTN_DESBLOQUEO1" onClick="LiberarCandadoPaReporte()"/> 
           <% if(beanSession.usuario.preguntarMenu("612") ){%>
          	<input id="btnAbrirDoc" type="button"  value="Abrir" title="ATENDIDO DOCUMENTO BTN98H5" onClick="modificarCRUD('abrirDocumento1','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')"/>                                             
           <% }%> 
         </td>
       </tr>
        <td>  
           <table  width="100%">
           
             <tr class="titulos"> 
              <td>Id Paciente</td>
              <td>Identificacion</td>
              <td>Nombre Paciente</td>
              <td>PROFESIONAL</td>                        
             </tr>
             <tr class="estiloImput">
              <td><label id="lblIdPaciente"></label></td>
              <td><label id="lblIdentificacion"></label></td>
              <td><label id="lblNombrePaciente"></label></td>
              <td>PROFESIONAL</td>                        
             </tr>       
             <tr>
             <tr class="titulos"> 
              <td>Id Documento</td>                        
              <td>Tipo</td>
              <td>Nombre Documento</td>
              <td>Especialidad</td> 
             </tr>
             <tr class="estiloImput">
              <td><label id="lblIdDocumento"></label>::Admision=<label id="lblIdAdmision"></label></td>                                     
              <td><label id="lblTipoDocumento"></label></td>
              <td><label id="lblDescripcionTipoDocumento"></label></td>
              <td><label id="lblEspecialidad"></label></td>
             </tr>       
             <tr>             
              <td colspan="4">
                  <div id="divParaPlanDeTratamiento"></div> 
              </td>
             </tr>
            </table>   
        </td>
       </tr>
      </table>
      
   </td>
 </tr> 
 
</table>



<!-------------------------para la vista previa---------------------------- -->

<div id="divVentanitaPdf"  style="display:none; z-index:2050; top:1px; left:190px;">

    <div id="idDivTransparencia" class="transParencia" style="z-index:2054; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
    </div>  
    <div id="idDivTransparencia2"  style="z-index:2055; position:absolute; top:5px; left:15px; width:100%">  
          <table id="idTablaVentanitaPDF" width="90%" height="90" class="fondoTabla" >
             <tr class="estiloImput" >
                 <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaPdf')" /></td>  
                 <td>&nbsp;</td>               
                 <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaPdf')" /></td>  
             </tr>   
             <tr class="estiloImput">  
                 <td colspan="3">                                                
                    <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
                      <tr>
                        <td>
                            <div id="divParaPaginaVistaPrevia" ></div>     
                        </td>
                      </tr>   
                    </table>
                 </td>  
             </tr> 
          </table>
    </div>     
</div> 

    <input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />       
    <input type="hidden"  id="txtIdDocumentoVistaPrevia" value="" />
    <input type="hidden"  id="txtTipoDocumentoVistaPrevia" value="" />                        
    <input type="hidden"  id="lblTituloDocumentoVistaPrevia" value=""  />      
    <input type="hidden"  id="lblIdAdmisionVistaPrevia" value=""  />          
<!-------------------------fin para la vista previa---------------------------- -->      
function envioObservacionesSegunNumero(textAreaDestino, texto){

//   cad = document.getElementById(textAreaDestino).value + '\n' + texto
//   cad = cad.substring(0,cad.length-1);

   asignaAtributo(textAreaDestino, document.getElementById(textAreaDestino).value + '\n' + texto ,0 )
   document.getElementById(textAreaDestino).focus(); 

   
}



function cargarTratamientoPorDiente() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/saludOral/tratamientoPorDiente.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarTratamientoPorDiente;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarTratamientoPorDiente() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
          document.getElementById("divParaTratamientoPorDiente").innerHTML = varajaxMenu.responseText;
		  buscarHCOdontologia('listTratamientoPorDiente','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function cargarOdontograma() { //alert('va a limpiaaaar')
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/saludOral/odontograma.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarOdontograma;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarOdontograma() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaOdontograma").innerHTML = varajaxMenu.responseText;
   	        asignaAtributo('lblIdTipoTratamiento','S',0)

		if(valorAtributo('cmbIdTipoTratamiento')=='S'){
			asignaAtributo('lblIdTipoTratamiento','S',0)
			mostrar('G-O') 
			mostrar('O') 
		}	
		else{
		    asignaAtributo('lblIdTipoTratamiento','P',0)
		    ocultar('G-O')/*oclusal no hay para placa*/
		    ocultar('O')/*oclusal no hay para placa*/
		}


			
		if (valorAtributo('cmbIdTipoTratamiento')=='S'){	
		 	ocultar('idTablaPlaca')
		    mostrar('idTablaTratamientos')
		}
		else{	
			ocultar('idTablaTratamientos')
		    mostrar('idTablaPlaca')
		}	

	    iraOdontograma()
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}


function cargarOdontogramaInicial() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/saludOral/odontogramaInicial.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarOdontogramaInicial;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarOdontogramaInicial() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
          document.getElementById("divParaOdontogramaInicial").innerHTML = varajaxMenu.responseText;
		  asignaAtributo('lblIdTipoTratamiento','I',0)
		  iraOdontograma()

        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}


function cargarControlDePlaca() { 
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/saludOral/controlDePlaca.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarControlDePlaca;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarControlDePlaca() {


	if(valorAtributo('cmbIdTipoTratamiento')=='S'){  
	    if (varajaxMenu.readyState == 4) {
	        if (varajaxMenu.status == 200) {   
	          document.getElementById("divParaControlDePlaca").innerHTML = varajaxMenu.responseText;
			  asignaAtributo('lblIdTipoTratamiento','P',0)			  
			  iraOdontograma()		 
	        } else {
	            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
	        }
	    }
	    if (varajaxMenu.readyState == 1) {
	        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
	    }
    } 
    else{
        ocultar('divControlDePlaca')
        alert('PARA VER HISTORICO DE PLACA, EL TIPO DE TAREA DEBE SER SEGUIMIENTO')
    }    


}

//----------------------------------------------------------------------------------------odontograma--------------------------------------------
var trat=0;
var idDien=0;

var diente=new Array();
var dienteIni=new Array();
 
 function caras(){
	   this.V;
	   this.M;
	   this.L;
	   this.D;
	   this.O;	
	   this.sellante;	
	   this.sellanteXhacer;		   
	   this.protesis;
       this.protesisXhacer;	   
       this.protesisRemovible;	   	   
	   this.corona;	
	   this.coronaXhacer;
	   this.nucleo;
	   this.nucleoXhacer;	   
	   this.endodoncia;
	   this.endodonciaXhacer;	   
	   this.exodonciaIndi;
       this.extraido;	
       this.incluido;		   
	   this.nucleo;  
	   this.radiografia;	
	   this.sinErupcionar;	
	   this.erupcion;		   
 }
 
	
 function iraOdontograma(){  
 	$('#drag'+ventanaActual.num).find('#idTratamientoDient').empty(); // limpiar
	$('#drag'+ventanaActual.num).find('#desTratamientoDient').empty(); // limpiar
    //$('#drag'+ventanaActual.num).find('#idDienteElegido').empty();// limpiar al pasar del formulario al odontograma para que no desaparesca el diente
	//$('#drag'+ventanaActual.num).find('#idDienteElegido').append(''); 
    consultarOdontogramaBD();
	diente[100]= new caras();	
 }
 


function consultarOdontogramaBD(){
   	
    if(valorAtributo('lblIdTipoTratamiento')!='P'){		/*PARA PLACA SE NECESITA SABER EL ID FOLIO*/
	   	datos_a_mandar = "accion=consultaOdontograma";		 
		datos_a_mandar = datos_a_mandar+"&idPaciente="+valorAtributo('lblIdPaciente');	
		datos_a_mandar = datos_a_mandar+"&num_trata_historico="+valorAtributo('cmb_num_trata_historico');		
	    datos_a_mandar = datos_a_mandar+"&id_tipo_tratamiento="+valorAtributo('lblIdTipoTratamiento');	
    }else{
	   	datos_a_mandar = "accion=consultaOdontogramaPlaca";		 
		datos_a_mandar = datos_a_mandar+"&idPaciente="+valorAtributo('lblIdPaciente');	
		datos_a_mandar = datos_a_mandar+"&num_trata_historico="+valorAtributo('cmb_num_trata_historico');		
	    datos_a_mandar = datos_a_mandar+"&id_tipo_tratamiento="+valorAtributo('lblIdTipoTratamiento');	
	    if(valorAtributo('cmbIdTipoTratamiento')=='P') /*si estan trabajando en la edicion de la placa*/
	          datos_a_mandar = datos_a_mandar+"&id_evolucion="+valorAtributo('lblIdDocumento');	
	    else  datos_a_mandar = datos_a_mandar+"&id_evolucion="+valorAtributo('lblIdDocumentoPlaca');	
    }


	varajaxInit=crearAjax();
    varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarHc_xml.jsp',true);
	varajaxInit.onreadystatechange=respuestaOdontogramaBD;
	varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxInit.send(datos_a_mandar);   
}
function respuestaOdontogramaBD(){ 
if(varajaxInit.readyState == 4 ){
 if(varajaxInit.status == 200){
  raiz=varajaxInit.responseXML.documentElement;

  if(raiz.getElementsByTagName('diente')!=null){
	   var idDiente=raiz.getElementsByTagName('idDiente');
	   var v=raiz.getElementsByTagName('v');
	   var m=raiz.getElementsByTagName('m');
	   var l=raiz.getElementsByTagName('l');
	   var d=raiz.getElementsByTagName('d');
	   var o=raiz.getElementsByTagName('o');	
	   var idTratam=raiz.getElementsByTagName('idTratam');
	   totalRegistros=idDiente.length;  
	   contCaries=0;
	   contObturad=0;
	   contAusentes=0;	
	   van = false
	   contDient=0;   
	   totPiezasPlaca=32;  
	   totSuperficiesPlaca =0;
	   lblCalculoPlaca =0;

	   for(j=11;j<=85;j++){	  //85
		  if( (j>=11&&j<=18)|| (j>=41&&j<=48)|| (j>=21&&j<=28) || (j>=31&&j<=38) || (j>=51&&j<=55)  || (j>=81&&j<=85) || (j>=61&&j<=65) || (j>=71&&j<=75)   ){	 																	
			  van = false	
			  
			  if(totalRegistros>0){ 
				  for(i=0;i<totalRegistros;i++){	
					  if( j == idDiente[i].firstChild.data ){  /*encontro diligenciado en esta posision*/
						   contDient = i
						   van = true;
						   break;								 
					  }
					  else van = false  
				  }
			  }else van = false
			
			  if(van){ 
				  diente[j]= new caras();	 // se carga por primera vez el array																			
				  diente[j].V=v[contDient].firstChild.data;	
				  diente[j].M=m[contDient].firstChild.data;		
				  diente[j].L=l[contDient].firstChild.data;	
				  diente[j].D=d[contDient].firstChild.data;		
				  diente[j].O=o[contDient].firstChild.data;	
				  diente[j].sellante='';	
				  diente[j].sellanteXhacer='';															
				  diente[j].protesis='';
				  diente[j].protesisXhacer='';
				  diente[j].protesisRemovible='';				  
				  diente[j].corona='';
				  diente[j].coronaXhacer='';
				  diente[j].nucleo='';
				  diente[j].nucleoXhacer='';				  
				  diente[j].endodoncia='';
				  diente[j].endodonciaXhacer='';
				  diente[j].exodonciaIndi='';
				  diente[j].extraido='';
				  diente[j].incluido='';				  
				  diente[j].sinErupcionar='';
				  diente[j].erupcion='';				  
				  
				  if(diente[j].V==4||diente[j].M==4||diente[j].L==4||diente[j].D==4||diente[j].O==4)  //cuantos dientes tienen caries	
				    contCaries=contCaries+1;		
				  if(diente[j].V==1||diente[j].M==1||diente[j].L==1||diente[j].D==1||diente[j].O==1||diente[j].V==2||diente[j].M==2||diente[j].L==2||diente[j].D==2||diente[j].O==2||diente[j].V==3||diente[j].M==3||diente[j].L==3||diente[j].D==3||diente[j].O==3||diente[j].V==5||diente[j].M==5||diente[j].L==5||diente[j].D==5||diente[j].O==5||  diente[j].V==6||diente[j].M==6||diente[j].L==6||diente[j].D==6||diente[j].O==6|| diente[j].V==7||diente[j].M==7||diente[j].L==7||diente[j].D==7||diente[j].O==7)  { 
  				    contObturad=contObturad+1;		  				    
				  }


				  if(diente[j].V==8)
				    totSuperficiesPlaca=totSuperficiesPlaca+1;	
				  if(diente[j].M==8)
				    totSuperficiesPlaca=totSuperficiesPlaca+1;					  
				  if(diente[j].L==8)
				    totSuperficiesPlaca=totSuperficiesPlaca+1;					
				  if(diente[j].D==8)  
				    totSuperficiesPlaca=totSuperficiesPlaca+1;									
				  if(diente[j].O==8)  
				    totSuperficiesPlaca=totSuperficiesPlaca+1;									


				  
				  if( idTratam[contDient].firstChild.data==1	)   diente[j].sellante = 'sellante';  
				  if( idTratam[contDient].firstChild.data==12	)   diente[j].sellanteXhacer = 'sellanteXhacer'; 														
				  if( idTratam[contDient].firstChild.data==2	)   diente[j].protesis = 'protesis';
				  if( idTratam[contDient].firstChild.data==3	)   diente[j].protesisXhacer = 'protesisXhacer';
				  if( idTratam[contDient].firstChild.data==13	)   diente[j].protesisRemovible = 'protesisRemovible';				  
				  if( idTratam[contDient].firstChild.data==4	)   diente[j].corona = 'corona'; 	
				  if( idTratam[contDient].firstChild.data==5	)   diente[j].coronaXhacer = 'coronaXhacer';
				  if( idTratam[contDient].firstChild.data==15	)   diente[j].nucleo = 'nucleo'; 	
				  if( idTratam[contDient].firstChild.data==16	)   diente[j].nucleoXhacer = 'nucleoXhacer';
				  
				  if( idTratam[contDient].firstChild.data==6	)   diente[j].endodoncia = 'endodoncia'; 
				  if( idTratam[contDient].firstChild.data==7	)   diente[j].endodonciaXhacer = 'endodonciaXhacer';
				  if( idTratam[contDient].firstChild.data==8	)   diente[j].exodonciaIndi = 'exodonciaIndi';
				  if( idTratam[contDient].firstChild.data==9	)   { diente[j].extraido = 'extraido';  contAusentes=contAusentes+1;	}
				  if( idTratam[contDient].firstChild.data==14	)   { diente[j].incluido = 'incluido';  }				  
				  if( idTratam[contDient].firstChild.data==10	)   diente[j].sinErupcionar = 'sinErupcionar';
				  if( idTratam[contDient].firstChild.data==11 )   diente[j].erupcion = 'erupcion';	
			  }
			  else{
				  diente[j]= new caras();	 // se carga por primera vez el array																			
				  diente[j].V=0;	
				  diente[j].M=0	
				  diente[j].L=0	
				  diente[j].D=0	
				  diente[j].O=0
				  diente[j].sellante='';	
				  diente[j].sellanteXhacer='';															
				  diente[j].protesis='';
				  diente[j].protesisXhacer='';
				  diente[j].protesisRemovible='';				  
				  diente[j].corona='';
				  diente[j].coronaXhacer='';
				  diente[j].nucleo='';
				  diente[j].nucleoXhacer='';				  
				  diente[j].endodoncia='';
				  diente[j].endodonciaXhacer='';
				  diente[j].exodonciaIndi='';
				  diente[j].extraido='';
			      diente[j].incluido='';
				  diente[j].sinErupcionar='';
				  diente[j].erupcion='';
			  } // if van
		  }  // if
	   }// for 85

  $('#drag'+ventanaActual.num).find('#lblCantCariados').empty(); 									   
  $('#drag'+ventanaActual.num).find('#lblCantCariados').append(contCaries);  

  $('#drag'+ventanaActual.num).find('#lblCantObturados').empty(); 									   
  $('#drag'+ventanaActual.num).find('#lblCantObturados').append(contObturad);  

  $('#drag'+ventanaActual.num).find('#lblCantAusentes').empty(); 									   
  $('#drag'+ventanaActual.num).find('#lblCantAusentes').append(contAusentes); 

  $('#drag'+ventanaActual.num).find('#lblTotalPiezas').empty(); 
  $('#drag'+ventanaActual.num).find('#lblTotalPiezas').append((totPiezasPlaca-contAusentes)); 

  $('#drag'+ventanaActual.num).find('#lblSuperficiesPlaca').empty(); 
  $('#drag'+ventanaActual.num).find('#lblSuperficiesPlaca').append((totSuperficiesPlaca));

  

  var numero =  (totSuperficiesPlaca*100)/( (totPiezasPlaca-contAusentes)  *4);  // es 5 tomando sumando a OCLUSAL, por el contrario es 4
  $('#drag'+ventanaActual.num).find('#lblCalculoPlaca').empty();   
  $('#drag'+ventanaActual.num).find('#lblCalculoPlaca').append((  numero.toFixed(2)  ));



  dibujarOdontoUsuario();
  }   
 }else{
 alert("Problemas de conexion con servidor: "+varajaxInit.status);
 }
}

//alert(99998)
  seleccionarDientePequnoConArea($('#drag' + ventanaActual.num).find('#idDienteElegido').html())

} 
var leftX = 8; topY = 40;
function dibujarOdontoUsuario(){		// es llamado desde cargar el odontograma en funcionesMenu.js   //fabrica los divs de cada diente con parametros con:  No diente, posX , posY */
leftX = 0
topY = 0
	//contDiv=document.getElementById('contenedorOdonto'); 
	for(d=18;d>=11;d--){	                       
		crearDientesDOM(d,leftX,topY+14);	      //del 18 - 11 

		if(valorAtributo('lblIdTipoTratamiento')!='P')		/*PARA PLACA los espacios son mas cortos*/
		     crearDientesDOM(30+d,leftX,topY+167);	  //del 48 - 41				
		else crearDientesDOM(30+d,leftX,topY+117);	  //del 48 - 41				
		
		if(d<=15){

		   if(valorAtributo('lblIdTipoTratamiento')!='P'){ 	/*PARA PLACA NO SE NECESITAN*/
		   		crearDientesDOM(40+d,leftX,topY+65);	  //del 55 - 51
		   		crearDientesDOM(70+d,leftX,topY+117);  //del 85 - 81			
		    }
		}		
	    leftX=leftX+37;	
	}
	for(d=21;d<=28;d++){	
		crearDientesDOM(d,leftX,topY+14);	      //del 21 - 28

		if(valorAtributo('lblIdTipoTratamiento')!='P')	
			 crearDientesDOM(10+d,leftX,topY+167);	  //del 31 - 38		
		else crearDientesDOM(10+d,leftX,topY+117);	  //del 31 - 38		los espacios son mas cortos


		if(d<=25){
			if(valorAtributo('lblIdTipoTratamiento')!='P'){ 	/*PARA PLACA NO SE NECESITAN*/
			   crearDientesDOM(40+d,leftX,topY+65);	  //del 61 - 65
		   	   crearDientesDOM(50+d,leftX,topY+117);  //del 71 - 75			
            }
		}
	    leftX=leftX+37;	
	}
}



 
 function crearDientesDOM(dientConten, posX, posY){  //alert(dientConten +'::'+ posX+'::'+posY)

	contIndex=0;

if(valorAtributo('cmbIdTipoTratamiento')=='S'){  
	if(valorAtributo('lblIdTipoTratamiento')=='S')
		 contenedorDiv=document.getElementById('contenedorOdonto'); 
	else if(valorAtributo('lblIdTipoTratamiento')=='I') 
	     contenedorDiv=document.getElementById('contenedorOdontoIni'); 
	else if(valorAtributo('lblIdTipoTratamiento')=='P') 
	     contenedorDiv=document.getElementById('contenedorOdontoPlaca'); 
}
else contenedorDiv=document.getElementById('contenedorOdonto'); 



	cuerpoDiv=document.createElement('DIV');  // se crean los div hijos pa cada diente
	cuerpoDiv.id='container'+dientConten;

	Nregistro=document.createElement("DIV");   //   div V
	Nregistro.className='pequeno M-V-'+diente[dientConten].V;	  
	Nregistro.id=dientConten+'-M-V';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	  
	
	Nregistro=document.createElement("DIV");    //   div M
	Nregistro.className='pequeno M-M-'+diente[dientConten].M;	  
	Nregistro.id=dientConten+'-M-M';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	  
	
	Nregistro=document.createElement("DIV");	//   div L
	Nregistro.className='pequeno M-L-'+diente[dientConten].L;	  	  
	Nregistro.id=dientConten+'-M-L';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	
	
	Nregistro=document.createElement("DIV");    //   div D
	Nregistro.className='pequeno M-D-'+diente[dientConten].D;	  	  
	Nregistro.id=dientConten+'-M-D';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	







 if(valorAtributo('lblIdTipoTratamiento')!='P'){ 
	Nregistro=document.createElement("DIV");	//   div Oclusal en placa no se presenta jjj
	Nregistro.className='pequeno M-O-'+diente[dientConten].O;	  	  
	Nregistro.id=dientConten+'-M-O';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);
 }
	
	Nregistro=document.createElement("DIV");	//   div Sellante  1    
	if(diente[dientConten].sellante!='')	
	Nregistro.className='pequeno '+diente[dientConten].sellante+'P';	
	Nregistro.id=dientConten+'-M-sellante';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro=document.createElement("DIV");	//   div SellanteXhacer  12
	if(diente[dientConten].sellanteXhacer!='')	
	Nregistro.className='pequeno '+diente[dientConten].sellanteXhacer+'P';	
	Nregistro.id=dientConten+'-M-sellanteXhacer';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	
	Nregistro=document.createElement("DIV");
	if(diente[dientConten].protesis!='')
		Nregistro.className='pequeno '+diente[dientConten].protesis+'P';
	Nregistro.id=dientConten+'-M-protesis';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro=document.createElement("DIV");	//   div protesisXhacer  3    mmmm
	if(diente[dientConten].protesisXhacer!='')
		Nregistro.className='pequeno '+diente[dientConten].protesisXhacer+'P';
	Nregistro.id=dientConten+'-M-protesisXhacer';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	

	Nregistro=document.createElement("DIV");	//   div protesisRemovible  3    mmmm
	if(diente[dientConten].protesisRemovible!='')
		Nregistro.className='pequeno '+diente[dientConten].protesisRemovible+'P';
	Nregistro.id=dientConten+'-M-protesisRemovible';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	


	Nregistro=document.createElement("DIV");   // div corona  4
	if(diente[dientConten].corona!='')	
		Nregistro.className='pequeno '+diente[dientConten].corona+'P' ;	
	Nregistro.id=dientConten+'-M-corona';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);
	
	Nregistro=document.createElement("DIV");   // div coronaXhacer   5
	if(diente[dientConten].coronaXhacer!='')		
		Nregistro.className='pequeno '+diente[dientConten].coronaXhacer+'P';	
	Nregistro.id=dientConten+'-M-coronaXhacer';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	
	
	
	Nregistro=document.createElement("DIV");   // div nucleo  4
	if(diente[dientConten].nucleo!='')	
		Nregistro.className='pequeno '+diente[dientConten].nucleo+'P' ;	
	Nregistro.id=dientConten+'-M-nucleo';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);
	
	Nregistro=document.createElement("DIV");   // div nucleoXhacer   5
	if(diente[dientConten].nucleoXhacer!='')		
		Nregistro.className='pequeno '+diente[dientConten].nucleoXhacer+'P';	
	Nregistro.id=dientConten+'-M-nucleoXhacer';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);		
	

	Nregistro=document.createElement("DIV");	// div endodoncia  6
	if(diente[dientConten].endodoncia!='')			
		Nregistro.className='pequeno '+diente[dientConten].endodoncia+'P';		
	Nregistro.id=dientConten+'-M-endodoncia';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	
	Nregistro=document.createElement("DIV");		// div endodonciaXhacer
	if(diente[dientConten].endodonciaXhacer!='')				
		Nregistro.className='pequeno '+diente[dientConten].endodonciaXhacer+'P';		
	Nregistro.id=dientConten+'-M-endodonciaXhacer';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	

	Nregistro=document.createElement("DIV");	// div exodonciaIndi
	if(diente[dientConten].exodonciaIndi!='')					
		Nregistro.className='pequeno '+diente[dientConten].exodonciaIndi+'P';			
	Nregistro.id=dientConten+'-M-exodonciaIndi';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	
	
	Nregistro=document.createElement("DIV");	// div extraido
	if(diente[dientConten].extraido!='')						
		Nregistro.className='pequeno '+diente[dientConten].extraido+'P';				
	Nregistro.id=dientConten+'-M-extraido';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	
	
	Nregistro=document.createElement("DIV");	// div incluido
	if(diente[dientConten].incluido!='')						
		Nregistro.className='pequeno '+diente[dientConten].incluido+'P';				
	Nregistro.id=dientConten+'-M-incluido';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	
	
	Nregistro=document.createElement("DIV");	// div sinErupcionar	
	if(diente[dientConten].sinErupcionar!='')							
		Nregistro.className='pequeno '+diente[dientConten].sinErupcionar+'P';					
	Nregistro.id=dientConten+'-M-sinErupcionar';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;
	cuerpoDiv.appendChild(Nregistro);	
	
	Nregistro=document.createElement("DIV");		// div erupcion  
	if(diente[dientConten].erupcion!='')								
		Nregistro.className='pequeno '+diente[dientConten].erupcion+'P';						
	Nregistro.id=dientConten+'-M-erupcion';
	Nregistro.style.position='absolute';
	Nregistro.style.top=posY+'px';
	Nregistro.style.left=posX+'px';
	Nregistro.style.zIndex=contIndex++;	 

/*
    table = document.createElement("TABLE");
    row = document.createElement("TR");
	cell = document.createElement("TD");
	cell.width="10%"; 
	cell.title="jas";
	//textNode = document.createTextNode("jas");
	//cell.appendChild(textNode);
	row.appendChild(cell);
	table.appendChild(row);	
	Nregistro.appendChild(table);	*/


	cuerpoDiv.appendChild(Nregistro);	
	contenedorDiv.appendChild(cuerpoDiv);
	
	// fabricar el map de cada diente
	centromapX=posX+17;
	centromapY=posY+17;	 // 37 diametro /2    asi funcia:posY-18

	
	cuerpoMap=document.getElementById('mapAreaOdontograma');
	Nregistro=document.createElement("area"); 
	Nregistro.id=dientConten;	
	Nregistro.shape='circle';
	Nregistro.coords=  centromapX+','+centromapY+','+'18'; 
	Nregistro.href='#'; 
	Nregistro.setAttribute('onclick','seleccionarDientePequnoConArea('+dientConten+')');	 
//	Nregistro.setAttribute('onmouseover','presentarEtiqueta('+dientConten+','+posX+','+posY+')');	 	
//	Nregistro.setAttribute('onmouseout','escondeEtiqueta()');		
	cuerpoMap.appendChild(Nregistro);	
	
 }
 




function seleccionarDientePequnoConArea(ele){ //alert(8888)

	$('#drag'+ventanaActual.num).find('#idDienteElegido').empty();   // ele = diente peque�o elegido en el odontograma pa trabajarlo
	$('#drag'+ventanaActual.num).find('#idDienteElegido').append(ele);   

    diente[100].V='0';	   //limpia el array de diente auxiliar
    diente[100].M='0';		
    diente[100].L='0';	
    diente[100].D='0';		
    diente[100].O='0';	
    diente[100].sellante='';
    diente[100].sellanteXhacer='';	
    diente[100].protesis='';
    diente[100].protesisXhacer='';
    diente[100].protesisRemovible='';	
    diente[100].corona='';	
    diente[100].coronaXhacer='';	
    diente[100].nucleo='';	
    diente[100].nucleoXhacer='';		
    diente[100].endodoncia='';	
    diente[100].endodonciaXhacer='';
    diente[100].exodonciaIndi='';
    diente[100].extraido='';	
    diente[100].incluido='';		
    diente[100].sinErupcionar='';	
    diente[100].erupcion='';	
	
	diente[100].V=diente[ele].V;	 //LLENA EN EL VECTOR AUXILIAR
    diente[100].M=diente[ele].M;	
    diente[100].L=diente[ele].L;	
    diente[100].D=diente[ele].D;	
    diente[100].O=diente[ele].O;	
    diente[100].sellante=diente[ele].sellante;	
    diente[100].sellanteXhacer=diente[ele].sellanteXhacer;		
    diente[100].protesis=diente[ele].protesis;
    diente[100].protesisXhacer=diente[ele].protesisXhacer;
    diente[100].protesisRemovible=diente[ele].protesisRemovible;	
    diente[100].corona=diente[ele].corona;	
    diente[100].coronaXhacer=diente[ele].coronaXhacer;		
    diente[100].nucleo=diente[ele].nucleo;	
    diente[100].nucleoXhacer=diente[ele].nucleoXhacer;			
    diente[100].endodoncia=diente[ele].endodoncia;	
    diente[100].endodonciaXhacer=diente[ele].endodonciaXhacer
    diente[100].exodonciaIndi=diente[ele].exodonciaIndi;
    diente[100].extraido=diente[ele].extraido;		
    diente[100].incluido=diente[ele].incluido;			
    diente[100].sinErupcionar=diente[ele].sinErupcionar;	
    diente[100].erupcion=diente[ele].erupcion;	

	document.getElementById('G-V').className="";	//limpia todos los div grandes
	document.getElementById('G-M').className="";		
	document.getElementById('G-L').className="";	
	document.getElementById('G-D').className="";		
	document.getElementById('G-O').className="";	
	document.getElementById('G-sellante').className="";
	document.getElementById('G-sellanteXhacer').className="";	
	document.getElementById('G-protesis').className="";
	document.getElementById('G-protesisXhacer').className="";	
	document.getElementById('G-protesisRemovible').className="";		
	document.getElementById('G-corona').className="";		
	document.getElementById('G-coronaXhacer').className="";	
	document.getElementById('G-nucleo').className="";		
	document.getElementById('G-nucleoXhacer').className="";		
	document.getElementById('G-endodoncia').className="";		
	document.getElementById('G-endodonciaXhacer').className="";	
	document.getElementById('G-exodonciaIndi').className="";			
	document.getElementById('G-extraido').className="";	
	document.getElementById('G-incluido').className="";		
	document.getElementById('G-sinErupcionar').className="";
	document.getElementById('G-erupcion').className="";

		
	 document.getElementById('G-V').className="grande G-V-"+diente[ele].V;	  // DIBUJAR DIENTE GRANDE A PARTIR DEL DIENTE PEQUE�O ESCOIDO
	 document.getElementById('G-M').className="grande G-M-"+diente[ele].M;	
	 document.getElementById('G-L').className="grande G-L-"+diente[ele].L;	
	 document.getElementById('G-D').className="grande G-D-"+diente[ele].D;	
	// if(valorAtributo('cmbIdTipoTratamiento')!='P'){ // para control de placa este no se debe presentar
	    document.getElementById('G-O').className="grande G-O-"+diente[ele].O;	
	// }

	if(diente[ele].sellante!='')         document.getElementById('G-sellante').className="grande sellanteG";
	if(diente[ele].sellanteXhacer!='')   document.getElementById('G-sellanteXhacer').className="grande sellanteXhacerG";	
	if(diente[ele].protesis!='')         document.getElementById('G-protesis').className="marcos protesisG"; 	
	if(diente[ele].protesisXhacer!='')   document.getElementById('G-protesisXhacer').className="marcos protesisXhacer"; 
	if(diente[ele].protesisRemovible!='')document.getElementById('G-protesisRemovible').className="marcos protesisRemovible"; 	
	if(diente[ele].corona!='') 	         document.getElementById('G-corona').className="grande coronaG"; 
	if(diente[ele].coronaXhacer!='')     document.getElementById('G-coronaXhacer').className="grande coronaXhacerG"; 
	if(diente[ele].nucleo!='') 	         document.getElementById('G-nucleo').className="grande nucleoG"; 
	if(diente[ele].nucleoXhacer!='')     document.getElementById('G-nucleoXhacer').className="grande nucleoXhacerG"; 	
	if(diente[ele].endodoncia!='')       document.getElementById('G-endodoncia').className="grande endodonciaG"; 	
	if(diente[ele].endodonciaXhacer!='') document.getElementById('G-endodonciaXhacer').className="grande endodonciaXhacer"; 
	if(diente[ele].exodonciaIndi!='')    document.getElementById('G-exodonciaIndi').className="grande exodonciaIndiG"; 	
	if(diente[ele].extraido!='')         document.getElementById('G-extraido').className="grande extraidoG"; 
	if(diente[ele].incluido!='')         document.getElementById('G-incluido').className="grande incluidoG"; 	
	if(diente[ele].sinErupcionar!='')    document.getElementById('G-sinErupcionar').className="marcos sinErupcionarG"; 
	if(diente[ele].erupcion!='') 		 document.getElementById('G-erupcion').className="grande erupcionG"; 
		
} 
 
 
 
 
 
function cambiarSeccionDienteGrande(cara){   //alert(cara); //arg=historiaOdontologia
//alert('aqui imprimir para elemento que es radio placa')
//alert( document.getElementById("8").val())


					

 //alert($('#drag' + ventanaActual.num).find('#8' + idElem).attr('checked'))

if(valorAtributo('lblIdTipoTratamiento')=='P' && cara =='O'  &&  ($('#drag'+ventanaActual.num).find('#8').attr('checked') )){ 
 // AL HACER CLIC NO HAGA NADA
 alert('no se puede en esta cara')
}
else
if(trat!=0){
    if($('#drag'+ventanaActual.num).find('#idDienteElegido').html()!=""){
		if(trat=='1'||trat=='2'||trat=='3'||trat=='4'||trat=='5'||trat=='6'||trat=='7'||trat=='8'){
		   document.getElementById('G-'+cara).className="grande G-"+cara+"-"+trat;
		   switch(cara){
			 case 'V': 
				 diente[100].V=trat;
			 break;
			 case 'M': 
				 diente[100].M=trat; 
			 break;	 
			 case 'L': 
				 diente[100].L=trat;
			 break;	 
			 case 'D': 
				 diente[100].D=trat;
			 break;	 
			 case 'O': 
				 diente[100].O=trat;  
			 break;	
		   }
    }   
    else{ 
		   switch(trat){
 		     case 'sellante':  
			    if(diente[100].sellante!=trat){				
			       diente[100].sellante=trat;				
	               document.getElementById('G-sellante').className="grande sellanteG"; 
				}
				else{
			       diente[100].sellante="";				
	               document.getElementById('G-sellante').className=""; 
				}
		     break;
 		     case 'sellanteXhacer':  
			    if(diente[100].sellanteXhacer!=trat){				
			       diente[100].sellanteXhacer=trat;				
	               document.getElementById('G-sellanteXhacer').className="grande sellanteXhacerG"; 
				}
				else{
			       diente[100].sellanteXhacer="";				
	               document.getElementById('G-sellanteXhacer').className=""; 
				}
		     break;
	
 		     case 'protesis':  
			    if(diente[100].protesis!=trat){	
				   diente[100].protesis=trat;
				   document.getElementById('G-protesis').className="marcos protesisG"; 	
		     	}
				else{
			       diente[100].protesis="";				
	               document.getElementById('G-protesis').className=""; 
				}
			 break;				 
 		     case 'protesisXhacer':  
			    if(diente[100].protesisXhacer!=trat){	
				   diente[100].protesisXhacer=trat;
				   document.getElementById('G-protesisXhacer').className="marcos protesisXhacer"; 	
		     	}
				else{
			       diente[100].protesisXhacer="";				
	               document.getElementById('G-protesisXhacer').className=""; 
				}
			 break;	
 		     case 'protesisRemovible':  
			    if(diente[100].protesisRemovible!=trat){	
				   diente[100].protesisRemovible=trat;
				   document.getElementById('G-protesisRemovible').className="marcos protesisRemovible"; 	
		     	}
				else{
			       diente[100].protesisRemovible="";				
	               document.getElementById('G-protesisRemovible').className=""; 
				}
			 break;	
			 
 		     case 'corona':  
			    if(diente[100].corona!=trat){	
				   diente[100].corona=trat;
				   document.getElementById('G-corona').className="grande coronaG"; 
		     	}
				else{
			       diente[100].corona="";				
	               document.getElementById('G-corona').className=""; 
				}		
		     break;				 
 		     case 'coronaXhacer':
			    if(diente[100].coronaXhacer!=trat){	 
				   diente[100].coronaXhacer=trat;
				   document.getElementById('G-coronaXhacer').className="grande coronaXhacerG"; 
		     	}
				else{
			       diente[100].coronaXhacer="";				
	               document.getElementById('G-coronaXhacer').className=""; 
				}		
		     break;	
 		     case 'nucleo':  
			    if(diente[100].nucleo!=trat){	
				   diente[100].nucleo=trat;
				   document.getElementById('G-nucleo').className="grande nucleoG"; 
		     	}
				else{
			       diente[100].nucleo="";				
	               document.getElementById('G-nucleo').className=""; 
				}		
		     break;				 
 		     case 'nucleoXhacer':
			    if(diente[100].nucleoXhacer!=trat){	 
				   diente[100].nucleoXhacer=trat;
				   document.getElementById('G-nucleoXhacer').className="grande nucleoXhacerG"; 
		     	}
				else{
			       diente[100].nucleoXhacer="";				
	               document.getElementById('G-nucleoXhacer').className=""; 
				}		
		     break;	
			 
 		     case 'endodoncia':  
			    if(diente[100].endodoncia!=trat){		
				   diente[100].endodoncia=trat;
				   document.getElementById('G-endodoncia').className="grande endodonciaG"; 	
		     	}
				else{
			       diente[100].endodoncia="";				
	               document.getElementById('G-endodoncia').className=""; 
				}				   
		     break;				 
 		     case 'endodonciaXhacer':  
			    if(diente[100].endodonciaXhacer!=trat){		
				   diente[100].endodonciaXhacer=trat;
				   document.getElementById('G-endodonciaXhacer').className="grande endodonciaXhacer"; 	
		     	}
				else{
			       diente[100].endodonciaXhacer="";				
	               document.getElementById('G-endodonciaXhacer').className=""; 
				}				 
	         break;
 		     case 'exodonciaIndi': 
			    if(diente[100].exodonciaIndi!=trat){				 
				   diente[100].exodonciaIndi=trat;
				   document.getElementById('G-exodonciaIndi').className="grande exodonciaIndiG"; 	
		     	}
				else{
			       diente[100].exodonciaIndi="";				
	               document.getElementById('G-exodonciaIndi').className=""; 
				}	
		     break;	
 		     case 'extraido': 
			    if(diente[100].extraido!=trat){				 
				   diente[100].extraido=trat;
				   document.getElementById('G-extraido').className="grande extraidoG"; 	
		     	}
				else{
			       diente[100].extraido="";				
	               document.getElementById('G-extraido').className=""; 
				}	
		     break;	
 		     case 'incluido': 
			    if(diente[100].incluido!=trat){				 
				   diente[100].incluido=trat;
				   document.getElementById('G-incluido').className="grande incluidoG"; 	
		     	}
				else{
			       diente[100].incluido="";				
	               document.getElementById('G-incluido').className=""; 
				}	
		     break;	
			 
 		     case 'sinErupcionar': 
			    if(diente[100].sinErupcionar!=trat){	 			 
				   diente[100].sinErupcionar=trat;
				   document.getElementById('G-sinErupcionar').className="marcos sinErupcionarG"; 	
		     	}
				else{
			       diente[100].sinErupcionar="";				
	               document.getElementById('G-sinErupcionar').className=""; 
				}	
		     break;	
 		     case 'erupcion': 
			    if(diente[100].erupcion!=trat){	 			 
				   diente[100].erupcion=trat;
				   document.getElementById('G-erupcion').className="grande erupcionG"; 	
		     	}
				else{
			       diente[100].erupcion="";				
	               document.getElementById('G-erupcion').className=""; 
				}	
		     break;	
			 
	       }
    }
    if(trat=='limpiaProcedCara'){
		idDienteLimpiar=$('#drag'+ventanaActual.num).find('#idDienteElegido').html();

		 switch(cara){
		   case 'V': 
			   diente[100].V=diente[idDienteLimpiar].V;
			   document.getElementById('G-'+cara).className="grande G-"+cara+"-"+diente[idDienteLimpiar].V;
		   break;
		   case 'M': 
			   diente[100].M=diente[idDienteLimpiar].M; 
			   document.getElementById('G-'+cara).className="grande G-"+cara+"-"+diente[idDienteLimpiar].M;			   
		   break;	 
		   case 'L': 
			   diente[100].L=diente[idDienteLimpiar].L;
			   document.getElementById('G-'+cara).className="grande G-"+cara+"-"+diente[idDienteLimpiar].L;			   
		   break;	 
		   case 'D': 
			   diente[100].D=diente[idDienteLimpiar].D;
			   document.getElementById('G-'+cara).className="grande G-"+cara+"-"+diente[idDienteLimpiar].D;			   
		   break;	 
		   case 'O': 
			   diente[100].O=diente[idDienteLimpiar].O; 
			   document.getElementById('G-'+cara).className="grande G-"+cara+"-"+diente[idDienteLimpiar].O;			   
  
		   break;	
		   
		 }	
	 }  
     if(trat=='limpiaTodoProced')  
	   seleccionarDientePequnoConArea($('#drag'+ventanaActual.num).find('#idDienteElegido').html())	 
  }
  else alert("Seleccione el diente a Tratar");

 }
 else alert("Seleccione un tratamiento");

  guardarDienteYTratamiento()
}





 
function gestionTratam(elemn, nom_tratamiento){// alert('987987 elemn='+elemn+'::'+ 'nom_tratamiento='+nom_tratamiento)
    trat=elemn;
	
	$('#drag'+ventanaActual.num).find('#idTratamientoDient').empty();

	if(trat=='4')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('01'); //caries
	if(trat=='1')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('02'); //Obturacion en amalgama buen estado 
	if(trat=='5')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('03'); //Obturacion en amalgama en MAL estado	
	if(trat=='2')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('04'); //Obturacion en resina buen estado
	if(trat=='6')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('05'); //Obturacion en resina MAL estado
	if(trat=='3')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('06'); //Obturacion temporal buen estado
	if(trat=='7')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('07'); //Obturacion temporal MAL estado	
	if(trat=='8')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('08'); //placa
	if(trat=='sellante')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('1'); 
	if(trat=='sellanteXhacer')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('12');
	if(trat=='protesis')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('2');
	if(trat=='protesisXhacer')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('3');
	if(trat=='protesisRemovible')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('13');	
	if(trat=='corona')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('4');
	if(trat=='coronaXhacer')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('5');
	if(trat=='nucleo')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('15');
	if(trat=='nucleoXhacer')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('16');	
	if(trat=='endodoncia')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('6');
	if(trat=='endodonciaXhacer')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('7');
	if(trat=='exodonciaIndi')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('8');
	if(trat=='extraido')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('9');
	if(trat=='incluido')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('14');	
	if(trat=='sinErupcionar')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('10'); 	
	if(trat=='erupcion')  $('#drag'+ventanaActual.num).find('#idTratamientoDient').append('11');
	
	$('#drag'+ventanaActual.num).find('#desTratamientoDient').empty(); 
    $('#drag'+ventanaActual.num).find('#desTratamientoDient').append(nom_tratamiento);  	
}

function guardarDienteYTratamiento(){ 
    modificarCRUDOdontologia('guardarDienteYTratamiento')    
}


function modificarCRUDOdontologia(arg, pag){

    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;
    
    if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 8 || valorAtributo('txtIdEsdadoDocumento') == 9 || valorAtributo('txtIdEsdadoDocumento') == 10) {
        alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR');
        arg ='';
    }


    switch (arg) {
	  	


        case 'eliminarTratamientoPorDiente':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=702&parametros=";                
                add_valores_a_mandar(valorAtributo('lblIdTnsDienteTratamien'));
                ajaxModificar();
            }
        break;
        case 'modificarTratamientoPorDiente':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=847&parametros=";
                add_valores_a_mandar(valorAtributo('txtDesTratamientoPorDiente'));                
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdTnsDienteTratamien'));
                ajaxModificar();
            }
        break;        
        case 'tratamientoPorDiente':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=700&parametros=";                
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('txtDesTratamientoPorDiente'));
                add_valores_a_mandar(IdSesion());
                ajaxModificar();
            }
            break;


        case 'guardarDienteYTratamiento':
            if (verificarCamposGuardar(arg)) {
                var id_trataDiente = 0

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=698&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoTratamiento'));
                add_valores_a_mandar($('#drag' + ventanaActual.num).find('#idDienteElegido').html());

                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoTratamiento'));
                add_valores_a_mandar($('#drag' + ventanaActual.num).find('#idDienteElegido').html());
                add_valores_a_mandar(diente[100].V); //v
                add_valores_a_mandar(diente[100].M); //m
                add_valores_a_mandar(diente[100].L); //l
                add_valores_a_mandar(diente[100].D); //d
                add_valores_a_mandar(diente[100].O); //o        

                if (diente[100].sellante == 'sellante')
                    id_trataDiente = 1
                else if (diente[100].sellanteXhacer == 'sellanteXhacer')
                    id_trataDiente = 12
                else if (diente[100].protesis == 'protesis')
                    id_trataDiente = 2
                else if (diente[100].protesisXhacer == 'protesisXhacer')
                    id_trataDiente = 3
                else if (diente[100].protesisRemovible == 'protesisRemovible')
                    id_trataDiente = 13

                if (diente[100].corona == 'corona')
                    id_trataDiente = 4
                else if (diente[100].coronaXhacer == 'coronaXhacer')
                    id_trataDiente = 5
                if (diente[100].nucleo == 'nucleo')
                    id_trataDiente = 15
                else if (diente[100].nucleoXhacer == 'nucleoXhacer')
                    id_trataDiente = 16
                else if (diente[100].endodoncia == 'endodoncia')
                    id_trataDiente = 6
                else if (diente[100].endodonciaXhacer == 'endodonciaXhacer')
                    id_trataDiente = 7
                else if (diente[100].exodonciaIndi == 'exodonciaIndi')
                    id_trataDiente = 8
                else if (diente[100].extraido == 'extraido')
                    id_trataDiente = 9
                else if (diente[100].incluido == 'incluido')
                    id_trataDiente = 14
                else if (diente[100].sinErupcionar == 'sinErupcionar')
                    id_trataDiente = 10
                else if (diente[100].erupcion == 'erupcion')
                    id_trataDiente = 11

                add_valores_a_mandar(id_trataDiente); //idtratam
                //  alert('valores_a_mandar='+valores_a_mandar)
                ajaxModificar();
            }
            break;
        case 'limpiarDiente':
            if (verificarCamposGuardar(arg)) {
                var id_trataDiente = 0
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=698&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoTratamiento'));
                add_valores_a_mandar($('#drag' + ventanaActual.num).find('#idDienteElegido').html());

                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoTratamiento'));
                add_valores_a_mandar($('#drag' + ventanaActual.num).find('#idDienteElegido').html());
                add_valores_a_mandar('0'); //v
                add_valores_a_mandar('0'); //m
                add_valores_a_mandar('0'); //l
                add_valores_a_mandar('0'); //d
                add_valores_a_mandar('0'); //o                
                id_trataDiente = 0
                add_valores_a_mandar(id_trataDiente); //idtratam
                //alert('valores_a_mandar='+valores_a_mandar)
                ajaxModificar();
            }
            break;    
        case 'eliminarTratamientoInicial':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=710&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                ajaxModificar();
            }
        break;   
        case 'dejarTratamientoInicial':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=699&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                ajaxModificar();
            }
            break;                         
    }
}

function respuestaModificarCRUDOdontologia(arg, xmlraiz) {
    if (xmlraiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
        switch (arg) {
			case 'eliminarTratamientoPorDiente':
                cargarTratamientoPorDiente()
                break;
			case 'modificarTratamientoPorDiente':
                cargarTratamientoPorDiente()
                break;                
            case 'tratamientoPorDiente':
                cargarTratamientoPorDiente()
                break;        	
			case 'guardarDienteYTratamiento':
                cargarOdontograma()
            break;
			case 'limpiarDiente':
                cargarOdontograma()
            break;  
            case 'eliminarTratamientoInicial':
                cargarOdontogramaInicial()
            break;     
			case 'dejarTratamientoInicial':
                cargarOdontogramaInicial()
            break;                             
        }
    }
}








function respuestaGuardarOdontogramaBDconAjax(){
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('respuesta')[0].firstChild.data=='true'){
				  alert("Se guardo el odontograma de este paciente");

			  }else{
				  alert("No puedo guardar los cambios al odontograma de este paciente");
			     }
			 }else{
					alert("Problemas de conexion con servidor: "+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}





function buscarHCOdontologia(arg, pag) { 
    pagina = pag;
    ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
    //    alto= ($('#drag'+ventanaActual.num).find("#divContenido").width())*92/100;  
    switch (arg) {

         case 'listTratamientoPorDiente':
            ancho = ($('#drag' + ventanaActual.num).find("#idTablaTratamEjec").width()) - 5;
            limpiarDivEditarJuan(arg);

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=701&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#listTratamientoPorDiente").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'Descripcion del Tratamiento', 'Profesional', 'Fecha elaboro', 'Folio', 'id_Estado_folio', 'Estado_folio'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'descripcion', index: 'descripcion', width: anchoP(ancho, 72) },
                    { name: 'Profesional', index: 'Profesional', width: anchoP(ancho, 10) },
                    { name: 'Fecha', index: 'Fecha', width: anchoP(ancho, 9) },
                    { name: 'folio', index: 'folio', width: anchoP(ancho, 4) },
                    { name: 'id_estado_folio', index: 'id_estado_folio', hidden: true },
                    { name: 'estado_folio', index: 'estado_folio', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    if (datosRow.id_estado_folio != '1') {
                        asignaAtributo('lblIdTnsDienteTratamien', datosRow.id, 0)
                        asignaAtributo('txtDesTratamientoPorDiente', datosRow.descripcion, 0)
                    } else {
                        alert('PARA PODER EDITAR, EL ESTADO DEL FOLIO DEBE ESTAR ABIERTO')
                    }
                },
                height: 140,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#listTratamientoPorDiente").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            $('#drag' + ventanaActual.num).find("#listTratamientoPorDiente").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
        break;
		case 'listPlaca':
            ancho = 1100;
            limpiarDivEditarJuan(arg);

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=848&parametros="; 
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));


            $('#drag' + ventanaActual.num).find("#listPlaca").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Piezas','Superficies','Ausentes','Calculo', 'Profesional', 'Fecha elaboro', 'Folio', 'id_Estado_folio', 'Estado folio'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },                    
                    
                    { name: 'piezas', index: 'piezas', width: anchoP(ancho, 18) },
                    { name: 'superficies', index: 'superficies', width: anchoP(ancho, 18) },
                    { name: 'ausentes', index: 'ausentes', width: anchoP(ancho, 18) },
                    { name: 'calculo', index: 'calculo', width: anchoP(ancho, 18) },

                    { name: 'Profesional', index: 'Profesional', width: anchoP(ancho, 10) },
                    { name: 'Fecha', index: 'Fecha', width: anchoP(ancho, 9) },
                    { name: 'folio', index: 'folio', width: anchoP(ancho, 4) },
                    { name: 'id_estado_folio', index: 'id_estado_folio', hidden: true },
                    { name: 'estado_folio', index: 'estado_folio', width: anchoP(ancho, 5) },
                ],
  				onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    if (datosRow.id_estado_folio != '1') {
                        asignaAtributo('lblIdDocumentoPlaca', datosRow.folio, 0)
                        

						if(valorAtributo('cmbIdTipoTratamiento')=='S'){  
                        	cargarControlDePlaca()

                        } else alert('EL TIPO DE TAREA DEBE ESTAR EN SEGUIMIENTO')    
                    } else {
                        alert('PARA PODER EDITAR, EL ESTADO DEL FOLIO DEBE ESTAR ABIERTO')
                    }
                },                

                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#listPlaca").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');            
            $('#drag' + ventanaActual.num).find("#listPlaca").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');            
        break;        
    }
}        
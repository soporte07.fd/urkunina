

<table id="documentoPDF" width="100%" border="1" cellpadding="1" cellspacing="1" align="center">                          
  
  <tr>
    <td>  
       <TABLE width="100%" class="fondoTablaPdf">    
          <tr>
            <td class="tituloLineaInferior">1.<label id="lblTituloDocumento"/></label>
            </td>
            <td>
               <jsp:include page="divsSinAcordeon.jsp" flush="FALSE" >
                      <jsp:param name="pagina" value="../documentosHistoriaClinicaPDF/encabezado.jsp" />                
                      <jsp:param name="display" value="block" />                
               </jsp:include>
            </td>
          </tr>   
       </TABLE>   
    </td>   
  </tr>   
  
  <tr bgcolor="#FFFFFF">
    <td>  
       <TABLE width="100%" class="fondoTablaPdf" id="idInfoTitulo">    
          <tr id="idTrInfoTitulo">
            <td class="tituloLineaInferior">1.INFORMACION DEL PACIENTE
            </td>
          </tr>   
          <tr  id="documentoPDFInfoPaciente">
            <td width="100%">
              <table width="100%" id="listPaciente1PDF" cellpadding="1" cellspacing="0" align="CENTER">
               <tr><th></th></tr>  
              </table>
              <table width="100%" id="listPaciente2PDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
              <table width="100%" id="listPaciente3PDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>              
       </TABLE> 
      </td>
  </tr>  
  
  <tr bgcolor="#FFFFFF">
    <td>  
       <TABLE width="100%"  class="fondoTablaPdf">
          <tr>
            <td width="100%">
              <table width="100%" id="listMotivoEnfermedadPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
          
       </TABLE> 
      </td>
  </tr>
   
  
  <tr  id="idAntecedentesReport">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf" style="display:none">
            <tr>
             <td class="tituloLineaInferior">4.Antecedentes
             </td>
            </tr>
            <tr>
            <td width="100%">
              <table width="100%" id="listAntecedentesPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr>
  

  <tr bgcolor="#FFFFFF"  id="idRevisionSistemas">
     <td>  
		<div id="idDivRevisionSistemas" style="display:block">     
         <TABLE width="100%" class="fondoTablaPdf">
            <tr>
               <td class="tituloLineaInferior">5.Revisi&oacute;n por sistemassss
               </td>
            </tr>
            <tr>
              <td width="100%">
                <table width="100%" id="listSistemasPDF" cellpadding="1" cellspacing="1" >
                 <tr><th></th></tr>  
                </table>
              </td>
            </tr> 
         </TABLE> 
		</div>         
      </td>
  </tr>  

  
  
  <tr bgcolor="#FFFFFF" id="idExamenFisicReport">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf" >
          <tr>
             <td class="tituloLineaInferior">6.Examen f&iacute;sico
             </td>
          </tr>
          <tr>
            <td width="100%">
              <table width="100%" id="listSignosVitalesPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr>  
  
  
  <tr>
     <td>  
       <TABLE width="100%" class="fondoTablaPdf" id="idInfoTitulo2">    
          <tr id="idTrInfoTitulo2">
             <td class="tituloLineaInferior">7.Examen Especialidad
             </td>
          </tr>
          <tr>
            <td width="100%">
				<div id="divParaExamenEspecialidad"></div> 
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr>  
  
  <tr bgcolor="#FFFFFF">
     <td>  
     <div id="idDivDiagnosticos" style="display:block">   
       <TABLE width="100%" class="fondoTablaPdf" id="idInfoTitulo3">    
          <tr id="idTrInfoTitulo3">
             <td class="tituloLineaInferior">8.Diagn&oacute;sticos
             </td>
          </tr>
          <tr>
            <td width="100%">
              <table width="100%" id="listDiagnosticosPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
       </div>
      </td>  
  </tr>  
  
  
  
  <tr bgcolor="#FFFFFF">
     <td> 
      <div id="idDivPlan" style="display:block">    
       <TABLE width="100%" class="fondoTablaPdf" id="idInfoTitulo4">    
          <tr id="idTrInfoTitulo4">
             <td class="tituloLineaInferior">9.Pl&aacute;n de Tratamiento
             </td>
          </tr>
          <tr>
            <td width="100%">
              <table width="100%" id="listPlanPDF" cellpadding="1"  cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </div>
      </td>  
  </tr>  
  <tr bgcolor="#FFFFFF">
     <td> 
      <div id="idDivMedicamentos" style="display:block">    
       <TABLE width="100%" class="fondoTablaPdf" id="idInfoTitulo4">    
          <tr id="idTrInfoTitulo5">
             <td class="tituloLineaInferior">Medicamentos
             </td>
          </tr>
          <tr>
            <td width="100%">
              <table width="100%" id="listMedicamentosPDF" cellpadding="1"  cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr>           
       </TABLE> 
      </div>
      </td>  
  </tr>    
  
  <tr bgcolor="#FFFFFF">&nbsp;
     <td valign="top">  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
            <td width="100%" valign="top">
              <table width="100%" id="listProfesionalPDF" cellpadding="1"  cellspacing="1">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr>      
</table>     

     <TABLE width="40%" class="fondoTablaPdf" align="center">
       <tr>
         <td width="100%" align="center">
           <table width="100%" id="listFinalizarImprimirPDF">
            <tr><th></th></tr>  
           </table>
         </td>
       </tr>             
     </TABLE>                         
     
     
<div id="divVentanitaOpcImpresion"  style="display:none; z-index:2050; top:200px; left:190px;">

    <div class="transParencia" style="z-index:2054; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
    </div>  
    <div  style="z-index:2055; position:absolute; top:200px; left:215px; width:100%">  
          <table width="40%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
             <tr class="estiloImput" >
                 <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaOpcImpresion')" /></td>  
                 <td>&nbsp;</td>               
                 <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaOpcImpresion')" /></td>  
             </tr>   
             <tr class="estiloImput">  
                 <td colspan="3">                                                
                    <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
                      <tr>
                        <td>TIPO DE IMPRESION:</td>
                        <td>
						  <select id="cmbOpcImpresion" style="width:95%"  tabindex="14">	                                        
                            <option value="Resumen">Resumen</option>                                                              
                            <option value="ResumenFacturacion">Resumen Facturacion</option>
                            <option value="Todo">Todo</option>                                                              
                          </select>                        
                        </td> 
                        <td>
                           <input name="btn_busDoc" type="button" class="small button blue" value="IMPRIMIR" onclick="caseImprimirDoc()" />                   
                        </td>                      
                      </tr>   
                    </table>
                 </td>  
             </tr> 
                   
          </table>
    </div>     
</div> 
             
      



     <TABLE width="80%" class="fondoTablaPdf" align="center">
       <tr>
         <td width="100%" align="center">
<table id="documentoPDF" width="100%" border="1" cellpadding="1" cellspacing="1" align="center">                          
  <tr bgcolor="#FFFFFF">
    <td>

      <table  width="100%"  cellpadding="1" cellspacing="1" align="center" class="letraTamano2" >
        <tr>
          <td align="center"><b>FORMULARIO DE SOLICITUD DE MEDICAMENTOS NO P.O.S.S</b></td>
        </tr>
        <tr>
          <td align="center"><b>(Es necesario que el medico solicitante diligencie todos los espacios en blanco)</b></td>    
        </tr>
        <tr>
          <td align="center">
              <table width="50%" id="listHoraFechaNoSolicitudPDF" cellpadding="1" cellspacing="0" align="CENTER">
               <tr><th></th></tr>  
              </table>           
          </td>    
        </tr>  
      </table> 
    
    
    </td>
  </tr>
  <tr>
    <td>  
       <TABLE width="100%" class="fondoTablaPdf">    
          <tr>
            <td width="25%" class="tituloLineaInferior">NOMBRE DE LA IPS RECEPTORA</td>
            <td width="75%" rowspan="2">
              <table width="100%" id="listAdministradoraPDF" cellpadding="1" cellspacing="0" align="CENTER">
               <tr><th></th></tr>  
              </table>               
            </td>                  
          </tr> 
          <tr>
            <td class="tituloLineaInferior">Fundaci&oacute;n Oftalmol&oacute;gica de Nari&ntilde;o</td>
          </tr>  
       </TABLE> 
    </td>   
  </tr>  

  <tr bgcolor="#FFFFFF">
    <td>  
       <TABLE width="100%" class="fondoTablaPdf">    
          <tr>
            <td class="tituloLineaInferior">DATOS DEL PACIENTE
            </td>
          </tr>   
          <tr>
            <td width="100%">
              <table width="100%" id="listPaciente2PDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr>  
       </TABLE> 
      </td>
  </tr>  
  
  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
             <td class="tituloLineaInferior">DIAGNOSTICO
             </td>
          </tr>
          <tr>
            <td width="100%">
              <table width="100%" id="listDiagnosticosPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr> 
  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
            <td width="100%">
              <table width="100%" id="listResumenEnferActualTratamientoPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr>   
  <tr bgcolor="#FFFFFF">
    <td width="100%">
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>  
             <td class="tituloLineaInferior">Medicamento POS-S utilizados previamente para el manejo de esta patolog&#237;a sin obtener respuesta cl&#237;nica o paraclinica satisfactoria en el termino previsto de sus indicaciones.</td>  
          </tr> 
       </TABLE>              
     </td>   
  </tr>  

  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
             <td class="tituloLineaInferior">Medicamento del POS-S utilizados(1)
             </td>
          </tr>       
          <tr>
            <td width="100%">
              <table width="100%" id="listMedicamentoUtilizado1PDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr> 
  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
             <td class="tituloLineaInferior">Medicamento del POS-S utilizados(2)
             </td>
          </tr>       
          <tr>
            <td width="100%">
              <table width="100%" id="listMedicamentoUtilizado2PDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr>  

  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
             <td class="tituloLineaInferior">Medicamento No POS-S Solicitado
             </td>
          </tr>       
          <tr>
            <td width="100%">
              <table width="100%" id="listMedicamentoNoPosSolicitadoPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr> 
  
  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
            <td width="100%">
              <table width="100%" id="listPreguntasPDF" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr>  

  
  <tr bgcolor="#FFFFFF">&nbsp;
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
            <td width="60%">
              <table width="100%" id="listProfesionalPDF" cellpadding="1"  cellspacing="1">
               <tr><th></th></tr>  
              </table>
            </td>
            <td width="40%">&nbsp;
            </td>            
          </tr> 
       </TABLE> 
      </td>  
  </tr>      
</table>           
         </td>
          
         <!-- celda para los botones de edicion -->
		 <td width="100%" align="center" valign="top">   
         <table>
			<tr><td><BR /><BR /><BR />.</td></tr>
            <tr><td><BR /><BR /><BR />.</td></tr> 
			<tr><td><BR /><BR /><BR />.</td></tr> 
			<tr><td><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR />.</td></tr>             
			<tr>
            	<td><input type="button" value="Editar" onclick="mostrarEdicionDeAnexo(); " title="Editar el resumen de la enfermedad actual"/></td>
             </tr> 
            <tr><td><BR /><BR />.</td></tr> 
            <tr><td><BR /><BR /><BR /><BR />.</td></tr> 
            <tr><td><BR /><BR />.</td></tr> 
            <tr><td><input type="button" value="Editar" onclick="mostrarEdicionDeAnexo()"/></td></tr> 
            <tr><td><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR />
            	<input type="button" value="Editar" onclick="mostrarEdicionDeAnexo()"/></td></tr> 
            <tr><td><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR />.</td></tr>                       
         </table>
         </td>
         
         
         <!-- celda para mostrar los campos de edicion -->         
         <td width="100%" align="center">
         	<div id="divCamposEdicion" style="display:none">
         	<table>
            	<tr><td>.</td></tr>
                <tr><td><BR /><BR /><BR />.</td></tr> 
                <tr><td><BR /><BR />.</td></tr> 
                <tr><td><BR /><BR /><BR /><BR /><BR /><BR /><BR />.</td></tr>             
                <tr class="estiloImput"> 
                    <td>
                    	<textarea style="border:1px solid " id="txt_resumen_enf" placeholder="Resumen de la enfermedad Actual..."></textarea>
                        <input type="button" title="bnt_i45s" value="Guardar" onclick="modificarCRUD('editarAnexoPDF1','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"/>
                    </td>
                 </tr> 
                <tr><td><BR /><BR />.</td></tr> 
                <tr><td><BR /><BR />.</td></tr> 
                <tr><td><BR /><BR />.</td></tr> 
                <tr class="estiloImput"> 
                	<td>
	                    <textarea style="border:1px solid " id="txt_indicacion_terapeutica" placeholder="Indicacion terapeutica..."></textarea>
                        <input type="button" value="Guardar" onclick="modificarCRUD('editarAnexoPDF2','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"/>                        
                    </td>
                 </tr> 
                <tr class="estiloImput"> 
                	<td><BR /><BR /><BR /><BR /><BR />
                   		<textarea style="border:1px solid " id="txt_explicaciones" placeholder="Explicación..."></textarea>
                        <input type="button" value="Guardar" onclick="modificarCRUD('editarAnexoPDF3','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"/>                        
                    </td>
                </tr> 
                <tr><td><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR />.</td></tr> 
            </table>
            </div>
         </td>
         
                   
       </tr>             
     </TABLE>
     
     
     
     <TABLE width="10%" class="fondoTablaPdf" align="center">
       <tr>
         <td width="100%" align="center">
           <table width="100%" id="listFinalizarImprimirPDF">
            <tr><th></th></tr>  
           </table>
         </td>
       </tr>             
     </TABLE>




<style>
.letraTamano {
	font-size:17px;
	height:17px;
}
</style> 





<table id="documentoPDF" width="100%" border="1" cellpadding="1" cellspacing="1" align="center">                          
  <tr bgcolor="#FFFFFF">
    <td>

      <table  width="100%"  cellpadding="1" cellspacing="1" align="center" class="letraTamano2" >
        <tr>
          <td align="center"><b>MINISTERIO DE LA PROTECCION SOCIAL </b></td>
        </tr>
        <tr>
          <td align="center"><b>SOLICITUD DE AUTORIZACION DE SERVICIOS DE SALUD</b></td>    
        </tr>
        <tr>
          <td align="center">
              <table width="50%" id="listHoraFechaNoSolicitudPDF" cellpadding="1" cellspacing="0" align="CENTER">
               <tr><th></th></tr>  
              </table>           
          </td>    
        </tr>  
      </table> 
    
    
    </td>
  </tr>
  <tr>
    <td>  
       <TABLE width="100%" class="fondoTablaPdf">    
          <tr class="tituloLineaInferior">
            <td colspan="7">INFORMACION DEL PRESTADOR (Solicitante)
            </td>
          </tr>   
          
          <tr width="100%">
            <td width="100%" colspan="7">
              <table width="100%" id="listAdministradoraTotalPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 

          <tr>
            <td colspan="7">
              <table width="100%" id="listAdministradoraPDF" cellpadding="1" cellspacing="0" align="CENTER">
               <tr><th></th></tr>  
              </table>               
            </td>
          </tr>          
          
       </TABLE> 
    </td>   
  </tr>  
  <tr bgcolor="#FFFFFF">
    <td>  
       <TABLE width="100%" class="fondoTablaPdf">    
          <tr>
            <td class="tituloLineaInferior">DATOS DEL PACIENTE</td>
          </tr>   
          <tr>
            <td width="100%">
              <table width="100%" id="listPaciente2PDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
              <table width="100%" id="listPaciente3PDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>  
            </td>
          </tr>  
       </TABLE> 
      </td>
  </tr>  
  
  <tr bgcolor="#FFFFFF"> 
    <td>  
       <TABLE width="100%"  class="fondoTablaPdf">
          <tr>
            <td width="100%" class="tituloLineaInferior">INFORMACION DE LA ATENCION Y SERVICIOS SOLICITADOS
            </td>
          </tr>       
          <tr>
            <td width="100%">
              <table width="100%" id="listAtencionServiciosPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
          
       </TABLE> 
      </td>
  </tr>
  
  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
            <td width="100%">
              <table width="100%" id="listPlanPDF" cellpadding="1"  cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr>  

  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
            <td width="100%">
              <table width="100%" id="listJustificacionPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr> 

  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
             <td class="tituloLineaInferior">IMPRESION DIAGNOSTICA
             </td>
          </tr>
          <tr>
            <td width="100%">
              <table width="100%" id="listDiagnosticosPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr> 
  
  <tr bgcolor="#FFFFFF">&nbsp;
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
             <td class="tituloLineaInferior" colspan="2">INFORMACION DE LA PERSONA QUE SOLICITA
             </td>
          </tr>
          <tr>
            <td width="60%">
              <table width="100%" id="listProfesionalPDF" cellpadding="1"  cellspacing="1">
               <tr><th></th></tr>  
              </table>
            </td>
            <td width="40%">
              

              <!--table width="100%">
               <tr><td valign="top" align="left" class="tituloLineaInferior">Tel&eacute;fono: 7290261&nbsp;</td><td align="left" class="tituloLineaInferior">Celular:&nbsp;3173706863</td></tr>  
              </table-->    

               <table width="100%" id="listTelefonosPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>




            </td>            
          </tr> 
       </TABLE> 
      </td>  
  </tr>      
</table>           

     <TABLE width="10%" class="fondoTablaPdf" align="center">
       <tr>
         <td width="100%" align="center">
           <table width="100%" id="listFinalizarImprimirPDF">
            <tr><th></th></tr>  
           </table>
         </td>
       </tr>             
     </TABLE>


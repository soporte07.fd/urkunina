var ventana = null;

//deshabilitar algunas teclas y boton derecho del mouse
var focos = new Array();
var lastField;

var ii = 0;
var ie = 0;

function cerrar_sesion() {
  $.ajax({
    url: "/clinica/paginas/sesion_nav.jsp",
    type: "POST",
    beforeSend: function () {},
    success: function (data) {
      if (data.sesion == tabID) {
        $.ajax({
          url: "/clinica/paginas/logout.jsp",
          type: "POST",
          beforeSend: function () {

          },
          success: function (data) {
            if (data.getElementsByTagName('respuesta')[0].firstChild.data) {
              location.reload()
            } else {
              alert("No ha sido posible cerrar sesi&oacute;n de manera segura. Por favor cierre el navegador y pongase en contacto con el equipo de soporte.")
            }
          },

          error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("No ha sido posible cerrar sesi&oacute;n de manera segura. Por favor cierre el navegador y pongase en contacto con el equipo de soporte.")
          }
        });
      }
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {}
  });
}

var message = "Funcion deshabilitada";
function clickIE() {
  if (document.all) {
    return false;
  }
}

function clickNS(e) {
  if (document.layers || (document.getElementById && !document.all)) {
    if (e.which == 2 || e.which == 3) {
      return false;
    }
  }
}

if (document.layers) {
  document.captureEvents(Event.MOUSEDOWN);
  document.onmousedown = clickNS;
} else {
  document.onmouseup = clickNS;
  document.oncontextmenu = clickIE;
}

var NS = (navigator.appName == "Netscape" && parseInt(navigator.appVersion, 10) >= 5) ? true : false;
var IE = (navigator.appVersion.search(/MSIE/) != -1) ? true : false;
function whichKey(km) {
  if (NS) {
    if (km.which >= 111 && km.which <= 123) {
      return (false);
    }
  }
  else {
    if (event.keyCode >= 111 && event.keyCode <= 123) {
      event.keyCode = 0;
      return (false);
    }
  }
}
document.onkeydown = whichKey;

function trim(s) {
  while (s.substring(0, 1) == ' ') {
    s = s.substring(1, s.length);
  }
  while (s.substring(s.length - 1, s.length) == ' ') {
    s = s.substring(0, s.length - 1);
  }
  return s;
}

function roundOff(value, precision) {
  value = "" + value //convert value to string
  precision = parseInt(precision);
  var whole = "" + Math.round(value * Math.pow(10, precision));
  var decPoint = whole.length - precision;
  if (decPoint != 0) {
    result = whole.substring(0, decPoint);
    if (result == "") result += "0";
    result += ".";
    result += whole.substring(decPoint, whole.length);
  } else {
    if (whole.indexOf("-") == 0) {
      result = "-0";
      result += ".";
      result += whole.substring(decPoint + 1, whole.length);
    } else {
      result = "0";
      result += ".";
      result += whole.substring(decPoint, whole.length);
    }
  }
  return result;
}

var pag;
var alto;
var ancho;
var ordenes = new Set();
var trabajos = new Set();

function abrirPopupArchivoAdjunto(pag, ancho, alto) {
  switch (tabActivo) {
    case 'divArchivosAdjuntos':
      subCarpeta = 'fisico';
      break;
    case 'divListadoIdentificacion':
      subCarpeta = 'identificacion';
      break;
    case 'divListadoRemision':
      subCarpeta = 'remision';
      break;
    case 'divListadoCarnet':
      subCarpeta = 'carnet';
      break;
    case 'divListadoAyudasDiagnosticas':
      subCarpeta = 'ayudaDx';
      break;
    case 'divListadoOtrosDocumentos':
      subCarpeta = 'otros';
      break;
    case 'divAyudasDiagnosticas':
      subCarpeta = 'ayudaDx';
      break;
    case 'divSubirArchivo':
      subCarpeta = 'ordenProgramacion';
      break;
  }

  if (banArchivosPdf == '') {
    dat = 'width=' + ancho + ',height=' + alto + ',toolbar=no,left=50,dependent=yes,directories=no,status=no,menubar=no,scrollbar=no,resizable=no';
    windowArchivosPdf = window.open(pag + subCarpeta + '/' + valorAtributo('lblNombreElemento') + '.pdf', '', dat);
    banArchivosPdf = 'abiertoPdf'
    windowArchivosPdf.ID
  }
  else {
    cerrarPopupArchivosPdf()
    abrirPopupArchivoAdjunto(pag, ancho, alto)
  }
}

function abrirPopupArchivoTipificacion(id_factura, tipo) {
  dat = 'width=1200,height=1200,toolbar=no,left=50,dependent=yes,directories=no,status=no,menubar=no,scrollbar=no,resizable=no';
  window.open('/tipificacion/' + id_factura + '/' + tipo + '.pdf', '', dat);
}


function abrirPopupOrden(nombre, ancho, alto) {
  nombre = nombre.replace("pdf","");
  dat = 'width=' + ancho + ',height=' + alto + ',toolbar=no,left=50,dependent=yes,directories=no,status=no,menubar=no,scrollbar=no,resizable=no';
  windowArchivosPdf = window.open('/docPDF/ordenProgramacion/' + nombre + '.pdf', '', dat);
  banArchivosPdf = 'abiertoPdf'
  windowArchivosPdf.ID

}

function abrirResultadoAnalito(cell) {

  var id = $('#listOrdenesArchivo').jqGrid('getGridParam', 'selrow');
  datosRow = $('#listOrdenesArchivo').getRowData(id);

  asignaAtributo("txtIdProcedimientoProgramacion", cell, 0);
  buscarHC('listResultadoPlantilla', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
  mostrar('divVentanitaResultadoPlantilla');

}

function abrirPopupTrabajoOp(pag, ancho, alto) {


  if (banArchivosPdf == '') {
    dat = 'width=' + ancho + ',height=' + alto + ',toolbar=no,left=50,dependent=yes,directories=no,status=no,menubar=no,scrollbar=no,resizable=no';

    windowArchivosPdf = window.open(pag + 'firmas/ventas' + '/' + valorAtributo('lblCedulaPdf') + '.png', '', dat);
    banArchivosPdf = 'abiertoPdf'
    windowArchivosPdf.ID
  }
  else {
    cerrarPopupArchivosPdf()
    abrirPopupTrabajoOp(pag, ancho, alto)
  }


}
function abrirPopupTrabajoOp2(pag, ancho, alto) {


  if (banArchivosPdf == '') {
    dat = 'width=' + ancho + ',height=' + alto + ',toolbar=no,left=50,dependent=yes,directories=no,status=no,menubar=no,scrollbar=no,resizable=no';

    windowArchivosPdf = window.open(pag + 'firmas/ventas' + '/' + valorAtributo('lblCedulaPdf2') + '.png', '', dat);
    banArchivosPdf = 'abiertoPdf'
    windowArchivosPdf.ID
  }
  else {
    cerrarPopupArchivosPdf()
    abrirPopupTrabajoOp2(pag, ancho, alto)
  }


}
function abrirPopupTrabajoCliente(pag, ancho, alto) {


  if (banArchivosPdf == '') {
    dat = 'width=' + ancho + ',height=' + alto + ',toolbar=no,left=50,dependent=yes,directories=no,status=no,menubar=no,scrollbar=no,resizable=no';

    windowArchivosPdf = window.open(pag + 'firmas/ventas' + '/' + valorAtributo('lblFirmaCliente') + '.png', '', dat);
    banArchivosPdf = 'abiertoPdf'
    windowArchivosPdf.ID
  }
  else {
    cerrarPopupArchivosPdf()
    abrirPopupTrabajoCliente(pag, ancho, alto)
  }


}

var banArchivosPdf = '';

function abrirPopupArchivosPdf() {


}
function cerrarPopupArchivosPdf() {// alert(44444)
  windowArchivosPdf.close();
  //windowArchivosPdf.opener.close()
  banArchivosPdf = '';
}


function abrir(pag, ancho, alto) {
  dat = 'width=' + ancho + ',height=' + alto + ',toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbar=no,resizable=no';
  window.open(pag, '', dat);
}

function abrirpopup(pag, ancho, alto, x, y) {
  dat = 'width=' + ancho + ',height=' + alto + ',left=' + x + ',top=' + y + ',dependent=yes,scrollbars=yes,resize=no,toolbar=no,status=no,directories=no,resizable=no';
  return window.open(pag, '', dat);
}

function cerrarPopUp(popup) {
  if (popup != null) {
    popup.close();
  }
}

/////////////////////////////////Funciones Franklin Jim�nez
/////funciones para mover capas
function Browser() {
  var ua, s, i;
  this.isIE = false;
  this.isNS = false;
  this.version = null;
  ua = navigator.userAgent;
  s = "MSIE";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isIE = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }
  s = "Netscape6/";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }
  // Treat any other "Gecko" browser as NS 6.1.
  s = "Gecko";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = 6.1;
    return;
  }
}

var browser = new Browser();

// Global object to hold drag information.
var dragObj = new Object();
dragObj.zIndex = 0;

function dragStart(event, id) {
  var el;
  var x, y;
  // If an element id was given, find it. Otherwise use the element being
  // clicked on.
  if (id)
    dragObj.elNode = document.getElementById(id);
  else {
    if (browser.isIE)
      dragObj.elNode = window.event.srcElement;
    if (browser.isNS)
      dragObj.elNode = event.target;
    // If this is a text node, use its parent element.
    if (dragObj.elNode.nodeType == 3)
      dragObj.elNode = dragObj.elNode.parentNode;
  }
  // Get cursor position with respect to the page.
  if (browser.isIE) {
    x = window.event.clientX + document.documentElement.scrollLeft
      + document.body.scrollLeft;
    y = window.event.clientY + document.documentElement.scrollTop
      + document.body.scrollTop;
  }
  if (browser.isNS) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }
  // Save starting positions of cursor and element.
  dragObj.cursorStartX = x;
  dragObj.cursorStartY = y;
  dragObj.elStartLeft = parseInt(dragObj.elNode.style.left, 10);
  dragObj.elStartTop = parseInt(dragObj.elNode.style.top, 10);
  if (isNaN(dragObj.elStartLeft)) dragObj.elStartLeft = 0;
  if (isNaN(dragObj.elStartTop)) dragObj.elStartTop = 0;
  // Update element's z-index.
  dragObj.elNode.style.zIndex = ++dragObj.zIndex;
  // Capture mousemove and mouseup events on the page.
  if (browser.isIE) {
    document.attachEvent("onmousemove", dragGo);
    document.attachEvent("onmouseup", dragStop);
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
  if (browser.isNS) {
    document.addEventListener("mousemove", dragGo, true);
    document.addEventListener("mouseup", dragStop, true);
    event.preventDefault();
  }
}

function dragGo(event) {
  var x, y;
  // Get cursor position with respect to the page.
  if (browser.isIE) {
    x = window.event.clientX + document.documentElement.scrollLeft
      + document.body.scrollLeft;
    y = window.event.clientY + document.documentElement.scrollTop
      + document.body.scrollTop;
  }
  if (browser.isNS) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }
  // Move drag element by the same amount the cursor has moved.
  dragObj.elNode.style.left = (dragObj.elStartLeft + x - dragObj.cursorStartX) + "px";
  dragObj.elNode.style.top = (dragObj.elStartTop + y - dragObj.cursorStartY) + "px";
  if (browser.isIE) {
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
  if (browser.isNS)
    event.preventDefault();
}

function dragStop(event) {
  // Stop capturing mousemove and mouseup events.
  if (browser.isIE) {
    document.detachEvent("onmousemove", dragGo);
    document.detachEvent("onmouseup", dragStop);
  }
  if (browser.isNS) {
    document.removeEventListener("mousemove", dragGo, true);
    document.removeEventListener("mouseup", dragStop, true);
  }
}

////funcion para crear objeto ajax
function crearAjax() {
  var ajax = false;
  /*@cc_on @*/
	/*@if (@_jscript_version >= 5)
	//usamos compilacion condicional para evitar errores en
	//versiones de internet explorer antiguas
	var ids=["Msxml2.XMLHTTP.5.0","Msxml2.XMLHTTP.4.0","Msxml2.XMLHTTP.3.0","Msxml2.XMLHTTP","Microsoft.XMLHTTP"];
	for(var i=0; !ajax && i<ids.length; i++){
	     try{
		      ajax = new ActiveXObject(ids[i]);
	     }catch (e){ajax = false;} 
	 }
    @end @*/
  if (!ajax && typeof XMLHttpRequest != 'undefined') {
    ajax = new XMLHttpRequest();
  }
  return ajax;
}
///****************fin funciones ajax

///**** manejador de eventos
function addEvent(obj, evType, fn, useCapture) {
  if (obj.addEventListener) {
    obj.addEventListener(evType, fn, useCapture);
  } else if (obj.attachEvent) {
    var r = obj.attachEvent("on" + evType, fn);
  } else { alert("no se puede crear el manejador de eventos, funcion addEvent"); }
  return false;
}

function bloqueateclas(e) {
  if (!window.event) {
    if (e.keyCode >= 111 && e.keyCode <= 123) {
      e.preventDefault();
      return false;
    }
  }
}
document.keypress = addEvent(document, "keypress", bloqueateclas, true);

var bloqueateclado = false;
function revisateclado(e) {///bloquear teclado
  if (bloqueateclado == true) {
    if (navigator.appVersion.search(/MSIE/) != -1)
      window.event.returnValue = false;
    else
      e.preventDefault();
  }
}
addEvent(document, "keydown", revisateclado, false);///bloquear el teclado si la variable bloqueateclado es true
addEvent(document, "keypress", revisateclado, false);

var bloqueaclick = false;
function revisaclick(e) {///bloquear teclado
  if (bloqueaclick == true) {
    if (navigator.appVersion.search(/MSIE/) != -1) {
      window.event.returnValue = false;
      window.event.cancelBubble = true;
    } else {
      e.preventDefault();
      e.stopPropagation();
    }
  }
}
if (history.forward(1)) { history.replace(history.forward(1)); }

//Funcion encargada de identificar la tecla presionada, si se trata de enter se pasa el
//foco al siguiente elemento, si se trata de escape se pasa el foco al elemento anterior
function muevefoco(e) {
  if (e.keyCode == 13 || e.keyCode == 27) {
    if (window.event) id = e.srcElement.id;
    else id = e.target.id;
    if (e.keyCode == 13) {
      for (i = 1; i < focos.length - 1; i++) {
        if (focos[i] == id) {
          document.getElementById(focos[i + 1]).focus();
          break;
        }
      }
    }
    else if (e.keyCode == 27) {
      for (i = 2; i <= focos.length; i++) {
        if (focos[i] == id) {
          document.getElementById(focos[i - 1]).focus();
          break;
        }
      }
    }
  }
}

// Para utilizar esta funcion se debe ubicar a cada uno de los elementos del 
// formulario el tabIndex correspondiente con el indice que queremos que tenga
// La funcion se encarga de llenar el array focos[] con los valores de los
// ID de cada uno de los elementos
function cargarFocos() {
  focos = new Array();//array que contiene todos los objetos que pueden recibir foco
  var nodeList = document.getElementsByTagName("input");
  for (var i = 0; i < nodeList.length; i++) {
    focos[nodeList[i].tabIndex] = nodeList[i].id;
  }
  nodeList = document.getElementsByTagName("select");
  for (var i = 0; i < nodeList.length; i++) {
    focos[nodeList[i].tabIndex] = nodeList[i].id;
  }
  nodeList = document.getElementsByTagName("textarea");
  for (var i = 0; i < nodeList.length; i++) {
    focos[nodeList[i].tabIndex] = nodeList[i].id;
  }
  addEvent(document, "keyup", muevefoco, true);
}

function crearMensaje() {
  return "<table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#F9F9F9'>" +
    "<tr>" +
    "<td width='33%' align='left'><img src='../utilidades/imagenes/mensajeEspera/publi_01.jpg' width='100%' height='100%'/></td>" +
    "<td align='center' valign='middle'><div align='center'><img src='../utilidades/imagenes/mensajeEspera/cargando.gif' /></div></td>" +
    "<td width='33%' align='right'><img src='../utilidades/imagenes/mensajeEspera/publi_03.jpg' width='100%' height='100%' /></td>" +
    "</tr>" +
    "</table>";
}
////////////////////fin funciones Franklin Jim�nez

//browser
function quebrowser() {
  if (window.opera) return 'Opera';
  if (window.netscape) return 'Netscape';
  if (window.document) return 'ie';
  return 'nose';
}
var b = quebrowser();
if (b != 'ie' && b != 'Netscape') {
  document.write("<META HTTP-EQUIV='Refresh' CONTENT='0; URL=/error_pages/nobrowser.html'>")
};


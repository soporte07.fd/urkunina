
        case 'listFolios':
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            var qry = "754";
            valores_a_mandar = valores_a_mandar + "?idQuery=" + qry + "&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#listFolios").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'Tipo', 'id_admision', 'identificacion_Paciente', 'FechaCierre',
                 'Medico', 'Especialidad'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 5)  },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 5) },
                    { name: 'id_admision', index: 'id_admision', width: anchoP(ancho, 35) },
                    { name: 'identificacion_Paciente', index: 'identificacion_Paciente', width: anchoP(ancho, 10) },
                    { name: 'FechaCierre', index: 'FechaCierre', width: anchoP(ancho, 8) },
                    { name: 'Medico', index: 'Medico', width: anchoP(ancho, 10) },
                    { name: 'Especialidad', index: 'Especialidad', width: anchoP(ancho, 12) },
                ],
                onSelectRow: function(rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listFolios').getRowData(rowid);
                    estad = 0;

                    //  asignaAtributo('txtIdBusPaciente',datosRow.idPaciente+'-'+datosRow.identificacion+' '+datosRow.Nombre ,0);          
                    asignaAtributo('lblTipoDocumento', datosRow.Tipo, 1);
                    asignaAtributo('lblIdDocumento', datosRow.id, 0);
                    setTimeout("habilitar('btnProcedimiento',0)", 2000);
                    switch (valorAtributo('lblTipoDocumento')) {
                        case 'EXOP':
                            setTimeout("habilitar('btnFormulaOftalmica',1)", 1000);
                            break;
                        case 'EVOP':
                            setTimeout("habilitar('btnFormulaOftalmica',1)", 1000);
                            break;
                        case 'EXBV':
                            setTimeout("habilitar('btnFormulaOftalmica',1)", 1000);
                            break;
                        default:
                            setTimeout("habilitar('btnFormulaOftalmica',0)", 1000);
                            break;
                    }
                    setTimeout("buscarHC('listDocumentosHistoricosTodo','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')", 2000);
                },
                height: 400,
                width: ancho,

            });
            $('#drag' + ventanaActual.num).find("#listFolios").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;  
function limpTablas(idTab) {
	var mytable = document.getElementById(idTab);
	$("#" + idTab + " tr").remove();
}

function limpTablasDiv(idTab) {
	var mytable = document.getElementById(idTab);
	$("#" + idTab + " div").remove();
}


function limpiarDatosAdmisionCuenta() {
	limpiaAtributo('txtIdDx', 0);
	limpiaAtributo('txtIPSRemite', 0);
	limpiaAtributo('txtObservacion', 0);
	limpiaAtributo('cmbIdEspecialidad', 0);
	limpiaAtributo('cmbTipoAdmision', 0);
	limpiaAtributo('cmbIdProfesionales', 0);
	limpiaAtributo('txtAdministradora1', 0);
	limpiaAtributo('cmbIdTipoRegimen', 0);
	limpiaAtributo('txtNoAutorizacion', 0);
	limpiaAtributo('txtFechaAdmision', 0);
	limpiaAtributo('lblIdPlanContratacion', 0);
	limpiaAtributo('lblNomPlanContratacion', 0);
	limpiaAtributo('cmbIdTipoAfiliado', 0);
	limpiaAtributo('cmbIdRango', 0);


	limpiaAtributo('lblIdAdmision', 0);
	limpiaAtributo('lblIdCuenta', 0);
	limpiaAtributo('lblIdTipoAdmision', 0);
	limpiaAtributo('lblNomTipoAdmision', 0);
	limpiaAtributo('lblIdPlan', 0);
	limpiaAtributo('lblNomPlan', 0);
	limpiaAtributo('lblIdTipoAfiliado', 0);
	limpiaAtributo('lblNomTipoAfiliado', 0);
	limpiaAtributo('lblIdRango', 0);
	limpiaAtributo('lblNomRango', 0);
	limpiaAtributo('lblIdEstadoCuenta', 0);
	limpiaAtributo('lblNomEstadoCuenta', 0);
	limpiaAtributo('lblUsuarioCrea', 0);
	limpiarListadosTotales('listProcedimientosDeFactura')

}
// JavaScript Document funciones para limpiar
function limpiarListadosTotales(casoList) { //alert(66666)

	//   $('#drag'+ventanaActual.num).find('#'+casoList).html(''); 	

	var mytable = document.getElementById(casoList);
	if (mytable.rows.length > 1) {
		//var casoList=mytableList;
		var ids = jQuery("#" + casoList).getDataIDs();
		for (var i = 0; i <= ids.length; i++) {
			var c = ids[i];
			//var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
			jQuery("#" + casoList).delRowData(c);// eliminar todo elemento del listado
		}

	}

	switch (casoList){
	 case 'listGrillaPaciente':
		limpiaAtributo('txtIdentificacion', 0);
		limpiaAtributo('txtNombre1', 0);
		limpiaAtributo('txtNombre2', 0);
		limpiaAtributo('txtApellido1', 0);
		limpiaAtributo('txtApellido2', 0);
		limpiaAtributo('txtFechaNac', 0);
		limpiaAtributo('txtMunicipio', 0);				
		limpiaAtributo('txtDireccionRes', 0);				
		limpiaAtributo('txtNomAcompanante', 0);				
		limpiaAtributo('txtCelular1', 0);								
		limpiaAtributo('txtCelular2', 0);										
		limpiaAtributo('txtTelefonos', 0);										
		limpiaAtributo('txtEmail', 0);												
	break;	 
	}

}


function limpiarDivEditarJuan(arg) {

	switch (arg) {


		case 'limpiarDatosApp':
			$("#imgDatoApp").attr('src', "");
			limpiaAtributo("lblIdApp", 0)
			limpiaAtributo("txtTituloApp", 0)
			limpiaAtributo("txtDescripcionApp", 0)
		break;

		case 'limpiarCoordinacionTerapia':
			limpiaAtributo('cmbTipoId', 0);
			limpiaAtributo('txtIdentificacion', 0);
			limpiaAtributo('txtIdBusPaciente', 0);
			limpiaAtributo('cmbProgramacion', 0);
			limpiaAtributo('cmbIdSedeBus', 0);
			limpiaAtributo('cmbIdEspecialidadBus', 0);
			limpiaAtributo('cmbIdProfesionales', 0);
			limpiaAtributo('cmbIdDepartamento', 0);
			limpiaAtributo('cmbIdMunicipio', 0);
			limpiaAtributo('cmbIdLocalidad', 0); 
			
			
		break;


		case 'lstMantenimientoRuta':
			limpiaAtributo('txtId', 0);
			limpiaAtributo('txtNombre', 0);
			limpiaAtributo('txtObservaciones', 0);
			limpiaAtributo('cmbVigente', 0);
			limpiaAtributo('txtDuracion', 0);
			limpiaAtributo('cmbCiclica', 0);
			cmbReset('cmbVigente', 0);
			cmbReset('cmbCiclica', 0);
			quitarTitulo('txtId');
			break;


		case 'limpiarSubespecialidad':
			limpiaAtributo('cmbIdSubEspecialidad', 0)
			break;

		case 'auditarRecibo':
			limpiaAtributo('lblIdReciboConsecutivo', 1)
			limpiaAtributo('lblIdRecibo', 1)
			break;

		case 'listArticulosPlan':

			limpiaAtributo('txtIdArticulo', 1)
			limpiaAtributo('txtNombreArticulo', 0)
			limpiaAtributo('txtNombreComercial', 0)
			limpiaAtributo('txtIVA', 0)
			limpiaAtributo('cmbIVAVenta', 0)
			limpiaAtributo('cmbPOS', 0)
			limpiaAtributo('cmbTipoServicio', 0)
			limpiaAtributo('cmbIdCentroCosto', 0)
			limpiaAtributo('cmbIdGrupoCentroCosto', 0)


			break;

		case 'limpiarRecibosCartera':

			limpiaAtributo('lblIdRecibo', 0)
			limpiaAtributo('lblIdFactura', 0)
			limpiaAtributo('lblTotalFactura', 0)
			limpiaAtributo('lblSaldoFactura', 0)
			limpiaAtributo('lblIdTipoPago', 0)
			limpiaAtributo('lblTipoPago', 0)
			limpiaAtributo('lblIdEstadoFactura', 0)
			limpiaAtributo('lblEstadoFactura', 0)
			limpiaAtributo('txtValorUnidadRecibo', 0)
			limpiaAtributo('cmbIdConceptoRecibo', 0)
			limpiaAtributo('txtObservacionRecibo', 0)
			limpiaAtributo('txtNumeroFactura', 0)
			limpiaAtributo('txtIdentificacion', 0)
			limpiaAtributo('txtIdBusPaciente', 0)
			limpiaAtributo('txtFechaDesdeFactura', 0)
			limpiaAtributo('txtFechaHastaFactura', 0)
			limpiarListadosTotales('listGrillaRecibosCartera')
			limpiarListadosTotales('listGrillaFacturasCartera')

			break;

		case 'auditarFactura':


			limpiaAtributo('lblIdFactura', 0)
			limpiaAtributo('lblNumeroFactura', 0)
			limpiaAtributo('lblIdEstadoFactura', 0)
			limpiaAtributo('lblEstadoFactura', 0)
			limpiaAtributo('lblValorTotalFactura', 0)
			limpiaAtributo('lblIdPaciente', 0)
			limpiaAtributo('lblNomPaciente', 0)
			limpiaAtributo('lblIdUsuario', 0)
			limpiaAtributo('lblNomUsuario', 0)
			limpiaAtributo('txtFechaAdmision', 0)
			limpiaAtributo('cmbIdProfesionales', 0)
			limpiaAtributo('lblIdAdmision', 0)
			limpiaAtributo('lblIdDoc', 0)
			limpiaAtributo('lblIdEstadoAuditoria', 0)



			break;


		case 'adicionarFirma':

			limpiaAtributo('lblIdEstadoFirmar', 0)
			limpiaAtributo('lblEstadoFirmar', 0)
			limpiaAtributo('lblFuncionarioFirmar', 0)
			limpiaAtributo('lblIdFuncionarioFirmar', 0)
			ordenes.clear();
			trabajos.clear();
			limpiaAtributo('lblOrdenesFirmar', 0)
			limpiaAtributo('lblTrabajosFirmar', 0)
			recargarFirmaTrabajo('');

			break;


		case 'limpiarFacturaVentas':


			limpiaAtributo('txtIdBusPaciente', 0);
			limpiaAtributo('txtIdentificacion', 0);
			limpiaAtributo('txtNombre1', 0);
			limpiaAtributo('txtNombre2', 0);
			limpiaAtributo('txtApellido1', 0);
			limpiaAtributo('txtApellido2', 0);
			limpiaAtributo('txtFechaNac', 0);
			limpiaAtributo('cmbSexo', 0);
			limpiaAtributo('txtMunicipio', 0);
			limpiaAtributo('txtDireccionRes', 0);
			limpiaAtributo('txtNomAcompanante', 0);
			limpiaAtributo('txtTelefonos', 0);
			limpiaAtributo('txtCelular1', 0);
			limpiaAtributo('txtCelular2', 0);
			limpiaAtributo('txtEmail', 0);
			limpiaAtributo('cmbIdEtnia', 0);
			limpiaAtributo('cmbIdNivelEscolaridad', 0);
			limpiaAtributo('cmbIdOcupacion', 0);
			limpiaAtributo('cmbIdEstrato', 0);
			limpiaAtributo('txtIdOcupacion', 0);

			limpiaAtributo('txtAdministradoraPaciente', 0);
			limpiaAtributo('txtAdministradora1', 0);
			limpiaAtributo('cmbBodegaVentas', 0);
			limpiaAtributo('txtObservacionVentas', 0);
			limpiaAtributo('lblIdDoc', 0);


			limpiaAtributo('cmbIdTipoRegimen', 0);
			limpiaAtributo('cmbIdTipoAfiliado', 0);
			limpiaAtributo('cmbIdRango', 0);
			limpiaAtributo('txtNoAutorizacion', 0);
			limpiaAtributo('cmbTipoAdmision', 0);
			limpiaAtributo('lblIdPlanContratacion', 0);
			limpiaAtributo('lblNomPlanContratacion', 0);


			limpiarListadosTotales('listFacturasVentas')
			limpiarListadosTotales('listArticulosVentas')
			limpiarListadosTotales('listOtrosArticulosVentas');


			limpiaAtributo('lblIdFactura', 0);
			limpiaAtributo('lblIdEstadoFactura', 0);
			limpiaAtributo('lblEstadoFactura', 0);
			limpiaAtributo('lblNumeroFactura', 0);
			limpiaAtributo('lblIdRecibo', 0);
			limpiaAtributo('lblTotalFactura', 0);
			limpiaAtributo('lblValorNoCubierto', 0);
			limpiaAtributo('lblValorCubierto', 0);
			limpiaAtributo('lblDescuento', 0);



			break;



		case 'limpiarRecibos':

			limpiaAtributo('txtBusNumRecibo', 0);
			limpiaAtributo('txtFechaRecibo', 0);
			limpiaAtributo('cmbIdPLan', 0);
			limpiaAtributo('lblIdRecibo', 0);
			limpiaAtributo('txtIdEditPacienteRecibo', 0);
			limpiaAtributo('txtAdministradora1', 0);
			limpiaAtributo('cmbIdTipoRegimen', 0);
			limpiaAtributo('lblIdPlanContratacion', 0);
			limpiaAtributo('txtValorUnidadRecibo', 0);
			limpiaAtributo('cmbCantidadRecibo', 0);
			limpiaAtributo('lblValorRecibo', 0);
			limpiaAtributo('cmbIdConceptoRecibo', 0);
			limpiaAtributo('txtObservacionRecibo', 0);
			limpiaAtributo('lblNomUsuarioRecibo', 0);
			limpiaAtributo('lblFechaElaboroRecibo', 0);
			limpiaAtributo('lblEstadoRecibo', 0);
			limpiaAtributo('lblNomPlanContratacion', 0);
			limpiaAtributo('lblIdUsuarioRecibo', 0);
			limpiaAtributo('lblIdEstadoRecibo', 0);
			break;
			
		case 'limpiarProcedimientos':
			limpiaAtributo('txtId', 0);
			limpiaAtributo('txtNombre', 0);
			limpiaAtributo('cmbIdTipoRips', 0);
			limpiaAtributo('cmbIdCentroCosto', 0);
			limpiaAtributo('cmbIdGrupoCentroCosto', 0);
			limpiaAtributo('cmbNivel', 0);
			limpiaAtributo('txtCodContable', 0);
			limpiaAtributo('txtCosto', 0);
			limpiaAtributo('cmbClase', 0);
			limpiaAtributo('cmbTipo', 0);
			limpiaAtributo('cmbVigente', 0);
			limpiaAtributo('cmbTipoServicio', 0);
			limpiaAtributo('cmbMostrarHc', 0);
			limpiaAtributo('txtCups', 0);
			limpiaAtributo('txtCodigoInterno', 0);
			break;

		case 'limpiarFactura':

			limpiaAtributo('lblIdAdmision', 0);
			limpiaAtributo('lblIdTipoAdmision', 0);
			limpiaAtributo('lblNomTipoAdmision', 0);
			limpiaAtributo('lblIdEpsPlan', 0);
			limpiaAtributo('lblIdPlan', 0);
			limpiaAtributo('lblNomPlan', 0);
			limpiaAtributo('lblIdRegimen', 0);
			limpiaAtributo('lblNomRegimen', 0);
			limpiaAtributo('lblIdTipoAfiliado', 0);
			limpiaAtributo('lblNomTipoAfiliado', 0);

			limpiaAtributo('lblIdRango', 0);
			limpiaAtributo('lblNomRango', 0);
			limpiaAtributo('lblUsuarioCrea', 0);


			limpiaAtributo('txtIdProcedimientoCex', 0);
			limpiaAtributo('lblValorUnitarioProc', 0);
			limpiaAtributo('cmbCantidad', 0);

			//info admisión
			limpiaAtributo('cmbIdTipoRegimen', 0);
			limpiaAtributo('cmbIdTipoAfiliado', 0);
			limpiaAtributo('cmbIdRango', 0);
			limpiaAtributo('txtNoAutorizacion', 0);
			limpiaAtributo('cmbTipoAdmision', 0);

			limpiaAtributo('lblIdFactura', 0);
			limpiaAtributo('lblIdEstadoFactura', 0);
			limpiaAtributo('lblEstadoFactura', 0);
			limpiaAtributo('lblNumeroFactura', 0);
			limpiaAtributo('lblIdRecibo', 0);
			limpiaAtributo('lblTotalFactura', 0);
			limpiaAtributo('lblValorNoCubierto', 0);
			limpiaAtributo('lblValorCubierto', 0);
			limpiaAtributo('lblDescuento', 0);

			break;
		case 'limpiarCamposIncapacidad':

			limpiaAtributo('txtDiasIncapacidad', 0);
			limpiaAtributo('txtFechaIncaInicio', 0);
			limpiaAtributo('txtFechaIncaFin', 0);
			limpiaAtributo('txtMotivo', 0);
			limpiaAtributo('txtObservaciones', 0);
			break;
		case 'limpiarFacturaOptica':


			limpiarListadosTotales('listArticulosDeFactura');
			limpiaAtributo('lblIdFactura', 0);
			limpiaAtributo('lblIdEstadoFactura', 0);
			limpiaAtributo('lblEstadoFactura', 0);
			limpiaAtributo('lblNumeroFactura', 0);
			limpiaAtributo('lblIdRecibo', 0);
			limpiaAtributo('lblTotalFactura', 0);
			limpiaAtributo('lblValorNoCubierto', 0);
			limpiaAtributo('lblValorCubierto', 0);
			limpiaAtributo('lblDescuento', 0);


			break;

		case 'limpiarDatosAdministradoraFactura':
			limpiaAtributo('lblIdPlanContratacion', 0)
			limpiaAtributo('lblNomPlanContratacion', 0)
			limpiaAtributo('cmbIdTipoRegimen', 0)
			limpiaAtributo('cmbIdPlan', 0)
			limpiaAtributo('cmbIdTipoAfiliado', 0);
			limpiaAtributo('cmbIdRango', 0);
			break;


		case 'limpiarDatosAdministradoraListaEspera':
			limpiaAtributo('lblIdPlanContratacion', 0)
			limpiaAtributo('lblNomPlanContratacion', 0)
			limpiaAtributo('cmbIdTipoRegimen', 0)
			break;

		case 'limpiarDatosPlan':
			limpiaAtributo('lblIdPlanContratacion', 0)
			limpiaAtributo('lblNomPlanContratacion', 0)
			break;

		case 'limpiarDatosRegimenFactura':
			asignaAtributoCombo2('cmbIdTipoAfiliado', '', '');
			asignaAtributoCombo2('cmbIdRango', '', '');
			break;

		case 'limpiarDatosAfiliadoFactura':
			asignaAtributoCombo2('cmbIdRango', '', '');
			break;

		case 'limpiarMotivoFolioEdit':
			limpiaAtributo('cmbIdMotivoEstadoEdit', 0);
			asignaAtributoCombo2('cmbMotivoClaseFolioEdit', '', '');
			break;

		case 'limpiarMotivoConsultaEdit':
			limpiaAtributo('cmbMotivoConsultaEdit', 0);
			asignaAtributoCombo2('cmbMotivoConsultaClaseEdit', '', '');


			break;

		case 'limpiarEstadoCitaEdit':
			limpiaAtributo('cmbMotivoConsultaClaseEdit', 0);
			break;

		case 'limpiarEstadoMotivoCitaEdit':
			asignaAtributoCombo2('cmbMotivoConsultaEdit', '', '');
			asignaAtributoCombo2('cmbMotivoConsultaClaseEdit', '', '');
			break;

		case 'DescuentoArticulo':
			limpiaAtributo('txtDescuentoArt', 0);
			limpiaAtributo('lblSerialDescuento', 0);

			break;
		case 'limpCamposPosVista':
			limpiaAtributo('txtTipoDocumentoVistaPrevia', 0);
			limpiaAtributo('txtIdDocumentoVistaPrevia', 0);
			limpiaAtributo('lblTituloDocumentoVistaPrevia', 0);
			limpiaAtributo('lblIdAdmisionVistaPrevia', 0);
			break;
		case 'listProcedimientosOrdenes':
			//				 limpiarListadosTotales('listProcedimientosOrdenes'); 				   			
			limpiaAtributo('lblIdProcedimientoAutoriza', 0);
			limpiaAtributo('lblCodigoProcedimientoAutoriza', 0);
			limpiaAtributo('lblNomProcedimientoAutoriza', 0);
			limpiaAtributo('cmbEsperarProcAutoriza', 0);
			limpiaAtributo('cmbIdEnviado', 0);
			limpiaAtributo('txtJustificacionFolio', 0);
			limpiaAtributo('lblIdAutorizacion', 0);
			break;
		case 'notificacionDestino':
			limpiaAtributo('lblIdNotificacion', 0);
			limpiaAtributo('cmbIdPersona', 0);
			break;
		case 'notificacion':
			limpiaAtributo('txtAsunto', 0);
			limpiaAtributo('txtDescripcion', 0);
			break;
		case 'limpiarGrupoUsuario':
			limpiaAtributo('cmbIdGrupo', 0);
			limpiaAtributo('lblIdGrupo', 0);
			break;

		case 'limpiarCrearUsuario':
			limpiaAtributo('cmbTipoId', 0);
			limpiaAtributo('txtIdentificacion', 0);
			limpiaAtributo('txtPNombre', 0);
			limpiaAtributo('txtSNombre', 0);
			limpiaAtributo('txtPApellido', 0);
			limpiaAtributo('txtSApellido', 0);
			limpiaAtributo('txtNomPersonal', 0);
			limpiaAtributo('txtUSuario', 0);
			limpiaAtributo('cmbIdTipoProfesion', 0);
			limpiaAtributo('txtRegistro', 0);
			limpiaAtributo('cmbVigente', 0);
			limpiaAtributo('txtCorreo', 0);
			limpiaAtributo('cmbEstado', 0);
			limpiaAtributo('cmbSede', 0);
			limpiaAtributo('txtTelefono', 0);
			limpiaAtributo('cmbEstado', 0);
			limpiaAtributo('txtPhone', 0);
			$("#imgFirmaProfesional").attr("src","../utilidades/firmas/blanco.png");


			break;
		case 'modificarFechaDocumentoInventario':
			limpiaAtributo('lblIdDoc', 0);
			limpiaAtributo('lblIdEstado', 0);
			limpiaAtributo('txtFechaModifica', 0);
			limpiaAtributo('lblNaturaleza', 0);
			break;
		case 'clonarDocumentoEgreso':
			//limpiaAtributo('lblIdDoc',0);
			limpiaAtributo('txtDocEgreso', 0);

			break;
		case 'limpiarMotivoAD':
			asignaAtributoCombo2('cmbMotivoConsulta', '', '[ Seleccione ]');
			asignaAtributoCombo2('cmbMotivoConsultaClase', '', '[ Seleccione ]');
			break;

		case 'limpiarMotivoClase':
			limpiaAtributo('cmbMotivoConsultaClase', 0);
			break;

		case 'limpiarMotivoCX':
			asignaAtributoCombo2('cmbMotivoConsulta', '', '[ Seleccione ]');
			asignaAtributoCombo2('cmbMotivoConsultaClase', '', '[ Seleccione ]');
			break;
		case 'abreVentanitaLiquidacion':
			limpiaAtributo('lblIdLiquidacion', 0);
			limpiaAtributo('cmbIdAnestesiologo', 0);
			limpiaAtributo('cmbIdTipoAnestesia', 0);

			limpiaAtributo('cmbIdTipoAtencion', 0);

			limpiaAtributo('lblIdTarifario_liq', 0);
			limpiaAtributo('lblVariabilidad_liq', 0);

			limpiaAtributo('lblValorTotal_liq', 0);
			limpiaAtributo('lblLiquida_liq', 0);
			limpiaAtributo('lblIdTipoTarifario_liq', 0);

			limpiarListadosTotales('listLiquidacionQx');
			limpiarListadosTotales('listLiquidacionQxProcedimientos');

			break;
		case 'limpiarTipoCitaLE':
			limpiaAtributo('cmbTipoCita', 0);
			break;
		case 'limpCamposDeArticulo':
			if (valorAtributo('txtNaturaleza') == 'I') {
				limpiaAtributo('txtLote', 0);
				limpiaAtributo('txtValorU', 0);
				limpiaAtributo('txtPrecioVenta', 0);
			} else {
				limpiaAtributo('cmbIdLote', 0);
				limpiaAtributo('lblValorUnitario', 0);
			}
			//limpiaAtributo('txtCantidad',0);				
			limpiaAtributo('cmbIva', 0);
			limpiaAtributo('lblValorImpuesto', 0);
			limpiaAtributo('lblSubTotal', 0);
			limpiaAtributo('lblTotal', 0);
			limpiaAtributo('lblExistencias', 0);

			break;
		case 'limpCamposSolicitudTransaccion':
			if (valorAtributo('lblNaturaleza') == 'I') {
				limpiaAtributo('txtLote', 0);
				limpiaAtributo('txtValorU', 0);
				limpiaAtributo('txtPrecioVenta', 0);
				limpiaAtributo('lblSubTotal', 0);
				limpiaAtributo('lblTotal', 0);
				limpiaAtributo('txtFechaVencimiento', 0);
			} else {
				limpiaAtributo('cmbIdLote', 0);
				limpiaAtributo('lblValorUnitario', 0);
			}
			limpiaAtributo('lblIdTransaccion', 0);
			limpiaAtributo('txtIdArticulo', 0);
			limpiaAtributo('txtCantidad', 0);
			limpiaAtributo('cmbIva', 0);
			limpiaAtributo('lblValorImpuesto', 0);
			limpiaAtributo('lblIdSolicitud', 0);
			break;
		case 'limpCamposDevolucionTransaccion':
			limpiaAtributo('cmbIdLote', 0);
			limpiaAtributo('lblValorUnitario', 0);
			limpiaAtributo('lblIdTransaccion', 0);
			limpiaAtributo('txtIdArticulo', 0);
			limpiaAtributo('txtCantidad', 0);
			limpiaAtributo('cmbIva', 0);
			limpiaAtributo('lblValorImpuesto', 0);
			limpiaAtributo('lblIdDevolucion', 0);
			break;
		case 'limpCamposTransaccion': /*limpia campos de la ventana entrada de bodegas*/
			if (valorAtributo('lblNaturaleza') == 'I') {
				limpiaAtributo('txtLote', 0);
				limpiaAtributo('txtValorU', 0);
				limpiaAtributo('txtPrecioVenta', 0);
				limpiaAtributo('lblSubTotal', 0);
				limpiaAtributo('lblTotal', 0);
				limpiaAtributo('txtFechaVencimiento', 0);
				limpiaAtributo('txtSerial', 0);
				limpiaAtributo('cmbMedida', 0);
				limpiaAtributo('chkLote', 0);
			} else {
				limpiaAtributo('cmbIdLote', 0);
				limpiaAtributo('lblValorUnitario', 0);
				limpiaAtributo('txtSerial', 0);
				limpiaAtributo('lblExistencias', 0);
				limpiaAtributo('txtFV', 0);
				limpiaAtributo('txtIdBarCode', 0);
				limpiaAtributo('lblMedida', 0);
				limpiaAtributo('lblTraidosCodBarras', 0);

				limpiaAtributo('txtIdLote1', 0);

				limpiaAtributo('txtVlrUnitarioAnterior', 0);
				limpiaAtributo('cmbIdDescuento', 0);

				asignaAtributo('txtIdLoteBan', 'NO', 0);

				$('#txtIdBarCode').focus();
			}
			if (valorAtributo('cmbIdTipoDocumento') == '19') limpiaAtributo('lblIdTransaccionDev', 0);
			limpiaAtributo('lblIdTransaccion', 0);
			limpiaAtributo('txtIdArticulo', 0);
			limpiaAtributo('txtCantidad', 0);
			limpiaAtributo('cmbIva', 0);
			limpiaAtributo('lblValorImpuesto', 0);
			limpiaAtributo('lblSubTotal', 0);
			limpiaAtributo('lblTotal', 0);
			break;
		case 'limpCamposTransaccion_dos': /*limpia campos de la ventana entrada de bodegas*/
			if (valorAtributo('lblNaturaleza') == 'I') {
				limpiaAtributo('txtLote', 0);
				limpiaAtributo('txtValorU', 0);
				limpiaAtributo('txtPrecioVenta', 0);
				limpiaAtributo('lblSubTotal', 0);
				limpiaAtributo('lblTotal', 0);
				limpiaAtributo('txtFechaVencimiento', 0);
				limpiaAtributo('txtSerial', 0);
			} else {
				limpiaAtributo('cmbIdLote', 0);
				limpiaAtributo('lblValorUnitario', 0);
				limpiaAtributo('txtSerial', 0);
				limpiaAtributo('lblExistencias', 0);
				limpiaAtributo('txtFV', 0);
				limpiaAtributo('txtIdBarCode', 0);
				limpiaAtributo('lblTraidosCodBarras', 0);

				limpiaAtributo('txtIdLote1', 0);
				asignaAtributo('txtIdLoteBan', 'NO', 0);

				limpiaAtributo('txtIdArticulo_dos', 0);
				limpiaAtributo('txtIdLote_dos', 0);
				limpiaAtributo('txtIdLote_dos', 0);
				limpiaAtributo('txtFV_dos', 0);


				$('#txtIdBarCode').focus();
			}
			if (valorAtributo('cmbIdTipoDocumento') == '19') limpiaAtributo('lblIdTransaccionDev', 0);
			limpiaAtributo('lblIdTransaccion', 0);
			limpiaAtributo('txtIdArticulo', 0);
			limpiaAtributo('txtCantidad', 0);
			limpiaAtributo('cmbIva', 0);
			limpiaAtributo('lblValorImpuesto', 0);
			limpiaAtributo('lblSubTotal', 0);
			limpiaAtributo('lblTotal', 0);
			break;
		case 'limpCamposTransaccionDevolucionCompra':
			limpiaAtributo('lblIdTransaccion', 0);
			limpiaAtributo('txtIdArticulo', 0);
			limpiaAtributo('txtCantidad', 0);
			limpiaAtributo('lblIdLote', 0);
			limpiaAtributo('lblValorImpuesto', 0);
			limpiaAtributo('lblSubTotal', 0);
			limpiaAtributo('lblTotal', 0);
			break;
		case 'listGrillaDocumentosBodegaConsumo':
			limpiaAtributo('lblIdDoc', 0);
			limpiaAtributo('lblIdEstado', 0);
			break;
		case 'limpCamposDocumento':
			limpiaAtributo('lblIdDoc', 0);
			limpiaAtributo('txtDocEgreso', 0);
			limpiaAtributo('cmbIdTipoDocumento', 0);
			limpiaAtributo('txtObservacion', 0);
			limpiaAtributo('cmbIdEstado', 0);
			limpiaAtributo('lblIdEstado', 0);
			limpiaAtributo('txtNumero', 0);
			limpiaAtributo('txtIdTercero', 0);
			limpiaAtributo('txtFechaDocumento', 0);
			if (valorAtributo('txtNaturaleza') == 'I') {
				limpiaAtributo('txtValorFlete', 0);
			}
			break;
		case 'limpCamposBodega':
			limpiaAtributo('cmbIdBodega', 1);
			limpiaAtributo('txtDescripcion', 0);
			limpiaAtributo('cmbIdEmpresa', 0);
			limpiaAtributo('cmbIdCentroCosto', 0);
			limpiaAtributo('cmbIdIdResponsable', 0);
			limpiaAtributo('cmbEstado', 0);
			limpiaAtributo('cmbSWRestitucion', 0);
			limpiaAtributo('txtAutorizacion', 0);
			limpiaAtributo('cmbSWRestriccionStock', 0);
			limpiaAtributo('cmbIdBodegaTipo', 0);
			limpiaAtributo('txtIdCodProveedor', 0);
			limpiaAtributo('cmbSWConsignacion', 0);
			limpiaAtributo('cmbSWAprovechamiento', 0);
			limpiaAtributo('cmbSWAfectaCosto', 0);
			break;
		case 'monitorizacionHH':
			limpiaAtributo('txtMSistolica', 0);
			limpiaAtributo('txtMDiastolica', 0);
			limpiaAtributo('txtFCardiaca', 0);
			limpiaAtributo('txtFRespiracion', 0);
			limpiaAtributo('txtTemperaturaMH', 0);
			limpiaAtributo('cmbHoraMHH', 0);
			limpiaAtributo('cmbMinutoMHH', 0);
			limpiaAtributo('txtMHObservacion', 0);
			break;
		case 'solicitudesFarmacia':
			limpiaAtributo('txtIdArticuloSolicitudes', 0);
			limpiaAtributo('txtCantidadSolicitud', 0);
			limpiaAtributo('txtObservacionSolicitud', 0);
			limpiaAtributo('lblIdSolicitud', 0);
			break;
		case 'solicitudesFGastos':
			limpiaAtributo('txtIdArticuloSGastos', 0);
			limpiaAtributo('txtCantidadSGasto', 0);
			limpiaAtributo('txtObservacionSGasto', 0);
			limpiaAtributo('lblIdSGasto', 0);
			break;
		case 'hojaGastos':
			limpiaAtributo('txtIdArticuloHG', 0);
			limpiaAtributo('cmbCantidadHG', 0);
			limpiaAtributo('txtIndicacionesHG', 0);
			break;
		case 'plantillaCanastaDetalle':
			limpiaAtributo('lblIdDetalle', 0);
			limpiaAtributo('txtIdArticulo', 0);
			limpiaAtributo('cmbCantidad', 0);
			limpiaAtributo('lbl_Unidad', 0);

			break;
		case 'plantillaCanasta':
			limpiaAtributo('lblCodigoCanasta', 0);
			limpiaAtributo('txtNombreCanasta', 0);
			limpiaAtributo('txtContenidoCanasta', 0);
			limpiaAtributo('cmbTipoCanasta', 0);
			limpiaAtributo('cmbVigente', 0);

			break;
		case 'AdmiMedicamento':
			limpiaAtributo('txtIdMedicamento', 0);
			limpiaAtributo('cmbIdViaMed', 0);
			limpiaAtributo('cmbUnidadAdm', 0);
			limpiaAtributo('cmbCantAdm', 0);
			limpiaAtributo('txtIndicaciones', 0);
			break;
		case 'plantillaFormulario':
			limpiaAtributo('txtNombre', 0);
			limpiaAtributo('txtContenido', 0);
			limpiaAtributo('cmbTipo', 0);
			limpiaAtributo('lblCodigo', 0);
			limpiaAtributo('txtAnCol1', 0);
			limpiaAtributo('txtAnCol2', 0);
			limpiaAtributo('txtTabla', 0);
			break;


		case 'plantillaElemento':
			limpiaAtributo('cmbExamen', 0);
			limpiaAtributo('txtOrden', 0);
			limpiaAtributo('txtDescripcion', 0);
			limpiaAtributo('txtEtiqueta', 0);
			limpiaAtributo('txtReferencia', 0);
			limpiaAtributo('txtOrdenPadre', 0);
			limpiaAtributo('txtAncho', 0);
			limpiaAtributo('txtMaxlenght', 0);
			limpiaAtributo('cmbJerarquia', 0);
			limpiaAtributo('cmbTipoEntrada', 0);
			limpiaAtributo('txtValorDefecto', 0);
			limpiaAtributo('txtidFormula', 0);


			break;

		case 'listGrillaVentana':
			limpiarListadosTotales('listGrillaVentana', 0);
			break;
		case 'GestionEvento':
			limpiaAtributo('txtIdEvento', 0);
			limpiaAtributo('txtIdEstado', 1);
			limpiaAtributo('txtDescripcion', 0);
			limpiaAtributo('cmbIdTipoEvento', 0);
			limpiaAtributo('cmbIdIdProceso', 0);
			limpiaAtributo('txtObservacion', 0);
			limpiaAtributo('cmbIdIdResponsable', 0);
			limpiaAtributo('txtPriorizacion', 0);
			limpiaAtributo('txtNoDocumento', 0);
			limpiaAtributo('txtDescDocumento', 0);
			limpiaAtributo('txtFechaEntrega', 0);

			limpiaAtributo('cmbIdMotivoAplaza', 0);
			limpiaAtributo('txtFechaAplazada', 0);
			limpiaAtributo('txtObservacionAplaza', 0);

			limpiaAtributo('cmbIdCalificacion', 0);
			limpiaAtributo('cmbIdPersonalRecibe', 0);
			limpiaAtributo('txtObservacionRecibe', 0);
			limpiaAtributo('txtFechaRecibe', 0);
			break;
		case 'evento':
			limpiaAtributo('txtId', 0);
			limpiaAtributo('txtDescripcion', 0);
			limpiaAtributo('lblIdEventoARelacio', 0);
			limpiaAtributo('lblDesEventoARelacio', 0);
			break;
		case 'articulo':
			limpiaAtributo('txtIdArticulo', 1);
			limpiaAtributo('txtNombreArticulo', 0);
			limpiaAtributo('txtNombreComercial', 0);
			limpiaAtributo('cmbIdGrupo', 0);
			limpiaAtributo('cmbIdClase', 0);
			//limpiaAtributo('cmbIdTipo',0);
			//limpiaAtributo('cmbIdSubTipo',0);
			limpiaAtributo('cmbPresentacion', 0);
			limpiaAtributo('txtConcentracion', 0);
			limpiaAtributo('txtCodArticulo', 0);
			limpiaAtributo('txtDescripcionCompleta', 0);
			limpiaAtributo('txtDescripcionAbreviada', 0);
			limpiaAtributo('txtIdFabricante', 0);
			limpiaAtributo('cmbIdUnidad', 0);
			limpiaAtributo('txtValorUnidad', 0);
			limpiaAtributo('cmbML', 0);
			limpiaAtributo('txtIVA', 0);
			limpiaAtributo('cmbIVAVenta', 0);
			limpiaAtributo('txtInvima', 0);
			limpiaAtributo('txtBarras', 0);
			limpiaAtributo('txtMarca', 0);
			limpiaAtributo('txtATC', 0);
			limpiaAtributo('txtCodAtc', 0); /**/
			limpiaAtributo('cmbIdFechaVencimiento', 0);
			limpiaAtributo('cmbIdProveedorUnico', 0);
			limpiaAtributo('cmbPOS', 0);
			limpiaAtributo('cmbVigente', 0);

			limpiaAtributo('cmbAnatomofarmacologico', 0);
			limpiaAtributo('cmbPrincipioActivo', 0);
			limpiaAtributo('cmbFormaFarmacologica', 0);
			limpiaAtributo('txtFactorConv', 0);

			limpiaAtributo('cmbViaArticulo', 0);
			limpiaAtributo('txtIdViaArticulo', 0);
			limpiaAtributo('lblNomArticulo', 0);

			break;

		case 'articuloTemporal':
			limpiaAtributo('txtNombreArticulo', 0);
			limpiaAtributo('txtNombreComercial', 0);
			limpiaAtributo('cmbIdClase', 0);
			limpiaAtributo('cmbPresentacion', 0);
			limpiaAtributo('txtConcentracion', 0);
			limpiaAtributo('txtATC', 0);
			limpiaAtributo('txtCodAtc', 0);
			limpiaAtributo('cmbPOS', 0);
			limpiaAtributo('cmbFormaFarmacologica', 0);
			$('#listGrillaViaArticulo').clearGridData(true);
			//.trigger("reloadGrid");

			break;



		case 'planContratacion':
			limpiaAtributo('txtIdPlan', 0);
			limpiaAtributo('txtDescPlan', 0);
			limpiaAtributo('txtIdAdministradora', 0);
			limpiaAtributo('cmbIdTipoCliente', 0);
			limpiaAtributo('txtNumeroContrato', 0);
			limpiaAtributo('cmbIdTipoRegimen', 0)
			limpiaAtributo('txtFechaInicio', 0);
			limpiaAtributo('txtFechaFinal', 0);
			limpiaAtributo('txtMontoContrato', 0);
			limpiaAtributo('cmbIdTipoPlan', 0);
			limpiaAtributo('lblFechaRegistro', 0);
			limpiaAtributo('txtObservacion', 0);
			limpiaAtributo('lblIdElaboro', 0);
			limpiaAtributo('cmbIdEstadoEdit', 0);
			break;

		case 'limpiarProcedimientoPlan':
			limpiaAtributo('txtIdProcedimiento', 0); 
			limpiaAtributo('txtPrecioProcedimiento', 0);
			limpiaAtributo('txtCodigoExterno', 0);
			limpiaAtributo('cmbNombreExterno', 0);
			limpiaAtributo('txtNombreExterno', 0);
			limpiaAtributo('txtCantidadMaximaMes', 0);
			break;

		case 'limpiarParametrosRuta':
			limpiaAtributo('cmbIdEspecialidad', 0);
			limpiaAtributo('cmbIdSubEspecialidad', 0);
			limpiaAtributo('cmbTipoCita', 0);
			limpiaAtributo('cmbEsperar', 0);
			limpiaAtributo('txtOrden', 0);
			limpiaAtributo('cmbEstado', 0);
			cmbReset('cmbEstado', 0);
			break;

		case 'limpiarFuncionariosVentas':
			limpiarListadosTotales('listFuncionariosTerceros')
			limpiaAtributo('cmbTipoId', 0);
			limpiaAtributo('txtIdentificacion', 0);
			limpiaAtributo('txtNombre', 0);
			limpiaAtributo('txtCorreo', 0);
			limpiaAtributo('txtTelefonoFu', 0);
			limpiaAtributo('cmbVigente', 0);

			break;
		case 'limpiarTerceros':
			limpiaAtributo('txtId', 1);
			limpiaAtributo('txtTipoId', 0);
			limpiaAtributo('txtIdTercero', 0);
			limpiaAtributo('txtNombreTercero', 0);
			limpiaAtributo('txtCodigo', 0);
			limpiaAtributo('txtDireccion', 0);
			limpiaAtributo('txtTelefono', 0);
			limpiaAtributo('cmbPersonaJuridica', 0);
			limpiaAtributo('txtDV', 0);
			limpiaAtributo('cmbVigenteTercero', 0);
			limpiaAtributo('cmbProveedor', 0);
			break;
		case 'limpiarExcepcionesProcedimiento':
			limpiaAtributo('txtCantidadExcepcion', 0);
			limpiaAtributo('year', 0);
			limpiaAtributo('mes', 0);

			break;

		case 'tarifarioDetalles':
			limpiaAtributo('lblCargo', 0); limpiaAtributo('txtIdCargoNuevo', 0); $('#drag' + ventanaActual.num).find('#txtIdCargoNuevo').attr('disabled', '');
			limpiaAtributo('txtDescCargoTarifario', 0);
			limpiaAtributo('cmbIdGrupoTipoCargo', 0);
			limpiaAtributo('cmbIdTipoCargo', 0);
			limpiaAtributo('cmbIdGrupoTarifario', 0);
			limpiaAtributo('cmbIdSubGrupoTarifario', 0);
			limpiaAtributo('cmbIdConceptoRips', 0);
			limpiaAtributo('cmbIdNivelAtencion', 0);
			limpiaAtributo('txtUnidadPrecio', 0);
			limpiaAtributo('cmbIdTipoUnidad', 0);
			limpiaAtributo('txtGravamen', 0);
			limpiaAtributo('cmbHonorarios', 0);
			limpiaAtributo('cmbExigeCantidad', 0);
			limpiaAtributo('cmbVigente', 0);

			limpiarListadosTotales('listGrillaTarifarioEquivalencia');

			break;
		case 'datosDeHistoriaClinicaPaciente':

			limpiaAtributo('txtMotivoConsulta', 0);

			limpiaAtributo('txtMotivoConsulta', 0);
			limpiaAtributo('txtEnfermedadActual', 0);
			asignaAtributo('cmb_SintomaticoPiel', 'NO', 0)
			asignaAtributo('cmb_SintomaticoRespira', 'NO', 0)


			limpiaAtributo('cmbAntecedenteTipo', 0);
			limpiaAtributo('cmbAntecedenteGrupo', 0);
			asignaAtributo('cmbTiene', 'NO', 0);
			asignaAtributo('cmbControlada', 'SI', 0);
			limpiaAtributo('txtAntecedenteObservaciones', 0);


			$('#drag' + ventanaActual.num).find('#1-Neurologico').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#2-Organo_de_los_sentidos').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#3-Cardiovascular').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#4-Gastrointestinal').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#5-Genitourinario').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#6-Neurologico').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#7-Piel_y_anexos').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#8-Osteomuscular').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#9-Endocrino').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#10-Psicosocial').attr('checked', '');
			limpiaAtributo('txtSistema', 0);
			limpiaAtributo('txtHallazgo', 0);

			asignaAtributo('txtTemperatura', '37', 0);
			asignaAtributo('txtSistolica', '120', 0);
			asignaAtributo('txtDiastolica', '80', 0);
			asignaAtributo('txtPulso', '82', 0);
			asignaAtributo('txtRespiracion', '14', 0);
			//limpiaAtributo('txtSistolica',0);
			//limpiaAtributo('txtDiastolica',0);
			//limpiaAtributo('txtPulso',0);
			//limpiaAtributo('txtRespiracion',0);
			limpiaAtributo('txtPeso', 0);
			limpiaAtributo('txtTalla', 0);
			limpiaAtributo('lblIMC', 0);
			limpiaAtributo('txtObservacionesExamenFisico', 0);

			limpiaAtributo('txtIdDx', 0);
			limpiaAtributo('cmbDxTipo', 0);
			limpiaAtributo('cmbDxClase', 0);
			limpiaAtributo('cmbIdSitioQuirurgicoDx', 0);
			limpiaAtributo('txtDxObservacion', 0);

			break;
		case 'datosListaEspera':
			limpiaAtributo('txtIdBusPaciente', 0);
			limpiaAtributo('cmbTipoId', 0);
			limpiaAtributo('txtIdentificacion', 0);
			limpiaAtributo('txtApellido1', 0);
			limpiaAtributo('txtApellido2', 0);
			limpiaAtributo('txtNombre1', 0);
			limpiaAtributo('txtNombre2', 0);
			limpiaAtributo('txtFechaNac', 0);
			limpiaAtributo('cmbSexo', 0);
			limpiaAtributo('txtMunicipio', 0);
			limpiaAtributo('txtDireccionRes', 0);
			limpiaAtributo('txtNomAcompanante', 0);
			limpiaAtributo('txtTelefonos', 0);
			limpiaAtributo('txtCelular1', 0);
			limpiaAtributo('txtCelular2', 0);
			limpiaAtributo('txtAdministradora1', 0);
			limpiaAtributo('txtObservacion', 0);
			limpiaAtributo('cmbIdEspecialidad', 0);
			//limpiaAtributo('cmbIdSubEspecialidad', 0);
			limpiaAtributo('cmbDiscapaciFisica', 0);
			limpiaAtributo('cmbEmbarazo', 0);
			limpiaAtributo('cmbTipoCita', 0);
			limpiaAtributo('cmbEsperar', 0);
			limpiaAtributo('cmbNecesidad', 0);
			limpiaAtributo('cmbEstado', 0)
			limpiaAtributo('txtEmail', 0)
			limpiaAtributo('lblIdPlanContratacion', 0)
			limpiaAtributo('lblNomPlanContratacion', 0)
			limpiaAtributo('cmbIdTipoRegimen', 0)
			$("#cmbSitio option[value='10']").attr('selected', 'selected');
			$("#cmbEstado option[value='1']").attr('selected', 'selected');
			limpiaAtributo('txtAdministradoraPaciente', 0)
			limpiarListadosTotales('listHistoricoListaEspera');
			limpTablas('listDocumentosHistoricos')
			limpiarListadosTotales('listProcedimientoLE');



			break;


		case 'datosListaEsperaCirugia':
			limpiaAtributo('txtIdBusPaciente', 0);
			limpiaAtributo('cmbTipoId', 0);
			limpiaAtributo('txtIdentificacion', 0);
			limpiaAtributo('txtApellido1', 0);
			limpiaAtributo('txtApellido2', 0);
			limpiaAtributo('txtNombre1', 0);
			limpiaAtributo('txtNombre2', 0);
			limpiaAtributo('txtFechaNac', 0);
			limpiaAtributo('cmbSexo', 0);
			limpiaAtributo('txtMunicipio', 0);
			limpiaAtributo('txtDireccionRes', 0);
			limpiaAtributo('txtNomAcompanante', 0);
			limpiaAtributo('txtTelefonos', 0);
			limpiaAtributo('txtCelular1', 0);
			limpiaAtributo('txtCelular2', 0);
			limpiaAtributo('txtAdministradora1', 0);
			limpiaAtributo('txtObservacion', 0);
			limpiaAtributo('cmbIdEspecialidad', 0);
			//limpiaAtributo('cmbIdSubEspecialidad', 0);
			limpiaAtributo('cmbDiscapaciFisica', 0);
			limpiaAtributo('cmbEmbarazo', 0);
			limpiaAtributo('cmbTipoCita', 0);
			limpiaAtributo('cmbEsperar', 0);
			limpiaAtributo('cmbNecesidad', 0);
			limpiaAtributo('cmbEstado', 0)
			limpiaAtributo('txtEmail', 0)
			limpiaAtributo('lblIdPlanContratacion', 0)
			limpiaAtributo('lblNomPlanContratacion', 0)
			limpiaAtributo('cmbIdTipoRegimen', 0)
			limpiarListadosTotales('listHistoricoListaEspera');
			limpTablas('listDocumentosHistoricos')
			limpiarListadosTotales('listCitaCirugiaProcedimientoLE');
			break;

		case 'camposDivVentanitaAgenda':
			limpiaAtributo('txtIdBusPaciente', 0);
			limpiaAtributo('cmbTipoId', 0);
			limpiaAtributo('txtIdentificacion', 0);
			limpiaAtributo('txtApellido1', 0);
			limpiaAtributo('txtApellido2', 0);
			limpiaAtributo('txtNombre1', 0);
			limpiaAtributo('txtNombre2', 0);
			limpiaAtributo('txtFechaNac', 0);
			limpiaAtributo('cmbSexo', 0);
			limpiaAtributo('txtMunicipio', 0);
			limpiaAtributo('txtDireccionRes', 0);
			limpiaAtributo('txtNomAcompanante', 0);
			limpiaAtributo('txtTelefonos', 0);
			limpiaAtributo('txtCelular1', 0);
			limpiaAtributo('txtCelular2', 0);
			limpiaAtributo('txtAdministradora1', 0);
			limpiaAtributo('txtNoAutorizacion', 0);
			limpiaAtributo('txtFechaVigencia', 0);
			limpiaAtributo('txtIPSRemite', 0);
			limpiaAtributo('txtFechaPacienteCita', 0);
			limpiaAtributo('txtObservacion', 0);
			limpiaAtributo('txtObservacionConfirma', 0);
			limpiaAtributo('cmbTipoCita', 0);
			limpiaAtributo('cmbEstadoCita', 0);
			limpiaAtributo('cmbMotivoConsulta', 0);
			limpiaAtributo('lblIdListaEspera', 0);
			limpiaAtributo('txtObservacionFolio', 0);

			limpiaAtributo('txtNoLente', 0);
			limpiaAtributo('cmbTipoAnestesia', 0);
			limpiaAtributo('txtIdInstrumentador', 0);
			limpiaAtributo('txtIdCirculante', 0);
			limpiaAtributo('cmbIdSitioQuirurgico', 0);
			limpiaAtributo('txtIdProcedimiento', 0);
			limpiaAtributo('txtAdministradoraPaciente', 0);

			break;

			case 'camposPaciente':
				limpiaAtributo('lblIdPaciente', 0);
				limpiaAtributo('cmbTipoId', 0);
				limpiaAtributo('txtIdentificacion', 0);
				limpiaAtributo('txtApellido1', 0);
				limpiaAtributo('txtApellido2', 0);
				limpiaAtributo('txtNombre1', 0);
				limpiaAtributo('txtNombre2', 0);
				limpiaAtributo('txtFechaNac', 0);
				limpiaAtributo('cmbSexo', 0);
				limpiaAtributo('txtMunicipio', 0);
				limpiaAtributo('txtDireccionRes', 0);
				limpiaAtributo('txtNomAcompanante', 0);
				limpiaAtributo('txtTelefonos', 0);
				limpiaAtributo('txtCelular1', 0);
				limpiaAtributo('txtCelular2', 0);
				limpiaAtributo('txtEmail', 0);
				
	
				break;

		case 'listProcedimientos':
			limpiaAtributo('lblIdProcedimiento', 0);
			limpiaAtributo('cmbIdClase', 0);
			limpiaAtributo('cmbIdOrigenAtencion', 0);
			limpiaAtributo('cmbIdTipoServicioSolicitado', 0);
			limpiaAtributo('cmbIdPrioridad', 0);
			limpiaAtributo('cmbIdGuia', 0);
			limpiaAtributo('cmbCantidad', 0);
			limpiaAtributo('cmbIdSitioQuirurgico', 0);
			limpiaAtributo('txtJustificacion', 0);
			limpiaAtributo('txtIdProcedimiento', 0);
			limpiaAtributo('txtIndicacion', 0);

			limpiarListadosTotales('listProcedimientos');
			limpiarListadosTotales('listProcedimientosDetalle');
			break;
		case 'listAyudasDiagnosticas':
			limpiaAtributo('lblIdProcedimiento', 0);
			limpiaAtributo('cmbIdClaseAD', 0);
			limpiaAtributo('cmbIdOrigenAtencionAD', 0);
			limpiaAtributo('cmbIdTipoServicioSolicitadoAD', 0);
			limpiaAtributo('cmbIdPrioridadAD', 0);
			limpiaAtributo('cmbIdGuiaAD', 0);
			limpiaAtributo('cmbCantidadAD', 0);
			limpiaAtributo('txtJustificacionAD', 0);
			limpiaAtributo('txtIdAyudaDiagnostica', 0);
			limpiaAtributo('txtIndicacionAD', 0);

			limpiarListadosTotales('listAyudasDiagnosticas');
			limpiarListadosTotales('listAyudasDiagnosticasDetalle');
			break;
		case 'listLaboratorios':
			limpiaAtributo('lblIdProcedimiento', 0);
			limpiaAtributo('cmbIdClaseLaboratorio', 0);
			limpiaAtributo('cmbIdOrigenAtencionLaboratorio', 0);
			limpiaAtributo('cmbIdTipoServicioSolicitadoLaboratorio', 0);
			limpiaAtributo('cmbIdPrioridadLaboratorio', 0);
			limpiaAtributo('cmbIdGuiaLaboratorio', 0);
			limpiaAtributo('cmbCantidadLaboratorio', 0);
			limpiaAtributo('txtJustificacionLaboratorio', 0);
			limpiaAtributo('txtIdLaboratorio', 0);
			limpiaAtributo('txtIndicacionLaboratorio', 0);

			limpiarListadosTotales('listLaboratorios');
			limpiarListadosTotales('listLaboratoriosDetalle');
			break;
		case 'listTerapia':
			limpiaAtributo('lblIdProcedimiento', 0);
			limpiaAtributo('cmbIdClaseTerapia', 0);
			limpiaAtributo('cmbIdOrigenAtencionTerapia', 0);
			limpiaAtributo('cmbIdTipoServicioSolicitadoTerapia', 0);
			limpiaAtributo('cmbIdPrioridadTerapia', 0);
			limpiaAtributo('cmbIdGuiaTerapia', 0);
			limpiaAtributo('cmbCantidadTerapia', 0);
			limpiaAtributo('txtJustificacionTerapia', 0);
			limpiaAtributo('txtIdTerapia', 0);
			limpiaAtributo('txtIndicacionTerapia', 0);

			limpiarListadosTotales('listTerapia');
			limpiarListadosTotales('listTerapiaDetalle');
			break;
		case 'listOrdenesAdministracionDetalle':
			//			       limpiarListadosTotales('listGrilla');
			limpiarListadosTotales('listOrdenesAdministracionDetalle');

			break;
		case 'listTransaccionBodega':
			limpiaAtributo('txtIdArticulo', 0);
			limpiaAtributo('cmbIdConcepto', 0);
			limpiaAtributo('txtCantidad', 0);
			limpiaAtributo('txtValor', 0);
			break;
		case 'datosReferenciaCompraActivos':  // para activarlos 
			limpiaAtributo('cmbConcepto', 0);
			limpiaAtributo('txtIdProveedor', 0);
			limpiaAtributo('cmbIdTipoReferencia', 0);
			limpiaAtributo('txtNoReferencia', 0);
			limpiaAtributo('txtFechaReferencia', 0);
			limpiaAtributo('txtObservacionReferencia', 0);
			break;

		case 'datosReferenciaCompraInactivos':
			limpiaAtributo('txtIdProveedor', 1);
			limpiaAtributo('cmbIdTipoReferencia', 1);
			limpiaAtributo('txtNoReferencia', 1);
			limpiaAtributo('txtFechaReferencia', 1);
			limpiaAtributo('txtObservacionReferencia', 1);
			break;
		case 'documentoYTransaccionInventarioDevoluciones':
			limpiaAtributo('lblIdDocumento', 0);
			limpiaAtributo('cmbConcepto', 0);
			limpiaAtributo('cmbCentroCostoSolicita', 0);
			limpiaAtributo('txtFechaConsumo', 0);
			limpiarListadosTotales('listDocumentosInventarioDevoluciones');
			limpiarListadosTotales('listTransaccionesInventarioDevoluciones');
			break;
		case 'documentoYTransaccionInventarioEntrar':
			limpiaAtributo('lblIdDocumento', 0);
			limpiaAtributo('txtIdArticulo', 0);
			limpiaAtributo('cmbIdConcepto', 0);
			limpiaAtributo('txtCantidad', 0);
			limpiaAtributo('txtValor', 0);
			limpiarListadosTotales('listDocumentosInventario');
			limpiarListadosTotales('listTransaccionesInventarioBodega');
			break;
		case 'documentoYTransaccionInventarioTraslado':
			limpiaAtributo('lblIdDocumento', 0);
			limpiaAtributo('txtCantidad', 0);
			limpiarListadosTotales('listDocumentosInventarioTraslado');
			limpiarListadosTotales('listTransaccionesInventarioTraslado');
			break;

		case 'bodegasConTransacciones':
			limpiaAtributo('lblId', 0);
			limpiaAtributo('txtNombreBodega', 0);
			limpiaAtributo('cmbCentroCosto', 0);
			limpiaAtributo('txtPropietario', 0);
			limpiaAtributo('cmbAfectaInventario', 0);
			limpiaAtributo('cmbPrincipal', 0);
			limpiarListadosTotales('listArticulosBodega');
			limpiarListadosTotales('listBodegas');
			break;

		case 'documentoYTransaccionInventario':

			limpiaAtributo('lblIdEstadoDocumento', 0);
			limpiaAtributo('lblIdAutorDocumento', 0);
			limpiaAtributo('lblIdDocumento', 0);
			limpiaAtributo('cmbConcepto', 0);
			//  limpiaAtributo('txtFechaConsumo',0);
			limpiaAtributo('cmbUbicacion', 0);
			limpiarListadosTotales('listTransaccionesInventario');
			limpiarListadosTotales('listDocumentosHistoricosInventario');
			limpiarListadosTotales('listTransaccionesInventarioPendientes');
			limpiarListadosTotales('listTransaccionesInventarioConsolidado');
			break;
		case 'ordenes':   //limpiarDivEditarJuan('ordenes'); 
			//   limpiarElementosDeSeleccionParaLista('listOrdenesMedicamentos');
			// limpiarListadosTotales('listOrdenesMedicamentos');

			limpiaAtributo('lblIdContacto', 0);
			limpiaAtributo('lblIdHc', 0);
			limpiaAtributo('lblIdPaciente', 0);
			limpiaAtributo('lblNombrePaciente', 0);
			limpiaAtributo('cmbTipoDocumento', 0);
			limpiaAtributo('txtIdEsdadoDocumento', 0);
			limpiaAtributo('txtIdEditable', 0);
			limpiaAtributo('txtIdProfesionalElaboro', 0);
			limpiaAtributo('lblIdDocumento', 0);
			limpiaAtributo('lblIdDocumento', 0);
			limpiaAtributo('cmbConcepto', 0);


			//				   limpiarElementosDeSeleccionParaLista('listOrdenesInsumos');
			//			   limpiarListadosTotales('listOrdenesInsumos');
			break;
		case 'paciente':
			limpiaAtributo('cmbTipoIdEdit', 0);
			limpiaAtributo('txtIdentificacion', 0);
			limpiaAtributo('txtIdPaciente', 0);
			limpiaAtributo('txtNombre1', 0);
			limpiaAtributo('txtNombre2', 0);
			limpiaAtributo('txtApellido1', 0);
			limpiaAtributo('txtApellido2', 0);
			limpiaAtributo('cmbSexo', 0);
			limpiaAtributo('txtFechaNacimiento', 0);
			limpiaAtributo('txtTelefono', 0);
			limpiaAtributo('txtCelular1', 0);
			limpiaAtributo('txtCelular2', 0);
			limpiaAtributo('txtMunicipioResi', 0);
			limpiaAtributo('txtDireccion', 0);
			limpiaAtributo('txtObservaciones', 0);
			limpiaAtributo('lblId', 0);
			limpiaAtributo('txtIdentificacionNueva', 0);
			limpiaAtributo('cmbTipoIdNueva', 0);
			limpiaAtributo('chk_nuevaIdPaciente', 0);
			limpiaAtributo('txtEmail', 0);
			limpiaAtributo('txtAdministradora1', 0);
			break;
		case 'reportarNoConformidad':
			$('#drag' + ventanaActual.num).find('#txtDescripEvento').val('');
			$("#cmbproceso option[value='00']").attr('selected', 'selected');
			$("#cmbNoConformidades option[value='00']").attr('selected', 'selected');
			$("#cmbUbicacionArea option[value='00']").attr('selected', 'selected');
			$("#cmbHoraEvent option[value='--']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtFechaEvento').val('');
			$('#drag' + ventanaActual.num).find('#txtMinutEvent').val('01');
			$('#drag' + ventanaActual.num).find('#txtMinutEvent').attr('disabled', '');
			$('#drag' + ventanaActual.num).find('#txtFechaEvento').attr('disabled', '');
			$('#drag' + ventanaActual.num).find('#cmbHoraEvent').attr('disabled', '');
			$('#drag' + ventanaActual.num).find('#txtDescripEvento').attr('disabled', '');


			$('#drag' + ventanaActual.num).find('#lblIdEvento').html('');
			$('#drag' + ventanaActual.num).find('#lblIdISORef').html('');
			$('#drag' + ventanaActual.num).find('#txtDescripEventoPosTrata').val('');
			$('#drag' + ventanaActual.num).find('#txtDescripEvento').attr('disabled', '');	//se habilita si paso de buscar a nuevo				
			$('#drag' + ventanaActual.num).find('#txtDescripEventoPosTrata').attr('disabled', '');	//se habilita si paso de buscar a nuevo							
			$('#drag' + ventanaActual.num).find('#lblEstadoEvent').html('');


			// para planes de accion
			limpiarListadosTotales('listPlanAccion');
			limpiarListadosTotales('listPersonasPlanAccion');
			limpiarListadosTotales('listAsociarEventos');
			limpiarListadosTotales('listPlanAccionSeguimiento');
			$('#drag' + ventanaActual.num).find('#lblIdISORef').html('');
			$('#drag' + ventanaActual.num).find('#lblDescripEventoPosTrata').html('');
			$('#drag' + ventanaActual.num).find('#txtConcepNoConforCalidad').val('');
			$("#cmbResponsaDestinoEvento option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#rbtImpacto').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#rbtNoImpacto').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#txtAnalisisCausas').val('');
			$('#drag' + ventanaActual.num).find('#txtFechaIniPlanAccion').val('');
			$('#drag' + ventanaActual.num).find('#txtMetaPlanAccion').val('');
			$("#cmbTipoPlanAccion option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtFechaIniPlanAccion').val('');
			$('#drag' + ventanaActual.num).find('#lblIdEventoDoc').html('');
			$("#cmbTipoPlanAccion option[value='00']").attr('selected', 'selected');
			$("#cmbCaracter option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtRealizarAcciones').val('');
			$("#cmbQuienRealizaAccion option[value='00']").attr('selected', 'selected');
			$("#cmbPersonaQuienRealizaAccion option[value='00']").attr('selected', 'selected');
			$("#cmbBusproceso2 option[value='00']").attr('selected', 'selected');
			$("#cmbBusNoConformidades2 option[value='00']").attr('selected', 'selected');
			$("#cmbBusUbicacionArea2 option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtBusFechaEventoNoConfDesde2').val('');
			$('#drag' + ventanaActual.num).find('#txtBusFechaEventoNoConfHasta2').val('');
			$("#cmbBusEstadoEvento2 option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#lblEventoPadreAsocia').html('');
			$('#drag' + ventanaActual.num).find('#lblFechaIniPlanAccion').html('');
			$("#cmbEficaz option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#lblIdEventoDocSeg').html('');
			$("#cmbSeguimietoCumple option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtRealizarAcciones').val('');
			$('#drag' + ventanaActual.num).find('#txtSeguimiAcciones').val('');
			$('#drag' + ventanaActual.num).find('#lblFechaIniPlanAccion').html('');
			$('#drag' + ventanaActual.num).find('#lblMetaPlanAccion').html('');
			$('#drag' + ventanaActual.num).find('#lblIdEventoDocSeg').html('');

			break;
		case 'administrarNoConformidad':
			limpiarListadosTotales('listPlanAccion');
			limpiarListadosTotales('listPersonasPlanAccion');
			limpiarListadosTotales('listEventosParaAsociar');
			limpiarListadosTotales('listAsociarEventos');
			limpiarListadosTotales('listPlanAccionSeguimiento');
			// para planes de accion
			$('#drag' + ventanaActual.num).find('#lblIdISORef').html('');
			$('#drag' + ventanaActual.num).find('#lblDescripEventoPosTrata').html('');
			$('#drag' + ventanaActual.num).find('#txtConcepNoConforCalidad').val('');
			$("#cmbResponsaDestinoEvento option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#rbtImpacto').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#rbtNoImpacto').attr('checked', '');
			$('#drag' + ventanaActual.num).find('#txtAnalisisCausas').val('');
			$('#drag' + ventanaActual.num).find('#txtFechaIniPlanAccion').val('');
			$('#drag' + ventanaActual.num).find('#txtMetaPlanAccion').val('');
			$("#cmbTipoPlanAccion option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtFechaIniPlanAccion').val('');
			$('#drag' + ventanaActual.num).find('#lblIdEventoDoc').html('');
			$("#cmbTipoPlanAccion option[value='00']").attr('selected', 'selected');
			$("#cmbCaracter option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtRealizarAcciones').val('');
			$("#cmbQuienRealizaAccion option[value='00']").attr('selected', 'selected');
			$("#cmbPersonaQuienRealizaAccion option[value='00']").attr('selected', 'selected');
			$("#cmbBusproceso2 option[value='00']").attr('selected', 'selected');
			$("#cmbBusNoConformidades2 option[value='00']").attr('selected', 'selected');
			$("#cmbBusUbicacionArea2 option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtBusFechaEventoNoConfDesde2').val('');
			$('#drag' + ventanaActual.num).find('#txtBusFechaEventoNoConfHasta2').val('');
			$("#cmbBusEstadoEvento2 option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#lblEventoPadreAsocia').html('');
			$('#drag' + ventanaActual.num).find('#lblFechaIniPlanAccion').html('');
			$("#cmbEficaz option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#lblIdEventoDocSeg').html('');
			$("#cmbSeguimietoCumple option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtRealizarAcciones').val('');
			$('#drag' + ventanaActual.num).find('#txtSeguimiAcciones').val('');
			$('#drag' + ventanaActual.num).find('#lblFechaIniPlanAccion').html('');
			$('#drag' + ventanaActual.num).find('#lblMetaPlanAccion').html('');
			$('#drag' + ventanaActual.num).find('#lblIdEventoDocSeg').html('');
			break;
		case 'reportarEventoAdverso':
			$("#cmbproceso option[value='00']").attr('selected', 'selected');
			$("#cmbClaseEA option[value='00']").attr('selected', 'selected');
			$("#cmbEAProceso option[value='00']").attr('selected', 'selected');
			$("#cmbUbicacionArea option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#lblIdEvento').html('');
			$('#drag' + ventanaActual.num).find('#lblFechaEvento').html('');
			$('#drag' + ventanaActual.num).find('#txtDescripEvento').val('');
			$('#drag' + ventanaActual.num).find('#txtBusIdPaciente').val('');
			$('#drag' + ventanaActual.num).find('#txtDescripEvento').val('');
			$('#drag' + ventanaActual.num).find('#txtFechaEvento').val('');
			$('#drag' + ventanaActual.num).find('#txtTratamiento').val('');
			break;

		case 'administrarEventoAdverso':

			$("#cmbproceso option[value='00']").attr('selected', 'selected');
			$("#cmbClaseEA option[value='00']").attr('selected', 'selected');
			$("#cmbEventoProceso option[value='00']").attr('selected', 'selected');
			$("#cmbUbicacionArea option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#lblIdEvento').html('');
			$('#drag' + ventanaActual.num).find('#lblDescripEvento').html('');
			$('#drag' + ventanaActual.num).find('#lblFechaEvento').html('');
			$('#drag' + ventanaActual.num).find('#lblTratamiento').html('');
			$('#drag' + ventanaActual.num).find('#lblIdPaciente').html('');
			$('#drag' + ventanaActual.num).find('#lblEstadoEvent').html('');
			$('#drag' + ventanaActual.num).find('#lblNomElavoroGestion').html('');
			$('#drag' + ventanaActual.num).find('#lblEventoPadreAsocia').html('');



			$("#cmbBarreraYDefensa option[value='00']").attr('selected', 'selected');
			$("#cmbAccionInsegura option[value='00']").attr('selected', 'selected');
			$("#cmbOrigenesFactContrib option[value='00']").attr('selected', 'selected');
			$("#cmbAmbito option[value='00']").attr('selected', 'selected');
			$("#cmbPrevenible option[value='00']").attr('selected', 'selected');
			$("#cmbOrganizCultura option[value='00']").attr('selected', 'selected');
			limpiarListadosTotales('listFactContribu');



			$('#drag' + ventanaActual.num).find('#txtAccionesOportunidadMejora').val('');
			$('#drag' + ventanaActual.num).find('#txtFechaCuandoOportMejora').val('');
			$("#cmbQuienRealizaoportMejora option[value='00']").attr('selected', 'selected');
			limpiarListadosTotales('listPersonasOportMejora');

			$("#cmbOportMejora option[value='00']").attr('selected', 'selected');
			$("#cmbTiempoEjecucion option[value='00']").attr('selected', 'selected');

			$('#drag' + ventanaActual.num).find('#txtTareaOport').val('');
			$('#drag' + ventanaActual.num).find('#txtTareaOportSeguimient').val('');
			limpiarListadosTotales('listSeguimientoOportMejora');


			$('#drag' + ventanaActual.num).find('#txtDescResponsabili').val('');
			$("#cmbNivelResponsabili option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtConclusiones').val('');
			limpiarListadosTotales('listNivelResponsabili');

			limpiarDivEditarJuan('administrarPlanMejoraEA');// limpiar para planes de accion
			limpiarListadosTotales('listMedicaDiagnosticos');



			break;
		case 'administrarPlanMejora':
			limpiarListadosTotales('listTareasPlanAccion');
			limpiarListadosTotales('listEspinaPescado');
			limpiarListadosTotales('listEventosParaAsociar');
			limpiarListadosTotales('listAsociarEventosTareas');
			limpiarListadosTotales('listPersonasPlanAccionGral');
			limpiarListadosTotales('listSeguimientoPlanAccion');
			// para planes de accion			   
			$('#drag' + ventanaActual.num).find('#lblIdPlanAccion').html('');
			$('#drag' + ventanaActual.num).find('#txtConceptoGral').val('');
			$("#cmbResponsable option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#lblEventoPadreAsocia').html('');
			$('#drag' + ventanaActual.num).find('#txtAnalisisCausas').val('');

			$('#drag' + ventanaActual.num).find('#txtRiesgoNoHacerlo').val('');
			$('#drag' + ventanaActual.num).find('#txtBeneficiosHacerlo').val('');

			$('#drag' + ventanaActual.num).find('#txtFechaIniPlanAccion').val('');
			$('#drag' + ventanaActual.num).find('#txtMetaPlanAccion').val('');
			$("#cmbTipoPlanAccion option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtFechaIniPlanAccion').val('');
			$("#cmbCaracter option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtRealizarAcciones').val('');
			$("#cmbQuienRealizaAccion option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtFechaCuando').val('');
			$('#drag' + ventanaActual.num).find('#txtCostosAdd').val('0');
			$('#drag' + ventanaActual.num).find('#chkRecursoAdd').attr('checked', '');
			$("#cmbFactor option[value='00']").attr('selected', 'selected');
			$("#cmbCriterioCausa option[value='00']").attr('selected', 'selected');
			//$("#txtAnalisisFactor option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtAnalisisFactor').val('');
			$("#cmbPersonaQuienRealizaAccion option[value='00']").attr('selected', 'selected');

			$("#cmbEficaz option[value='00']").attr('selected', 'selected');
			$("#cmbSeguimietoCumple option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtSeguimMetaPlanAccion').val('');
			$('#drag' + ventanaActual.num).find('#txtRealizarAcciones').val('');

			break;
		case 'administrarPlanMejoraEA':
			$('#drag' + ventanaActual.num).find('#lblIdEventoEA').html('');
			$("#cmbprocesoEA option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtAspectoMejorar').val('');
			$('#drag' + ventanaActual.num).find('#lblEstadoEvent').html('');
			$("#cmbTipoPlan option[value='00']").attr('selected', 'selected');
			$("#cmbRiesgo option[value='00']").attr('selected', 'selected');
			$("#cmbCosto option[value='00']").attr('selected', 'selected');
			$("#cmbVolumen option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#lblTotPrioriza').html('0');
			break;
		case 'relacionEventoOM':
			limpiaAtributo('lblIdEventoOM', 0);
			limpiaAtributo('cmbProcesoOM', 0);
			limpiaAtributo('txtAspectoMejorarOM', 0);
			limpiaAtributo('cmbTipoPlanOM', 0);
			limpiaAtributo('cmbRiesgoOM', 0);
			limpiaAtributo('cmbCostoOM', 0);
			limpiaAtributo('cmbVolumenOM', 0);
			limpiaAtributo('lblTotPriorizaOM', 0);
			break;
		case 'adminItem':
			$('#drag' + ventanaActual.num).find('#lblCodItem').html('');
			$('#drag' + ventanaActual.num).find('#txtNomItem').val('');
			$("#cmbUnidMed option[value='00']").attr('selected', 'selected');
			$("#cmbServicio option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtObserva').val('');

			$("#cmbGrupoItem option[value='00']").attr('selected', 'selected');
			$("#cmbClase option[value='00']").attr('selected', 'selected');
			$("#cmbEstado option[value='00']").attr('selected', 'selected');
			break;
		case 'fichaTecnica':
			limpiaAtributo('lblId', 0);
			limpiaAtributo('txtNombre', 0)
			limpiaAtributo('cmbTipo', 0)
			limpiaAtributo('cmbProceso', 0)
			limpiaAtributo('txtObjetivo', 0)
			limpiaAtributo('cmbEstado', 0)
			limpiaAtributo('cmbPeriodicidad', 0)
			limpiaAtributo('txtFechaVigenciaIni', 0)
			limpiaAtributo('cmbOrigenInfo', 0)
			limpiaAtributo('cmbUnidadMedida', 0);
			limpiaAtributo('txtDescripcionResponsable', 0);
			limpiaAtributo('txtLineaBase', 0);
			limpiaAtributo('txtMeta', 0);
			limpiaAtributo('chkDenominadorRequerido', 0);
			limpiaAtributo('txtDescripcionDenominador', 0);
			limpiaAtributo('txtDescripcionNumerador', 0);
			limpiaAtributo('txtInfoAdicional', 0);
			limpiaAtributo('cmbTendencia', 0);

			break;
		case 'fichaPlatin':
			limpiaAtributo('cmbPrograma', 0);
			limpiaAtributo('txtIdPaciente', 0)
			limpiaAtributo('cmbTipoPlan', 0)
			limpiaAtributo('txtFechaIniPlatin', 0)
			limpiaAtributo('txtFechaFinPlatin', 0)

			limpiaAtributo('lblNomRemitidoPor', 0)
			limpiaAtributo('lblMotivoIngreso', 0)
			limpiaAtributo('cmbMotivoRemision', 0)
			limpiaAtributo('lblEdadPaciente', 0)
			limpiaAtributo('lblFechaIngresoM', 0)
			break;


	}
}

function limpiarElementosDeSeleccionParaLista(arg) {

	switch (arg) {

		case 'listOrdenesMedicamentos':
			limpiaAtributo('txtIdArticulo', 0);
			$("#cmbIdVia" + " option[value='']").attr('selected', 'selected');	// limpiaAtributo('cmbIdVia',0);					  
			$("#cmbIdTipoDosis" + " option[value='']").attr('selected', 'selected');	// limpiaAtributo('cmbIdTipoDosis',0);				
			$("#cmbIdRepeticionProgramada" + " option[value='']").attr('selected', 'selected');	// limpiaAtributo('cmbIdRepeticionProgramada',0);				
			limpiaAtributo('txtObservaciones', 0);
			limpiaAtributo('chkInmediato', 0);
			limpiaAtributo('lblDosis', 0);
			limpiaAtributo('lblTotUnidades', 0);
			limpiarTodosLosCombosDeLaDosisOrdenMedica();
			asignaAtributo('txtFechaOrdenMed', document.getElementById('lblFechaDeHoy').lastChild.nodeValue, 0);

			break;
		case 'listOrdenesMedicamentosModificar':
			limpiaAtributo('txtIdArticulo', 0);
			$("#cmbIdVia" + " option[value='']").attr('selected', 'selected');	// limpiaAtributo('cmbIdVia',0);					  
			$("#cmbIdTipoDosis" + " option[value='']").attr('selected', 'selected');	// limpiaAtributo('cmbIdTipoDosis',0);				
			$("#cmbIdRepeticionProgramada" + " option[value='']").attr('selected', 'selected');	// limpiaAtributo('cmbIdRepeticionProgramada',0);				
			limpiaAtributo('txtObservaciones', 0);
			limpiaAtributo('chkInmediato', 0);
			limpiaAtributo('lblDosis', 0);
			limpiaAtributo('lblTotUnidades', 0);
			limpiaAtributo('lblIdArticulo', 0);
			limpiarTodosLosCombosDeLaDosisOrdenMedica();
			asignaAtributo('txtFechaOrdenMed', document.getElementById('lblFechaDeHoy').lastChild.nodeValue, 0);

			break;
		case 'listOrdenesInsumos':
			limpiaAtributo('txtIdArticuloInsumo', 0);
			$("#cmbDosisCantInsumo" + " option[value='']").attr('selected', 'selected');	// limpiaAtributo('cmbIdVia',0);					  
			$("#cmbIdTipoDosisInsumo" + " option[value='']").attr('selected', 'selected');	// limpiaAtributo('cmbIdTipoDosis',0);				
			$("#cmbIdRepeticionProgramadaInsumo" + " option[value='']").attr('selected', 'selected');	// limpiaAtributo('cmbIdRepeticionProgramada',0);				
			limpiaAtributo('txtObservacionesInsumo', 0);
			limpiaAtributo('chkInmediatoInsumo', 0);
			limpiaAtributo('lblDosisInsumo', 0);
			limpiaAtributo('lblTotUnidadesInsumo', 0);
			asignaAtributo('txtFechaOrdenMedInsumo', document.getElementById('lblFechaDeHoy').lastChild.nodeValue, 0);

		case 'listTareasPlanAccion':
			$("#cmbCaracter option[value='00']").attr('selected', 'selected');
			$("#cmbSeguimietoCumple option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtRealizarAcciones').val('');
			$('#drag' + ventanaActual.num).find('#txtFechaCuando').val('');
			$("#cmbQuienRealizaAccion option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtCostosAdd').val('0');
			break;
		case 'listEspinaPescado':
			$("#cmbFactor option[value='00']").attr('selected', 'selected');
			$("#cmbCriterioCausa option[value='00']").attr('selected', 'selected');
			$('#drag' + ventanaActual.num).find('#txtAnalisisFactor').val('');
			break;
		case 'listPersonasPlanAccionGral':
			$("#cmbPersonaQuienRealizaAccion option[value='00']").attr('selected', 'selected');
			break;
		/*ini platin*/
		case 'listDxIntegralInicial':
			$('#drag' + ventanaActual.num).find('#txtDxIntegralInicial').val('');
			$('#drag' + ventanaActual.num).find('#lblIdDx').html('')
			break;
		case 'listSituaActual':
			$('#drag' + ventanaActual.num).find('#txtSituacionActual_' + tabActivo).val('');
			$('#drag' + ventanaActual.num).find('#lblIdSituacionActual_' + tabActivo).html('')
			break;
		case 'listObjMeta':
			$('#drag' + ventanaActual.num).find('#txtObjetivo_' + tabActivo).val('');
			$('#drag' + ventanaActual.num).find('#txtMeta_' + tabActivo).val('');
			$('#drag' + ventanaActual.num).find('#lblIdObjetivo_' + tabActivo).html('')
			break;
		case 'listActividad':
			$('#drag' + ventanaActual.num).find('#txtActividad_' + tabActivo).val('');
			$('#drag' + ventanaActual.num).find('#lblIdActividad_' + tabActivo).html('')
			break;
	}
}

function limpiarDatosAdmision() {

	limpiaAtributo('lblIdCita', 0);
	limpiaAtributo('txtIdBusPaciente', 0);
	limpiaAtributo('txtIdentificacion', 0);
	limpiaAtributo('txtNombre1', 0);
	limpiaAtributo('txtNombre2', 0);
	limpiaAtributo('txtApellido1', 0);
	limpiaAtributo('txtApellido2', 0);
	limpiaAtributo('txtFechaNac', 0);
	limpiaAtributo('cmbSexo', 0);
	limpiaAtributo('txtMunicipio', 0);
	limpiaAtributo('txtDireccionRes', 0);
	limpiaAtributo('txtNomAcompanante', 0);
	limpiaAtributo('txtTelefonos', 0);
	limpiaAtributo('txtCelular1', 0);
	limpiaAtributo('txtCelular2', 0);
	limpiaAtributo('txtEmail', 0);
	limpiaAtributo('cmbIdEtnia', 0);
	limpiaAtributo('cmbIdNivelEscolaridad', 0);
	limpiaAtributo('cmbIdOcupacion', 0);
	limpiaAtributo('cmbIdEstrato', 0);

	limpiaAtributo('txtAdministradora1', 0);
	limpiaAtributo('txtNoAutorizacion', 0);
	limpiaAtributo('txtIPSRemite', 0);
	limpiaAtributo('txtObservacion', 0);
	limpiaAtributo('txtIdDx', 0);
	limpiaAtributo('txtIdOcupacion', 0);

	limpiaAtributo('cmbIdEspecialidad', 0);
	limpiaAtributo('cmbIdProfesionales', 0);
	limpiaAtributo('cmbIdTipoRegimen', 0);
}

function limpiarEvento() {

	limpiaAtributo('txtIdEstado', 0);
	limpiaAtributo('lblPersonalRegistra', 0);
	limpiaAtributo('lblAnulado', 0);
	limpiaAtributo('lblDescripcion', 0);
	limpiaAtributo('cmbIdTipoEvento', 0);
	limpiaAtributo('cmbIdIdProceso', 0);
	limpiaAtributo('txtObservacion', 0);
	limpiaAtributo('cmbIdIdResponsable', 0);
	limpiaAtributo('txtPriorizacion', 0);
	limpiaAtributo('txtNoDocumento', 0);
	limpiaAtributo('txtDescDocumento', 0);
	limpiaAtributo('txtFechaInicia', 0);
	limpiaAtributo('txtFechaEntrega', 0);

}


function limpiarDatosAdmisiones() {
	limpiaAtributo('lblIdCita', 0);
	limpiaAtributo('txtIdBusPaciente', 0);
	limpiaAtributo('txtIdentificacion', 0);
	limpiaAtributo('txtNombre1', 0);
	limpiaAtributo('txtNombre2', 0);
	limpiaAtributo('txtApellido1', 0);
	limpiaAtributo('txtApellido2', 0);
	limpiaAtributo('txtFechaNac', 0);
	limpiaAtributo('cmbSexo', 0);
	limpiaAtributo('txtMunicipio', 0);
	limpiaAtributo('txtDireccionRes', 0);
	limpiaAtributo('txtNomAcompanante', 0);
	limpiaAtributo('txtTelefonos', 0);
	limpiaAtributo('txtCelular1', 0);
	limpiaAtributo('txtCelular2', 0);
	limpiaAtributo('txtEmail', 0);
	limpiaAtributo('cmbIdEtnia', 0);
	limpiaAtributo('cmbIdNivelEscolaridad', 0);
	limpiaAtributo('cmbIdOcupacion', 0);
	limpiaAtributo('cmbIdEstrato', 0);

	limpiaAtributo('txtAdministradora1', 0);
	limpiaAtributo('txtNoAutorizacion', 0);
	limpiaAtributo('txtIPSRemite', 0);
	limpiaAtributo('txtObservacion', 0);
	//limpiaAtributo('txtIdDx', 0);
	limpiaAtributo('txtIdOcupacion', 0);

	limpiaAtributo('cmbIdEspecialidad', 0);
	limpiaAtributo('cmbIdProfesionales', 0);
	limpiaAtributo('cmbIdTipoRegimen', 0);
	limpiaAtributo('cmbIdPlan', 0);

	limpiarListadosTotales('listCitaCirugiaProcedimiento')
	limpiarListadosTotales('listAdmisionCuenta')
	limpiarListadosTotales('listProcedimientosDeFactura');


	limpiaAtributo('lblIdAdmision', 0);
	limpiaAtributo('lblIdTipoAdmision', 0);
	limpiaAtributo('lblNomTipoAdmision', 0);
	limpiaAtributo('lblIdEpsPlan', 0);
	limpiaAtributo('lblIdPlan', 0);
	limpiaAtributo('lblNomPlan', 0);
	limpiaAtributo('lblIdRegimen', 0);
	limpiaAtributo('lblNomRegimen', 0);
	limpiaAtributo('lblIdTipoAfiliado', 0);
	limpiaAtributo('lblNomTipoAfiliado', 0);

	limpiaAtributo('lblIdRango', 0);
	limpiaAtributo('lblNomRango', 0);
	limpiaAtributo('lblUsuarioCrea', 0);


	limpiaAtributo('txtIdProcedimientoCex', 0);
	limpiaAtributo('lblValorUnitarioProc', 0);
	limpiaAtributo('cmbCantidad', 0);

	//info admisión
	limpiaAtributo('cmbIdTipoRegimen', 0);
	limpiaAtributo('cmbIdTipoAfiliado', 0);
	limpiaAtributo('cmbIdRango', 0);
	limpiaAtributo('txtNoAutorizacion', 0);
	limpiaAtributo('cmbTipoAdmision', 0);
	limpiaAtributo('lblIdPlanContratacion', 0);
	limpiaAtributo('lblNomPlanContratacion', 0);

	limpiaAtributo('lblIdFactura', 0);
	limpiaAtributo('lblIdEstadoFactura', 0);
	limpiaAtributo('lblEstadoFactura', 0);
	limpiaAtributo('lblNumeroFactura', 0);
	limpiaAtributo('lblIdRecibo', 0);
	limpiaAtributo('lblTotalFactura', 0);
	limpiaAtributo('lblValorNoCubierto', 0);
	limpiaAtributo('lblValorCubierto', 0);
	limpiaAtributo('lblDescuento', 0);



}

function limpiarDatosPacienteAdmisionCita() {
	limpiarListadosTotales('listCitaCirugiaProcedimiento');
	$('#drag' + ventanaActual.num).find('#txtIdBusPacienteCita').val('');
	$("#cmbIdEspecialidadCita option[value='']").attr('selected', 'selected');
	$("#cmbIdProfesionalesCita option[value='']").attr('selected', 'selected');
	$("#cmbExito option[value='S']").attr('selected', 'selected');
}

function limpiarVentanaUrgencias() {
	limpiaAtributo("cmbTipoIdUrgencias", 0)
	limpiaAtributo("txtIdentificacionUrgencias", 0)
	limpiaAtributo("txtIdBusPacienteUrgencias", 0)
	limpiaAtributo("cmbIdTipoServicioUrgencias", 0)
	limpiaAtributo("lblIdPacienteUrgencias", 0)
}

function limpiarDatosPacienteCita() {

	$('#drag' + ventanaActual.num).find('#lblIdCita').html('');
	$('#drag' + ventanaActual.num).find('#lblIdListaEspera').html('');
	$('#drag' + ventanaActual.num).find('#txtBusIdPaciente').val('');
	$('#drag' + ventanaActual.num).find('#txtIdPaciente').val('');
	$('#drag' + ventanaActual.num).find('#txtNombre1').val('');
	$('#drag' + ventanaActual.num).find('#txtNombre2').val('');
	$('#drag' + ventanaActual.num).find('#txtApellido1').val('');
	$('#drag' + ventanaActual.num).find('#txtApellido2').val('');

	$('#drag' + ventanaActual.num).find('#txtTelefonos').val('');
	$('#drag' + ventanaActual.num).find('#txtObservacion').val('');
	$('#drag' + ventanaActual.num).find('#txtDireccionRes').val('');
	$('#drag' + ventanaActual.num).find('#txtNomAcompanante').val('');


	$("#cmbTipoUsuario option[value='00']").attr('selected', 'selected');
	$("#cmbTipoCita option[value='00']").attr('selected', 'selected');
	$("#cmbEstadoCita option[value='00']").attr('selected', 'selected');

	$('#drag' + ventanaActual.num).find('#lblIdTurno').html('');
	$('#drag' + ventanaActual.num).find('#lblFecha').html('');
	$('#drag' + ventanaActual.num).find('#lblHora').html('');
	$('#drag' + ventanaActual.num).find('#txtMunicipio').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora1').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora2').val('');
	$('#drag' + ventanaActual.num).find('#txtObservacion').val('');

	/*pa deschequear */
	$('#drag' + ventanaActual.num).find('#txtBusIdPaciente').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#cmbTipoId').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#txtIdPaciente').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#txtNombre1').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#txtNombre2').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#txtApellido1').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#txtApellido2').attr('disabled', '');

}
function limpiarAutocompleIdPaciente() {

	$('#drag' + ventanaActual.num).find('#txtIdPaciente').val($('#drag' + ventanaActual.num).find('#txtBusIdPaciente').val());
	$('#drag' + ventanaActual.num).find('#txtBusIdPaciente').val('');
	$("#cmbTipoUsuario option[value='00']").attr('selected', 'selected');
	$("#cmbTipoCita option[value='00']").attr('selected', 'selected');
	$("#cmbEstadoCita option[value='00']").attr('selected', 'selected');

	$('#drag' + ventanaActual.num).find('#txtDireccionRes').val('');
	$('#drag' + ventanaActual.num).find('#txtNomAcompanante').val('');
	$('#drag' + ventanaActual.num).find('#txtTelefonos').val('');
	$('#drag' + ventanaActual.num).find('#txtObservacion').val('');


	$('#drag' + ventanaActual.num).find('#txtMunicipio').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora1').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora2').val('');

}
function limpiarAutocompleIdPacienteListaEspera() {

	$('#drag' + ventanaActual.num).find('#txtIdPacienteListaEspera').val($('#drag' + ventanaActual.num).find('#txtBusIdPacienteListaEspera').val());
	$('#drag' + ventanaActual.num).find('#txtBusIdPacienteListaEspera').val('');
	$("#cmbTipoIdListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbTipoUsuarioListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbTipoCitaListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbEstadoCitaListaEspera option[value='00']").attr('selected', 'selected');

	$('#drag' + ventanaActual.num).find('#txtDireccionResListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtNomAcompananteListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtTelefonosListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtObservacionListaEspera').val('');


	$('#drag' + ventanaActual.num).find('#txtMunicipioListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora1ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora2ListaEspera').val('');

}
function limpiarDatosPacientePaciente() {
	$('#drag' + ventanaActual.num).find('#txtIdPaciente').val('');
	$('#drag' + ventanaActual.num).find('#txtNombre1').val('');
	$('#drag' + ventanaActual.num).find('#txtNombre2').val('');
	$('#drag' + ventanaActual.num).find('#txtApellido1').val('');
	$('#drag' + ventanaActual.num).find('#txtApellido2').val('');

	$('#drag' + ventanaActual.num).find('#txtTelefonos').val('');
	$('#drag' + ventanaActual.num).find('#txtObservacion').val('');
	$('#drag' + ventanaActual.num).find('#txtDireccionRes').val('');
	$('#drag' + ventanaActual.num).find('#txtNomAcompanante').val('');


	$("#cmbTipoUsuario option[value='00']").attr('selected', 'selected');
	$("#cmbTipoCita option[value='00']").attr('selected', 'selected');
	$("#cmbEstadoCita option[value='00']").attr('selected', 'selected');

	$('#drag' + ventanaActual.num).find('#txtMunicipio').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora1').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora2').val('');

}
function limpiarDatosPacientePacienteListaEspera() {
	$('#drag' + ventanaActual.num).find('#txtIdPacienteListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtNombre1ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtNombre2ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtApellido1ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtApellido2ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtFechaNacimientoListaEspera').val('');
	$("#cmbSexoListaEspera option[value='00']").attr('selected', 'selected');

	$('#drag' + ventanaActual.num).find('#txtTelefonosListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtObservacionListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtDireccionResListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtNomAcompananteListaEspera').val('');


	$("#cmbTipoUsuarioListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbTipoCitaListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbEstadoCitaListaEspera option[value='00']").attr('selected', 'selected');

	$('#drag' + ventanaActual.num).find('#txtMunicipioListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora1ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora2ListaEspera').val('');

}


function limpiarDatosPacienteListaEspera() {

	limpiarListadosTotales('listListaEsperaTraer');

	$('#drag' + ventanaActual.num).find('#txtBusIdPacienteListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtIdPacienteListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtNombre1ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtNombre2ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtApellido1ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtApellido2ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtFechaNacimientoListaEspera').val('');

	$('#drag' + ventanaActual.num).find('#txtTelefonosListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtObservacionListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtDireccionResListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtNomAcompananteListaEspera').val('');


	$("#cmbTipoUsuarioListaEspera option[value='00']").attr('selected', 'selected');

	$('#drag' + ventanaActual.num).find('#txtMunicipioListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora1ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtAdministradora2ListaEspera').val('');
	$('#drag' + ventanaActual.num).find('#txtObservacionListaEspera').val('');

	/*pa deschequear */
	$('#drag' + ventanaActual.num).find('#txtBusIdPacienteListaEspera').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#cmbTipoIdListaEspera').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#txtIdPacienteListaEspera').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#txtNombre1ListaEspera').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#txtNombre2ListaEspera').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#txtApellido1ListaEspera').attr('disabled', '');
	$('#drag' + ventanaActual.num).find('#txtApellido2ListaEspera').attr('disabled', '');

	$("#cmbTipoIdListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbProfesionalesListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbDiscapaciFisicaListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbEmbarazoListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbTipoCitaListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbZonaResidenciaListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbNecesidadListaEspera option[value='00']").attr('selected', 'selected');
	$("#cmbDisponibleListaEspera option[value='00']").attr('selected', 'selected');

	// $("#cmbProfesionalesListaEsperaTraer option[value='00']").attr('selected', 'selected');	   solicitud de Rut       
	$('#drag' + ventanaActual.num).find('#cmbProfesionalesListaEsperaCita').attr('disabled', '');

}

function permitirNumeros(e) {
	var code = (e.which) ? e.which : e.keyCode;
	if (code > 31 && (code < 48 || code > 57)) {
		e.preventDefault();
	}
}


//			   proximo=cant+1;    	// pilas toca asi, o si no hay problemas en el  momento de consultar y agregar uno nuevo			   








/* if($.trim(datosDelRegistro.idRegistro)==proximo){ alert('ya existe un idReg con esa id')
	 //proximo=proximo+datosDelRegistro.idRegistro;

    
 }		*/

function cmbReset(idCmb, def) {
	name = String(idCmb);
	document.getElementById(name).selectedIndex = def;
}






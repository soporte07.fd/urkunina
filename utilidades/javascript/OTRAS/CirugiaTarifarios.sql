--CREAR CANDADO
INSERT INTO cirugia.candado (id, id_plan) 
VALUES( (SELECT max(id)+1 from cirugia.liquidacion), ?::integer  );
 

UPDATE cirugia.candado SET id_tarifario =  
(SELECT id_tarifario FROM  facturacion.planes  where id = (SELECT id_plan FROM cirugia.candado ));

UPDATE cirugia.candado SET id_tipo_tarifario = 
 (SELECT id_tipo_tarifario FROM facturacion.tarifarios where id = (SELECT id_tarifario FROM cirugia.candado));
 
UPDATE cirugia.candado SET  porcen_bilateral =  
 (SELECT porcen_bilateral FROM  facturacion.planes  where id = (SELECT id_plan FROM cirugia.candado ));

/* 1- CREAR LIQUIDACION CABECERA */
INSERT INTO 
  cirugia.liquidacion
(
  id,
  id_factura,
  id_finalidad,
  id_tipo_atencion,
  id_anestesiologo,
  id_tipo_anestesia,
  sw_cirujano,
  sw_anestesiologo,
  sw_ayudante,
  sw_sala,
  sw_materiales,
  id_plan,
  id_tarifario,
  porcen_variabilidad,
  id_tipo_tarifario_base
) 
VALUES ( 
   (SELECT id FROM cirugia.candado ),?::integer,?,?,?,?, ?,?,?,?,?, 
   (SELECT id_plan FROM cirugia.candado ),
   (SELECT id_tarifario FROM cirugia.candado),
   (SELECT porcen_variabilidad FROM  facturacion.planes  where id = (SELECT id_plan FROM cirugia.candado )),
   (SELECT id_tipo_tarifario FROM cirugia.candado )
);


/* 2- TRAER LOS PROCEDIMIENTOS PROGRAMADOS DE LAS AGENDAS  Y CRUZARLOS CON PLANES DE CONTRATOS Y SI NO SE ENCUENTRAN AQUI, 
CRUZA CON LOS PROCEDIMIENTOS DE LOS TARIFARIOS */

INSERT INTO
  cirugia.liquidacion_procedimientos
(
  id_liquidacion,
  id_sitio_quirurgico,
  id_procedimiento,
  id_lateralidad,
  id_tipo_unidad,
  id_unidad_precio,
  id_tipo_tarifario, /*S, I, C*/
  valor_unitario
)
(
    select
      (SELECT id FROM cirugia.candado )  id_liquidacion,
      ccp.id_sitio_quirurgico,
      ccp.id_procedimiento,
      sq.lateralidad,
      t.id_tipo_unidad,
      case when t.id_tipo_unidad = '01' then 0 else t.precio end precio,  /*01 SOLO PORQUE ES dinero*/
      case when proced_contratados.precio is not null then 'C' else (SELECT id_tipo_tarifario FROM cirugia.candado) end id_tipo_tarifario,
      case when t.id_tipo_unidad = '01' then t.precio else  proced_contratados.precio end valor_unitario   /*01 SOLO PORQUE ES dinero*/
    from
      citas.cita_cirugia_procedimiento  ccp
    inner join adm.sitio_quirurgico sq on ccp.id_sitio_quirurgico = sq.id
    left join (
        SELECT
          pd.id_procedimiento,
          pd.precio
        FROM
          facturacion.planes_detalle pd
        where  id_plan = (SELECT id_plan FROM cirugia.candado )
    )proced_contratados on  ccp.id_procedimiento =  proced_contratados.id_procedimiento

    left join (
        SELECT
          id_cargo,
          id_tipo_unidad,
          precio
        FROM
          facturacion.tarifarios_detalle
        where  id_tarifario = (SELECT id_tarifario FROM cirugia.candado )
    )t on ccp.id_procedimiento = t.id_cargo
    where ccp.id_agenda_detalle = ?::integer

);

/*3. SI HAY AL MENOS UN CONTRATADO BANDERIAR EN LIQUIDACION */
UPDATE
  cirugia.liquidacion
SET
  tot_contratadas =  ( SELECT  count(*)  tot_contratadas  
  from cirugia.liquidacion_procedimientos  where id_tipo_tarifario = 'C' and  id_liquidacion= (SELECT id FROM cirugia.candado ) ),
  tot_procedimientos =  ( SELECT  count(*)  tot_contratadas 
   from cirugia.liquidacion_procedimientos  where  id_liquidacion= (SELECT id FROM cirugia.candado ) ) 
WHERE id =  (SELECT id FROM cirugia.candado );

------------------------- REGLAS ORDENAMIENTO


/*A. SI TODOS SON CONTRATADOS*/
UPDATE
  cirugia.liquidacion_procedimientos
SET
  orden = 1
WHERE id_liquidacion =  (SELECT id FROM cirugia.candado )
AND id_procedimiento = (
      SELECT
       id_procedimiento
      FROM   cirugia.liquidacion_procedimientos
      WHERE id_liquidacion =  (SELECT id FROM cirugia.candado )
      ORDER BY valor_unitario DESC
      limit 1
)
AND (     SELECT  tot_procedimientos - tot_contratadas  valor FROM cirugia.liquidacion  WHERE id =  (SELECT id FROM cirugia.candado )      )  = 0;



/*B. TODOS ISS O TODOS SOAT*/
UPDATE
  cirugia.liquidacion_procedimientos
SET
  orden = 1
WHERE id_liquidacion =  (SELECT id FROM cirugia.candado )
AND id_procedimiento = (
      SELECT
       id_procedimiento
      FROM   cirugia.liquidacion_procedimientos
      WHERE id_liquidacion =  (SELECT id FROM cirugia.candado )
      ORDER BY id_unidad_precio DESC
      limit 1
)
AND (     SELECT  tot_contratadas  valor FROM cirugia.liquidacion  WHERE id =  (SELECT id FROM cirugia.candado )      )  = 0;


/*C. MIX, CONTRATADAS CON ISS O CONTRATADAS CON SOAT  */
UPDATE
  cirugia.liquidacion_procedimientos
SET
  orden = 1
WHERE id_liquidacion =  (SELECT id FROM cirugia.candado )
AND (     SELECT  tot_contratadas  valor FROM cirugia.liquidacion  WHERE id =  (SELECT id FROM cirugia.candado )      )  > 0
AND (     
        (SELECT  tot_procedimientos - tot_contratadas  valor FROM cirugia.liquidacion  WHERE id =  (SELECT id FROM cirugia.candado )      ) <
        (SELECT  tot_procedimientos  valor FROM cirugia.liquidacion  WHERE id =  (SELECT id FROM cirugia.candado ))
    );


/*D. solo para SOAT Y AL MENOS UNA PACTADA  sw_omite_reglas_tarifarios Y LOS PONE TODO EN 1  */
/*
UPDATE
  cirugia.liquidacion_procedimientos
SET
  orden = 1
WHERE id_liquidacion =  (SELECT id FROM cirugia.candado )
AND ( SELECT sw_omite_reglas_tarifarios FROM cirugia.candado )  = 'S'
AND id_procedimiento NOT IN ('147100','147401','147403','147402','147400','132300');
*/
-------------------------FIN  REGLAS ORDENAMIENTO



/*4. llena liquidacion de derechos ingresanto el porcentaje y se dispara el triguer por cada registro*/

INSERT INTO
  cirugia.liquidacion_derechos
(
  id_liquidacion,
  id_procedimiento,
  id_derechos,
  porcentaje,
  id_unidad_precio,
  id_tarifario,
  id_tipo_tarifario
)
(
      SELECT
              liq.id_liquidacion,
              liq.id_procedimiento,
              liq.id_derechos,
              p.porcentaje,
              liq.id_unidad_precio,
              liq.id_tarifario,
              liq.id_tipo_tarifario
      FROM
        cirugia.porcentajes p
      inner join (

                SELECT
                  lp.id_liquidacion,
                  lp.id_procedimiento,
                  d.id id_derechos,
                  '' porcentaje,
                  lp.id_unidad_precio,
                  l.id_tarifario,
                  lp.id_tipo_tarifario,

                lp.id_via_acceso,
                lp.id_lateralidad,
                lp.orden
                FROM
                  cirugia.liquidacion_procedimientos lp
                inner join  cirugia.liquidacion l on lp.id_liquidacion = l.id
                inner join (
                    SELECT --solo los derechos que estan liquidandose o chuleados en la ventana (sw = 1)
                      d.id,
                      d.nombre,
                      d.orden
                    FROM 
                      cirugia.derechos d
                    INNER JOIN (  

                        SELECT case when sw_cirujano = '1'      then 'H' else ''  end id FROM  cirugia.liquidacion where id = ( SELECT id FROM cirugia.candado)  
                        union 
                        SELECT case when sw_anestesiologo = '1' then 'A' else ''  end id FROM  cirugia.liquidacion where id = ( SELECT id FROM cirugia.candado)  
                        union
                        SELECT case when sw_sala = '1'          then 'D' else ''  end id FROM  cirugia.liquidacion where id = ( SELECT id FROM cirugia.candado)  
                        union
                        SELECT case when sw_materiales = '1'    then 'M' else ''  end id FROM  cirugia.liquidacion where id = ( SELECT id FROM cirugia.candado)  
                    )derechos_liquidados on d.id = derechos_liquidados.id            
                
                ) d on d.orden < 5
                where id_liquidacion =(SELECT id FROM cirugia.candado )
                and lp.id_tipo_tarifario !='C'
                and lp.id_tipo_unidad != '01' /*01- significa que el plan del contrato es con dinero por lo que no se aplican los calculos */
      )liq on ( p.id_tarifario = liq.id_tarifario and p.id_via_acceso= liq.id_via_acceso and p.lateralidad = liq.id_lateralidad and p.id_derechos = liq.id_derechos and p.orden_procedimiento = liq.orden)
);


/* 5. SE TRAEN LOS RESULTADOS POR PROCEDIMIENTO DE ESTA LIQUIDACION A liquidacion_procedimientos*/
UPDATE
   cirugia.liquidacion_procedimientos T1
SET
   valor_unitario = T2.valor_unitario
FROM
  (

    SELECT
      id_liquidacion,
      id_procedimiento,
      sum( valor_unitario) valor_unitario
    FROM
      cirugia.liquidacion_derechos
    where id_liquidacion= (SELECT id FROM cirugia.candado )
    group by id_liquidacion, id_procedimiento

  )T2
  INNER JOIN (

    SELECT
      id_liquidacion,
      id_procedimiento,
      sum( valor_unitario) valor_unitario
    FROM
      cirugia.liquidacion_derechos
    where id_liquidacion= (SELECT id FROM cirugia.candado )
    group by id_liquidacion, id_procedimiento

  )T3 USING (valor_unitario)

WHERE T1.id_liquidacion = (SELECT id FROM cirugia.candado )
AND   T1.id_liquidacion = T3.id_liquidacion AND   T1.id_procedimiento = T3.id_procedimiento;



/* CALCULO DEL VALOR CON DESCUENTO:
   Una vez se ejecuta el trigger, se traen por cada procedimiento de la liquidacion liquidacion_procedimientos el valor para sumarlo y ponelo en valor_unitario
   posteriormente se calcula el valor_con_descuento*/

/* 6.0 valor_con_descuento NO CONTRATADAS */ 
UPDATE
   cirugia.liquidacion_procedimientos T1
SET
   valor_con_descuento = T2.valor_con_descuento
FROM
  (

     SELECT
      id_liquidacion,
      id_procedimiento,id_tipo_tarifario,

      CASE WHEN id_tipo_tarifario !='C'  THEN lp.valor_unitario - (lp.valor_unitario * (l.porcen_variabilidad*-1) / 100  )
           ELSE
                CASE  WHEN orden = 1 THEN   valor_unitario ELSE valor_unitario/2    
           END
      END  valor_con_descuento

     FROM  cirugia.liquidacion_procedimientos lp
     INNER JOIN cirugia.liquidacion l on  lp.id_liquidacion = l.id
     where id_liquidacion= (SELECT id FROM cirugia.candado )


  )T2
  INNER JOIN (

     SELECT
      id_liquidacion,
      id_procedimiento,

      CASE WHEN id_tipo_tarifario !='C' THEN lp.valor_unitario - (lp.valor_unitario * (l.porcen_variabilidad*-1) / 100  )
           ELSE
                CASE  WHEN orden = 1 THEN   valor_unitario ELSE valor_unitario/2    END
      END  valor_con_descuento

     FROM  cirugia.liquidacion_procedimientos lp
     INNER JOIN cirugia.liquidacion l on  lp.id_liquidacion = l.id
     where id_liquidacion= (SELECT id FROM cirugia.candado )


  )T3 USING (valor_con_descuento)

WHERE T1.id_liquidacion = (SELECT id FROM cirugia.candado )
AND   T1.id_liquidacion = T3.id_liquidacion AND   T1.id_procedimiento = T3.id_procedimiento
AND   T1.id_tipo_tarifario !='C'; /*solo a las no contratadas*/ 



/* 6.1 SOAT E ISS solo a las contratadas */
UPDATE
   cirugia.liquidacion_procedimientos T1
set
   valor_con_descuento = valor_unitario * ( SELECT porcen_bilateral FROM cirugia.candado )
where  id_lateralidad = 'B'
and  id_tipo_tarifario ='C'
and  orden = 1
and  id_liquidacion = (SELECT id FROM cirugia.candado )
and ( (SELECT id_tipo_tarifario_base FROM cirugia.liquidacion LIMIT 1)  = 'S'  OR (SELECT id_tipo_tarifario_base FROM cirugia.liquidacion LIMIT 1)  = 'I');


/* 6.2 SOAT E ISS solo a las contratadas orden 2 - B*/
UPDATE
   cirugia.liquidacion_procedimientos T1
set
   valor_con_descuento = valor_con_descuento / 2
where  id_lateralidad = 'B'
and  id_tipo_tarifario ='C'
and  orden = 2
and  id_liquidacion = (SELECT id FROM cirugia.candado )
and ( (SELECT id_tipo_tarifario_base FROM cirugia.liquidacion LIMIT 1)  = 'S'  OR (SELECT id_tipo_tarifario_base FROM cirugia.liquidacion LIMIT 1)  = 'I');


/* 6.3 SOAT E ISS solo a las contratadas orden 2 - U */
UPDATE
   cirugia.liquidacion_procedimientos T1
set
   valor_con_descuento = valor_unitario / 2
where  id_lateralidad = 'U'
and  id_tipo_tarifario ='C'
and  orden = 2
and  id_liquidacion = (SELECT id FROM cirugia.candado )
and ( (SELECT id_tipo_tarifario_base FROM cirugia.liquidacion LIMIT 1)  = 'S'  OR (SELECT id_tipo_tarifario_base FROM cirugia.liquidacion LIMIT 1)  = 'I');

/* 6.4 SOAT E ISS solo a las contratadas orden 1 - U */
UPDATE
   cirugia.liquidacion_procedimientos T1
set
   valor_con_descuento = valor_unitario 
where  id_lateralidad = 'U'
and  id_tipo_tarifario ='C'
and  orden = 1
and  id_liquidacion = (SELECT id FROM cirugia.candado )
and ( (SELECT id_tipo_tarifario_base FROM cirugia.liquidacion LIMIT 1)  = 'S'  OR (SELECT id_tipo_tarifario_base FROM cirugia.liquidacion LIMIT 1)  = 'I');

/* 6.5 ISS solo a las contratadas orden 1 - U */
UPDATE
   cirugia.liquidacion_procedimientos T1
set
   valor_con_descuento = valor_unitario 
where  id_lateralidad = 'U'
and  id_tipo_tarifario ='I'
and  orden = 1
and  id_liquidacion = (SELECT id FROM cirugia.candado )
and ( (SELECT id_tipo_tarifario_base FROM cirugia.liquidacion LIMIT 1)  = 'I')
and   T1.id_tipo_unidad = '01'; /*si es dinero*/



/* 6.6 ISS solo a las contratadas orden 1 - B */
UPDATE
   cirugia.liquidacion_procedimientos T1
set
   valor_con_descuento  =  valor_unitario*2 - (valor_unitario*2 * ((SELECT porcen_variabilidad FROM cirugia.liquidacion WHERE id = (SELECT id FROM cirugia.candado ))*-1) / 100  ) 
where  id_lateralidad = 'B'
and  id_tipo_tarifario ='I'
and  orden = 1
and  id_liquidacion = (SELECT id FROM cirugia.candado )
and ( (SELECT id_tipo_tarifario_base FROM cirugia.liquidacion LIMIT 1)  = 'I')
and   id_tipo_unidad = '01'; /*si es dinero*/

/* 7. ACTUALIZA VALOR TOTAL DE LA LIQUIDACION*/
UPDATE
   cirugia.liquidacion T1
SET
   valor_total = (
     SELECT
      sum(  valor_con_descuento ) valor_con_descuento
     FROM  cirugia.liquidacion_procedimientos lp
     where id_liquidacion= ( SELECT id FROM cirugia.candado )
     group by id_liquidacion
)
WHERE T1.id = (SELECT id FROM cirugia.candado );

DELETE FROM cirugia.candado;
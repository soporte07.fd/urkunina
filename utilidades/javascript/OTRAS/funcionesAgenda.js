// JavaScript para la programacion de los botones
function actualizarCantidadTerapias() {
    terapia_seleccionada = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow');
    cantidad = $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(terapia_seleccionada).CANTIDAD;
    gestionado = $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(terapia_seleccionada).GESTIONADO;

    $("#cmbCantidadCitasTerapia").empty();
    $("#cmbCantidadCitasTerapia").append(new Option("[ SELECCIONE ]", ""));
    for (i = 1; i <= cantidad - gestionado; i++) {
        $("#cmbCantidadCitasTerapia").append(new Option(i, i));
    }
}

function ocultarAgendaOrdenes(){
    $("#copiarCitas").attr('checked', false);
    ocultar("divVentanitaAgendaOrdenes")
    setTimeout(() => {
        buscarAGENDA("listDias")
    }, 200);
}

function mostrarAgendaOrdenes() {
    if (valorAtributo('cmbSede') != '') {
        if (valorAtributo('cmbIdEspecialidad') != '') {
            if (valorAtributo('cmbIdProfesionales') != '') {
                //LIMPIAR ELEMENTOS DE LA VENTANA
                $("[id*='Ordenes']").val("");
                $("#copiarCitas").attr('checked', true)
                //PREPARA ELEMENTOS PARA LA CREACION DE CITAS
                setTimeout(() => {
                    mostrar("divVentanitaAgendaOrdenes")
                    asignaAtributo("lblSedeAgendaOrdenes", $("#cmbSede option:selected").text())
                    asignaAtributo("lblEspecialidadAgendaOrdenes", $("#cmbIdEspecialidad option:selected").text())
                    asignaAtributo("lblIdProfesionalAgendaOrdenes", valorAtributo("cmbIdProfesionales"))
                    asignaAtributo("lblProfesionalAgendaOrdenes", $("#cmbIdProfesionales option:selected").text())
                    buscarAGENDA("listDias")
                    setTimeout(() => {
                        buscarAGENDA('listCoordinacionTerapiaOrdenadosAgenda')
                        setTimeout(() => {
                            buscarAGENDA('listaProgramacionTerapiasAgenda')
                        }, 300);
                    }, 200);
                }, 200);
            } else {
                alert("FALTA SELECCIONAR PROFESIONAL")
            }
        } else {
            alert("FALTA SELECCIONAR LA ESPECIALIDAD")
        }
    } else {
        alert("FALTA SELECCIONAR LA SEDE")
    }
}

/** funciones para la accion de busqueda*/
function mostrarVentanaInfoAdmision() {
    mostrar('divAsociarCita')
    asignaAtributo("lblIdAdmisionAsociarCita", valorAtributo('lblIdAdmisionAgen'), 1);
    limpiaAtributo("lblIdFacturaAsociarCita");
    setTimeout(() => {
        buscarAGENDA('citasDisponiblesAsociar')
        setTimeout(() => {
            buscarAGENDA('citasAdmision')
        }, 300);
    }, 300);
}

function calcularMinutosParaDividir() {
    minutos_cita = valorAtributo('lblDuracion');
    $("#cmbDuracionMinutosSubDividir").empty();
    for (i = 1; i < minutos_cita; i++) {
        if (minutos_cita % i == 0) {
            $("#cmbDuracionMinutosSubDividir").append(new Option(i, i));
        }
    }
}

function cargarInfoServicio() {
    if (valorAtributo('cmbIdTipoServicio') == 'URG') {
        asignaAtributo('cmbIdEspecialidad', '39', 0)
        asignaAtributo('cmbIdSubEspecialidad', '64', 0)
        asignaAtributo('cmbTipoAdmision', 'UR', 0)
        asignaAtributo('cmbIdProfesionalesFactura', '98400407', 0)
    } else if (valorAtributo('cmbIdTipoServicio') == 'HOS') {
        asignaAtributo('cmbIdEspecialidad', '10', 0)
        asignaAtributo('cmbIdSubEspecialidad', '60', 0)
        asignaAtributo('cmbTipoAdmision', 'H1', 0)
        asignaAtributo('cmbIdProfesionalesFactura', '98400407', 0)
    }
}

function guardarUnaCitaAgenda() {
    if (valorAtributo('txtEstadoCita') == 'D') {
        buscarAGENDA('listProcedimientosAgenda')
        setTimeout("guardarYtraerDatoAlListado('consultarDisponibleDesdeAgenda')", 500)
    } else {
        if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
            if (valorAtributo('cmbEstadoCita') == 'A' || valorAtributo('cmbEstadoCita') == 'C') {
                verificarNotificacionAntesDeGuardar('crearCitaCirugia')
            } else {
                modificarCRUD('crearCitaCirugia')
            }
        }
        else {
            if (valorAtributo('cmbEstadoCita') == 'A' || valorAtributo('cmbEstadoCita') == 'C') {
                buscarAGENDA('listProcedimientosAgenda')
                setTimeout("verificarNotificacionAntesDeGuardar('crearCita')", 500)
            } else {
                buscarAGENDA('listProcedimientosAgenda')
                setTimeout("modificarCRUD('crearCita')", 500)
            }
        }
    }
}

function sacarValoresColumnasDeGrilla(idList) {
    var reg1 = '',
        reg2 = '',
        reg3 = '';
    var ids2 = jQuery("#" + idList).getDataIDs();

    for (var i = 0; i < ids2.length; i++) {
        var c2 = ids2[i];

        var RowData = jQuery("#" + idList).getRowData(c2);

        if (i < (ids2.length - 1)) {
            reg1 = reg1 + $.trim(RowData.idSitioQuirur) + '__';
            reg2 = reg2 + $.trim(RowData.idProcedimiento) + '__';
            reg3 = reg3 + $.trim(RowData.Observaciones) + '__';
        } else {
            reg1 = reg1 + $.trim(RowData.idSitioQuirur);
            reg2 = reg2 + $.trim(RowData.idProcedimiento);
            reg3 = reg3 + $.trim(RowData.Observaciones);
        }
    }
    asignaAtributo('txtColumnaIdSitioQuirur', reg1, 0)
    asignaAtributo('txtColumnaIdProced', reg2, 0)
    asignaAtributo('txtColumnaObservacion', reg3, 0)
}

function sacarValoresColumnasDeGrillaGestion(idList) { // PARA BANDEJA
    var reg1 = '',
        reg2 = '',
        reg3 = '',
        reg4 = '';
    var ids2 = jQuery("#" + idList).getDataIDs();

    for (var i = 0; i < ids2.length; i++) {
        var c2 = ids2[i];
        var RowData = jQuery("#" + idList).getRowData(c2);

        if (i < (ids2.length - 1)) {
            reg1 = reg1 + $.trim(RowData.idSitioQuirur) + '__';
            reg2 = reg2 + $.trim(RowData.idProcedimiento) + '__';
            reg3 = reg3 + $.trim(RowData.Observaciones) + '__';
            reg4 = reg4 + $.trim(RowData.ID) + '__';
        } else {
            reg1 = reg1 + $.trim(RowData.idSitioQuirur);
            reg2 = reg2 + $.trim(RowData.idProcedimiento);
            reg3 = reg3 + $.trim(RowData.Observaciones);
            reg4 = reg4 + $.trim(RowData.ID);
        }
    }
    asignaAtributo('txtColumnaIdSitioQuirur', reg1, 0)
    asignaAtributo('txtColumnaIdProced', reg2, 0)
    asignaAtributo('txtColumnaObservacion', reg3, 0)
    asignaAtributo('txtColumnaID', reg4, 0)
}



function sacarValoresColumnasDeGrillaPersonal(idList) {
    var reg1 = '',
        reg2 = '',
        reg3 = '';
    var ids2 = jQuery("#" + idList).getDataIDs();



    for (var i = 0; i < ids2.length; i++) {
        var c2 = ids2[i];

        var RowData = jQuery("#" + idList).getRowData(c2);

        if (i < (ids2.length - 1)) {
            reg1 = reg1 + $.trim(RowData.ID_PROFESION) + '__';
            reg2 = reg2 + $.trim(RowData.ID_PERSONAL) + '__';
            reg3 = reg3 + $.trim(RowData.OBSERVACION) + '__';
        } else {
            reg1 = reg1 + $.trim(RowData.ID_PROFESION);
            reg2 = reg2 + $.trim(RowData.ID_PERSONAL);
            reg3 = reg3 + $.trim(RowData.OBSERVACION);
        }
    }
    console.log(reg1 + ' : ' + reg2 + ' : ' + reg3)
    asignaAtributo('txtColumnaIdProfesion', reg1, 0)
    asignaAtributo('txtColumnaIdProfesional', reg2, 0)
    asignaAtributo('txtColumnaObservacionPersonal', reg3, 0)


}



function buscarInformacionPacienteExistenteLE() {
    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
        buscarInformacionBasicaPaciente()
        setTimeout("buscarAGENDA('listHistoricoListaEsperaCirugia')", 500);
    } else {
        buscarInformacionBasicaPaciente()
        setTimeout("buscarAGENDA('listHistoricoListaEspera')", 500);
    }
}

function buscarInformacionBasicaPacienteAgendaOrdenes() {
    if (valorAtributo('txtIdBusPacienteOrdenes') != '') {
        $.ajax({
            url: "/clinica/paginas/accionesXml/buscarHc_xml.jsp",
            type: "POST",
            data: { "accion": "InformacionBasicaDelPaciente", "id": valorAtributoIdAutoCompletar('txtIdBusPacienteOrdenes') },
            beforeSend: function () { },
            success: function (data) {
                if (data != null) {
                    totalRegistros = data.getElementsByTagName('identificacion').length;
                    if (totalRegistros > 0) {
                        IdPaciente = data.getElementsByTagName('IdPaciente')[0].firstChild.data;
                        TipoIdPaciente = data.getElementsByTagName('TipoIdPaciente')[0].firstChild.data;
                        identificacion = data.getElementsByTagName('identificacion')[0].firstChild.data;
                        nombre1 = data.getElementsByTagName('Nombre1')[0].firstChild.data;
                        nombre2 = data.getElementsByTagName('Nombre2')[0].firstChild.data;
                        apellido1 = data.getElementsByTagName('Apellido1')[0].firstChild.data;
                        apellido2 = data.getElementsByTagName('Apellido2')[0].firstChild.data;
                        idMunicipio = data.getElementsByTagName('idMunicipio')[0].firstChild.data;
                        nomMunicipio = data.getElementsByTagName('nomMunicipio')[0].firstChild.data;
                        Direccion = data.getElementsByTagName('Direccion')[0].firstChild.data;
                        Telefonos = data.getElementsByTagName('Telefonos')[0].firstChild.data;
                        Celular1 = data.getElementsByTagName('Celular1')[0].firstChild.data;
                        Celular2 = data.getElementsByTagName('Celular2')[0].firstChild.data;

                        Etnia = data.getElementsByTagName('Etnia')[0].firstChild.data;
                        NivelEscolar = data.getElementsByTagName('NivelEscolar')[0].firstChild.data;
                        idOcupacion = data.getElementsByTagName('idOcupacion')[0].firstChild.data;
                        Ocupacion = data.getElementsByTagName('Ocupacion')[0].firstChild.data;
                        Estrato = data.getElementsByTagName('Estrato')[0].firstChild.data;

                        fechaNac = data.getElementsByTagName('fechaNac')[0].firstChild.data;
                        edad = data.getElementsByTagName('edad')[0].firstChild.data;
                        sexo = data.getElementsByTagName('sexo')[0].firstChild.data;
                        acompanante = data.getElementsByTagName('acompanante')[0].firstChild.data;
                        administradora = data.getElementsByTagName('administradora')[0].firstChild.data;
                        email = data.getElementsByTagName('email')[0].firstChild.data;

                        asignaAtributo('cmbTipoIdOrdenes', TipoIdPaciente, 0)
                        asignaAtributo('txtIdentificacionOrdenes', identificacion, 0)
                        asignaAtributo('txtNombre1Ordenes', nombre1, 0)
                        asignaAtributo('txtNombre2Ordenes', nombre2, 0)
                        asignaAtributo('txtApellido1Ordenes', apellido1, 0)
                        asignaAtributo('txtApellido2Ordenes', apellido2, 0)
                        asignaAtributo('txtMunicipioOrdenes', idMunicipio + '-' + nomMunicipio, 0)
                        asignaAtributo('txtDireccionResOrdenes', Direccion, 0)
                        asignaAtributo('txtTelefonosOrdenes', Telefonos, 0)
                        asignaAtributo('txtCelular1Ordenes', Celular1, 0)
                        asignaAtributo('txtCelular2Ordenes', Celular2, 0)

                        asignaAtributo('cmbIdEtniaOrdenes', Etnia, 0)
                        asignaAtributo('cmbIdNivelEscolaridadOrdenes', NivelEscolar, 0)
                        asignaAtributo('txtIdOcupacionOrdenes', idOcupacion + '-' + Ocupacion, 0)
                        asignaAtributo('cmbIdEstratoOrdenes', Estrato, 0)

                        asignaAtributo('txtNomAcompananteOrdenes', acompanante, 0);
                        asignaAtributo('txtFechaNacOrdenes', fechaNac, 0)
                        asignaAtributo('lblEdadOrdenes', edad, 0);
                        asignaAtributo('cmbSexoOrdenes', sexo, 0)
                        asignaAtributo('txtEmailOrdenes', email, 0)
                        asignaAtributo('txtAdministradoraPacienteOrdenes', administradora, 0)

                        setTimeout(() => {
                            buscarAGENDA('listCoordinacionTerapiaOrdenadosAgenda')
                            setTimeout(() => {
                                buscarAGENDA('listaProgramacionTerapiasAgenda')
                            }, 300);
                        }, 300);

                    } else {
                        alert("PACIENTE NO EXISTE")
                    }
                } else {
                    alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) { }
        });
    } else {
        alert("INGRESE PACIENTE A BUSCAR");
    }
}

function buscarInformacionBasicaPaciente() {
    // borrarRegistrosTabla('tablaCitas');    
    if (valorAtributo('txtIdBusPaciente') != '') {
        varajaxInit = crearAjax();
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarHc_xml.jsp', true);
        varajaxInit.onreadystatechange = respuestabuscarInformacionBasicaPaciente;
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        valores_a_mandar = "accion=InformacionBasicaDelPaciente";
        valores_a_mandar = valores_a_mandar + "&id=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
        varajaxInit.send(valores_a_mandar);
    } else
        alert("INGRESE PACIENTE A BUSCAR");
}

function respuestabuscarInformacionBasicaPaciente() {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('infoBasicaPaciente') != null) {
                totalRegistros = raiz.getElementsByTagName('identificacion').length;
                if (totalRegistros > 0) {
                    IdPaciente = raiz.getElementsByTagName('IdPaciente')[0].firstChild.data;
                    TipoIdPaciente = raiz.getElementsByTagName('TipoIdPaciente')[0].firstChild.data;
                    identificacion = raiz.getElementsByTagName('identificacion')[0].firstChild.data;
                    nombre1 = raiz.getElementsByTagName('Nombre1')[0].firstChild.data;
                    nombre2 = raiz.getElementsByTagName('Nombre2')[0].firstChild.data;
                    apellido1 = raiz.getElementsByTagName('Apellido1')[0].firstChild.data;
                    apellido2 = raiz.getElementsByTagName('Apellido2')[0].firstChild.data;
                    idMunicipio = raiz.getElementsByTagName('idMunicipio')[0].firstChild.data;
                    nomMunicipio = raiz.getElementsByTagName('nomMunicipio')[0].firstChild.data;
                    Direccion = raiz.getElementsByTagName('Direccion')[0].firstChild.data;
                    Telefonos = raiz.getElementsByTagName('Telefonos')[0].firstChild.data;
                    Celular1 = raiz.getElementsByTagName('Celular1')[0].firstChild.data;
                    Celular2 = raiz.getElementsByTagName('Celular2')[0].firstChild.data;

                    Etnia = raiz.getElementsByTagName('Etnia')[0].firstChild.data;
                    NivelEscolar = raiz.getElementsByTagName('NivelEscolar')[0].firstChild.data;
                    idOcupacion = raiz.getElementsByTagName('idOcupacion')[0].firstChild.data;
                    Ocupacion = raiz.getElementsByTagName('Ocupacion')[0].firstChild.data;
                    Estrato = raiz.getElementsByTagName('Estrato')[0].firstChild.data;

                    fechaNac = raiz.getElementsByTagName('fechaNac')[0].firstChild.data;
                    edad = raiz.getElementsByTagName('edad')[0].firstChild.data;
                    sexo = raiz.getElementsByTagName('sexo')[0].firstChild.data;
                    acompanante = raiz.getElementsByTagName('acompanante')[0].firstChild.data;
                    administradora = raiz.getElementsByTagName('administradora')[0].firstChild.data;
                    email = raiz.getElementsByTagName('email')[0].firstChild.data;

                    asignaAtributo('lblIdPaciente', IdPaciente, 0)
                    asignaAtributo('cmbTipoId', TipoIdPaciente, 0)
                    asignaAtributo('txtIdentificacion', identificacion, 0)
                    asignaAtributo('txtNombre1', nombre1, 0)
                    asignaAtributo('txtNombre2', nombre2, 0)
                    asignaAtributo('txtApellido1', apellido1, 0)
                    asignaAtributo('txtApellido2', apellido2, 0)
                    asignaAtributo('txtMunicipio', idMunicipio + '-' + nomMunicipio, 0)
                    asignaAtributo('txtDireccionRes', Direccion, 0)
                    asignaAtributo('txtTelefonos', Telefonos, 0)
                    asignaAtributo('txtCelular1', Celular1, 0)
                    asignaAtributo('txtCelular2', Celular2, 0)

                    asignaAtributo('cmbIdEtnia', Etnia, 0)
                    asignaAtributo('cmbIdNivelEscolaridad', NivelEscolar, 0)
                    asignaAtributo('txtIdOcupacion', idOcupacion + '-' + Ocupacion, 0)
                    asignaAtributo('cmbIdEstrato', Estrato, 0)

                    asignaAtributo('txtNomAcompanante', acompanante, 0);
                    asignaAtributo('txtFechaNac', fechaNac, 0)
                    asignaAtributo('lblEdad', edad, 0);
                    asignaAtributo('cmbSexo', sexo, 0)
                    asignaAtributo('txtEmail', email, 0)
                    asignaAtributo('txtAdministradoraPaciente', administradora, 0)

                } else {
                    alert('PACIENTE NO EXISTE')
                }
            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}



function buscarInformacionArticuloTemporal() {
    // borrarRegistrosTabla('tablaCitas');    
    if (valorAtributo('txtNombreArticulo') != '') {
        varajaxInit = crearAjax();
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarHc_xml.jsp', true);
        varajaxInit.onreadystatechange = respuestaInformacionArticuloTemporal;
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        valores_a_mandar = "accion=InformacionArticuloTemporal";
        console.log('Valor: ' + valorAtributoIdAutoCompletar('txtNombreArticulo'))
        valores_a_mandar = valores_a_mandar + "&id=" + valorAtributoIdAutoCompletar('txtNombreArticulo');
        varajaxInit.send(valores_a_mandar);
    } else
        alert("INGRESE ARTICULO A BUSCAR")

}


function actualizarEstadoFacturacionCita(estado) {
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarHc_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=actualizarEstadoFacturacionCita";
    valores_a_mandar = valores_a_mandar + "&usuario=" + IdSesion();
    valores_a_mandar = valores_a_mandar + "&id_cita=" + valorAtributo('lblIdCita');
    valores_a_mandar = valores_a_mandar + "&estado=" + estado;
    varajaxInit.send(valores_a_mandar);
}



function respuestaInformacionArticuloTemporal() {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('infoVAR') != null) {
                totalRegistros = raiz.getElementsByTagName('VAR1').length;

                if (totalRegistros > 0) {

                    var presentacion = raiz.getElementsByTagName('VAR1')[0].firstChild.data;
                    var atc = raiz.getElementsByTagName('VAR2')[0].firstChild.data;
                    var pos = raiz.getElementsByTagName('VAR3')[0].firstChild.data;
                    var cum = raiz.getElementsByTagName('VAR4')[0].firstChild.data;
                    var administracion = raiz.getElementsByTagName('VAR5')[0].firstChild.data;
                    var medicamento = raiz.getElementsByTagName('VAR6')[0].firstChild.data;

                    asignaAtributo('lblPresentacion', presentacion, 0)
                    asignaAtributo('txtCodAtc', atc, 1)
                    asignaAtributo('cmbPOS', pos, 0)
                    asignaAtributo('txtATC', cum, 1)
                    asignaAtributo('lblViaArticulo', administracion, 0)
                    asignaAtributo('txtNombreComercial', medicamento, 0)


                } else {
                    alert('ARTICULO NO EXISTE')
                    //                         alert("Atención: Datos Incompeltos o no se en el Información del Paciente");
                }
            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}






function buscarInformacionBasicaPacienteIdEvolucion(idEvolucion) {
    // borrarRegistrosTabla('tablaCitas');    
    if (idEvolucion != '') {
        varajaxInit = crearAjax();
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarHc_xml.jsp', true);
        varajaxInit.onreadystatechange = respuestabuscarInformacionBasicaPacienteIdEvolucion;
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        valores_a_mandar = "accion=InformacionBasicaDelPacienteIdEvolucion";
        valores_a_mandar = valores_a_mandar + "&idEvolucion=" + idEvolucion;
        varajaxInit.send(valores_a_mandar);
    } else
        alert("SELECCIONE EVOLUCION")

}

function respuestabuscarInformacionBasicaPacienteIdEvolucion() {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('infoBasicaPaciente') != null) {
                totalRegistros = raiz.getElementsByTagName('IDENTIFICACION_PACIENTE').length;

                if (totalRegistros > 0) {

                    FECHA_ELABORO = raiz.getElementsByTagName('FECHA_ELABORO')[0].firstChild.data;
                    FECHA_INGRESO = raiz.getElementsByTagName('FECHA_INGRESO')[0].firstChild.data;
                    ID_FOLIO = raiz.getElementsByTagName('ID_FOLIO')[0].firstChild.data;
                    ID_HC = raiz.getElementsByTagName('ID_HC')[0].firstChild.data;
                    NOM_ADMINISTRADORA = raiz.getElementsByTagName('NOM_ADMINISTRADORA')[0].firstChild.data;
                    IDENTIFICACION_PACIENTE = raiz.getElementsByTagName('IDENTIFICACION_PACIENTE')[0].firstChild.data;
                    ID_PROFESIONAL = raiz.getElementsByTagName('ID_PROFESIONAL')[0].firstChild.data;


                    nombre1 = raiz.getElementsByTagName('Nombre1')[0].firstChild.data;
                    nombre2 = raiz.getElementsByTagName('Nombre2')[0].firstChild.data;
                    apellido1 = raiz.getElementsByTagName('Apellido1')[0].firstChild.data;
                    apellido2 = raiz.getElementsByTagName('Apellido2')[0].firstChild.data;
                    fechaNac = raiz.getElementsByTagName('fechaNac')[0].firstChild.data;
                    edad = raiz.getElementsByTagName('edad')[0].firstChild.data;
                    sexo = raiz.getElementsByTagName('sexo')[0].firstChild.data;
                    Ocupacion = raiz.getElementsByTagName('Ocupacion')[0].firstChild.data;
                    NivelEscolar = raiz.getElementsByTagName('NivelEscolar')[0].firstChild.data;
                    Etnia = raiz.getElementsByTagName('Etnia')[0].firstChild.data;
                    nomMunicipio = raiz.getElementsByTagName('nomMunicipio')[0].firstChild.data;
                    Direccion = raiz.getElementsByTagName('Direccion')[0].firstChild.data;
                    acompanante = raiz.getElementsByTagName('acompanante')[0].firstChild.data;
                    Telefonos = raiz.getElementsByTagName('Telefonos')[0].firstChild.data;

                    asignaAtributo('lblFechaElaboraEvo', FECHA_ELABORO, 0)
                    asignaAtributo('lblFechaAdmision', FECHA_INGRESO, 0)
                    asignaAtributo('lblIdFolio', ID_FOLIO, 0)
                    asignaAtributo('lblIdentificacionPaciente', ID_HC, 0)
                    asignaAtributo('lblNomAdministradora', NOM_ADMINISTRADORA, 0)
                    asignaAtributo('lblIdentificacion', IDENTIFICACION_PACIENTE, 0)
                    asignaAtributo('lblFirmaProfesional', ID_PROFESIONAL, 0)


                    asignaAtributo('lblNombre1', nombre1, 0)
                    asignaAtributo('lblNombre2', nombre2, 0)
                    asignaAtributo('lblApellido1', apellido1, 0)
                    asignaAtributo('lblApellido2', apellido2, 0)
                    asignaAtributo('lblFechaNacimiento', fechaNac, 0)
                    asignaAtributo('lblEdad', edad, 0)
                    asignaAtributo('lblSexo', sexo, 0)

                    asignaAtributo('lblOcupacion', Ocupacion, 0)
                    asignaAtributo('lblNivelEscolar', NivelEscolar, 0)
                    asignaAtributo('lblEtnia', Etnia, 0)

                    asignaAtributo('lblMunicipio', nomMunicipio, 0)
                    asignaAtributo('lblDireccion', Direccion, 0)
                    asignaAtributo('lblAcompanante', acompanante, 0)
                    asignaAtributo('lblTelefono', Telefonos, 0)
                } else {
                    alert('PACIENTE NO EXISTE')
                    //                         alert("Atención: Datos Incompeltos o no se en el Información del Paciente");
                }
            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}

function abrirVentanaTraerCitaAlaAdmisionCuenta() {
    mostrar('divVentanitaAgendaAdmision')
    limpiarDatosPacienteAdmisionCita();
    buscarAGENDA('listAgendaAdmisionCuentas')
}

function abrirVentanaAdmisionUrgencia() {
    mostrar('divVentanitaAdmisionUrgencias')
    asignaAtributo("cmbEstadoAdmisionURG", "1", 0)
    setTimeout(() => {
        buscarAGENDA('listAgendaAdmisionCuentas')
    }, 100);
}

function abrirVentanaTraerCitaAlaAdmision() {
    mostrar('divVentanitaAgendaAdmision')
    limpiarDatosPacienteAdmisionCita();
    buscarAGENDA('listAgendaAdmision');
}


function abrirVentanaDesdeListaEspera() {
    mostrar('divVentanitaListaEspera')
    limpiarDatosPacienteListaEspera();
}


function irMesSiguienteAgenda() {

    var mes = $('#drag' + ventanaActual.num).find('#lblMes').html();
    var anio = $('#drag' + ventanaActual.num).find('#lblAnio').html();

    if (mes == 12) {
        mes = 1;
        anio++;
    } else
        mes++;

    $('#drag' + ventanaActual.num).find('#lblAnio').html(anio);
    $('#drag' + ventanaActual.num).find('#lblMes').html(mes);
    // VerificarMesEditable(); // una vez cambie el mes se evalua si se puede o no editar campos
    buscarAGENDA('listDias')
    setTimeout(() => {
        buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
    }, 300);
}

function irMesAnteriorAgenda() {

    var mes = $('#drag' + ventanaActual.num).find('#lblMes').html();
    var anio = $('#drag' + ventanaActual.num).find('#lblAnio').html();

    if (mes == 1) {
        mes = 12;
        anio--;
    } else
        mes--;

    $('#drag' + ventanaActual.num).find('#lblAnio').html(anio);
    $('#drag' + ventanaActual.num).find('#lblMes').html(mes);
    //  VerificarMesEditable(); // una vez cambie el mes se evalua si se puede o no editar campos
    buscarAGENDA('listDias')
    setTimeout(() => {
        buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
    }, 300);
}

function irMesSiguienteHorario() {

    var mes = $('#drag' + ventanaActual.num).find('#lblMes').html();
    var anio = $('#drag' + ventanaActual.num).find('#lblAnio').html();

    if (mes == 12) {
        mes = 1;
        anio++;
    } else
        mes++;

    $('#drag' + ventanaActual.num).find('#lblAnio').html(anio);
    $('#drag' + ventanaActual.num).find('#lblMes').html(mes);
    // VerificarMesEditable(); // una vez cambie el mes se evalua si se puede o no editar campos
    setTimeout(() => {
        buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
    }, 200);
}

function irMesAnteriorHorario() {

    var mes = $('#drag' + ventanaActual.num).find('#lblMes').html();
    var anio = $('#drag' + ventanaActual.num).find('#lblAnio').html();

    if (mes == 1) {
        mes = 12;
        anio--;
    } else
        mes--;

    $('#drag' + ventanaActual.num).find('#lblAnio').html(anio);
    $('#drag' + ventanaActual.num).find('#lblMes').html(mes);
    //  VerificarMesEditable(); // una vez cambie el mes se evalua si se puede o no editar campos
    setTimeout(() => {
        buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
    }, 200);
}


function limpiarComboDestino(idElemDestino) { //  alert(idElemDestino +'-'+ valorAtributo(idDependeDe) +'-'+ idQuery)
    asignaAtributo(idElemDestino, '', 0)
}

function comboDependiente(idElemDestino, idDependeDe, idQuery) { // alert(idElemDestino +'::'+ valorAtributo(idDependeDe) +'-'+ idQuery)
    cargarComboGRALCondicion1('cmbPadre', '1', idElemDestino, idQuery, valorAtributo(idDependeDe));
}

function comboDependienteDosCondiciones(idElemDestino, condicion1, condicion2, idQuery) { // alert(idElemDestino +'::'+ valorAtributo(idDependeDe) +'-'+ idQuery)
    cargarComboGRALCondicion2('cmbPadre', '1', idElemDestino, idQuery, valorAtributo(condicion1), valorAtributo(condicion2))
}

function comboDependienteAutocomplete(idElemDestino, idDependeDe, idQuery) { /*origen es un txt autocomplete */
    cargarComboGRALCondicion1('cmbPadre', '1', idElemDestino, idQuery, valorAtributoIdAutoCompletar(idDependeDe));
}

//SEDES
function comboDependienteAutocompletarSede(idElemDestino, condicion1, idQuery) { /*origen es un txt autocomplete */
    cargarComboGRALCondicion2('cmbPadre', '1', idElemDestino, idQuery, valorAtributoIdAutoCompletar(condicion1), IdSede());
}
//***************** */

function comboDependienteAutocompleteEmpresa2(idElemDestino, condicion1, idQuery) { /*origen es un txt autocomplete */
    cargarComboGRALCondicion2('cmbPadre', '1', idElemDestino, idQuery, valorAtributoIdAutoCompletar(condicion1), IdEmpresa());
}

function comboDependienteEmpresa(idElemDestino, idQuery) {
    cargarComboGRALCondicion1('cmbPadre', '1', idElemDestino, idQuery, IdEmpresa());
}
function comboDependienteSede(idElemDestino, idQuery) {
    cargarComboGRALCondicion2('cmbPadre', '1', idElemDestino, idQuery, IdEmpresa(), IdSede());
}

function comboDependienteSedeYServicios(idElemDestino, idQuery) {
    cargarComboGRALCondicion2('cmbPadre', '1', idElemDestino, idQuery, IdSede(), valorAtributo('cmbIdTipoServicio'));
}




function cargarUnLabel(idElemDestino, nomElemDestino, idDependeDe1, idDependeDe2, idQuery) {
    cargarLabelCondicion2(idElemDestino, nomElemDestino, valorAtributoIdAutoCompletar(idDependeDe1), valorAtributo(idDependeDe2), idQuery)
}

function cargarUnLabelPlanDeContratacionAdmisiones(idElemDestino, nomElemDestino, idDependeDe1, idDependeDe2, idQuery) {
    var a = valorAtributo(idDependeDe2).split('-');
    var regimen = a[0];
    var plan = a[1];
    cargarLabelPlanContratacionAdmisiones(idElemDestino, nomElemDestino, valorAtributoIdAutoCompletar(idDependeDe1), regimen, plan, idQuery)
}


function cargarUnLabelPlanDeContratacionEmpresa(idElemDestino, nomElemDestino, idDependeDe1, idDependeDe2, idQuery) {
    var a = valorAtributo(idDependeDe2).split('-');
    var regimen = a[0];
    var plan = a[1];
    cargarLabelPlanContratacionEmpresa(idElemDestino, nomElemDestino, valorAtributoIdAutoCompletar(idDependeDe1), regimen, plan, IdEmpresa(), idQuery)
}


function cargarUnLabelPlanDeContratacionListaEspera(idElemDestino, nomElemDestino, idDependeDe1, idDependeDe2, idQuery) {

    var a = valorAtributo(idDependeDe2).split('-');
    var regimen = a[0];
    var plan = a[1];
    cargarLabelPlanContratacionListaEspera(idElemDestino, nomElemDestino, valorAtributoIdAutoCompletar(idDependeDe1), regimen, plan, idQuery)
}

function cargarUnLabelPlanDeContratacionAgenda(idElemDestino, nomElemDestino, idDependeDe1, idDependeDe2, idQuery) {

    var a = valorAtributo(idDependeDe2).split('-');
    var regimen = a[0];
    var plan = a[1];
    cargarLabelPlanContratacionAgenda(idElemDestino, nomElemDestino, valorAtributoIdAutoCompletar(idDependeDe1), regimen, plan, idQuery)
}


//function cargarTipoCitaSegunEspecialidad(idElemDestino, idDependeDe) {
/*if($("#cmbIdSubEspecialidad").val()=='29'){
 cargarComboGRALCondicion1('cmbPadre','1',idElemDestino,142,valorAtributo(idDependeDe));            
 }else{*/
//cargarComboGRALCondicion1('cmbPadre','1',idElemDestino,9,valorAtributo(idDependeDe));     
/* CAMBIAR POR COMBO CONDICION 2 */
//cargarComboGRALCondicion2('cmbPadre', '1', idElemDestino, 551, valorAtributo(idDependeDe), valorAtributo('cmbIdEspecialidad'))
//} 

//}



//function cargarTipoCitaSegunEspecialidad( ) {
//  cargarComboGRALCondicion1('', '', 'cmbTipoCita', 551, valorAtributo('cmbIdEspecialidad'));
//}


function cargarAntecendetesFolio(idElemento) {
    cargarComboGRALCondicion1('cmbPadre', '1', idElemento, 121, valorAtributo('lblTipoDocumento'));
}

function cargarTipoCitaSegunEspecialidad(idElemento) {
    cargarComboGRALCondicion1('cmbPadre', '1', idElemento, 551, valorAtributo('cmbIdEspecialidad'));
}

function cargarCitaSegunEspecialidad(idElemento, idEspecialidad) {
    cargarComboGRALCondicion1('cmbPadre', '1', idElemento, 551, valorAtributo(idEspecialidad));
}

function cargarProfesionalesDesdeEspecialidadTraerLE(idElemento) {
    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
        cargarComboGRALCondicion1('cmbPadre', '1', 'cmbIdProfesionalesLETraer', 101, valorAtributo('cmbIdEspecialidad'));
    } else {
        cargarComboGRALCondicion1('cmbPadre', '1', 'cmbIdProfesionalesLETraer', 101, valorAtributo('cmbIdEspecialidad'));
    }
}

function cargarProfesionalesProgramacion(idElemento) {
    cargarComboGRALCondicion2('cmbPadre', 'deptoDefecto', idElemento, 522, valorAtributo('cmbIdEspecialidad'), valorAtributo('lblFechaSeleccionada'))
}

function cargarProfesionalesDesdeEspecialidadSede(idElemento) {
    cargarComboGRALCondicion2('cmbPadre', '1', idElemento, 197, valorAtributo('cmbIdEspecialidad'), valorAtributo('cmbSede'));
}

function cargarProfesionalesDesdeEspecialidad(idElemento) {
    cargarComboGRALCondicion1('cmbPadre', '1', idElemento, 101, valorAtributo('cmbIdEspecialidad'));
}

function cargarProfesionalDesdeEspecialidad(idElemento) {
    cargarComboGRALCondicion1('cmbPadre', '1', idElemento, 101, valorAtributo('cmbIdEspecialidad'));
}

function cargarProfesionalesDesdeEspecialidadDocumentos(idElemento) {
    // asignaAtributo('cmbIdSubEspecialidad', '', 0)        
    cargarComboGRALCondicion1('cmbPadre', '1', 'cmbIdProfesionales', 101, valorAtributo('cmbIdEspecialidadBus'));
}

function cargarProfesionalDesdeEspecialidad(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 101, valorAtributo(comboOrigen));
}

function cargarAreaDesdeSede(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 1249, valorAtributo(comboOrigen));
}

function cargarEspecialidadDesdeSedeCirugia(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 850, valorAtributo(comboOrigen));
}

function cargarEspecialidadDesdeSede(comboOrigen, comboDestino) { //alert(6666)
    cargarComboGRALCondicion2('cmbPadre', '1', comboDestino, 849, IdEmpresa(), valorAtributo(comboOrigen));

    // cargarComboGRALCondicion2('cmbPadre', '1', idElemDestino, idQuery, IdEmpresa(), IdSede());


}

function cargarServicioDesdeSede(comboOrigen, comboDestino) { //alert(6666)
    cargarComboGRALCondicion2('cmbPadre', '1', comboDestino, 1266, IdEmpresa(), valorAtributo(comboOrigen));

}

function cargarSubEspecialidadDesdeEspecialidad(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 111, valorAtributo(comboOrigen));
}

function cargarAreaDesdeServicio(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 976, valorAtributo(comboOrigen));
}

function cargarDepDesdeServicio(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 168, valorAtributo(comboOrigen));
}

function cargarMunicipioDesdeDepartamento(comboOrigen1, comboOrigen2, comboDestino) {
    cargarComboGRALCondicion2('cmbPadre', '1', comboDestino, 169, valorAtributo(comboOrigen1), valorAtributo(comboOrigen2));
}

function cargarLocalidad(comboOrigen1, comboOrigen2, comboOrigen3, comboDestino) {
    cargarComboGRALCondicion3('cmbPadre', '1', comboDestino, 170, valorAtributo(comboOrigen1), valorAtributo(comboOrigen2), valorAtributo(comboOrigen3));
}

function cargarHabitacionDesdeArea(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 546, valorAtributo(comboOrigen));
}

function cargarCamaDesdeHabitacion(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 547, valorAtributo(comboOrigen));
}


function cargarEspecialidad(comboDestino) {
    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
        cargarComboGRAL('cmbPadre', '1', comboDestino, 125)
    }

}

function cargarFuncionarioTerceroVentas(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 102, valorAtributo(comboOrigen));
}

function buscarAGENDA(arg) {

    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';

    switch (arg) {
        case 'citasAdmisionHC':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=813&parametros=";

            add_valores_a_mandar(valorAtributo("lblIdAdmisionAgen"));

            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: "xml",
                mtype: "GET",
                colNames: [
                    "C", "ID_CITA", "PROFESIONAL", "ESPECIALIDAD", "TIPO CITA", "ESTADO", "FECHA"
                ],
                colModel: [
                    { name: "c", index: "contador", width: 1 },
                    { name: "ID_CITA", index: "ID_CITA", width: 3 },
                    { name: "PROFESIONAL", index: "PROFESIONAL", width: 8 },
                    { name: "ESPECIALIDAD", index: "ESPECIALIDAD", width: 8 },
                    { name: "TIPO_CITA", index: "TIPO CITA", width: 8 },
                    { name: "ESTADO", index: "ESTADO", width: 4 },
                    { name: "FECHA", index: "FECHA", width: 8 },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);
                },
                height: 200,
                width: 600
            });
            $("#" + arg).setGridParam({ url: valores_a_mandar }).trigger("reloadGrid");
            break;

        case 'citasDisponiblesAsociarHC':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=812&parametros=";

            add_valores_a_mandar(valorAtributo("lblIdAdmisionAgen"));
            add_valores_a_mandar(valorAtributo("lblIdAdmisionAgen"));
            add_valores_a_mandar(valorAtributo("lblIdAdmisionAgen"));
            add_valores_a_mandar("");
            add_valores_a_mandar("");

            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: "xml",
                mtype: "GET",
                colNames: [
                    "C", "ID_CITA", "PROFESIONAL", "ESPECIALIDAD", "TIPO CITA", "ESTADO", "FECHA"
                ],
                colModel: [
                    { name: "c", index: "contador", width: 1 },
                    { name: "ID_CITA", index: "ID_CITA", width: 3 },
                    { name: "PROFESIONAL", index: "PROFESIONAL", width: 8 },
                    { name: "ESPECIALIDAD", index: "ESPECIALIDAD", width: 8 },
                    { name: "TIPO_CITA", index: "TIPO CITA", width: 8 },
                    { name: "ESTADO", index: "ESTADO", width: 4 },
                    { name: "FECHA", index: "FECHA", width: 8 },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);
                },
                height: 200,
                width: 600,
                multiselect: true
            });
            $("#" + arg).setGridParam({ url: valores_a_mandar }).trigger("reloadGrid");
            break;

        case 'citasAdmision':
            fila_seleccionada = $("#listAdmisionCuenta").jqGrid('getGridParam', 'selrow')

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=813&parametros=";

            add_valores_a_mandar($("#listAdmisionCuenta").getRowData(fila_seleccionada).ID);

            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: "xml",
                mtype: "GET",
                colNames: [
                    "C", "ID_CITA", "PROFESIONAL", "ESPECIALIDAD", "TIPO CITA", "ESTADO", "FECHA"
                ],
                colModel: [
                    { name: "c", index: "contador", width: 1 },
                    { name: "ID_CITA", index: "ID_CITA", width: 3 },
                    { name: "PROFESIONAL", index: "PROFESIONAL", width: 8 },
                    { name: "ESPECIALIDAD", index: "ESPECIALIDAD", width: 8 },
                    { name: "TIPO_CITA", index: "TIPO CITA", width: 8 },
                    { name: "ESTADO", index: "ESTADO", width: 4 },
                    { name: "FECHA", index: "FECHA", width: 8 },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);
                },
                height: 200,
                width: 600
            });
            $("#" + arg).setGridParam({ url: valores_a_mandar }).trigger("reloadGrid");
            break;

        case 'citasDisponiblesAsociar':
            fila_seleccionada = $("#listAdmisionCuenta").jqGrid('getGridParam', 'selrow')

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=812&parametros=";

            add_valores_a_mandar($("#listAdmisionCuenta").getRowData(fila_seleccionada).ID);
            add_valores_a_mandar($("#listAdmisionCuenta").getRowData(fila_seleccionada).ID);
            add_valores_a_mandar($("#listAdmisionCuenta").getRowData(fila_seleccionada).ID);
            add_valores_a_mandar($("#listAdmisionCuenta").getRowData(fila_seleccionada).ID_FACTURA);
            add_valores_a_mandar($("#listAdmisionCuenta").getRowData(fila_seleccionada).ID_FACTURA);


            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: "xml",
                mtype: "GET",
                colNames: [
                    "C", "ID_CITA", "PROFESIONAL", "ESPECIALIDAD", "TIPO CITA", "ESTADO", "FECHA"
                ],
                colModel: [
                    { name: "c", index: "contador", width: 1 },
                    { name: "ID_CITA", index: "ID_CITA", width: 3 },
                    { name: "PROFESIONAL", index: "PROFESIONAL", width: 8 },
                    { name: "ESPECIALIDAD", index: "ESPECIALIDAD", width: 8 },
                    { name: "TIPO_CITA", index: "TIPO CITA", width: 8 },
                    { name: "ESTADO", index: "ESTADO", width: 4 },
                    { name: "FECHA", index: "FECHA", width: 8 },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);
                },
                height: 200,
                width: 600,
                multiselect: true
            });
            $("#" + arg).setGridParam({ url: valores_a_mandar }).trigger("reloadGrid");
            break;

        case 'listCoordinacionTerapia':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=507&parametros=";

            add_valores_a_mandar(valorAtributo("txtDocFechaDesde"));
            add_valores_a_mandar(valorAtributo("txtDocFechaHasta"));
            add_valores_a_mandar(valorAtributo("cmbIdSedeBus"));
            add_valores_a_mandar(valorAtributo("cmbIdSedeBus"));
            add_valores_a_mandar(valorAtributo("cmbIdEspecialidadBus"));
            add_valores_a_mandar(valorAtributo("cmbIdEspecialidadBus"));
            add_valores_a_mandar(valorAtributo("cmbIdProfesionales"));
            add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdBusPaciente"));
            add_valores_a_mandar(valorAtributo("cmbIdDepartamento"));
            add_valores_a_mandar(valorAtributo("cmbIdMunicipio"));
            add_valores_a_mandar(valorAtributo("cmbIdLocalidad"));
            add_valores_a_mandar(valorAtributo("cmbProgramacion"));
            add_valores_a_mandar(valorAtributo("cmbProgramacion"));
            add_valores_a_mandar(valorAtributo("cmbProgramacion"));

            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: "xml",
                mtype: "GET",
                colNames: [
                    "C", "IDADMISION", "FECHA INGRESO", "IDPACIENTE", "IDENTIFICACION", "NOMBRE", "IDSEDE", "SEDE", "IDESPECIALIDAD", "ESPECIALIDAD",
                    "IDPROFESIONAL", "PROFESIONAL", "ID_EVO", "TIPO", "NOMBRE DOCUMENTO", "IDESTADODOC", "ESTADO", "ADMINISTRADORA", "ORDENADOS", "PROGRAMADOS", "ID_PLAN", "PLAN"
                ],
                colModel: [
                    { name: "c", index: "contador", hidden: true },
                    { name: "idAdmision", index: "idAdmision", hidden: true },
                    { name: "fecha_Ingreso", index: "fecha_Ingreso", width: 8 },
                    { name: "idPaciente", index: "idPaciente", hidden: true },
                    { name: "identificacion", index: "identificacion", hidden: true },
                    { name: "Nombre", index: "Nombre", width: 15 },
                    { name: "idSede", index: "idSede", hidden: true },
                    { name: "Sede", index: "Sede", width: 10 },
                    { name: "idEspecialidad", index: "idEspecialidad", hidden: true },
                    { name: "Especialidad", index: "Especialidad", width: 10 },
                    { name: "idProfesional", index: "idProfesional", hidden: true },
                    { name: "Profesional", index: "Profesional", width: 15 },
                    { name: "id_evo", index: "id_evo", hidden: true },
                    { name: "tipo", index: "tipo", hidden: true },
                    { name: "NombreDocumento", index: "NombreDocumento", width: 18 },
                    { name: "idEstadoDoc", index: "idEstadoDoc", hidden: true },
                    { name: "Estado", index: "Estado", hidden: true },
                    { name: "administradora", index: "administradora", hidden: true },
                    { name: "ORDENADOS", index: "ORDENADOS", width: 5 },
                    { name: "PROGRAMADOS", index: "PROGRAMADOS", width: 5 },
                    { name: "id_plan", index: "id_plan", hidden: true },
                    { name: "plan", index: "plan", hidden: true },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);

                    asignaAtributo("lblIdPaciente", datosRow.idPaciente, 1);
                    asignaAtributo("lblIdentificacion", datosRow.identificacion, 1);
                    asignaAtributo("lblNombrePaciente", datosRow.Nombre, 1);
                    asignaAtributo("lblIdDocumento", datosRow.id_evo, 1);
                    asignaAtributo("lblTipoDocumento", datosRow.tipo, 1);
                    asignaAtributo("lblIdAdmision", datosRow.idAdmision, 1);
                    asignaAtributo("lblDescripcionTipoDocumento", datosRow.NombreDocumento, 1);
                    asignaAtributo("lblEspecialidad", datosRow.Especialidad, 1);
                    asignaAtributo("lblIdPlan", datosRow.id_plan, 1);
                    asignaAtributo("lblPlan", datosRow.plan, 1);

                    buscarAGENDA('listCoordinacionTerapiaOrdenados');
                    tabActivo = "divProcedimientosOrdenados";
                    setTimeout(() => {
                        buscarAGENDA('listCoordinacionTerapiaHistoricos');
                    }, 500);
                },
                height: 400,
                width: 1270
            });
            $("#" + arg).setGridParam({ url: valores_a_mandar }).trigger("reloadGrid");
            break;


        case "listCoordinacionTerapiaOrdenados":
            limpiarListadosTotales('listaProgramacionTerapias');

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=516&parametros=";

            add_valores_a_mandar(valorAtributo("lblIdDocumento"));

            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: "xml",
                mtype: "GET",
                colNames: [
                    "C", "ID", "ID_PROCEDIMIENTO", "CUPS", "POS", "ID_CLASE", "CLASE", "NOMBRE", "INDICACION",
                    "ID_ESTADO", "NECESIDAD", "DIAGNOSTICO", "CANTIDAD", "GESTIONADO", "ESTADO"
                ],
                colModel: [
                    { name: "C", index: "C", hidden: true },
                    { name: "ID", index: "ID", hidden: true },
                    { name: "ID_PROCEDIMIENTO", index: "ID_PROCEDIMIENTO", hidden: true },
                    { name: "CUPS", index: "CUPS", width: 5 },
                    { name: "POS", index: "POS", width: 5 },
                    { name: "ID_CLASE", index: "ID_CLASE", hidden: true },
                    { name: "CLASE", index: "CLASE", width: 8 },
                    { name: "NOMBRE", index: "NOMBRE", width: 40 },
                    { name: "INDICACION", index: "INDICACION", width: 15 },
                    { name: "ID_ESTADO", index: "ID_ESTADO", hidden: true },
                    { name: "NECESIDAD", index: "NECESIDAD", width: 10 },
                    { name: "DIAGNOSTICO", index: "DIAGNOSTICO", hidden: true },
                    { name: "CANTIDAD", index: "CANTIDAD", width: 5 },
                    { name: "GESTIONADO", index: "GESTIONADO", width: 6 },
                    { name: "ESTADO", index: "ESTADO", width: 11 },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = $('#' + arg).getRowData(rowid);


                    asignaAtributo("lblIdProcedimientoAutoriza", datosRow.ID, 0);
                    asignaAtributo("lblCantidadLab", datosRow.CANTIDAD, 0);
                    asignaAtributo("lblDiagnosticoLab", datosRow.DIAGNOSTICO, 0);
                    asignaAtributo("lblIdProcedimientoLE", datosRow.ID_PROCEDIMIENTO, 0);

                    buscarAGENDA("listaProgramacionTerapias");
                },
                height: 200,
                width: 1220
            });
            $("#" + arg).setGridParam({ url: valores_a_mandar }).trigger("reloadGrid");
            break;

        case "listCoordinacionTerapiaOrdenadosAgenda":
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=819&parametros=";

            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPacienteOrdenes'));
            add_valores_a_mandar(valorAtributo("cmbSede"));

            $('#drag' + ventanaActual.num).find("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid({
                url: valores_a_mandar,
                datatype: "xml",
                mtype: "GET",
                colNames: [
                    "C", "ID", "ID_PROCEDIMIENTO", "CUPS", "POS", "ID_CLASE", "CLASE", "NOMBRE", "INDICACION",
                    "ID_ESTADO", "NECESIDAD", "DIAGNOSTICO", "CANTIDAD", "GESTIONADO", "ESTADO"
                ],
                colModel: [
                    { name: "C", index: "C", hidden: true },
                    { name: "ID", index: "ID", hidden: true },
                    { name: "ID_PROCEDIMIENTO", index: "ID_PROCEDIMIENTO", hidden: true },
                    { name: "CUPS", index: "CUPS", width: 5 },
                    { name: "POS", index: "POS", width: 5 },
                    { name: "ID_CLASE", index: "ID_CLASE", hidden: true },
                    { name: "CLASE", index: "CLASE", width: 8 },
                    { name: "NOMBRE", index: "NOMBRE", width: 40 },
                    { name: "INDICACION", index: "INDICACION", width: 15 },
                    { name: "ID_ESTADO", index: "ID_ESTADO", hidden: true },
                    { name: "NECESIDAD", index: "NECESIDAD", width: 10 },
                    { name: "DIAGNOSTICO", index: "DIAGNOSTICO", hidden: true },
                    { name: "CANTIDAD", index: "CANTIDAD", width: 5 },
                    { name: "GESTIONADO", index: "GESTIONADO", width: 6 },
                    { name: "ESTADO", index: "ESTADO", width: 11 },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listCoordinacionTerapiaOrdenadosAgenda').getRowData(rowid);
                    asignaAtributo("lblIdProcedimientoAutoriza", datosRow.ID, 0);
                    asignaAtributo("lblCantidadLab", datosRow.CANTIDAD, 0);
                    asignaAtributo("lblDiagnosticoLab", datosRow.DIAGNOSTICO, 0);
                    asignaAtributo("lblIdProcedimientoLE", datosRow.ID_PROCEDIMIENTO, 0);

                    buscarAGENDA("listaProgramacionTerapiasAgenda");
                },
                height: 100,
                width: 1010,
                caption: "PROCEDIMIENTOS ORDENADOS",
            });
            $('#drag' + ventanaActual.num).find("#listCoordinacionTerapiaOrdenadosAgenda").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case "listCoordinacionTerapiaHistoricos":
            limpiarDivEditarJuan(arg);

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=615&parametros=";

            add_valores_a_mandar(valorAtributo("lblIdPaciente"));
            add_valores_a_mandar(valorAtributo("lblIdDocumento"));

            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: "xml",
                mtype: "GET",
                colNames: [
                    "C", "FECHA", "FOLIO", "ID", "ID_PROCEDIMIENTO", "CUPS", "POS", "ID_CLASE", "CLASE", "NOMBRE", "CANTIDAD", "INDICACION",
                    "ID_ESTADO", "ESTADO", "NECESIDAD", "DIAGNOSTICO",
                ],
                colModel: [
                    { name: "C", index: "C", hidden: true },
                    { name: "FECHA", index: "FECHA", width: 6 },
                    { name: "FOLIO", index: "FOLIO", width: 10 },
                    { name: "ID", index: "ID", hidden: true },
                    { name: "ID_PROCEDIMIENTO", index: "ID_PROCEDIMIENTO", hidden: true },
                    { name: "CUPS", index: "CUPS", width: 5 },
                    { name: "POS", index: "POS", width: 5 },
                    { name: "ID_CLASE", index: "ID_CLASE", hidden: true },
                    { name: "CLASE", index: "CLASE", width: 10 },
                    { name: "NOMBRE", index: "NOMBRE", width: 40 },
                    { name: "CANTIDAD", index: "CANTIDAD", width: 5 },
                    { name: "INDICACION", index: "INDICACION", width: 15 },
                    { name: "ID_ESTADO", index: "ID_ESTADO", hidden: true },
                    { name: "ESTADO", index: "ESTADO", width: 10 },
                    { name: "NECESIDAD", index: "NECESIDAD", width: 10 },
                    { name: "DIAGNOSTICO", index: "DIAGNOSTICO", hidden: true },
                ],
                onSelectRow: function (rowid) {
                    asignaAtributo("lblIdOrden", datosRow.ID, 0);
                },
                subGridOptions: {
                    plusicon: "ui-icon-triangle-1-e",
                    minusicon: "ui-icon-triangle-1-s",
                    openicon: "ui-icon-arrowreturn-1-e",
                },
                subGridRowExpanded: function (subgrid_id, row_id) {
                    var subgrid_table_id, pager_id;
                    subgrid_table_id = subgrid_id + "_t";
                    // pager_id = "p_"+subgrid_table_id;
                    datosRow = $("#listCoordinacionTerapiaHistoricos").getRowData(row_id);

                    valores_a_mandar = pag;
                    valores_a_mandar = valores_a_mandar + "?idQuery=272&parametros=";
                    //add_valores_a_mandar(valorAtributo('lblIdOrden'));
                    add_valores_a_mandar(datosRow.ID);

                    $("#" + subgrid_id).html(
                        "<table id='" + subgrid_table_id + "' class='scroll'></table>"
                    );
                    jQuery("#" + subgrid_table_id).jqGrid({
                        url: valores_a_mandar,
                        datatype: 'xml',
                        mtype: 'GET',
                        colNames: ['C', 'ID', 'CANTIDAD', 'ID_ESTADO', 'ESTADO', 'ID_SEDE',
                            'SEDE', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'ID_TIPO_CITA', 'TIPO_CITA',
                            'ID_MEDICO', 'MEDICO', 'FECHA_PROGRAMACION'],
                        colModel: [
                            { name: 'contador', index: 'contador', width: 5 },
                            { name: 'ID', index: 'ID', hidden: true },
                            { name: 'CANTIDAD', index: 'CANTIDAD', width: 10 },
                            { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                            { name: 'ESTADO', index: 'ESTADO', width: 20 },
                            { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                            { name: 'SEDE', index: 'SEDE', width: 20 },
                            { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                            { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', width: 30 },
                            { name: 'ID_TIPO_CITA', index: 'ID_TIPO_CITA', hidden: true },
                            { name: 'TIPO_CITA', index: 'TIPO_CITA', width: 30 },
                            { name: 'ID_MEDICO', index: 'ID_MEDICO', hidden: true },
                            { name: 'MEDICO', index: 'MEDICO', width: 30 },
                            { name: 'FECHA_PROGRAMACION', index: 'FECHA_PROGRAMACION', width: 20 },
                        ],

                        onSelectRow: function (rowid) {
                            var datosRow = $('#' + subgrid_table_id).getRowData(rowid);

                            asignaAtributoCombo2('cmbSedeTerapia', datosRow.ID_SEDE, datosRow.SEDE);
                            asignaAtributoCombo2('cmbIdEspecialidadTerapia', datosRow.ID_ESPECIALIDAD, datosRow.ESPECIALIDAD);
                            asignaAtributoCombo2('cmbTipoCitaTerapia', datosRow.ID_TIPO_CITA, datosRow.TIPO_CITA);
                            asignaAtributoCombo2('cmbProfesionalesTerapia', datosRow.ID_MEDICO, datosRow.MEDICO);


                        },
                        height: "auto",
                        autowidth: true,
                    });
                    jQuery("#" + subgrid_table_id).jqGrid("navGrid", "#" + pager_id, {
                        edit: false,
                        add: false,
                        del: false,
                    });
                },
                subGridRowColapsed: function (subgrid_id, row_id) {
                    // this function is called before removing the data
                    //var subgrid_table_id;
                    //subgrid_table_id = subgrid_id+"_t";
                    //jQuery("#"+subgrid_table_id).remove();
                },


                height: 200,
                width: 1230,
                //shrinkToFit: true,
                subGrid: true
            });
            $("#" + arg).setGridParam({ url: valores_a_mandar }).trigger("reloadGrid");
            break;


        case 'listaTerapiasCita':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=276&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPacienteTerapia'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPacienteTerapia'));
            add_valores_a_mandar(IdSesion());
            add_valores_a_mandar(valorAtributo('cmbConCita'));
            add_valores_a_mandar(valorAtributo('cmbConCita'));
            add_valores_a_mandar(valorAtributo('txtFechaInicio'));
            add_valores_a_mandar(valorAtributo('txtFechaFin'));
            add_valores_a_mandar(valorAtributo('cmbIdDepartamento'));
            add_valores_a_mandar(valorAtributo('cmbIdMunicipio'));
            add_valores_a_mandar(valorAtributo('cmbIdLocalidad'));
            add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
            add_valores_a_mandar(IdSede());

            var estados = 'http://190.60.242.160:8001/combo/107';
            var motivos = ":Escoja un estado";

            var editOptions = {
                editData: {
                    LoginSesion: LoginSesion(),
                    IdSesion: IdSesion(),

                },
                width: 400,
                recreateForm: true,
                closeAfterEdit: true,
                viewPagerButtons: false,
                afterShowForm: function (formid) {

                    var g = $("#" + arg);
                    var myInfo = '<div class="ui-state-highlight ui-corner-all">' +
                        '<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>' +
                        '<strong>Atenci&oacute;n:</strong><br/>';

                    if (valorAtributo('txtEstadoCita') === 'R') {
                        myInfo += 'La cita reprogramada no se puede modificar.</div>';
                    } else {
                        myInfo += 'Si reprograma una cita, esta se clonara.</div>';
                    }

                    var infoTR = $("table#TblGrid_" + g[0].id + ">tbody>tr.tinfo"),
                        infoTD = infoTR.children("td.topinfo");
                    infoTD.html(myInfo);
                    infoTR.show();


                    var id = $('#' + arg).jqGrid('getGridParam', 'selrow');
                    datosRow = $('#' + arg).getRowData(id);

                    var e = datosRow.ID_ESTADO_CITA == '' ? 'A' : datosRow.ID_ESTADO_CITA;

                    motivos = 'accionesXml/obtenerCombos.jsp?idQueryCombo=96&c1=' + e;
                    $.get(motivos, function (data) {
                        $('#MOTIVO_CONSULTA').html($(data).html());
                        $('#MOTIVO_CONSULTA').val(datosRow.ID_MOTIVO_CONSULTA);

                    });

                    $.get(estados, function (data) {
                        $('#ESTADO_CITA').html($(data).html());
                        $('#ESTADO_CITA').val(datosRow.ID_ESTADO_CITA);

                    });



                }, onclickSubmit: function (params) {
                    var id = jQuery("#" + arg).jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#" + arg).jqGrid('getRowData', id);
                    var a = ret.ID_AGENDA_DETALLE === '' ? 'crearTerapiaCita' : 'modificarTerapiaCita';
                    var q = ret.ID_AGENDA_DETALLE === '' ? '277' : '278';
                    return {
                        txtIdPaciente: ret.ID_PACIENTE,
                        txtTipoCita: ret.ID_TIPO_CITA,
                        txtIdListaEspera: ret.ID_LISTA_ESPERA,
                        txtIdEspecialidad: ret.ID_ESPECIALIDAD,
                        txtIdCita: ret.ID_AGENDA_DETALLE,
                        IdSede: ret.ID_SEDE,
                        accion: a,
                        idQuery: q
                    }
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                width: 700
            };

            var viewOptions = {
                width: 800
            };

            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'ID', 'ID_PACIENTE', 'IDENTIFICACION', 'NOMBRE', 'TELEFONO', 'DIRECCION', 'MUNICIPIO', 'BARRIO', 'ID_PROCEDIMIENTO', 'PROCEDIMIENTO', 'ID_SEDE', 'SEDE',
                    'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'ID_TIPO_CITA', 'TIPO CITA', 'CANTIDAD', 'FECHA PROGRAMACION',
                    'ESTADO', 'FECHA CITA', 'HORA', 'ID_ESTADO_CITA', 'ESTADO CITA', 'ID_MOTIVO_CONSULTA', 'MEDIO CONTACTO',
                    'OBSERVACIONES', 'ID_LISTA_ESPERA', 'ID CITA', 'RANGO_VENCIMIENTO', 'RANGO_MAXIMO'],
                colModel: [
                    { name: 'C', index: 'C', hidden: true },
                    { name: 'ID_PROGRAMACION', index: 'ID_PROGRAMACION', width: 2 },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: 15 },
                    { name: 'TELEFONO', index: 'TELEFONO', hidden: true, 'editrules': { edithidden: true } },
                    { name: 'DIRECCION', index: 'DIRECCION', hidden: true, 'editrules': { edithidden: true } },
                    { name: 'MUNICIPIO', index: 'MUNICIPIO', hidden: true, 'editrules': { edithidden: true } },
                    { name: 'BARRIO', index: 'BARRIO', hidden: true, 'editrules': { edithidden: true } },
                    { name: 'ID_PROCEDIMIENTO', index: 'ID_PROCEDIMIENTO', hidden: true },
                    { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', hidden: true },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', hidden: true, 'editrules': { edithidden: true } },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', hidden: true, 'editrules': { edithidden: true } },
                    { name: 'ID_TIPO_CITA', index: 'ID_TIPO_CITA', hidden: true },
                    { name: 'TIPO_CITA', index: 'TIPO_CITA', width: 20 },
                    { name: 'CANTIDAD', index: 'CANTIDAD', hidden: true },
                    { name: 'FECHA_PROGRAMACION', index: 'FECHA_PROGRAMACION', width: 5 },
                    { name: 'ESTADO', index: 'ESTADO', width: 6 },
                    {
                        name: 'FECHA_CITA', index: 'FECHA_CITA', width: 5, editable: true, editrules: { required: true, custom: true, custom_func: validarFecha },
                        editoptions: {
                            dataInit: function (elem) {
                                $(elem).datepicker();
                            }
                        }
                    },
                    {
                        name: 'HORA', index: 'HORA', width: 4, editable: true, editrules: { required: true }
                        , editoptions: {
                            dataInit: function (elem) {
                                $(elem).timepicker({
                                    showPeriod: true,
                                    showLeadingZero: true
                                });
                            }
                        },
                    },
                    { name: 'ID_ESTADO_CITA', index: 'ID_ESTADO_CITA', hidden: true },
                    {
                        name: 'ESTADO_CITA', index: 'ESTADO_CITA', width: 6, editable: true, edittype: "select", editrules: { required: true },
                        editoptions: {
                            dataUrl: estados,
                            dataEvents: [{
                                type: "change",
                                fn: function (elem) {
                                    var pl = $(elem.target).val();
                                    motivos = 'accionesXml/obtenerCombos.jsp?idQueryCombo=96&c1=' + pl;
                                    $.get(motivos, function (data) { $('#MOTIVO_CONSULTA').html($(data).html()); });
                                }
                            }],
                            dataInit: function (elem) { $(elem).width(200); }
                        }
                    },
                    { name: 'ID_MOTIVO_CONSULTA', index: 'ID_MOTIVO_CONSULTA', hidden: true },
                    {
                        name: 'MOTIVO_CONSULTA', index: 'MOTIVO', hidden: true, editable: true, edittype: 'select',
                        editrules: { edithidden: true, required: true }, editoptions: { value: motivos, dataInit: function (elem) { $(elem).width(200); } }
                    },
                    { name: 'OBSERVACIONES', index: 'OBSERVACIONES', hidden: true, editable: true, edittype: "textarea", editrules: { edithidden: true }, editoptions: { cols: 50, rows: 20 } },
                    { name: 'ID_LISTA_ESPERA', index: 'ID_LISTA_ESPERA', hidden: true },
                    { name: 'ID_AGENDA_DETALLE', index: 'ID_AGENDA_DETALLE', width: 3 },
                    { name: 'RANGO_VENCIMIENTO', index: 'RANGO_VENCIMIENTO', hidden: true },
                    { name: 'RANGO_MAXIMO', index: 'RANGO_MAXIMO', hidden: true },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);
                    asignaAtributo('txtRangoVencimiento', datosRow.RANGO_VENCIMIENTO, 0);
                    asignaAtributo('txtRangoMaximo', datosRow.RANGO_MAXIMO, 0);
                    asignaAtributo('txtEstado', datosRow.ESTADO, 0);
                    asignaAtributo('txtEstadoCita', datosRow.ID_ESTADO_CITA, 0);




                    /* asignaAtributo('txtIdPaciente', datosRow.ID_PACIENTE, 0);
                    asignaAtributo('txtTipoCita', datosRow.ID_TIPO, 0);
                    asignaAtributo('txtIdListaEspera', datosRow.ID_LISTA_ESPERA, 0);
                    asignaAtributo('txtIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0);

                   
                    
                   
                   */

                },
                //autowidth: true,
                width: 1245,
                height: 520,
                pager: '#pager1',
                caption: "AGENDA",
                editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
                //sortable: true,
                viewrecords: true,
                grouping: true,
                //toppager: true,
                groupingView: {
                    groupField: ['ESTADO'],
                    groupText: ['<b>{0} - {1} Terapia(s)</b>']
                }

            });
            $("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid')
                .navGrid("#pager1", {
                    del: false, add: false, edit: true, view: true, refresh: true, search: false,
                    edittext: "Editar", viewtext: "Ver", refreshtext: "Refrescar"
                }, editOptions, {}, {}, {}, viewOptions);



            break;

        case 'listaProgramacionTerapiasAgenda':
            terapia_seleccionada = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow');

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=821&parametros=";
            add_valores_a_mandar($("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(terapia_seleccionada).ID);

            $('#' + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'ID', 'CANTIDAD', 'ID_ESTADO', 'ESTADO', 'ID_SEDE',
                    'SEDE', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'ID_TIPO_CITA', 'TIPO CITA', 'FECHA', 'FECHA2', 'HORA'],
                colModel: [
                    { name: 'contador', index: 'contador', width: 2 },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'CANTIDAD', index: 'CANTIDAD', hidden: true },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', hidden: true },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', hidden: true },
                    { name: 'ID_TIPO_CITA', index: 'ID_TIPO_CITA', hidden: true },
                    { name: 'TIPO_CITA', index: 'TIPO CITA', width: 18 },
                    { name: 'FECHA', index: 'FECHA', width: 7 },
                    { name: 'FECHA2', index: 'FECHA2', hidden: true },
                    { name: 'HORA', index: 'HORA', hidden: true },
                ],

                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);

                    asignaAtributo("lblIdProgramacionTerapiaEditar", datosRow.ID, 0)
                    asignaAtributo("txtFechaTerapiaEditar", datosRow.FECHA2, 0)
                    asignaAtributo("txtHoraTerapiaEditar", datosRow.HORA, 0)
                    asignaAtributoCombo2('cmbTipoCitaTerapiaEditar', datosRow.ID_TIPO_CITA, datosRow.TIPO_CITA);
                    mostrar('divVentanitaEditarTerapia')
                },
                height: 150,
                // width: 1270,
                width: 1010,
            });
            $('#' + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listaProgramacionTerapias':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=272" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdProcedimientoAutoriza'));


            $('#' + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'ID', 'CANTIDAD', 'ID_ESTADO', 'ESTADO', 'ID_SEDE',
                    'SEDE', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'ID_TIPO_CITA', 'TIPO_CITA',
                    'ID_MEDICO', 'MEDICO', 'FECHA_PROGRAMACION'],
                colModel: [
                    { name: 'contador', index: 'contador', width: 5 },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'CANTIDAD', index: 'CANTIDAD', width: 10 },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: 20 },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', width: 20 },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', width: 30 },
                    { name: 'ID_TIPO_CITA', index: 'ID_TIPO_CITA', hidden: true },
                    { name: 'TIPO_CITA', index: 'TIPO_CITA', width: 30 },
                    { name: 'ID_MEDICO', index: 'ID_MEDICO', hidden: true },
                    { name: 'MEDICO', index: 'MEDICO', width: 30 },
                    { name: 'FECHA_PROGRAMACION', index: 'FECHA_PROGRAMACION', width: 20 },
                ],

                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);

                    asignaAtributo("lblIdProgramacionTerapiaEditar", datosRow.ID, 0)
                    asignaAtributo("txtFechaInicioTerapiaEditar", datosRow.FECHA_PROGRAMACION, 0)
                    asignaAtributoCombo2('cmbSedeTerapiaEditar', datosRow.ID_SEDE, datosRow.SEDE);
                    asignaAtributoCombo2('cmbIdEspecialidadTerapiaEditar', datosRow.ID_ESPECIALIDAD, datosRow.ESPECIALIDAD);
                    asignaAtributoCombo2('cmbTipoCitaTerapiaEditar', datosRow.ID_TIPO_CITA, datosRow.TIPO_CITA);
                    asignaAtributoCombo2('cmbProfesionalesTerapiaEditar', datosRow.ID_MEDICO, datosRow.MEDICO);

                    mostrar('divVentanitaEditarTerapia')

                },
                height: 200,
                // width: 1270,
                autowidth: true
            });
            $('#' + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;





        case 'listaPacientesUrgencias':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=240&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdTipoServicioUrgencias'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoServicioUrgencias'));
            add_valores_a_mandar(valorAtributo('lblIdPacienteUrgencias'));
            add_valores_a_mandar(valorAtributo('lblIdPacienteUrgencias'));

            if (valorAtributo('lblIdPacienteUrgencias') == "") {
                add_valores_a_mandar("1");
                add_valores_a_mandar("1");
            } else {
                add_valores_a_mandar("");
                add_valores_a_mandar("");
            }
            add_valores_a_mandar(valorAtributo('cmbPreferencialURG'));
            add_valores_a_mandar(valorAtributo('cmbPreferencialURG'));
            add_valores_a_mandar(IdSede());
            add_valores_a_mandar(valorAtributo('cmbTieneOrden'))
            add_valores_a_mandar(valorAtributo('cmbTieneOrden'))

            $('#drag' + ventanaActual.num).find("#listaPacientesUrgencias").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'ID_ADMISION', 'ID_PACIENTE', 'IDENTIFICACION', 'NOMBRE', 'ID_PREFERENCIAL', 'PREF',
                    'INGRESO', 'EGRESO', 'TIEMPO ESTANCIA', 'TRIAGE', 'ID_TIPO', 'TIPO ADMISION', 'ID_SERVICIO',
                    'SERVICIO', 'AREA', 'HABITACION', 'CAMA', 'ID_PROFESIONAL', 'PROFESIONAL', 'ESTADO FACTURA',
                    'MODIFICACION', 'ID_ESTADO', 'ESTADO', 'ID_FACTURA'],
                colModel: [
                    { name: 'contador', index: 'contador', width: 30 },
                    { name: 'ID_ADMISION', index: 'ID_ADMISION', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: 100 },
                    { name: 'NOMBRE', index: 'NOMBRE', width: 250 },
                    { name: 'ID_PREFERENCIAL', index: 'ID_PREFERENCIAL', hidden: true },
                    { name: 'PREF', index: 'PREFERENCIAL', width: 40 },
                    { name: 'INGRESO', index: 'INGRESO', width: 115 },
                    { name: 'EGRESO', index: 'EGRESO', width: 115 },
                    { name: 'TIEMPO', index: 'TIEMPO ESTANCIA', width: 150 },
                    { name: 'TRIAGE', index: 'TRIAGE', width: 110 },
                    { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                    { name: 'TIPO_ADMISION', index: 'TIPO ADMISION', width: 150 },
                    { name: 'ID_SERVICIO', index: 'ID_SERVICIO', hidden: true },
                    { name: 'SERVICIO', index: 'SERVICIO', width: 100 },
                    { name: 'AREA', index: 'AREA', width: 150 },
                    { name: 'HABITACION', index: 'HABITACION', width: 150 },
                    { name: 'CAMA', index: 'CAMA', width: 150 },
                    { name: 'ID_PROFESIONAL', index: 'ID_PROFESIONAL', hidden: true },
                    { name: 'PROFESIONAL', index: 'PROFESIONAL', width: 200 },
                    { name: 'ESTADO FACTURA', index: 'FACTURA', hidden: true },
                    { name: 'MODIFICACION', index: 'MODIFICACION', width: 115 },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', hidden: true },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                ],

                onSelectRow: function (rowid) {
                    datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo("lblIdPacienteModificarURG", datosRow.ID_PACIENTE, 1)
                    asignaAtributo("lblIdentificacionPacienteModificarURG", datosRow.IDENTIFICACION, 1)
                    asignaAtributo("lblNombrePacienteModificarURG", datosRow.NOMBRE, 1)
                    asignaAtributo("lblIdAdmisionModificarURG", datosRow.ID_ADMISION, 1)
                    asignaAtributo("cmbModificarPreferencialURG", datosRow.ID_PREFERENCIAL, 0)
                    asignaAtributo("cmbModificarEstadoAdmisionURG", datosRow.ID_ESTADO, 0)
                    asignaAtributo("lblIdServicio", datosRow.ID_SERVICIO, 0)
                    asignaAtributo("lblIdFactura", datosRow.ID_FACTURA, 0)
                    asignaAtributo("lblServicio", datosRow.SERVICIO, 0)
                    mostrar("divmodificarAdmisionPacienteURG")
                    buscarHC("ordenesNoGestionadasURG", '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                    setTimeout(() => {
                        cargarComboGRALCondicion1('cmbPadre', '', "cmbProfesionalAdmision", 77, IdEmpresa());
                        setTimeout(() => {
                            asignaAtributo("cmbProfesionalAdmision", datosRow.ID_PROFESIONAL, 0)
                            cargarComboGRALCondicion1('cmbPadre', datosRow.ID_TIPO, "cmbIdTipoAdmisionModificarURG", 157, IdEmpresa());
                        }, 200);
                    }, 200);
                },
                height: 350,
                width: 1200,
                shrinkToFit: false,
            });
            $('#drag' + ventanaActual.num).find("#listaPacientesUrgencias").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listPacientesParlante':
            ancho = 800;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=168" + "&parametros=";

            $('#drag' + ventanaActual.num).find("#listPacientesParlante").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'CONSULTORIO', 'ID_PACIENTE', 'IDENTIFICACION', 'PACIENTE', 'ORDEN', 'TURNO', 'ID_ESTADO', 'ESTADO', 'IDS'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'CONSULTORIO', index: 'CONSULTORIO', width: anchoP(ancho, 5), align: 'left' },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true, align: 'left' },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 10) },
                    { name: 'PACIENTE', index: 'PACIENTE', width: anchoP(ancho, 30), align: 'left' },
                    { name: 'ORDEN', index: 'ORDEN', width: anchoP(ancho, 5), align: 'left' },
                    { name: 'TURNO', index: 'TURNO', width: anchoP(ancho, 5), align: 'left' },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true, align: 'left' },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 10), align: 'left' },
                    { name: 'IDS', index: 'IDS', hidden: true, align: 'left' }
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdPacienteParlante', datosRow.ID_PACIENTE, 0)
                    asignaAtributo('lblPacienteParlante', datosRow.PACIENTE, 0)
                    asignaAtributo('lblConsultorioParlante', datosRow.CONSULTORIO, 0)
                    asignaAtributo('lblEstadoParlante', datosRow.ID_ESTADO, 0)
                    asignaAtributo('lblTurno', datosRow.TURNO, 0)
                    asignaAtributo('lblIds', datosRow.IDS, 0)

                    mostrar('divVentanitaParlante');

                },
                height: 350,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#listPacientesParlante").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listAgendaNotifica':
            limpiarListadosTotales('listAgendaNotifica');
            ancho = ($('#drag' + ventanaActual.num).find("#idDivAgendaNotifica").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=851&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));


            $('#drag' + ventanaActual.num).find("#listAgendaNotifica").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'id', 'asunto', 'contenido', 'fecha_crea', 'estado cita', 'Observaciones cita'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 5), align: 'left' },
                    { name: 'asunto', index: 'asunto', hidden: true, align: 'left' },
                    { name: 'contenido', index: 'contenido', width: anchoP(ancho, 30) },
                    { name: 'fecha_crea', index: 'fecha_crea', width: anchoP(ancho, 10), align: 'left' },
                    { name: 'estado', index: 'estado', width: anchoP(ancho, 10), align: 'left' },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 30), align: 'left' },
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblFechaSeleccionada', datosRow.Fecha2, 0)

                    if (valorAtributo('txt_banderaOpcionCirugia') != 'ARCHIVO')
                        buscarAGENDA('listAgenda')
                },
                height: 200,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#listAgendaNotifica").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listPersonalCirugia':

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=111&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));

            $('#drag' + ventanaActual.num).find("#listPersonalCirugia").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'ID_PROFESION', 'PROFESION', 'ID_PERSONAL', 'PERSONAL', 'OBSERVACION'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_PROFESION', index: 'ID_PROFESION', width: anchoP(ancho, 5) },
                    { name: 'PROFESION', index: 'PROFESION', width: anchoP(ancho, 20) },
                    { name: 'ID_PERSONAL', index: 'ID_PERSONAL', width: anchoP(ancho, 20) },
                    { name: 'PERSONAL', index: 'PERSONAL', width: anchoP(ancho, 40) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 20) }
                ],
                onSelectRow: function (rowid) {
                    eliminarElementoGrilla('listPersonalCirugia', rowid);
                },

                height: 50,
                width: 1000,
            });
            $('#drag' + ventanaActual.num).find("#listPersonalCirugia").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listAgendaAdmisionCuenta':
            ancho = ($('#drag' + ventanaActual.num).find("#listAgendaAdmision").width()) - 80;
            valores_a_mandar = pag;
            //if (valorAtributo('cmbAdmision') == 'Sin')
            //     valores_a_mandar = valores_a_mandar + "?idQuery=657" + "&parametros=";
            //else
            //     valores_a_mandar = valores_a_mandar + "?idQuery=67" + "&parametros=";

            valores_a_mandar = valores_a_mandar + "?idQuery=668" + "&parametros=";

            add_valores_a_mandar(valorAtributo('cmbExito'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidadCita'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoServicio'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionalesCita'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPacienteCita'));
            add_valores_a_mandar(valorAtributo('cmbAsiste'));
            add_valores_a_mandar(valorAtributo('cmbAdmision'));

            $('#drag' + ventanaActual.num).find("#listAgendaAdmision").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'FECHA_CITA', 'HORA', 'MIN_DURACION',
                    'ID PACIENTE', 'TIPO_ID', 'IDENTIFICACION', 'NOMBRE PACIENTE',
                    'ID_AGENDA', 'ID_ELABORO', 'FECHA_ELABORO', 'ID_ESTADO_AGENDA', 'TIPO', 'NOMTIPO', 'ID_MOTIVO',
                    'ID_ADMINISTRADORA', 'NOM_ADMINISTRADORA', 'NO_REMISION', 'ID_INSTITU_REMITE', 'NOM_INSTITU_REMITE', 'OBSERVACION', 'ID_ESTADO', 'ESTADO',
                    'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'ID_SERVICIO', 'ID_MEDICO', 'MEDICO', 'ADMISION',
                    'NO_LENTE_INTRAOCULAR', 'ID_TIPO_ANESTECIA', 'ID_INSTRUMENTADOR', 'ID_CIRCULANTE',
                    'PREFACTURA', 'ASISTE', 'PREFERENCIAL', 'OBSERVACION', 'MOTIVO', 'ID_MOTIVO_CLASE', 'MOTIVO_CLASE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'FECHA_CITA', index: 'FECHA_CITA', hidden: true },
                    { name: 'HORA', index: 'HORA', width: anchoP(ancho, 3) },
                    { name: 'DURACION', index: 'DURACION', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'TIPO_ID', index: 'TIPO_ID', width: anchoP(ancho, 2) },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 6) },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: anchoP(ancho, 20) },
                    { name: 'ID_AGENDA', index: 'ID_AGENDA', hidden: true },
                    { name: 'ID_ELABORO', index: 'ID_ELABORO', hidden: true },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', hidden: true },
                    { name: 'ID_ESTADO_AGENDA', index: 'ID_ESTADO_AGENDA', hidden: true },
                    { name: 'TIPO', index: 'TIPO', hidden: true },
                    { name: 'NOMTIPO', index: 'NOMTIPO', width: anchoP(ancho, 14) },
                    { name: 'ID_MOTIVO', index: 'ID_MOTIVO', hidden: true },
                    { name: 'ID_ADMINISTRADORA', index: 'ID_ADMINISTRADORA', hidden: true },
                    { name: 'NOM_ADMINISTRADORA', index: 'NOM_ADMINISTRADORA', hidden: true },
                    { name: 'NO_REMISION', index: 'NO_REMISION', hidden: true },
                    { name: 'ID_INSTITU_REMITE', index: 'ID_INSTITU_REMITE', hidden: true },
                    { name: 'NOM_INSTITU_REMITE', index: 'NOM_INSTITU_REMITE', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 8) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 8) },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', hidden: true },
                    { name: 'ID_SERVICIO', index: 'ID_SERVICIO', hidden: true },
                    //{ name: 'ID_SUB_ESPECIALIDAD', index: 'ID_SUB_ESPECIALIDAD', hidden: true },
                    //{ name: 'NOM_SUB_ESPECIALIDAD', index: 'NOM_SUB_ESPECIALIDAD', hidden: true },
                    { name: 'ID_MEDICO', index: 'ID_MEDICO', hidden: true },
                    { name: 'MEDICO', index: 'MEDICO', width: anchoP(ancho, 10) },

                    { name: 'ID_ADMISION', index: 'ID_ADMISION', width: anchoP(ancho, 10) },

                    { name: 'NO_LENTE_INTRAOCULAR', index: 'NO_LENTE_INTRAOCULAR', hidden: true },
                    { name: 'ID_TIPO_ANESTECIA', index: 'ID_TIPO_ANESTECIA', hidden: true },
                    { name: 'ID_INSTRUMENTADOR', index: 'ID_INSTRUMENTADOR', hidden: true },
                    { name: 'ID_CIRCULANTE', index: 'ID_CIRCULANTE', hidden: true },
                    { name: 'PREFACTURA', index: 'PREFACTURA', width: anchoP(ancho, 5) },
                    { name: 'ASISTE', index: 'ASISTE', width: anchoP(ancho, 5) },
                    { name: 'PREFERENCIAL', index: 'PREFERENCIAL', width: anchoP(ancho, 5) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 5) },
                    { name: 'MOTIVO', index: 'MOTIVO', hidden: true },
                    { name: 'ID_MOTIVO_CLASE', index: 'ID_MOTIVO_CLASE', hidden: true },
                    { name: 'MOTIVO_CLASE', index: 'MOTIVO_CLASE', hidden: true },
                ],
                height: 400,
                width: ancho,
                onSelectRow: function (rowid) {

                    //limpiarDivEditarJuan('camposDivVentanitaAgenda') 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listAgendaAdmision').getRowData(rowid);


                    cargarComboGRAL('cmbPadre', '', 'cmbTipoAdmision', 536)


                    if (confirm('TRAER DE LA AGENDAS A UNA ADMISION... ?')) {

                        if (datosRow.ID_ESTADO == 'N')
                            alert('TIENE QUE ESTAR EN ESTADO EXITO .');
                        else if (datosRow.ID_ESTADO == 'L')
                            alert('TIENE QUE ESTAR EN ESTADO EXITO..');
                        else if (datosRow.ID_ESTADO == 'R')
                            alert('TIENE QUE ESTAR EN ESTADO EXITO...');
                        else { /*para los demas casos de si exito*/


                            if (datosRow.ID_ADMISION == '') {
                                limpiarDatosAdmision();
                                limpiarListadosTotales('listAdmisionCuenta');
                                asignaAtributo('lblIdCita', datosRow.ID, 0)
                                asignaAtributo('lblFechaCita', datosRow.FECHA_CITA, 0);
                                asignaAtributo('cmbTipoAdmision', datosRow.TIPO, 0)
                                //                     asignaAtributo('cmbEstadoCita', datosRow.ID_ESTADO, 0)
                                //                     asignaAtributo('cmbMotivoConsulta', datosRow.ID_MOTIVO, 0)
                                asignaAtributo('txtIdBusPaciente', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.TIPO_ID + datosRow.IDENTIFICACION + ' ' + datosRow.NOMBRE_PACIENTE), 0)
                                asignaAtributo('txtAdministradora1', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.NOM_ADMINISTRADORA), 0)
                                asignaAtributo('txtNoAutorizacion', datosRow.NO_REMISION, 0)
                                asignaAtributo('txtIPSRemite', concatenarCodigoNombre(datosRow.ID_INSTITU_REMITE, datosRow.NOM_INSTITU_REMITE), 0)

                                asignaAtributo('cmbIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0)
                                asignaAtributo('cmbIdTipoServicio', datosRow.ID_SERVICIO, 0)
                                //asignaAtributo('cmbIdSubEspecialidad', datosRow.ID_SUB_ESPECIALIDAD, 0)
                                asignaAtributo('cmbIdProfesionales', datosRow.ID_MEDICO, 0)

                                buscarInformacionBasicaPacienteParaAdmisiones(datosRow.ID_SERVICIO)

                                if (datosRow.ID_SERVICIO == 'CIR') {
                                    asignaAtributo('txtNoLente', datosRow.NO_LENTE_INTRAOCULAR, 0)
                                    asignaAtributo('cmbTipoAnestesia', datosRow.ID_TIPO_ANESTECIA, 0)
                                    asignaAtributo('txtIdInstrumentador', datosRow.ID_INSTRUMENTADOR, 0)
                                    asignaAtributo('txtIdCirculante', datosRow.ID_CIRCULANTE, 0)
                                    asignaAtributo('cmbIdSitioQuirurgico', datosRow.ID_SITIO_QUIRURGICO, 0)
                                    asignaAtributo('txtIdProcedimiento', datosRow.ID_PROCEDIMIENTO, 0)
                                    mostrar('divAcordionInfoCirugia')
                                } else {
                                    mostrar('divAcordionInfoCirugia')
                                }
                                ocultar('divVentanitaAgendaAdmision')
                            }
                        }


                    } else {
                        if (datosRow.ID_ADMISION == '') {
                            habilitar('BTN_MODIFICAR_ESTADO_CITA', 1);
                        } else {
                            habilitar('BTN_MODIFICAR_ESTADO_CITA', 0);
                        }



                        limpiaAtributo('cmbEstadoCitaEdit', 0);
                        limpiaAtributo('cmbMotivoConsultaEdit', 0);
                        limpiaAtributo('cmbMotivoConsultaClaseEdit', 0);

                        mostrar('divVentanitaEditarAgenda')
                        asignaAtributo('lblIdCitaEdit', datosRow.ID, 0)
                        asignaAtributo('lblPacienteCitaEdit', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.TIPO_ID + datosRow.IDENTIFICACION + ' ' + datosRow.NOMBRE_PACIENTE), 0)
                        asignaAtributo('lblFechaCitaEdit', datosRow.FECHA_CITA, 0)
                        asignaAtributo('cmbEstadoCitaEdit', datosRow.ID_ESTADO, 0)
                        //asignaAtributo('cmbMotivoConsultaEdit', datosRow.ID_MOTIVO, 0)
                        asignaAtributoCombo2('cmbMotivoConsultaEdit', datosRow.ID_MOTIVO, datosRow.MOTIVO)
                        asignaAtributoCombo2('cmbMotivoConsultaClaseEdit', datosRow.ID_MOTIVO_CLASE, datosRow.MOTIVO_CLASE)
                        asignaAtributo('txtIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0)

                    }

                },

            });
            $('#drag' + ventanaActual.num).find("#listAgendaAdmision").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listAgendaAdmisionCuentas':
            valores_a_mandar = pag;

            valores_a_mandar = valores_a_mandar + "?idQuery=681&parametros=";
            add_valores_a_mandar(valorAtributo('cmbExito'));
            add_valores_a_mandar(IdSede());
            add_valores_a_mandar(valorAtributo('txtFechaTraerCita'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidadCita'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoServicio'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionalesCita'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPacienteCita'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministraPaciente'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministraPaciente'));
            add_valores_a_mandar(valorAtributo('cmbAsiste'));
            add_valores_a_mandar(valorAtributo('cmbAdmision'));
            add_valores_a_mandar(valorAtributo('cmbAdmision'));

            $('#drag' + ventanaActual.num).find("#listAgendaAdmision").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', '', 'ID', 'FECHA_CITA', 'HORA', 'MIN_DURACION',
                    'ID PACIENTE', 'TIPO_ID', 'IDENTIFICACION', 'NOMBRE PACIENTE',
                    'ID_ELABORO', 'ID_ESTADO_AGENDA', 'TIPO', 'NOMTIPO', 'ID_MOTIVO',
                    'ID_ADMINISTRADORA', 'ADMINISTRADORA', 'NO_REMISION', 'ID_INSTITU_REMITE', 'NOM_INSTITU_REMITE', 'OBSERVACION', 'ID_ESTADO', 'ESTADO',
                    'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'ID_SERVICIO', 'NOMBRE_SERVICIO', 'ID_MEDICO', 'MEDICO',
                    'NO_LENTE_INTRAOCULAR', 'ID_TIPO_ANESTECIA', 'ID_INSTRUMENTADOR', 'ID_CIRCULANTE',
                    'ASISTE', 'PREFERENCIAL', 'OBSERVACION',
                    'ADMISION', 'PREFACTURA', 'CITA FUTURA', 'MOTIVO', 'ID_MOTIVO_CLASE', 'MOTIVO_CLASE',
                    'ID_PLAN', 'PLAN', 'ID_REGIMEN', 'REGIMEN', 'ESTADO_FACTURA', ''

                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: 5 },
                    { name: 'EDITAR1', index: '', width: 5 },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'FECHA_CITA', index: 'FECHA_CITA', hidden: true },
                    { name: 'HORA', index: 'HORA', width: 12 },
                    { name: 'DURACION', index: 'DURACION', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'TIPO_ID', index: 'TIPO_ID', hidden: true },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: 20 },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: 50 },

                    { name: 'ID_ELABORO', index: 'ID_ELABORO', hidden: true },

                    { name: 'ID_ESTADO_AGENDA', index: 'ID_ESTADO_AGENDA', hidden: true },
                    { name: 'TIPO', index: 'TIPO', hidden: true },
                    { name: 'NOMTIPO', index: 'NOMTIPO', width: 40 },
                    { name: 'ID_MOTIVO', index: 'ID_MOTIVO', hidden: true },
                    { name: 'ID_ADMINISTRADORA', index: 'ID_ADMINISTRADORA', hidden: true },
                    { name: 'NOM_ADMINISTRADORA', index: 'NOM_ADMINISTRADORA', width: 40 },
                    { name: 'NO_REMISION', index: 'NO_REMISION', hidden: true },
                    { name: 'ID_INSTITU_REMITE', index: 'ID_INSTITU_REMITE', hidden: true },
                    { name: 'NOM_INSTITU_REMITE', index: 'NOM_INSTITU_REMITE', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: 40 },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', hidden: true },
                    { name: 'ID_SERVICIO', index: 'ID_SERVICIO', hidden: true },
                    { name: 'NOMBRE_SERVICIO', index: 'NOMBRE_SERVICIO', hidden: true },
                    { name: 'ID_MEDICO', index: 'ID_MEDICO', hidden: true },
                    { name: 'MEDICO', index: 'MEDICO', width: 40 },

                    { name: 'NO_LENTE_INTRAOCULAR', index: 'NO_LENTE_INTRAOCULAR', hidden: true },
                    { name: 'ID_TIPO_ANESTECIA', index: 'ID_TIPO_ANESTECIA', hidden: true },
                    { name: 'ID_INSTRUMENTADOR', index: 'ID_INSTRUMENTADOR', hidden: true },
                    { name: 'ID_CIRCULANTE', index: 'ID_CIRCULANTE', hidden: true },

                    { name: 'ASISTE', index: 'ASISTE', width: 10 },
                    { name: 'PREFERENCIAL', index: 'PREFERENCIAL', width: 10 },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: 40 },

                    { name: 'ID_ADMISION', index: 'ID_ADMISION', width: 15 },
                    { name: 'PREFACTURA', index: 'PREFACTURA', width: 10 },
                    { name: 'CITA_FUTURA', index: 'CITA_FUTURA', width: 10 },
                    { name: 'MOTIVO', index: 'MOTIVO', hidden: true },
                    { name: 'ID_MOTIVO_CLASE', index: 'ID_MOTIVO_CLASE', hidden: true },
                    { name: 'MOTIVO_CLASE', index: 'MOTIVO_CLASE', hidden: true },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'PLAN', index: 'PLAN', hidden: true },
                    { name: 'ID_REGIMEN', index: 'ID_REGIMEN', hidden: true },
                    { name: 'REGIMEN', index: 'REGIMEN', hidden: true },
                    { name: 'ESTADO_FACTURA', index: 'ESTADO_FACTURA', hidden: true },
                    { name: 'ESTADO_FACTURACION', index: '', width: 15 },
                ],
                height: 400,
                width: 1300,

                onCellSelect: function (row, col, content, event) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listAgendaAdmision').getRowData(row);

                    if (col != 1) {
                        if (datosRow.ID_ESTADO == 'N')
                            alert('TIENE QUE ESTAR EN ESTADO EXITO.');
                        else if (datosRow.ID_ESTADO == 'L')
                            alert('TIENE QUE ESTAR EN ESTADO EXITO..');
                        else if (datosRow.ID_ESTADO == 'R')
                            alert('TIENE QUE ESTAR EN ESTADO EXITO...');
                        else {
                            limpiarDatosAdmisiones();
                            limpiarListadosTotales('listAdmisionCuenta');

                            asignaAtributo('txtIdBusPaciente', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.TIPO_ID + datosRow.IDENTIFICACION + ' ' + datosRow.NOMBRE_PACIENTE), 0)
                            buscarInformacionBasicaPacienteParaAdmisiones(datosRow.ID_SERVICIO)

                            asignaAtributo('cmbPreferencial', datosRow.PREFERENCIAL.split(';')[1], 0)
                            asignaAtributo('lblIdCita', datosRow.ID, 0)
                            asignaAtributo('lblIdAgendaDetalle', datosRow.ID, 0)
                            asignaAtributo('lblFechaCita', datosRow.FECHA_CITA, 0);
                            asignaAtributo('cmbTipoAdmision', datosRow.TIPO, 0)
                            asignaAtributo('lblCitaFutura', datosRow.CITA_FUTURA, 0)
                            asignaAtributo('txtAdministradora1', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.NOM_ADMINISTRADORA), 0)

                            var num_auto = validarNumero(datosRow.NO_REMISION) ? datosRow.NO_REMISION : 0;
                            asignaAtributo('txtNoAutorizacion', num_auto, 0)

                            asignaAtributo('txtIPSRemite', concatenarCodigoNombre(datosRow.ID_INSTITU_REMITE, datosRow.NOM_INSTITU_REMITE), 0)

                            asignaAtributo('cmbIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0)
                            asignaAtributoCombo('cmbTipoAdmision', datosRow.TIPO, datosRow.NOMTIPO)
                            asignaAtributoCombo('cmbIdTipoServicio', datosRow.ID_SERVICIO, datosRow.NOMBRE_SERVICIO)
                            asignaAtributoCombo('cmbIdSubEspecialidad', datosRow.ID_SUB_ESPECIALIDAD, datosRow.NOM_SUB_ESPECIALIDAD)
                            asignaAtributoCombo('cmbIdProfesionales', datosRow.ID_MEDICO, datosRow.MEDICO)
                            asignaAtributoCombo('cmbIdProfesionalesFactura', datosRow.ID_MEDICO, datosRow.MEDICO)

                            asignaAtributo('lblIdPlanContratacion', datosRow.ID_PLAN, 0)
                            asignaAtributo('lblNomPlanContratacion', datosRow.PLAN, 0)
                            asignaAtributoCombo('cmbIdTipoRegimen', datosRow.ID_REGIMEN, datosRow.REGIMEN);
                            setTimeout(() => {
                                traerPlanesDeContratacion('cmbIdPlan', '165', 'txtAdministradora1', 'cmbIdTipoRegimen')
                                setTimeout(() => {
                                    asignaAtributo('cmbIdPlan', datosRow.ID_PLAN, 0)
                                }, 300);
                            }, 200);


                            if (datosRow.ID_SERVICIO == 'CIR') {
                                asignaAtributo('txtNoLente', datosRow.NO_LENTE_INTRAOCULAR, 0)
                                asignaAtributo('cmbTipoAnestesia', datosRow.ID_TIPO_ANESTECIA, 0)
                                asignaAtributo('txtIdInstrumentador', datosRow.ID_INSTRUMENTADOR, 0)
                                asignaAtributo('txtIdCirculante', datosRow.ID_CIRCULANTE, 0)
                                asignaAtributo('cmbIdSitioQuirurgico', datosRow.ID_SITIO_QUIRURGICO, 0)
                                asignaAtributo('txtIdProcedimiento', datosRow.ID_PROCEDIMIENTO, 0)
                                asignaAtributoCombo2('cmbFinalidad', '1', 'Diagnostico')
                            } else {
                                asignaAtributoCombo2('cmbFinalidad', '10', 'No Aplica')
                            }

                            mostrar('divAcordionInfoCirugia')
                            ocultar('divVentanitaAgendaAdmision')

                            if (confirm("FACTURAR PACIENTE ? ")) {
                                setTimeout(() => {
                                    modificarCRUD("actualizarEstadoFacturacionCita_F");
                                }, 1500);
                            } else {
                                setTimeout(() => {
                                    modificarCRUD("actualizarEstadoFacturacionCita_S");
                                }, 1500);
                            }

                            setTimeout(() => {
                                if (valorAtributo('txtAdmisiones') == 'SI') {
                                    if (datosRow.ID_SERVICIO == 'CIR') {
                                        buscarAGENDA('listADMISIONESCirugiaProcedimientoTraer')
                                    } else {
                                        buscarAGENDA('listAdmisionCEXProcedimientoTraer')
                                    }
                                } else {
                                    if (tipoServicio == 'CIR') {
                                        buscarAGENDA('listAdmisionCirugiaProcedimientoTraer')
                                    }
                                }

                                setTimeout(() => {
                                    buscarAGENDA('listAdmisionCuenta')
                                }, 200);
                            }, 600);

                        }
                    } else {
                        if (datosRow.ID_ADMISION == '') {
                            habilitar('BTN_MODIFICAR_ESTADO_CITA', 1);
                        } else {
                            habilitar('BTN_MODIFICAR_ESTADO_CITA', 0);
                        }
                        if (datosRow.PREFACTURA == 'SI') {
                            asignaAtributo('txtEstadoFacturaVentanitaAgenda', datosRow.ESTADO_FACTURA, 0)
                            mostrar('BTN_MODIFICAR_PREFACTURA')
                            ocultar('BTN_MODIFICAR_ESTADO_CITA')
                        } else {
                            ocultar('BTN_MODIFICAR_PREFACTURA')
                            mostrar('BTN_MODIFICAR_ESTADO_CITA')
                        }
                        limpiaAtributo('cmbEstadoCitaEdit', 0);
                        limpiaAtributo('cmbMotivoConsultaEdit', 0);
                        limpiaAtributo('cmbMotivoConsultaClaseEdit', 0);

                        mostrar('divVentanitaEditarAgenda')

                        if (datosRow.PREFERENCIAL == '') {
                            preferencial = 'NO'
                        } else {
                            preferencial = datosRow.PREFERENCIAL.split(';')[1];
                        }
                        asignaAtributo('cmbPreferencial', preferencial, 0)
                        asignaAtributo('lblIdCitaEdit', datosRow.ID, 0)
                        asignaAtributo('lblPacienteCitaEdit', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.TIPO_ID + datosRow.IDENTIFICACION + ' ' + datosRow.NOMBRE_PACIENTE), 0)
                        asignaAtributo('lblFechaCitaEdit', datosRow.FECHA_CITA, 0)
                        asignaAtributo('cmbEstadoCitaEdit', datosRow.ID_ESTADO, 0)
                        asignaAtributoCombo2('cmbMotivoConsultaEdit', datosRow.ID_MOTIVO, datosRow.MOTIVO)
                        asignaAtributoCombo2('cmbMotivoConsultaClaseEdit', datosRow.ID_MOTIVO_CLASE, datosRow.MOTIVO_CLASE)
                        asignaAtributo('txtIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0)
                    }
                }
            });
            $('#drag' + ventanaActual.num).find("#listAgendaAdmision").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listAgendaAdmision':
            ancho = ($('#drag' + ventanaActual.num).find("#listAgendaAdmision").width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=668" + "&parametros=";

            add_valores_a_mandar(valorAtributo('cmbExito'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidadCita'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoServicio'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionalesCita'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPacienteCita'));
            add_valores_a_mandar(valorAtributo('cmbAsiste'));
            add_valores_a_mandar(valorAtributo('cmbAdmision'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'FECHA_CITA', 'HORA', 'MIN_DURACION',
                    'ID PACIENTE', 'TIPO_ID', 'IDENTIFICACION', 'NOMBRE PACIENTE',
                    'ID_AGENDA', 'ID_ELABORO', 'FECHA_ELABORO', 'ID_ESTADO_AGENDA', 'TIPO', 'NOMTIPO', 'ID_MOTIVO',
                    'ID_ADMINISTRADORA', 'NOM_ADMINISTRADORA', 'NO_REMISION', 'ID_INSTITU_REMITE', 'NOM_INSTITU_REMITE', 'OBSERVACION', 'ID_ESTADO', 'ESTADO',
                    'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'ID_SERVICIO', 'ID_MEDICO', 'MEDICO', 'ADMISION',
                    'NO_LENTE_INTRAOCULAR', 'ID_TIPO_ANESTECIA', 'ID_INSTRUMENTADOR', 'ID_CIRCULANTE',
                    'PREFACTURA', 'ASISTE', 'PREFERENCIAL', 'OBSERVACION', 'MOTIVO', 'ID_MOTIVO_CLASE', 'MOTIVO_CLASE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'FECHA_CITA', index: 'FECHA_CITA', hidden: true },
                    { name: 'HORA', index: 'HORA', width: anchoP(ancho, 4) },
                    { name: 'DURACION', index: 'DURACION', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'TIPO_ID', index: 'TIPO_ID', hidden: true },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 8) },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: anchoP(ancho, 18) },
                    { name: 'ID_AGENDA', index: 'ID_AGENDA', hidden: true },
                    { name: 'ID_ELABORO', index: 'ID_ELABORO', hidden: true },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', hidden: true },
                    { name: 'ID_ESTADO_AGENDA', index: 'ID_ESTADO_AGENDA', hidden: true },
                    { name: 'TIPO', index: 'TIPO', hidden: true },
                    { name: 'NOMTIPO', index: 'NOMTIPO', width: anchoP(ancho, 10) },
                    { name: 'ID_MOTIVO', index: 'ID_MOTIVO', hidden: true },
                    { name: 'ID_ADMINISTRADORA', index: 'ID_ADMINISTRADORA', hidden: true },
                    { name: 'NOM_ADMINISTRADORA', index: 'NOM_ADMINISTRADORA', hidden: true },
                    { name: 'NO_REMISION', index: 'NO_REMISION', hidden: true },
                    { name: 'ID_INSTITU_REMITE', index: 'ID_INSTITU_REMITE', hidden: true },
                    { name: 'NOM_INSTITU_REMITE', index: 'NOM_INSTITU_REMITE', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 8) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', hidden: true },
                    { name: 'ID_SERVICIO', index: 'ID_SERVICIO', hidden: true },
                    //{ name: 'ID_SUB_ESPECIALIDAD', index: 'ID_SUB_ESPECIALIDAD', hidden: true },
                    //{ name: 'NOM_SUB_ESPECIALIDAD', index: 'NOM_SUB_ESPECIALIDAD', width: anchoP(ancho, 12) },
                    { name: 'ID_MEDICO', index: 'ID_MEDICO', hidden: true },
                    { name: 'MEDICO', index: 'MEDICO', width: anchoP(ancho, 10) },
                    { name: 'ID_ADMISION', index: 'ID_ADMISION', width: anchoP(ancho, 6) },
                    { name: 'NO_LENTE_INTRAOCULAR', index: 'NO_LENTE_INTRAOCULAR', hidden: true },
                    { name: 'ID_TIPO_ANESTECIA', index: 'ID_TIPO_ANESTECIA', hidden: true },
                    { name: 'ID_INSTRUMENTADOR', index: 'ID_INSTRUMENTADOR', hidden: true },
                    { name: 'ID_CIRCULANTE', index: 'ID_CIRCULANTE', hidden: true },
                    { name: 'PREFACTURA', index: 'PREFACTURA', hidden: true },
                    { name: 'ASISTE', index: 'ASISTE', width: anchoP(ancho, 4) },
                    { name: 'PREFERENCIAL', index: 'PREFERENCIAL', width: anchoP(ancho, 4) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 18) },
                    { name: 'MOTIVO', index: 'MOTIVO', hidden: true },
                    { name: 'ID_MOTIVO_CLASE', index: 'ID_MOTIVO_CLASE', hidden: true },
                    { name: 'MOTIVO_CLASE', index: 'MOTIVO_CLASE', hidden: true }
                ],
                height: 400,
                width: ancho,
                onSelectRow: function (rowid) {

                    //limpiarDivEditarJuan('camposDivVentanitaAgenda') 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    if (confirm('9.8 TRAER DE LA AGENDAS A UNA ADMISION ?')) {

                        if (datosRow.ID_ESTADO == 'N')
                            alert('TIENE QUE ESTAR EN ESTADO EXITO .');
                        else if (datosRow.ID_ESTADO == 'L')
                            alert('TIENE QUE ESTAR EN ESTADO EXITO..');
                        else if (datosRow.ID_ESTADO == 'R')
                            alert('TIENE QUE ESTAR EN ESTADO EXITO...');
                        else { /*para los demas casos de si exito*/


                            if (datosRow.ID_ADMISION == '') {
                                limpiarDatosAdmision();
                                limpiarListadosTotales('listAdmision');
                                asignaAtributo('lblIdCita', datosRow.ID, 0)
                                asignaAtributo('lblFechaCita', datosRow.FECHA_CITA, 0);
                                asignaAtributo('cmbTipoAdmision', datosRow.TIPO, 0)
                                //                     asignaAtributo('cmbEstadoCita', datosRow.ID_ESTADO, 0)
                                //                     asignaAtributo('cmbMotivoConsulta', datosRow.ID_MOTIVO, 0)
                                asignaAtributo('txtIdBusPaciente', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.TIPO_ID + datosRow.IDENTIFICACION + ' ' + datosRow.NOMBRE_PACIENTE), 0)
                                asignaAtributo('txtAdministradora1', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.NOM_ADMINISTRADORA), 0)
                                asignaAtributo('txtNoAutorizacion', datosRow.NO_REMISION, 0)
                                asignaAtributo('txtIPSRemite', concatenarCodigoNombre(datosRow.ID_INSTITU_REMITE, datosRow.NOM_INSTITU_REMITE), 0)

                                asignaAtributo('cmbIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0)
                                asignaAtributo('cmbIdTipoServicio', datosRow.ID_SERVICIO, 0)
                                //asignaAtributo('cmbIdSubEspecialidad', datosRow.ID_SUB_ESPECIALIDAD, 0)
                                asignaAtributo('cmbIdProfesionales', datosRow.ID_MEDICO, 0)

                                buscarInformacionBasicaPacienteParaAdmisiones(datosRow.ID_SERVICIO);

                                if (datosRow.ID_SERVICIO == 'CIR') {
                                    asignaAtributo('txtNoLente', datosRow.NO_LENTE_INTRAOCULAR, 0)
                                    asignaAtributo('cmbTipoAnestesia', datosRow.ID_TIPO_ANESTECIA, 0)
                                    asignaAtributo('txtIdInstrumentador', datosRow.ID_INSTRUMENTADOR, 0)
                                    asignaAtributo('txtIdCirculante', datosRow.ID_CIRCULANTE, 0)
                                    asignaAtributo('cmbIdSitioQuirurgico', datosRow.ID_SITIO_QUIRURGICO, 0)
                                    asignaAtributo('txtIdProcedimiento', datosRow.ID_PROCEDIMIENTO, 0)
                                    mostrar('divAcordionInfoCirugia')
                                } else {
                                    mostrar('divAcordionInfoCirugia')
                                }
                                ocultar('divVentanitaAgendaAdmision')
                            } else {
                                alert('NO SE PUEDE TRAER PORQUE YA TIENE ADMISION');
                            }
                        }
                    } else {
                        if (datosRow.ID_ADMISION == '') {
                            habilitar('BTN_MODIFICAR_ESTADO_CITA', 1);
                        } else {
                            habilitar('BTN_MODIFICAR_ESTADO_CITA', 0);
                        }

                        limpiaAtributo('cmbEstadoCitaEdit', 0);
                        limpiaAtributo('cmbMotivoConsultaEdit', 0);
                        limpiaAtributo('cmbMotivoConsultaClaseEdit', 0);

                        mostrar('divVentanitaEditarAgenda')
                        asignaAtributo('lblIdCitaEdit', datosRow.ID, 0)
                        asignaAtributo('lblPacienteCitaEdit', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.TIPO_ID + datosRow.IDENTIFICACION + ' ' + datosRow.NOMBRE_PACIENTE), 0)
                        asignaAtributo('lblFechaCitaEdit', datosRow.FECHA_CITA, 0)
                        asignaAtributo('cmbEstadoCitaEdit', datosRow.ID_ESTADO, 0)
                        //asignaAtributo('cmbMotivoConsultaEdit', datosRow.ID_MOTIVO, 0)
                        asignaAtributoCombo2('cmbMotivoConsultaEdit', datosRow.ID_MOTIVO, datosRow.MOTIVO)
                        asignaAtributoCombo2('cmbMotivoConsultaClaseEdit', datosRow.ID_MOTIVO_CLASE, datosRow.MOTIVO_CLASE)
                        asignaAtributo('txtIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0)
                    }

                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listAdmisionCuenta':
            ancho = ($('#drag' + ventanaActual.num).find("#listAdmisionCuenta").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=520&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(IdSede())

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', '', 'ID ADM', 'INST_REMITE', 'DIAGNOSTICO', 'ID_SERVICIO', 'SERVICIO', 'INTERNACION', 'ID_ESPECIALIDAD', 'NOMBRE_ESPECIALIDAD',
                    'CAUSA_EXTERNA', 'FINALIDAD_CONSULTA', 'ID_PROFESIONAL', 'PROF_ADMISION', 'ID_PROFESIONAL_FACTURA', 'PROF_FACTURA', 'ID_TIPO_ADMISION', 'TIPO_ADMISION', 'ID_ADMINISTRADORA',
                    'ADMINISTRADORA', 'ID FACT', 'NO FACT', 'ID_PLAN', 'PLAN_DESCRIPCION', 'ID_TIPO_AFILIADO', 'NOM_TIPO_AFILIADO', 'ID_RANGO', 'NOM_RANGO',
                    'ID_REGIMEN', 'NOMBRE_REGIMEN', 'ID_ESTADO', 'ESTADO', 'FECHA',
                    //'ID_CITA', 
                    'NO_AUTORIZACION', 'OBSERVACION', 'USUARIO CREA', 'ES_CIRUGIA',
                    //'ID_ANESTESIA', 'ANESTESIA', 'ID_ANESTESIOLOGO', 'ANESTESIOLOGO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    {
                        name: "", index: '', width: anchoP(ancho, 2), align: 'center', formatter: function () {
                            return '<img id="editarAuto" src="/clinica/utilidades/imagenes/icons/icons/link.svg" width="18px" height="18px" align="middle">'
                        }
                    },
                    { name: 'ID', index: 'ID ADM', width: anchoP(ancho, 3) },
                    { name: 'INST_REMITE', index: 'INST_REMITE', hidden: true },
                    { name: 'DIAGNOSTICO', index: 'DIAGNOSTICO', hidden: true },
                    { name: 'ID_SERVICIO', index: 'ID_SERVICIO', hidden: true },
                    { name: 'SERVICIO', index: 'SERVICIO', width: anchoP(ancho, 7) },
                    { name: 'INTERNACION', index: 'INTERNACION', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'NOMBRE_ESPECIALIDAD', index: 'NOMBRE_ESPECIALIDAD', hidden: true },
                    { name: 'CAUSA_EXTERNA', index: 'CAUSA_EXTERNA', hidden: true },
                    { name: 'FINALIDAD_CONSULTA', index: 'FINALIDAD_CONSULTA', hidden: true },
                    { name: 'ID_PROFESIONAL', index: 'ID_PROFESIONAL', hidden: true },
                    { name: 'PROF_ADMISION', index: 'PROF_ADMISION', width: anchoP(ancho, 6) },
                    { name: 'ID_PROFESIONAL_FACTURA', index: 'ID_PROFESIONAL_FACTURA', hidden: true },
                    { name: 'PROF_FACTURA', index: 'PROF_FACTURA', hidden: true },
                    { name: 'ID_TIPO_ADMISION', index: 'ID_TIPO_ADMISION', hidden: true },
                    { name: 'TIPO_ADMISION', index: 'TIPO_ADMISION', width: anchoP(ancho, 7) },
                    { name: 'ID_ADMINISTRADORA', index: 'ID_ADMINISTRADORA', hidden: true },
                    { name: 'ADMINISTRADORA', index: 'ADMINISTRADORA', hidden: true },
                    { name: 'ID_FACTURA', index: 'ID FACT', width: anchoP(ancho, 3) },
                    { name: 'NO_FACTURA', index: 'NO FACT', width: anchoP(ancho, 3) },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'PLAN_DESCRIPCION', index: 'PLAN_DESCRIPCION', width: anchoP(ancho, 11) },
                    { name: 'ID_TIPO_AFILIADO', index: 'ID_TIPO_AFILIADO', hidden: true },
                    { name: 'NOM_TIPO_AFILIADO', index: 'NOM_TIPO_AFILIADO', hidden: true },
                    { name: 'ID_RANGO', index: 'ID_RANGO', hidden: true },
                    { name: 'NOM_RANGO', index: 'NOM_RANGO', hidden: true },
                    { name: 'ID_REGIMEN', index: 'ID_REGIMEN', hidden: true },
                    { name: 'NOMBRE_REGIMEN', index: 'NOMBRE_REGIMEN', hidden: true },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 5) },
                    { name: 'FECHA', index: 'FECHA', width: anchoP(ancho, 6) },
                    //{ name: 'ID_CITA', index: 'ID_CITA', hidden: true },
                    { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', hidden: true },
                    { name: 'USUARIO_CREA', index: 'USUARIO_CREA', width: anchoP(ancho, 5) },
                    { name: 'ES_CIRUGIA', index: 'ES_CIRUGIA', hidden: true },
                    /*{ name: 'ID_ANESTESIA', index: 'ID_ANESTESIA', hidden: true },
                    { name: 'ANESTESIA', index: 'ANESTESIA', hidden: true },
                    { name: 'ID_ANESTESIOLOGO', index: 'ID_ANESTESIOLOGO', hidden: true },
                    { name: 'ANESTESIOLOGO', index: 'ANESTESIOLOGO', hidden: true }*/
                ],

                onCellSelect: function (rowid, iCol, cellcontent, e) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    if (iCol == 1) {
                        mostrar('divAsociarCita')
                        asignaAtributo("lblIdAdmisionAsociarCita", datosRow.ID, 1);
                        asignaAtributo("lblIdFacturaAsociarCita", datosRow.ID_FACTURA, 1);
                        setTimeout(() => {
                            buscarAGENDA('citasDisponiblesAsociar')
                            setTimeout(() => {
                                buscarAGENDA('citasAdmision')
                            }, 300);
                        }, 300);
                    }
                    asignaAtributo('lblIdAdmision', datosRow.ID, 0)
                    asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
                    asignaAtributo('cmbCausaExterna', datosRow.CAUSA_EXTERNA, 0)
                    asignaAtributo('cmbFinalidad', datosRow.FINALIDAD_CONSULTA, 0)

                    asignaAtributo('cmbIdTipoServicio', datosRow.ID_SERVICIO, 0)
                    asignaAtributo('lblIdTipoAdmision', datosRow.ID_TIPO_ADMISION, 0)
                    asignaAtributo('lblNomTipoAdmision', datosRow.TIPO_ADMISION, 0)
                    asignaAtributo('lblIdEpsPlan', datosRow.ID_ADMINISTRADORA, 0)
                    asignaAtributo('lblIdRegimen', datosRow.ID_REGIMEN, 0)
                    asignaAtributo('lblNomRegimen', datosRow.NOMBRE_REGIMEN, 0)
                    asignaAtributo('lblIdTipoAfiliado', datosRow.ID_TIPO_AFILIADO, 0)
                    asignaAtributo('lblNomTipoAfiliado', datosRow.NOM_TIPO_AFILIADO, 0)
                    asignaAtributo('lblIdRango', datosRow.ID_RANGO, 0)
                    asignaAtributo('lblNomRango', datosRow.NOM_RANGO, 0)
                    asignaAtributo('lblUsuarioCrea', datosRow.USUARIO_CREA, 0)

                    asignaAtributo('txtIPSRemite', datosRow.INST_REMITE, 0)
                    asignaAtributo('txtIdDx', datosRow.DIAGNOSTICO, 0)
                    asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0)
                    asignaAtributo('lblInternado', datosRow.INTERNACION, 0)
                    if (datosRow.INTERNACION == "S") {
                        asignaAtributoCombo('cmbIdEspecialidad', datosRow.ID_ESPECIALIDAD, datosRow.NOMBRE_ESPECIALIDAD)
                    } else {
                        asignaAtributo('cmbIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0)
                    }
                    /*setTimeout(() => {
                        cargarTipoCitaSegunEspecialidad('cmbTipoAdmision', 'cmbIdEspecialidad')
                        setTimeout(() => {
                            asignaAtributo('cmbTipoAdmision', datosRow.ID_TIPO_ADMISION, 0)
                        }, 100);
                    }, 300);*/
                    asignaAtributoCombo('cmbTipoAdmision', datosRow.ID_TIPO_ADMISION, datosRow.TIPO_ADMISION)
                    asignaAtributoCombo('cmbIdProfesionales', datosRow.ID_PROFESIONAL, datosRow.PROF_ADMISION)
                    asignaAtributoCombo('cmbIdProfesionalesFactura', datosRow.ID_PROFESIONAL_FACTURA, datosRow.PROF_FACTURA)
                    asignaAtributo('txtAdministradora1', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.ADMINISTRADORA), 0)
                    asignaAtributo('txtIdAdminisAdmision', datosRow.ID_ADMINISTRADORA, 0)
                    asignaAtributoCombo('cmbIdTipoRegimen', datosRow.ID_REGIMEN, datosRow.NOMBRE_REGIMEN);
                    /*setTimeout(() => {
                        traerPlanesDeContratacion("cmbIdPlan", '165', 'txtAdministradora1', 'cmbIdTipoRegimen')
                        setTimeout(() => {
                            asignaAtributo("cmbIdPlan", datosRow.ID_PLAN, 0)
                            asignaAtributo('lblIdPlanContratacion', datosRow.ID_PLAN, 0)
                            setTimeout(() => {
                                asignaAtributo("lblIdPlan", datosRow.ID_PLAN)
                                asignaAtributo("lblNomPlan", valorAtributo("cmbIdPlan"))
                            }, 200);
                        }, 200);
                    }, 200);*/
                    asignaAtributoCombo("cmbIdPlan", datosRow.ID_PLAN, datosRow.PLAN_DESCRIPCION)
                    asignaAtributoCombo('cmbIdTipoAfiliado', datosRow.ID_TIPO_AFILIADO, datosRow.NOM_TIPO_AFILIADO);
                    asignaAtributoCombo('cmbIdRango', datosRow.ID_RANGO, datosRow.NOM_RANGO);
                    asignaAtributo('lblIdPlanContratacion', datosRow.ID_PLAN, 0)
                    asignaAtributo("lblIdPlan", datosRow.ID_PLAN)
                    asignaAtributo("lblNomPlan", datosRow.PLAN_DESCRIPCION)

                    asignaAtributo('txtNoAutorizacion', datosRow.NO_AUTORIZACION, 0)
                    asignaAtributo('cmbIdSubEspecialidad', datosRow.ID_SUB_ESPECIALIDAD, 0)
                    asignaAtributo('txtNoAutorizacion', datosRow.NO_AUTORIZACION, 0)
                    asignaAtributo('txtEsCirugia', datosRow.ES_CIRUGIA, 0)
                    asignaAtributo('lblIdCita', datosRow.ID_CITA, 0)
                    asignaAtributo('txtEstadoAdmisionFactura', datosRow.ESTADO, 0)

                    var cir = datosRow.ES_CIRUGIA === 'S' ? 'CIR' : '';
                    asignaAtributo('cmbIdTipoServicio', cir, 0)

                    if (cir === '') {
                        desHabilitar('btnAdicionarFactura', 0)
                        desHabilitar('btnContratados', 1)
                        desHabilitar('btnTarifarios', 1)
                        //asignaAtributoCombo2('cmbFinalidad', '10', 'No Aplica')
                    } else {
                        desHabilitar('btnAdicionarFactura', 1)
                        desHabilitar('btnContratados', 0)
                        desHabilitar('btnTarifarios', 0)
                        //asignaAtributoCombo2('cmbFinalidad', '1', 'Diagnostico')

                        asignaAtributoCombo2('cmbIdAnestesiologo', datosRow.ID_ANESTESIOLOGO, datosRow.ANESTESIOLOGO)
                        asignaAtributoCombo2('cmbIdTipoAnestesia', datosRow.ID_ANESTESIA, datosRow.ANESTESIA)
                        asignaAtributo('chk_Cirujano', 1, 0)
                        asignaAtributo('chk_Sala', 1, 0)
                        asignaAtributo('chk_Materiales', 1, 0)
                    }
                    limpiaAtributo('txtFechaAdmision', 0)
                    limpiaAtributo('txtIdProcedimientoCex', 0)
                    limpiaAtributo('cmbCantidad', 0)

                    limpiaAtributo('lblTotalCuenta', 0)
                    limpiaAtributo('lblValorNoCubierto', 0)
                    limpiaAtributo('lblValorCubierto', 0)
                    limpiaAtributo('lblDescuento', 0)
                    limpiaAtributo('lblTotalFactura', 0)

                    limpiarListadosTotales('listCitaCirugiaProcedimiento')

                    setTimeout(() => {
                        buscarAGENDA('listAdmisionCEXProcedimientoTraer')
                        setTimeout(() => {
                            buscarFacturacion('listProcedimientosDeFactura')
                            setTimeout(() => {
                                guardarYtraerDatoAlListado('consultarValoresCuenta');
                                setTimeout(() => {
                                    buscarFacturacion('listRecibosFactura');
                                    setTimeout(() => {
                                        buscarFacturacion('listArticulosDeFactura')
                                    }, 200);
                                }, 200);
                            }, 100);
                        }, 200);
                    }, 100);
                },
                height: 170,
                width: 1190,
            });
            $('#drag' + ventanaActual.num).find("#listAdmisionCuenta").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listMisCitasHoy':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=614" + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbExitoMisCitas'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('txtBusFechaDesde'));
            add_valores_a_mandar(valorAtributo('txtBusFechaHasta'));
            add_valores_a_mandar(IdSesion());

            $('#drag' + ventanaActual.num).find("#listMisCitasHoy").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'FECHA AGENDA', 'HORA', 'TIPO', 'IDENTIFICACION', 'NOMBRE', 'ESTADO', 'TIPO',],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'FECHA_AGENDA', index: 'FECHA_AGENDA', width: anchoP(ancho, 10) },
                    { name: 'HORA', index: 'HORA', width: anchoP(ancho, 5) },
                    // { name: 'SUB_ESPECIALIDAD', index: 'SUB_ESPECIALIDAD', width: anchoP(ancho, 15) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 5) },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 10) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 30) },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 8) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 15) },
                ],
                height: 400,
                width: ancho,

            });
            $('#drag' + ventanaActual.num).find("#listMisCitasHoy").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listAdmision':
            // limpiarListadosTotales('listAdmision');
            ancho = ($('#drag' + ventanaActual.num).find("#listAdmision").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=423" + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'ID', 'ID_PACIENTE', 'T.I', 'Identificacion', 'Nombre Paciente', 'Especialidad', 'Profesional',
                    'idTipoAdmision', 'TipoAdmision', 'idAdministradora', 'Administradora', 'Id Cita', 'Fecha Admision', 'Observacion', 'usuario Crea',
                    'id_cuenta', 'EstadoCuenta'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'T.I', index: 'T.I', hidden: true },
                    { name: 'Identificacion', index: 'Identificacion', hidden: true },
                    { name: 'NombrePaciente', index: 'NombrePaciente', hidden: true },
                    { name: 'Especialidad', index: 'Especialidad', width: anchoP(ancho, 15) },
                    // { name: 'SubEspecialidad', index: 'SubEspecialidad', width: anchoP(ancho, 15) },
                    { name: 'Profesional', index: 'Profesional', width: anchoP(ancho, 15) },
                    { name: 'idTipoAdmision', index: 'idTipoAdmision', hidden: true },
                    { name: 'TipoAdmision', index: 'TipoAdmision', width: anchoP(ancho, 10) },
                    { name: 'idAdministradora', index: 'idAdministradora', hidden: true },
                    { name: 'Administradora', index: 'Administradora', width: anchoP(ancho, 20) },
                    { name: 'id_cita', index: 'id_cita', hidden: true },
                    { name: 'fechaAdmision', index: 'fechaAdmision', width: anchoP(ancho, 10) },
                    { name: 'Observacion', index: 'Observacion', width: anchoP(ancho, 10) },
                    { name: 'usuarioCrea', index: 'usuarioCrea', width: anchoP(ancho, 10) },
                    { name: 'id_cuenta', index: 'id_cuenta', width: anchoP(ancho, 10) },
                    { name: 'EstadoCuenta', index: 'EstadoCuenta', width: anchoP(ancho, 5) },
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdAdmision', datosRow.ID, 0)
                    asignaAtributo('lblIdCuenta', datosRow.id_cuenta, 0)
                    asignaAtributo('lblEspecialidad', datosRow.Especialidad, 0)
                    asignaAtributo('lblFecha', datosRow.fechaAdmision, 0)
                    asignaAtributo('lblUsuarioCrea', datosRow.usuarioCrea, 0)
                    asignaAtributo('txtAdministradoraEdit', concatenarCodigoNombre(datosRow.idAdministradora, datosRow.Administradora), 0)
                    asignaAtributo('cmbTipoAdmisionEdit', datosRow.idTipoAdmision, 0)
                    mostrar('divVentanitaEditarAdmision')
                    // alert(88888888888)
                    limpiaAtributo('lblIdPlan', 0)
                    limpiaAtributo('cmbIdTipoAfiliado', 0)
                    limpiaAtributo('cmbIdRango', 0)
                    limpiaAtributo('txtIdProcedimientoCex', 0)
                    limpiaAtributo('cmbCantidad', 0)
                    limpiarListadosTotales('listProcedimientosDeFactura')
                    // limpiarListadosTotales('listAdmisionTotalFactura')   

                    buscarFacturacion('listProcedimientosDeFactura')
                    // setTimeout("buscarFacturacion('listAdmisionTotalFactura')",5);                        

                },
                height: 200,
                width: ancho - 50,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listCirugiaProcedimiDeLE':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=342" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'idSitioQuirur', 'nombre Sitio', 'idProcedimiento', 'Nombre Procedimiento', 'Observaciones'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', hidden: true },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 10) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', hidden: true },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 70) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 30) },
                ],
                height: 100,
                width: 800,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listCitaCirugiaProcedimientoTraer':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=60" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdListaEspera'));

            $('#drag' + ventanaActual.num).find("#listCitaCirugiaProcedimiento").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'idSitioQuirur', 'nombre SitioQuirur', 'idProcedimiento', 'Nombre Procedimiento', 'Observaciones'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', width: anchoP(ancho, 10) },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 20) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 10) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 40) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 20) },
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listCitaCirugiaProcedimiento').getRowData(rowid);
                    asignaAtributo('lblNomSitioQuirurgico', datosRow.nombreSitioQuirur, 0)
                    asignaAtributo('lblIdProcedimiento', datosRow.idProcedimiento, 0)
                    asignaAtributo('lblNomProcedimiento', datosRow.nombreProcedimiento, 0)
                    mostrar('divVentanitaEliminarCirugiaProcedimientos')

                },
                height: 100,
                width: 800,
            });
            $('#drag' + ventanaActual.num).find("#listCitaCirugiaProcedimiento").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;




        case 'listCitaCexProcedimientoTraer':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=106" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdListaEspera'));

            $('#drag' + ventanaActual.num).find("#listCitaCexProcedimientoTraer").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'idSitioQuirur', 'nombre SitioQuirur', 'idProcedimiento', 'Nombre Procedimiento', 'Observaciones'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', width: anchoP(ancho, 10) },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 20) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 15) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 60) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 60) }
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listCitaCexProcedimientoTraer').getRowData(rowid);

                    console.log('106')
                    jQuery('#listCitaCexProcedimientoTraer').delRowData(rowid);
                    //asignaAtributo('lblNomSitioQuirurgico', datosRow.nombreSitioQuirur, 0)
                    //asignaAtributo('lblIdProcedimiento', datosRow.idProcedimiento, 0)
                    //asignaAtributo('lblNomProcedimiento', datosRow.nombreProcedimiento, 0)
                    //mostrar('divVentanitaEliminarCirugiaProcedimientos')
                },
                height: 50,
                width: 1120,
            });
            $('#drag' + ventanaActual.num).find("#listCitaCexProcedimientoTraer").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listCitaAgendaProcedimientos':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=55&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));

            $('#drag' + ventanaActual.num).find("#listCitaCexProcedimientoTraer").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'ID Sitio', 'Nombre Sitio', 'ID Procedimiento', 'Nombre Procedimiento', 'Observaciones'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', width: anchoP(ancho, 10) },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 20) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 15) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 60) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 60) }
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listCitaCexProcedimientoTraer').getRowData(rowid);
                    // jQuery('#listCitaCexProcedimientoTraer').delRowData(rowid);
                    eliminarElementoGrilla('listCitaCexProcedimientoTraer', rowid);
                    //asignaAtributo('lblNomSitioQuirurgico', datosRow.nombreSitioQuirur, 0)
                    //asignaAtributo('lblIdProcedimiento', datosRow.idProcedimiento, 0)
                    //asignaAtributo('lblNomProcedimiento', datosRow.nombreProcedimiento, 0)
                    //mostrar('divVentanitaEliminarCirugiaProcedimientos')
                },
                height: 80,
                autowidth: true,
            });
            $('#drag' + ventanaActual.num).find("#listCitaCexProcedimientoTraer").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listAdmisionCEXProcedimientoTraer':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=573" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPlanContratacion'));
            add_valores_a_mandar(valorAtributo('cmbTipoAdmision'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#listCitaCirugiaProcedimiento").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'lateralidad', 'idSitioQuirur', 'nombre SitioQuirur', 'idProcedimiento', 'Nombre Procedimiento', 'Observaciones', 'Contratado', 'Precio', 'factor_lateralidad'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'lateralidad', index: 'lateralidad', width: anchoP(ancho, 6) },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', width: anchoP(ancho, 6) },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 20) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 10) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 40) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 20) },
                    { name: 'Contratado', index: 'Contratado', width: anchoP(ancho, 7) },
                    { name: 'Precio', index: 'Precio', width: anchoP(ancho, 6) },
                    { name: 'factor_lateralidad', index: 'factor_lateralidad', width: anchoP(ancho, 6) }
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listCitaCirugiaProcedimiento').getRowData(rowid);

                    if (valorAtributo('lblIdEstadoFactura') == 'P' || valorAtributo('lblIdEstadoFactura') == 'T' || valorAtributo('lblIdEstadoFactura') == 'D') {
                        asignaAtributo('txtIdProcedimientoCex', datosRow.idProcedimiento + '-' + datosRow.nombreProcedimiento, 0)
                        asignaAtributo('lblValorUnitarioProc', datosRow.Precio, 0)
                        $('#drag' + ventanaActual.num).find('#txtIdProcedimientoCex').focus();
                    } else
                        alert('NO HA SELECCIONADO ADMISION O ESTADO DE FACTURA NO LO PERMITE');

                },
                height: 100,
                width: 1190,
            });
            $('#drag' + ventanaActual.num).find("#listCitaCirugiaProcedimiento").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listEsperaCEXProcedimientoTraer':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=105&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPlanContratacion'));
            add_valores_a_mandar(valorAtributo('cmbTipoCita'));

            $('#drag' + ventanaActual.num).find("#listEsperaCEXProcedimientoTraer").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['c', 'idProcedimiento', 'Nombre Procedimiento', 'Contratado', 'Precio', 'factor_lateralidad'],
                colModel: [
                    { name: 'c', index: 'contador', hidden: true },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 10) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 40) },
                    { name: 'Contratado', index: 'Contratado', width: anchoP(ancho, 7) },
                    { name: 'Precio', index: 'Precio', width: anchoP(ancho, 6) },
                    { name: 'factor_lateralidad', index: 'factor_lateralidad', width: anchoP(ancho, 6) }
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listEsperaCEXProcedimientoTraer').getRowData(rowid);

                    var ids = jQuery("#listProcedimientoLE").getDataIDs();

                    for (var i = 0; i < ids.length; i++) {
                        var c = ids[i];
                        var datosRow2 = jQuery("#listProcedimientoLE").getRowData(c);

                        if (datosRow2.idProcedimiento == datosRow.idProcedimiento) {
                            eliminarElementoGrilla('listProcedimientoLE', c);
                        }
                    }

                    var ids = jQuery("#listProcedimientoLE").getDataIDs();
                    cant = ids.length;
                    proximo = cant + 1;
                    var datarow = {
                        tipo: '0',
                        idSitioQuirur: valorAtributo('cmbSitio'),
                        nombreSitioQuirur: valorAtributoCombo('cmbSitio'),
                        idProcedimiento: datosRow.idProcedimiento,
                        nombreProcedimiento: datosRow.nombreProcedimiento,
                        Observaciones: '',
                    };
                    var su = jQuery('#listProcedimientoLE').addRowData(proximo, datarow);

                },
                height: 50,
                width: 1170,
            });
            $('#drag' + ventanaActual.num).find("#listEsperaCEXProcedimientoTraer").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listProcedimientosAgenda':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=108&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
            add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
            add_valores_a_mandar(valorAtributo('lblIdPlanContratacion'));
            add_valores_a_mandar(valorAtributo('cmbTipoCita'));

            $('#drag' + ventanaActual.num).find("#listProcedimientosAgenda").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['c', 'id_procedimiento', 'id_plan', 'id_tipo_cita', 'Procedimiento', 'Precio', 'por_variabilidad',
                    'Maximo', 'Agendados', 'Facturados', 'le', 'sitio', 'Observacion'],
                colModel: [
                    { name: 'c', index: 'c', hidden: true },
                    { name: 'id_procedimiento', index: 'id_procedimiento', width: anchoP(ancho, 8) },
                    { name: 'id_plan', index: 'id_plan', hidden: true },
                    { name: 'id_tipo_cita', index: 'id_tipo_cita', hidden: true },
                    { name: 'procedimiento', index: 'procedimiento', width: anchoP(ancho, 20) },
                    { name: 'precio', index: 'precio', width: anchoP(ancho, 6) },
                    { name: 'por_variabilidad', index: 'por_variabilidad', hidden: true },
                    { name: 'maximo', index: 'maximo', width: anchoP(ancho, 6) },
                    { name: 'agendados', index: 'agendados', width: anchoP(ancho, 6) },
                    { name: 'facturados', index: 'facturados', width: anchoP(ancho, 6) },
                    { name: 'le', index: 'le', width: anchoP(ancho, 3) },
                    { name: 'sitio', index: 'sitio', width: anchoP(ancho, 8) },
                    { name: 'observacion', index: 'observacion', width: anchoP(ancho, 10) },
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listProcedimientosAgenda').getRowData(rowid);

                    var ids = jQuery("#listCitaCexProcedimientoTraer").getDataIDs();
                    for (var i = 0; i < ids.length; i++) {
                        var c = ids[i];
                        var datosRow2 = jQuery("#listCitaCexProcedimientoTraer").getRowData(c);

                        if (datosRow2.idProcedimiento == datosRow.id_procedimiento) {
                            eliminarElementoGrilla('listCitaCexProcedimientoTraer', c)
                            //jQuery('#listCitaCexProcedimientoTraer').delRowData(c);
                            break;
                        }

                    }

                    var ids = jQuery("#listCitaCexProcedimientoTraer").getDataIDs();
                    cant = ids.length;
                    proximo = cant + 1;
                    var datarow = {
                        tipo: '0',
                        idSitioQuirur: valorAtributo('cmbIdSitio'),
                        nombreSitioQuirur: valorAtributoCombo('cmbIdSitio'),
                        idProcedimiento: datosRow.id_procedimiento,
                        nombreProcedimiento: datosRow.procedimiento,
                        Observaciones: datosRow.observacion,
                    };
                    var su = jQuery('#listCitaCexProcedimientoTraer').addRowData(proximo, datarow);
                },
                height: 80,
                autowidth: true,
            });
            $('#drag' + ventanaActual.num).find("#listProcedimientosAgenda").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listProcedimientosAgendaCirugia':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=109" + "&parametros=";

            add_valores_a_mandar(valorAtributo('lblIdPlanContratacion'));
            add_valores_a_mandar(valorAtributo('lblIdPlanContratacion'));
            add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
            add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
            sacarValoresColumnasDeGrilla('listCitaCirugiaProcedimiento');
            add_valores_a_mandar(valorAtributo('txtColumnaIdProced'));

            $('#drag' + ventanaActual.num).find("#listProcedimientosAgendaCirugia").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['c', 'id_procedimiento', 'id_plan', 'Procedimiento', 'Precio', 'Contratado', 'por_variabilidad',
                    'Maximo', 'Agendados', 'Facturados', 'le', 'sitio', 'Observacion'],
                colModel: [
                    { name: 'c', index: 'c', hidden: true },
                    { name: 'id_procedimiento', index: 'id_procedimiento', width: anchoP(ancho, 8) },
                    { name: 'id_plan', index: 'id_plan', hidden: true },
                    { name: 'procedimiento', index: 'procedimiento', width: anchoP(ancho, 20) },
                    { name: 'precio', index: 'precio', width: anchoP(ancho, 6) },
                    { name: 'contratado', index: 'contratado', width: anchoP(ancho, 6) },
                    { name: 'por_variabilidad', index: 'por_variabilidad', hidden: true },
                    { name: 'maximo', index: 'maximo', width: anchoP(ancho, 5) },
                    { name: 'agendados', index: 'agendados', width: anchoP(ancho, 6) },
                    { name: 'facturados', index: 'facturados', width: anchoP(ancho, 6) },
                    { name: 'le', index: 'le', width: anchoP(ancho, 3) },
                    { name: 'sitio', index: 'sitio', width: anchoP(ancho, 8) },
                    { name: 'observacion', index: 'observacion', width: anchoP(ancho, 10) },
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listProcedimientosAgendaCirugia').getRowData(rowid);


                },
                height: 50,
                width: 1000,
            });
            $('#drag' + ventanaActual.num).find("#listProcedimientosAgendaCirugia").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listAdmisionCirugiaProcedimientoTraer':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=387" + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
            add_valores_a_mandar(valorAtributo('lblIdCita'));
            $('#drag' + ventanaActual.num).find("#listCitaCirugiaProcedimiento").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'lateralidad', 'idSitioQuirur', 'nombre SitioQuirur', 'idProcedimiento', 'Nombre Procedimiento', 'Observaciones', 'Contratado', 'Precio', 'factor_lateralidad', 'precioXFactor'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'lateralidad', index: 'lateralidad', width: anchoP(ancho, 5) },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', width: anchoP(ancho, 5) },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 20) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 10) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 40) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 20) },
                    { name: 'Contratado', index: 'Contratado', width: anchoP(ancho, 7) },
                    { name: 'Precio', index: 'Precio', width: anchoP(ancho, 6) },
                    { name: 'factor_lateralidad', index: 'factor_lateralidad', width: anchoP(ancho, 6) },
                    { name: 'precioXFactor', index: 'precioXFactor', width: anchoP(ancho, 6) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listCitaCirugiaProcedimiento').getRowData(rowid);
                    asignaAtributo('txtIdProcedimientoCex', datosRow.idProcedimiento + '-' + datosRow.nombreProcedimiento, 0)
                    $('#drag' + ventanaActual.num).find('#txtIdProcedimientoCex').focus();
                },
                height: 100,
                width: 1150,
            });
            $('#drag' + ventanaActual.num).find("#listCitaCirugiaProcedimiento").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listADMISIONESCirugiaProcedimientoTraer':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=606" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPlanContratacion'));
            add_valores_a_mandar(valorAtributo('lblIdCita'));
            $('#drag' + ventanaActual.num).find("#listCitaCirugiaProcedimiento").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'lateralidad', 'idSitioQuirur', 'nombre SitioQuirur', 'idProcedimiento', 'Nombre Procedimiento', 'Observaciones', 'Contratado', 'Precio', 'factor_lateralidad'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'lateralidad', index: 'lateralidad', width: anchoP(ancho, 6) },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', width: anchoP(ancho, 6) },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 10) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 10) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 40) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 10) },
                    { name: 'Contratado', index: 'Contratado', width: anchoP(ancho, 7) },
                    { name: 'Precio', index: 'Precio', width: anchoP(ancho, 6) },
                    { name: 'factor_lateralidad', index: 'factor_lateralidad', width: anchoP(ancho, 6) },
                    // {name: 'precioXFactor', index: 'precioXFactor', width: anchoP(ancho, 6)},
                ],

                onSelectRow: function (rowid) {
                    if (valorAtributo('lblIdEstadoFactura') == 'P' || valorAtributo('lblIdEstadoFactura') == 'T') {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#listCitaCirugiaProcedimiento').getRowData(rowid)
                        asignaAtributo('txtIdProcedimientoCex', datosRow.idProcedimiento + '-' + datosRow.nombreProcedimiento, 0)
                        asignaAtributo('lblValorUnitarioProc', datosRow.Precio, 0)
                        $('#drag' + ventanaActual.num).find('#txtIdProcedimientoCex').focus();
                    } else
                        alert('NO HA SELECCIONADO ADMISION O ESTADO DE FACTURA NO LO PERMITE')
                },
                height: 100,
                width: 1150,
            });
            $('#drag' + ventanaActual.num).find("#listCitaCirugiaProcedimiento").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listCitaCirugiaProcedimiento':
            limpiarListadosTotales('listCitaCirugiaProcedimiento');
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=335" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'idSitioQuirur', 'nombre SitioQuirur', 'idProcedimiento', 'Nombre Procedimiento', 'Observaciones', 'id'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', hidden: true },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 10) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 8) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 60) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 15) },
                    { name: 'id', index: 'id', hidden: true },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0)
                    asignaAtributo('lblNomSitioQuirurgico', datosRow.nombreSitioQuirur, 0)
                    asignaAtributo('lblIdProcedimiento', datosRow.idProcedimiento, 0)
                    asignaAtributo('lblNomProcedimiento', datosRow.nombreProcedimiento, 0)
                    asignaAtributo('txtObservacionProc', datosRow.Observaciones, 0)
                    mostrar('divVentanitaEliminarCirugiaProcedimientos')
                },
                height: 50,
                width: 1000,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listListaEsperaProcedimiento':
            limpiarListadosTotales('listListaEsperaProcedimiento');
            ancho = 900;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=331" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblListaEspera'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'IdLEP', 'Id Lista', 'idSitioQuirur', 'nombre SitioQuirur', 'idProcedimiento', 'Nombre Procedimiento', 'Observaciones'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'IdLEP', index: 'IdLEP', width: anchoP(ancho, 5) },
                    { name: 'IdLista', index: 'IdLista', width: anchoP(ancho, 5) },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', width: anchoP(ancho, 10) },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 20) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 10) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 35) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 20) },
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblListaEsperaProcedimiento', datosRow.IdLEP, 0)
                    asignaAtributo('lblSitioQuirurgico', datosRow.nombreSitioQuirur, 0)
                    asignaAtributo('lblLEProcedimiento', datosRow.nombreProcedimiento, 0)
                    mostrar('divVentanitaListaEsperaProcedimientos')

                },
                height: 300,
                autowidth: true,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listCitaCirugiaProcedimientoLE':
            //limpiarListadosTotales('listListaEsperaProcedimiento');
            ancho = ($('#drag' + ventanaActual.num).find("#divListaEsperaProcedimientosLE").width() - 50);

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=335" + "&parametros=";
            //add_valores_a_mandar(valorAtributo('lblListaEspera'));alert(3333)
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'idSitioQuirur', 'nombre SitioQuirur', 'idProcedimiento', 'Nombre Procedimiento', 'Observaciones'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', width: anchoP(ancho, 10) },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 20) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 10) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 40) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 20) },
                ],
                height: 50,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listProcedimientoLE':
            //limpiarListadosTotales('listListaEsperaProcedimiento');
            ancho = ($('#drag' + ventanaActual.num).find("#divListaEsperaProcedimientosLE").width());

            valores_a_mandar = pag;
            //valores_a_mandar = valores_a_mandar + "?idQuery=" + "&parametros=";
            //add_valores_a_mandar(valorAtributo('lblListaEspera'));alert(3333)
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'ID Sitio', 'Nombre Sitio', 'ID Procedimiento', 'Nombre Procedimiento', 'Observaciones'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', width: anchoP(ancho, 10) },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 20) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 10) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 40) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 20) },
                ],
                height: 50,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listProcedimientoLE').getRowData(rowid);
                    jQuery('#listProcedimientoLE').delRowData(rowid);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listCitaCirugiaProcedimientoLEGestion':
            limpiarListadosTotales('listListaEsperaProcedimiento');
            ancho = ($('#drag' + ventanaActual.num).find("#divListaEsperaProcedimientosLEGestion").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=381&parametros=";
            add_valores_a_mandar(valorAtributo('cmbClaseBus'));
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));
            add_valores_a_mandar(valorAtributo('txtFechaDesdeP'));
            add_valores_a_mandar(valorAtributo('txtFechaHastaP'));
            add_valores_a_mandar(valorAtributo('cmbEstadoBus'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', 'idSitioQuirur', 'nombre Sitio', 'idProcedimiento', 'Nombre Procedimiento', 'Observaciones', 'ID'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'idSitioQuirur', index: 'idSitioQuirur', hidden: true },
                    { name: 'nombreSitioQuirur', index: 'nombreSitioQuirur', width: anchoP(ancho, 8) },
                    { name: 'idProcedimiento', index: 'idProcedimiento', width: anchoP(ancho, 8) },
                    { name: 'nombreProcedimiento', index: 'nombreProcedimiento', width: anchoP(ancho, 45) },
                    { name: 'Observaciones', index: 'Observaciones', width: anchoP(ancho, 20) },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 2) },
                ],
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listHistoricoListaEsperaCirugia':
            limpiarDivEditarJuan('listHistoricoListaEspera');
            ancho = ($('#drag' + ventanaActual.num).find("#divContenidoListaEsperaHist").width()) - 60;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=28" + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            $('#drag' + ventanaActual.num).find("#listHistoricoListaEspera").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id lista', 'FechaIngreso', 'Especialidad', 'idMedico', 'nombreMedico', 'idEstadoL', 'EstadoL',
                    'noAutorizacion', 'fechaVigencia', 'Observacion Lista', 'idGradoNecesidad', 'Grado Necesidad', 'usuarioElaboro', 'id_administradora', 'Administradora', 'espera', 'CantProcedimientos', 'id_plan', 'plan', 'id_regimen', 'regimen',
                    '-', 'id cita', 'Estado', 'tipo', 'FechaCita', 'Especialidad', 'Profesional', 'ObservacionCita'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'idlista', index: 'idlista', hidden: true },
                    { name: 'Fechalista', index: 'Fechalista', width: anchoP(ancho, 10) },
                    { name: 'Especialidad', index: 'Especialidad', width: anchoP(ancho, 10) },
                    //{ name: 'SubEspecialidad', index: 'SubEspecialidad', width: anchoP(ancho, 10) },
                    { name: 'idMedico', index: 'idMedico', hidden: true },
                    { name: 'nombreMedico', index: 'nombreMedico', width: anchoP(ancho, 10) },
                    { name: 'idEstadoL', index: 'idEstadoL', hidden: true },
                    { name: 'EstadoLista', index: 'EstadoLista', width: anchoP(ancho, 7) },
                    { name: 'noAutorizacion', index: 'noAutorizacion', hidden: true },
                    { name: 'fechaVigencia', index: 'fechaVigencia', hidden: true },
                    { name: 'Observacion', index: 'Observacion', width: anchoP(ancho, 6) },
                    { name: 'idGradoNece', index: 'idGradoNece', hidden: true },
                    { name: 'NOMBRE_GRADO_NECESIDAD', index: 'NOMBRE_GRADO_NECESIDAD', width: anchoP(ancho, 6) },
                    { name: 'usuarioElaboro', index: 'usuarioElaboro', hidden: true },
                    { name: 'id_administradora', index: 'id_administradora', hidden: true },
                    { name: 'administradora', index: 'administradora', hidden: true },
                    { name: 'esperar', index: 'esperar', hidden: true },
                    { name: 'CantProcedimientos', index: 'procedimientos', width: anchoP(ancho, 4) },
                    { name: 'id_plan', index: 'id_plan', hidden: true },
                    { name: 'plan', index: 'plan', hidden: true },
                    { name: 'id_regimen', index: 'id_regimen', hidden: true },
                    { name: 'regimen', index: 'regimen', hidden: true },

                    { name: 'separa', index: 'separa', width: anchoP(ancho, 1) },

                    { name: 'idCita', index: 'idCita', hidden: true },
                    { name: 'Estado', index: 'Estado', width: anchoP(ancho, 6) },
                    { name: 'tipo', index: 'tipo', width: anchoP(ancho, 4) },
                    { name: 'fechaCita', index: 'fechaCita', width: anchoP(ancho, 10) },
                    { name: 'EspecialidadCita', index: 'EspecialidadCita', width: anchoP(ancho, 12) },
                    { name: 'Profesional', index: 'Profesional', width: anchoP(ancho, 12) },
                    { name: 'ObservacionCita', index: 'ObservacionCita', width: anchoP(ancho, 12) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listHistoricoListaEspera').getRowData(rowid);
                    if (datosRow.idCita == '') {
                        mostrar('divVentanitaEliminarListaEspera')
                        asignaAtributo('lblListaEspera', datosRow.idlista, 0)
                        asignaAtributo('lblEspecialidad', datosRow.Especialidad, 0)
                        // asignaAtributo('lblSubEspecialidad', datosRow.SubEspecialidad, 0)
                        asignaAtributo('lblUsuarioElaboro', datosRow.usuarioElaboro, 0)
                        asignaAtributo('txtNoAutorizacionLE', datosRow.noAutorizacion, 0)
                        asignaAtributo('txtFechaVigenciaLE', datosRow.fechaVigencia, 0)
                        asignaAtributo('txtObservacionLE', datosRow.Observacion, 0)
                        asignaAtributo('lblIdAdministradora', datosRow.id_administradora, 0)
                        asignaAtributo('cmbEstadoLEMod', datosRow.idEstadoL, 0)
                        asignaAtributo('lblAdministradora', datosRow.administradora, 0)
                        asignaAtributo('lblLEEsperar', datosRow.esperar, 0)
                        asignaAtributo('cmbGradoNecesidadMod', datosRow.idGradoNece, 0)
                        limpiaAtributo('cmbIdSitioQuirurgico', 0)
                        limpiaAtributo('txtIdProcedimiento', 0)

                        asignaAtributo('txtAdministradoraLE', datosRow.id_administradora + ' - ' + datosRow.administradora, 0)
                        asignaAtributoCombo('cmbIdTipoRegimenLE', datosRow.id_regimen, datosRow.regimen);

                        asignaAtributo('lblIdPlanContratacionLE', datosRow.id_plan, 0)
                        asignaAtributo('lblNomPlanContratacionLE', datosRow.plan, 0)

                        limpiaAtributo('txtObservacionLEsp', 0)
                        buscarAGENDA('listListaEsperaProcedimiento')
                    } else
                        alert('ESTA LISTA DE ESPERA YA TIENE ASIGNADO UNA CITA')
                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#listHistoricoListaEspera").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listHistoricoListaEspera':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#divContenidoListaEsperaHist").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=316&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id lista', 'FechaIngreso', 'Especialidad', 'idMedico', 'nombreMedico', 'idEstadoL', 'EstadoL', 'noAutorizacion', 'fechaVigencia', 'Observacion Lista', 'idGradoNecesidad', 'Grado Necesidad', 'usuarioElaboro', 'id_administradora', 'Administradora', 'esperar', 'id_plan', 'plan', 'id_regimen', 'regimen',
                    '-', 'id cita', 'Estado', 'tipo', 'FechaCita', 'Especialidad', 'Profesional', 'ObservacionCita'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'idlista', index: 'idlista', hidden: true },
                    { name: 'Fechalista', index: 'Fechalista', width: anchoP(ancho, 10) },
                    { name: 'Especialidad', index: 'Especialidad', width: anchoP(ancho, 12) },
                    { name: 'idMedico', index: 'idMedico', hidden: true },
                    { name: 'nombreMedico', index: 'nombreMedico', width: anchoP(ancho, 10) },
                    { name: 'idEstadoL', index: 'idEstadoL', hidden: true },
                    { name: 'EstadoLista', index: 'EstadoLista', width: anchoP(ancho, 7) },
                    { name: 'noAutorizacion', index: 'noAutorizacion', hidden: true },
                    { name: 'fechaVigencia', index: 'fechaVigencia', hidden: true },
                    { name: 'Observacion', index: 'Observacion', width: anchoP(ancho, 6) },
                    { name: 'idGradoNece', index: 'idGradoNece', hidden: true },
                    { name: 'NOMBRE_GRADO_NECESIDAD', index: 'NOMBRE_GRADO_NECESIDAD', width: anchoP(ancho, 6) },
                    { name: 'usuarioElaboro', index: 'usuarioElaboro', hidden: true },
                    { name: 'id_administradora', index: 'id_administradora', hidden: true },
                    { name: 'administradora', index: 'administradora', hidden: true },
                    { name: 'esperar', index: 'esperar', hidden: true },
                    { name: 'id_plan', index: 'id_plan', hidden: true },
                    { name: 'plan', index: 'plan', hidden: true },
                    { name: 'id_regimen', index: 'id_regimen', hidden: true },
                    { name: 'regimen', index: 'regimen', hidden: true },

                    { name: 'separa', index: 'separa', width: anchoP(ancho, 1) },

                    { name: 'idCita', index: 'idCita', hidden: true },
                    { name: 'Estado', index: 'Estado', width: anchoP(ancho, 6) },
                    { name: 'tipo', index: 'tipo', width: anchoP(ancho, 4) },
                    { name: 'fechaCita', index: 'fechaCita', width: anchoP(ancho, 10) },
                    { name: 'EspecialidadCita', index: 'EspecialidadCita', width: anchoP(ancho, 12) },
                    { name: 'Profesional', index: 'Profesional', width: anchoP(ancho, 12) },
                    { name: 'ObservacionCita', index: 'ObservacionCita', width: anchoP(ancho, 12) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    if (datosRow.idCita == '') {
                        mostrar('divVentanitaEliminarListaEspera')
                        asignaAtributo('lblListaEspera', datosRow.idlista, 0)
                        asignaAtributo('lblEspecialidad', datosRow.Especialidad, 0)
                        asignaAtributo('lblUsuarioElaboro', datosRow.usuarioElaboro, 0)
                        asignaAtributo('txtNoAutorizacionLE', datosRow.noAutorizacion, 0)
                        asignaAtributo('txtFechaVigenciaLE', datosRow.fechaVigencia, 0)
                        asignaAtributo('txtObservacionLE', datosRow.Observacion, 0)
                        asignaAtributo('lblIdAdministradora', datosRow.id_administradora, 0)
                        asignaAtributo('cmbEstadoLEMod', datosRow.idEstadoL, 0)

                        asignaAtributo('cmbGradoNecesidadMod', datosRow.idGradoNece, 0)
                        asignaAtributo('lblLEEsperar', datosRow.esperar, 0)

                        asignaAtributo('txtAdministradoraLE', datosRow.id_administradora + ' - ' + datosRow.administradora, 0)
                        asignaAtributoCombo('cmbIdTipoRegimenLE', datosRow.id_regimen, datosRow.regimen);

                        asignaAtributo('lblIdPlanContratacionLE', datosRow.id_plan, 0)
                        asignaAtributo('lblNomPlanContratacionLE', datosRow.plan, 0)
                        limpiaAtributo('cmbIdSitioQuirurgico', 0)
                        limpiaAtributo('txtIdProcedimiento', 0)
                        limpiaAtributo('txtObservacionLEsp', 0)

                        buscarAGENDA('listListaEsperaProcedimiento')
                    } else
                        alert('ESTA LISTA DE ESPERA YA TIENE ASIGNADO UNA CITA')
                },
                height: 150,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listHistoricoListaEsperaEnAgenda':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#divContenidoListaEsperaHist").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=15&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id lista', 'FechaIngreso', 'idEspecialidad', 'Especialidad', 'ID_TIPO_CITA',
                    'TIPO_CITA', 'idMedico', 'nombreMedico', 'idEstadoL', 'EstadoL',
                    'Observacion Lista', 'idGradoNecesidad', 'Grado Necesidad', 'usuarioElaboro',
                    'id_administradora', 'Administradora', 'id_intitucion_remite', 'ID_PLAN', 'PLAN', 'ID_REGIMEN', 'REGIMEN',
                    '- ', 'id cita', 'Estado', 'tipo', 'FechaCita', 'Especialidad', 'Profesional', 'ObservacionCita'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'idlista', index: 'idlista', hidden: true },
                    { name: 'Fechalista', index: 'Fechalista', width: anchoP(ancho, 10) },
                    { name: 'idEspecialidad', index: 'idEspecialidad', hidden: true },
                    { name: 'Especialidad', index: 'Especialidad', width: anchoP(ancho, 12) },
                    //{ name: 'idSubEspecialidad', index: 'idSubEspecialidad', hidden: true },
                    //{ name: 'SubEspecialidad', index: 'SubEspecialidad', width: anchoP(ancho, 12) },
                    { name: 'ID_TIPO_CITA', index: 'ID_TIPO_CITA', hidden: true },
                    { name: 'TIPO_CITA', index: 'TIPO_CITA', width: anchoP(ancho, 2) },

                    { name: 'idMedico', index: 'idMedico', hidden: true },
                    { name: 'nombreMedico', index: 'nombreMedico', width: anchoP(ancho, 10) },
                    { name: 'idEstadoL', index: 'idEstadoL', hidden: true },
                    { name: 'EstadoLista', index: 'EstadoLista', width: anchoP(ancho, 7) },
                    { name: 'Observacion', index: 'Observacion', width: anchoP(ancho, 6) },
                    { name: 'idGradoNece', index: 'idGradoNece', hidden: true },
                    { name: 'NOMBRE_GRADO_NECESIDAD', index: 'NOMBRE_GRADO_NECESIDAD', width: anchoP(ancho, 6) },
                    { name: 'usuarioElaboro', index: 'usuarioElaboro', hidden: true },
                    { name: 'id_administradora', index: 'id_administradora', hidden: true },
                    { name: 'administradora', index: 'administradora', hidden: true },
                    { name: 'id_intitucion_remite', index: 'id_intitucion_remite', hidden: true },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'PLAN', index: 'PLAN', hidden: true },
                    { name: 'ID_REGIMEN', index: 'ID_REGIMEN', hidden: true },
                    { name: 'REGIMEN', index: 'REGIMEN', hidden: true },

                    { name: 'separa', index: 'separa', width: anchoP(ancho, 1) },

                    { name: 'idCita', index: 'idCita', hidden: true },
                    { name: 'Estado', index: 'Estado', width: anchoP(ancho, 6) },
                    { name: 'tipo', index: 'tipo', width: anchoP(ancho, 4) },
                    { name: 'fechaCita', index: 'fechaCita', width: anchoP(ancho, 10) },
                    { name: 'EspecialidadCita', index: 'EspecialidadCita', width: anchoP(ancho, 12) },
                    { name: 'Profesional', index: 'Profesional', width: anchoP(ancho, 12) },
                    { name: 'ObservacionCita', index: 'ObservacionCita', width: anchoP(ancho, 12) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    if (datosRow.idCita == '') {

                        //mostrar('divVentanitaEliminarListaEspera')
                        if (valorAtributo('cmbIdEspecialidad') == datosRow.idEspecialidad) {
                            if (datosRow.idEstadoL != '5') {
                                asignaAtributo('lblIdListaEspera', datosRow.idlista, 0)
                                asignaAtributo('txtAdministradora1', concatenarCodigoNombre(datosRow.id_administradora, datosRow.administradora), 0)
                                asignaAtributoCombo('cmbTipoCita', datosRow.ID_TIPO_CITA, datosRow.TIPO_CITA)
                                asignaAtributo('txtIPSRemite', datosRow.id_intitucion_remite, 0)
                                buscarInformacionBasicaPaciente()
                                ocultar('divVentanitaPacienteYaExiste')

                                asignaAtributoCombo('cmbIdTipoRegimen', datosRow.ID_REGIMEN, datosRow.REGIMEN);
                                asignaAtributo('lblIdPlanContratacion', datosRow.ID_PLAN, 0)
                                asignaAtributo('lblNomPlanContratacion', datosRow.PLAN, 0)
                                asignaAtributo('txtIPSRemite', '0000-OTRO::PASTO', 0)
                                asignaAtributoCombo('cmbIdPlan', datosRow.ID_PLAN, datosRow.PLAN)

                                if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
                                    alert('SE TRAERAN LOS PROCEDIMIENTOS DE LA LISTA DE ESPERA')
                                    // modificarCRUD('traerProcediDeListaEsperaACirugia','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
                                    buscarAGENDA('listCitaCirugiaProcedimientoTraer')
                                    limpiaAtributo('cmbIdSitioQuirurgico', 0);
                                    limpiaAtributo('txtIdProcedimiento', 0);
                                    limpiaAtributo('txtObservacionProcedimientoCirugia', 0);

                                } else {
                                    setTimeout("buscarAGENDA('listProcedimientosAgenda')", 1000);
                                    setTimeout("buscarAGENDA('listCitaAgendaProcedimientos')", 500);
                                }

                            } else
                                alert('NO PUEDE PORQUE LA LISTA ESPERA ESTA ELIMINADA')
                        } else
                            alert('No puede asignar Lista de Espera de otra Especialidad')
                    } else
                        alert('ESTA LISTA DE ESPERA YA TIENE ASIGNADO UNA CITA.')
                },
                height: 150,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listAgenda':
            // ancho= 700;  
            document.getElementById('divListAgenda').innerHTML = '<table id="listAgenda" class="scroll"></table>';
            ancho = ($('#drag' + ventanaActual.num).find("#divListAgenda").width()) - 5;
            if (valorAtributo('cmbIdProfesionales') != '') {
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=427&parametros=";
                add_valores_a_mandar(valorAtributo('cmbExito'));
                add_valores_a_mandar(valorAtributo('cmbSede'));
                add_valores_a_mandar(valorAtributo('lblFechaSeleccionada'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                $('#drag' + ventanaActual.num).find("#listAgenda").jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['contador', 'ID', 'FECHA_CITA', 'HORA_CITA', 'FECHA_HORA_CITA', 'MIN_DURACION',
                        'ID PACIENTE', 'ID HC', 'TIPO_ID', 'IDENTIFICACION', 'NOMBRE PACIENTE', 'TELEFONO',
                        'ID_AGENDA', 'ID_ELABORO', 'FECHA_ELABORO', 'ID_ESTADO_AGENDA', 'TIPO', 'Tipo', 'ID_MOTIVO',
                        'ID_ADMINISTRADORA', 'NOM_ADMINISTRADORA', 'NO_AUTORIZACION', 'FECHA_VIGENCIA', 'FECHA_PACIENTE',
                        'ID_INSTITU_REMITE', 'NOM_INSTITU_REMITE', 'OBSERVACION', 'OBSERVACION_CONFIRMA', 'ID_ESTADO', 'ESTADO',
                        'ID_LISTA_ESPERA', 'NO_LENTE_INTRAOCULAR', 'OBSERVACION_FOLIO', 'ID_TIPO_ANESTECIA', 'ANESTECIOLOGO', 'INSTRUMENTADOR',
                        'CIRCULANTE', 'ID_MOTIVO_CONSULTA_CLASE', 'MOTIVO_CONSULTA', 'SITIO', 'ID_PLAN',
                        'ID_TIPO_REGIMEN', 'REGIMEN', 'PLAN', 'ID_CONSULTORIO', 'CONSULTORIO', 'ID_PERSONAL', 'NOMBRE_PERSONAL', 'ID_ESPECIALIDAD',
                        'ID_SEDE', 'HORA', 'MINUTO', 'PERIODO'
                    ],
                    colModel: [
                        { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                        { name: 'ID', index: 'ID', hidden: true },
                        { name: 'FECHA_CITA', index: 'FECHA_CITA', hidden: true },
                        { name: 'HORA_CITA', index: 'HORA_CITA', width: anchoP(ancho, 4) },
                        { name: 'FECHA_HORA_CITA', index: 'FECHA_HORA_CITA', hidden: true },
                        { name: 'DURACION', index: 'DURACION', width: anchoP(ancho, 2) },
                        { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                        { name: 'ID_HC', index: 'ID_HC', hidden: true },
                        { name: 'TIPO_ID', index: 'TIPO_ID', width: anchoP(ancho, 2) },
                        { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 7) },

                        { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: anchoP(ancho, 19) },
                        { name: 'TELEFONO', index: 'TELEFONO', width: anchoP(ancho, 8) },
                        { name: 'ID_AGENDA', index: 'ID_AGENDA', hidden: true },
                        { name: 'ID_ELABORO', index: 'ID_ELABORO', hidden: true },
                        { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', hidden: true },
                        { name: 'ID_ESTADO_AGENDA', index: 'ID_ESTADO_AGENDA', hidden: true },
                        { name: 'TIPO', index: 'TIPO', hidden: true },
                        { name: 'tipo_cita', index: 'tipo_cita', width: anchoP(ancho, 10) },

                        { name: 'ID_MOTIVO', index: 'ID_MOTIVO', hidden: true },
                        { name: 'ID_ADMINISTRADORA', index: 'ID_ADMINISTRADORA', hidden: true },
                        { name: 'NOM_ADMINISTRADORA', index: 'NOM_ADMINISTRADORA', hidden: true },
                        { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', hidden: true },
                        { name: 'FECHA_VIGENCIA', index: 'FECHA_VIGENCIA', hidden: true },
                        { name: 'FECHA_PACIENTE', index: 'FECHA_PACIENTE', hidden: true },
                        { name: 'ID_INSTITU_REMITE', index: 'ID_INSTITU_REMITE', hidden: true },
                        { name: 'NOM_INSTITU_REMITE', index: 'NOM_INSTITU_REMITE', hidden: true },
                        { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 6) },
                        { name: 'OBSERVACION_CONFIRMA', index: 'OBSERVACION_CONFIRMA', hidden: true },
                        { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                        { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 6) },
                        { name: 'ID_LISTA_ESPERA', index: 'ID_LISTA_ESPERA', hidden: true },

                        { name: 'NO_LENTE_INTRAOCULAR', index: 'NO_LENTE_INTRAOCULAR', hidden: true },
                        { name: 'OBSERVACION_FOLIO', index: 'OBSERVACION_FOLIO', hidden: true },
                        { name: 'ID_TIPO_ANESTECIA', index: 'ID_TIPO_ANESTECIA', hidden: true },
                        { name: 'ANESTECIOLOGO', index: 'ANESTECIOLOGO', hidden: true },
                        { name: 'INSTRUMENTADOR', index: 'INSTRUMENTADOR', hidden: true },
                        { name: 'CIRCULANTE', index: 'CIRCULANTE', hidden: true },
                        { name: 'ID_MOTIVO_CONSULTA_CLASE', index: 'ID_MOTIVO_CONSULTA_CLASE', hidden: true },
                        { name: 'MOTIVO_CONSULTA', index: 'MOTIVO_CONSULTA', hidden: true },
                        { name: 'SITIO', index: 'SITIO', hidden: true },
                        { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                        { name: 'ID_TIPO_REGIMEN', index: 'ID_TIPO_REGIMEN', hidden: true },
                        { name: 'REGIMEN', index: 'REGIMEN', hidden: true },
                        { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 4) },
                        { name: 'ID_CONSULTORIO', index: 'ID_CONSULTORIO', hidden: true },
                        { name: 'CONSULTORIO', index: 'CONSULTORIO', hidden: true },
                        { name: 'ID_PERSONAL', index: 'ID_PERSONAL', hidden: true },
                        { name: 'NOMBRE_PERSONAL', index: 'NOMBRE_PERSONAL', hidden: true },
                        { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                        { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                        { name: 'HORA', index: 'HORA', hidden: true },
                        { name: 'MINUTO', index: 'MINUTO', hidden: true },
                        { name: 'PERIODO', index: 'PERIODO', hidden: true },
                    ],
                    height: 900,
                    width: ancho,
                    multiselect: $('#copiarCitas').attr('checked'),
                    onCellSelect: function (rowid, col, content, event) {
                        $("#tabsCoordinacionTerapia").tabs("option", "selected", 0);

                        if (!$('#copiarCitas').attr('checked')) {
                            if (col != 0) {
                                var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                                limpiarDivEditarJuan('camposDivVentanitaAgenda')

                                limpiaAtributo('cmbIdSitio', 0)

                                if (datosRow.ID_ESTADO == 'C' || datosRow.ID_ESTADO == 'D' || datosRow.ID_ESTADO == 'A') {
                                    asignaAtributo('txtIdBusPaciente', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.TIPO_ID + datosRow.IDENTIFICACION + ' ' + datosRow.NOMBRE_PACIENTE), 0)
                                    if (valorAtributo('txtIdBusPaciente') != '') {
                                        setTimeout(() => {
                                            buscarInformacionBasicaPaciente();
                                        }, 200);
                                    }
                                    asignaAtributo('lblUsuariosDato', datosRow.ID_ELABORO, 0)
                                    asignaAtributo('lblIdAgendaDetalle', datosRow.ID, 0)
                                    asignaAtributo('lblHora', datosRow.HORA_CITA, 0)
                                    asignaAtributo('lblDuracion', datosRow.DURACION, 0)
                                    asignaAtributo('lblFechaCita', datosRow.FECHA_CITA, 0)
                                    asignaAtributo('lblConsultorio', datosRow.CONSULTORIO)
                                    asignaAtributoCombo('cmbTipoCita', datosRow.TIPO, datosRow.tipo_cita)
                                    asignaAtributo('cmbEstadoCita', datosRow.ID_ESTADO, 0)
                                    asignaAtributo('txtEstadoCita', datosRow.ID_ESTADO, 0)
                                    asignaAtributoCombo2('cmbMotivoConsulta', datosRow.ID_MOTIVO, datosRow.MOTIVO_CONSULTA)
                                    asignaAtributo('cmbMotivoConsultaClase', datosRow.ID_MOTIVO_CONSULTA_CLASE, 0)
                                    asignaAtributo('txtTelefonos', datosRow.TELEFONO, 0)
                                    asignaAtributo('txtObservacionFolio', datosRow.OBSERVACION_FOLIO, 0)

                                    asignaAtributo('txtAdministradora1', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.NOM_ADMINISTRADORA), 0);
                                    asignaAtributo('txtNoAutorizacion', '00000', 0)
                                    asignaAtributo('txtFechaVigencia', '22/10/2021', 0)
                                    asignaAtributo('txtFechaPacienteCita', '22/10/2021', 0)
                                    asignaAtributo('txtIPSRemite', '0000-OTRO::', 0)
                                    asignaAtributo('cmbIdSitioQuirurgico', datosRow.SITIO, 0)

                                    asignaAtributoCombo('cmbIdTipoRegimen', 'C-0', 'CONTRIBUTIVO - EVENTO');
                                    asignaAtributo('lblIdPlanContratacion', '1', 0)
                                    asignaAtributo('lblNomPlanContratacion', 'xxxx', 0)
                                    asignaAtributoCombo("cmbIdPlan", '1',"1-ccc")


                                    if (datosRow.ID_LISTA_ESPERA == '0')
                                        asignaAtributo('lblIdListaEspera', '', 0)
                                    else
                                        asignaAtributo('lblIdListaEspera', datosRow.ID_LISTA_ESPERA, 0)

                                    asignaAtributo('txtObservacionConfirma', datosRow.OBSE0VACION_CONFIRMA, 0)
                                    asignaAtributo('txtNoLente', '', 0)
                                    asignaAtributo('cmbTipoAnestesia', datosRow.ID_TIPO_ANESTECIA, 0)
                                    asignaAtributo('txtAnestesiologo', datosRow.ANESTECIOLOGO, 0)
                                    asignaAtributo('txtInstrumentador', datosRow.INSTRUMENTADOR, 0)
                                    asignaAtributo('txtCirculante', datosRow.CIRCULANTE, 0)
                                    asignaAtributo('cmbIdSitioQuirurgico', datosRow.SITIO, 0)
                                    asignaAtributo('txtIdProcedimiento', datosRow.ID_PROCEDIMIENTO, 0)

                                    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
                                        setTimeout("buscarAGENDA('listCitaCirugiaProcedimiento')", 600);
                                        setTimeout("buscarAGENDA('listProcedimientosAgendaCirugia')", 900);
                                        setTimeout("buscarAGENDA('listPersonalCirugia')", 1200);
                                        asignaAtributo('lblIdDoc', '', 0)
                                    } else {
                                        setTimeout("buscarAGENDA('listProcedimientosAgenda')", 300);
                                        setTimeout("buscarAGENDA('listCitaAgendaProcedimientos')", 600);
                                    }

                                    asignaAtributoCombo('cmbIdProfesionalesAg', datosRow.ID_PERSONAL, datosRow.NOMBRE_PERSONAL)
                                    asignaAtributoCombo('cmbConsultorioAg', datosRow.ID_CONSULTORIO, datosRow.CONSULTORIO)

                                    asignaAtributo('txtFechaDestinoCita', datosRow.FECHA_CITA, 0)
                                    asignaAtributo('cmbHoraInicioDiv', datosRow.HORA, 0)
                                    asignaAtributo('cmbMinutoInicioDiv', datosRow.MINUTO, 0)
                                    asignaAtributo('cmbPeriodoInicioDiv', datosRow.PERIODO, 0)

                                    mostrar('divVentanitaAgenda')

                                    if (datosRow.ID_PACIENTE != '') {

                                        habilitar('txtIdBusPaciente', 0)
                                        setTimeout("habilitar('cmbTipoId',0)", 1000);
                                        setTimeout("habilitar('txtIdentificacion',0)", 1000);
                                        habilitar('btn_busPacienteCita', 0)
                                        habilitar('idTraerListaEspera', 0)
                                    } else {
                                        habilitar('txtIdBusPaciente', 1)
                                        setTimeout("habilitar('cmbTipoId',1)", 1500);
                                        setTimeout("habilitar('txtIdentificacion',1)", 1500);
                                        habilitar('btn_busPacienteCita', 1)
                                        habilitar('idTraerListaEspera', 1)
                                    }

                                }
                            }
                        }
                    },

                });
                $('#drag' + ventanaActual.num).find("#listAgenda").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            } else
                alert('SELECCIONE EL PROFESIONAL')
            break;

        case 'listAgendaArchivo':
            // ancho= 700;  
            ancho = ($('#drag' + ventanaActual.num).find("#divListAgenda").width()) + 10;

            if (valorAtributo('cmbIdProfesionales') != '') {
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=63&parametros=";
                add_valores_a_mandar(valorAtributo('cmbExito'));
                add_valores_a_mandar(valorAtributo('lblFechaSeleccionada'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                //add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                $('#drag' + ventanaActual.num).find("#listAgenda").jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['contador', 'ID', 'FECHA_CITA', 'HORA',
                        'ID PACIENTE', 'TIPO_ID', 'IDENTIFICACION',
                        'ID_DOCUMENTO_PDF', 'IDENTIF_PDF',
                        'ID_REMISION_PDF', 'REMISION_PDF',
                        'ID_CONSENTIMIENTO_PDF', 'CONSENTIMIENTO_PDF',
                        'SITIO_ARCHIVO', 'OTROS',

                        'NOMBRE PACIENTE',
                        'ID_ELABORO', 'ID_AGENDA', 'info_usuario', 'FECHA_ELABORO', 'ID_ESTADO_AGENDA', 'TIPO', 'NOM_TIPO', 'ID_MOTIVO',
                        'ID_ADMINISTRADORA', 'NOM_ADMINISTRADORA', 'NO_AUTORIZACION', 'FECHA_VIGENCIA', 'SEMAF_AUTORIZACION', 'FECHA_PACIENTE', 'ID_INSTITU_REMITE',
                        'NOM_INSTITU_REMITE', 'OBSERVACION', 'OBSERVACION_CONFIRMA', 'ID_ESTADO', 'ESTADO', 'ID_LISTA_ESPERA'
                    ],
                    colModel: [
                        { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                        { name: 'ID', index: 'ID', hidden: true },
                        { name: 'FECHA_CITA', index: 'FECHA_CITA', hidden: true },
                        { name: 'HORA', index: 'HORA', width: anchoP(ancho, 3) },
                        { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                        { name: 'TIPO_ID', index: 'TIPO_ID', width: anchoP(ancho, 2) },
                        { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 7) },

                        { name: 'ID_DOCUMENTO_PDF', index: 'ID_DOCUMENTO_PDF', hidden: true },
                        { name: 'DOCUMENTO_PDF', index: 'DOCUMENTO_PDF', width: anchoP(ancho, 5) },

                        { name: 'ID_REMISION_PDF', index: 'ID_REMISION_PDF', hidden: true },
                        { name: 'REMISION_PDF', index: 'REMISION_PDF', width: anchoP(ancho, 4) },

                        { name: 'ID_CONSENTIMIENTO_PDF', index: 'ID_CONSENTIMIENTO_PDF', hidden: true },
                        { name: 'CONSENTIMIENTO_PDF', index: 'CONSENTIMIENTO_PDF', width: anchoP(ancho, 4) },
                        { name: 'SITIO_ARCHIVO', index: 'SITIO_ARCHIVO', width: anchoP(ancho, 2) },
                        { name: 'OTROS', index: 'OTROS', width: anchoP(ancho, 3) }, //  C   

                        { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: anchoP(ancho, 18) },

                        { name: 'ID_ELABORO', index: 'ID_ELABORO', hidden: true },
                        { name: 'ID_AGENDA', index: 'ID_AGENDA', hidden: true },
                        { name: 'info_usuario', index: 'info_usuario', hidden: true },
                        { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', hidden: true },
                        { name: 'ID_ESTADO_AGENDA', index: 'ID_ESTADO_AGENDA', hidden: true },
                        { name: 'TIPO', index: 'TIPO', hidden: true },
                        { name: 'NOM_TIPO', index: 'NOM_TIPO', width: anchoP(ancho, 13) },
                        { name: 'ID_MOTIVO', index: 'ID_MOTIVO', hidden: true },
                        { name: 'ID_ADMINISTRADORA', index: 'ID_ADMINISTRADORA', hidden: true },
                        { name: 'NOM_ADMINISTRADORA', index: 'NOM_ADMINISTRADORA', hidden: true },

                        { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', hidden: true },
                        { name: 'FECHA_VIGENCIA', index: 'FECHA_VIGENCIA', hidden: true },
                        { name: 'SEMAF_AUTORIZACION', index: 'SEMAF_AUTORIZACION', width: anchoP(ancho, 1) },

                        { name: 'FECHA_PACIENTE', index: 'FECHA_PACIENTE', hidden: true },
                        { name: 'ID_INSTITU_REMITE', index: 'ID_INSTITU_REMITE', hidden: true },
                        { name: 'NOM_INSTITU_REMITE', index: 'NOM_INSTITU_REMITE', hidden: true },
                        { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 5) },
                        { name: 'OBSERVACION_CONFIRMA', index: 'OBSERVACION_CONFIRMA', hidden: true },
                        { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                        { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 8) },
                        { name: 'ID_LISTA_ESPERA', index: 'ID_LISTA_ESPERA', hidden: true }

                    ],
                    height: 900,
                    width: ancho,
                    onSelectRow: function (rowid) {

                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#listAgenda').getRowData(rowid);

                        limpiarDivEditarJuan('camposDivVentanitaAgenda')


                        //     if(datosRow.ID_ESTADO=='A' ||  datosRow.ID_ESTADO=='C'  ||  datosRow.ID_ESTADO=='D'  ){  

                        asignaAtributo('lblUsuariosDato', datosRow.ID_ELABORO, 0)
                        asignaAtributo('lblIdAgendaDetalle', datosRow.ID, 0)
                        asignaAtributo('lblHora', datosRow.HORA, 0)
                        asignaAtributo('lblDuracion', datosRow.DURACION, 0)
                        asignaAtributo('lblFechaCita', datosRow.FECHA_CITA, 0)
                        asignaAtributo('cmbTipoCita', datosRow.TIPO, 0)
                        asignaAtributo('cmbEstadoCita', datosRow.ID_ESTADO, 0) /**/
                        asignaAtributo('cmbMotivoConsulta', datosRow.ID_MOTIVO, 0)
                        asignaAtributo('txtTelefonos', datosRow.TELEFONO, 0)
                        asignaAtributo('txtIdBusPaciente', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.TIPO_ID + datosRow.IDENTIFICACION + ' ' + datosRow.NOMBRE_PACIENTE), 0)
                        asignaAtributo('txtAdministradora1', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.NOM_ADMINISTRADORA), 0)
                        asignaAtributo('txtNoAutorizacion', datosRow.NO_AUTORIZACION, 0)
                        asignaAtributo('txtFechaVigencia', datosRow.FECHA_VIGENCIA, 0)
                        asignaAtributo('txtFechaPacienteCita', datosRow.FECHA_PACIENTE, 0)
                        asignaAtributo('txtIPSRemite', concatenarCodigoNombre(datosRow.ID_INSTITU_REMITE, datosRow.NOM_INSTITU_REMITE), 0)


                        if (datosRow.ID_LISTA_ESPERA == '0' || datosRow.ID_LISTA_ESPERA == 'null')
                            asignaAtributo('lblIdListaEspera', '', 0)
                        else
                            asignaAtributo('lblIdListaEspera', datosRow.ID_LISTA_ESPERA, 0)

                        asignaAtributo('txtObservacionConfirma', datosRow.OBSERVACION_CONFIRMA, 0)



                        if (valorAtributo('txtIdBusPaciente') != '') {
                            buscarInformacionBasicaPaciente();
                        }

                        ocultar('divArchivosAdjuntos')
                        mostrar('divVentanitaAgenda')

                        if (datosRow.ID_PACIENTE != '') {

                            habilitar('txtIdBusPaciente', 0)
                            setTimeout("habilitar('cmbTipoId',0)", 1000);
                            setTimeout("habilitar('txtIdentificacion',0)", 1000);
                            habilitar('btn_busPacienteCita', 0)
                            habilitar('idTraerListaEspera', 0)
                        }
                        //  }else alert('IMPOSIBLE MODIFICAR LOS ESTADOS DE NO EXITO..')
                    },

                });
                $('#drag' + ventanaActual.num).find("#listAgenda").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            } else
                alert('SELECCIONE EL PROFESIONAL')
            break;

        case 'listDiasProgramacion':
            limpiarListadosTotales('listAgenda');
            ancho = 100; //($('#drag'+ventanaActual.num).find("#divListadoDias").width())-100;      
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=519" + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbExito'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('cmbExito'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('lblMes'));
            add_valores_a_mandar(valorAtributo('lblAnio'));


            $('#drag' + ventanaActual.num).find("#listDiasProgramacion").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['', '', 'Dia', 'fecha2', 'T', 'Dis'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'Dian', index: 'Dian', width: anchoP(ancho, 25), align: 'left' },
                    { name: 'Dia', index: 'Dia', width: anchoP(ancho, 45), align: 'center' },
                    { name: 'Fecha2', index: 'Fecha2', hidden: true },
                    { name: 'T', index: 'T', width: anchoP(ancho, 15), align: 'right' },
                    { name: 'D', index: 'D', width: anchoP(ancho, 15), align: 'right' },
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listDiasProgramacion').getRowData(rowid);
                    asignaAtributo('lblFechaSeleccionada', datosRow.Fecha2, 0)
                    buscarAGENDA('listAgendaProgramacion')
                },
                height: 700,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#listDiasProgramacion").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listUbicacionCama':
            ancho = ($('#drag' + ventanaActual.num).find("#divListContenedor").width()) - 70;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=634&parametros=";
            add_valores_a_mandar(valorAtributo('cmbServicio'));
            add_valores_a_mandar(valorAtributo('cmbIdArea'));
            add_valores_a_mandar(valorAtributo('cmbIdHabitacion'));
            add_valores_a_mandar(valorAtributo('cmbIdEstadoCama'));
            $('#drag' + ventanaActual.num).find("#listUbicacionCama").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_SERVICIO', 'SERVICIO', 'ID_AREA', 'AREA', 'ID_HABITACION', 'HABITACION', 'ID_ESTADO', 'ESTADO', 'ID_CAMA', 'CAMA', 'ID_PACIENTE', 'IDENTIFICACION', 'NOMBRE PACIENTE', 'ID_ADMISION'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID_SERVICIO', index: 'ID_SERVICIO', hidden: true },
                    { name: 'SERVICIO', index: 'SERVICIO', width: anchoP(ancho, 15) },
                    { name: 'ID_AREA', index: 'ID_AREA', hidden: true },
                    { name: 'AREA', index: 'AREA', width: anchoP(ancho, 10) },
                    { name: 'ID_HABITACION', index: 'ID_HABITACION', hidden: true },
                    { name: 'HABITACION', index: 'HABITACION', width: anchoP(ancho, 10) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 7) },
                    { name: 'ID_CAMA', index: 'ID_CAMA', hidden: true },
                    { name: 'CAMA', index: 'CAMA', width: anchoP(ancho, 8) },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 7) },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: anchoP(ancho, 15) },
                    { name: 'ID_ADMISION', index: 'ID_ADMISION', hidden: true },
                ],
                height: 700,
                autowidth: true,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listUbicacionCama').getRowData(rowid);

                    document.getElementById("trBusPaciente").style.display = "none";
                    document.getElementById("trInfoPaciente").style.display = "none";
                    document.getElementById("trAccionCama").style.display = "none";
                    document.getElementById("trAccionDejarDisponible").style.display = "none";

                    setTimeout(() => {
                        switch (datosRow.ID_ESTADO) {
                            case "1":
                                document.getElementById("trBusPaciente").style.display = "grid";
                                document.getElementById("trAccionCama").style.display = "grid";

                                limpiaAtributo("txtIdBusPaciente", 0);
                                limpiaAtributo("txtIdFacturaUbicacionPaciente", 0);
                                limpiaAtributo("lblIdAdmisionUbicacionPaciente", 0);
                                limpiaAtributo("txtIdAreaActual", 0);
                                limpiaAtributo("txtIdHabitacionActual", 0);
                                limpiaAtributo("txtIdCamaActual", 0);
                                break;
                            case "2":
                                document.getElementById("trInfoPaciente").style.display = "grid";
                                asignaAtributo("lblIdAdmisionUbicacionPaciente", datosRow.ID_ADMISION, 0);
                                asignaAtributo("lblIdPacienteUbicacionPaciente", datosRow.ID_PACIENTE, 0);
                                asignaAtributo("lblTipoIdUbicacionPaciente", datosRow.IDENTIFICACION, 0);
                                asignaAtributo("lblNomUbicacionPaciente", datosRow.NOMBRE_PACIENTE, 0);
                                break;
                            case "3":
                                //ACCION PARA CAMAS NO HABILITADAS
                                break;
                            case "4":
                                document.getElementById("trAccionDejarDisponible").style.display = "grid";
                                break;
                            default:
                                break;
                        }

                        asignaAtributo('cmbServicio', datosRow.ID_SERVICIO, 0);
                        asignaAtributo('txtIdArea', datosRow.ID_AREA, 0);
                        asignaAtributo('txtIdHabitacion', datosRow.ID_HABITACION, 0);
                        asignaAtributo('txtIdCama', datosRow.ID_CAMA, 0);
                        asignaAtributo('lblArea', datosRow.AREA, 0);
                        asignaAtributo('lblHabitacion', datosRow.HABITACION, 0);
                        asignaAtributo('lblCama', datosRow.CAMA, 0);

                        mostrar('divVentanitaUbicacionPaciente');
                    }, 100);
                },
            });
            $('#drag' + ventanaActual.num).find("#listUbicacionCama").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listAgendaProgramacion':
            ancho = ($('#drag' + ventanaActual.num).find("#divListAgenda").width()) + 10;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=518&parametros=";
            add_valores_a_mandar(valorAtributo('cmbExito'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('lblFechaSeleccionada'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            $('#drag' + ventanaActual.num).find("#listAgenda").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'HORA', 'ID PACIENTE', 'TIPO_ID', 'IDENTIFICACION', 'NOMBRE PACIENTE',
                    'ID_AGENDA', 'ESTADO', 'ESTADO_DOC', 'NOM_ESTADO',
                    'NO_LENTE_INTRAOCULAR', 'ID_TIPO_ANESTECIA', 'ANESTECIOLOGO', 'INSTRUMENTADOR', 'CIRCULANTE', 'OBSERVACION_FOLIO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'HORA', index: 'HORA', width: anchoP(ancho, 3) },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', width: anchoP(ancho, 5) },
                    { name: 'TIPO_ID', index: 'TIPO_ID', width: anchoP(ancho, 2) },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 8) },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: anchoP(ancho, 25) },
                    { name: 'ID_AGENDA', index: 'ID_AGENDA', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', hidden: true },
                    { name: 'ESTADO_DOC', index: 'ESTADO_DOC', width: anchoP(ancho, 10) },
                    { name: 'NOM_ESTADO', index: 'NOM_ESTADO', width: anchoP(ancho, 10) },
                    { name: 'NO_LENTE_INTRAOCULAR', index: 'NO_LENTE_INTRAOCULAR', hidden: true },
                    { name: 'ID_TIPO_ANESTECIA', index: 'ID_TIPO_ANESTECIA', hidden: true },
                    { name: 'ANESTECIOLOGO', index: 'ANESTECIOLOGO', hidden: true },
                    { name: 'INSTRUMENTADOR', index: 'INSTRUMENTADOR', hidden: true },
                    { name: 'CIRCULANTE', index: 'CIRCULANTE', hidden: true },
                    { name: 'OBSERVACION_FOLIO', index: 'OBSERVACION_FOLIO', hidden: true },


                ],
                height: 900,
                width: ancho,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listAgenda').getRowData(rowid);
                    limpiarDivEditarJuan('camposDivVentanitaAgenda')

                    limpiarListadosTotales('listPersonalCirugia')
                    limpiarListadosTotales('listCitaCirugiaProcedimiento')


                    //    if(datosRow.ID_ESTADO=='A' ||  datosRow.ID_ESTADO=='C'  ||  datosRow.ID_ESTADO=='D'  ){   

                    asignaAtributo('lblIdAgendaDetalle', datosRow.ID, 0)
                    asignaAtributo('lblHora', datosRow.HORA, 0)
                    asignaAtributo('lblFechaCita', datosRow.FECHA_CITA, 0)
                    asignaAtributo('cmbTipoCita', datosRow.TIPO, 0)
                    asignaAtributo('cmbEstadoCita', datosRow.ID_ESTADO, 0)
                    asignaAtributo('txtEstadoCita', datosRow.ID_ESTADO, 0)
                    asignaAtributo('cmbMotivoConsulta', datosRow.ID_MOTIVO, 0)
                    asignaAtributo('txtTelefonos', datosRow.TELEFONO, 0)
                    asignaAtributo('txtIdBusPaciente', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.TIPO_ID + datosRow.IDENTIFICACION + ' ' + datosRow.NOMBRE_PACIENTE), 0)

                    asignaAtributo('txtNoLente', datosRow.NO_LENTE_INTRAOCULAR, 0)
                    asignaAtributo('cmbTipoAnestesia', datosRow.ID_TIPO_ANESTECIA, 0)
                    asignaAtributo('txtAnestesiologo', datosRow.ANESTECIOLOGO, 0)
                    asignaAtributo('txtInstrumentador', datosRow.INSTRUMENTADOR, 0)
                    asignaAtributo('txtCirculante', datosRow.CIRCULANTE, 0)

                    document.getElementById('txtNoLente').title = datosRow.OBSERVACION_FOLIO;
                    document.getElementById('imgObservacion').title = datosRow.OBSERVACION_FOLIO;


                    asignaAtributo('cmbIdSitioQuirurgico', datosRow.ID_SITIO_QUIRURGICO, 0)
                    asignaAtributo('txtIdProcedimiento', datosRow.ID_PROCEDIMIENTO, 0)

                    asignaAtributo('txtIdBusPaciente', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.NOMBRE_PACIENTE), 0)

                    buscarInformacionBasicaPaciente();

                    sacarValoresColumnasDeGrillaPersonal('listPersonalCirugia');

                    setTimeout("buscarAGENDA('listPersonalCirugia')", 600);
                    setTimeout("buscarAGENDA('listCitaCirugiaProcedimiento')", 1200);



                    asignaAtributo('lblIdDoc', '', 0)

                    mostrar('divVentanitaAgenda')
                    ocultar('divAcordionDocumentoTransacciones')

                    //  }else alert('IMPOSIBLE MODIFICAR LOS ESTADOS DE NO EXITO.,')
                },

            });
            $('#drag' + ventanaActual.num).find("#listAgenda").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listDiasC':
            limpiarListadosTotales('listAgenda');
            ancho = 240; //($('#drag'+ventanaActual.num).find("#divListadoDias").width())-100;      
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=425&parametros=";
            add_valores_a_mandar(valorAtributo('cmbExito'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('cmbExito'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('lblMes'));
            add_valores_a_mandar(valorAtributo('lblAnio'));

            $('#drag' + ventanaActual.num).find("#listDiasC").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Dia', 'Dia', 'fecha2', 'T', 'Dis', 'S'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'Dia', index: 'Dian', width: anchoP(ancho, 25), align: 'left' },
                    { name: 'Dia', index: 'Dia', width: anchoP(ancho, 45), align: 'center' },
                    { name: 'Fecha2', index: 'Fecha2', hidden: true },
                    { name: 'T', index: 'T', width: anchoP(ancho, 40), align: 'right' },
                    { name: 'D', index: 'D', width: anchoP(ancho, 40), align: 'right' },
                    { name: 'S', index: 'S', width: anchoP(ancho, 40), align: 'right' },

                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblFechaSeleccionada', datosRow.Fecha2, 0)

                    if (valorAtributo('txt_banderaOpcionCirugia') != 'ARCHIVO')
                        buscarAGENDA('listAgenda')
                    else
                        buscarAGENDA('listAgendaArchivo')
                },
                height: 700,
                autowidth: true,
                multiselect: true,
                multiboxonly: true,
            });
            $('#drag' + ventanaActual.num).find("#listDiasC").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listDias':
            document.getElementById('divListadoDias').innerHTML = '<table id="listDias" class="scroll"></table>';
            if (!$('#copiarCitas').attr('checked')) {
                limpiarListadosTotales('listAgenda');
            }
            ancho = 240; //($('#drag'+ventanaActual.num).find("#divListadoDias").width())-100;      
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=425&parametros=";
            add_valores_a_mandar(valorAtributo('cmbExito'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('cmbExito'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('lblMes'));
            add_valores_a_mandar(valorAtributo('lblAnio'));

            $('#drag' + ventanaActual.num).find("#listDias").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Dian', 'Dia', 'fecha2', 'T', 'Dis', 'S'],
                colModel: [
                    { name: '', index: 'contador', hidden: true },
                    { name: 'Dian', index: 'Dian', width: anchoP(ancho, 25), align: 'left' },
                    { name: 'Dia', index: 'Dia', width: anchoP(ancho, 45), align: 'center' },
                    { name: 'Fecha2', index: 'Fecha2', hidden: true },
                    { name: 'T', index: 'T', width: anchoP(ancho, 40), align: 'right' },
                    { name: 'D', index: 'D', width: anchoP(ancho, 40), align: 'right' },
                    { name: 'S', index: 'S', width: anchoP(ancho, 40), align: 'right' },

                ],

                onSelectRow: function (rowid) {
                    if(!$('#copiarCitas').attr('checked')){
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                        asignaAtributo('lblFechaSeleccionada', datosRow.Fecha2, 0)
    
                        if (valorAtributo('txt_banderaOpcionCirugia') != 'ARCHIVO') {
                            setTimeout(() => {
                                buscarAGENDA('listAgenda')
                            }, 200);
                        } else {
                            setTimeout(() => {
                                buscarAGENDA('listAgendaArchivo')
                            }, 200);
                        }
                    }
                },
                height: 700,
                width: ancho,
                multiselect: $('#copiarCitas').attr('checked'),
                //multiboxonly: true,
                //multikey: $('#copiarCitas').attr('checked') ? "ctrlKey" : "",
            });
            $('#drag' + ventanaActual.num).find("#listDias").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listListaEsperaTraer':
            limpiarDivEditarJuan(arg);
            numero_cita_seleccionada = $("#listAgenda").jqGrid('getGridParam', 'selrow')

            ancho = 1250;
            valores_a_mandar = pag;

            valores_a_mandar = valores_a_mandar + "?idQuery=312&parametros=";
            add_valores_a_mandar(valorAtributo('cmbBusSinCita'));
            add_valores_a_mandar(valorAtributo('cmbTipoCitaLE'));
            add_valores_a_mandar(valorAtributo('cmbEstadoLE'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradoraLE'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('cmbNecesidad'));
            add_valores_a_mandar($("#listAgenda").getRowData(numero_cita_seleccionada).ID_SEDE);

            add_valores_a_mandar(valorAtributo('cmbDiasEsperaLE'));
            add_valores_a_mandar(valorAtributo('cmbDiasEsperaLE'));

            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPacienteLE'));
            add_valores_a_mandar(valorAtributo('cmbRangosEdad'));
            add_valores_a_mandar(valorAtributo('cmbRangosEdad'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['#', 'ID LISTA', 'ID PACIENTE', 'TIP', 'IDE_PAC', 'NOMBRE PACIENTE', 'FECHA_NACIMIENTO', 'EDAD',
                    'TELEFONO', 'ID_MUNICIPIO', 'MUNICIPIO', 'DIRECCION',
                    'ID_ADMINISTRADORA', 'ADMINISTRADORA', 'PLAN', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'PROFESIONAL', 'DIAS_ESPERA', 'TIPO_CITA', 'NOMBRE_TIPO',
                    'NO_AUTORIZACION', 'FECHA_VIGENCIA', 'ID_GRADO_NECESIDAD', 'OBSERVACION', 'ID_ESTADO_LE', 'NOM_ESTADO_LE', 'ID_ELABORO', 'FECHA_ELABORO',
                    'ID_CITA', 'SITIO', 'ID_PLAN', 'ID_REGIMEN', 'REGIMEN'
                ],
                colModel: [
                    { name: '#', index: 'Cuantos', width: 15 },
                    { name: 'ID_LISTA', index: 'ID_LISTA', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'TIPO', index: 'TIPO', width: 20 },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: 80 },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: 250 },
                    { name: 'FECHA_NACIMIENTO', index: 'FECHA_NACIMIENTO', hidden: true },
                    { name: 'EDAD', index: 'EDAD', hidden: true },
                    { name: 'TELEFONO', index: 'TELEFONO', width: 80 },
                    { name: 'ID_MUNICIPIO', index: 'ID_MUNICIPIO', hidden: true },
                    { name: 'MUNICIPIO', index: 'MUNICIPIO', width: 150 },
                    { name: 'DIRECCION', index: 'DIRECCION', hidden: true },

                    { name: 'ID_ADMINISTRADORA', index: 'ID_ADMINISTRADORA', hidden: true },
                    { name: 'NOM_ADMINISTRADORA', index: 'NOM_ADMINISTRADORA', width: 150 },
                    { name: 'PLAN', index: 'PLAN', width: 250 },

                    { name: 'ID_ESPECIALIDAD', index: 'NOM_ESPECIALIDAD', hidden: true },
                    { name: 'NOM_ESPECIALIDAD', index: 'NOM_ESPECIALIDAD', hidden: true },
                    { name: 'PROFESIONAL', index: 'PROFESIONAL', hidden: true },
                    { name: 'DIAS_ESPERA', index: 'DIAS_ESPERA', width: 50 },
                    { name: 'TIPO_CITA', index: 'TIPO_CITA', hidden: true },
                    { name: 'NOMBRE_TIPO', index: 'NOMBRE_TIPO', width: 250 },
                    { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', width: 80 },
                    { name: 'FECHA_VIGENCIA', index: 'FECHA_VIGENCIA', width: 80 },
                    { name: 'ID_GRADO_NECESIDAD', index: 'ID_GRADO_NECESIDAD', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: 300 },
                    { name: 'ID_ESTADO_LE', index: 'ID_ESTADO_LE', hidden: true },
                    { name: 'NOM_ESTADO_LE', index: 'NOM_ESTADO_LE', hidden: true },
                    { name: 'ID_ELABORO', index: 'ID_ELABORO', hidden: true },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', hidden: true },
                    { name: 'ID_CITA', index: 'ID_CITA', hidden: true },
                    { name: 'SITIO', index: 'SITIO', hidden: true },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'ID_REGIMEN', index: 'ID_REGIMEN', hidden: true },
                    { name: 'REGIMEN', index: 'REGIMEN', hidden: true },
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    if (confirm('TRAER DE LA LISTA DE ESPERA A UNA CITA?')) {

                        if (datosRow.ID_CITA == 'NO') {
                            limpiarDatosPacientePaciente();

                            asignaAtributo('lblIdListaEspera', datosRow.ID_LISTA, 0)
                            asignaAtributo('txtIdBusPaciente', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.NOMBRE_PACIENTE), 0)
                            setTimeout(() => {
                                buscarInformacionBasicaPaciente();
                            }, 300);
                            asignaAtributo('txtAdministradora1', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.NOM_ADMINISTRADORA), 0)
                            asignaAtributoCombo('cmbTipoCita', datosRow.TIPO_CITA, datosRow.NOMBRE_TIPO)
                            asignaAtributo('txtNoAutorizacion', datosRow.NO_AUTORIZACION, 0)
                            asignaAtributo('txtFechaVigencia', datosRow.FECHA_VIGENCIA, 0)
                            asignaAtributo('cmbIdSitioQuirurgico', datosRow.SITIO, 0)
                            asignaAtributo('lblIdPlanContratacion', datosRow.ID_PLAN, 0)
                            asignaAtributo('lblNomPlanContratacion', datosRow.PLAN, 0)
                            asignaAtributoCombo('cmbIdPlan', datosRow.ID_PLAN, datosRow.PLAN)
                            asignaAtributo('txtIPSRemite', '0000-OTRO::PASTO', 0)
                            asignaAtributoCombo2('cmbIdTipoRegimen', datosRow.ID_REGIMEN, datosRow.REGIMEN)

                            ocultar('divVentanitaListaEspera');


                            buscarAGENDA('listCitaCexProcedimientoTraer')
                            buscarAGENDA('listProcedimientosAgenda')
                        } else
                            alert('ESTA LISTA DE ESPERA YA TIENE ASIGNADO UNA CITA !!!')

                    } else { /*************************  MODIFICAR  ************/
                        //ocultar('divVentanitaListaEspera');
                        if (datosRow.ID_CITA == 'NO') {
                            mostrar('divVentanitaEditarListaEspera');
                            asignaAtributo('lblListaEsperaModificar', datosRow.ID_LISTA, 0)
                            asignaAtributo('txtAdministradora1LE', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.NOM_ADMINISTRADORA), 0)
                            asignaAtributo('txtObservacionLE', datosRow.OBSERVACION, 0)
                            asignaAtributo('cmbIdEspecialidadLE', datosRow.ID_ESPECIALIDAD, 0)
                            //asignaAtributo('cmbIdSubEspecialidadLE', datosRow.ID_SUB_ESPECIALIDAD, 0)
                            asignaAtributo('cmbDiscapaciFisicaLE', datosRow.DISCAPACIDAD_FISICA, 0)
                            asignaAtributo('cmbEmbarazoLE', datosRow.EMBARAZO, 0)
                            asignaAtributo('cmbTipoCitaLEM', datosRow.TIPO_CITA, 0)
                            asignaAtributo('cmbNecesidadLE', datosRow.ID_GRADO_NECESIDAD, 0)
                            asignaAtributo('cmbEstadoLEM', datosRow.ID_ESTADO_LE, 0)
                            asignaAtributo('cmbIdSitioQuirurgico', datosRow.SITIO, 0)


                            asignaAtributo('lblTelefono', datosRow.TELEFONO, 0)
                            asignaAtributo('lblNombrePaciente', datosRow.NOMBRE_PACIENTE, 0)
                            asignaAtributo('lblEdad', datosRow.EDAD, 0)
                            asignaAtributo('lblDias', datosRow.DIAS_ESPERA, 0)
                        } else
                            alert('ESTA LISTA DE ESPERA YA TIENE ASIGNADO UNA CITA')

                        //limpiarDatosPacienteListaEspera();
                    }
                },
                height: 500,
                width: 1250,
                shrinkToFit: false,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listListaEsperaTraerCirugia':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#divContenidoLETraer").width()) - 80;
            valores_a_mandar = pag;

            if (valorAtributo('cmbBusSinCita') == 'S')
                idQuery = 338;
            else
                idQuery = 339;

            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoLE'));
            add_valores_a_mandar(valorAtributo('cmbTipoCitaLE'));
            add_valores_a_mandar(valorAtributo('cmbEstadoLE'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradoraLE'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            //add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
            add_valores_a_mandar(valorAtributo('cmbNecesidad'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionalesLETraer'));
            add_valores_a_mandar(valorAtributo('cmbDiasEsperaLE'));
            add_valores_a_mandar(valorAtributo('cmbDiasEsperaLE'));

            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPacienteLE'));
            add_valores_a_mandar(valorAtributo('cmbRangosEdad'));
            add_valores_a_mandar(valorAtributo('cmbRangosEdad'));

            //alert(valores_a_mandar)
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Cuantos', 'ID LISTA', 'ID PACIENTE', 'NOMBRE PACIENTE', 'FECHA_NACIMIENTO', 'EDAD',
                    'TELEFONO', 'ID_MUNICIPIO', 'MUNICIPIO', 'DIRECCION',
                    'ID_ADMINISTRADORA', 'ADMINISTRADORA', 'ID_PLAN', 'PLAN', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'PROFESIONAL', 'NOMBRE PROCEDIMIENTO', 'DIAS_ESPERA', 'TIPO_CITA',
                    'NO_AUTORIZACION', 'FECHA_VIGENCIA', 'ID_GRADO_NECESIDAD', 'OBSERVACION', 'ID_ESTADO_LE', 'NOM_ESTADO_LE', 'ID_ELABORO', 'FECHA_ELABORO',
                    'ID_CITA', 'id_institucion', 'ID_REGIMEN', 'REGIMEN'
                ],
                colModel: [
                    { name: 'Cuantos', index: 'Cuantos', width: anchoP(ancho, 2) },
                    { name: 'ID_LISTA', index: 'ID_LISTA', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: anchoP(ancho, 20) },
                    { name: 'FECHA_NACIMIENTO', index: 'FECHA_NACIMIENTO', hidden: true },
                    { name: 'EDAD', index: 'EDAD', width: anchoP(ancho, 2) },
                    { name: 'TELEFONO', index: 'TELEFONO', width: anchoP(ancho, 5) },
                    { name: 'ID_MUNICIPIO', index: 'ID_MUNICIPIO', hidden: true },
                    { name: 'MUNICIPIO', index: 'MUNICIPIO', width: anchoP(ancho, 10) },
                    { name: 'DIRECCION', index: 'DIRECCION', hidden: true },

                    { name: 'ID_ADMINISTRADORA', index: 'ID_ADMINISTRADORA', hidden: true },
                    { name: 'NOM_ADMINISTRADORA', index: 'NOM_ADMINISTRADORA', hidden: true },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 12) },

                    { name: 'ID_ESPECIALIDAD', index: 'NOM_ESPECIALIDAD', hidden: true },
                    { name: 'NOM_ESPECIALIDAD', index: 'NOM_ESPECIALIDAD', hidden: true },
                    //{ name: 'ID_SUB_ESPECIALIDAD', index: 'ID_SUB_ESPECIALIDAD', hidden: true },
                    //{ name: 'NOM_SUB_ESPECIALIDAD', index: 'NOM_SUB_ESPECIALIDAD', width: anchoP(ancho, 10) },
                    { name: 'PROFESIONAL', index: 'PROFESIONAL', width: anchoP(ancho, 10) },

                    { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 20) },

                    { name: 'DIAS_ESPERA', index: 'DIAS_ESPERA', width: anchoP(ancho, 4) },
                    { name: 'TIPO_CITA', index: 'TIPO_CITA', width: anchoP(ancho, 4) },
                    { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', width: anchoP(ancho, 4) },
                    { name: 'FECHA_VIGENCIA', index: 'FECHA_VIGENCIA', hidden: true },
                    { name: 'ID_GRADO_NECESIDAD', index: 'ID_GRADO_NECESIDAD', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 2) },
                    { name: 'ID_ESTADO_LE', index: 'ID_ESTADO_LE', hidden: true },
                    { name: 'NOM_ESTADO_LE', index: 'NOM_ESTADO_LE', hidden: true },
                    { name: 'ID_ELABORO', index: 'ID_ELABORO', hidden: true },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', width: anchoP(ancho, 7) },
                    { name: 'ID_CITA', index: 'ID_CITA', hidden: true },
                    { name: 'ins_remite', index: 'ins_remite', hidden: true },
                    { name: 'ID_REGIMEN', index: 'ID_REGIMEN', hidden: true },
                    { name: 'REGIMEN', index: 'REGIMEN', hidden: true }
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    if (confirm('TRAER DE LA LISTA DE ESPERA No = ' + datosRow.ID_LISTA + ' A UNA CITA DE CIRUGIA?')) {
                        if (datosRow.ID_CITA == '') {
                            limpiarDatosPacientePaciente();
                            asignaAtributo('lblIdListaEspera', datosRow.ID_LISTA, 0)
                            asignaAtributo('txtIdBusPaciente', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.NOMBRE_PACIENTE), 0)
                            asignaAtributo('txtAdministradora1', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.NOM_ADMINISTRADORA), 0)
                            asignaAtributo('cmbTipoCita', datosRow.TIPO_CITA, 0)
                            asignaAtributo('txtNoAutorizacion', datosRow.NO_AUTORIZACION, 0)
                            asignaAtributo('txtFechaVigencia', datosRow.FECHA_VIGENCIA, 0)
                            asignaAtributo('txtIPSRemite', datosRow.ins_remite, 0)
                            asignaAtributo('lblIdPlanContratacion', datosRow.ID_PLAN, 0)
                            asignaAtributo('lblNomPlanContratacion', datosRow.PLAN, 0)
                            asignaAtributoCombo2('cmbIdTipoRegimen', datosRow.ID_REGIMEN, datosRow.REGIMEN)

                            ocultar('divVentanitaListaEspera');
                            buscarInformacionBasicaPaciente()
                            alert('SE TRAERAN LOS PROCEDIMIENTOS DESDE LA LISTA DE ESPERA')

                            buscarAGENDA('listCitaCirugiaProcedimientoTraer')
                        } else
                            alert('ESTA LISTA DE ESPERA YA TIENE ASIGNADO UNA CITA?')
                    } else { /*************************  MODIFICAR  ************/
                        //ocultar('divVentanitaListaEspera');
                        if (datosRow.ID_CITA == '') {
                            mostrar('divVentanitaEditarListaEspera');
                            asignaAtributo('lblListaEsperaModificar', datosRow.ID_LISTA, 0)
                            asignaAtributo('txtAdministradora1LE', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.NOM_ADMINISTRADORA), 0)
                            asignaAtributo('txtObservacionLE', datosRow.OBSERVACION, 0)
                            asignaAtributo('cmbIdEspecialidadLE', datosRow.ID_ESPECIALIDAD, 0)
                            asignaAtributo('cmbIdSubEspecialidadLE', datosRow.ID_SUB_ESPECIALIDAD, 0)

                            asignaAtributo('txtNoAutorizacionLE', datosRow.NO_AUTORIZACION, 0)
                            asignaAtributo('txtFechaVencimientoLE', datosRow.FECHA_VIGENCIA, 0)

                            asignaAtributo('cmbTipoCitaLEM', datosRow.TIPO_CITA, 0)
                            asignaAtributo('cmbNecesidadLE', datosRow.ID_GRADO_NECESIDAD, 0)
                            asignaAtributo('cmbEstadoLEM', datosRow.ID_ESTADO_LE, 0)

                            asignaAtributo('lblTelefono', datosRow.TELEFONO, 0)
                            asignaAtributo('lblNombrePaciente', datosRow.NOMBRE_PACIENTE, 0)
                            asignaAtributo('lblEdad', datosRow.EDAD, 0)
                            asignaAtributo('lblDias', datosRow.DIAS_ESPERA, 0)

                            asignaAtributo('txtIPSRemite', datosRow.ins_remite, 0)
                            //limpiarDatosPacienteListaEspera();
                        } else
                            alert('ESTA LISTA DE ESPERA YA TIENE ASIGNADO UNA CITA')

                    }
                },
                height: 900,
                width: ancho,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        /*AGENDA FIN*/
    }
}

function prueba(rowid) {
    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listAgendaAdmision').getRowData(rowid);
    alert(datosRow.FECHA_CITA)
}

function NoBack() {


    //alert("Desea Salir 2?" );
    //Sgh.Utilidades.Conexion.destroy();
    //Sgh.Utilidades.Conexion.destroydb();

    //beanSession.conexion.destroy();
    location.reload();


    //Sgh.AdminSeguridad.Usuario.menu.clear();
    //System.out.println(" menu ::::"+ menu.size());

    /*
    function createCookie(name,value,days)
    {
        if (days)
        {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    }

    function readCookie(name)
    {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++)
        {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    function eraseCookie(name)
    {
        createCookie(name,"",-1);
    }


    var phpjs = {

        array_filter: function( arr, func )
        {
            var retObj = {}; 
            for ( var k in arr )
            {
                if ( func( arr[ k ] ) )
                {
                    retObj[ k ] = arr[ k ];
                }
            }
            return retObj;
        },
        filemtime: function( file )
        {
            var headers = this.get_headers( file, 1 );
            return ( headers && headers[ 'Last-Modified' ] && Date.parse( headers[ 'Last-Modified' ] ) / 1000 ) || false;
        },
        get_headers: function( url, format )
        {
            var req = window.ActiveXObject ? new ActiveXObject( 'Microsoft.XMLHTTP' ) : new XMLHttpRequest();
            if ( !req )
            {
                throw new Error('XMLHttpRequest not supported.');
            }

            var tmp, headers, pair, i, j = 0;

            try
            {
                req.open( 'HEAD', url, false );
                req.send( null ); 
                if ( req.readyState < 3 )
                {
                    return false;
                }
                tmp = req.getAllResponseHeaders();
                tmp = tmp.split( '\n' );
                tmp = this.array_filter( tmp, function( value )
                {
                    return value.toString().substring( 1 ) !== '';
                });
                headers = format ? {} : [];
	
                for ( i in tmp )
                {
                    if ( format )
                    {
                        pair = tmp[ i ].toString().split( ':' );
                        headers[ pair.splice( 0, 1 ) ] = pair.join( ':' ).substring( 1 );
                    }
                    else
                    {
                        headers[ j++ ] = tmp[ i ];
                    }
                }
	
                return headers;
            }
            catch ( err )
            {
                return false;
            }
        }
    };

    var cssRefresh = function( links ) {

        this.reloadFile = function( links )
        {
            for ( var a = 0, l = links.length; a < l; a++ )
            {
                var link = links[ a ],
                    newTime = phpjs.filemtime( this.getRandom( link.href ) );

                //	has been checked before
                if ( link.last )
                {
                    //	has been changed
                    if ( link.last != newTime )
                    {
                        //	reload
                        link.elem.setAttribute( 'href', this.getRandom( link.href ) );
                    }
                }

                //	set last time checked
                link.last = newTime;
            }
            setTimeout( function()
            {
                this.reloadFile( links );
            }, 1000 );
        };

        this.getRandom = function( f )
        {
            return f + '?x=' + Math.random();
        };


        this.reloadFile( links );
    };

    var getLinks = function()
    {
        var files = document.getElementsByTagName( 'link' ),
            links = [];

        for ( var a = 0, l = files.length; a < l; a++ )
        {			
            var elem = files[ a ],
                rel = elem.rel;

            if ( typeof rel != 'string' || rel.length == 0 || rel == 'stylesheet' )
            {
                links.push({
                    'elem' : elem,
                    'href' : elem.getAttribute( 'href' ).split( '?' )[ 0 ],
                    'last' : false
                });
            }
        }
        return links;
    };

    var links = getLinks(),
        wp = false;

    for ( var a = 0, l = links.length; a < l; a++ )
    {
        if ( links[ a ].href.indexOf( 'wp-content/' ) > -1 )
        {
            wp = true;
            break;
        }
    }		

    if ( wp )
    {
        var wpra = readCookie('wprefresh-asked');
        if ( wpra )
        {
            cssRefresh( links );
        }
        else
        {
            if ( confirm( 'Is this a WordPress site? Try WP Refresh!' ) )
            {
                createCookie( 'wprefresh-asked', 'yes', 7 );
                window.open( 'http://wprefresh.frebsite.nl', 'wpr' );
            }
            else
            {
                createCookie( 'wprefresh-asked', 'yes', 1 );
                cssRefresh( links );
            }
        }
    }
    else
    {
        cssRefresh( links );
    }
    */

}
function cerrarAyuda() {
    //alert('Salir de la ayuda');
    ocultar('divAyuda');
    ocultar('imgCerrar');
    ocultar('texAyuda');
    document.getElementById('divAyuda').setAttribute("hidden", "true");
    var vid = document.getElementById("myVideo");
    vid.pause();

    // function playVid() {
    //     vid.play();
    // }

    // function pauseVid() {
    //     vid.pause();
    // }
}

function validarFecha(value) {

    var estado = $('#ESTADO_CITA').val();

    if (valorAtributo('txtEstadoCita') === 'R') {
        return [false, "Cita reprogramada no se puede editar!", ""];
    } else if (valorAtributo('txtEstadoCita') === '' && estado === 'R') {
        return [false, "No puede reprogramar porque aun no ha creado la cita!", ""];
    } else if (estado === 'A' || estado === 'C') {



        var dateParts = value.split("/");

        var fechaCita = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
        var fechaActual = new Date();
        var fechaVencimiento = new Date();
        var fechaMaximo = new Date();
        fechaActual.setHours(0, 0, 0, 0);

        fechaVencimiento.setDate(fechaActual.getDate() + parseInt(valorAtributo('txtRangoVencimiento')));
        fechaMaximo.setDate(fechaActual.getDate() + parseInt(valorAtributo('txtRangoMaximo')));
        fechaVencimiento.setHours(0, 0, 0, 0);
        fechaMaximo.setHours(0, 0, 0, 0);
        console.log(fechaCita);

        if (fechaCita.getTime() < fechaActual.getTime()) {
            return [false, "No puede agendar un dia pasado!", ""];
        } else if (fechaCita.getTime() >= fechaMaximo.getTime()) {
            return [false, "No puede agendar mas de " + valorAtributo('txtRangoMaximo') + " dias en adelante!", ""];
        }/*else if(valorAtributo('txtRangoMaximo').includes("Cita vencida") && fechaCita.getTime()>=fechaVencimiento.getTime()){
            return [false, "Una cita vencida no puede agendar mas de "+valorAtributo('txtRangoVencimiento')+" dias en adelante!", ""];
        }*/else {
            return [true, "", ""];
        }
    } else {
        return [true, "", ""];
    }


}

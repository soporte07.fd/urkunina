function buscarPaciente(arg, pag) {
    pagina = pag;
    ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
    switch (arg) {
        case 'paciente':
            //limpiarDivEditarJuan(arg);
            //$('#drag' + ventanaActual.num).find('#divContenido').css('height', '400px');
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=327&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdentificacion'));
            add_valores_a_mandar(valorAtributo('cmbTipoId'));
            add_valores_a_mandar(valorAtributo('txtNombrePaciente'));

            //var sexo = 'accionesXml/obtenerCombos.jsp?idQueryCombo=110';
            //var sexo2 = ":cargando";
            //var regimen = 'accionesXml/obtenerCombos.jsp?idQueryCombo=518';
            //var regimenAfiliacion = 'accionesXml/obtenerCombos.jsp?idQueryCombo=305';
            //var etnia = 'accionesXml/obtenerCombos.jsp?idQueryCombo=2';
            //var discapacidad = 'accionesXml/obtenerCombos.jsp?idQueryCombo=953';
            //var estadoCivil = 'accionesXml/obtenerCombos.jsp?idQueryCombo=589';
            //var programaSocial = 'accionesXml/obtenerCombos.jsp?idQueryCombo=956';

            var tipoID = 'http://190.60.242.160:8001/combo/105';
            var escolaridad = 'http://190.60.242.160:8001/combo/4';
            var grupopoblacional = 'http://190.60.242.160:8001/combo/955';
            var parentesco = 'http://190.60.242.160:8001/combo/957';
            var condicionVulnerabilidad = 'http://190.60.242.160:8001/combo/959';
            var hechosVictimizantes = 'http://190.60.242.160:8001/combo/954';


            //var administradora = 'accionesXml/obtenerAutocomplete.jsp?idQuery=308';
            var administradora = 'http://190.60.242.160:8001/autocomplete/308';
            var jsonAdministradora = new Array();

            var ipsPrimaria = 'http://190.60.242.160:8001/autocomplete/313';
            var jsonIpsPrimaria = new Array();

            var municipio = 'http://190.60.242.160:8001/autocomplete/212';
            var jsonMunicipio = new Array();

            var barrio = 'http://190.60.242.160:8001/autocomplete/229';
            var jsonBarrio = new Array();

            var ocupacion = 'http://190.60.242.160:8001/autocomplete/8';
            var jsonOcupacion = new Array();

            var localidad = 'http://190.60.242.160:8001/autocomplete/934';
            var jsonLocalidad = new Array();


            $.get(administradora, function (data) {
                jsonAdministradora = data;
            });

            $.get(municipio, function (data) {
                jsonMunicipio = data;
            });

            $.get(barrio, function (data) {
                jsonBarrio = data;
            });

            $.get(ipsPrimaria, function (data) {
                jsonIpsPrimaria = data;
            });

            $.get(ocupacion, function (data) {
                jsonOcupacion = data;
            });

            $.get(localidad,function(data)
            {
                jsonLocalidad = data;
            });


            var editOptions = {
                editData: {
                    accion: "editarPaciente",
                    idQuery: "328",
                    usuarioModifica: LoginSesion()
                },
                beforeShowForm: function (formid) {
                    $('#TipoId', formid).attr("readonly", "readonly").addClass("ui-state-disabled");
                    $('#identificacion', formid).attr("readonly", "readonly").addClass("ui-state-disabled");
                    $('#afiliacion', formid).attr("readonly", "readonly").addClass("ui-state-disabled");
                    return true;
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                afterShowForm: function (formid) {
                    var id = $('#' + arg).jqGrid('getGridParam', 'selrow');
                    datosRow = $('#' + arg).getRowData(id);

                    // $('#sexo').html(sexo2);
                    // $('#sexo').val(datosRow.sexo);

                },

                closeAfterEdit: true,
                recreateForm: true,
                width: 1300,
                onclickSubmit: function (params) {
                    var id = jQuery("#listGrilla").jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#listGrilla").jqGrid('getRowData', id);
                    return { id: ret.id }
                }
            };
            var addOptions = {
                editData: {
                    accion: "crearPaciente",
                    idQuery: "336",
                    usuarioCrea: LoginSesion()
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                recreateForm: true,
                closeAfterAdd: true,
                width: 1300
            };
            var viewOptions =
            {
                labelswidth:'10%',
                width: 1200,

            };
            $('#drag' + ventanaActual.num).find("#listGrilla").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'TipoId', 'Identificacion','Codigo Afiliacion', 'Nombre 1', 'Nombre 2', 'Apellido 1', 'Apellido 2',
                    'Nombre Completo', 'Fecha Nacimiento', 'Sexo', 'Municipio','Nivel Sisben', 'Direccion', 'Barrio','Localidad', 'Estrato', 'Estrato',                    
                    'Telefono', 'Celular 1', 'Celular 2', 'e-mail', 'Administradora', 'IPS Primaria', 'Regimen', 'Regimen', 'Tipo Afiliado', 'Tipo Afiliado',
                    'Ocupacion', 'Estado Civil', 'Estado Civil', 'Lugar Nacimiento', 'Etnia', 'Etnia', 'Pueblo', 'Discapacidad', 'Discapacidad', 'Nivel Escolar', 'Nivel Escolar', 'Grupo Poblacional', 'Grupo Poblacional',
                    'Acudiente', 'Parentesco', 'Parentesco', 'TipoId Acudiente', 'Identificacion Acudiente', 'Direccion Acudiente', 'Telefono Acudiente', 'e-mail Acudiente',
                    'Condicion Vulneravilidad', 'Condicion Vulnerabilidad', 'Hechos Victimizantes', 'Hechos Victimizantes', 'Programa Social','Programa Social'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2), hidden: true },
                    { name: 'id', index: 'id', align: 'center', hidden: true },
                    {
                        name: 'TipoId', index: 'TipoId', align: 'center', width: anchoP(ancho, 6), editrules: { required: true }, formoptions: { rowpos: 3, colpos: 1 }, editable: true, edittype: "select",
                        editoptions: {
                            dataUrl: tipoID
                        },viewable: true
                    },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 30), editrules: { required: true,number:true }, editable: true, formoptions: { rowpos: 3, colpos: 2 }, viewable: true,editoptions:{dataInit: function(elem){$(elem).bind("keypress",function(e){return numeros(e)})}} },
                    { name: 'afiliacion', index: 'afiliacion', width: anchoP(ancho, 30), editrules: { required: false }, editable: true, formoptions: { rowpos: 4, colpos: 1 }, viewable: true,editoptions:{dataInit: function(elem){$(elem).bind("keypress",function(e){return numeros(e)})}}},
                    { name: 'Nombre1', index: 'Nombre1', hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 5, colpos: 1 },viewable:false },
                    { name: 'Nombre2', index: 'Nombre2', hidden: true, editrules: { required: false, edithidden: true }, editable: true, formoptions: { rowpos: 5, colpos: 2 },viewable:false },
                    { name: 'Apellido1', index: 'Apellido1', hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 6, colpos: 1 },viewable:false },
                    { name: 'Apellido2', index: 'Apellido2', hidden: true, editrules: { required: false, edithidden: true }, editable: true, formoptions: { rowpos: 6, colpos: 2 },viewable:false },
                    { name: 'NombreCompleto', index: 'NombreCompleto', width: anchoP(ancho, 110), align: 'left', viewable:true,formoptions: { rowpos: 4, colpos: 1 } },
                    {
                        name: 'FechaNacimiento', index: 'FechaNacimiento', width: anchoP(ancho, 35), align: 'left',
                        editrules: { required: true, date: true }, editable: true, datefmt: 'd/m/Y',
                        editoptions: {
                            dataInit: function (elem) {
                                window.setTimeout(function () {
                                    $(elem).datepicker({
                                        dateFormat: 'dd/mm/yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: '1930:2020'
                                    });
                                }, 100);
                            }
                        },
                        formoptions: { rowpos: 7, colpos: 1 }
                    },
                    {
                        name: 'Sexo', index: 'Sexo', width: anchoP(ancho, 10), align: 'left', editrules: { required: true }, editable: true, edittype: "select",
                        editoptions: { value: {'':' ', 'F': 'FEMENINO', 'M': 'MASCULINO', 'O': 'OTRO' } }, formoptions: { rowpos: 7, colpos: 2 },

                    },
                    {
                        name: 'MunicipioResi', index: 'MunicipioResi', width: anchoP(ancho), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "text",
                        editoptions: {
                            dataEvents: [{
                                type: "keypress",
                                fn: function (elem) {

                                    $('#MunicipioResi').trigger("focus");
                                    $('#MunicipioResi').autocomplete(jsonMunicipio, {
                                        autoFocus: true,
                                        multiple: false,
                                        matchContains: true,
                                        cacheLength: 1,
                                        width: 600,
                                        height: 600,
                                        deferRequestBy: 0,
                                        selSeparator: '|',
                                        minChars: 1,
                                        minLength: 0,
                                        delay: 0,
                                        search: function (event, ui) {
                                            window.pageIndex = 0;
                                        }
                                    });
                                }
                            }]
                        }, formoptions: { rowpos: 8, colpos: 1 }
                    },
                    {
                        name: 'Sisben', index: 'Sisben', width: anchoP(ancho, 20), align: 'left',hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 8, colpos: 2 },
                        edittype: "select", editoptions: { value: {'':'','N':'NO APLICA', '1': 'NIVEL 1', '2': 'NIVEL 2', '3': 'NIVEL 3'} }
                    },
                    { name: 'Direccion', index: 'Direccion', width: anchoP(ancho, 100), align: 'left', editrules: { required: true }, editable: true, formoptions: { rowpos: 9, colpos: 1 } },
                    {
                        name: 'Barrio', index: 'Barrio', width: anchoP(ancho, 50), align: 'left', editrules: { required: true }, editable: true,
                        editoptions: {
                            dataEvents: [{
                                type: "keypress",
                                fn: function (elem) {
                                    $('#Barrio').trigger("focus");
                                    $('#Barrio').autocomplete(jsonBarrio, {
                                        autoFocus: true,
                                        multiple: false,
                                        matchContains: true,
                                        cacheLength: 1,
                                        width: 600,
                                        height: 600,
                                        deferRequestBy: 0,
                                        selSeparator: '|',
                                        minChars: 1,
                                        minLength: 0,
                                        delay: 0,
                                        search: function (event, ui) {
                                            window.pageIndex = 0;
                                        }
                                    });
                                }
                            }]
                        }, formoptions: { rowpos: 9, colpos: 2 }
                    },
                    {
                        name: 'Localidad', index: 'Localidad', width: anchoP(ancho, 50), align: 'left', editrules: { required: true }, editable: true,
                        editoptions: {
                            dataEvents: [{
                                type: "keypress",
                                fn: function (elem) {
                                    $('#Localidad').trigger("focus");
                                    $('#Localidad').autocomplete(jsonLocalidad, {
                                        autoFocus: true,
                                        multiple: false,
                                        matchContains: true,
                                        cacheLength: 1,
                                        width: 600,
                                        height: 600,
                                        deferRequestBy: 0,
                                        selSeparator: '|',
                                        minChars: 1,
                                        minLength: 0,
                                        delay: 0,
                                        search: function (event, ui) {
                                            window.pageIndex = 0;
                                        }
                                    });
                                }
                            }]
                        }, formoptions: { rowpos: 10, colpos: 1 }
                    },
                    {
                        name: 'idestrato', index: 'idestrato', width: anchoP(ancho, 20), align: 'left',viewable:false ,hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 10, colpos: 2 },
                        edittype: "select", editoptions: { value: {'':' ', 0: '0 - Bajo-Bajo', 1: '1 - Bajo', 2: '2 - Medio-Bajo', 3: '3 - Medio', 4: '4 - Medio-Alto', 5: '5 - Alto', 6: '6 - Sin Dato', 7: '7 - No Aplica' } }
                    },
                    { name: 'Estrato', index: 'Estrato', width: anchoP(ancho, 20), align: 'left', hidden: true, viewable:true, formoptions: { rowpos: 26, colpos: 1 }},                                                            
                    { name: 'Telefono', index: 'Telefono', width: anchoP(ancho, 20), align: 'left', editrules: { required: false }, editable: true, formoptions: { rowpos: 11, colpos: 1 },editoptions:{dataInit: function(elem){$(elem).bind("keypress",function(e){return numeros(e)})}}},
                    { name: 'celular1', index: 'celular1', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: false }, editable: true, formoptions: { rowpos: 11, colpos: 2 },editoptions:{dataInit: function(elem){$(elem).bind("keypress",function(e){return numeros(e)})}} },
                    { name: 'celular2', index: 'celular2', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: false }, editable: true, formoptions: { rowpos: 12, colpos: 1 },editoptions:{dataInit: function(elem){$(elem).bind("keypress",function(e){return numeros(e)})}}},
                    { name: 'email', index: 'email', align: 'center', hidden: true, editrules: { edithidden: true, required: false }, editable: true, edittype: "text", formoptions: { rowpos: 12, colpos: 2 }, formatter:'email',editoptions:{dataInit: function(elem){$(elem).bind("keypress",function(e){return email(e)})}}},
                    {
                        name: 'administradora', index: 'administradora', width: anchoP(ancho, 50), align: 'left', editrules: { required: true }, editable: true, edittype: "text",
                        editoptions: {
                            dataEvents: [{
                                type: "keypress",
                                fn: function (elem) {

                                    $('#administradora').trigger("focus");
                                    $('#administradora').autocomplete(jsonAdministradora, {
                                        autoFocus: true,
                                        multiple: false,
                                        matchContains: true,
                                        cacheLength: 1,
                                        width: 600,
                                        height: 600,
                                        deferRequestBy: 0,
                                        selSeparator: '|',
                                        minChars: 1,
                                        minLength: 0,
                                        delay: 0,
                                        search: function (event, ui) {
                                            window.pageIndex = 0;
                                        }
                                    });

                                }
                            }]
                        }, formoptions: { rowpos: 13, colpos: 1 }
                    },
                    {
                        name: 'ipsprimaria', index: 'ipsprimaria', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { required: true, edithidden: true }, editable: true, edittype: "text",
                        editoptions: {
                            size:50,
                            dataEvents: [{
                                type: "keypress",
                                fn: function (elem) {
                                  
                                    $('#ipsprimaria').trigger("focus");
                                    $('#ipsprimaria').autocomplete(jsonIpsPrimaria, {
                                        autoFocus: true,
                                        multiple: false,
                                        matchContains: true,
                                        cacheLength: 1,
                                        width: 600,
                                        height: 600,
                                        deferRequestBy: 0,
                                        selSeparator: '|',
                                        minChars: 1,
                                        minLength: 0,
                                        delay: 0,
                                        search: function (event, ui) {
                                            window.pageIndex = 0;
                                        }
                                    });
                                }
                            }]
                        }, formoptions: { rowpos: 13, colpos: 2 }
                    },
                    {
                        name: 'idregimen', index: 'idregimen', width: anchoP(ancho, 30), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select",
                        editoptions: {
                            value: {'':' ', 'C': 'CONTRIBUTIVO', 'S': 'SUBSIDIADO', 'A': 'PARTICULAR' },
                            dataInit: function (elem) { $(elem).width(150); }
                        }, formoptions: { rowpos: 14, colpos: 1 },viewable:false
                    },
                    {
                        name: 'regimen', index: 'regimen', width: anchoP(ancho, 30), align: 'left', formoptions: { rowpos: 8, colpos: 3 }
                    },
                    {
                        name: 'idregia', index: 'idregia', width: anchoP(ancho, 30), align: 'left', hidden: true, editrules: { required: true, edithidden: true }, editable: true, edittype: "select",
                        editoptions: {
                            value: {'':' ', '0': 'OTRO', '1': 'BENEFICIARIO', '2': 'COTIZANTE', '3': 'ADICIONAL', '4': 'EMPLEADO', '5': 'AFILIADO', '6': 'PARTICULAR', '8': 'SUBSIDIADOS-COOMEVA', '9': 'MEDICINA PREPAGADA' },
                            dataInit: function (elem) { $(elem).width(150); }
                        }, formoptions: { rowpos: 14, colpos: 2 },viewable:false
                    },
                    {
                        name: 'regimenafiliacion', index: 'regimenafiliacion', width: anchoP(ancho, 30), align: 'left', formoptions: {rowpos:8,colpos:4}
                    },
                    {
                        name: 'Ocupacion', index: 'Ocupacion', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { required: true, edithidden: true }, editable: true, edittype: "text",
                        editoptions: {
                            dataEvents: [{
                                type: "keypress",
                                fn: function (elem) {

                                    $('#Ocupacion').trigger("focus");
                                    $('#Ocupacion').autocomplete(jsonOcupacion, {
                                        autoFocus: true,
                                        multiple: false,
                                        matchContains: true,
                                        cacheLength: 1,
                                        width: 600,
                                        height: 600,
                                        deferRequestBy: 0,
                                        selSeparator: '|',
                                        minChars: 1,
                                        minLength: 0,
                                        delay: 0,
                                        search: function (event, ui) {
                                            window.pageIndex = 0;
                                        }
                                    });

                                }
                            }]
                        }, formoptions: { rowpos: 15, colpos: 1 }
                    },
                    {
                        name: 'idec', index: 'idec', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select",
                        editoptions: {
                            value: {'':' ', '0': 'SOLTERO', '1': 'UNION LIBRE', '2': 'CASADO', '3': 'DIVORCIADO/SEPARADO', '4': 'VIUDO' },
                            dataInit: function (elem) { $(elem).width(150); }
                        }, formoptions: { rowpos: 15, colpos: 2 },viewable:false
                    },
                    {
                        name: 'estadocivil', index: 'estadocivil', width: anchoP(ancho, 20), align: 'left', hidden: true,formoptions: { rowpos: 9, colpos: 2 }
                    },
                    {
                        name: 'lugarNacimiento', index: 'lugarNacimiento', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "text",
                        editoptions: {
                            dataEvents: [{
                                type: "keypress",
                                fn: function (elem) {

                                    $('#lugarNacimiento').trigger("focus");
                                    $('#lugarNacimiento').autocomplete(jsonMunicipio, {
                                        autoFocus: true,
                                        multiple: false,
                                        matchContains: true,
                                        cacheLength: 1,
                                        width: 600,
                                        height: 600,
                                        deferRequestBy: 0,
                                        selSeparator: '|',
                                        minChars: 1,
                                        minLength: 0,
                                        delay: 0,
                                        search: function (event, ui) {
                                            window.pageIndex = 0;
                                        }
                                    });

                                }
                            }]
                        }, formoptions: { rowpos: 16, colpos: 1 }
                    },
                    {
                        name: 'idet', index: 'idet', width: anchoP(ancho, 60), align: 'left',hidden:true , editrules: { edithidden:true ,required: true }, editable: true, edittype: "select",
                        editoptions: {
                            value: {'': ' ', 6: 'MESTIZO', 1:'INDIGENA',2: 'GITANO', 3: 'RAIZAL', 4: 'PALENQUERO', 5: 'AFROCOLOMBIANO' },
                            dataInit: function (elem) { $(elem).width(150); }
                        }, formoptions: { rowpos: 16, colpos: 2 },viewable:false
                    },
                    {
                        name: 'Etnia', index: 'Etnia', width: anchoP(ancho, 60), align: 'left',formoptions: { rowpos: 10, colpos: 1 }
                    },
                    { name: 'Pueblo', index: 'Pueblo', width: anchoP(ancho, 20), align: 'left', editable: true, hidden: true, editrules: { required: false, edithidden: true }, formoptions: { rowpos: 17, colpos: 1 } },
                    {
                        name: 'iddis', index: 'iddis', width: anchoP(ancho, 60), align: 'left', hidden: true, editrules: { required: true, edithidden: true }, editable: true, edittype: "select",
                        editoptions: {
                            value: {'':' ', 1: 'FISICA', 2: 'PSIQUICA', 3: 'NO APLICA', 4: 'SENSORIAL'},
                            dataInit: function (elem) { $(elem).width(150); }
                        }, formoptions: { rowpos: 17, colpos: 2 },viewable:false
                    },
                    {
                        name: 'discapacidad', index: 'discapacidad', width: anchoP(ancho, 60), align: 'left', hidden: true,formoptions: { rowpos: 11, colpos: 1 }
                    },
                    {
                        name: 'idne', index: 'idne', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select",
                        editoptions: {
                            dataUrl: escolaridad,
                            dataInit: function (elem) { $(elem).width(150); }
                        }, formoptions: { rowpos: 18, colpos: 1 },viewable:false
                    },
                    {
                        name: 'NivelEsc', index: 'NivelEsc', width: anchoP(ancho, 20), align: 'left', hidden: true,formoptions: { rowpos: 11, colpos: 2 }
                    },
                    {
                        name: 'idgp', index: 'idgp', width: anchoP(ancho, 60), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select",
                        editoptions: {
                            dataUrl: grupopoblacional,
                            dataInit: function (elem) { $(elem).width(150); }
                        }, formoptions: { rowpos: 18, colpos: 2 },viewable:false
                    },
                    {
                        name: 'grupopoblacional', index: 'grupopoblacional', width: anchoP(ancho, 60), align: 'left',formoptions: { rowpos: 11, colpos: 3 }
                    },
                    { name: 'acudiente', index: 'acudiente', width: anchoP(ancho, 20), align: 'left', hidden: true, editable: true, formoptions: { rowpos: 19, colpos: 1 }, editrules: { required: false, edithidden: true } },
                    {
                        name: 'idps', index: 'idps', width: anchoP(ancho, 20), align: 'left', hidden: true, editable: true, editrules: { required: false, edithidden: true }, edittype: "select",
                        editoptions: {
                            dataUrl: parentesco,
                            dataInit: function (elem) { $(elem).width(150); }
                        }, formoptions: { rowpos: 19, colpos: 2 },viewable:false
                    },
                    
                    {
                        name: 'parentesco', index: 'parentesco', width: anchoP(ancho, 20), align: 'left', hidden: true,formoptions: { rowpos: 26, colpos: 1 }
                    },
                    {
                        name: 'tipo_id_acudiente', index: 'tipo_id_acudiente', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: false }, editable: true, edittype: "select",
                        editoptions: {
                            dataUrl: tipoID
                        }, formoptions: { rowpos: 20, colpos: 1 }
                    },
                    { name: 'identificacionA', index: 'identificacionA', width: anchoP(ancho, 20), align: 'left', hidden: true, editable: true, editrules: { edithidden: true, required: false }, formoptions: { rowpos: 20, colpos: 2 },editoptions:{dataInit: function(elem){$(elem).bind("keypress",function(e){return numeros(e)})}}},
                    { name: 'acudienteDir', index: 'acudienteDir', width: anchoP(ancho, 20), align: 'left', editable: true, hidden: true, editrules: { required: false, edithidden: true }, formoptions: { rowpos: 21, colpos: 1 }},
                    { name: 'acudienteTel', index: 'acudienteTel', width: anchoP(ancho, 20), align: 'left', editable: true, hidden: true, editrules: { required: false, edithidden: true }, formoptions: { rowpos: 21, colpos: 2 },editoptions:{dataInit: function(elem){$(elem).bind("keypress",function(e){return numeros(e)})}}},
                    { name: 'acudienteEmail', index: 'acudienteEmail', width: anchoP(ancho, 20), align: 'left', editable: true, hidden: true, editrules: { required: false, edithidden: true }, formoptions: { rowpos: 22, colpos: 1 },editoptions:{dataInit: function(elem){$(elem).bind("keypress",function(e){return email(e)})}} },
                    {
                        name: 'idcv', index: 'idcv', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select",
                        editoptions: {
                            dataUrl: condicionVulnerabilidad,
                            dataInit: function (elem) { $(elem).width(150); }
                        }, formoptions: { rowpos: 22, colpos: 2 },viewable:false
                    },
                    {
                        name: 'condicionVulnerabilidad', index: 'condicionVulnerabilidad', width: anchoP(ancho, 20), align: 'left', hidden: true,formoptions: { rowpos: 14, colpos: 1 }
                    },
                    {
                        name: 'HV', index: 'HV', width: anchoP(ancho, 20), align: 'left', editrules: { edithidden: true, required: true },hidden:true ,editable: true, edittype: "select",
                        editoptions: {
                            dataUrl: hechosVictimizantes,
                            dataInit: function (elem) { $(elem).width(150); }
                        }, formoptions: { rowpos: 23, colpos: 1 },viewable:false
                    },
                    {
                        name: 'hechos_victimizantes', index: 'hechos_victimizantes', width: anchoP(ancho, 20),hidden:true ,align: 'left',formoptions: { rowpos: 14, colpos: 3 }
                    },                                        
                    {
                        name: 'idpso', index: 'idpso', width: anchoP(ancho, 20),hidden:true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "select",
                        editoptions: {
                            value: {'':' ', 0: 'NINGUNO', 1: 'ICBF', 2: 'FAMILIAS EN ACCION', 3: 'CDI', 4: 'JOVENES EN ACCION', 5: 'ADULTO MAYOR', 6: 'OTRO' },
                            dataInit: function (elem) { $(elem).width(150); }
                        }, formoptions: { rowpos: 23, colpos: 2 },viewable:false
                    },
                    {
                        name: 'programaSocial', index: 'programaSocial', width: anchoP(ancho, 20), align: 'left',hidden:true,formoptions: { rowpos: 14, colpos: 2 }
                    }                                                                             
                ],
                height: 300,
                width: 1200,
                //shrinkToFit : false,
                //autowidth: true,
                caption: "Paciente",
                pager: "#pagerGrilla",
                editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
                onSelectRow: function (rowid) {
                    mostrar('divEditar')
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrilla').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblId', datosRow.id, estad);
                    asignaAtributo('cmbTipoIdEdit', datosRow.TipoId, 1);
                    asignaAtributo('txtIdPaciente', datosRow.identificacion, 1);
                    asignaAtributo('txtNombre1', datosRow.Nombre1, estad);
                    asignaAtributo('txtNombre2', datosRow.Nombre2, estad);
                    asignaAtributo('txtApellido1', datosRow.Apellido1, estad);
                    asignaAtributo('txtApellido2', datosRow.Apellido2, estad);
                    asignaAtributo('cmbSexo', datosRow.Sexo, estad);
                    asignaAtributo('txtFechaNacimiento', datosRow.FechaNacimiento, estad);
                    asignaAtributo('txtTelefono', datosRow.Telefono, estad);
                    asignaAtributo('txtCelular1', datosRow.celular1, estad);
                    asignaAtributo('txtCelular2', datosRow.celular2, estad);
                    asignaAtributo('txtAdministradora1', datosRow.id_administradora + '-' + datosRow.administradora, estad);
                    asignaAtributo('txtMunicipioResi', datosRow.MunicipioResi, estad);
                    asignaAtributo('txtDireccion', datosRow.Direccion, estad);
                    asignaAtributo('txtObservaciones', datosRow.Observaciones, estad);
                    asignaAtributo('lblIdAnteriores', datosRow.idAnteriores, estad);
                    asignaAtributo('txtEmail', datosRow.email, estad);

                    buscarHC('listGrillaHomologacionIdentifica', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                },
            });
            $('#drag' + ventanaActual.num).find("#listGrilla").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid')
                .navGrid('#pagerGrilla', {
                    del: false, add: true, edit: true, view: true, refresh: true, search: false,
                    edittext: "Editar", addtext: "Adicionar", viewtext: "Ver", refreshtext: "Refrescar"
                }, editOptions, addOptions,{},{}, viewOptions);
            break;
    }
}

function numeros(e)
{
    tecla = (document.all) ? e.keyCode : e.which;
    if(tecla == 8)return true;
    patron = /\d/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}
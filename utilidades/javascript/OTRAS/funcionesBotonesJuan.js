// JavaScript para la programacion de los botones

var ind1=0;
var indprr=0;

function seleccionandoHistoricoAtencion(idEstado, Estado, tipo_evolucion, nombre_evolucion, id_evolucion, Fecha, idAdmision, idProfesionalElaboro, motivoConsulta, enfermedadActual, sintomaticoPiel, sintomaticoRespira){
	  if(valorAtributo('txt_banderaOpcionCirugia') == 'OK')
		  if(tipo_evolucion=='BIOM'){
			  mostrar('divVentanitaLenteBiometria')
			  limpiaAtributo('txt_BIOM_C10',0); 			  		  
			  limpiaAtributo('txt_gestionarPaciente',0); 			  			  
		  }
	  ocultar('divAntecedentes')
	  ocultar('divRevisionPorSistemas')
	  ocultar('divProcedimientosCir')				  
	  ocultar('divExamenFisico')
	  ocultar('divContenidos')	
	  ocultar('divDiagnosticos')	
	  ocultar('divProcedimientos')
	  ocultar('divArchivosAdjuntos')
	  ocultar('divAdmMedicamentos')	
      ocultar('divHojaGastos')		
	  ocultar('divMonitorizacion')
	  ocultar("divFolioPlantilla")
	  ocultar("divOrdenes")
	  ocultar("divHojaRuta")
	  ocultar("divDatosGeneraleshc")
	  ///
	  ocultar("divEncuesta")
	  ocultar("divFactoresRiesgo")
	  ocultar("divFolioEspecialidad")
	  ocultar("divAntecedentesFarmacologicos")
	  ocultar("divSignosVitales")
	  ocultar("divOdonto")
	  ocultar("divIndicaciones")
	
	ocultar('divExamenFisicoDescripcion')	
	limpiaAtributo('txtExamenFisicoDescripcion',0)

	  limpiaAtributo('lblIdDocumento',0); 			  		  
	  limpiaAtributo('txtIdEsdadoDocumento',0); 			  		  			  
	  limpiaAtributo('cmbTipoDocumento',0); 			  
	  limpiarDivEditarJuan('datosDeHistoriaClinicaPaciente')
	  

	  estad= idEstado;	  
	  asignaAtributo('txtIdEsdadoDocumento',idEstado,estad);			  			  
	  asignaAtributo('lblNomEstadoDocumento',Estado,estad);			  			  				
	  asignaAtributo('lblTipoDocumento',tipo_evolucion,estad);			  
	  asignaAtributo('lblDescripcionTipoDocumento',nombre_evolucion,estad);			  
	  asignaAtributo('txtIdTipoDocumento',id_evolucion,estad);		  				
	  asignaAtributo('lblIdDocumento',id_evolucion,estad); 
	  asignaAtributo('lbl_idFecha',Fecha,estad);		// PARA LA IMPRESION EN PDF		
	  asignaAtributo('lblIdAdmision',idAdmision,estad);					
	  asignaAtributo('txtIdProfesionalElaboro',idProfesionalElaboro,estad);	

	  if(idEstado==0 || idEstado==12){
		  estad = 0;
		  desHabilitar('btnProcedimiento',0)	
	  }
	  else desHabilitar('btnProcedimiento',1)	

	  
	  asignaAtributo('txtMotivoConsulta',motivoConsulta,estad)				
	  asignaAtributo('txtEnfermedadActual',enfermedadActual,estad)	
	  asignaAtributo('cmb_SintomaticoPiel',sintomaticoPiel,estad)				
	  asignaAtributo('cmb_SintomaticoRespira',sintomaticoRespira,estad);
	  
	  llenarTablaAlAcordion(id_evolucion, tipo_evolucion, nombre_evolucion);		
}


function enviarBackup(objeto){
	
	 if(confirm("Est� seguro de restaurar la copia: "+objeto+" ?")){
		  if(confirm("Antes de restaurar, desea crear una copia de seguridad en este momento?")){
			document.getElementById('backupEsco').value=objeto;
			adicionarJuan('paginaBackupRestau','/sga/paginas/accionesXml/adicionar_xml.jsp');  	 
		  }
		  else{
   		    document.getElementById('backupEsco').value=objeto;
			adicionarJuan('paginaBackup','/sga/paginas/accionesXml/adicionar_xml.jsp');  	  
		  }
	 }	
}

/*
function abrirVentanasOportunidadMejora(){

	valOport=$('#drag'+ventanaActual.num).find('#lblIdEventoEA').html()
	cargarMenu('calidad/planMejora/administrarPlanMejora.jsp','2-1-3','administrarPlanMejora','administrarPlanMejora','s','s','s','s','s','s')
	alert("Gesti�n de la OPORTUNIDAD DE MEJORA DEL PLAN No= "+valOport); 
	$('#drag'+ventanaActual.num).find('#txtBusIdEvento').val(valOport);
	buscarJuan('administrarPlanMejora','/clinica/paginas/accionesXml/buscar_xml.jsp')
	mostrarBuscar(); 
}
function abrirVentanasOM(){
	 if($('#drag'+ventanaActual.num).find('#lblIdEventoOM').html()!=''){
		valOport=$('#drag'+ventanaActual.num).find('#lblIdEventoOM').html();
		cargarMenu('calidad/planMejora/administrarPlanMejora.jsp','2-1-3','administrarPlanMejora','administrarPlanMejora','s','s','s','s','s','s')
		alert("Gesti�n de la OPORTUNIDAD DE MEJORA DEL PLAN No= "+valOport); 
		$('#drag'+ventanaActual.num).find('#txtBusIdEvento').val(valOport);
		buscarJuan('administrarPlanMejora','/clinica/paginas/accionesXml/buscar_xml.jsp')
		mostrarBuscar(); 
	 }
	 else alert('No existe Oportunidad Mejora Asociada');
}
*/


function verificarCamposAutocompletar(arg){ 

	   var datoscadena=$('#drag'+ventanaActual.num).find('#'+arg).val().split('-');	
	   if( !(datoscadena.length==2 && datoscadena[0]!='' && datoscadena[1]!='') ){   
		   alert('Falta '+arg+'  \n \nEjemplo:  555555-nombre ');  return false;
	   }	
	   else $('#drag'+ventanaActual.num).find('#hidd_Id'+arg).val(datoscadena[0]);
	   

return true;

}



/*

function verificarCamposModificar(arg){ 
    switch (arg){			
		case 'reportarNoConformidad': 
        if( $('#drag'+ventanaActual.num).find('#txtDescripEvento').val()=="" ){  alert('Falta ingresar Descripcion evento');  return false; }
	    if( $('#drag'+ventanaActual.num).find('#cmbproceso').val()=="00"){	        alert('Falta escoger el proceso'); 	 return false; }									 
		if( $('#drag'+ventanaActual.num).find('#cmbNoConformidades').val()=="00"){	alert('Falta escoger la no conformidad del proceso'); return false; } 										 		
		if( $('#drag'+ventanaActual.num).find('#cmbUbicacionArea').val()=="00"){  	alert('Falta escoger el Area donde se presenta el evento ');  return false; }										 		                
		if( $('#drag'+ventanaActual.num).find('#txtFechaEvento').val()==""){  	    alert('Falta ingresar fecha del evento ');  return false; }										 		                
		if( $('#drag'+ventanaActual.num).find('#cmbHoraEvent').val()=="--"){  	    alert('Falta ingresar hora del evento ');  return false; }										 		                		
		if( $('#drag'+ventanaActual.num).find('#txtMinutEvent').val()=="" ){  	    alert('Falta ingresar minuto del evento ');  return false; }										 		                				
		if( $('#drag'+ventanaActual.num).find('#txtMinutEvent').val()>59 || $('#drag'+ventanaActual.num).find('#txtMinutEvent').val()<1){  	    alert('El valor del minuto debe estar entre 1 y 59 ');  return false; }										 		                				
        if( $('#drag'+ventanaActual.num).find('#txtEstadoEvent').val()!="Reportada" ){  alert('El Evento pas� a otro estado y no permite Modificarlo');  return false; }
        if($('#drag'+ventanaActual.num).find('#IdUsuario').val() != $('#drag'+ventanaActual.num).find('#lblElaboro').val()) {		alert('Usted no report� esta No conformidad, por ello no puede modificarla');  return false; }	
		break;
		case 'administrarNoConformidad': 
        if( $('#drag'+ventanaActual.num).find('#txtConcepNoConforCalidad').val()=="" ){  alert('Falta ingresar concepto');  return false; }
	    if( $('#drag'+ventanaActual.num).find('#cmbResponsaDestinoEvento').val()=="00"){alert('Falta escoger el persona destinatario'); 	 return false; }									 
        if( $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="Reportada" ||  $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="Asignada"){  alert('eEl estado del Evento no permite Modificarlo ');  return false; }		
        break;
		
	}
    return true;
}*/


function verificarCamposGuardarUsuario(){   
        if( $('#drag'+ventanaActual.num).find('#cmbTipoIdUsua').val()=="" || $('#drag'+ventanaActual.num).find('#txtId').val()=="" || $('#drag'+ventanaActual.num).find('#txtLogin').val()=="" || $('#drag'+ventanaActual.num).find('#txtContrasena').val()=="" ||$('#drag'+ventanaActual.num).find('#txtContrasena2').val()=="" || $('#drag'+ventanaActual.num).find('#cmbRol').val()==""){ 
		    alert("Debe de ingresar datos Completos");
		    return false;
	    }
		else if($('#drag'+ventanaActual.num).find('#txtContrasena').val()!= $('#drag'+ventanaActual.num).find('#txtContrasena2').val()){
		    alert("La confirmaci�n de la contrase�a no es igual a la contrase�a");
 		    $('#drag'+ventanaActual.num).find('#txtContrasena').val('');	
			$('#drag'+ventanaActual.num).find('#txtContrasena2').val('');		
			$('#drag'+ventanaActual.num).find('#txtContrasena2').focus();	
		    return false;	
		}	
		else if( $('#drag'+ventanaActual.num).find('#lblNomPersona').html()=='' ){
		    alert("Antes de editar informaci�n, debe de buscar un usuario");
			$('#drag'+ventanaActual.num).find('#txtId').val('');
 		    $('#drag'+ventanaActual.num).find('#txtContrasena').val('');	
			$('#drag'+ventanaActual.num).find('#txtContrasena2').val('');	
			$('#drag'+ventanaActual.num).find('#txtLogin').val('');				
			$("#cmbRol option[value='']").attr('selected', 'selected');	
            $('#drag'+ventanaActual.num).find('#txtId').focus();	
		    return false;	
		}		
		else if( $('#drag'+ventanaActual.num).find('#hddId').val() != $('#drag'+ventanaActual.num).find('#cmbTipoIdUsua').val() || $('#drag'+ventanaActual.num).find('#hddTipoIdUsua').val() != $('#drag'+ventanaActual.num).find('#txtId').val()){
		    alert("El Tipo o la Identificaci�n, no pertenecen a "+$('#drag'+ventanaActual.num).find('#lblNomPersona').html());

            $('#drag'+ventanaActual.num).find('#cmbTipoIdUsua').val($('#drag'+ventanaActual.num).find('#hddId').val());
			$('#drag'+ventanaActual.num).find('#txtId').val($('#drag'+ventanaActual.num).find('#hddTipoIdUsua').val())

		    return false;	
		}	

return true;
}



//--------------------------------------------  inicio indicadores eventos 
/*
function verSubIndicadoresJuan(id, descripcion){ 
	

	cuerpoMap=document.getElementById('divIndicadores'); 
	tablabody=document.getElementById('divSublistadoIndicadores');
	tablabody.style.top=cuerpoMap.style.top;
	tablabody.style.left=cuerpoMap.style.left;	





    mostrar('divSublistadoIndicadores');
	$('#drag'+ventanaActual.num).find('#lblIdSubEvento').html('');
	$('#drag'+ventanaActual.num).find('#lblIdSubEvento').html(id);

	$('#drag'+ventanaActual.num).find('#lblTituloCabezaEvento').html('');
	$('#drag'+ventanaActual.num).find('#lblTituloCabezaEvento').html('&nbsp;&nbsp;-&nbsp;&nbsp;  '+descripcion);


    indicadoresJuan('sublistadoIndicadores',id);

	
}
var tabActivo="";

function activarTab(idTab){///
	//alert(idTab);
 	//alert(arg);
	//argAux=arg;//para el guardado de los tab (div) cambia momentaneamente luego en el case se regresa al valor normal

}
*/

function irMesSiguiente(){

   var mes= $('#drag'+ventanaActual.num).find('#lblMes').html();	
   var anio= $('#drag'+ventanaActual.num).find('#lblAnio').html();	   
   
   if(mes==12){  mes=1; anio++;  }
   else         mes++;
   
   $('#drag'+ventanaActual.num).find('#lblAnio').html(anio);	      
   $('#drag'+ventanaActual.num).find('#lblMes').html(mes);
   
   llenarCalendario();
}
function irMesAnterior(){

   var mes= $('#drag'+ventanaActual.num).find('#lblMes').html();	
   var anio= $('#drag'+ventanaActual.num).find('#lblAnio').html();	   
   
   if(mes==1){  mes=12; anio--;  }
   else         mes--;
   
   $('#drag'+ventanaActual.num).find('#lblAnio').html(anio);	      
   $('#drag'+ventanaActual.num).find('#lblMes').html(mes);
   
   llenarCalendario();
}

function cambiarLosTiposDePlanes(valor){  
  $('#drag'+ventanaActual.num).find('#IdTipoPlanes').val(valor);
   llenarCalendario();
 
}
function cambiarLosTiposDeJornada(valor){  
  $('#drag'+ventanaActual.num).find('#IdTipoJornada').val(valor);
   llenarCitas();
 
}

/*
function BuscarrelacionPlanAccionEvento(){  

	 limpiarDivEditarJuan('administrarPlanMejoraEA');
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscar_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestaBuscarrelacionPlanAccionEvento;
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  	
		
	 valores_a_mandar="accion=relacionPlanAccionEvento"; 
	 valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();				 
	 varajaxInit.send(valores_a_mandar); 

}*/
var evento_OM ='';
var tipo_evento_OM = '';
/*
function AjaxRelacionEventoOM(idElemento, TipoEvento){  
     evento_OM = valorAtributo(idElemento);
	 tipo_evento_OM = TipoEvento;

	 limpiarDivEditarJuan('relacionEventoOM');
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscar_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestaAjaxRelacionEventoOM;
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  	
		
	 valores_a_mandar="accion=relacionEventoOM"; 
	 valores_a_mandar=valores_a_mandar+"&IdEvento="+evento_OM;
	 valores_a_mandar=valores_a_mandar+"&TipoEvento="+tipo_evento_OM;	 
	 varajaxInit.send(valores_a_mandar); 

}
function respuestaAjaxRelacionEventoOM(){   
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('rows')!=null){    
					   totalRegistros=raiz.getElementsByTagName('IdProceso').length;  	
					   
					   if(totalRegistros>0){    
						  
						 $('#drag'+ventanaActual.num).find('#lblIdEventoOM').html($.trim(raiz.getElementsByTagName('IdEvento')[0].firstChild.data));	
						 $("#cmbProcesoOM option[value='"+$.trim(raiz.getElementsByTagName('IdProceso')[0].firstChild.data)+"']").attr('selected', 'selected');
						 $('#drag'+ventanaActual.num).find('#txtAspectoMejorarOM').val(raiz.getElementsByTagName('DescripEvento')[0].firstChild.data);
						 $("#cmbTipoPlanOM option[value='"+$.trim( raiz.getElementsByTagName('TipoPlan')[0].firstChild.data  )+"']").attr('selected', 'selected');						 
						 $("#cmbRiesgoOM option[value='"+$.trim(raiz.getElementsByTagName('Riesgo')[0].firstChild.data  )+"']").attr('selected', 'selected');
						 $("#cmbCostoOM option[value='"+$.trim(raiz.getElementsByTagName('Costo')[0].firstChild.data  )+"']").attr('selected', 'selected');
						 $("#cmbVolumenOM option[value='"+$.trim( raiz.getElementsByTagName('Volumen')[0].firstChild.data )+"']").attr('selected', 'selected');						 
						 $('#drag'+ventanaActual.num).find('#lblTotPriorizaOM').html($.trim(raiz.getElementsByTagName('priorizacion')[0].firstChild.data));				 
					     ocultar('divBotonAspectoaMejorar');
					   }
					   else{
						 mostrar('divBotonAspectoaMejorar');
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}

function respuestaBuscarrelacionPlanAccionEvento(){   
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('rows')!=null){    
					   totalRegistros=raiz.getElementsByTagName('IdProceso').length;  	
					   
					   if(totalRegistros>0){    
						  
						 $('#drag'+ventanaActual.num).find('#lblIdEventoEA').html($.trim(raiz.getElementsByTagName('IdEvento')[0].firstChild.data));				 

						 $("#cmbprocesoEA option[value='"+$.trim(raiz.getElementsByTagName('IdProceso')[0].firstChild.data)+"']").attr('selected', 'selected');
						 $('#drag'+ventanaActual.num).find('#txtAspectoMejorar').val(raiz.getElementsByTagName('DescripEvento')[0].firstChild.data);

						 $("#cmbTipoPlan option[value='"+$.trim( raiz.getElementsByTagName('TipoPlan')[0].firstChild.data  )+"']").attr('selected', 'selected');						 

						 $("#cmbRiesgo option[value='"+$.trim(raiz.getElementsByTagName('Riesgo')[0].firstChild.data  )+"']").attr('selected', 'selected');
						 $("#cmbCosto option[value='"+$.trim(raiz.getElementsByTagName('Costo')[0].firstChild.data  )+"']").attr('selected', 'selected');
						 $("#cmbVolumen option[value='"+$.trim( raiz.getElementsByTagName('Volumen')[0].firstChild.data )+"']").attr('selected', 'selected');						 
						 $('#drag'+ventanaActual.num).find('#lblTotPrioriza').html($.trim(raiz.getElementsByTagName('priorizacion')[0].firstChild.data));				 
					     ocultar('divBotonAspectoaMejorar');
					   }
					   else{
						 mostrar('divBotonAspectoaMejorar');
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}


function llenarCalendario(){  

     borrarRegistrosTabla('tablaCalendario');
	  
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarCalendario_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestaListadoCalendario;
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  
	
		
	 valores_a_mandar="accion=listadoCalendario"; 
	 valores_a_mandar=valores_a_mandar+"&mes="+$('#drag'+ventanaActual.num).find('#lblMes').html();				 
	 valores_a_mandar=valores_a_mandar+"&anio="+$('#drag'+ventanaActual.num).find('#lblAnio').html();	
	 valores_a_mandar=valores_a_mandar+"&tipoPlan="+$('#drag'+ventanaActual.num).find('#IdTipoPlanes').val();	 
	 varajaxInit.send(valores_a_mandar); 

}
function respuestaListadoCalendario(){   

	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('indicador')!=null){    
					   totalRegistros=raiz.getElementsByTagName('fecha').length;  	
					   
					   if(totalRegistros>0){    
						  agregarDatosTablaCalendario(raiz,totalRegistros);						   
					   }
					   else{
						   alert("!!!  Error, seguramente hay datos repetidos asi:  en el mismo turno, misma jornada, mismo profesional \n Verifique citas anteriores");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}


var variableMesEditable = true;

function VerificarMesEditable(){
   if(  parseInt($('#drag'+ventanaActual.num).find('#lblMes').html())  >=  parseInt($('#drag'+ventanaActual.num).find('#lblMesHoy').html())    &&   parseInt($('#drag'+ventanaActual.num).find('#lblAnio').html())  >=  parseInt($('#drag'+ventanaActual.num).find('#lblAnioHoy').html())     )  {
	      variableMesEditable= true;
   }
   else{ alert('Mes no editable no podr� modificar !!');  variableMesEditable= true;} // cambiar por true para que si se pueda editar

}


function irMesSiguienteCitas(){

   var mes= $('#drag'+ventanaActual.num).find('#lblMes').html();	
   var anio= $('#drag'+ventanaActual.num).find('#lblAnio').html();	   
   
   if(mes==12){  mes=1; anio++;  }
   else         mes++;
   
   $('#drag'+ventanaActual.num).find('#lblAnio').html(anio);	      
   $('#drag'+ventanaActual.num).find('#lblMes').html(mes);
   VerificarMesEditable(); // una vez cambie el mes se evalua si se puede o no editar campos
   llenarCitas();
}
function irMesAnteriorCitas(){

   var mes= $('#drag'+ventanaActual.num).find('#lblMes').html();	
   var anio= $('#drag'+ventanaActual.num).find('#lblAnio').html();	   
   
   if(mes==1){  mes=12; anio--;  }
   else         mes--;
   
   $('#drag'+ventanaActual.num).find('#lblAnio').html(anio);	      
   $('#drag'+ventanaActual.num).find('#lblMes').html(mes);
   VerificarMesEditable(); // una vez cambie el mes se evalua si se puede o no editar campos
   llenarCitas();
}
function llenarCitas(){  

     borrarRegistrosTabla('tablaCitas');	  
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarCitas_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestaListadoCitas;
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  
	
	 valores_a_mandar="accion=listadoCitas"; 
	 valores_a_mandar=valores_a_mandar+"&mes="+$('#drag'+ventanaActual.num).find('#lblMes').html();				 
	 valores_a_mandar=valores_a_mandar+"&anio="+$('#drag'+ventanaActual.num).find('#lblAnio').html();	
	 valores_a_mandar=valores_a_mandar+"&tipoJornada="+$('#drag'+ventanaActual.num).find('#IdTipoJornada').val();	 
	 valores_a_mandar=valores_a_mandar+"&idProfesional="+$('#drag'+ventanaActual.num).find('#cmbProfesionales').val();	
	 valores_a_mandar=valores_a_mandar+"&EstadoExitoCita="+$('#drag'+ventanaActual.num).find('#cmbEstadoExitoCita').val();		 
	 varajaxInit.send(valores_a_mandar); 
}
function respuestaListadoCitas(){   

	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('indicador')!=null){    
					   totalRegistros=raiz.getElementsByTagName('fecha').length;  	
					   
					   if(totalRegistros>0){    
						  agregarDatosTablaCitas(raiz,totalRegistros);						   
					   }
					   else{
						   alert(" !!!  Error, seguramente hay datos repetidos asi:  en el mismo turno, misma jornada, mismo profesional \n Verifique citas anteriores ");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}


function buscarInformacionPacienteCitaRegistrada(idPaciente, fecha, idTurno){  

    // borrarRegistrosTabla('tablaCitas');	  
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarCitas_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestaBuscarInformacionPacienteCitaRegistrada;   
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  
	
		
	 valores_a_mandar="accion=InformacionPacienteCitaRegistrada";   
	 valores_a_mandar=valores_a_mandar+"&idPaciente="+idPaciente;				 
	 valores_a_mandar=valores_a_mandar+"&fecha="+fecha;				 	 
	 valores_a_mandar=valores_a_mandar+"&idTurno="+idTurno;				 	 	 
	 varajaxInit.send(valores_a_mandar); 

}
function respuestaBuscarInformacionPacienteCitaRegistrada(){   

	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('citasPaciente')!=null){    
					   totalRegistros=raiz.getElementsByTagName('IdCita').length;  	
					   
					   if(totalRegistros>0){  
  				  		   IdCita=raiz.getElementsByTagName('IdCita')[0].firstChild.data; 
  				  		   Municipio=raiz.getElementsByTagName('Municipio')[0].firstChild.data;
  				  		   Direccion=raiz.getElementsByTagName('Direccion')[0].firstChild.data;
  				  		   Acompanante=raiz.getElementsByTagName('Acompanante')[0].firstChild.data;
  				  		   Telefonos=raiz.getElementsByTagName('Telefonos')[0].firstChild.data;
  				  		   Administradora1=raiz.getElementsByTagName('Administradora1')[0].firstChild.data;
  				  		   Administradora2=raiz.getElementsByTagName('Administradora2')[0].firstChild.data;
  				  		   TipoPaciente=raiz.getElementsByTagName('TipoPaciente')[0].firstChild.data;
  				  		   TipoCita=raiz.getElementsByTagName('TipoCita')[0].firstChild.data
  				  		   EstadoCita=raiz.getElementsByTagName('EstadoCita')[0].firstChild.data;
  				  		   Observacion=raiz.getElementsByTagName('Observacion')[0].firstChild.data;
  				  		   idCita=raiz.getElementsByTagName('IdElaboro')[0].firstChild.data;		
						   
						   
						   $('#drag'+ventanaActual.num).find('#lblIdCita').html(IdCita);
						   $('#drag'+ventanaActual.num).find('#txtMunicipio').val(Municipio);
						   $('#drag'+ventanaActual.num).find('#txtDireccionRes').val(Direccion);   
						   $('#drag'+ventanaActual.num).find('#txtNomAcompanante').val(Acompanante);   
						   $('#drag'+ventanaActual.num).find('#txtTelefonos').val(Telefonos);       						   
						   $('#drag'+ventanaActual.num).find('#txtAdministradora1').val(Administradora1);
						   $('#drag'+ventanaActual.num).find('#txtAdministradora2').val(Administradora2); 
						   $("#cmbTipoUsuario option[value='"+TipoPaciente+"']").attr('selected', 'selected');	   
						   $("#cmbTipoCita option[value='"+TipoCita+"']").attr('selected', 'selected');	   
						   $("#cmbEstadoCita option[value='"+EstadoCita+"']").attr('selected', 'selected');	  
						   $('#drag'+ventanaActual.num).find('#txtObservacion').val(Observacion); 	
						   
						   buscarPlatin('listadoDocumenHC','/clinica/paginas/accionesXml/buscarCitas_xml.jsp');
						   
						   
						   
					   }
					   else{
						   alert("!!!  Error, seguramente hay datos repetidos asi:  en el mismo turno, misma jornada, mismo profesional \n Verifique citas anteriores");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}
*/
function buscarInformacionPaciente(){  

    // borrarRegistrosTabla('tablaCitas');	  
	if(valorAtributo('lblIdContacto')!=''){
	   varajaxInit=crearAjax();  
	   varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarCitas_xml.jsp',true);
	   varajaxInit.onreadystatechange=respuestabuscarInformacionPaciente;   
	   varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			
	   valores_a_mandar="accion=InformacionPaciente";   
	   valores_a_mandar=valores_a_mandar+"&idContacto="+valorAtributo('lblIdContacto'); 
	   varajaxInit.send(valores_a_mandar); 
	}

}
function respuestabuscarInformacionPaciente(){  
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('citasPaciente')!=null){    
					   totalRegistros=raiz.getElementsByTagName('Administradora1').length;   	
					   
					   if(totalRegistros>0){  


  				  		   TipoIdPaciente=raiz.getElementsByTagName('TipoIdPaciente')[0].firstChild.data;
  				  		   IdPaciente=raiz.getElementsByTagName('IdPaciente')[0].firstChild.data;						   						   					   
  				  		   nombre1=raiz.getElementsByTagName('Nombre1')[0].firstChild.data;
  				  		   nombre2=raiz.getElementsByTagName('Nombre2')[0].firstChild.data;
  				  		   apellido1=raiz.getElementsByTagName('Apellido1')[0].firstChild.data;
  				  		   apellido2=raiz.getElementsByTagName('Apellido2')[0].firstChild.data;	 
  				  		   idMunicipio=raiz.getElementsByTagName('idMunicipio')[0].firstChild.data;
  				  		   Municipio=raiz.getElementsByTagName('Municipio')[0].firstChild.data;						   
  				  		   Administradora1=raiz.getElementsByTagName('Administradora1')[0].firstChild.data;						   
  				  		   Direccion=raiz.getElementsByTagName('Direccion')[0].firstChild.data;
  				  		   Telefonos=raiz.getElementsByTagName('Telefonos')[0].firstChild.data;
  				  		   TipoPaciente=raiz.getElementsByTagName('TipoPaciente')[0].firstChild.data;
  				  		   fechaNac=raiz.getElementsByTagName('fechaNac')[0].firstChild.data;						   
  				  		   edad=raiz.getElementsByTagName('edad')[0].firstChild.data;						   						   
  				  		   tipoServicio=raiz.getElementsByTagName('tipoServicio')[0].firstChild.data;	 					   

  				  		   fechaInicioIngreso=raiz.getElementsByTagName('fechaInicioIngreso')[0].firstChild.data;						   
  				  		   triageIngreso=raiz.getElementsByTagName('triageIngreso')[0].firstChild.data;		 
  				  		   idOrigenAtencion=raiz.getElementsByTagName('idOrigenAtencion')[0].firstChild.data;						   
  				  		   origenAtencion=raiz.getElementsByTagName('origenAtencion')[0].firstChild.data;
  				  		   destinoPaciente=raiz.getElementsByTagName('destinoPaciente')[0].firstChild.data;						   								   				   



                           asignaAtributo('lblTipoIdPaciente',TipoIdPaciente,0)    
                           asignaAtributo('lblIdentificacionPaciente',IdPaciente,0)
                           asignaAtributo('lblNombre1',nombre1,0)    
                           asignaAtributo('lblNombre2',nombre2,0)    
                           asignaAtributo('lblApellido1',apellido1,0)    
                           asignaAtributo('lblApellido2',apellido2,0)    						   						   						   

                           asignaAtributo('txtMunicipio',idMunicipio+'-'+Municipio,0)    
                           asignaAtributo('lblMunicipio',Municipio,0)    						       
                           asignaAtributo('lblNombreAdmin',Administradora1,0)        
                           asignaAtributo('lblDireccion',Direccion,0)        
                           asignaAtributo('lblTelefono',Telefonos,0)        
                           asignaAtributo('lblTipoUsuario',TipoPaciente,0)        
                           asignaAtributo('lblFechaNacimiento',fechaNac,0)
                           asignaAtributo('lblEdad',edad,0)						    
						   asignaAtributo('lblTipoServicio',tipoServicio,0) 
						   
                           asignaAtributo('lblFechaIngContacto',fechaInicioIngreso,0)  
						   asignaAtributo('lblClasTriage',triageIngreso,0) 	
						   asignaAtributo('lblOrigenAtencion',origenAtencion,0) 	
						   asignaAtributo('txtOrigenAtencion',idOrigenAtencion+'-'+origenAtencion,0) 							   
						   asignaAtributo('lbl_DestinoPaciente',destinoPaciente,0) 							   						   					    
						   
						   /*      						   						   						   						   						   
						   $('#drag'+ventanaActual.num).find('#lblMunicipio').val(Municipio);
						   $('#drag'+ventanaActual.num).find('#lblNombreAdmin').val(Administradora1);						   
						   $('#drag'+ventanaActual.num).find('#lblDireccion').val(Direccion);   
						   $('#drag'+ventanaActual.num).find('#lblTelefono').val(Telefonos);       						   
						   $('#drag'+ventanaActual.num).find('#lblTipoUsuario').val(TipoPaciente);
						   $('#drag'+ventanaActual.num).find('#lblFechaNacimiento').val(fechaNac); 
						   */
						   
					   }
					   else{
//						   alert("Atenci�n: Datos Incompeltos o no se en el Informaci�n del Paciente");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}



function buscarInformacionPacienteExistente(){  

    // borrarRegistrosTabla('tablaCitas');	  
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarCitas_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestabuscarInformacionPacienteExistente;   
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			
	 valores_a_mandar="accion=InformacionPacienteExistente";   
	 var datoscadena=$('#drag'+ventanaActual.num).find('#txtBusIdPaciente').val().split('-');			 
	 valores_a_mandar=valores_a_mandar+"&idPaciente="+datoscadena[0];	 
	// alert(valores_a_mandar);
	 varajaxInit.send(valores_a_mandar); 

}
function respuestabuscarInformacionPacienteExistente(){  
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('citasPaciente')!=null){    
					   totalRegistros=raiz.getElementsByTagName('Municipio').length;  	
					   
					   if(totalRegistros>0){  
  				  		  
  				  		   Municipio=raiz.getElementsByTagName('Municipio')[0].firstChild.data;
  				  		   Direccion=raiz.getElementsByTagName('Direccion')[0].firstChild.data;
  				  		   Acompanante=raiz.getElementsByTagName('Acompanante')[0].firstChild.data;
  				  		   Telefonos=raiz.getElementsByTagName('Telefonos')[0].firstChild.data;
  				  		   Administradora1=raiz.getElementsByTagName('Administradora1')[0].firstChild.data;
  				  		   Administradora2=raiz.getElementsByTagName('Administradora2')[0].firstChild.data;
  				  		   TipoPaciente=raiz.getElementsByTagName('TipoPaciente')[0].firstChild.data;

  				  		   //TipoCita=raiz.getElementsByTagName('TipoCita')[0].firstChild.data
  				  		   //EstadoCita=raiz.getElementsByTagName('EstadoCita')[0].firstChild.data;
  				  		   Observacion=raiz.getElementsByTagName('Observacion')[0].firstChild.data;
  				  		   //idCita=raiz.getElementsByTagName('IdElaboro')[0].firstChild.data;	
						   
						   $('#drag'+ventanaActual.num).find('#lblIdCita').html('');
						   $('#drag'+ventanaActual.num).find('#txtMunicipio').val(Municipio);
						   $('#drag'+ventanaActual.num).find('#txtDireccionRes').val(Direccion);   
						   $('#drag'+ventanaActual.num).find('#txtNomAcompanante').val(Acompanante);   
						   $('#drag'+ventanaActual.num).find('#txtTelefonos').val(Telefonos);       						   
						   $('#drag'+ventanaActual.num).find('#txtAdministradora1').val(Administradora1);
						   $('#drag'+ventanaActual.num).find('#txtAdministradora2').val(Administradora2); 

						   
						   
						   $("#cmbTipoUsuario option[value='"+TipoPaciente+"']").attr('selected', 'selected');	   
						   $("#cmbTipoCita option[value='00']").attr('selected', 'selected');	   
						   $("#cmbEstadoCita option[value='00']").attr('selected', 'selected');	  
						   $('#drag'+ventanaActual.num).find('#txtObservacion').val(Observacion); 	
   						   buscarPlatin('listadoDocumenHC','/clinica/paginas/accionesXml/buscarCitas_xml.jsp');
					   }
					   else{
						   alert("Caramba sin datos en el Informaci�n del Paciente");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}

function buscarInformacionPacienteExistenteListaEspera(){  

    // borrarRegistrosTabla('tablaCitas');	  
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarCitas_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestabuscarInformacionPacienteExistenteListaEspera;   
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			
	 valores_a_mandar="accion=InformacionPacienteExistente";   
	 var datoscadena=$('#drag'+ventanaActual.num).find('#txtBusIdPacienteListaEspera').val().split('-');			 
	 valores_a_mandar=valores_a_mandar+"&idPaciente="+datoscadena[0];	 
	// alert(valores_a_mandar);
	 varajaxInit.send(valores_a_mandar); 

}
function respuestabuscarInformacionPacienteExistenteListaEspera(){   
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('citasPaciente')!=null){    
					   totalRegistros=raiz.getElementsByTagName('Municipio').length;  	
					   
					   if(totalRegistros>0){  
  				  		  
  				  		   Municipio=raiz.getElementsByTagName('Municipio')[0].firstChild.data;
  				  		   Direccion=raiz.getElementsByTagName('Direccion')[0].firstChild.data;
  				  		   Acompanante=raiz.getElementsByTagName('Acompanante')[0].firstChild.data;
  				  		   Telefonos=raiz.getElementsByTagName('Telefonos')[0].firstChild.data;
  				  		   Administradora1=raiz.getElementsByTagName('Administradora1')[0].firstChild.data;
  				  		   Administradora2=raiz.getElementsByTagName('Administradora2')[0].firstChild.data;
  				  		   TipoPaciente=raiz.getElementsByTagName('TipoPaciente')[0].firstChild.data;
   				  		   fechaNac=raiz.getElementsByTagName('fechaNac')[0].firstChild.data;	
						   sexo=raiz.getElementsByTagName('sexo')[0].firstChild.data;	
  				  		   //TipoCita=raiz.getElementsByTagName('TipoCita')[0].firstChild.data
  				  		   //EstadoCita=raiz.getElementsByTagName('EstadoCita')[0].firstChild.data;
  				  		   Observacion=raiz.getElementsByTagName('Observacion')[0].firstChild.data;
  				  		   //idCita=raiz.getElementsByTagName('IdElaboro')[0].firstChild.data;	
						   
//						   $('#drag'+ventanaActual.num).find('#lblIdCita').html('');
						   $('#drag'+ventanaActual.num).find('#txtMunicipioListaEspera').val(Municipio);
						   $('#drag'+ventanaActual.num).find('#txtDireccionResListaEspera').val(Direccion);   
						   $('#drag'+ventanaActual.num).find('#txtNomAcompananteListaEspera').val(Acompanante);   
						   $('#drag'+ventanaActual.num).find('#txtTelefonosListaEspera').val(Telefonos);       						   
						   $('#drag'+ventanaActual.num).find('#txtAdministradora1ListaEspera').val(Administradora1);
						   $('#drag'+ventanaActual.num).find('#txtAdministradora2ListaEspera').val(Administradora2); 
   						   $('#drag'+ventanaActual.num).find('#txtFechaNacimientoListaEspera').val(fechaNac); 						   
						   $("#cmbTipoUsuarioListaEspera option[value='"+TipoPaciente+"']").attr('selected', 'selected');	   
						   $("#cmbSexoListaEspera option[value='"+sexo+"']").attr('selected', 'selected');	   
						   //$("#cmbTipoCita option[value='00']").attr('selected', 'selected');	   
						   //$("#cmbEstadoCita option[value='00']").attr('selected', 'selected');	  
						   $('#drag'+ventanaActual.num).find('#txtObservacion').val(Observacion); 	
   						   buscarPlatin('listHistoricoListaEspera','/clinica/paginas/accionesXml/buscarCitas_xml.jsp');						   
   						   setTimeout("buscarPlatin('listadoDocumenHCListaEspera','/clinica/paginas/accionesXml/buscarCitas_xml.jsp')",300);//ubo problemas con otro list el de eventos relacion list												   


						   						   
					   }
					   else{
						   alert("Caramba sin datos en el Lista Espera");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}



function listadoCitas(){  

	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('indicador')!=null){    
					   totalRegistros=raiz.getElementsByTagName('fecha').length;  	
					   
					   if(totalRegistros>0){    
						  agregarDatosTablaCitas(raiz,totalRegistros);						   
					   }
					   else{
						   alert("!!!  Error, seguramente hay datos repetidos con este profesional asi:  en el mismo turno, misma jornada \n Verifique citas anteriores");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}
var semana=0;
/*function agregarDatosTablaHorasCitas(Nregistro){   semana++;  
	  TH=document.createElement("TH");
	  TH.id=i; 
	  TH.className='citas_th56';
	  	
								tabla=document.createElement("table");	//numero del dia	
								tabla.setAttribute('height','10%')
								 
								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.innerHTML='&nbsp;';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);
								  
								//  alert($('#drag'+ventanaActual.num).find('#IdTipoJornada').val());
                                if($('#drag'+ventanaActual.num).find('#IdTipoJornada').val()=='a'){	  
								  
								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='1';								  
								  tr.appendChild(th);
									th=document.createElement("Td");
									th.className
									th.innerHTML='02:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='2';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='03:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='3';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='03:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='4';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='04:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);
								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='5';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='04:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='6';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='05:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='7';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='05:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='8';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='06:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);	
								  
								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='9';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='06:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='10';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='07:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);	

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='11';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='07:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);	

								  

							}else if($('#drag'+ventanaActual.num).find('#IdTipoJornada').val()=='m'){	  
								  
								  tr=document.createElement("TR");
								  tr.id='hora_'+semana+'_1';    //   alert('hora_'+semana+'_1');
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='1';
								  tr.appendChild(th); 
									th=document.createElement("Td");
//									th.id='1';																											
									th.innerHTML='08:00';//jjj
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='2';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.id=2;																											
									th.innerHTML='08:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='3';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='09:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='4';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='09:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);
								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='5';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='10:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='6';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='10:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='7';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='11:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='8';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='11:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);	
								  
								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='9';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='12:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='10';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='12:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);	

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='11';								  
								  tr.appendChild(th);								  
									th=document.createElement("Td");
									th.innerHTML='01:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);	

							}else if($('#drag'+ventanaActual.num).find('#IdTipoJornada').val()=='t'){	  
							      tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='1';								  
								  tr.appendChild(th);
									th=document.createElement("Td");
									th.innerHTML='01:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);
								
								
								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='2';								  
								  tr.appendChild(th);
								  
									th=document.createElement("Td");
									th.innerHTML='02:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='3';								  
								  tr.appendChild(th);
								  
									th=document.createElement("Td");
									th.innerHTML='02:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='4';								  
								  tr.appendChild(th);
								  
									th=document.createElement("Td");
									th.innerHTML='03:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='5';								  
								  tr.appendChild(th);
								  
									th=document.createElement("Td");
									th.innerHTML='03:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);
								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='6';								  
								  tr.appendChild(th);
								  
									th=document.createElement("Td");
									th.innerHTML='04:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='7';								  
								  tr.appendChild(th);
								  
									th=document.createElement("Td");
									th.innerHTML='04:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='8';								  
								  tr.appendChild(th);
								  
									th=document.createElement("Td");
									th.innerHTML='05:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='9';								  
								  tr.appendChild(th);
								  
									th=document.createElement("Td");
									th.innerHTML='05:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);	

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='10';								  
								  tr.appendChild(th);
								  
									th=document.createElement("Td");
									th.innerHTML='06:00';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);

								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.className='citas_th561';
									th.innerHTML='11';								  
								  tr.appendChild(th);
								  
									th=document.createElement("Td");
									th.innerHTML='06:30';								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);								

								
							}
							TH.appendChild(tabla);	  							
							
	  Nregistro.appendChild(TH);
}
*/
var meridiano= 'am';
var contTurno=0;
function agregarDatosTablaCitas(raiz, totalRegistros){    
         

         tablabody=document.getElementById('tablaCitas').lastChild;
		 var cuadro=0,diaInicio=0; 
		 
		 diaInicio=raiz.getElementsByTagName('num_dia')[0].firstChild.data;
	     diaf=raiz.getElementsByTagName('diaf')[totalRegistros-1].firstChild.data;		 
		 dia=1;
		  
		 for(i=1;i<7;i++){	//semanas		horizontales 
		  
			  Nregistro=document.createElement("TR");
			  Nregistro.id=i;
			  
			  
/**********horas *************/
//alert(i);
agregarDatosTablaHorasCitas(Nregistro);
/**********horas fin**********/

			  
	  		  for(j=1;j<=7;j++){ 
  			      cuadro++;
						 if(cuadro>=diaInicio && dia<=diaf){ //alert('-entr-');
							TH=document.createElement("TH");
							TH.id='cua'+dia;   
							TH.className='citas_th54';
							
								tabla=document.createElement("table");	//numero del dia	
								tabla.setAttribute('height','10%')
								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.innerHTML=dia;								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);	
								TH.appendChild(tabla);
								
								/*****************************************tabla principal donde se agregan los eventos**************/
								tablaC=document.createElement("table");	
								tablaC.setAttribute('height','90%')
								  trC=document.createElement("TR"); 
								  //trC.setAttribute('onmouseover',"onmouseoverTurnoHora('"+i+"','"+cuadro+"')");
									thC=document.createElement("Td");
									thC.id='cuadro'+dia;	 
								    thC.setAttribute('width','90%')																		
								  trC.appendChild(thC);								  
								  tablaC.appendChild(trC);	
								TH.appendChild(tablaC);		
								/*****************************************fin tabla principal donde se agregan los eventos**************/								
							
							
							Nregistro.appendChild(TH);	
							//TH.innerHTML='cuadro'+dia;	
							dia++;
			             }
						 else{
							TH=document.createElement("TH");
							TH.id=cuadro; 							
							TH.innerHTML='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							TH.className='citas_th55';
							Nregistro.appendChild(TH);							 
						 
						 }							
			  }/*1 a 7*/			  
			  tablabody.appendChild(Nregistro); 
		 }/*1 a 5*/
		 
		/*ahora a llenar los cuadros del dia en la tabla principal de eventos*/
		idTurno=0;
		semana=0;
		 for(m=0;m<totalRegistros;m++){ 
            fecha=raiz.getElementsByTagName('fecha')[m].firstChild.data;
		    diaf=raiz.getElementsByTagName('diaf')[m].firstChild.data;
			tipoAgenda=raiz.getElementsByTagName('tipoAgenda')[m].firstChild.data;		
			planeada=raiz.getElementsByTagName('planeada')[m].firstChild.data;
		    idPaciente=raiz.getElementsByTagName('idPaciente')[m].firstChild.data; 
			nombrePaciente=raiz.getElementsByTagName('nombrePaciente')[m].firstChild.data;
			estadoCita=raiz.getElementsByTagName('estadoCita')[m].firstChild.data;	
			tipoCita=raiz.getElementsByTagName('tipoCita')[m].firstChild.data;
			title=raiz.getElementsByTagName('title')[m].firstChild.data;			
		//	alert(m+'..'+totalRegistros);
			cuadros=document.getElementById('cuadro'+diaf);// Se trae el numero del cuadro
		    tr=document.createElement("tr");
		    td=document.createElement("td");
/*			cuadros2=document.getElementById('cuadro2'+diaf);// Se trae el numero del cuadroxxxxxxxx
		    tr2=document.createElement("tr");
		    td2=document.createElement("td");
		    tr2.appendChild(td2);
		    cuadros2.appendChild(tr2); */
			//alert('citas_td01_o'+'-'+'citas_td'+tipoAgenda+'_'+estadoCita);
			
			td.className='citas_td'+tipoAgenda+'_'+estadoCita;

			
				if(nombrePaciente.length>1){
				      idTurno=idTurno+1;
				      if(idTurno==12){ idTurno=1; /*  contTurno = contTurno+11;  alert(contTurno); */ }
					 
					  td.innerHTML=tipoCita+'-'+nombrePaciente.substring(0,20);
					  td.setAttribute('title',idTurno+'-'+title);
					  //trC.setAttribute('onmouseover',"onmouseoverTurnoHora('"+i+"','"+cuadro+"')");
					  
					  if(variableMesEditable){ // para evaluar si el mes es anterior al vigente no se podra modificar nada		   
							if(planeada==0 )
							   td.setAttribute('onclick',"abrirVentanaCitasRegistradas('"+idPaciente+"','"+nombrePaciente+"','"+estadoCita+"','"+diaf+"','"+idTurno+"')"); // atencion usuario: visualizar la informacion anteriormente registrada de una cita creada
							else if(planeada==1){					  
							   td.setAttribute('onclick',"abrirVentanaAgenda('"+diaf+"','"+idTurno+"')");	// director tecnico			  
							}
							else if(planeada==3){// disponible
							   td.setAttribute('onclick',"editarAgendaDisponible('"+diaf+"','"+idTurno+"')"); 		// en blanco 		  				   
							}
					  }
				}
		    //td.innerHTML=idPaciente+'-'+nombrePaciente;
		    tr.appendChild(td);
		    cuadros.appendChild(tr); 
		 }

	
}
function onmouseoverTurnoHora(idTurno){// alert(idTurno); //jjj

		th=document.getElementById('hora_1_1');
		th.className='titulos'; 
		//th.innerHTML ='ffff';
		//th.setAttribute('bgcolor','FFFF00'); 
		


}


function abrirVentanaAgenda(  diaf, idTurno){// director tecnico			  

				
			  if($('#drag'+ventanaActual.num).find('#chkDejarDisponiblePlaneadas').attr('checked')==true){ // SI ESTA CHECKEADO SE LIMPIARA
				  fecha= diaf+'/'+$('#drag'+ventanaActual.num).find('#lblMes').html()+'/'+$('#drag'+ventanaActual.num).find('#lblAnio').html();
				  idProfesional=$('#drag'+ventanaActual.num).find('#cmbProfesionales').val();  
				  IdTipoAgenda=$('#drag'+ventanaActual.num).find('#IdTipoAgenda').val();
				  Jornada=$('#drag'+ventanaActual.num).find('#IdTipoJornada').val();   
				  dejarDisponiblePlaneadasAjax('datosDejarDisponiblePlaneadas',idProfesional, fecha, Jornada, IdTipoAgenda, idTurno);	
			  }	
}

function abrirVentanaCitasRegistradas(idPaciente, nombrePaciente, estadoCita, diaf, idTurno){  //las que ya fueron creadas solo se permitiran modificarxxxxxx
  limpiarDatosPacienteCita();	
  limpiarListadosTotales('listDocumentosPaciente');
  if($('#drag'+ventanaActual.num).find('#planeada').val()=='0'){ // como es no planeada con datos del paciente
      
	  if($('#drag'+ventanaActual.num).find('#chkDejarDisponibleNoPlaneadas').attr('checked')==false){ // si no intenta dejar disponible 
		  fecha= diaf+'/'+$('#drag'+ventanaActual.num).find('#lblMes').html()+'/'+$('#drag'+ventanaActual.num).find('#lblAnio').html();
		  mostrar('divSubVentanaAgendaNoPlaneada');
		  buscarInformacionPacienteCitaRegistrada(idPaciente, fecha, idTurno );
		  ocultar('divBotonGuardarCita');
		  mostrar('divBotonModificarCita');	  

		  $('#drag'+ventanaActual.num).find('#txtBusIdPaciente').val(idPaciente+'-'+nombrePaciente);	   
		  $('#drag'+ventanaActual.num).find('#txtBusIdPaciente').attr('disabled','disabled');		   
		  $('#drag'+ventanaActual.num).find('#cmbTipoId').attr('disabled','disabled');		   
		  $('#drag'+ventanaActual.num).find('#txtIdPaciente').attr('disabled','disabled');		   
		  $('#drag'+ventanaActual.num).find('#txtNombre1').attr('disabled','disabled');		   
		  $('#drag'+ventanaActual.num).find('#txtNombre2').attr('disabled','disabled');		   	  
		  $('#drag'+ventanaActual.num).find('#txtApellido1').attr('disabled','disabled');		   
		  $('#drag'+ventanaActual.num).find('#txtApellido2').attr('disabled','disabled');	
		  
          $('#drag'+ventanaActual.num).find('#cmbProfesionalesListaEsperaCita').attr('disabled','');	
		  $("#cmbProfesionalesListaEsperaCita option[value='"+$('#drag'+ventanaActual.num).find('#cmbProfesionales').val()+"']").attr('selected', 'selected');
		  $("#cmbIdTurno option[value='"+$.trim(idTurno)+"']").attr('selected', 'selected');
		  $("#cmbIdJornada option[value='"+$('#drag'+ventanaActual.num).find('#IdTipoJornada').val()+"']").attr('selected', 'selected');		  		  
		  
		  $('#drag'+ventanaActual.num).find('#lblFecha').html(fecha);
		  $('#drag'+ventanaActual.num).find('#lblHora').html('hora');	
	  }
	  else{   // si intenta borrar un registro
     	    $('#drag'+ventanaActual.num).find('#chkDejarDisponibleNoPlaneadas').attr('checked','');   																				  
			if(estadoCita=='p' || estadoCita=='o'){
			    if(confirm('    ATENCION ! ! ! \n Est� seguro de limpiar este registro? \n Se perder� informaci�n de esta cita \n Conste que le advert� ')){
				fecha= diaf+'/'+$('#drag'+ventanaActual.num).find('#lblMes').html()+'/'+$('#drag'+ventanaActual.num).find('#lblAnio').html();
				idProfesional=$('#drag'+ventanaActual.num).find('#cmbProfesionales').val();
				IdTipoAgenda=$('#drag'+ventanaActual.num).find('#IdTipoAgenda').val();
				Jornada=$('#drag'+ventanaActual.num).find('#IdTipoJornada').val();   
				dejarDisponiblePlaneadasAjax('datosDejarDisponibleNoPlaneadas',idProfesional, fecha, Jornada, IdTipoAgenda, idTurno);	
				}
			}
			else alert('Este tipo de cita no se puede eliminar, solo se permiten eliminar las pendientes o confirmadas');
	  }

  }
  else alert(' Ud no tiene permisos de agendar No planeadas ');
 
}




function editarAgendaDisponible(diaf, idTurno){
	
  limpiarDatosPacienteCita();	
  limpiarListadosTotales('listDocumentosPaciente');
  mostrar('divBotonGuardarCita');
  ocultar('divBotonModificarCita');	  
  
	
  fecha= diaf+'/'+$('#drag'+ventanaActual.num).find('#lblMes').html()+'/'+$('#drag'+ventanaActual.num).find('#lblAnio').html();

  IdTipoAgenda=$('#drag'+ventanaActual.num).find('#IdTipoAgenda').val();
  Jornada=$('#drag'+ventanaActual.num).find('#IdTipoJornada').val();   


  if($('#drag'+ventanaActual.num).find('#planeada').val()=='1'){ // los casos de planeada::  director tecnico
      idProfesional=$('#drag'+ventanaActual.num).find('#cmbProfesionales').val();

      if($('#drag'+ventanaActual.num).find('#chkTodaLaJornada').attr('checked')==false){ // para guardar individual cada celda uno a uno
         // alert(' 1.1 '+fecha+'--'+idTurno+'**'+idProfesional+'..'+IdTipoAgenda+'__'+Jornada);	
		  ingresoDatosAgendaAjax('datosAgendaPlaneadaIndividual',idProfesional, fecha, Jornada, IdTipoAgenda, idTurno);	
		  
	  }
	  else { // para guardar la jornada
	   alert('1.2')  
		  
	  }
	  
  }
  else { // para mandar a abrir la ventana de citas atusuario limpia para editar
  

	 idProfesional=$('#drag'+ventanaActual.num).find('#cmbProfesionalesListaEsperaCita').val();
	 $("#cmbIdTurno option[value='"+$.trim(idTurno)+"']").attr('selected', 'selected');		  	 
	 $('#drag'+ventanaActual.num).find('#lblFecha').html(fecha);
	 $('#drag'+ventanaActual.num).find('#lblHora').html('hora');	
	 mostrar('divSubVentanaAgendaNoPlaneada'); 


  }
	 
}	
/*
function guardarAsignacionCitaNoPlaneada(){
	
   if(verificarCamposGuardar('guardarAsignacionCitaNoPlaneada')){	
   
	   fecha= $('#drag'+ventanaActual.num).find('#lblFecha').html();
	   idProfesional=$('#drag'+ventanaActual.num).find('#cmbProfesionales').val();
	   IdTipoAgenda=$('#drag'+ventanaActual.num).find('#IdTipoAgenda').val();
	   Jornada=$('#drag'+ventanaActual.num).find('#IdTipoJornada').val();   

	   idTurno= $('#drag'+ventanaActual.num).find('#cmbIdTurno').val()
	   
	   tipoCita=$('#drag'+ventanaActual.num).find('#cmbTipoCita').val();   
	   estadoCita=$('#drag'+ventanaActual.num).find('#cmbEstadoCita').val();  
	   varajaxInit=crearAjax();  
	   varajaxInit.open("POST",'/clinica/paginas/accionesXml/modificar_xml.jsp',true);
	   varajaxInit.onreadystatechange=respuestaingresodatosAgendaNoPlaneada;
	   varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  	   
	   
	   if($('#drag'+ventanaActual.num).find('#txtBusIdPaciente').val()!=''){// para pacientes existentes
		   valores_a_mandar="accion=datosAgendaNoPlaneadaPacientExistente"; 
		   valores_a_mandar=valores_a_mandar+"&idPaciente="+$('#drag'+ventanaActual.num).find('#hidd_IdPaciente').val();		   
	   }
	   else{ //para nuevos pacientes
		   valores_a_mandar="accion=datosAgendaNoPlaneadaPacientNuevo"; 
		   valores_a_mandar=valores_a_mandar+"&TipoId="+$('#drag'+ventanaActual.num).find('#cmbTipoId').val();   
		   valores_a_mandar=valores_a_mandar+"&idPaciente="+$('#drag'+ventanaActual.num).find('#hidd_IdPaciente').val();		   
		   valores_a_mandar=valores_a_mandar+"&Nombre1="+$('#drag'+ventanaActual.num).find('#txtNombre1').val();		   		   
		   valores_a_mandar=valores_a_mandar+"&Nombre2="+$('#drag'+ventanaActual.num).find('#txtNombre2').val();		   		   
		   valores_a_mandar=valores_a_mandar+"&Apellido1="+$('#drag'+ventanaActual.num).find('#txtApellido1').val();		   		   
		   valores_a_mandar=valores_a_mandar+"&Apellido2="+$('#drag'+ventanaActual.num).find('#txtApellido2').val();	

		   valores_a_mandar=valores_a_mandar+"&Telefonos="+$('#drag'+ventanaActual.num).find('#txtTelefonos').val();	
		   valores_a_mandar=valores_a_mandar+"&DireccionRes="+$('#drag'+ventanaActual.num).find('#txtDireccionRes').val();	
		   valores_a_mandar=valores_a_mandar+"&NomAcompanante="+$('#drag'+ventanaActual.num).find('#txtNomAcompanante').val();			   

	   }
	   
	   valores_a_mandar=valores_a_mandar+"&idProfesional="+idProfesional;		
	   valores_a_mandar=valores_a_mandar+"&fecha="+fecha;	
	   valores_a_mandar=valores_a_mandar+"&Jornada="+Jornada;	
	   valores_a_mandar=valores_a_mandar+"&IdTipoAgenda="+IdTipoAgenda;	
	   valores_a_mandar=valores_a_mandar+"&idTurno="+idTurno;		
	   valores_a_mandar=valores_a_mandar+"&tipoCita="+tipoCita;		   
	   valores_a_mandar=valores_a_mandar+"&estadoCita="+estadoCita;	
	   valores_a_mandar=valores_a_mandar+"&TipoUsuario="+$('#drag'+ventanaActual.num).find('#cmbTipoUsuario').val();		
	   valores_a_mandar=valores_a_mandar+"&Observacion="+$('#drag'+ventanaActual.num).find('#txtObservacion').val();			   
	   
	   
	   valores_a_mandar=valores_a_mandar+"&idMunicipioReside="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtMunicipio').val();	
	   valores_a_mandar=valores_a_mandar+"&Telefonos="+$('#drag'+ventanaActual.num).find('#txtTelefonos').val();	
	   valores_a_mandar=valores_a_mandar+"&DireccionRes="+$('#drag'+ventanaActual.num).find('#txtDireccionRes').val();	
	   valores_a_mandar=valores_a_mandar+"&NomAcompanante="+$('#drag'+ventanaActual.num).find('#txtNomAcompanante').val();			   
	   
	   
	   valores_a_mandar=valores_a_mandar+"&idAdministradora1="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtAdministradora1').val();		   	   	   
	   valores_a_mandar=valores_a_mandar+"&idAdministradora2="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtAdministradora2').val();	
	   valores_a_mandar=valores_a_mandar+"&idListaEspera="+$('#drag'+ventanaActual.num).find('#lblIdListaEspera').html();		   
	  // alert('ffffffff '+valores_a_mandar);
	   varajaxInit.send(valores_a_mandar);
	}   

}
*/
function guardarListaEspera(){
	
   if(verificarCamposGuardar('guardarAsignacionCitaNoPlaneadaListaEspera')){	
   
	   IdTipoAgenda=$('#drag'+ventanaActual.num).find('#IdTipoAgendaListaEspera').val();
	   varajaxInit=crearAjax();  
	   varajaxInit.open("POST",'/clinica/paginas/accionesXml/modificar_xml.jsp',true);
	   varajaxInit.onreadystatechange=respuestaguardarListaEspera;
	   varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  
		  
	   if($('#drag'+ventanaActual.num).find('#txtBusIdPacienteListaEspera').val()!=''){// para pacientes existentes
		   valores_a_mandar="accion=datosAgendaNoPlaneadaPacientExistenteListaEspera"; 
		   valores_a_mandar=valores_a_mandar+"&idPaciente="+$('#drag'+ventanaActual.num).find('#hidd_IdPacienteListaEspera').val();		   
	   }
	   else{ //para nuevos pacientes
		   valores_a_mandar="accion=datosAgendaNoPlaneadaPacientNuevoListaEspera"; 
		   valores_a_mandar=valores_a_mandar+"&TipoId="+$('#drag'+ventanaActual.num).find('#cmbTipoIdListaEspera').val();   
		   valores_a_mandar=valores_a_mandar+"&idPaciente="+$('#drag'+ventanaActual.num).find('#hidd_IdPacienteListaEspera').val();		   
		   valores_a_mandar=valores_a_mandar+"&Nombre1="+$('#drag'+ventanaActual.num).find('#txtNombre1ListaEspera').val();		   		   
		   valores_a_mandar=valores_a_mandar+"&Nombre2="+$('#drag'+ventanaActual.num).find('#txtNombre2ListaEspera').val();		   		   
		   valores_a_mandar=valores_a_mandar+"&Apellido1="+$('#drag'+ventanaActual.num).find('#txtApellido1ListaEspera').val();		   		   
		   valores_a_mandar=valores_a_mandar+"&Apellido2="+$('#drag'+ventanaActual.num).find('#txtApellido2ListaEspera').val();	
		   valores_a_mandar=valores_a_mandar+"&FechaNacimiento="+$('#drag'+ventanaActual.num).find('#txtFechaNacimientoListaEspera').val();		
		   valores_a_mandar=valores_a_mandar+"&sexo="+$('#drag'+ventanaActual.num).find('#cmbSexoListaEspera').val();	

		   valores_a_mandar=valores_a_mandar+"&Telefonos="+$('#drag'+ventanaActual.num).find('#txtTelefonosListaEspera').val();	
		   valores_a_mandar=valores_a_mandar+"&DireccionRes="+$('#drag'+ventanaActual.num).find('#txtDireccionResListaEspera').val();	
		   valores_a_mandar=valores_a_mandar+"&NomAcompanante="+$('#drag'+ventanaActual.num).find('#txtNomAcompananteListaEspera').val();			   
	   }

	   valores_a_mandar=valores_a_mandar+"&Telefonos="+$('#drag'+ventanaActual.num).find('#txtTelefonosListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&idMunicipioReside="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtMunicipioListaEspera').val();
	   valores_a_mandar=valores_a_mandar+"&DireccionRes="+$('#drag'+ventanaActual.num).find('#txtDireccionResListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&NomAcompanante="+$('#drag'+ventanaActual.num).find('#txtNomAcompananteListaEspera').val();		
	   valores_a_mandar=valores_a_mandar+"&FechaNacimiento="+$('#drag'+ventanaActual.num).find('#txtFechaNacimientoListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&sexo="+$('#drag'+ventanaActual.num).find('#cmbSexoListaEspera').val();
	   
	   valores_a_mandar=valores_a_mandar+"&idProfesional="+$('#drag'+ventanaActual.num).find('#cmbProfesionalesListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&discapacitad="+$('#drag'+ventanaActual.num).find('#cmbDiscapaciFisicaListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&embarazo="+$('#drag'+ventanaActual.num).find('#cmbEmbarazoListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&tipoCita="+$('#drag'+ventanaActual.num).find('#cmbTipoCitaListaEspera').val();		   
	   valores_a_mandar=valores_a_mandar+"&ZonaResidenciaListaEspera="+$('#drag'+ventanaActual.num).find('#cmbZonaResidenciaListaEspera').val();  
	   valores_a_mandar=valores_a_mandar+"&Necesidad="+$('#drag'+ventanaActual.num).find('#cmbNecesidadListaEspera').val();  	   
	   valores_a_mandar=valores_a_mandar+"&Disponible="+$('#drag'+ventanaActual.num).find('#cmbDisponibleListaEspera').val();  	   	   
	   valores_a_mandar=valores_a_mandar+"&TipoUsuario="+$('#drag'+ventanaActual.num).find('#cmbTipoUsuarioListaEspera').val();		
	   valores_a_mandar=valores_a_mandar+"&Observacion="+$('#drag'+ventanaActual.num).find('#txtObservacionListaEspera').val();			   
	   valores_a_mandar=valores_a_mandar+"&idMunicipioReside="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtMunicipioListaEspera').val();		   	   
	   valores_a_mandar=valores_a_mandar+"&idAdministradora1="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtAdministradora1ListaEspera').val();		   	   	   
	   valores_a_mandar=valores_a_mandar+"&idAdministradora2="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtAdministradora2ListaEspera').val();	
	  // alert('xxxxxx '+valores_a_mandar);
	   varajaxInit.send(valores_a_mandar);
	}    

}
function respuestaguardarListaEspera(){   
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('raiz')!=null){    

					   if(raiz.getElementsByTagName('respuesta')[0].firstChild.data){    

						   buscarJuan('listPacientesListaEspera','/clinica/paginas/accionesXml/buscarCitas_xml.jsp');
						   limpiarDatosPacienteListaEspera();
					       limpiarListadosTotales('listDocumentosPacienteListaEspera');   
					       limpiarListadosTotales('listHistoricoListaEspera');   						   
						   alert('Paciente ingresado a lista de espera satisfactoriamente');

					   }
					   else{
						   alert("Caramba no se pudo ingresar a la lista de espera");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}

function modificarListaEspera(){
	
   if(verificarCamposGuardar('guardarAsignacionCitaNoPlaneadaListaEspera')){	
   
	   IdTipoAgenda=$('#drag'+ventanaActual.num).find('#IdTipoAgendaListaEspera').val();
	   varajaxInit=crearAjax();  
	   varajaxInit.open("POST",'/clinica/paginas/accionesXml/modificar_xml.jsp',true);
	   varajaxInit.onreadystatechange=respuestamodificarListaEspera;
	   varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  
		  

	   valores_a_mandar="accion=modificarListaEspera"; 
	   valores_a_mandar=valores_a_mandar+"&idPaciente="+$('#drag'+ventanaActual.num).find('#hidd_IdPacienteListaEspera').val();		   
	   valores_a_mandar=valores_a_mandar+"&IdListaEspera="+$('#drag'+ventanaActual.num).find('#lblIdListaEspera2').html();	

	   valores_a_mandar=valores_a_mandar+"&FechaNacimiento="+$('#drag'+ventanaActual.num).find('#txtFechaNacimientoListaEspera').val();		
	   valores_a_mandar=valores_a_mandar+"&sexo="+$('#drag'+ventanaActual.num).find('#cmbSexoListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&Telefonos="+$('#drag'+ventanaActual.num).find('#txtTelefonosListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&DireccionRes="+$('#drag'+ventanaActual.num).find('#txtDireccionResListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&NomAcompanante="+$('#drag'+ventanaActual.num).find('#txtNomAcompananteListaEspera').val();			   

	   valores_a_mandar=valores_a_mandar+"&idProfesional="+$('#drag'+ventanaActual.num).find('#cmbProfesionalesListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&discapacitad="+$('#drag'+ventanaActual.num).find('#cmbDiscapaciFisicaListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&embarazo="+$('#drag'+ventanaActual.num).find('#cmbEmbarazoListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&tipoCita="+$('#drag'+ventanaActual.num).find('#cmbTipoCitaListaEspera').val();		   
	   valores_a_mandar=valores_a_mandar+"&ZonaResidenciaListaEspera="+$('#drag'+ventanaActual.num).find('#cmbZonaResidenciaListaEspera').val();  
	   valores_a_mandar=valores_a_mandar+"&Necesidad="+$('#drag'+ventanaActual.num).find('#cmbNecesidadListaEspera').val();  	   
	   valores_a_mandar=valores_a_mandar+"&Disponible="+$('#drag'+ventanaActual.num).find('#cmbDisponibleListaEspera').val();  	   	   
	   valores_a_mandar=valores_a_mandar+"&TipoUsuario="+$('#drag'+ventanaActual.num).find('#cmbTipoUsuarioListaEspera').val();		
	   valores_a_mandar=valores_a_mandar+"&Observacion="+$('#drag'+ventanaActual.num).find('#txtObservacionListaEspera').val();			   
	   valores_a_mandar=valores_a_mandar+"&idMunicipioReside="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtMunicipioListaEspera').val();		   	   
	   valores_a_mandar=valores_a_mandar+"&idAdministradora1="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtAdministradora1ListaEspera').val();		   	   	   
	   valores_a_mandar=valores_a_mandar+"&idAdministradora2="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtAdministradora2ListaEspera').val();	
	   valores_a_mandar=valores_a_mandar+"&estado="+$('#drag'+ventanaActual.num).find('#cmbEstadoListaEspera').val();			   
	   //alert('----'+valores_a_mandar);
	   varajaxInit.send(valores_a_mandar);
	}    

}
function respuestamodificarListaEspera(){   
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('raiz')!=null){    

					   if(raiz.getElementsByTagName('respuesta')[0].firstChild.data){    

						   //limpiarDatosPacienteListaEspera();
				           ocultar('divListaEspera');	

						   //limpiarListadosTotales('listDocumentosPacienteListaEspera');   
						   //limpiarListadosTotales('listPacientesListaEspera');   						   
						   alert('Modificado en la lista de espera satisfactoriament');
						   buscarJuan('listPacientesListaEsperaTraer','/clinica/paginas/accionesXml/buscarCitas_xml.jsp')
						   mostrar('divTraerListaEspera');						   

					   }
					   else{
						   alert("Caramba no se pudo ingresar a la lista de espera");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}
function llamarVentanaListaEspera(){
    limpiarDatosPacienteListaEspera();mostrar('divListaEspera'); 
	limpiarListadosTotales('listDocumentosPacienteListaEspera');
	limpiarListadosTotales('listHistoricoListaEspera');   
	buscarJuan('listPacientesListaEspera','/clinica/paginas/accionesXml/buscarCitas_xml.jsp'); 
	ocultar('divBotonModificarCitaListaEspera'); 
	mostrar('divBotonGuardarCitaListaEspera');  
	cargarDatosIdPaciente(); // para el autocompletar de lista de espera una vez se dispara las citas

}

function llamarVentanaDesdeListaEspera(){
   limpiarDatosPacienteListaEspera(); 
   buscarJuan('listPacientesListaEsperaTraer','/clinica/paginas/accionesXml/buscarCitas_xml.jsp');
   setTimeout("llamarAutocomIdDescripcion('txtBusIdPacienteCitaHoy',15)",1500);   
}

function respuestaingresodatosAgendaNoPlaneadaListaEspera(){   
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('raiz')!=null){    

					   if(raiz.getElementsByTagName('respuesta')[0].firstChild.data){    
						  //agregarDatosTablaCalendario(raiz,totalRegistros);	
						 // alert('tarea cambiada');
						  llenarCitas();
						  ocultar('divSubVentanaAgendaNoPlaneada');
					   }
					   else{
						   alert("Caramba no se pudo actualizar la tarea");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}



function respuestaingresodatosAgendaNoPlaneada(){   
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('raiz')!=null){    

					   if(raiz.getElementsByTagName('respuesta')[0].firstChild.data){    
						  //agregarDatosTablaCalendario(raiz,totalRegistros);	
						 // alert('tarea cambiada');
						  llenarCitas();
						  ocultar('divSubVentanaAgendaNoPlaneada');
					   }
					   else{
						   alert("Caramba no se pudo actualizar la tarea");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}



function modificarAsignacionCitaNoPlaneada(){ // boton modificar
	
   if(verificarCamposGuardar('guardarAsignacionCitaNoPlaneada')){	
   
	   fecha= $('#drag'+ventanaActual.num).find('#lblFecha').html();
	   idProfesional=$('#drag'+ventanaActual.num).find('#cmbProfesionalesListaEsperaCita').val();
	   IdTipoAgenda=$('#drag'+ventanaActual.num).find('#IdTipoAgenda').val();
	   Jornada= $('#drag'+ventanaActual.num).find('#cmbIdJornada').val();
	   idTurno=$('#drag'+ventanaActual.num).find('#cmbIdTurno').val();
	   tipoCita=$('#drag'+ventanaActual.num).find('#cmbTipoCita').val();   
	   estadoCita=$('#drag'+ventanaActual.num).find('#cmbEstadoCita').val();  
	   
	
	   varajaxInit=crearAjax();  
	   varajaxInit.open("POST",'/clinica/paginas/accionesXml/modificar_xml.jsp',true);
	   varajaxInit.onreadystatechange=respuestaModificardatosAgendaNoPlaneada;
	   varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  
	  
	   valores_a_mandar="accion=modificarDatosAgendaNoPlaneadaPacientExistente"; 
	   
	   valores_a_mandar=valores_a_mandar+"&idCita="+$('#drag'+ventanaActual.num).find('#lblIdCita').html();		   	   
	   valores_a_mandar=valores_a_mandar+"&idPaciente="+$('#drag'+ventanaActual.num).find('#hidd_IdPaciente').val();		   
	   valores_a_mandar=valores_a_mandar+"&Telefonos="+$('#drag'+ventanaActual.num).find('#txtTelefonos').val();	
	   valores_a_mandar=valores_a_mandar+"&DireccionRes="+$('#drag'+ventanaActual.num).find('#txtDireccionRes').val();	
	   valores_a_mandar=valores_a_mandar+"&NomAcompanante="+$('#drag'+ventanaActual.num).find('#txtNomAcompanante').val();			   

	   valores_a_mandar=valores_a_mandar+"&idProfesional="+idProfesional;		
	   valores_a_mandar=valores_a_mandar+"&fecha="+fecha;	
	   valores_a_mandar=valores_a_mandar+"&Jornada="+Jornada;	
	   valores_a_mandar=valores_a_mandar+"&idTurno="+idTurno;		
       valores_a_mandar=valores_a_mandar+"&IdTipoAgenda="+IdTipoAgenda;	
	   valores_a_mandar=valores_a_mandar+"&tipoCita="+tipoCita;		   
	   valores_a_mandar=valores_a_mandar+"&estadoCita="+estadoCita;	
	   valores_a_mandar=valores_a_mandar+"&TipoUsuario="+$('#drag'+ventanaActual.num).find('#cmbTipoUsuario').val();		
	   valores_a_mandar=valores_a_mandar+"&Observacion="+$('#drag'+ventanaActual.num).find('#txtObservacion').val();			   
	   
	   
	   valores_a_mandar=valores_a_mandar+"&idMunicipioReside="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtMunicipio').val();		   	   
	   valores_a_mandar=valores_a_mandar+"&idAdministradora1="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtAdministradora1').val();		   	   	   
	   valores_a_mandar=valores_a_mandar+"&idAdministradora2="+$('#drag'+ventanaActual.num).find('#hidd_IdtxtAdministradora2').val();		   	   	   	   

	//  alert('--------------------'+valores_a_mandar);
	   varajaxInit.send(valores_a_mandar);
	}   

}

    function cargarFosiga(paginaAbrir){ 
	   window.open('http://www.fosyga.gov.co/fisalud/CGI/buda_consulta_afil_Invitado_dnn.asp?num_doc='+$('#drag'+ventanaActual.num).find('#txtIdPaciente').val(), "", "width=800, height=400, top=80, left=40, scrollbars=yes, Resizable=No, Directories=No, Location=No, Menubar=No, Status=No, Titlebar=No, Toolbar=No, fullscreen=yes");
    
	}


function respuestaModificardatosAgendaNoPlaneada(){   
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('raiz')!=null){    

					   if(raiz.getElementsByTagName('respuesta')[0].firstChild.data){    
						  llenarCitas();
						  ocultar('divSubVentanaAgendaNoPlaneada');
					   }
					   else{
						   alert("Caramba no se pudo actualizar la tarea");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}






function ingresoDatosAgendaAjax(tipoCambioEstado, idProfesional, fecha, Jornada, IdTipoAgenda, idTurno){  
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/modificar_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestaingresoDatosAgendaAjax;
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  
		
	 valores_a_mandar="accion="+tipoCambioEstado; 
	 valores_a_mandar=valores_a_mandar+"&idProfesional="+idProfesional;		
	 valores_a_mandar=valores_a_mandar+"&fecha="+fecha;	
	 valores_a_mandar=valores_a_mandar+"&Jornada="+Jornada;	
	 valores_a_mandar=valores_a_mandar+"&IdTipoAgenda="+IdTipoAgenda;	
	 valores_a_mandar=valores_a_mandar+"&idTurno="+idTurno;		 
	 varajaxInit.send(valores_a_mandar); 
}
function respuestaingresoDatosAgendaAjax(){   
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('raiz')!=null){    

					   if(raiz.getElementsByTagName('respuesta')[0].firstChild.data){    
						  //agregarDatosTablaCalendario(raiz,totalRegistros);	
						 // alert('tarea cambiada');
						  llenarCitas();
					   }
					   else{
						   alert("Caramba no se pudo actualizar la tarea");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}




function dejarDisponiblePlaneadasAjax(tipoCambioEstado, idProfesional, fecha, Jornada, IdTipoAgenda, idTurno){  
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/modificar_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestadejarDisponiblePlaneadasAjax;
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  
		
	 valores_a_mandar="accion="+tipoCambioEstado; 
	 valores_a_mandar=valores_a_mandar+"&idProfesional="+idProfesional;		
	 valores_a_mandar=valores_a_mandar+"&fecha="+fecha;	
	 valores_a_mandar=valores_a_mandar+"&Jornada="+Jornada;	
	 valores_a_mandar=valores_a_mandar+"&IdTipoAgenda="+IdTipoAgenda;	
	 valores_a_mandar=valores_a_mandar+"&idTurno="+idTurno;		 
	 //alert(valores_a_mandar);
	 varajaxInit.send(valores_a_mandar); 
}
function respuestadejarDisponiblePlaneadasAjax(){   
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('raiz')!=null){    

					   if(raiz.getElementsByTagName('respuesta')[0].firstChild.data){    
						  //agregarDatosTablaCalendario(raiz,totalRegistros);	
						 // alert('tarea cambiada');
						  llenarCitas();
					   }
					   else{
						   alert("Caramba no se pudo Modificar");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}





/****************************************************************************citas fin *************************************************************/

function totalRegistrosTabla(idTabla){
  		 var mytable=document.getElementById(idTabla)
		 return mytable.rows.length;
}

function borrarRegistrosTabla(idTabla){
  		 var mytable=document.getElementById(idTabla)
		 while (mytable.rows.length>0) //deletes all rows of a table
		 mytable.deleteRow(0) //delete first row of contracting table until there are none left 
}	


function agregarDatosTablaCalendario(raiz, totalRegistros){   		
		 

         tablabody=document.getElementById('tablaCalendario').lastChild;
		 var cuadro=0,diaInicio=0; 
		 
		 diaInicio=raiz.getElementsByTagName('num_dia')[0].firstChild.data;
	     diaf=raiz.getElementsByTagName('diaf')[totalRegistros-1].firstChild.data;		 
		 dia=1;
		  
		 for(i=1;i<7;i++){	//semanas		 
		  
			  Nregistro=document.createElement("TR");
			  Nregistro.id=i;   
			  
	  		  for(j=1;j<=7;j++){ 
  			      cuadro++;	
						 if(cuadro>=diaInicio && dia<=diaf){ //alert('-entr-');
							TH=document.createElement("TH");
							TH.id='cua'+dia;   
							TH.className='th54';
							
								tabla=document.createElement("table");	//numero del dia	
								tabla.setAttribute('height','10%')
								  tr=document.createElement("TR");
									th=document.createElement("Td");
									th.innerHTML=dia;								  
								  tr.appendChild(th);
								  tabla.appendChild(tr);	
								TH.appendChild(tabla);
								
								/*****************************************tabla principal donde se agregan los eventos**************/
								tablaC=document.createElement("table");	
								tablaC.setAttribute('height','90%')
								  trC=document.createElement("TR");
									thC=document.createElement("Td");
									thC.id='cuadro'+dia;	 
								  trC.appendChild(thC);
								  tablaC.appendChild(trC);	
								TH.appendChild(tablaC);		
								/*****************************************fin tabla principal donde se agregan los eventos**************/								
							
							
							Nregistro.appendChild(TH);	
							//TH.innerHTML='cuadro'+dia;	
							dia++;
			             }
						 else{
							TH=document.createElement("TH");
							TH.id=cuadro; 							
							TH.innerHTML='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							TH.className='th55';
							Nregistro.appendChild(TH);							 
						 
						 }							
			  }/*1 a 7*/			  
			  tablabody.appendChild(Nregistro); 
		 }/*1 a 5*/
		 
		/*ahora a llenar los cuadros del dia en la tabla principal de eventos*/
		 for(m=0;m<totalRegistros;m++){
		    diaf=raiz.getElementsByTagName('diaf')[m].firstChild.data;
		    idTarea=raiz.getElementsByTagName('idTarea')[m].firstChild.data; 
			desTarea=raiz.getElementsByTagName('desTarea')[m].firstChild.data;
			tipoPlan=raiz.getElementsByTagName('tipoPlan')[m].firstChild.data;
			estadoTarea=raiz.getElementsByTagName('estadoTarea')[m].firstChild.data;
			
			estadoGraf=raiz.getElementsByTagName('estadoGraf')[m].firstChild.data;

			idEventoMejorar=raiz.getElementsByTagName('idEventoMejorar')[m].firstChild.data;
			descEventoMejorar=raiz.getElementsByTagName('descEventoMejorar')[m].firstChild.data;
			descRolResponsable=raiz.getElementsByTagName('descRolResponsable')[m].firstChild.data;	  
			descProceso=raiz.getElementsByTagName('descProceso')[m].firstChild.data;	
			
			cuadros=document.getElementById('cuadro'+diaf);
		    tr=document.createElement("tr");
		    td=document.createElement("td");

			td.className='td'+tipoPlan+'_'+estadoGraf;
			td.setAttribute('title',idTarea+'-'+desTarea);
			
			if(desTarea.length>1){
			   td.innerHTML=desTarea.substring(0,40)+'..';
			   td.setAttribute('onclick',"abrirVentanaTareas('"+idTarea+"','"+desTarea+"','"+estadoTarea+"','"+idEventoMejorar+"','"+descEventoMejorar+"','"+descRolResponsable+"','"+descProceso+"')"); 
			}

		    //td.innerHTML=idTarea+'-'+desTarea;
		    tr.appendChild(td);
		    cuadros.appendChild(tr); 
		 }
	
}


function abrirVentanaTareas(idTarea, desTarea, estadoTarea, idEventoMejorar, descEventoMejorar, descRolResponsable,descProceso){
   mostrar('divSublistadoCalendario')
   $('#drag'+ventanaActual.num).find('#lblIdEventoMejorar').html(idEventoMejorar)   
   $('#drag'+ventanaActual.num).find('#lblAspectoMejorar').html(descEventoMejorar)
   $('#drag'+ventanaActual.num).find('#lblProcesoResp').html(descProceso)
   $('#drag'+ventanaActual.num).find('#lblIdEstadTarea').html(idTarea)
   $('#drag'+ventanaActual.num).find('#lblDescrTarea').html(desTarea)
   $('#drag'+ventanaActual.num).find('#lblRolResponsableTarea').html(descRolResponsable)
   
   if( estadoTarea=='00' ) {	   
	    $('#drag'+ventanaActual.num).find('#chkEstadTar').attr('checked','');   
   }
   else $('#drag'+ventanaActual.num).find('#chkEstadTar').attr('checked','checked');   
}



function cambiarFuncionATipoAgenda(idTipoAgenda, planeada){

   $('#drag'+ventanaActual.num).find('#IdTipoAgenda').val(idTipoAgenda);
   $('#drag'+ventanaActual.num).find('#planeada').val(planeada);  
}


function irACambiarEstadoTarea(){

  if($('#drag'+ventanaActual.num).find('#chkEstadTar').attr('checked')==false)
     cambiarEstadoTareaAjax('00');
  else
	 cambiarEstadoTareaAjax('01');
}







function cambiarEstadoTareaAjax(estadoCambiar){  
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/modificar_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestacambiarEstadoTareaAjax;
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  
		
	 valores_a_mandar="accion=cambiarEstadoTarea"; 
	 valores_a_mandar=valores_a_mandar+"&idTarea="+$('#drag'+ventanaActual.num).find('#lblIdEstadTarea').html();		
	 valores_a_mandar=valores_a_mandar+"&estadoTarea="+estadoCambiar;	

	 varajaxInit.send(valores_a_mandar); 
}
function respuestacambiarEstadoTareaAjax(){   
	
	if(varajaxInit.readyState == 4 ){      
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('raiz')!=null){    

					   if(raiz.getElementsByTagName('respuesta')[0].firstChild.data){    
						  //agregarDatosTablaCalendario(raiz,totalRegistros);	
						 // alert('tarea cambiada');
						  llenarCalendario();
					   }
					   else{
						   alert("Caramba no se pudo actualizar la tarea");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}




function agregarDatosTablaIndicadores(id,IdISOEstruct,IdISOProcesDescrip,ISOCuantas,reportadas,asignadas,cuantosImpacto,cuantosPlanes,cuantosPlanesCerrad){
		 
		 tablabody=document.getElementById(tablaListadoIndicadores).lastChild;
		  
		  Nregistro=document.createElement("TR");
		  Nregistro.id='filap_'+id;
		  Nregistro.className='label_left1';
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.innerHTML=id;
		  Ncampo.setAttribute('hidden','true');
		  Ncampo.headers=j+1;
		  Nregistro.appendChild(Ncampo);

          Ncampo=document.createElement("TD");
	      Ncampo.setAttribute('width','7%');
		  
	      if(tablaListadoIndicadores!='sublistadoIndicadores'){  // funciona para indicador y subindicador, para el segundo se elimina este siguiente item
		     Ncampo.setAttribute('onMouseOver',"verSubIndicadoresJuan('"+id+"','"+IdISOProcesDescrip+"')");
  	        // Ncampo.setAttribute('onMouseOut',"ocultar('divSublistadoIndicadores')");
			 Ncampo.innerHTML=IdISOEstruct;
			 Ncampo.className='label_estadisticoGrafi';
		  }
		  else{
		     Ncampo.innerHTML=id;	
			 Ncampo.setAttribute('title',id);			 
		  }	  
		  Nregistro.appendChild(Ncampo);
		  
          Ncampo=document.createElement("TD");
	      Ncampo.setAttribute('width','40%');
		  Ncampo.innerHTML=IdISOProcesDescrip;
		  Nregistro.appendChild(Ncampo);

          Ncampo=document.createElement("TD");
	      Ncampo.setAttribute('width','10%');
		  Ncampo.innerHTML=ISOCuantas;
		  Nregistro.appendChild(Ncampo);

          Ncampo=document.createElement("TD");
	      Ncampo.setAttribute('width','10%');
		  Ncampo.innerHTML=reportadas;
		  Nregistro.appendChild(Ncampo);

          Ncampo=document.createElement("TD");
	      Ncampo.setAttribute('width','10%');
		  Ncampo.innerHTML=asignadas;
		  Nregistro.appendChild(Ncampo);
		  
          Ncampo=document.createElement("TD");
	      Ncampo.setAttribute('width','10%');
		  Ncampo.innerHTML=cuantosImpacto;
		  Nregistro.appendChild(Ncampo);		  

          Ncampo=document.createElement("TD");
	      Ncampo.setAttribute('width','10%');
		  Ncampo.innerHTML=cuantosPlanes;
		  Nregistro.appendChild(Ncampo);

          Ncampo=document.createElement("TD");
	      Ncampo.setAttribute('width','10%');
		  Ncampo.innerHTML=cuantosPlanesCerrad;
		  Nregistro.appendChild(Ncampo);


	      tablabody.appendChild(Nregistro); //alert('d');
	
	}
var tablaListadoIndicadores=""
function indicadoresJuan(arg,pag){ 

	  switch(arg){ 	   
		case 'administrarNoConformidad': 
		 tablaListadoIndicadores="listadoIndicadores";
		
		 var mytable=document.getElementById("listadoIndicadores")
		 while (mytable.rows.length>0) //deletes all rows of a table
		 mytable.deleteRow(0) //delete first row of contracting table until there are none left 
		  
		 varajaxInit=crearAjax();  
		 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarIndicadores_xml.jsp',true);
		 varajaxInit.onreadystatechange=respuestaListadoIndicadores;
		 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			  
		
         if($('#drag'+ventanaActual.num).find('#cmbBusproceso').val()=="00" && $('#drag'+ventanaActual.num).find('#cmbBusNoConformidades').val()=="00" && $('#drag'+ventanaActual.num).find('#cmbBusUbicacionArea').val()=="00" && $('#drag'+ventanaActual.num).find('#cmbBusEstadoEvento').val()=="00"){ 		
            
 		    $('#drag'+ventanaActual.num).find('#divIndicadores').show();			
			$('#drag'+ventanaActual.num).find('#divIndicadores').draggable(
			      {
				   		zIndex: 	1000,
				        ghosting:	true,
				        opacity: 	0.7,
				        containment : 'parent',
				        handle:	'#tituloForma'
				  }
	        );			
			
	        valores_a_mandar="accion=listadoIndicadores"; 
	        valores_a_mandar=valores_a_mandar+"&ventanaActual="+ventanaActual.opc;	
		    valores_a_mandar=valores_a_mandar+"&fechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
  		    valores_a_mandar=valores_a_mandar+"&fechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();	
    	    varajaxInit.send(valores_a_mandar); 
         }
		 else
		 alert('No se debe especificar filtros para esta consulta');
		 break;
		 
		case 'administrarEventoAdverso': 
		 tablaListadoIndicadores="listadoIndicadores";
		
		 var mytable=document.getElementById("listadoIndicadores")
		 while (mytable.rows.length>0) //deletes all rows of a table
		 mytable.deleteRow(0) //delete first row of contracting table until there are none left 
		  
		 varajaxInit=crearAjax();  
		 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarIndicadores_xml.jsp',true);
		 varajaxInit.onreadystatechange=respuestaListadoIndicadores;
		 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  
		
         if($('#drag'+ventanaActual.num).find('#cmbBusproceso').val()=="00" && $('#drag'+ventanaActual.num).find('#cmbBusClaseEA').val()=="00" && $('#drag'+ventanaActual.num).find('#cmbBusEventoProceso').val()=="00" && $('#drag'+ventanaActual.num).find('#cmbBusUbicacionArea').val()=="00"){ 		
            
 		    $('#drag'+ventanaActual.num).find('#divIndicadores').show();			
			$('#drag'+ventanaActual.num).find('#divIndicadores').draggable(
			      {
				   		zIndex: 	1000,
				        ghosting:	true,
				        opacity: 	0.7,
				        containment : 'parent',
				        handle:	'#tituloForma'
				  }
	        );			
			
	        valores_a_mandar="accion=listadoIndicadores"; 
	        valores_a_mandar=valores_a_mandar+"&ventanaActual="+ventanaActual.opc;	
		    valores_a_mandar=valores_a_mandar+"&fechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
  		    valores_a_mandar=valores_a_mandar+"&fechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();	
//   	    alert(valores_a_mandar);
			varajaxInit.send(valores_a_mandar); 
         }
		 else
		 alert('No se debe especificar filtros para esta consulta');
		 break;		 
		 
		case 'sublistadoIndicadores':
			 tablaListadoIndicadores="sublistadoIndicadores"
			 var mytable=document.getElementById(tablaListadoIndicadores)
			 while (mytable.rows.length>0) //deletes all rows of a table
			 mytable.deleteRow(0) //delete first row of contracting table until there are none left 
			  
			 varajaxInit=crearAjax();  
			 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarIndicadores_xml.jsp',true);
			 varajaxInit.onreadystatechange=respuestaListadoIndicadores;
			 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			 valores_a_mandar="accion=sublistadoIndicadores"; 
		     valores_a_mandar=valores_a_mandar+"&ventanaActual="+ventanaActual.opc;			 
			 valores_a_mandar=valores_a_mandar+"&fechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();  
			 valores_a_mandar=valores_a_mandar+"&fechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();	
			 valores_a_mandar=valores_a_mandar+"&IdSubEvento="+$('#drag'+ventanaActual.num).find('#lblIdSubEvento').html();				 
			 varajaxInit.send(valores_a_mandar); 
		 break;
	  }
}
function respuestaListadoIndicadores(){ 
    totalId=0;
    totalISOCuantas=0;
    totalreportadas=0;
    totalasignadas=0;
    totalcuantosImpacto=0;
    totalcuantosPlanes=0;
    totalcuantosImpacto=0;
    totalcuantosPlanesCerrad=0;
	
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			      raiz=varajaxInit.responseXML.documentElement;

				  if(raiz.getElementsByTagName('indicador')!=null){
					   totalRegistros=raiz.getElementsByTagName('IdISOEvento').length;  	

					   if(totalRegistros>0){ 
					       for(j=0;j<totalRegistros;j++){						   
					          id=raiz.getElementsByTagName('IdISOEvento')[j].firstChild.data;						   					  
					          IdISOEstruct=raiz.getElementsByTagName('IdISOEstruct')[j].firstChild.data;
					          IdISOProcesDescrip=raiz.getElementsByTagName('IdISOProcesDescrip')[j].firstChild.data;							  
					          ISOCuantas=raiz.getElementsByTagName('ISOCuantas')[j].firstChild.data;							  
					          reportadas=raiz.getElementsByTagName('reportadas')[j].firstChild.data;							  
					          asignadas=raiz.getElementsByTagName('asignadas')[j].firstChild.data;							  
					          cuantosImpacto=raiz.getElementsByTagName('cuantosImpacto')[j].firstChild.data;							  
					          cuantosPlanes=raiz.getElementsByTagName('cuantosPlanes')[j].firstChild.data;							  
					          cuantosPlanesCerrad=raiz.getElementsByTagName('cuantosPlanesCerrad')[j].firstChild.data;							  							  

							  totalISOCuantas=totalISOCuantas+parseInt(ISOCuantas); 
							  totalreportadas=totalreportadas+parseInt(reportadas); 
							  totalasignadas=totalasignadas+parseInt(asignadas); 
							  totalcuantosImpacto=totalcuantosImpacto+parseInt(cuantosImpacto); 
							  totalcuantosPlanes=totalcuantosPlanes+parseInt(cuantosPlanes); 
							  totalcuantosPlanesCerrad=totalcuantosPlanesCerrad+parseInt(cuantosPlanesCerrad); 							  

							  agregarDatosTablaIndicadores(id,IdISOEstruct,IdISOProcesDescrip,ISOCuantas,reportadas,asignadas,cuantosImpacto,cuantosPlanes,cuantosPlanesCerrad);
						   }
						   
						   if( tablaListadoIndicadores=='listadoIndicadores'){
						   
							   $('#drag'+ventanaActual.num).find('#lblCuantasNC').html('');
							   $('#drag'+ventanaActual.num).find('#lblReportadas').html('');
							   $('#drag'+ventanaActual.num).find('#lblAsignadas').html('');
							   $('#drag'+ventanaActual.num).find('#lblImpacto').html('');
							   $('#drag'+ventanaActual.num).find('#lblPlanes').html('');
							   $('#drag'+ventanaActual.num).find('#lblPlanesCerrad').html('');						   
							   
							   $('#drag'+ventanaActual.num).find('#lblCuantasNC').html(totalISOCuantas); 
							   $('#drag'+ventanaActual.num).find('#lblReportadas').html(totalreportadas); 
							   $('#drag'+ventanaActual.num).find('#lblAsignadas').html(totalasignadas); 
							   $('#drag'+ventanaActual.num).find('#lblImpacto').html(totalcuantosImpacto); 
							   $('#drag'+ventanaActual.num).find('#lblPlanes').html(totalcuantosPlanes); 
							   $('#drag'+ventanaActual.num).find('#lblPlanesCerrad').html(totalcuantosPlanesCerrad); 	
							}
							else  if( tablaListadoIndicadores=='sublistadoIndicadores'){
							   $('#drag'+ventanaActual.num).find('#lblSubCuantasNC').html('');
							   $('#drag'+ventanaActual.num).find('#lblSubReportadas').html('');
							   $('#drag'+ventanaActual.num).find('#lblSubAsignadas').html('');
							   $('#drag'+ventanaActual.num).find('#lblSubImpacto').html('');
							   $('#drag'+ventanaActual.num).find('#lblSubPlanes').html('');
							   $('#drag'+ventanaActual.num).find('#lblSubPlanesCerrad').html('');						   
							   
							   $('#drag'+ventanaActual.num).find('#lblSubCuantasNC').html(totalISOCuantas); 
							   $('#drag'+ventanaActual.num).find('#lblSubReportadas').html(totalreportadas); 
							   $('#drag'+ventanaActual.num).find('#lblSubAsignadas').html(totalasignadas); 
							   $('#drag'+ventanaActual.num).find('#lblSubImpacto').html(totalcuantosImpacto); 
							   $('#drag'+ventanaActual.num).find('#lblSubPlanes').html(totalcuantosPlanes); 
							   $('#drag'+ventanaActual.num).find('#lblSubPlanesCerrad').html(totalcuantosPlanesCerrad); 	
								
								
							}	
							
							
							   
					   }
					   else{
						    agregarDatosTablaIndicadores('','','Sin datos en este rango','','','','','','');
						
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}
//--------------------------------------------  fin indicadores eventos
	
	
//----------------------------------------------------------------     AUTOCOMPLETAR

function destruirAutocom(paramAutoc){
	
	$('#'+paramAutoc).autocomplete("destroy");
	
}	


	


function limpiarFiltrosDeBusquedaReporteEA(txt_id_elemento){
	
	llamarAutocomPaciente(txt_id_elemento,'%');
		$("#cmbBusproceso option[value='00']").attr('selected', 'selected');
		$("#cmbBusClaseEA option[value='00']").attr('selected', 'selected');
		$("#cmbBusEAProceso option[value='00']").attr('selected', 'selected');
		$("#cmbBusUbicacionArea option[value='00']").attr('selected', 'selected');					

		$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val('');
		$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val('');

}


function llamarAutocomPaciente(paramAutoc,textoAescribir){ //alert(paramAutoc +'-'+textoAescribir);
    //limpiarDivEditarJuan('reportarEventoAdverso');

    if( $('#drag'+ventanaActual.num).find('#txtBusIdPaciente').val()=="" ){// para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
	    datos_a_mandar="accion="+paramAutoc;
		datos_a_mandar=datos_a_mandar+"&textoAescribir="+textoAescribir;				
		varajaxInit=crearAjax();
		varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarAutocomplete_xml.jsp',true);
		varajaxInit.onreadystatechange=function(){respuestallamarAutocomPaciente(paramAutoc)};
		varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		//alert(datos_a_mandar);
		varajaxInit.send(datos_a_mandar);	
	}	
}

function llamarAutocomPaciente2(paramAutoc,textoAescribir){ //alert(paramAutoc +'-'+textoAescribir);
    //limpiarDivEditarJuan('reportarEventoAdverso');

    if( $('#drag'+ventanaActual.num).find('#txtIdPaciente').val()=="" ){// para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
	    datos_a_mandar="accion="+paramAutoc;
		datos_a_mandar=datos_a_mandar+"&textoAescribir="+textoAescribir;				
		varajaxInit=crearAjax();
		varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarAutocomplete_xml.jsp',true);
		varajaxInit.onreadystatechange=function(){respuestallamarAutocomPaciente(paramAutoc)};
		varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		//alert(datos_a_mandar);
		varajaxInit.send(datos_a_mandar);	
	}	
}

function respuestallamarAutocomPaciente(paramAutoc){  
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('item')!=null){
				cod=raiz.getElementsByTagName('cod');		  
			    text=raiz.getElementsByTagName('text');	
				data = new Array();
			    for(a=0;a<text.length;a++ ){						     
				     data[a]=cod[a].firstChild.data+'-'+ text[a].firstChild.data;
				}
				
				$('#'+paramAutoc).flushCache(); //sirve para borrar el cache
				$('#'+paramAutoc).autocomplete(data,{
				   multiple:false,
				   matchContains: true, //sirve para buscar en cualquier parte de la frase
				  // autofill:false,
				   //selectedFirst:false, // ordena segun los caracteres coinidentes
				   cacheLength:1,
				   width:600,
				   height:1500,

				   deferRequestBy: 0, //miliseconds

				   //formatItem:formatItem,
				   //formatResult:formatResult,											  
				   selSeparator: '|', //separador de campos '|' por defecto
				  // lineSeparator:'\n' //separador de lineas (registros) '\n' por defecto
				   minChars:1 //minimo de caracteres para empezar a desplegar la lista
			    }

				
				);
				
			  }else{
				  alert("No se puede Autocompletar");
			     }
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
		}
$('#drag'+ventanaActual.num).find('#'+paramAutoc).focus();
}



function buscarPacienteHoy(){/*se necesita llararce en varios*/

     buscarJuan('listCitasHoy','/clinica/paginas/accionesXml/buscarCitas_xml.jsp');

}
function buscarMisPacienteHoy(){/*se necesita llararce en varios*/
	buscarJuan('listMisCitasHoy','/clinica/paginas/accionesXml/buscarCitas_xml.jsp');
	
}

function llamarAutocomPacienteListaEspera(paramAutoc,textoAescribir){// alert(paramAutoc +'-'+textoAescribir);
    //limpiarDivEditarJuan('reportarEventoAdverso');

    if( $('#drag'+ventanaActual.num).find('#txtBusIdPacienteListaEspera').val()=="" ){// para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
	    datos_a_mandar="accion="+paramAutoc;
		datos_a_mandar=datos_a_mandar+"&textoAescribir="+textoAescribir;				
		varajaxInit=crearAjax();
		varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarAutocomplete_xml.jsp',true);
		varajaxInit.onreadystatechange=function(){respuestallamarAutocomPaciente(paramAutoc)};
		varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		//alert(datos_a_mandar);
		varajaxInit.send(datos_a_mandar);	
	}	
}
function respuestallamarAutocomPaciente(paramAutoc){  
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('item')!=null){
				cod=raiz.getElementsByTagName('cod');		  
			    text=raiz.getElementsByTagName('text');	
				data = new Array();
			    for(a=0;a<text.length;a++ ){						     
				     data[a]=cod[a].firstChild.data+'-'+ text[a].firstChild.data;
				}
				
				$('#'+paramAutoc).flushCache(); //sirve para borrar el cache
				$('#'+paramAutoc).autocomplete(data,{
				   multiple:false,
				   matchContains: true, //sirve para buscar en cualquier parte de la frase
				  // autofill:false,
				   //selectedFirst:false, // ordena segun los caracteres coinidentes
				   cacheLength:1,
				   width:600,
				   height:1500,

				   deferRequestBy: 0, //miliseconds

				   //formatItem:formatItem,
				   //formatResult:formatResult,											  
				   selSeparator: '|', //separador de campos '|' por defecto
				  // lineSeparator:'\n' //separador de lineas (registros) '\n' por defecto
				   minChars:1 //minimo de caracteres para empezar a desplegar la lista
			    }

				
				);
				
			  }else{
				  alert("No se puede Autocompletar");
			     }
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
		}
$('#drag'+ventanaActual.num).find('#'+paramAutoc).focus();
}


/*

function llamarAutocompletarMunicipio(paramAutoc,textoAescribir){// alert(paramAutoc +'-'+textoAescribir);
    //limpiarDivEditarJuan('reportarEventoAdverso');

  //  if( $('#drag'+ventanaActual.num).find('#txtMunicipio').val()=="" ){// para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
	    datos_a_mandar="accion="+paramAutoc;
		datos_a_mandar=datos_a_mandar+"&textoAescribir="+textoAescribir;				
		varajaxInit=crearAjax();
		varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarAutocomplete_xml.jsp',true);
		varajaxInit.onreadystatechange=function(){respuestallamarAutocompletarMunicipio(paramAutoc)};
		varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		//alert(datos_a_mandar);
		varajaxInit.send(datos_a_mandar);	
	//}	
}
function respuestallamarAutocompletarMunicipio(paramAutoc){  
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('item')!=null){
				cod=raiz.getElementsByTagName('cod');		  
			    text=raiz.getElementsByTagName('text');	
				data = new Array();
			    for(a=0;a<text.length;a++ ){						     
				     data[a]=cod[a].firstChild.data+'-'+ text[a].firstChild.data; 
				}
				
				$('#'+paramAutoc).flushCache(); //sirve para borrar el cache
				$('#'+paramAutoc).autocomplete(data,{
				   multiple:false,
				   matchContains: true, //sirve para buscar en cualquier parte de la frase
				  // autofill:false,
				   //selectedFirst:false, // ordena segun los caracteres coinidentes
				   cacheLength:1,
				   width:600,
				   height:1500,
				   deferRequestBy: 0, //miliseconds
				   //formatItem:formatItem,
				   //formatResult:formatResult,											  
				   selSeparator: '|', //separador de campos '|' por defecto
				  // lineSeparator:'\n' //separador de lineas (registros) '\n' por defecto
				   minChars:1 //minimo de caracteres para empezar a desplegar la lista
			    }

				
				);
				
			  }else{
				  alert("No se puede Autocompletar");
			     }
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
		}
$('#drag'+ventanaActual.num).find('#'+paramAutoc).focus();
}
*/

function llamarAutocompletarMunicipioListaEspera(paramAutoc,textoAescribir){// alert(paramAutoc +'-'+textoAescribir);
    //limpiarDivEditarJuan('reportarEventoAdverso');

   // if( $('#drag'+ventanaActual.num).find('#txtMunicipioListaEspera').val()=="" ){// para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
	    datos_a_mandar="accion="+paramAutoc;
		datos_a_mandar=datos_a_mandar+"&textoAescribir="+textoAescribir;				
		varajaxInit=crearAjax();
		varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarAutocomplete_xml.jsp',true);
		varajaxInit.onreadystatechange=function(){respuestallamarAutocompletarMunicipioListaEspera(paramAutoc)};
		varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		//alert(datos_a_mandar);
		varajaxInit.send(datos_a_mandar);	
	//}	
}
function respuestallamarAutocompletarMunicipioListaEspera(paramAutoc){  
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('item')!=null){
				cod=raiz.getElementsByTagName('cod');		  
			    text=raiz.getElementsByTagName('text');	
				data = new Array();
			    for(a=0;a<text.length;a++ ){						     
				     data[a]=cod[a].firstChild.data+'-'+ text[a].firstChild.data; 
				}
				
				$('#'+paramAutoc).flushCache(); //sirve para borrar el cache
				$('#'+paramAutoc).autocomplete(data,{
				   multiple:false,
				   matchContains: true, //sirve para buscar en cualquier parte de la frase
				  // autofill:false,
				   //selectedFirst:false, // ordena segun los caracteres coinidentes
				   cacheLength:1,
				   width:600,
				   height:1500,
				   deferRequestBy: 0, //miliseconds
				   //formatItem:formatItem,
				   //formatResult:formatResult,											  
				   selSeparator: '|', //separador de campos '|' por defecto
				  // lineSeparator:'\n' //separador de lineas (registros) '\n' por defecto
				   minChars:1 //minimo de caracteres para empezar a desplegar la lista
			    }

				
				);
				
			  }else{
				  alert("No se puede Autocompletar");
			     }
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
		}
$('#drag'+ventanaActual.num).find('#'+paramAutoc).focus();
}
/*
function llamarAutocompletarAdministradora(paramAutoc,textoAescribir){// alert(paramAutoc +'-'+textoAescribir);
    //limpiarDivEditarJuan('reportarEventoAdverso');

   // if( $('#drag'+ventanaActual.num).find('#txtAdministradora1').val()=="" ){// para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
	    datos_a_mandar="accion="+paramAutoc;
		datos_a_mandar=datos_a_mandar+"&textoAescribir="+textoAescribir;				
		varajaxInit=crearAjax();
		varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarAutocomplete_xml.jsp',true);
		varajaxInit.onreadystatechange=function(){respuestallamarAutocompletarAdministradora(paramAutoc)};
		varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		//alert(datos_a_mandar);
		varajaxInit.send(datos_a_mandar);	
//	}	
}
function respuestallamarAutocompletarAdministradora(paramAutoc){  
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('item')!=null){
				cod=raiz.getElementsByTagName('cod');		  
			    text=raiz.getElementsByTagName('text');	
				data = new Array();
			    for(a=0;a<text.length;a++ ){						     
				     data[a]=cod[a].firstChild.data+'-'+ text[a].firstChild.data; 
				}
				
				$('#'+paramAutoc).flushCache(); //sirve para borrar el cache
				$('#'+paramAutoc).autocomplete(data,{
				   multiple:false,
				   matchContains: true, //sirve para buscar en cualquier parte de la frase
				   cacheLength:1,
				   width:600,
				   height:1500,
				   deferRequestBy: 0, //miliseconds
				   selSeparator: '|', //separador de campos '|' por defecto
				   minChars:1 //minimo de caracteres para empezar a desplegar la lista
			    }
				);
			  }else{
				  alert("No se puede Autocompletar");
			     }
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
		}
$('#drag'+ventanaActual.num).find('#'+paramAutoc).focus();
}

function llamarAutocompletarAdministradoraLE(paramAutoc,textoAescribir){// alert(paramAutoc +'-'+textoAescribir);
    //limpiarDivEditarJuan('reportarEventoAdverso');

   // if( $('#drag'+ventanaActual.num).find('#txtAdministradora1').val()=="" ){// para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
	    datos_a_mandar="accion="+paramAutoc;
		datos_a_mandar=datos_a_mandar+"&textoAescribir="+textoAescribir;				
		varajaxInit=crearAjax();
		varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarAutocomplete_xml.jsp',true);
		varajaxInit.onreadystatechange=function(){respuestallamarAutocompletarAdministradoraLE(paramAutoc)};
		varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		//alert(datos_a_mandar);
		varajaxInit.send(datos_a_mandar);	
//	}	
}
function respuestallamarAutocompletarAdministradoraLE(paramAutoc){  
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('item')!=null){
				cod=raiz.getElementsByTagName('cod');		  
			    text=raiz.getElementsByTagName('text');	
				data = new Array();
			    for(a=0;a<text.length;a++ ){						     
				     data[a]=cod[a].firstChild.data+'-'+ text[a].firstChild.data; 
				}
				
				$('#'+paramAutoc).flushCache(); //sirve para borrar el cache
				$('#'+paramAutoc).autocomplete(data,{
				   multiple:false,
				   matchContains: true, //sirve para buscar en cualquier parte de la frase
				   cacheLength:1,
				   width:600,
				   height:1500,
				   deferRequestBy: 0, //miliseconds
				   selSeparator: '|', //separador de campos '|' por defecto
				   minChars:1 //minimo de caracteres para empezar a desplegar la lista
			    }
				);
			  }else{
				  alert("No se puede Autocompletar");
			     }
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
		}
$('#drag'+ventanaActual.num).find('#'+paramAutoc).focus();
}


function llamarAutocompletarAdministradora2(paramAutoc,textoAescribir){// alert(paramAutoc +'-'+textoAescribir);
    //limpiarDivEditarJuan('reportarEventoAdverso');

 //   if( $('#drag'+ventanaActual.num).find('#txtAdministradora2').val()=="" ){// para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
	    datos_a_mandar="accion="+paramAutoc;
		datos_a_mandar=datos_a_mandar+"&textoAescribir="+textoAescribir;				
		varajaxInit=crearAjax();
		varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarAutocomplete_xml.jsp',true);
		varajaxInit.onreadystatechange=function(){respuestallamarAutocompletarAdministradora(paramAutoc)};
		varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		//alert(datos_a_mandar);
		varajaxInit.send(datos_a_mandar);	
//	}	
}

function respuestallamarAutocompletarAdministradora(paramAutoc){  
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('item')!=null){
				cod=raiz.getElementsByTagName('cod');		  
			    text=raiz.getElementsByTagName('text');	
				data = new Array();
			    for(a=0;a<text.length;a++ ){						     
				     data[a]=cod[a].firstChild.data+'-'+ text[a].firstChild.data; 
				}
				
				$('#'+paramAutoc).flushCache(); //sirve para borrar el cache
				$('#'+paramAutoc).autocomplete(data,{
				   multiple:false,
				   matchContains: true, //sirve para buscar en cualquier parte de la frase
				   cacheLength:1,
				   width:600,
				   height:1500,
				   deferRequestBy: 0, //miliseconds
				   selSeparator: '|', //separador de campos '|' por defecto
				   minChars:1 //minimo de caracteres para empezar a desplegar la lista
			    }
				);
				
			  }else{
				  alert("No se puede Autocompletar");
			     }
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
		}
$('#drag'+ventanaActual.num).find('#'+paramAutoc).focus();
}

function llamarAutocompletarAdministradoraListaEspera(paramAutoc,textoAescribir){// alert(paramAutoc +'-'+textoAescribir);
    //limpiarDivEditarJuan('reportarEventoAdverso');

  //  if( $('#drag'+ventanaActual.num).find('#txtAdministradora1ListaEspera').val()=="" ){// para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
	    datos_a_mandar="accion="+paramAutoc;
		datos_a_mandar=datos_a_mandar+"&textoAescribir="+textoAescribir;				
		varajaxInit=crearAjax();
		varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarAutocomplete_xml.jsp',true);
		varajaxInit.onreadystatechange=function(){respuestallamarAutocompletarAdministradoraListaEspera(paramAutoc)};
		varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		//alert(datos_a_mandar);
		varajaxInit.send(datos_a_mandar);	
//	}	
}
function respuestallamarAutocompletarAdministradoraListaEspera(paramAutoc){  
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('item')!=null){
				cod=raiz.getElementsByTagName('cod');		  
			    text=raiz.getElementsByTagName('text');	
				data = new Array();
			    for(a=0;a<text.length;a++ ){						     
				     data[a]=cod[a].firstChild.data+'-'+ text[a].firstChild.data; 
				}
				
				$('#'+paramAutoc).flushCache(); //sirve para borrar el cache
				$('#'+paramAutoc).autocomplete(data,{
				   multiple:false,
				   matchContains: true, //sirve para buscar en cualquier parte de la frase
				   cacheLength:1,
				   width:600,
				   height:1500,
				   deferRequestBy: 0, //miliseconds
				   selSeparator: '|', //separador de campos '|' por defecto
				   minChars:1 //minimo de caracteres para empezar a desplegar la lista
			    }
				);
				
			  }else{
				  alert("No se puede Autocompletar");
			     }
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
		}
$('#drag'+ventanaActual.num).find('#'+paramAutoc).focus();
}
*/
//----------------------------------------------------------------   FIN  AUTOCOMPLETAR





function mostrarNuevoJuan(arg){    

      limpiarDivEditarJuan(arg); // pa crear un elemento nuevo hay que limpiar primero 
	  switch(arg){ 
		case 'reportarNoConformidad':
		  $('#drag'+ventanaActual.num).find('#divContenido').css('height','250px');
		  $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	      $('#drag'+ventanaActual.num).find('#divEditar').show();
		  $('#drag'+ventanaActual.num).find('#divEditarFases').hide();
		break;
		case 'administrarPlanMejora': 
		  $("#cmbproceso option[value='00']").attr('selected', 'selected');
		  $('#drag'+ventanaActual.num).find('#txtAspectoMejorar').val('');			     			  								
		  $("#cmbTipoPlan option[value='00']").attr('selected', 'selected');			  	    
		  $("#cmbRiesgo option[value='00']").attr('selected', 'selected');			  	    				
		  $("#cmbCosto option[value='00']").attr('selected', 'selected');			  	    								
		  $("#cmbVolumen option[value='00']").attr('selected', 'selected');					
		  $('#drag'+ventanaActual.num).find('#lblTotPrioriza').html(''); 	
		  $('#drag'+ventanaActual.num).find('#lblTotPrioriza').html('');
		  $('#drag'+ventanaActual.num).find('#lblFechaCreacion').html(''); 
		  $('#drag'+ventanaActual.num).find('#lblIdEvento').html(''); 				

		  $('#drag'+ventanaActual.num).find('#divBuscar').show();
	      $('#drag'+ventanaActual.num).find('#divEditar').hide();
		  $('#drag'+ventanaActual.num).find('#divEditarFases').hide();
		  $('#drag'+ventanaActual.num).find('#divContenido').css('height','200px');		  
		  $('#drag'+ventanaActual.num).find('#divEditar').css('height','200px');		  		  
		break;		
		case 'fichaTecnica': 
		  $('#drag'+ventanaActual.num).find('#divBuscar').show();
	      $('#drag'+ventanaActual.num).find('#divEditar').hide();
		break;		
		
		
      }
}





function guardarJuan(arg, pag){  //alert(arg+'--'+pag);  // un nuevo boton adicionar   
	pagina=pag;
	paginaActual=arg;

	switch (arg){		
	
		case 'usuario':  
           if(verificarCamposGuardar()){
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&tipoId="+$('#drag'+ventanaActual.num).find('#cmbTipoid').val();
		       valores_a_mandar=valores_a_mandar+"&id="+valorAtributoIdAutoCompletar('txtId');
		       valores_a_mandar=valores_a_mandar+"&login="+$('#drag'+ventanaActual.num).find('#txtLogin').val();
		       valores_a_mandar=valores_a_mandar+"&contrasena="+$('#drag'+ventanaActual.num).find('#txtContrasena').val();
		       valores_a_mandar=valores_a_mandar+"&rol="+$('#drag'+ventanaActual.num).find('#cmbRol').val();		   
		       valores_a_mandar=valores_a_mandar+"&estado="+$('#drag'+ventanaActual.num).find('#cmbEstado').val();			   			   
			   ajaxModificar();
		   }
		break;	
		case 'reportarNoConformidad': 
            if(verificarCamposGuardar(arg)){
			 valores_a_mandar='accion='+arg;
			 valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbproceso').val();			 
			 valores_a_mandar=valores_a_mandar+"&idNoConformidades="+$('#drag'+ventanaActual.num).find('#cmbNoConformidades').val();
			 valores_a_mandar=valores_a_mandar+"&AreaEvento="+$('#drag'+ventanaActual.num).find('#cmbUbicacionArea').val();
             valores_a_mandar=valores_a_mandar+"&DescripEvento="+$('#drag'+ventanaActual.num).find('#txtDescripEvento').val();
             valores_a_mandar=valores_a_mandar+"&FechaEvento="+$('#drag'+ventanaActual.num).find('#txtFechaEvento').val();
			 valores_a_mandar=valores_a_mandar+"&HoraEvent="+$('#drag'+ventanaActual.num).find('#cmbHoraEvent').val();	
             valores_a_mandar=valores_a_mandar+"&MinutEvent="+$('#drag'+ventanaActual.num).find('#txtMinutEvent').val();
             valores_a_mandar=valores_a_mandar+"&DescripEventoPosTrata="+$('#drag'+ventanaActual.num).find('#txtDescripEventoPosTrata').val();	

			 ajaxGuardar();
		    }
		break;			
		case 'administrarNoConformidad': 
            if(verificarCamposGuardar(arg)){
			 valores_a_mandar='accion='+arg;
			 valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();
			 valores_a_mandar=valores_a_mandar+"&idNoConformidades="+$('#drag'+ventanaActual.num).find('#cmbNoConformidades').val();
             valores_a_mandar=valores_a_mandar+"&ConcepNoConforCalidad="+$('#drag'+ventanaActual.num).find('#txtConcepNoConforCalidad').val();			 
			 valores_a_mandar=valores_a_mandar+"&ResponsaDestinoEvento="+$('#drag'+ventanaActual.num).find('#cmbResponsaDestinoEvento').val();	
			 ajaxGuardar();
		    }
		break;			
		case 'reportarEventoAdverso': 
            if(verificarCamposGuardar(arg)){
			 valores_a_mandar='accion='+arg;			 
			 var datoscadena=$('#drag'+ventanaActual.num).find('#txtBusIdPaciente').val().split('-');			 
			 valores_a_mandar=valores_a_mandar+"&idPaciente="+datoscadena[0];	
			 valores_a_mandar=valores_a_mandar+"&UnidadPertenece="+$('#drag'+ventanaActual.num).find('#cmbUnidadPertenece').val();			 			 
			 valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbproceso').val();			 
			 valores_a_mandar=valores_a_mandar+"&EAProceso="+$('#drag'+ventanaActual.num).find('#cmbEAProceso').val();
			 valores_a_mandar=valores_a_mandar+"&AreaEvento="+$('#drag'+ventanaActual.num).find('#cmbUbicacionArea').val();
             valores_a_mandar=valores_a_mandar+"&DescripEvento="+$('#drag'+ventanaActual.num).find('#txtDescripEvento').val();
             valores_a_mandar=valores_a_mandar+"&FechaEvento="+$('#drag'+ventanaActual.num).find('#txtFechaEvento').val()+' '+$('#drag'+ventanaActual.num).find('#cmbHoraEvent').val()+':00';
             valores_a_mandar=valores_a_mandar+"&Tratamiento="+$('#drag'+ventanaActual.num).find('#txtTratamiento').val();	
			 ajaxGuardar();
		    }
		break;	
		case 'administrarEventoAdverso': 
            if(verificarCamposGuardar(arg)){
			 valores_a_mandar='accion='+arg;
             valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();
		     valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbproceso').val();			 
			 valores_a_mandar=valores_a_mandar+"&idNoConformidades="+$('#drag'+ventanaActual.num).find('#cmbNoConformidades').val();
             ajaxGuardar();
		    }
		break;	
		case 'administrarPlanMejora': 
            if(verificarCamposGuardar(arg)){
				if( $('#drag'+ventanaActual.num).find('#lblIdEvento').html()==""){
				   valores_a_mandar='accion='+arg;
				   valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbproceso').val();			 
				   valores_a_mandar=valores_a_mandar+"&AspectoMejorar="+$('#drag'+ventanaActual.num).find('#txtAspectoMejorar').val();
				   valores_a_mandar=valores_a_mandar+"&TipoPlan="+$('#drag'+ventanaActual.num).find('#cmbTipoPlan').val();			 
				   valores_a_mandar=valores_a_mandar+"&Riesgo="+$('#drag'+ventanaActual.num).find('#cmbRiesgo').val();	
				   valores_a_mandar=valores_a_mandar+"&Costo="+$('#drag'+ventanaActual.num).find('#cmbCosto').val();			 
				   valores_a_mandar=valores_a_mandar+"&Volumen="+$('#drag'+ventanaActual.num).find('#cmbVolumen').val();			 			 
				   ajaxGuardar();
				}  
				else alert('La oportunidad ya existe, pruebe con MODIFICAR'); 								 			  			     
		    }
		break;			
		case 'administrarPlanMejoraEA': //se crea cuando se necesita plan de accion en gestionar evento adverso*/ 
            if(verificarCamposGuardar(arg)){ 
				if( $('#drag'+ventanaActual.num).find('#lblIdEventoEA').html()==""){
				   valores_a_mandar='accion=administrarPlanMejoraEA';
				   valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();
				   valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbprocesoEA').val();			 
				   valores_a_mandar=valores_a_mandar+"&AspectoMejorar="+$('#drag'+ventanaActual.num).find('#txtAspectoMejorar').val();
				   valores_a_mandar=valores_a_mandar+"&TipoPlan="+$('#drag'+ventanaActual.num).find('#cmbTipoPlan').val();			 
				   valores_a_mandar=valores_a_mandar+"&Riesgo="+$('#drag'+ventanaActual.num).find('#cmbRiesgo').val();	
				   valores_a_mandar=valores_a_mandar+"&Costo="+$('#drag'+ventanaActual.num).find('#cmbCosto').val();			 
				   valores_a_mandar=valores_a_mandar+"&Volumen="+$('#drag'+ventanaActual.num).find('#cmbVolumen').val();			 			 

				   ajaxGuardar($('#drag'+ventanaActual.num).find('#lblIdEventoEA').html());
				}  else alert('La oportunidad ya existe, pruebe con MODIFICAR'); 								 			  			     
		    }
		break;	
		case 'relacionEventoOM': //se crea cuando se necesita plan de accion en gestionar evento adverso*/ 
            if(verificarCamposGuardar(arg)){ 
				if( $('#drag'+ventanaActual.num).find('#lblIdEventoOM').html()==""){
				   valores_a_mandar='accion='+arg;
				   valores_a_mandar=valores_a_mandar+"&IdEvento="+evento_OM;
				   valores_a_mandar=valores_a_mandar+"&TipoEvento="+tipo_evento_OM;				   
				   valores_a_mandar=valores_a_mandar+"&idProceso="+valorAtributo('cmbProcesoOM');
				   valores_a_mandar=valores_a_mandar+"&AspectoMejorar="+valorAtributo('txtAspectoMejorarOM');
				   valores_a_mandar=valores_a_mandar+"&TipoPlan="+valorAtributo('cmbTipoPlanOM');
				   valores_a_mandar=valores_a_mandar+"&Riesgo="+valorAtributo('cmbRiesgoOM');
				   valores_a_mandar=valores_a_mandar+"&Costo="+valorAtributo('cmbCostoOM');
				   valores_a_mandar=valores_a_mandar+"&Volumen="+valorAtributo('cmbVolumenOM');
				   ajaxGuardar();
				}  else alert('La oportunidad ya existe, pruebe con MODIFICAR'); 								 			  			     
		    }
		break;		
		case 'administrarPlanMejoraIndicador': //se crea cuando se necesita plan de accion en gestionar evento adverso*/ 
            if(verificarCamposGuardar(arg)){ 
				if( $('#drag'+ventanaActual.num).find('#lblIdEventoEA').html()==""){
				   valores_a_mandar='accion='+arg;
				   valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblId').html();
				   valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbprocesoEA').val();			 
				   valores_a_mandar=valores_a_mandar+"&AspectoMejorar="+$('#drag'+ventanaActual.num).find('#txtAspectoMejorar').val();
				   valores_a_mandar=valores_a_mandar+"&TipoPlan="+$('#drag'+ventanaActual.num).find('#cmbTipoPlan').val();			 
				   valores_a_mandar=valores_a_mandar+"&Riesgo="+$('#drag'+ventanaActual.num).find('#cmbRiesgo').val();	
				   valores_a_mandar=valores_a_mandar+"&Costo="+$('#drag'+ventanaActual.num).find('#cmbCosto').val();			 
				   valores_a_mandar=valores_a_mandar+"&Volumen="+$('#drag'+ventanaActual.num).find('#cmbVolumen').val();			 			 

				   ajaxGuardar($('#drag'+ventanaActual.num).find('#lblIdEventoEA').html());
				}  else alert('La oportunidad ya existe, pruebe con MODIFICAR'); 								 			  			     
		    }
		break;		
		case 'fichaTecnica': 
            if(verificarCamposGuardar(arg)){ 
			//	if( $('#drag'+ventanaActual.num).find('#lblId').html()==""){
				   valores_a_mandar='accion='+arg;
				   valores_a_mandar=valores_a_mandar+"&txtNombre="+valorAtributo('txtNombre');
				   valores_a_mandar=valores_a_mandar+"&cmbTipo="+valorAtributo('cmbTipo');			  
				   valores_a_mandar=valores_a_mandar+"&cmbProceso="+valorAtributo('cmbProceso');			  			  
				   valores_a_mandar=valores_a_mandar+"&txtObjetivo="+valorAtributo('txtObjetivo');			  			  
				   valores_a_mandar=valores_a_mandar+"&cmbEstado="+valorAtributo('cmbEstado');
				   valores_a_mandar=valores_a_mandar+"&cmbPeriodicidad="+valorAtributo('cmbPeriodicidad');			  			  			  			  
				   valores_a_mandar=valores_a_mandar+"&txtFechaVigenciaIni="+valorAtributo('txtFechaVigenciaIni');			  			  			  			  			  
				   valores_a_mandar=valores_a_mandar+"&cmbOrigenInfo="+valorAtributo('cmbOrigenInfo');			  			  			  			  			  			  
				   valores_a_mandar=valores_a_mandar+"&cmbUnidadMedida="+valorAtributo('cmbUnidadMedida');
				   valores_a_mandar=valores_a_mandar+"&txtDescripcionResponsable="+valorAtributo('txtDescripcionResponsable');			  			  
				   valores_a_mandar=valores_a_mandar+"&txtLineaBase="+valorAtributo('txtLineaBase');			  			  			  
				   valores_a_mandar=valores_a_mandar+"&txtMeta="+valorAtributo('txtMeta');	
				  
				   valores_a_mandar=valores_a_mandar+"&txtDescripcionDenominador="+valorAtributo('txtDescripcionDenominador') ;	
				   valores_a_mandar=valores_a_mandar+"&txtDescripcionNumerador="+valorAtributo('txtDescripcionNumerador');		
				   valores_a_mandar=valores_a_mandar+"&txtInfoAdicional="+valorAtributo('txtInfoAdicional');						   
				   valores_a_mandar=valores_a_mandar+"&cmbTendencia="+valorAtributo('cmbTendencia');	
				   ajaxGuardar();          
			//	}  else alert('Elemento ya existe, pruebe con MODIFICAR'); 	
		    } 
		break;		
  
	}

	}
	
	
function guardarPestanas(casoList){ // envia a guardar los datos nuevos de cada listado tipo de 2 paramertros
	  banDatosNew=0;
	  valores_a_mandar="accion="+casoList;
	  valores_a_mandar=valores_a_mandar+"&id_cita="+$('#drag'+ventanaActual.num).find('#id_cita').val();	
	  valores_a_mandar=valores_a_mandar+"&examenes="
	  
	  var ids = jQuery("#"+casoList).getDataIDs();
	  for(var i=0;i<ids.length;i++){ 
		  var c = ids[i];
		  var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
		  valorestado=$.trim(datosDelRegistro.tipo);  

		  if(valorestado=='1'){
			  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.idTipoExa)+'_-'+$.trim(datosDelRegistro.descripciones)+',-' ;		
			  banDatosNew=1;
		 }
	  }
    if(banDatosNew==1){  // siempre y cuando existan datos nuevos de tipo 1
    varajaxInit=crearAjax();
    varajaxInit.open("POST",'/clinica/paginas/accionesXml/guardar_xml.jsp',true);
	varajaxInit.onreadystatechange=respuestaGuardarListST2;
	varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxInit.send(valores_a_mandar); 
	}

}

function respuestaGuardarListST2(){
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('respuesta')[0].firstChild.data=='true'){
				  alert("Se guard� Exitosamente");

			  }else{
				  alert("No puedo guardar los cambios del Listado del Examen ");
			     }
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}
function guardarPestanasListSTV(casoList){// envia a guardar los datos nuevos de cada listado tipo de 2 paramertros
	  banDatosNew=0;   valores_a_mandal='';
	  valores_a_mandar="accion="+casoList;

    switch(casoList){  
		case  'listPlanAccion': 
		    if(verificarCamposGuardar(casoList)){
			  banDatosNew=1;
			  valores_a_mandar=valores_a_mandar+"&IdEventoRef="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();
			  valores_a_mandar=valores_a_mandar+"&idTipoPlanAccion="+$('#drag'+ventanaActual.num).find('#cmbTipoPlanAccion').val();
			  valores_a_mandar=valores_a_mandar+"&AnalisisCausas="+$('#drag'+ventanaActual.num).find('#txtAnalisisCausas').val();			  
              valores_a_mandar=valores_a_mandar+"&FechaIniPlanAccion="+$('#drag'+ventanaActual.num).find('#txtFechaIniPlanAccion').val();			 
              valores_a_mandar=valores_a_mandar+"&MetaPlanAccion="+$('#drag'+ventanaActual.num).find('#txtMetaPlanAccion').val();	
			  valores_a_mandar=valores_a_mandar+"&examenes="
			  var ids = jQuery("#"+casoList).getDataIDs();
			  for(var i=0;i<ids.length;i++){ 
				  var c = ids[i];
				  var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
				  valorestado=$.trim(datosDelRegistro.tipo);  		
				  //if(valorestado=='1'){
					  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.Caracter)+'_-'+$.trim(datosDelRegistro.RealizarAcciones)+'_-'+$.trim(datosDelRegistro.FechaCuando)+'_-'+$.trim(datosDelRegistro.quienRealizaAccion)+',-' ;		
					  banDatosNew=1;
				// }
			  }
			  valores_a_mandar=valores_a_mandar+"&examenes2="		// para las personas asociadas al plan de accion	 
			  var ids = jQuery("#listPersonasPlanAccion").getDataIDs();			   
			  for(var i=0;i<ids.length;i++){ 
				  var c = ids[i];
				  var datosDelRegistro = jQuery("#listPersonasPlanAccion").getRowData(c);  
				  valorestado=$.trim(datosDelRegistro.tipo);  //los historicos 
				  
				 // if(valorestado=='1'){
					  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.IdPersona)+'_-' ;		
					  valores_a_mandar1=valores_a_mandar1+$.trim(datosDelRegistro.IdPersona)+'_-' ;	

					  banDatosNew=1;
				// }
			  }		
			 // alert(valores_a_mandar1);
			  
			}  
		break;	
		case  'listTareasPlanAccion': 
		    if(verificarCamposGuardar(casoList)){
			  banDatosNew=1;
			  valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();
			  valores_a_mandar=valores_a_mandar+"&lblIdPlanAccion="+$('#drag'+ventanaActual.num).find('#lblIdPlanAccion').html();			  
			  valores_a_mandar=valores_a_mandar+"&idTipoPlanAccion="+$('#drag'+ventanaActual.num).find('#cmbTipoPlanAccion').val();
			  valores_a_mandar=valores_a_mandar+"&RiesgoNoHacerlo="+$('#drag'+ventanaActual.num).find('#txtRiesgoNoHacerlo').val();			  
			  valores_a_mandar=valores_a_mandar+"&BeneficiosHacerlo="+$('#drag'+ventanaActual.num).find('#txtBeneficiosHacerlo').val();			  			  
              valores_a_mandar=valores_a_mandar+"&FechaIniPlanAccion="+$('#drag'+ventanaActual.num).find('#txtFechaIniPlanAccion').val();			 
              valores_a_mandar=valores_a_mandar+"&MetaPlanAccion="+$('#drag'+ventanaActual.num).find('#txtMetaPlanAccion').val();	
			  valores_a_mandar=valores_a_mandar+"&ConceptoGral="+$('#drag'+ventanaActual.num).find('#txtConceptoGral').val();
	          valores_a_mandar=valores_a_mandar+"&idResponsable="+$('#drag'+ventanaActual.num).find('#cmbResponsable').val();			  
			  
			  valores_a_mandar=valores_a_mandar+"&tareas="
			  var ids = jQuery("#"+casoList).getDataIDs();
			  for(var i=0;i<ids.length;i++){ 
				  var c = ids[i];
				  var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
				 		
				  if($.trim(datosDelRegistro.tipo)!='ajena'){
					  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.Caracter)+'_-'+$.trim(datosDelRegistro.RealizarAcciones)+'_-'+$.trim(datosDelRegistro.FechaCuando)+'_-'+$.trim(datosDelRegistro.quienRealizaAccion)+'_-'+$.trim(datosDelRegistro.costosAdd)+'_-'+$.trim(datosDelRegistro.idEstado)+ ',-' ;	
					  banDatosNew=1;
				  }
				  
			  }
			  
			  valores_a_mandar=valores_a_mandar+"&personas="		// para las personas asociadas al plan de accion	 
			  var ids = jQuery("#listPersonasPlanAccionGral").getDataIDs();			   
			  for(var i=0;i<ids.length;i++){ 
				  var c = ids[i];
				  var datosDelRegistro = jQuery("#listPersonasPlanAccionGral").getRowData(c);  
				  valorestado=$.trim(datosDelRegistro.tipo);  //los historicos 
				  
				 // if(valorestado=='1'){
					  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.IdPersona)+'_-' ;		
					  valores_a_mandar1=valores_a_mandar1+$.trim(datosDelRegistro.IdPersona)+'_-' ;	
					  banDatosNew=1;
				// }
			  }		
			  valores_a_mandar=valores_a_mandar+"&espinaPescado="		
			  var ids = jQuery("#listEspinaPescado").getDataIDs();			   
			  for(var i=0;i<ids.length;i++){ 
				  var c = ids[i];
				  var datosDelRegistro = jQuery("#listEspinaPescado").getRowData(c);  
				 // valorestado=$.trim(datosDelRegistro.tipo);  //los historicos 
				  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.IdCriterio)+'_-'+$.trim(datosDelRegistro.Analisis)+',-' ;		
  				  banDatosNew=1;
			  }				  
			  
			}  
		break;			
		
		
		case  'listPlanAccionSeguimiento': 
		    //if(verificarCamposGuardar(casoList)){
	       if($('#drag'+ventanaActual.num).find('#lblEventoPadreAsocia').html()=='-'){	
			  banDatosNew=1;
  			  valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEventoDocSeg').html();
			  valores_a_mandar=valores_a_mandar+"&IdEventoRef="+$('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html();
			  valores_a_mandar=valores_a_mandar+"&Eficaz="+$('#drag'+ventanaActual.num).find('#cmbEficaz').val();
			  valores_a_mandar=valores_a_mandar+"&SeguimMetaPlanAccion="+$('#drag'+ventanaActual.num).find('#txtSeguimMetaPlanAccion').val();			  
			  valores_a_mandar=valores_a_mandar+"&examenes="
			  var ids = jQuery("#"+casoList).getDataIDs();
			  for(var i=0;i<ids.length;i++){ 
				  var c = ids[i];
				  var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
				  valorestado=$.trim(datosDelRegistro.cumple);  
		
				  if(valorestado!='00'){
					  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.idISO)+'_-'+$.trim(datosDelRegistro.cumple)+'_-'+$.trim(datosDelRegistro.RealizarAccionesSeguimiento)+',-' ;		
					  banDatosNew=1;
				 }
			  }			  
		   }  
		   else alert('No se puede modificar porque el documento est� Relacionado con el Evento '+$('#drag'+ventanaActual.num).find('#lblEventoPadreAsocia').html());
		break;			
		case  'listAsociarEventos': 
		      valores_a_mandar1='';
			  valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();
			  valores_a_mandar=valores_a_mandar+"&examenes="
			 
			  var ids = jQuery("#"+casoList).getDataIDs();
			  banDatosNew=1;
			   
			  for(var i=0;i<ids.length;i++){ 
				  var c = ids[i];
				  var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
				  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.IdEvento)+'_-' ;
				  valores_a_mandar1=valores_a_mandar1+$.trim(datosDelRegistro.IdEvento)+'_-' ;


			  }
		break;	  
		case  'listPersonasPlanAccion': 
		      valores_a_mandar=valores_a_mandar+"&sidx=1&IdEventoRef="+$('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html();		
			  valores_a_mandar=valores_a_mandar+"&examenes="			 
			  var ids = jQuery("#"+casoList).getDataIDs();			   
			  for(var i=0;i<ids.length;i++){ 
				  var c = ids[i];
				  var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
				  valorestado=$.trim(datosDelRegistro.tipo);  //los historicos 
				  
				 // if(valorestado=='1'){
					  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.IdPersona)+'_-' ;		
					  banDatosNew=1;
				// }
			  }
		break;	
		case  'listAsociarEventosTareas': 
		      valores_a_mandar1='';
			  valores_a_mandar=valores_a_mandar+"&IdPlanAccion="+$('#drag'+ventanaActual.num).find('#lblIdPlanAccion').html();
		     // valores_a_mandar=valores_a_mandar+"&tareas="	
			  var ids = jQuery("#"+casoList).getDataIDs();
			  banDatosNew=1;
			   
			  if(ids.length>0){ 
				  valores_a_mandar=valores_a_mandar+"&tareas="	
				  for(var i=0;i<ids.length;i++){ 
					  var c = ids[i];
					  var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
						  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.idPlan)+'_-'+$.trim(datosDelRegistro.idISO)+',-' ;		
				  }
			  }	
			  else valores_a_mandar=valores_a_mandar+"&tareas=,-"

		break;		
		case  'listFactContribu': 
		
			  if(verificarCamposGuardar(casoList)){
				  banDatosNew=1;
				  valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();
				  valores_a_mandar=valores_a_mandar+"&BarreraYDefensa="+$('#drag'+ventanaActual.num).find('#cmbBarreraYDefensa').val();
				  valores_a_mandar=valores_a_mandar+"&AccionInsegura="+$('#drag'+ventanaActual.num).find('#cmbAccionInsegura').val();
				  valores_a_mandar=valores_a_mandar+"&Ambito="+$('#drag'+ventanaActual.num).find('#cmbAmbito').val();
				  valores_a_mandar=valores_a_mandar+"&Prevenible="+$('#drag'+ventanaActual.num).find('#cmbPrevenible').val();
				  valores_a_mandar=valores_a_mandar+"&OrganizCultura="+$('#drag'+ventanaActual.num).find('#cmbOrganizCultura').val();			  
	
				  var ids = jQuery("#"+casoList).getDataIDs();
				  banDatosNew=1;
				  if(ids.length>0){ 
					  valores_a_mandar=valores_a_mandar+"&origenFactores="	
					  for(var i=0;i<ids.length;i++){ 
						  var c = ids[i];
						  var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
							  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.Id)+'_-' ;		
					  }				  			  
				  }	
				  else valores_a_mandar=valores_a_mandar+"&origenFactores=,-"
				  
				  valores_a_mandar=valores_a_mandar+"&oportMejora="		
				  var ids = jQuery("#listPersonasOportMejora").getDataIDs();	
				  if(ids.length>0){ 
					  for(var i=0;i<ids.length;i++){ 
						  var c = ids[i];
						  var datosDelRegistro = jQuery("#listPersonasOportMejora").getRowData(c);  
						  valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.actividad)+'_-'+$.trim(datosDelRegistro.fecha)+'_-'+$.trim(datosDelRegistro.idPersona)+'_-'+$.trim(datosDelRegistro.costos)+'_-'+$.trim(datosDelRegistro.horas)+',-' ;		
						  banDatosNew=1;
					  }		
				  }	
				  else valores_a_mandar=valores_a_mandar+"&oportMejora=,-"
			  }	
//			  alert(valores_a_mandar);
		break;	
		case  'listSeguimientoOportMejora': // ea
		      if(verificarCamposGuardar(casoList)){
				  valores_a_mandar1='';
				  valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();
				  var ids = jQuery("#"+casoList).getDataIDs();
				  banDatosNew=1;			   
				  if(ids.length>0){ 
					  valores_a_mandar=valores_a_mandar+"&tareas="	
					  for(var i=0;i<ids.length;i++){ 
						  var c = ids[i];
						  var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
						  /*Si el campo esta vacio o fue editado por este usuario de sesion se puede modificar o guardar*/
						  if( $.trim(datosDelRegistro.IdElaboroSeguimTarea)==$('#drag'+ventanaActual.num).find('#IdUsuario').val() || $.trim(datosDelRegistro.IdElaboroSeguimTarea)=='' ){
							if( datosDelRegistro.codOportMejora!='00')// deben de enviarse a update solo los que esten diligenciados							 
							 valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.idTarea)+'_-'+$.trim(datosDelRegistro.codOportMejora)+'_-'+$.trim(datosDelRegistro.codTiempoEjec)+'_-'+$.trim(datosDelRegistro.tareaSeguimiento)+',-' ;
						  
						  
						  }	
							  
					  }
				  }	
				  else valores_a_mandar=valores_a_mandar+"&tareas=,-"
			  }
		break;
		case  'listNivelResponsabili': // ea
		      if(verificarCamposGuardar(casoList)){
				  valores_a_mandar1='';
				  valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();
				  valores_a_mandar=valores_a_mandar+"&Conclusiones="+$('#drag'+ventanaActual.num).find('#txtConclusiones').val();				  
				  var ids = jQuery("#"+casoList).getDataIDs();
				  banDatosNew=1;			   
				  if(ids.length>0){ 
					  valores_a_mandar=valores_a_mandar+"&tareas="	
					  for(var i=0;i<ids.length;i++){ 
						  var c = ids[i];
						  var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
						  /*Si el campo esta vacio o fue editado por este usuario de sesion se puede modificar o guardar*/
						  if( $.trim(datosDelRegistro.IdElaboroSeguimTarea)==$('#drag'+ventanaActual.num).find('#IdUsuario').val() || $.trim(datosDelRegistro.IdElaboroSeguimTarea)=='' ){
							// valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.idTarea)+'_-'+$.trim(datosDelRegistro.codOportMejora)+'_-'+$.trim(datosDelRegistro.codTiempoEjec)+'_-'+$.trim(datosDelRegistro.tareaSeguimiento)+',-' ;
							 valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.descResponsabili)+'_-'+$.trim(datosDelRegistro.idNivel)+',-' ;						  
						  }	
							  
					  }
				  }	
				  else valores_a_mandar=valores_a_mandar+"&tareas=,-"
			  }
		break;	
		case  'listSeguimientoPlanAccion': // oportunidad mejora  valores_a_mandar=valores_a_mandar+"&sidx=1&lblIdPlanAccion="+$('#drag'+ventanaActual.num).find('#lblIdPlanAccion').html();
		      if(verificarCamposGuardar(casoList)){
				  varCumple=1;
				  valores_a_mandar1='';
				  valores_a_mandar=valores_a_mandar+"&IdPlanAccion="+$('#drag'+ventanaActual.num).find('#lblIdPlanAccion').html();
				  var ids = jQuery("#"+casoList).getDataIDs();
				  banDatosNew=1;			   
				  if(ids.length>0){ 
					  valores_a_mandar=valores_a_mandar+"&tareas="	
					  for(var i=0;i<ids.length;i++){ 
						  var c = ids[i];
						  var datosDelRegistro = jQuery("#"+casoList).getRowData(c);  
						  /*Si el campo esta vacio o fue editado por este usuario de sesion se puede modificar o guardar*/
						  if( $.trim(datosDelRegistro.IdElaboroSeguimTarea)==$('#drag'+ventanaActual.num).find('#IdUsuario').val() || $.trim(datosDelRegistro.IdElaboroSeguimTarea)=='' ){
							if( datosDelRegistro.Cumple!='')// deben de enviarse a update solo los que esten diligenciados							 
							 valores_a_mandar=valores_a_mandar+$.trim(datosDelRegistro.idTarea)+'_-'+$.trim(datosDelRegistro.Cumple)+'_-'+$.trim(datosDelRegistro.tareaSeguimiento)+',-' ;
						  }	
						  if(datosDelRegistro.Cumple==''){
							  varCumple=0;
						  }
						  
					  }
				      
					  if(varCumple==1){ /*para verificar que toda tarea se encuentra con seguimiento*/
						  
						 if($('#drag'+ventanaActual.num).find('#cmbEficaz').val()!='00'){ 
							 alert('Despues de esta acci�n finalizar� el pl�n y se cerrar�');
							 valores_a_mandar=valores_a_mandar+"&seguimientoEficaz="+$('#drag'+ventanaActual.num).find('#cmbEficaz').val();			  				  
							 valores_a_mandar=valores_a_mandar+"&seguimientoMeta="+$('#drag'+ventanaActual.num).find('#txtSeguimMetaPlanAccion').val();				  
						 }
						 else{
						   valores_a_mandar=valores_a_mandar+"&seguimientoEficaz=00";			  				  
						   valores_a_mandar=valores_a_mandar+"&seguimientoMeta=''";	
						 }
					  }
					  else{
						 alert('Aun no puede cerrar porque existen tareas sin realizar seguimiento'); 
					     valores_a_mandar=valores_a_mandar+"&seguimientoEficaz=00";			  				  
					     valores_a_mandar=valores_a_mandar+"&seguimientoMeta=''";	
						 $('#drag'+ventanaActual.num).find('#txtSeguimMetaPlanAccion').val('');
						 $("#cmbEficaz option[value='00']").attr('selected', 'selected');
					  }
					  
					  
				  }	
				  else valores_a_mandar=valores_a_mandar+"&tareas=,-"
			  }
		break;		
		
	}
    if(banDatosNew==1){  // siempre y cuando existan datos nuevos de tipo 1
    varajaxInit=crearAjax();
    varajaxInit.open("POST",'/clinica/paginas/accionesXml/guardar_xml.jsp',true);
	varajaxInit.onreadystatechange=respuestaGuardarListSTV;
	varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded'); 
	//alert(valores_a_mandar);
	varajaxInit.send(valores_a_mandar); 
	}

}

function respuestaGuardarListSTV(){
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('respuesta')[0].firstChild.data=='true'){

					  switch (raiz.getElementsByTagName('accion')[0].firstChild.data){
						case 'listPlanAccion':
							 alert("Se guard� exitosamente el Listado");
							barraBuscar('administrarNoConformidad','/clinica/paginas/accionesXml/buscar_xml.jsp');
						break;
						case 'listAsociarEventosTareas':
							  listadoTabla('listTareasPlanAccion');
							  alert("Se guard� exitosamente el Listado de tareas ajenas");
						break;						
						case 'listTareasPlanAccion':
							 listadoTabla('listTareasPlanAccion');
						alert("Se guard� exitosamente el Plan de acci�n");
							if( $('#drag'+ventanaActual.num).find('#lblIdPlanAccion').html()=="" )						   
							   barraBuscar('administrarPlanMejora','/clinica/paginas/accionesXml/buscar_xml.jsp');
						break;						
						case 'listSeguimientoOportMejora':
						alert("Se guard� exitosamente el Seguimiento a la Oportunidad de Mejora");
                               listadoTabla('listSeguimientoOportMejora');

						break;						
						
						default:
		   				    alert("Se guard� exitosamente el Listado");
						break;
					  }

			  }else{
				  alert("No puedo guardar los cambios del Listado");
			     }
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}


/* Mensaje personalizado segun ventana. Cuando es positivo, o verdadero el guardar */
function respuestaGuardarJuan(arg, xmlraiz){ 

		switch(arg){
		  case 'reportarNoConformidad':
	        alert("!No conformidad registrada Exitosamente");
			limpiarDivEditarJuan('reportarNoConformidad');
		  break;  
		  case 'administrarNoConformidad':
	        alert("!No conformidad administrada y guardada Exitosamente");
		  break;  
		  
		  case 'reportarEventoAdverso':
	        alert("!Evento Adverso registrado Exitosamente");
			limpiarDivEditarJuan('reportarEventoAdverso');
			
		  break;  
		  case 'administrarEventoAdverso':
	        alert("!Evento Adverso modificado Exitosamente");
		  break;  
		  
		  case 'administrarPlanMejora':		  
	        alert("!Oportunidad de mejora guardada Exitosamente");
			$("#cmbproceso option[value='00']").attr('selected', 'selected');	
			$('#drag'+ventanaActual.num).find('#txtAspectoMejorar').val('');	
			$('#drag'+ventanaActual.num).find('#lblEstadoEvent').html(''); 		
			$("#cmbTipoPlan option[value='00']").attr('selected', 'selected');				
			$("#cmbRiesgo option[value='00']").attr('selected', 'selected');				
			$("#cmbCosto option[value='00']").attr('selected', 'selected');				
			$("#cmbVolumen option[value='00']").attr('selected', 'selected');
			$('#drag'+ventanaActual.num).find('#lblTotPrioriza').html('0'); 				
		  break;  
          case 'administrarPlanMejoraEA':		  
	        alert("!Guardada Exitosamente, continue con el plan de acci�n de acreditacion");
		    BuscarrelacionPlanAccionEvento();

          default:  	alert('!! Modificaci�n exitosa');
		  
		}		
}

function vacio(valor){if(valor==null){return true}for(var i=0;i<valor.length;i++){if((valor.charAt(i)!=' ')&&(valor.charAt(i)!="\t")&&(valor.charAt(i)!="\n")&&(valor.charAt(i)!="\r")){return false}}return true}

/** funciones para la accion de modificacion esta en mi pc  Franklin la tiene como =modificarJuan*/
function modificarJuan(arg, pag){   // PARA UPDATE

    pagina=pag;
	paginaActual=arg;	
	switch (arg){
		case 'reportarNoConformidad': 
            if(verificarCamposModificar(arg)){ 
			 valores_a_mandar='accion='+arg;
 			 valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();
			 valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbproceso').val();			 
			 valores_a_mandar=valores_a_mandar+"&idNoConformidades="+$('#drag'+ventanaActual.num).find('#cmbNoConformidades').val();
			 valores_a_mandar=valores_a_mandar+"&AreaEvento="+$('#drag'+ventanaActual.num).find('#cmbUbicacionArea').val();
             valores_a_mandar=valores_a_mandar+"&DescripEvento="+$('#drag'+ventanaActual.num).find('#txtDescripEvento').val();
             valores_a_mandar=valores_a_mandar+"&FechaEvento="+$('#drag'+ventanaActual.num).find('#txtFechaEvento').val();
			 valores_a_mandar=valores_a_mandar+"&HoraEvent="+$('#drag'+ventanaActual.num).find('#cmbHoraEvent').val();	
             valores_a_mandar=valores_a_mandar+"&MinutEvent="+$('#drag'+ventanaActual.num).find('#txtMinutEvent').val();
             valores_a_mandar=valores_a_mandar+"&DescripEventoPosTrata="+$('#drag'+ventanaActual.num).find('#txtDescripEventoPosTrata').val();			 
			 valores_a_mandar=valores_a_mandar+"&IdISORef="+$('#drag'+ventanaActual.num).find('#lblIdISORef').html();			 			 
			 ajaxModificar();
		    }	
		 break;  
		case 'administrarPlanMejora':  						
            if(verificarCamposGuardar(arg)){				
				if($('#drag'+ventanaActual.num).find('#lblIdEvento').html()!=""){
					  // if( $('#drag'+ventanaActual.num).find('#lblIdPlanAccion').html()=="" ){	 // para validar si exise plan de accion que no permita guardar							
						 valores_a_mandar='accion='+arg;
						 valores_a_mandar=valores_a_mandar+"&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();			 				   
						 valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbproceso').val();			 
						 valores_a_mandar=valores_a_mandar+"&AspectoMejorar="+$('#drag'+ventanaActual.num).find('#txtAspectoMejorar').val();
						 valores_a_mandar=valores_a_mandar+"&TipoPlan="+$('#drag'+ventanaActual.num).find('#cmbTipoPlan').val();			 
						 valores_a_mandar=valores_a_mandar+"&Riesgo="+$('#drag'+ventanaActual.num).find('#cmbRiesgo').val();	
						 valores_a_mandar=valores_a_mandar+"&Costo="+$('#drag'+ventanaActual.num).find('#cmbCosto').val();			 
						 valores_a_mandar=valores_a_mandar+"&Volumen="+$('#drag'+ventanaActual.num).find('#cmbVolumen').val();			 			 

						 ajaxModificar();
					  // } else alert('!! No se puede modificar la OPORTUNIDAD DE MEJORA, porque  tiene un pl�n de acci�n asociado');
				}else alert('No se puede modificar porque a�n no ha sido creado');				   			   
		    }
		break;		
		case 'administrarEventoAdverso':  						
            if(verificarCamposGuardar(arg)){				
				if($('#drag'+ventanaActual.num).find('#lblIdEvento').html()!=""){
					  // if( $('#drag'+ventanaActual.num).find('#lblIdPlanAccion').html()=="" ){	 // para validar si exise plan de accion que no permita guardar							
						 valores_a_mandar='accion='+arg;
						 valores_a_mandar=valores_a_mandar+"&idEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();			 				   
						 valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbproceso').val();			 
						 valores_a_mandar=valores_a_mandar+"&idEventoProceso="+$('#drag'+ventanaActual.num).find('#cmbEventoProceso').val();
						// alert('dddd '+valores_a_mandar);
						ajaxModificar();
					  // } else alert('!! No se puede modificar la OPORTUNIDAD DE MEJORA, porque  tiene un pl�n de acci�n asociado');
				}else alert('No se puede modificar porque a�n no ha sido creado');				   			   
		    }
		break;	
		case 'usuario':
			var passwd = $('#drag'+ventanaActual.num).find('#txtContrasena').val();
			   if ($('#drag'+ventanaActual.num).find('#txtContrasena').val() != $('#drag'+ventanaActual.num).find('#txtContrasena2').val())
			   {
				   alert('NO COINCIDEN LAS CONTRASE\u00d1AS')
				   limpiaAtributo('txtContrasena',0)
				   limpiaAtributo('txtContrasena2',0)
			   }
			   else
			   {
				if(/[A-Z]/.test(passwd) && /[0-9]/.test(passwd))
				{
		       		valores_a_mandar=valores_a_mandar+"&tipoId="+$('#drag'+ventanaActual.num).find('#cmbTipoid').val();
		       		valores_a_mandar=valores_a_mandar+"&id="+$('#drag'+ventanaActual.num).find('#txtId').val();
		       		valores_a_mandar=valores_a_mandar+"&login="+$('#drag'+ventanaActual.num).find('#txtLogin').val();
		       		valores_a_mandar=valores_a_mandar+"&contrasena="+$('#drag'+ventanaActual.num).find('#txtContrasena').val();
		       		valores_a_mandar=valores_a_mandar+"&rol="+$('#drag'+ventanaActual.num).find('#cmbRol').val();		   
		       		valores_a_mandar=valores_a_mandar+"&estado="+$('#drag'+ventanaActual.num).find('#cmbEstado').val();			   			   
			   		ajaxModificar();
				}
				else{
					alert('LA CONTRASE\u00d1A DEBE TENER AL MENOS UNA MAYUSCULA Y UN NUMERO')
					limpiaAtributo('txtContrasena',0)
					 limpiaAtributo('txtContrasena2',0)
				}		 
			   }             		       		   
		break;
		case 'password':
			   var passwd = $('#drag'+ventanaActual.num).find('#txtContrasena').val();
			   if ($('#drag'+ventanaActual.num).find('#txtContrasena').val() != $('#drag'+ventanaActual.num).find('#txtContrasena2').val())
			   {
				   alert('NO COINCIDEN LAS CONTRASE\u00d1AS')
				   limpiaAtributo('txtContrasena',0)
				   limpiaAtributo('txtContrasena2',0)
			   }
			   else{
			   if(/[A-Z]/.test(passwd) && /[0-9]/.test(passwd))
			   {				   
					valores_a_mandar="accion="+arg;			   
					valores_a_mandar=valores_a_mandar+"&tipoId="+document.getElementById('lblTipoId').innerHTML;			   
					valores_a_mandar=valores_a_mandar+"&id="+document.getElementById('lblIdUsuarioSesion').innerHTML;			   			   
					valores_a_mandar=valores_a_mandar+"&login="+document.getElementById('lblLoginUsuarioSesion').innerHTML;			   
					valores_a_mandar=valores_a_mandar+"&contrasena="+$('#drag'+ventanaActual.num).find('#txtContrasena').val();			   			   
					valores_a_mandar=valores_a_mandar+"&rol="+$('#drag'+ventanaActual.num).find('#cmbRol').val();		   			   			   
					valores_a_mandar=valores_a_mandar+"&estado="+$('#drag'+ventanaActual.num).find('#cmbEstado').val();			   
					ajaxModificar();
			   }
			   else{
				   alert('LA CONTRASE\u00d1A DEBE TENER AL MENOS UNA MAYUSCULA Y UN NUMERO')
				   limpiaAtributo('txtContrasena',0)
					limpiaAtributo('txtContrasena2',0)
			   }
			}					   		   
		break;
		case 'fichaTecnica': 
            if(verificarCamposGuardar(arg)){ 
				if( $('#drag'+ventanaActual.num).find('#lblId').html()!=""  ){
				   valores_a_mandar='accion='+arg;
				   valores_a_mandar=valores_a_mandar+"&lblId="+valorAtributo('lblId');				   
				   valores_a_mandar=valores_a_mandar+"&txtNombre="+valorAtributo('txtNombre');
				   valores_a_mandar=valores_a_mandar+"&cmbTipo="+valorAtributo('cmbTipo');			  
				   valores_a_mandar=valores_a_mandar+"&cmbProceso="+valorAtributo('cmbProceso');			  			  
				   valores_a_mandar=valores_a_mandar+"&txtObjetivo="+valorAtributo('txtObjetivo');			  			  
				   valores_a_mandar=valores_a_mandar+"&cmbEstado="+valorAtributo('cmbEstado');
				   valores_a_mandar=valores_a_mandar+"&cmbPeriodicidad="+valorAtributo('cmbPeriodicidad');			  			  			  			  
				   valores_a_mandar=valores_a_mandar+"&txtFechaVigenciaIni="+valorAtributo('txtFechaVigenciaIni');			  			  			  			  			  
				   valores_a_mandar=valores_a_mandar+"&cmbOrigenInfo="+valorAtributo('cmbOrigenInfo');			  			  			  			  			  			  
				   valores_a_mandar=valores_a_mandar+"&cmbUnidadMedida="+valorAtributo('cmbUnidadMedida');
				   valores_a_mandar=valores_a_mandar+"&txtDescripcionResponsable="+valorAtributo('txtDescripcionResponsable');			  			  
				   valores_a_mandar=valores_a_mandar+"&txtLineaBase="+valorAtributo('txtLineaBase');			  			  			  
				   valores_a_mandar=valores_a_mandar+"&txtMeta="+valorAtributo('txtMeta');	
				  
				   valores_a_mandar=valores_a_mandar+"&txtDescripcionDenominador="+valorAtributo('txtDescripcionDenominador') ;	
				   valores_a_mandar=valores_a_mandar+"&txtDescripcionNumerador="+valorAtributo('txtDescripcionNumerador');		
				   valores_a_mandar=valores_a_mandar+"&txtInfoAdicional="+valorAtributo('txtInfoAdicional');						   
   				   ajaxGuardar();          
				}  else alert('Elemento ya existe, pruebe con NUEVO'); 	
		    } 
		break;		
        case 'indicador': 
            if(verificarCamposGuardar(arg)){ 
				if( $('#drag'+ventanaActual.num).find('#lblId').html()!=""){
				   valores_a_mandar='accion='+arg;
				   valores_a_mandar=valores_a_mandar+"&lblId="+valorAtributo('lblId');				   
				   valores_a_mandar=valores_a_mandar+"&ValNumerador="+valorAtributo('txtValNumerador');
				   valores_a_mandar=valores_a_mandar+"&ValExclusionNumerador="+valorAtributo('txtValExclusionNumerador');
				   valores_a_mandar=valores_a_mandar+"&CriterioExclusionNumerador="+valorAtributo('txtCriterioExclusionNumerador');		

				   valores_a_mandar=valores_a_mandar+"&ValDenominador="+valorAtributo('txtValDenominador');
				   valores_a_mandar=valores_a_mandar+"&ValExclusionDenominador="+valorAtributo('txtValExclusionDenominador');
				   valores_a_mandar=valores_a_mandar+"&CriterioExclusionDenominador="+valorAtributo('txtCriterioExclusionDenominador');				   

				   valores_a_mandar=valores_a_mandar+"&txtAnalisis="+valorAtributo('txtAnalisis');				   
				   valores_a_mandar=valores_a_mandar+"&lblPeriodo="+valorAtributo('lblPeriodo');				   				   
				   
				   valores_a_mandar=valores_a_mandar+"&cmbEstado="+valorAtributo('cmbEstado');			  
   				  // alert('pa modificar'+valores_a_mandar);
				   ajaxGuardar();          
				}  else alert('Elemento ya existe, pruebe con NUEVO'); 	
		    } 
		break;				
		
	}
}




/** funciones para la accion de eliminacion*/
function eliminarJuan(arg, pag){ 
	pagina=pag;
	paginaActual=arg;	
	switch (arg){
		case 'ubicaciones':
		  if(document.getElementById('hddOpcion').value=='Pais'){
		       if(document.getElementById('cmbPaises').options[document.getElementById('cmbPaises').selectedIndex].value!='00'){
			       if(confirm('Realmente desea eliminar el pais: '+document.getElementById('cmbPaises').options[document.getElementById('cmbPaises').selectedIndex].text+'?')){
				      valores_a_mandar="";
				      valores_a_mandar=valores_a_mandar+"accion="+arg;
				      valores_a_mandar=valores_a_mandar+"&codigo="+document.getElementById('cmbPaises').options[document.getElementById('cmbPaises').selectedIndex].value;
                      valores_a_mandar=valores_a_mandar+"&opcion=Pais";
				      ajaxEliminar();
				   }
			    }else{
  				   alert('Favor seleccionar un pais, para ser Eliminado.');
				}
		   }else if(document.getElementById('hddOpcion').value=='Depto'){
			    if(document.getElementById('cmbDepartamento').options[document.getElementById('cmbDepartamento').selectedIndex].value!='00'){
				    if(confirm('Realmente desea eliminar el Departamento: '+document.getElementById('cmbDepartamento').options[document.getElementById('cmbDepartamento').selectedIndex].text+'?')){
					   valores_a_mandar="";
				       valores_a_mandar=valores_a_mandar+"accion="+arg;
	    		       valores_a_mandar=valores_a_mandar+"&codigo="+document.getElementById('cmbDepartamento').options[document.getElementById('cmbDepartamento').selectedIndex].value;
                       valores_a_mandar=valores_a_mandar+"&codPais="+document.getElementById('cmbPaises').options[document.getElementById('cmbPaises').selectedIndex].value;
                       valores_a_mandar=valores_a_mandar+"&opcion=Depto";
				       ajaxEliminar();
					}
				}else{
					alert('Favor seleccionar un Departamento, para ser Eliminado.');
				}
		   }else if(document.getElementById('hddOpcion').value=='Municipio'){
			    if(document.getElementById('cmbMunicipio').options[document.getElementById('cmbMunicipio').selectedIndex].value!='00'){
					if(confirm('Realmente desea eliminar el Municipio: '+document.getElementById('cmbMunicipio').options[document.getElementById('cmbMunicipio').selectedIndex].text+'?')){
					  valores_a_mandar="";
				      valores_a_mandar=valores_a_mandar+"accion="+arg;
				      valores_a_mandar=valores_a_mandar+"&codigo="+document.getElementById('cmbMunicipio').options[document.getElementById('cmbMunicipio').selectedIndex].value;
                      valores_a_mandar=valores_a_mandar+"&codDepto="+document.getElementById('cmbDepartamento').options[document.getElementById('cmbDepartamento').selectedIndex].value;
                      valores_a_mandar=valores_a_mandar+"&opcion=Municipio";
				      ajaxEliminar();
					}
				}else{
					alert('Favor seleccionar un Municipio, para ser Eliminado.');
				}
		   }else if(document.getElementById('hddOpcion').value=='Barrio'){
			    if(document.getElementById('cmbBarrios').options[document.getElementById('cmbBarrios').selectedIndex].value!='00'){
					if(confirm('Realmente desea eliminar el Barrio: '+document.getElementById('cmbBarrios').options[document.getElementById('cmbBarrios').selectedIndex].text+'?')){
				      valores_a_mandar="";
				      valores_a_mandar=valores_a_mandar+"accion="+arg;
				      valores_a_mandar=valores_a_mandar+"&codigo="+document.getElementById('cmbBarrios').options[document.getElementById('cmbBarrios').selectedIndex].value;
                      valores_a_mandar=valores_a_mandar+"&codMunicipio="+document.getElementById('cmbMunicipio').options[document.getElementById('cmbMunicipio').selectedIndex].value;
                      valores_a_mandar=valores_a_mandar+"&opcion=Barrio";
				      ajaxEliminar();
				    }
				}else{
					alert('Favor seleccionar un Barrio, para ser Eliminado.');
				} 
		   } 	
		break;
	}	
}
function respuestaEliminarJuan(arg, xmlraiz){ 

		switch(arg){
		 case 'ubicaciones':
	        alert("!Ubicaciones  eliminado Exitosamente");
		  break;  	
		  case 'Centros de salud':
	        alert("!Centros de salud eliminado Exitosamente");
		  break;  	
          case 'diagnosticos':
	        alert("Diagnosticos eliminado Exitosamente !");
		  break;  			  		  
		  case 'gruposEspeciales':
	        alert("Grupo Especial eliminado Exitosamente !");
		  break;  			  		  
		  case 'tarifarios':
	        alert("Tarifario eliminado Exitosamente !");
		  break;  			  		  
		  case 'finalidadDeConsulta':
	        alert("Finalidad de La Consulta eliminada Exitosamente !");
		  break;  			  		  
		  case 'cargos':
	        alert("Cargo eliminado Exitosamente !");
		  break;  			  
		}		
}

//lugar y fecha nacimiento
function actualizarCombosUbicacionesMupios(codPais, codDepto, nombreComboDepto, codMpio, nombreComboMpio){
//   alert("1pilas el ajax.. codDepto= "+codDepto +" codMunicip= "+codMpio );
   cargarDepartamentosIns(codDepto, nombreComboDepto, codPais,'/clinica/paginas/accionesXml/cargarDeptos_xml.jsp');
  // alert("2pilas el ajax.. codDepto= "+codDepto +" codMunicip= "+codMpio );
   cargarCiudadesIns(codMpio, nombreComboMpio, codDepto,'/clinica/paginas/accionesXml/cargarMunicipios_xml.jsp'  );
}



//lugar y fecha recidencia
function actualizarCombosUbicacionesBarrios(codPais, codDepto, nombreComboDepto, codMpio, nombreComboMpio, codBarrio , nombreComboBarrio){
   cargarDepartamentosIns(codDepto, nombreComboDepto, codPais,'/clinica/paginas/accionesXml/cargarDeptos_xml.jsp');
   cargarCiudadesIns(codMpio, nombreComboMpio, codDepto,'/clinica/paginas/accionesXml/cargarMunicipios_xml.jsp'  );
   cargarBarriosIns(codMpio,codBarrio, nombreComboBarrio, '/clinica/paginas/accionesXml/cargarBarrios_xml.jsp' );
}
function activarPlanDeaccion(elemento){ 

 if(elemento=='rbtImpacto'){ 
    quitarDatosListasST2('listPlanAccionSeguimiento');
    $('#drag'+ventanaActual.num).find('#divEditarPlanDeAccion').show();
//    $('#drag'+ventanaActual.num).find('#divContenido').css('height','720px');	
    listadoTabla('listPlanAccion');
	$('#drag'+ventanaActual.num).find('#divDatosPlanAccion').show();
    $('#drag'+ventanaActual.num).find('#cmbproceso').attr('disabled','disabled');	
    $('#drag'+ventanaActual.num).find('#cmbNoConformidades').attr('disabled','disabled');	
    $('#drag'+ventanaActual.num).find('#cmbUbicacionArea').attr('disabled','disabled');		
    $('#drag'+ventanaActual.num).find('#cmbResponsaDestinoEvento').attr('disabled','disabled');		
    $('#drag'+ventanaActual.num).find('#btnEnvio').attr('disabled','disabled');	
	
	
 }
 else{
    $('#drag'+ventanaActual.num).find('#cmbproceso').attr('disabled','');	
    $('#drag'+ventanaActual.num).find('#cmbNoConformidades').attr('disabled','');	
    $('#drag'+ventanaActual.num).find('#cmbResponsaDestinoEvento').attr('disabled','');	
    $('#drag'+ventanaActual.num).find('#btnEnvio').attr('disabled','');				
	 
   if( $('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html()=='' ){
	  $('#drag'+ventanaActual.num).find('#divEditarPlanDeAccion').hide();
	//  $('#drag'+ventanaActual.num).find('#divContenido').css('height','700px');
	  mostrar('divPlanAccion01');
	  mostrar('divBotonGuardNCAsociadas');
   }
   else{ alert('No puede cambiar porque existe un Pl�n de acci�n asociado');
	   $('#drag'+ventanaActual.num).find('#rbtImpacto').attr('checked','checked');
   }	
 }
}


function activarPlanDeaccionOportMejora(elemento){ 

 if(elemento=='rbtImpacto'){ 
    $('#drag'+ventanaActual.num).find('#divEditarPlanDeAccion').show();
	$('#drag'+ventanaActual.num).find('#divDatosPlanAccion').show();
    $('#drag'+ventanaActual.num).find('#cmbproceso').attr('disabled','disabled');	
    $('#drag'+ventanaActual.num).find('#cmbClaseEA').attr('disabled','disabled');	
    $('#drag'+ventanaActual.num).find('#cmbEventoProceso').attr('disabled','disabled');		
    $('#drag'+ventanaActual.num).find('#cmbUbicacionArea').attr('disabled','disabled');		
    $('#drag'+ventanaActual.num).find('#btnEnvio').attr('disabled','disabled');	
	
	
 }
 else{
    $('#drag'+ventanaActual.num).find('#cmbproceso').attr('disabled','');	
    $('#drag'+ventanaActual.num).find('#cmbClaseEA').attr('disabled','');	
    $('#drag'+ventanaActual.num).find('#cmbEventoProceso').attr('disabled','');	
    $('#drag'+ventanaActual.num).find('#cmbUbicacionArea').attr('disabled','');		
    $('#drag'+ventanaActual.num).find('#btnEnvio').attr('disabled','');				
	 

    $('#drag'+ventanaActual.num).find('#divEditarPlanDeAccion').hide();

//	  mostrar('divPlanAccion01');
	//  mostrar('divBotonGuardNCAsociadas');

 }
}




function limpiarFiltros(){ 

  $('#drag'+ventanaActual.num).find('#chk_solo_mis').attr('checked','');
  $("#cmbBusproceso option[value='00']").attr('selected', 'selected');  
  $("#cmbBusNoConformidades option[value='00']").attr('selected', 'selected');  
  $("#cmbBusUbicacionArea option[value='00']").attr('selected', 'selected'); 
  $('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val('');
  $('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val('');  

}	

function calcularTotalPriorizacion(){
  
	  if($('#drag'+ventanaActual.num).find('#cmbRiesgo').val()==00){
		  $('#drag'+ventanaActual.num).find('#lblTotPrioriza').html(
			$('#drag'+ventanaActual.num).find('#cmbCosto').val()*$('#drag'+ventanaActual.num).find('#cmbVolumen').val()														
		  )	  
	  }
	  else if($('#drag'+ventanaActual.num).find('#cmbCosto').val()==00){
		  $('#drag'+ventanaActual.num).find('#lblTotPrioriza').html(
			$('#drag'+ventanaActual.num).find('#cmbRiesgo').val()*$('#drag'+ventanaActual.num).find('#cmbVolumen').val()														
		  )	  
	  }
	  else if($('#drag'+ventanaActual.num).find('#cmbVolumen').val()==00){
		  $('#drag'+ventanaActual.num).find('#lblTotPrioriza').html(
			$('#drag'+ventanaActual.num).find('#cmbRiesgo').val()*$('#drag'+ventanaActual.num).find('#cmbCosto').val()														
		  )	  
	  }	
	  else{	  
		  $('#drag'+ventanaActual.num).find('#lblTotPrioriza').html(
			$('#drag'+ventanaActual.num).find('#cmbRiesgo').val()*$('#drag'+ventanaActual.num).find('#cmbCosto').val()*$('#drag'+ventanaActual.num).find('#cmbVolumen').val()														
		  )
	  }	  

}
function calcularTotalPriorizacion(){
  
	  if($('#drag'+ventanaActual.num).find('#cmbRiesgoOM').val()==00){
		  $('#drag'+ventanaActual.num).find('#lblTotPriorizaOM').html(
			$('#drag'+ventanaActual.num).find('#cmbCostoOM').val()*$('#drag'+ventanaActual.num).find('#cmbVolumenOM').val()														
		  )	  
	  }
	  else if($('#drag'+ventanaActual.num).find('#cmbCostoOM').val()==00){
		  $('#drag'+ventanaActual.num).find('#lblTotPriorizaOM').html(
			$('#drag'+ventanaActual.num).find('#cmbRiesgoOM').val()*$('#drag'+ventanaActual.num).find('#cmbVolumenOM').val()														
		  )	  
	  }
	  else if($('#drag'+ventanaActual.num).find('#cmbVolumenOM').val()==00){
		  $('#drag'+ventanaActual.num).find('#lblTotPriorizaOM').html(
			$('#drag'+ventanaActual.num).find('#cmbRiesgoOM').val()*$('#drag'+ventanaActual.num).find('#cmbCostoOM').val()														
		  )	  
	  }	
	  else{	  
		  $('#drag'+ventanaActual.num).find('#lblTotPriorizaOM').html(
			$('#drag'+ventanaActual.num).find('#cmbRiesgoOM').val()*$('#drag'+ventanaActual.num).find('#cmbCostoOM').val()*$('#drag'+ventanaActual.num).find('#cmbVolumenOM').val()														
		  )
	  }	  

}

/** funciones para la accion de busqueda*/
function buscarJuan(arg, pag){   //alert('buscarJuan(arg, pag)= '+arg +'-'+ pag);

	pagina=pag;
    ancho= ($('#drag'+ventanaActual.num).find("#divContenido").width())*95/100;

	 switch (arg){	 
		 		
		 case 'reportarNoConformidad': 
		     limpiarDivEditarJuan(arg);
		     $('#drag'+ventanaActual.num).find('#divContenido').css('height','400px');	
		 	 valores_a_mandar=pag+"?accion="+ventanaActual.opc;
			 valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbBusproceso').val();			 
			 valores_a_mandar=valores_a_mandar+"&idNoConformidades="+$('#drag'+ventanaActual.num).find('#cmbBusNoConformidades').val();
			 valores_a_mandar=valores_a_mandar+"&AreaEvento="+$('#drag'+ventanaActual.num).find('#cmbBusUbicacionArea').val();
             valores_a_mandar=valores_a_mandar+"&txtBusFechaEventoNoConfDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
             valores_a_mandar=valores_a_mandar+"&txtBusFechaEventoNoConfHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();
             valores_a_mandar=valores_a_mandar+"&txtBusIdEventoNoConf="+$('#drag'+ventanaActual.num).find('#txtBusIdEventoNoConf').val();             
			 valores_a_mandar=valores_a_mandar+"&solo_mis="+document.getElementById('chk_solo_mis').checked;


			 $('#drag'+ventanaActual.num).find("#list").jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
		     colNames:[ 'idProceso', 'Descripcion del proceso','IdEvento','idElaboro','Descripci�n evento','designacion','idResponsable' , 'Id no conformidad',
					    'Descripcion no conformidad','idUbicacion','Area', 'Fecha evento','horaEvento','minutEvento','Estado', 
			            'IsoIdTipoISOEvento','IsoDescrTipoISOEvento' ,'IsoFechaIniPlanAccion','posiTratam','IdISORef','IdISORefDoc','Tipo','analisisDoc','metasDoc','Asociado','idEventoPadreAsocia'],
             colModel :[  
               {name:'idProceso', index:'idProceso',  align:'left',hidden:true},  
               {name:'DescriProceso', index:'DescriProceso',  width:((ancho*17)/100), align:'left' },  	   
               {name:'IdEvento', index:'IdEvento',  sorttype:"int", align:'left',hidden:true},  
			   {name:'idElaboro', index:'idElaboro',  align:'left',hidden:true},  
               {name:'descripEvento', index:'descripEvento',  width:((ancho*20)/100),sorttype:"int",  align:'left'},  	
               {name:'designacion', index:'designacion',  width:((ancho*29)/100),sorttype:"int",  align:'left',hidden:true},  	
               {name:'idResponsable', index:'idResponsable',  width:((ancho*24)/100),sorttype:"int",  align:'left',hidden:true}, 			   
			   {name:'idNoConformidad', index:'idNoConformidad', align:'center',hidden:true},                  
			   {name:'descripNoConformidad', index:'descripNoConformidad',  width:((ancho*35)/100), align:'center',hidden:true},     
			   {name:'idUbicacion', index:'idUbicacion',  align:'left',hidden:true},     
			   {name:'descUbicacion', index:'descUbicacion',  width:((ancho*11)/100), align:'left'},     
			   {name:'fechaEvento', index:'fechaEvento',  width:((ancho*6)/100), align:'left'},   
			   {name:'horaEvento', index:'horaEvento',   align:'center',hidden:true},     
			   {name:'minutEvento', index:'minutEvento',  align:'center',hidden:true},     			   
			   {name:'Estado', index:'Estado',  width:((ancho*5)/100), align:'left'},     

               {name:'IsoIdTipoISOEvento', index:'IsoIdTipoISOEvento',hidden:true}, 
			   {name:'IsoDescrTipoISOEvento', index:'IsoDescrTipoISOEvento',  hidden:true}, 
			   {name:'IsoFechaIniPlanAccion', index:'IsoFechaIniPlanAccion', hidden:true}, 	
			   {name:'posiTratam', index:'posiTratam',hidden:true},     
			   {name:'IdISORef', index:'IdISORef',hidden:true},  
			   {name:'IdISORefDoc', index:'IdISORefDoc', hidden:true},  
		       {name:'tipo', index:'tipo',  width:((ancho*5)/100), align:'left'},  
			   {name:'analisisDoc', index:'analisisDoc',hidden:true},  
			   {name:'metasDoc', index:'metasDoc',hidden:true},  
  			   {name:'Asociado', index:'Asociado', width:((ancho*3)/100)},
   			   {name:'idEventoPadreAsocia', index:'idEventoPadreAsocia',hidden:true},
			   
			   ], 			 
             pager: jQuery('#pager'), 
             rowNum:100, 
             rowList:[20,50,100], 
             sortname: '1', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 600, 
			 width: ancho, 
			 
			 ondblClickRow: function( rowid){
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num).find('#list').getRowData(rowid);   //trae los datos de esta fila con este idrow  

             if(datosDelRegistro.Estado=='Reportada' && datosDelRegistro.tipo=='No Impacto'){// parche se deberia asociar eventos relacionados y sobreescribir para que automaticamente pase de reportada a asignada y tenga un responsable
				  $('#drag'+ventanaActual.num).find('#divContenido').css('height','600px');			   
				  $('#drag'+ventanaActual.num).find('#divBuscar').hide();
				  $('#drag'+ventanaActual.num).find('#divEditar').show();	
				  $('#drag'+ventanaActual.num).find('#divEditar').css('height','600px');
				  $('#drag'+ventanaActual.num).find('#divEditarFases').hide();
	
				  $('#drag'+ventanaActual.num).find('#txtFechaEvento').val($.trim(datosDelRegistro.fechElaboracion));			     
				  $("#cmbproceso option[value='"+$.trim(datosDelRegistro.idProceso)+"']").attr('selected', 'selected');
				  $("#cmbNoConformidades option[value='"+$.trim(datosDelRegistro.idNoConformidad)+"']").attr('selected', 'selected');			  
				  $("#cmbUbicacionArea option[value='"+$.trim(datosDelRegistro.idUbicacion)+"']").attr('selected', 'selected');
				  $('#drag'+ventanaActual.num).find('#lblIdEvento').html($.trim(datosDelRegistro.IdEvento)); 
				  $('#drag'+ventanaActual.num).find('#lblElaboro').val(''); 
				  $('#drag'+ventanaActual.num).find('#lblElaboro').val($.trim(datosDelRegistro.idElaboro)); 
				  
				  $('#drag'+ventanaActual.num).find('#txtDescripEvento').val($.trim(datosDelRegistro.descripEvento )); 
	//			  $('#drag'+ventanaActual.num).find('#lblFechaEvento').html($.trim(datosDelRegistro.fechaEvento)); 
				  $("#cmbHoraEvent option[value='"+$.trim(datosDelRegistro.horaEvento)+"']").attr('selected', 'selected');			  
				  $('#drag'+ventanaActual.num).find('#txtMinutEvent').val($.trim(datosDelRegistro.minutEvento));			     			  
	
				  $('#drag'+ventanaActual.num).find('#txtFechaEvento').val($.trim(datosDelRegistro.fechaEvento));	
				  $('#drag'+ventanaActual.num).find('#txtEstadoEvent').val($.trim(datosDelRegistro.Estado));
				  $('#drag'+ventanaActual.num).find('#txtDescripEventoPosTrata').val($.trim(datosDelRegistro.posiTratam));
				  $('#drag'+ventanaActual.num).find('#lblIdISORef').html($.trim(datosDelRegistro.IdISORef));
				  
				  $('#drag'+ventanaActual.num).find('#lblConcepNoConforCalidad').html('calidad no ha generado'); 
				  $('#drag'+ventanaActual.num).find('#lblFechaNoConforCalidadAsign').html('calidad no ha generado'); 			  
              } 
			  else{
					  $('#drag'+ventanaActual.num).find('#divContenido').css('height','600px');			   
					  $('#drag'+ventanaActual.num).find('#divBuscar').hide();
					  $('#drag'+ventanaActual.num).find('#divEditar').show();	
					  $('#drag'+ventanaActual.num).find('#divEditar').css('height','600px');
					  $('#drag'+ventanaActual.num).find('#divEditarFases').show();
		
					  $('#drag'+ventanaActual.num).find('#txtFechaEvento').val($.trim(datosDelRegistro.fechElaboracion));			     
					  $("#cmbproceso option[value='"+$.trim(datosDelRegistro.idProceso)+"']").attr('selected', 'selected');
					  $("#cmbNoConformidades option[value='"+$.trim(datosDelRegistro.idNoConformidad)+"']").attr('selected', 'selected');			  
					  $("#cmbUbicacionArea option[value='"+$.trim(datosDelRegistro.idUbicacion)+"']").attr('selected', 'selected');
					  $('#drag'+ventanaActual.num).find('#lblIdEvento').html($.trim(datosDelRegistro.IdEvento)); 
					  $('#drag'+ventanaActual.num).find('#txtDescripEvento').val($.trim(datosDelRegistro.descripEvento ));
					  $('#drag'+ventanaActual.num).find('#txtDescripEvento').attr('disabled','disabled');	//se convierte en solo lectura
	  			      $('#drag'+ventanaActual.num).find('#txtMinutEvent').attr('disabled','disabled');
			          $('#drag'+ventanaActual.num).find('#txtFechaEvento').attr('disabled','disabled');
			          $('#drag'+ventanaActual.num).find('#cmbHoraEvent').attr('disabled','disabled');				

					  
					  $('#drag'+ventanaActual.num).find('#txtConcepNoConforCalidad').val($.trim(datosDelRegistro.designacion)); 	
					  $("#cmbResponsaDestinoEvento option[value='"+$.trim(datosDelRegistro.idResponsable)+"']").attr('selected', 'selected');
		
					  $("#cmbHoraEvent option[value='"+$.trim(datosDelRegistro.horaEvento)+"']").attr('selected', 'selected');			  
					  $('#drag'+ventanaActual.num).find('#txtMinutEvent').val($.trim(datosDelRegistro.minutEvento));			     			  
		
					  $('#drag'+ventanaActual.num).find('#txtFechaEvento').val($.trim(datosDelRegistro.fechaEvento));
					  $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html($.trim(datosDelRegistro.Estado));
				      $('#drag'+ventanaActual.num).find('#txtDescripEventoPosTrata').val($.trim(datosDelRegistro.posiTratam));
					  $('#drag'+ventanaActual.num).find('#txtDescripEventoPosTrata').attr('disabled','disabled');	//se convierte en solo lectura
					  $('#drag'+ventanaActual.num).find('#lblIdISORef').html($.trim(datosDelRegistro.IdISORef));
					  
		
					  
					  if(datosDelRegistro.tipo=='Impacto') {
						 $('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html($.trim(datosDelRegistro.IdISORefDoc));
						 $('#drag'+ventanaActual.num).find('#rbtImpacto').attr('checked','checked');
						 $("#cmbTipoPlanAccion option[value='"+$.trim(datosDelRegistro.IsoIdTipoISOEvento)+"']").attr('selected', 'selected');
						 $('#drag'+ventanaActual.num).find('#txtFechaIniPlanAccion').val($.trim(datosDelRegistro.IsoFechaIniPlanAccion));
						 $('#drag'+ventanaActual.num).find('#txtAnalisisCausas').val($.trim(datosDelRegistro.analisisDoc));
						 $('#drag'+ventanaActual.num).find('#txtMetaPlanAccion').val($.trim(datosDelRegistro.metasDoc));
						 $('#drag'+ventanaActual.num).find('#lblEventoPadreAsocia').html($.trim(datosDelRegistro.idEventoPadreAsocia));				 
						 //seguimiento
						 $('#drag'+ventanaActual.num).find('#lblMetaPlanAccion').html($.trim(datosDelRegistro.metasDoc));
						 $('#drag'+ventanaActual.num).find('#lblFechaIniPlanAccion').html($.trim(datosDelRegistro.IsoFechaIniPlanAccion));
						 activarPlanDeaccion('rbtImpacto'); 		
						 setTimeout("buscarJuan('listAsociarEventos','/clinica/paginas/accionesXml/buscar_xml.jsp')", 3000);//ubo problemas con otro list el de eventos relacion list						
					  }	 
					  else{
						 $('#drag'+ventanaActual.num).find('#rbtNoImpacto').attr('checked','checked');
						 activarPlanDeaccion('rbtNoImpacto'); 
					     $('#drag'+ventanaActual.num).find('#rbtImpacto').attr('disabled','disabled');	//se convierte en solo lectura  porque usuario no puede cambiar 
						 $('#drag'+ventanaActual.num).find('#rbtNoImpacto').attr('disabled','disabled');	//se convierte en solo lectura      
						 
					  }	  				  
			  }     
             },
			   
			 //height: 210, 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             //caption: 'Resultados' /*en el css se choca con master.css*/
         });  
	    $('#drag'+ventanaActual.num).find("#list").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;	 
		case 'administrarNoConformidad': 
		      limpiarDivEditarJuan(arg); 
		    // $('#drag'+ventanaActual.num).find('#divContenido').css('height','500px');	
		 	 valores_a_mandar=pag+"?accion="+ventanaActual.opc;
			 valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbBusproceso').val();			 
			 valores_a_mandar=valores_a_mandar+"&idNoConformidades="+$('#drag'+ventanaActual.num).find('#cmbBusNoConformidades').val();
			 valores_a_mandar=valores_a_mandar+"&AreaEvento="+$('#drag'+ventanaActual.num).find('#cmbBusUbicacionArea').val();
             valores_a_mandar=valores_a_mandar+"&txtBusFechaEventoNoConfDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
             valores_a_mandar=valores_a_mandar+"&txtBusFechaEventoNoConfHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();
             valores_a_mandar=valores_a_mandar+"&txtBusIdEventoNoConf="+$('#drag'+ventanaActual.num).find('#txtBusIdEventoNoConf').val();                          
			 valores_a_mandar=valores_a_mandar+"&EstadoEvento="+$('#drag'+ventanaActual.num).find('#cmbBusEstadoEvento').val();			 
			 valores_a_mandar=valores_a_mandar+"&solo_plan="+document.getElementById('chk_solo_plan').checked;			 
			// alert(valores_a_mandar);
			 $('#drag'+ventanaActual.num).find("#list").jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:[ 'contador','idProceso', 'Descripcion del proceso','IdEvento','idElaboro','Descripci�n evento','designacion','idResponsable' , 'Id no conformidad',
					    'Descripcion no conformidad','idUbicacion','Area', 'Fecha evento','horaEvento','minutEvento','Estado', 
			            'IsoIdTipoISOEvento','IsoDescrTipoISOEvento' ,'IsoFechaIniPlanAccion','posiTratam','IdISORef','IdISORefDoc','Tipo','analisisDoc','metasDoc','Asociado','idEventoPadreAsocia','modificable','fechaElaboro'],
			 
             colModel :[  
               {name:'contador', index:'contador',  align:'left', width:((ancho*2)/100)},  						
               {name:'idProceso', index:'idProceso',  align:'left',hidden:true},  
               {name:'DescriProceso', index:'DescriProceso',  width:((ancho*11)/100), align:'left' },  	   
               {name:'IdEvento', index:'IdEvento',  sorttype:"int", align:'left',hidden:true},  
			   {name:'idElaboro', index:'idElaboro',  align:'left',hidden:true},  
               {name:'descripEvento', index:'descripEvento',  width:((ancho*31)/100),sorttype:"int",  align:'left'},  	
               {name:'designacion', index:'designacion',  width:((ancho*29)/100),sorttype:"int",  align:'left',hidden:true},  	
               {name:'idResponsable', index:'idResponsable',  width:((ancho*24)/100),sorttype:"int",  align:'left',hidden:true}, 			   
			   {name:'idNoConformidad', index:'idNoConformidad', align:'center',hidden:true},                  
			   {name:'descripNoConformidad', index:'descripNoConformidad',  width:((ancho*35)/100), align:'center',hidden:true},     
			   {name:'idUbicacion', index:'idUbicacion',  align:'left',hidden:true},     
			   {name:'descUbicacion', index:'descUbicacion',  width:((ancho*6)/100), align:'left'},     
			   {name:'fechaEvento', index:'fechaEvento',  width:((ancho*5)/100), align:'left'},   
			   {name:'horaEvento', index:'horaEvento',   align:'center',hidden:true},     
			   {name:'minutEvento', index:'minutEvento',  align:'center',hidden:true},     			   
			   {name:'Estado', index:'Estado',  width:((ancho*5)/100), align:'left'},     

               {name:'IsoIdTipoISOEvento', index:'IsoIdTipoISOEvento',hidden:true}, 
			   {name:'IsoDescrTipoISOEvento', index:'IsoDescrTipoISOEvento',  hidden:true}, 
			   {name:'IsoFechaIniPlanAccion', index:'IsoFechaIniPlanAccion', hidden:true}, 	
			   {name:'posiTratam', index:'posiTratam',hidden:true},     
			   {name:'IdISORef', index:'IdISORef',hidden:true},  
			   {name:'IdISORefDoc', index:'IdISORefDoc', hidden:true},  
		       {name:'tipo', index:'tipo',  width:((ancho*5)/100), align:'left'},  
			   {name:'analisisDoc', index:'analisisDoc',hidden:true},  
			   {name:'metasDoc', index:'metasDoc',hidden:true},  
  			   {name:'Asociado', index:'Asociado', width:((ancho*2)/100)},
   			   {name:'idEventoPadreAsocia', index:'idEventoPadreAsocia',hidden:true},
  			   {name:'modificable', index:'modificable', width:((ancho*2)/100)},			   
  			   {name:'fechaElaboro', index:'fechaElaboro', width:((ancho*5)/100)},			   			   
			   
			   ], 
             pager: jQuery('#pager'), 
             rowNum:100, 
             rowList:[20,50,100], 
             sortname: '1', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 550, 
			 width: ancho,
			 
			 ondblClickRow: function( rowid) { 
				
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num).find('#list').getRowData(rowid);  

     	      $('#drag'+ventanaActual.num).find('#divContenido').css('height','720px');		   
              $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();	
              $('#drag'+ventanaActual.num).find('#divEditar').css('height','600px');
			  $('#drag'+ventanaActual.num).find('#divEditarFases').show();

              $('#drag'+ventanaActual.num).find('#txtFechaEvento').val($.trim(datosDelRegistro.fechElaboracion));			     
              $("#cmbproceso option[value='"+$.trim(datosDelRegistro.idProceso)+"']").attr('selected', 'selected');
              $("#cmbNoConformidades option[value='"+$.trim(datosDelRegistro.idNoConformidad)+"']").attr('selected', 'selected');			  
              $("#cmbUbicacionArea option[value='"+$.trim(datosDelRegistro.idUbicacion)+"']").attr('selected', 'selected');
		      $('#drag'+ventanaActual.num).find('#lblIdEvento').html($.trim(datosDelRegistro.IdEvento)); 
			  
		      $('#drag'+ventanaActual.num).find('#lblDescripEvento').html($.trim(datosDelRegistro.descripEvento));
		      $('#drag'+ventanaActual.num).find('#txtConcepNoConforCalidad').val($.trim(datosDelRegistro.designacion)); 	
              $("#cmbResponsaDestinoEvento option[value='"+$.trim(datosDelRegistro.idResponsable)+"']").attr('selected', 'selected');

              $("#cmbHoraEvent option[value='"+$.trim(datosDelRegistro.horaEvento)+"']").attr('selected', 'selected');			  
			  $('#drag'+ventanaActual.num).find('#txtMinutEvent').val($.trim(datosDelRegistro.minutEvento));			     			  

			  $('#drag'+ventanaActual.num).find('#txtFechaEvento').val($.trim(datosDelRegistro.fechaEvento));
 			  $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html($.trim(datosDelRegistro.Estado));
			  $('#drag'+ventanaActual.num).find('#lblDescripEventoPosTrata').html($.trim(datosDelRegistro.posiTratam));
			  $('#drag'+ventanaActual.num).find('#lblIdISORef').html($.trim(datosDelRegistro.IdISORef));
			  

			  
			  if(datosDelRegistro.tipo=='Impacto') {
				 $('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html($.trim(datosDelRegistro.IdISORefDoc));
				 $('#drag'+ventanaActual.num).find('#rbtImpacto').attr('checked','checked');
                 $("#cmbTipoPlanAccion option[value='"+$.trim(datosDelRegistro.IsoIdTipoISOEvento)+"']").attr('selected', 'selected');
  		         $('#drag'+ventanaActual.num).find('#txtFechaIniPlanAccion').val($.trim(datosDelRegistro.IsoFechaIniPlanAccion));
                 $('#drag'+ventanaActual.num).find('#txtAnalisisCausas').val($.trim(datosDelRegistro.analisisDoc));
			     $('#drag'+ventanaActual.num).find('#txtMetaPlanAccion').val($.trim(datosDelRegistro.metasDoc));
				 $('#drag'+ventanaActual.num).find('#lblEventoPadreAsocia').html($.trim(datosDelRegistro.idEventoPadreAsocia));				 
				 //seguimiento
				 $('#drag'+ventanaActual.num).find('#lblMetaPlanAccion').html($.trim(datosDelRegistro.metasDoc));
   		         $('#drag'+ventanaActual.num).find('#lblFechaIniPlanAccion').html($.trim(datosDelRegistro.IsoFechaIniPlanAccion));				 
				 activarPlanDeaccion('rbtImpacto'); 
				 setTimeout("buscarJuan('listAsociarEventos','/clinica/paginas/accionesXml/buscar_xml.jsp')", 3000);//ubo problemas con otro list el de eventos relacion list
				 if(datosDelRegistro.modificable=='0'){
					alert('El plan de acci�n ya debi� iniciarce, o est� asociado a otro evento, por ello no se permite hacer cambios');
					ocultar('divPlanAccion01');
					ocultar('divBotonGuardNCAsociadas');		
				 }	
				 else{

				    if( $('#drag'+ventanaActual.num).find('#cmbResponsaDestinoEvento').val()==$('#drag'+ventanaActual.num).find('#IdUsuario').val() ){
 					   mostrar('divPlanAccion01');
					   mostrar('divBotonGuardNCAsociadas');
//					   $('#drag'+ventanaActual.num).find('#cmbResponsaDestinoEvento')
				    }
					else{ 

					   alert('Usted no es el responsable del Plan de acci�n, por ello no puede modificar');
					   ocultar('divPlanAccion01');
					   ocultar('divBotonGuardNCAsociadas');		
					}
				 }	 				
			  }	 
			  else{
 				 $('#drag'+ventanaActual.num).find('#rbtNoImpacto').attr('checked','checked');
			     activarPlanDeaccion('rbtNoImpacto'); 	 
			  }
              },
			 //height: 210, 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             /*caption: 'Resultados' */
         });  
	    $('#drag'+ventanaActual.num).find("#list").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;	
		case 'administrarNoConformidadAsociar': 
		 	 valores_a_mandar=pag+"?accion=administrarNoConformidadAsociar";
			 valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbBusproceso2').val();			 
			 valores_a_mandar=valores_a_mandar+"&idNoConformidades="+$('#drag'+ventanaActual.num).find('#cmbBusNoConformidades2').val();
			 valores_a_mandar=valores_a_mandar+"&AreaEvento="+$('#drag'+ventanaActual.num).find('#cmbBusUbicacionArea2').val();
             valores_a_mandar=valores_a_mandar+"&txtBusFechaEventoNoConfDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaEventoNoConfDesde2').val();
             valores_a_mandar=valores_a_mandar+"&txtBusFechaEventoNoConfHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaEventoNoConfHasta2').val();
             valores_a_mandar=valores_a_mandar+"&EstadoEvento="+$('#drag'+ventanaActual.num).find('#cmbBusEstadoEvento2').val();			 
			 
			 $('#drag'+ventanaActual.num).find("#listEventosParaAsociar").jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:[ 'idProceso', 'Descripcion del proceso','IdEvento','Descripci�n evento','designacion','idResponsable' , 'Id no conformidad',
					    'Descripcion no conformidad','idUbicacion','Area', 'Fecha evento','Estado'],
             colModel :[  
               {name:'idProceso', index:'idProceso',  hidden:true},  
               {name:'DescriProceso', index:'DescriProceso',  width:((ancho*10)/100), align:'left' }, 
   			   {name:'IdEvento', index:'IdEvento',width:((ancho*11)/100),  sorttype:"int", align:'left'},  		
               {name:'descripEvento', index:'descripEvento',  width:((ancho*29)/100),sorttype:"int",  align:'left'},  	
               {name:'designacion', index:'designacion',  width:((ancho*29)/100),sorttype:"int",  align:'left',hidden:true},  	
               {name:'idResponsable', index:'idResponsable',  width:((ancho*24)/100),sorttype:"int",  align:'left',hidden:true}, 			   
			   {name:'idNoConformidad', index:'idNoConformidad', align:'center',hidden:true},                  
			   {name:'descripNoConformidad', index:'descripNoConformidad',  width:((ancho*45)/100), align:'center',hidden:true},     
			   {name:'idUbicacion', index:'idUbicacion',  align:'left',hidden:true},     
			   {name:'descUbicacion', index:'descUbicacion',  width:((ancho*11)/100), align:'left'},     
			   {name:'fechaEvento', index:'fechaEvento',  width:((ancho*6)/100), align:'left'},   
			   {name:'Estado', index:'Estado',  width:((ancho*5)/100), align:'left'},     

			   ],
            // pager: jQuery('#pager22'), 
             rowNum:100, 
             rowList:[20,50,100], 
             sortname: '1', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 100, 
			 width: ancho-10,
			 
			 ondblClickRow: function( rowid) {				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num).find('#listEventosParaAsociar').getRowData(rowid);   
                /*pasar de los eventos buscados a los asociados*/
				ban=0;				
				var ids = jQuery("#listAsociarEventos").getDataIDs();
				  for(var i=0;i<ids.length;i++){ 
					  var c = ids[i];
					  var datosDelRegistroDes = jQuery("#listAsociarEventos").getRowData(c);  
						 if($.trim(datosDelRegistroDes.IdEvento) == datosDelRegistro.IdEvento)	
						  ban=1;
				  }
				 if(ban==0){
					 var ids = jQuery("#listAsociarEventos").getDataIDs();
					 cant = ids.length;		 
					 proximo=cant+1;  	
			
					 var datarow = {		 	 
					  tipo:'0',
					  idProceso:datosDelRegistro.idProceso, 
					  DescriProceso:datosDelRegistro.DescriProceso, 
					  IdEvento:datosDelRegistro.IdEvento,
					  descripEvento:datosDelRegistro.descripEvento, 
					  designacion:datosDelRegistro.designacion, 
					  idResponsable:datosDelRegistro.idResponsable, 
					  idNoConformidad:datosDelRegistro.idNoConformidad, 
					  descripNoConformidad:datosDelRegistro.descripNoConformidad, 
					  idUbicacion:datosDelRegistro.idUbicacion, 
					  descUbicacion:datosDelRegistro.descUbicacion, 
					  fechaEvento:datosDelRegistro.fechaEvento, 
					  Estado:datosDelRegistro.Estado, 
					 };
					 var su=jQuery('#listAsociarEventos').addRowData(proximo,datarow);
				 }
				 else  alert('Elemento ya se encuentra en lista');
              },
			   /*****************************fin****************/
			  
			 //height: 210, 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             //caption: 'Resultados' 
         });  
	    $('#drag'+ventanaActual.num).find("#listEventosParaAsociar").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;	
		case 'listAsociarEventos': 	
        valores_a_mandar=pag+"?accion=listAsociarEventos";
		
		if($('#drag'+ventanaActual.num).find('#lblEventoPadreAsocia').html()=='-')
		     valores_a_mandar=valores_a_mandar+"&sidx=1&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEvento').html();
		else valores_a_mandar=valores_a_mandar+"&sidx=1&IdEvento="+$('#drag'+ventanaActual.num).find('#lblEventoPadreAsocia').html();

		$('#drag'+ventanaActual.num).find('#listAsociarEventos').jqGrid({  
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['tipo', 'idProceso', 'Descripcion del proceso','Id Evento','Descripci�n evento','designacion','idResponsable' , 'Id no conformidad',
					    'Descripcion no conformidad','idUbicacion','Area', 'Fecha evento','Estado'],
             colModel :[  
			   {name:'tipo',index:'tipo',hidden:true},		
               {name:'idProceso', index:'idProceso',  align:'left',hidden:true},  
               {name:'DescriProceso', index:'DescriProceso',  width:((ancho*10)/100), align:'left' }, 
   			   {name:'IdEvento', index:'IdEvento',width:((ancho*11)/100),  sorttype:"int", align:'left'},  		
               {name:'descripEvento', index:'descripEvento',  width:((ancho*29)/100),sorttype:"int",  align:'left'},  	
               {name:'designacion', index:'designacion',  width:((ancho*29)/100),sorttype:"int",  align:'left',hidden:true},  	
               {name:'idResponsable', index:'idResponsable',  width:((ancho*24)/100),sorttype:"int",  align:'left',hidden:true}, 			   
			   {name:'idNoConformidad', index:'idNoConformidad', align:'center',hidden:true},                  
			   {name:'descripNoConformidad', index:'descripNoConformidad',  width:((ancho*45)/100), align:'center',hidden:true},     
			   {name:'idUbicacion', index:'idUbicacion',  align:'left',hidden:true},     
			   {name:'descUbicacion', index:'descUbicacion',  width:((ancho*11)/100), align:'left'},     
			   {name:'fechaEvento', index:'fechaEvento',  width:((ancho*6)/100), align:'left'},   
			   {name:'Estado', index:'Estado',  width:((ancho*5)/100), align:'left'},     

			   ],
            // pager: jQuery('#pager2'), 
             rowNum:100, 
             rowList:[20,50,100], 
             sortname: '1', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 180, 
			 width: ancho-10,
			 
			 ondblClickRow: function( rowid) {
			 jQuery('#listAsociarEventos').delRowData(rowid);	//elimina el registro seleccionado
             },
			 //height: 210, 
            // imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             //caption: 'Resultados' 
         });  
	    $('#drag'+ventanaActual.num).find("#listAsociarEventos").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        	   
		break;	
		case 'listPersonasPlanAccion': 	
			valores_a_mandar=pag+"?accion=listPersonasPlanAccion";		
			valores_a_mandar=valores_a_mandar+"&sidx=1&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html();
			$('#drag'+ventanaActual.num).find('#listPersonasPlanAccion').jqGrid({  
	
				 url:valores_a_mandar, 	
				 datatype: 'xml', 
				 mtype: 'GET', 
				 colNames:['tipo', 'Id Persona', 'Nombre Persona'],
				 colModel :[  
				   {name:'tipo',index:'tipo',hidden:true},		
				   {name:'IdPersona', index:'IdPersona',  align:'center'},  
				   {name:'NombrePersona', index:'NombrePersona',  width:((ancho*70)/100), align:'left' }, 
				   ],
				 pager: jQuery('#pagerlistPersonasPlanAccion'), 
				 rowNum:100, 
				 rowList:[20,50,100], 
				 sortname: '1', 
				 sortorder: "desc", 
				 viewrecords: true, 
				 hidegrid: false, 
				 height: 220, 
				 width: ancho-10,
				 
				 ondblClickRow: function( rowid) {					 
				 var datosDelRegistro = jQuery('#drag'+ventanaActual.num).find('#listPersonasPlanAccion').getRowData(rowid);
	
				  $('#drag'+ventanaActual.num).find('#txtFechaEvento').val($.trim(datosDelRegistro.IdPersona));			     
				  $("#cmbproceso option[value='"+$.trim(datosDelRegistro.NombrePersona)+"']").attr('selected', 'selected');
				  },
				 //height: 210, 
				 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
				 //caption: 'Resultados' 
			 });  
			   if( $('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html()!="")
			     $('#drag'+ventanaActual.num).find("#listPersonasPlanAccion").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
				 
		break;	
		case 'reportarEventoAdverso': 

			  altoLis= $('#drag'+ventanaActual.num).find("#divListadoEventosA").height()-70;
		    // $('#drag'+ventanaActual.num).find('#divContenido').css('height','500px');	
		 	 valores_a_mandar=pag+"?accion=administrarEventoAdverso";			 
 			 var datoscadena=$('#drag'+ventanaActual.num).find('#txtBusIdPaciente2').val().split('-');			 
			 valores_a_mandar=valores_a_mandar+"&idPaciente="+datoscadena[0];
			 valores_a_mandar=valores_a_mandar+"&IdUnidadPertenece=00";	
			 valores_a_mandar=valores_a_mandar+"&idEvento="+$('#drag'+ventanaActual.num).find('#txtBusIdEvento').val();			 
			 valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbBusproceso').val();	
			 valores_a_mandar=valores_a_mandar+"&idClase="+$('#drag'+ventanaActual.num).find('#cmbBusClaseEA').val();	
			 valores_a_mandar=valores_a_mandar+"&idEventoProceso="+$('#drag'+ventanaActual.num).find('#cmbBusEventoProceso').val();
			 valores_a_mandar=valores_a_mandar+"&AreaEvento="+$('#drag'+ventanaActual.num).find('#cmbBusUbicacionArea').val();
             valores_a_mandar=valores_a_mandar+"&FechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
             valores_a_mandar=valores_a_mandar+"&FechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();
             valores_a_mandar=valores_a_mandar+"&txtBusIdEventoNoConf="+$('#drag'+ventanaActual.num).find('#txtBusIdEventoNoConf').val();                          
			 valores_a_mandar=valores_a_mandar+"&EstadoEvento="+$('#drag'+ventanaActual.num).find('#cmbBusEstadoEvento').val();			 

			 $('#drag'+ventanaActual.num).find("#listadoEventoA").jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:[ 'contador','idEvento','Descripcion Evento','idPaciente','Nombre Paciente','IdUnidadPertenece','Unidad Pertenece','idProceso',
					   'Descrici�n Proceso','Clase','Desc clase','tratamiento','idEventoProceso','Eventos del Proceso','idArea','nom Area','fecha Evento',
						'idElaboro','nomElaboro','idEstado','desEstado', 
						'IdElavoroGestion','NomElavoroGestion','IdBarreraYDefensa','IdAccionInsegura','IdAmbito','IdPrevenible','IdOrganizCultura','tipo','Conclusiones'],
			 colModel :[  
               {name:'contador', index:'contador',align:'left', width:((ancho*2)/100)},  
               {name:'idEvento', index:'idEvento',sorttype:"int", align:'left',hidden:true},  
               {name:'descEvento', index:'descEvento',width:((ancho*31)/100),sorttype:"int", align:'left'},  				   
               {name:'idPaciente', index:'idPaciente', align:'left',hidden:true },  	
               {name:'nomPaciente', index:'nomPaciente',  width:((ancho*15)/100), align:'left' },  	
               {name:'IdUnidadPertenece', index:'IdUnidadPertenece', align:'left',hidden:true },  	
               {name:'nomUnidadPertenece', index:'nomUnidadPertenece',  width:((ancho*10)/100), align:'left' },  	   			   			   
               {name:'idProceso', index:'idProceso',  align:'left',hidden:true},  
               {name:'descriProceso', index:'descriProceso',  width:((ancho*10)/100), align:'left' }, 
               {name:'idClase', index:'idClase',  width:((ancho*20)/100), align:'left',hidden:true }, 			   
               {name:'descClase', index:'descClase',  width:((ancho*10)/100), align:'left' }, 			   			   
			   {name:'tratamiento', index:'tratamiento',sorttype:"int",  align:'left',hidden:true},  				   
               {name:'idEventoProceso', index:'idEventoProceso',  align:'left',hidden:true},  
               {name:'descEventoProceso', index:'descEventoProceso',  width:((ancho*15)/100), align:'left' }, 
               {name:'idArea', index:'idArea',align:'left',hidden:true }, 			   
               {name:'nomArea', index:'nomArea',  width:((ancho*5)/100), align:'left' }, 
			   {name:'fechaEvento', index:'fechaEvento', width:((ancho*7)/100), align:'left'},   
			   {name:'idElaboro', index:'idElaboro',  align:'left',hidden:true},
			   {name:'nomElaboro', index:'nomElaboro',  align:'left',hidden:true},			   
			   {name:'idEstado', index:'idEstado',hidden:true},  
			   {name:'desEstado', index:'desEstado',  width:((ancho*7)/100), align:'left'}, 
			   {name:'IdElavoroGestion', index:'IdElavoroGestion',  hidden:true},			   			   
			   {name:'NomElavoroGestion', index:'NomElavoroGestion',hidden:true},			   			   
			   {name:'IdBarreraYDefensa', index:'IdBarreraYDefensa',hidden:true},			   			   
			   {name:'IdAccionInsegura', index:'IdAccionInsegura',  hidden:true},
			   {name:'IdAmbito', index:'IdAmbito',hidden:true},			   			   			   
			   {name:'IdPrevenible', index:'IdPrevenible',hidden:true},			   			   
			   {name:'IdOrganizCultura', index:'IdOrganizCultura',hidden:true},		
			   {name:'tipo', index:'tipo',hidden:true},
			   {name:'Conclusiones', index:'Conclusiones',hidden:true},
			   ], 
             viewrecords: true, 
			 hidegrid: false, 
			 height: altoLis, 
			 width: ancho,
 
         });  
	    $('#drag'+ventanaActual.num).find("#listadoEventoA").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');
		break;	
		case 'administrarEventoAdverso': 
              cargarEAHospitalConClaseEAeIncidentes('00','00','cmbEventoProceso','00','/clinica/paginas/accionesXml/cargarEventosAdversosClase_xml.jsp');// para que se carguen todos los ea e incidentes antes de escoger una opcio			  
			  limpiarDivEditarJuan(arg); 

			  
			  altoLis= $('#drag'+ventanaActual.num).find("#divListadoEventosA").height()-70;
		    // $('#drag'+ventanaActual.num).find('#divContenido').css('height','500px');	
		 	 valores_a_mandar=pag+"?accion="+ventanaActual.opc;			 
 			 var datoscadena=$('#drag'+ventanaActual.num).find('#txtBusIdPaciente').val().split('-');			 
			 valores_a_mandar=valores_a_mandar+"&idPaciente="+datoscadena[0];
			 valores_a_mandar=valores_a_mandar+"&idEvento="+$('#drag'+ventanaActual.num).find('#txtBusIdEvento').val();			 
			 valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbBusproceso').val();	
			 valores_a_mandar=valores_a_mandar+"&idClase="+$('#drag'+ventanaActual.num).find('#cmbBusClaseEA').val();	
			 valores_a_mandar=valores_a_mandar+"&idEventoProceso="+$('#drag'+ventanaActual.num).find('#cmbBusEventoProceso').val();
			 valores_a_mandar=valores_a_mandar+"&AreaEvento="+$('#drag'+ventanaActual.num).find('#cmbBusUbicacionArea').val();
             valores_a_mandar=valores_a_mandar+"&FechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
             valores_a_mandar=valores_a_mandar+"&FechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();
             valores_a_mandar=valores_a_mandar+"&txtBusIdEventoNoConf="+$('#drag'+ventanaActual.num).find('#txtBusIdEventoNoConf').val();                          
			 valores_a_mandar=valores_a_mandar+"&EstadoEvento="+$('#drag'+ventanaActual.num).find('#cmbBusEstadoEvento').val();	
			 valores_a_mandar=valores_a_mandar+"&IdUnidadPertenece="+$('#drag'+ventanaActual.num).find('#cmbBusUnidadPertenece').val();					 
			 $('#drag'+ventanaActual.num).find("#listadoEventoA").jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:[ 'contador','idEvento','Descripcion Evento','idPaciente','Nombre Paciente','IdUnidadPertenece','Unidad Pertenece','idProceso',
					   'Descrici�n Proceso','Clase','Desc clase','tratamiento','idEventoProceso','Eventos del Proceso','idArea','nom Area','fecha Evento',
						'idElaboro','nomElaboro','idEstado','desEstado', 
						'IdElavoroGestion','NomElavoroGestion','IdBarreraYDefensa','IdAccionInsegura','IdAmbito','IdPrevenible','IdOrganizCultura','tipo','Conclusiones'],
			 colModel :[  
               {name:'contador', index:'contador',align:'left', width:((ancho*2)/100)},  
               {name:'idEvento', index:'idEvento',sorttype:"int", align:'left',hidden:true},  
               {name:'descEvento', index:'descEvento',width:((ancho*31)/100),sorttype:"int", align:'left'},  				   
               {name:'idPaciente', index:'idPaciente', align:'left',hidden:true },  	
               {name:'nomPaciente', index:'nomPaciente',  width:((ancho*15)/100), align:'left' },  	
               {name:'IdUnidadPertenece', index:'IdUnidadPertenece', align:'left',hidden:true },  	
               {name:'nomUnidadPertenece', index:'nomUnidadPertenece',  width:((ancho*10)/100), align:'left' },  	   			   			   
               {name:'idProceso', index:'idProceso',  align:'left',hidden:true},  
               {name:'descriProceso', index:'descriProceso',  width:((ancho*10)/100), align:'left' }, 
               {name:'idClase', index:'idClase',  width:((ancho*20)/100), align:'left',hidden:true }, 			   
               {name:'descClase', index:'descClase',  width:((ancho*10)/100), align:'left' }, 			   			   
			   {name:'tratamiento', index:'tratamiento',sorttype:"int",  align:'left',hidden:true},  				   
               {name:'idEventoProceso', index:'idEventoProceso',  align:'left',hidden:true},  
               {name:'descEventoProceso', index:'descEventoProceso',  width:((ancho*15)/100), align:'left' }, 
               {name:'idArea', index:'idArea',align:'left',hidden:true }, 			   
               {name:'nomArea', index:'nomArea',  width:((ancho*5)/100), align:'left' }, 
			   {name:'fechaEvento', index:'fechaEvento', width:((ancho*7)/100), align:'left'},   
			   {name:'idElaboro', index:'idElaboro',  align:'left',hidden:true},
			   {name:'nomElaboro', index:'nomElaboro',  align:'left',hidden:true},			   
			   {name:'idEstado', index:'idEstado',hidden:true},  
			   {name:'desEstado', index:'desEstado',  width:((ancho*7)/100), align:'left'}, 
			   {name:'IdElavoroGestion', index:'IdElavoroGestion',  hidden:true},			   			   
			   {name:'NomElavoroGestion', index:'NomElavoroGestion',hidden:true},			   			   
			   {name:'IdBarreraYDefensa', index:'IdBarreraYDefensa',hidden:true},			   			   
			   {name:'IdAccionInsegura', index:'IdAccionInsegura',  hidden:true},
			   {name:'IdAmbito', index:'IdAmbito',hidden:true},			   			   			   
			   {name:'IdPrevenible', index:'IdPrevenible',hidden:true},			   			   
			   {name:'IdOrganizCultura', index:'IdOrganizCultura',hidden:true},		
			   {name:'tipo', index:'tipo',hidden:true},
			   {name:'Conclusiones', index:'Conclusiones',hidden:true},
			   ], 
             viewrecords: true, 
			 hidegrid: false, 
			 height: altoLis, 
			 width: ancho,
			 
			 ondblClickRow: function( rowid) { 
			 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num).find('#listadoEventoA').getRowData(rowid);  

     	      $('#drag'+ventanaActual.num).find('#divContenido').css('height','600px');		   
              $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();	
              $('#drag'+ventanaActual.num).find('#divEditar').css('height','600px');
			  $('#drag'+ventanaActual.num).find('#divEditarFases').show();

              $("#cmbproceso option[value='"+$.trim(datosDelRegistro.idProceso)+"']").attr('selected', 'selected');
              $("#cmbClaseEA option[value='"+$.trim(datosDelRegistro.idClase)+"']").attr('selected', 'selected');			  
              $("#cmbEventoProceso option[value='"+$.trim(datosDelRegistro.idEventoProceso)+"']").attr('selected', 'selected');	
              $("#cmbUbicacionArea option[value='"+$.trim(datosDelRegistro.idArea)+"']").attr('selected', 'selected');	
		      $('#drag'+ventanaActual.num).find('#lblIdEvento').html($.trim(datosDelRegistro.idEvento)); 	
		      $('#drag'+ventanaActual.num).find('#lblDescripEvento').html($.trim(datosDelRegistro.descEvento));	
			  
			  $('#drag'+ventanaActual.num).find('#lblFechaEvento').html($.trim(datosDelRegistro.fechaEvento));	
			  $('#drag'+ventanaActual.num).find('#lblTratamiento').html($.trim(datosDelRegistro.tratamiento));	
			  $('#drag'+ventanaActual.num).find('#lblIdPaciente').html($.trim(datosDelRegistro.idPaciente));	
			  $('#drag'+ventanaActual.num).find('#lblNomPaciente').html($.trim(datosDelRegistro.nomPaciente));
 			  $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html($.trim(datosDelRegistro.desEstado));			  

			 /************************************** Administrar evento adverso segundo actor ****************************************/			  
			 $("#cmbBarreraYDefensa option[value='"+$.trim(datosDelRegistro.IdBarreraYDefensa)+"']").attr('selected', 'selected');
			 $("#cmbAccionInsegura option[value='"+$.trim(datosDelRegistro.IdAccionInsegura)+"']").attr('selected', 'selected');
			 $("#cmbAmbito option[value='"+$.trim(datosDelRegistro.IdAmbito)+"']").attr('selected', 'selected');
			 $("#cmbPrevenible option[value='"+$.trim(datosDelRegistro.IdPrevenible)+"']").attr('selected', 'selected');			 
			 $("#cmbOrganizCultura option[value='"+$.trim(datosDelRegistro.IdOrganizCultura)+"']").attr('selected', 'selected');
			 
			 $('#drag'+ventanaActual.num).find('#lblNomElavoroGestion').html($.trim(datosDelRegistro.NomElavoroGestion));
			 
			 $('#drag'+ventanaActual.num).find('#txtConclusiones').val($.trim(datosDelRegistro.Conclusiones));

			 
			 /*permiso de aparecer o no los botones del gestion de primer actor*/
			 if(  $.trim(datosDelRegistro.idEstado)==1  ){   mostrar('divBotonGuardGestionEA');	mostrar('divBotonGuardGestionEA2');	}
			 else{  ocultar('divBotonGuardGestionEA');   ocultar('divBotonGuardGestionEA2');}

			 /*permiso de aparecer o no los botones del gestion de segundo actor*/
			 if(  $.trim(datosDelRegistro.idEstado)==2  ){   
			        mostrar('divBotonseguimientoOporMejora');	
					mostrar('divBotonseguimientoOporMejora2');
					//mostrar('divBotonResumenConclusiones');
					
			 }
			 else{  ocultar('divBotonseguimientoOporMejora');   ocultar('divBotonseguimientoOporMejora2');/* ocultar('divBotonResumenConclusiones');*/}

			 if(  $.trim(datosDelRegistro.idEstado)>=2  ){ 	mostrar('divBotonResumenConclusiones');
					
			 }
			 else{  ocultar('divBotonResumenConclusiones');}

			 
              listadoTabla('listFactContribu');
			  setTimeout("listadoTabla('listPersonasOportMejora')",2000);
			//  setTimeout("listadoTabla('listSeguimientoOportMejora')",2000);			  
			 // setTimeout("listadoTabla('listNivelResponsabili')",3000);			  			  
		  
              },
			 //height: 210, 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
            // caption: 'Resultados' 
         });  
	    $('#drag'+ventanaActual.num).find("#listadoEventoA").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;
		case 'administrarPlanMejora': 
		     limpiarDivEditarJuan(arg); 
			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','720px');				 
		 	 valores_a_mandar=pag+"?accion="+ventanaActual.opc;
			 valores_a_mandar=valores_a_mandar+"&idProceso="+$('#drag'+ventanaActual.num).find('#cmbBusproceso').val();			 
			 valores_a_mandar=valores_a_mandar+"&tipoPlan="+$('#drag'+ventanaActual.num).find('#cmbBusTipoPlan').val();			 			 
             valores_a_mandar=valores_a_mandar+"&BusFechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
             valores_a_mandar=valores_a_mandar+"&BusFechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();
             valores_a_mandar=valores_a_mandar+"&BusIdEvento="+$('#drag'+ventanaActual.num).find('#txtBusIdEvento').val();	
             valores_a_mandar=valores_a_mandar+"&BusAspectoMejorar="+$('#drag'+ventanaActual.num).find('#txtBusAspectoMejorar').val();				 
			 valores_a_mandar=valores_a_mandar+"&EstadoEvento="+$('#drag'+ventanaActual.num).find('#cmbBusEstadoEvento').val();		
			 valores_a_mandar=valores_a_mandar+"&solo_mis="+document.getElementById('chk_solo_mis').checked;
			 // alert(valores_a_mandar);
 var lastsel2;
			 $('#drag'+ventanaActual.num).find("#listTodoAspecto").jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['valor','stock','ship', 'contador','idProceso', 'Descrip Proceso','Id Evento','Descripcion Aspecto a mejorar','tipoPlan','Tipo de Pl�n','riesgo','costo','volumen','Prioriza','Costo Tot','fecha Creado','idElaboro','idPlanAccion','fecha Ini PlanAccion','% Cumpli','Estado','modif','SeguimientoEficaz','SeguimientoMeta'],			 
             colModel :[  
//			   {name:'valor', index:'valor',editable: true,  align:'left', width:((ancho*2)/100)}, 	
			   //{name:'valor',index:'valor', width:100, sortable:false,editable: true,edittype:"textarea", editoptions:{rows:"2",cols:"2"}},
			   {name:'id',index:'id', width:90, sorttype:"text", editable: true,hidden:true},
			   {name:'stock',index:'stock', width:60, editable: true, edittype:"checkbox",editoptions: {value:"Yes:No"},hidden:true},
			   {name:'ship',index:'ship', width:90, editable: true, edittype:"select", editoptions:{value:"M:Masculino;F:Femenino;O:Otro"},hidden:true},
               {name:'contador', index:'contador',  align:'left', width:((ancho*2)/100)},  						
               {name:'idProceso', index:'idProceso',  align:'left',hidden:true},  
               {name:'DescriProceso', index:'DescriProceso',  width:((ancho*7)/100), align:'left' },  	   
               {name:'IdEvento', index:'IdEvento',  sorttype:"int", align:'left',hidden:true},  
               {name:'descEvento', index:'descEvento', width:((ancho*30)/100),  sorttype:"int", align:'left'},  
			   
               {name:'tipoPlan', index:'tipoPlan',  sorttype:"int",  align:'left' ,hidden:true},  
               {name:'descrpPlan', index:'descrpPlan',  width:((ancho*8)/100),sorttype:"int",  align:'left'},  				   
               {name:'riesgo', index:'riesgo', width:((ancho*3)/100),  sorttype:"int", align:'left'},  			   
               {name:'costo', index:'costo', width:((ancho*3)/100), sorttype:"int", align:'left'},  	
               {name:'volumen', index:'volumen', width:((ancho*3)/100), sorttype:"int", align:'left'},  
               {name:'prioriza', index:'prioriza', width:((ancho*4)/100), sorttype:"int", align:'left'},  			   
               {name:'costoTot', index:'costoTot', width:((ancho*8)/100), sorttype:"int", align:'right', formatoptions:{decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 0}, prefix: "$ ",formatter:'currency'},  
			   {name:'fechaCreacion', index:'fechaCreacion', align:'left',hidden:true},    			   
			   {name:'idElaboro', index:'idElaboro',  align:'left',hidden:true},  	
			   {name:'idPlanAccion', index:'idPlanAccion',  align:'left',hidden:true},  
			   {name:'fechaIniPlanAccion', index:'fechaIniPlanAccion',width:((ancho*8)/100),  align:'left'}, 
               {name:'porcentCumpl', index:'porcentCumpl', width:((ancho*5)/100), align:'left' },  			   
			   
			   {name:'estado', index:'estado',  width:((ancho*5)/100), align:'left'}, 			   
			   {name:'modificable', index:'modificable',  align:'left', width:((ancho*3)/100)},  				   			   
			   {name:'SeguimientoEficaz', index:'SeguimientoEficaz',  align:'left', hidden:true },  				   			   
			   {name:'SeguimientoMeta', index:'SeguimientoMeta',  align:'left', hidden:true},  				   			   			   
			   ], 
			 /*
			  onSelectRow: function(id){// alert(lastsel2+'-'+id);
				if(id && id!==lastsel2){
				  jQuery('#drag'+ventanaActual.num).find('#listTodoAspecto').restoreRow(lastsel2);
				  jQuery('#drag'+ventanaActual.num).find('#listTodoAspecto').editRow(id,true);
//				  lastsel2=id;
				}
              },*/
			 
             pager: jQuery('#pagerListadosPlanesMejora'), 
             rowNum:100, 
             rowList:[20,50,100], 
             sortname: '1', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 480, 
			 width: ancho,
			
			 ondblClickRow: function(rowid) { 
								 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num).find('#listTodoAspecto').getRowData(rowid);  
     	     // $('#drag'+ventanaActual.num).find('#divContenido').css('height','720px');		   
              $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();	
              $('#drag'+ventanaActual.num).find('#divEditar').css('height','600px');
			  $('#drag'+ventanaActual.num).find('#divEditarFases').show();

              $("#cmbproceso option[value='"+$.trim(datosDelRegistro.idProceso)+"']").attr('selected', 'selected');
		      $('#drag'+ventanaActual.num).find('#lblIdEvento').html($.trim(datosDelRegistro.IdEvento)); 			
              $('#drag'+ventanaActual.num).find('#txtAspectoMejorar').val($.trim(datosDelRegistro.descEvento));
              $("#cmbTipoPlan option[value='"+$.trim(datosDelRegistro.tipoPlan)+"']").attr('selected', 'selected');	
              $('#drag'+ventanaActual.num).find('#lblFechaCreacion').html($.trim(datosDelRegistro.fechaCreacion));				  
 			  $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html($.trim(datosDelRegistro.estado));
			  
			  
			  $("#cmbRiesgo option[value='"+$.trim(datosDelRegistro.riesgo)+"']").attr('selected', 'selected');
			  $("#cmbCosto option[value='"+$.trim(datosDelRegistro.costo)+"']").attr('selected', 'selected');
			  $("#cmbVolumen option[value='"+$.trim(datosDelRegistro.volumen)+"']").attr('selected', 'selected');			  			  
			  $('#drag'+ventanaActual.num).find('#lblTotPrioriza').html($.trim(datosDelRegistro.prioriza));
			  
			  $("#cmbEficaz option[value='"+$.trim(datosDelRegistro.SeguimientoEficaz)+"']").attr('selected', 'selected');
			  $('#drag'+ventanaActual.num).find('#txtSeguimMetaPlanAccion').val($.trim(datosDelRegistro.SeguimientoMeta));
			  
			  /********************************************** gestion plan de accion ************************************/

              presentarDatosPlanAccionGral(datosDelRegistro.IdEvento);

			  $('#drag'+ventanaActual.num).find('#divEditarPlanDeAccion').show();
			  $('#drag'+ventanaActual.num).find('#divDatosPlanAccion').show();
			  
			  
			  if(datosDelRegistro.modificable=='noxxxxxxxxxxxxxxxxxxxxxxx'){ // se debe de dejar la palabra no  -> para cuando la entidad este acostumbrada
				  ocultar('divBotonPlanAccion');
				  mostrar('divNoModificar');					  
				  ocultar('divBotonPlanAccion1');
				  mostrar('divNoModificar1');					  
			  }	
			  else{	
			      mostrar('divBotonPlanAccion');
				  ocultar('divNoModificar');	
			      mostrar('divBotonPlanAccion1');
				  ocultar('divNoModificar1');
			  }		 
			  if(datosDelRegistro.SeguimientoEficaz!='00')   ocultar('divBotonSeguimiePlanAccio');		// si es diferentes quiere decir que ya tiene un seguimiento y esta cerrado el plan		  
			  else mostrar('divBotonSeguimiePlanAccio');				  
			  
			  
            		  
              },
			 //height: 210, 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
            // caption: 'Resultados' 
         });  
	    $('#drag'+ventanaActual.num).find("#listTodoAspecto").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;	
		case 'listPacientesListaEspera':
		     limpiarDivEditarJuan(arg); 
			 ancho= ($('#drag'+ventanaActual.num).find("#divContenidoListaEspera2").width())*97/100;
			
			 $('#drag'+ventanaActual.num).find('#divContenidoListaEspera2').css('height','180px');				 
		 	 valores_a_mandar=pag+"?accion="+arg;			

			 $('#drag'+ventanaActual.num).find("#listPacientesListaEspera").jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['Cuantos','idLista','Id Paciente','Nombre Paciente', 'Profesional Preferencial', 'DiasEspera','Fecha Ingr.Lista','nom Elaboro'],			 
             colModel :[  
               {name:'Cuantos', index:'Cuantos',  width:((ancho*5)/100), align:'left'}, 
               {name:'idLista', index:'idLista',  width:((ancho*5)/100), align:'left'},  			   
               {name:'idPaciente', index:'idPaciente',  width:((ancho*15)/100), align:'left'},  
               {name:'NombrePaciente', index:'NombrePaciente',  width:((ancho*30)/100), align:'left' },  	   
               {name:'Profesional', index:'Profesional',  width:((ancho*20)/100), align:'left' },  	   			   
               {name:'DiasEspera', index:'DiasEspera',  width:((ancho*5)/100), align:'left' },  			   
               {name:'FechaUltimoIngreso', index:'FechaUltimoIngreso',  width:((ancho*10)/100), align:'left' },  	
               {name:'nomElaboro', index:'nomElaboro',  width:((ancho*5)/100), align:'left' },  			   
			   ], 
			 height: 100, 
			 width: ancho,	
         });  
	    $('#drag'+ventanaActual.num).find("#listPacientesListaEspera").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;	
/*		case 'listCitasHoy': 
		     limpiarDivEditarJuan(arg); 
	//		 ancho= ($('#drag'+ventanaActual.num).find("#divContenidoCitasHoy").width())*97/100;			
//			 $('#drag'+ventanaActual.num).find('#divContenidoCitasHoy').css('height','530px');				 
		 	 valores_a_mandar=pag+"?accion="+arg;			 
			  
			  if($('#drag'+ventanaActual.num).find('#txtIdPacienteCitaHoy').val()!=''){
  			     var datoscadena=$('#drag'+ventanaActual.num).find('#txtIdPacienteCitaHoy').val().split('-');	
			     if( !(datoscadena.length==2 && datoscadena[0]!='' && datoscadena[1]!='') ){   
				     alert('Falta ingresar correctamente: Identificaci�n, Apellidos con nombres separados por el signo - \n \nEjemplo:  CC98400895-Andrade Jesus Mario ');  return false;
				 }	
				 else valores_a_mandar=valores_a_mandar+"&IdPaciente="+datoscadena[0];

			  }
			  else valores_a_mandar=valores_a_mandar+"&IdPaciente="+$('#drag'+ventanaActual.num).find('#txtIdPacienteCitaHoy').val();
			 
  	         valores_a_mandar=valores_a_mandar+"&idProfesional="+$('#drag'+ventanaActual.num).find('#cmbProfesionales').val();	
             valores_a_mandar=valores_a_mandar+"&BusFechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
             valores_a_mandar=valores_a_mandar+"&BusFechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();

			 $('#drag'+ventanaActual.num).find("#"+arg).jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['Cuantos','Id cita','idEstadoCita','Estado','Tipo','Fecha','Jornada','Turno','Hora','IdPaciente','Nombre Paciente', 'idProfesional','Profesional','codServicio','Servicio','FechaAsistencia','Hoy'],			 
             colModel :[  
               {name:'Cuantos', index:'Cuantos',  width:((ancho*2)/100), align:'left'},  						
               {name:'idcita', index:'idcita',  width:((ancho*5)/100), align:'left'},  
               {name:'idEstadoCita', index:'idEstadoCita',  hidden:true, align:'left'},  
               {name:'Estado', index:'Estado',  width:((ancho*8)/100), align:'left'},  
               {name:'Tipo', index:'Tipo',  width:((ancho*3)/100), align:'left'},  
               {name:'Fecha', index:'Fecha',  width:((ancho*7)/100), align:'left'},  
               {name:'Jornada', index:'Jornada',  width:((ancho*5)/100), align:'left'},  			   
               {name:'Turno', index:'Turno',  hidden:true, align:'left'},  
               {name:'Hora', index:'Hora',  width:((ancho*5)/100), align:'left'},  
               {name:'IdPaciente', index:'IdPaciente',  width:((ancho*15)/100), align:'left'},
               {name:'NombrePaciente', index:'NombrePaciente',  width:((ancho*20)/100), align:'left' },  	   
               {name:'idProfesional', index:'idProfesional', hidden:true , align:'left' },  	   			   
               {name:'Profesional', index:'Profesional',  width:((ancho*10)/100), align:'left' },  	   			   
               {name:'codServicio', index:'codServicio', hidden:true, align:'left' },  	
               {name:'Servicio', index:'Servicio',  width:((ancho*5)/100), align:'left' },  				   
               {name:'FechaAsistencia', index:'FechaAsistencia', hidden:true, align:'left' },  				   			   
               {name:'Hoy', index:'Hoy', hidden:true, align:'left' },  				   
			   ], 
			 height: 500, 
			 width: ancho,			 

         });  
	    $('#drag'+ventanaActual.num).find("#"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;	*/
		case 'listMisCitasHoy': 
		     limpiarDivEditarJuan(arg); 
			 ancho= ($('#drag'+ventanaActual.num).find("#divContenidoCitasHoy").width())*97/100;			
			 $('#drag'+ventanaActual.num).find('#divContenidoCitasHoy').css('height','530px');				 
		 	 valores_a_mandar=pag+"?accion="+arg;			 
            // valores_a_mandar=valores_a_mandar+"&IdPaciente="+$('#drag'+ventanaActual.num).find('#txtBusIdPaciente').val();
			 
			  /*var datoscadena=$('#drag'+ventanaActual.num).find('#txtBusIdPaciente').val().split('-');	
			  if( !(datoscadena.length==2 && datoscadena[0]!='' && datoscadena[1]!='') ){   alert('Falta ingresar correctamente: Identificaci�n, Apellidos con nombres separados por el signo - \n \nEjemplo:  CC98400895-Andrade Jesus Alfredo ');  return false;}		
			  */
			  
			  if($('#drag'+ventanaActual.num).find('#txtBusIdPacienteCitaHoy').val()!=''){
  			     var datoscadena=$('#drag'+ventanaActual.num).find('#txtBusIdPacienteCitaHoy').val().split('-');	
			     if( !(datoscadena.length==2 && datoscadena[0]!='' && datoscadena[1]!='') ){   
				     alert('Falta ingresar correctamente: Identificaci�n, Apellidos con nombres separados por el signo - \n \nEjemplo:  CC98400895-Andrade Jesus Mario ');  return false;
				 }	
				 else valores_a_mandar=valores_a_mandar+"&IdPaciente="+datoscadena[0];

			  }
			  else valores_a_mandar=valores_a_mandar+"&IdPaciente="+$('#drag'+ventanaActual.num).find('#txtBusIdPacienteCitaHoy').val();
			 
  	         valores_a_mandar=valores_a_mandar+"&idProfesional="+$('#drag'+ventanaActual.num).find('#cmbProfesionales').val();	
             valores_a_mandar=valores_a_mandar+"&BusFechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
             valores_a_mandar=valores_a_mandar+"&BusFechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();
//alert(valores_a_mandar);
			 $('#drag'+ventanaActual.num).find("#"+arg).jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['Cuantos','Id cita','idEstadoCita','Estado','Tipo','Fecha','Jornada','Turno','Hora','IdPaciente','Nombre Paciente', 'idProfesional','Profesional','codServicio','Servicio','FechaAsistencia','Hoy'],			 
             colModel :[  
               {name:'Cuantos', index:'Cuantos',  width:((ancho*2)/100), align:'left'},  						
               {name:'idcita', index:'idcita',  width:((ancho*5)/100), align:'left'},  
               {name:'idEstadoCita', index:'idEstadoCita',  hidden:true, align:'left'},  
               {name:'Estado', index:'Estado',  width:((ancho*8)/100), align:'left'},  
               {name:'Tipo', index:'Tipo',  width:((ancho*3)/100), align:'left'},  
               {name:'Fecha', index:'Fecha',  width:((ancho*7)/100), align:'left'},  
               {name:'Jornada', index:'Jornada',  width:((ancho*5)/100), align:'left'},  			   
               {name:'Turno', index:'Turno',  hidden:true, align:'left'},  
               {name:'Hora', index:'Hora',  width:((ancho*5)/100), align:'left'},  
               {name:'IdPaciente', index:'IdPaciente',  width:((ancho*15)/100), align:'left'},
               {name:'NombrePaciente', index:'NombrePaciente',  width:((ancho*20)/100), align:'left' },  	   
               {name:'idProfesional', index:'idProfesional', hidden:true , align:'left' },  	   			   
               {name:'Profesional', index:'Profesional',  width:((ancho*10)/100), align:'left' },  	   			   
               {name:'codServicio', index:'codServicio', hidden:true, align:'left' },  	
               {name:'Servicio', index:'Servicio',  width:((ancho*5)/100), align:'left' },  				   
               {name:'FechaAsistencia', index:'FechaAsistencia', hidden:true, align:'left' },  				   			   
               {name:'Hoy', index:'Hoy', hidden:true, align:'left' },  				   
			   ], 
			 height: 500, 
			 width: ancho,			 

         });  
	    $('#drag'+ventanaActual.num).find("#"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;			
		
	   case 'listPacientesListaEsperaTraer':
		     limpiarDivEditarJuan(arg);  
//             limpiarDatosPacienteListaEspera();
			 mostrar('divTraerListaEspera'); 			 
			 
			 ancho= ($('#drag'+ventanaActual.num).find("#divContenidoListaEspera2Traer").width())*97/100;
 			 $('#drag'+ventanaActual.num).find('#divContenidoListaEspera2Traer').css('height','90%');				 
		 	 valores_a_mandar=pag+"?accion="+arg;

			  if($('#drag'+ventanaActual.num).find('#txtBusIdPacienteCitaHoy').val()!=''){
  			     var datoscadena=$('#drag'+ventanaActual.num).find('#txtBusIdPacienteCitaHoy').val().split('-');	
			     if( !(datoscadena.length==2 && datoscadena[0]!='' && datoscadena[1]!='') ){   
				     alert('Falta ingresar correctamente: Identificaci�n, Apellidos con nombres separados por el signo - \n \nEjemplo:  CC98400895-Andrade Jesus Mario ');  return false;
				 }	
				 else valores_a_mandar=valores_a_mandar+"&IdPaciente="+datoscadena[0];

			  }
			  else valores_a_mandar=valores_a_mandar+"&IdPaciente="+$('#drag'+ventanaActual.num).find('#txtBusIdPacienteCitaHoy').val();
             valores_a_mandar=valores_a_mandar+"&Edad="+$('#drag'+ventanaActual.num).find('#cmbBusEdad').val();				 
  	         valores_a_mandar=valores_a_mandar+"&idProfesional="+$('#drag'+ventanaActual.num).find('#cmbProfesionalesListaEsperaTraer').val();	
             valores_a_mandar=valores_a_mandar+"&BusFechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesdeListaEspera').val();
             valores_a_mandar=valores_a_mandar+"&BusFechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHastaListaEspera').val();
             valores_a_mandar=valores_a_mandar+"&estado="+$('#drag'+ventanaActual.num).find('#cmbBusEstadoListaEspera').val();			
             valores_a_mandar=valores_a_mandar+"&disponible="+$('#drag'+ventanaActual.num).find('#cmbBuscarDisponibleListaEspera').val();			 			 
             valores_a_mandar=valores_a_mandar+"&SinCita="+$('#drag'+ventanaActual.num).find('#cmbBusSinCita').val();	
             valores_a_mandar=valores_a_mandar+"&TipoCita="+$('#drag'+ventanaActual.num).find('#cmbBusTipoCita').val();				 
			 
			  if($('#drag'+ventanaActual.num).find('#txtAdministradora1LE').val()!=''){
  			     var datoscadena=$('#drag'+ventanaActual.num).find('#txtAdministradora1LE').val().split('-');	

				 if( !(datoscadena.length==2 && datoscadena[0]!='' && datoscadena[1]!='') ){   
				     alert('Falta ingresar correctamente:  C�digo y administradora - \n \nEjemplo:  0000-Particulares ');  return false;
				 }	
				 else{ 
  			         valores_a_mandar=valores_a_mandar+"&administradora1LE="+datoscadena[0];			  				 
				 }
			  }		
			 valores_a_mandar=valores_a_mandar+"&administradora1LE=";			  				 			  


			 
			 $('#drag'+ventanaActual.num).find("#"+arg).jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['Cuantos','id Lista','Id Paciente','Nombre Paciente','FechaNacimientoPaciente','sexo','edad', 'administradora1', 'Administradora 1', 'Administradora 2','telefonos','codmunicipio','municipio','direccion', 'idProfesional','Profesional Preferencial','tipoCita','observacion', 'Fecha Ingr.Lista',
					   'acompanante', 'tipoPaciente','Dias Espera','DiscapacidadFisica','Embarazo','ZonaResidencia','GradoNecesidad','idDisponible','Disponible','idElaboro','estado'
			 ],			 

             colModel :[  
               {name:'Cuantos', index:'Cuantos',  width:((ancho*3)/100), align:'left'},  						
               {name:'idListaEspera', index:'idListaEspera',  hidden:true, align:'left'},  									   
               {name:'idPaciente', index:'idPaciente',  hidden:true, align:'left'},  
               {name:'NombrePaciente', index:'NombrePaciente',  width:((ancho*20)/100), align:'left' }, 
               {name:'FechaNacimientoPaciente', index:'FechaNacimientoPaciente',  hidden:true, align:'left' },  
               {name:'sexo', index:'sexo',  hidden:true, align:'left' },  			   
               {name:'edad', index:'edad',  width:((ancho*5)/100), align:'left' }, 			   
               {name:'administradora1', index:'administradora1',  hidden:true,  align:'left' },  	   			   			   
               {name:'nomAdministradora1', index:'nomAdministradora1',  width:((ancho*30)/100),  align:'left' },  	   			   			   			   
               {name:'administradora2', index:'administradora2',  hidden:true, align:'left' },  	   			   			   
			   
               {name:'telefonos', index:'telefonos',  width:((ancho*25)/100),  align:'left' },  	
               {name:'codmunicipio', index:'codmunicipio',   hidden:true, align:'left' },  	   			   			   			   
               {name:'municipio', index:'municipio',  width:((ancho*15)/100), align:'left' },  	   			   			   
               {name:'direccion', index:'direccion',  hidden:true, align:'left' },  
               {name:'idProfesional', index:'idProfesional', hidden:true, align:'left' },  				   
               {name:'Profesional', index:'Profesional',  width:((ancho*10)/100), align:'left' },  	
               {name:'tipoCita', index:'tipoCita',  width:((ancho*2)/100), align:'left' },  
               {name:'observacion', index:'observacion',  width:((ancho*27)/100), align:'left' },  	
               {name:'FechaUltimoIngreso', index:'FechaUltimoIngreso',  width:((ancho*13)/100), align:'left' },  
               {name:'acompanante', index:'acompanante',  hidden:true, align:'left' },  
               {name:'tipoPaciente', index:'tipoPaciente',   hidden:true, align:'left' },  	 
               {name:'diasEspera', index:'diasEspera',  width:((ancho*5)/100), align:'left' },  	
               {name:'DiscapacidadFisica', index:'DiscapacidadFisica',   hidden:true, align:'left' },  	
               {name:'Embarazo', index:'Embarazo',   hidden:true, align:'left' },  	   			   			   
               {name:'ZonaResidencia', index:'ZonaResidencia',   hidden:true, align:'left' },  	   			   			   
               {name:'GradoNecesidad', index:'GradoNecesidad',   hidden:true, align:'left' },  	   			   			   			   
               {name:'idDisponible', index:'idDisponible',hidden:true, align:'left' },  	   		
               {name:'Disponible', index:'Disponible', align:'left' },  	   					   
               {name:'idElaboro', index:'idElaboro',  width:((ancho*10)/100), align:'left' },  	   		
               {name:'estado', index:'estado',  width:((ancho*10)/100), align:'left' },  	   					   
			   
			   ], 
			 
			    ondblClickRow: function( rowid) {	
				 var datosDelRegistro = jQuery('#drag'+ventanaActual.num).find('#'+arg).getRowData(rowid);			  
			    if(confirm('Desea Traer de la lista de espera?')){
			  

			     limpiarDatosPacientePaciente(); 
				 $('#drag'+ventanaActual.num).find('#lblIdListaEspera').html(datosDelRegistro.idListaEspera); 
				 $('#drag'+ventanaActual.num).find('#txtBusIdPaciente').val(datosDelRegistro.idPaciente+'-'+datosDelRegistro.NombrePaciente);
				 $('#drag'+ventanaActual.num).find('#txtMunicipio').val(datosDelRegistro.codmunicipio+'-'+datosDelRegistro.municipio);			 
				 $('#drag'+ventanaActual.num).find('#txtDireccionRes').val(datosDelRegistro.direccion);     			 
				 $('#drag'+ventanaActual.num).find('#txtNomAcompanante').val(datosDelRegistro.acompanante);       			 
				 $('#drag'+ventanaActual.num).find('#txtTelefonos').val(datosDelRegistro.telefonos);   
				 $('#drag'+ventanaActual.num).find('#txtAdministradora1').val(datosDelRegistro.administradora1);
				 $('#drag'+ventanaActual.num).find('#txtAdministradora2').val(datosDelRegistro.administradora2); 
				 
				 $("#cmbTipoUsuario option[value='00']").attr('selected', 'selected');	   
				 $("#cmbTipoCita option[value='00']").attr('selected', 'selected');	   
				 $("#cmbEstadoCita option[value='00']").attr('selected', 'selected');	 
	
				 $("#cmbTipoUsuario option[value='"+datosDelRegistro.tipoPaciente+"']").attr('selected', 'selected');	
				 $("#cmbTipoCita option[value='"+datosDelRegistro.tipoCita+"']").attr('selected', 'selected');	
	   
				 $('#drag'+ventanaActual.num).find('#txtObservacion').val(datosDelRegistro.observacion);  
				 
				 ocultar('divTraerListaEspera');
				 // buscarInformacionPacienteExistente();
				 buscarPlatin('listadoDocumenHC','/clinica/paginas/accionesXml/buscarCitas_xml.jsp');
  				 mostrar('divBotonGuardarCitaListaEspera');
				 $('#drag'+ventanaActual.num).find('#cmbProfesionalesListaEsperaCita').attr('disabled','disabled');
				}
				else{	/**********************************************************************************************************************  MODIFICAR  ************/		 
				   ocultar('divTraerListaEspera');
				   mostrar('divListaEspera');				   
				   limpiarDatosPacienteListaEspera();
				   limpiarListadosTotales('listDocumentosPacienteListaEspera');  
				   limpiarListadosTotales('listHistoricoListaEspera');

				   setTimeout("buscarPlatin('listHistoricoListaEspera','/clinica/paginas/accionesXml/buscarCitas_xml.jsp')",300);//ubo problemas con otro list el de eventos relacion list												   
  				   setTimeout("buscarPlatin('listadoDocumenHCListaEspera','/clinica/paginas/accionesXml/buscarCitas_xml.jsp')",500);//ubo problemas con otro list el de eventos relacion list					   
				   buscarJuan('listPacientesListaEspera','/clinica/paginas/accionesXml/buscarCitas_xml.jsp');

				   $('#drag'+ventanaActual.num).find('#txtBusIdPacienteListaEspera').val(datosDelRegistro.idPaciente+'-'+datosDelRegistro.NombrePaciente);			
   				   $('#drag'+ventanaActual.num).find('#hidd_IdPacienteListaEspera').val(datosDelRegistro.idPaciente);   

				   $('#drag'+ventanaActual.num).find('#txtFechaNacimientoListaEspera').val(datosDelRegistro.FechaNacimientoPaciente);
				   $("#cmbSexoListaEspera option[value='"+datosDelRegistro.sexo+"']").attr('selected', 'selected');
				   $('#drag'+ventanaActual.num).find('#lblIdListaEspera2').html(datosDelRegistro.idListaEspera); 
				   $('#drag'+ventanaActual.num).find('#txtMunicipioListaEspera').val(datosDelRegistro.codmunicipio+'-'+datosDelRegistro.municipio);
				   $('#drag'+ventanaActual.num).find('#txtDireccionResListaEspera').val(datosDelRegistro.direccion);   
				   $('#drag'+ventanaActual.num).find('#txtNomAcompananteListaEspera').val(datosDelRegistro.acompanante);   
				   $('#drag'+ventanaActual.num).find('#txtTelefonosListaEspera').val(datosDelRegistro.telefonos);       						   
				   $('#drag'+ventanaActual.num).find('#txtAdministradora1ListaEspera').val(datosDelRegistro.administradora1);
				   $('#drag'+ventanaActual.num).find('#txtAdministradora2ListaEspera').val(datosDelRegistro.administradora2); 
				   $('#drag'+ventanaActual.num).find('#txtObservacionListaEspera').val(datosDelRegistro.observacion); 				   
				   $("#cmbTipoUsuarioListaEspera option[value='"+datosDelRegistro.tipoPaciente+"']").attr('selected', 'selected');	   
				   $("#cmbTipoCitaListaEspera option[value='"+datosDelRegistro.tipoCita+"']").attr('selected', 'selected');	   
				   
				   $("#cmbProfesionalesListaEspera option[value='"+datosDelRegistro.idProfesional+"']").attr('selected', 'selected');	   				   
				   $("#cmbDiscapaciFisicaListaEspera option[value='"+datosDelRegistro.DiscapacidadFisica+"']").attr('selected', 'selected');	   
				   $("#cmbEmbarazoListaEspera option[value='"+datosDelRegistro.Embarazo+"']").attr('selected', 'selected');	   				   				   
				   $("#cmbZonaResidenciaListaEspera option[value='"+datosDelRegistro.ZonaResidencia+"']").attr('selected', 'selected');				   
				   $("#cmbNecesidadListaEspera option[value='"+datosDelRegistro.GradoNecesidad+"']").attr('selected', 'selected');				   				   
				   $("#cmbDisponibleListaEspera option[value='"+datosDelRegistro.idDisponible+"']").attr('selected', 'selected');				   				   				   
				   $("#cmbEstadoListaEspera option[value='"+datosDelRegistro.estado+"']").attr('selected', 'selected');	
				   ocultar('divBotonGuardarCitaListaEspera');
  				   mostrar('divBotonModificarCitaListaEspera');
   				   $('#drag'+ventanaActual.num).find('#cmbProfesionalesListaEsperaCita').attr('disabled','');

				}
		  

	         },	
			 
			 height: 500, 
			 width: ancho,			 

         });  
	    $('#drag'+ventanaActual.num).find("#"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;			
		
     		
	 }
	 
	 
	 
	 
}


function respuestaBuscarJuan(){  
	if(varajax.readyState == 4 ){
		VentanaModal.cerrar();
			if(varajax.status == 200){
			  raiz=varajax.responseXML.documentElement;
			  posicionActual=0;///posicion de elementos de la busqueda
			  switch(paginaActual){
				  
				    case 'paginaNotificaciones': 
					 document.getElementById('mensajesNuevos').innerHTML = "Tiene 3 Mensajes Nuevos";				  
				  
				    break; 
					
					case 'paginaCargarManifiestos': 
					break;		

			  }
		 }
	}
}


function numHistoriaPacienteTipoId(){ 
	
	 if($('#drag'+ventanaActual.num).find('#cmbTipoId').val()!="00" ){
		 if( $('#drag'+ventanaActual.num).find('#txtIdPersona').val()!=""){
             $('#drag'+ventanaActual.num).find('#txtIdHistoria').val( $('#drag'+ventanaActual.num).find('#cmbTipoId').val()+$('#drag'+ventanaActual.num).find('#txtIdPersona').val() );
             $('#drag'+ventanaActual.num).find('#cmbSexo').focus();
	     }	 	
	 }
	 else{
		   alert("Selecciones Tipo Identificaci�n");
		   $('#drag'+ventanaActual.num).find('#txtIdHistoria').val('');
           $('#drag'+ventanaActual.num).find('#cmbTipoId').focus();	   
     }   
}


function numHistoriaPacienteId(){
	
  	  if($('#drag'+ventanaActual.num).find('#txtIdPersona').val()!="" ){
		  if($('#drag'+ventanaActual.num).find('#cmbTipoId').val()!="00"){
            $('#drag'+ventanaActual.num).find('#txtIdHistoria').val(  $('#drag'+ventanaActual.num).find('#cmbTipoId').val()+$('#drag'+ventanaActual.num).find('#txtIdPersona').val()  );
            $('#drag'+ventanaActual.num).find('#cmbSexo').focus();
	     }
		 else{
			 alert("Selecciones Tipo Identificaci�n");
			 $('#drag'+ventanaActual.num).find('#txtIdHistoria').val('');
			 $('#drag'+ventanaActual.num).find('#cmbTipoId').focus();	 
		 }
	  }
	  else{
		   alert("Falta Identificaci�n del Paciente");
		   $('#drag'+ventanaActual.num).find('#txtIdHistoria').val('');
           $('#drag'+ventanaActual.num).find('#txtIdPersona').focus();	   
      }
}

function entidaAdministradoraSalud(){   
/*    crearNinguno=0;
    
	for( ii=0;ii<=3;ii++){
	    if( document.getElementById('cmbTipoAfiliado').options[ii].value == '3'){
			crearNinguno=1;
		}
	}
	*/
	if( document.getElementById('cmbNombreEntidad').options[document.getElementById('cmbNombreEntidad').selectedIndex].id =='P'  ){
	   $('#drag'+ventanaActual.num).find('#txtTipoUsuario').val('PARTICULAR');
	   $('#drag'+ventanaActual.num).find('#txtCarnet').val('');	   
	   $('#drag'+ventanaActual.num).find('#txtCarnet').attr('disabled','disabled');	
	   
	  /* if(	$('#drag'+ventanaActual.num).find('#txtTipoUsuario').val()==null ){
          $('#cmbTipoAfiliado').append('<option value="3">NINGUNO</option>');	
	   }*/   

	   
	   $('#drag'+ventanaActual.num).find('#cmbTipoAfiliado').attr('disabled','disabled');	   	   
 	   $("#cmbTipoAfiliado option[value='3']").attr('selected', 'selected');	
	}
	else if( document.getElementById('cmbNombreEntidad').options[document.getElementById('cmbNombreEntidad').selectedIndex].id =='S'  ){
	   $('#drag'+ventanaActual.num).find('#txtTipoUsuario').val('SUBSIDIADO');
	   $('#drag'+ventanaActual.num).find('#txtCarnet').attr('disabled','');
	   $('#drag'+ventanaActual.num).find('#cmbTipoAfiliado').attr('disabled','disabled');	   
 	   $("#cmbTipoAfiliado option[value='3']").attr('selected', 'selected');	
	}
	else if( document.getElementById('cmbNombreEntidad').options[document.getElementById('cmbNombreEntidad').selectedIndex].id =='V'  ){
	   $('#drag'+ventanaActual.num).find('#txtTipoUsuario').val('SUBSIDIADO');
	   $('#drag'+ventanaActual.num).find('#txtCarnet').attr('disabled','');
	   $('#drag'+ventanaActual.num).find('#cmbTipoAfiliado').attr('disabled','disabled');	   
 	   $("#cmbTipoAfiliado option[value='3']").attr('selected', 'selected');	
	}
	
	else if( document.getElementById('cmbNombreEntidad').options[document.getElementById('cmbNombreEntidad').selectedIndex].id =='C'  ){
	   $('#drag'+ventanaActual.num).find('#txtTipoUsuario').val('CONTRIBUTIVO');
	   $('#drag'+ventanaActual.num).find('#txtCarnet').attr('disabled','');
	   $('#drag'+ventanaActual.num).find('#cmbTipoAfiliado').attr('disabled','');	   
 	   $("#cmbTipoAfiliado option[value='1']").attr('selected', 'selected');	

      // $("#cmbTipoAfiliado").find("option[value='3']").remove();
   }
	 
}

function tipoDeAfiliado(){ 
	
    if( document.getElementById('cmbNombreEntidad').options[document.getElementById('cmbNombreEntidad').selectedIndex].id =='C' &&   $('#drag'+ventanaActual.num).find('#cmbTipoAfiliado').val()=='3'){
		alert("El tipo de afiliado no puede ser NINGUNO");	
		$("#cmbTipoAfiliado option[value='00']").attr('selected', 'selected');
	}

}


var datos_a_mandar="";
function iniciarConsulta(){

   	datos_a_mandar="accion=iniciarConsulta";
	datos_a_mandar=datos_a_mandar+"&tipoIdPaciente="+$('#drag'+ventanaActual.num).find('#tipoIdPacienteCita').val();
	datos_a_mandar=datos_a_mandar+"&idPaciente="+$('#drag'+ventanaActual.num).find('#idPacienteCita').val();	
    alert(datos_a_mandar);	
   	
	varajaxInit=crearAjax();
    varajaxInit.open("POST",'/clinica/paginas/accionesXml/modificar_xml.jsp',true);
	varajaxInit.onreadystatechange=respuestaModificarInicioConsulta;
	varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxInit.send(datos_a_mandar);
   
   
}
function respuestaModificarInicioConsulta(){
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('respuesta')[0].firstChild.data=='true'){
				  alert("Empieza la atenci�n a este paciente");

			  }else{
				  alert("No puede empiezar la atenci�n a este paciente");
			     }
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}
function presentarEventosAsociarAdministrar(){ 
   if($('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html()!=''){
     // para que por defecto busque el proceso y  sus respectivas nc
     $("#cmbBusproceso2 option[value='"+$.trim($('#drag'+ventanaActual.num).find('#cmbBusproceso').val())+"']").attr('selected', 'selected');
     cargarNoConformidades($.trim($('#drag'+ventanaActual.num).find('#cmbBusproceso').val()),'cmbBusNoConformidades2',$.trim($('#drag'+ventanaActual.num).find('#cmbBusproceso').val()), '/clinica/paginas/accionesXml/cargarNoConformidades_xml.jsp'); 
     buscarJuan('administrarNoConformidadAsociar', '/clinica/paginas/accionesXml/buscar_xml.jsp'); 		
   }
   else alert('Debe primero guardar un plan de acci�n, para luego relacionar otros eventos') 
}
function presentarTareasAsociadas(){ 
   if($('#drag'+ventanaActual.num).find('#lblIdPlanAccion').html()!=''){
		//listadoTabla('tareasAjenasAsociar');
		listadoTabla('listAsociarEventosTareas');
   }
   else alert('Debe primero guardar un plan de acci�n, para luego relacionar tareas') 
}





function presentarEventosAsociarReportar(){ 
   if($('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html()!=''){
     // para que por defecto busque el proceso y  sus respectivas nc
//     $("#cmbBusproceso2 option[value='"+$.trim($('#drag'+ventanaActual.num).find('#cmbBusproceso').val())+"']").attr('selected', 'selected');
   //  cargarNoConformidades($.trim($('#drag'+ventanaActual.num).find('#cmbBusproceso').val()),'cmbBusNoConformidades2',$.trim($('#drag'+ventanaActual.num).find('#cmbBusproceso').val()), '/clinica/paginas/accionesXml/cargarNoConformidades_xml.jsp'); 
     buscarJuan('administrarNoConformidadAsociar', '/clinica/paginas/accionesXml/buscar_xml.jsp'); 		
   }
   else alert('Debe primero guardar un plan de acci�n, para luego relacionar otros eventos') 
}
function presentarSeguimientoEventosAdverso(){ 

      buscarJuan('administrarSeguimientoEAAsociar', '/clinica/paginas/accionesXml/buscar_xml.jsp'); 		
}


function presentarPersonasPlanDeAccion(){ 
   if($('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html()=='' ){ 
	   $('#drag'+ventanaActual.num).find('#divBuscar4').hide();
	   $('#drag'+ventanaActual.num).find('#divBuscar5').show(); 
   }
   else if($('#drag'+ventanaActual.num).find('#lblEventoPadreAsocia').html()=='-' || $('#drag'+ventanaActual.num).find('#lblEventoPadreAsocia').html()==''  ){
	   $('#drag'+ventanaActual.num).find('#divBuscar4').hide();
	   $('#drag'+ventanaActual.num).find('#divBuscar5').show(); 
   }
   else {
	   $('#drag'+ventanaActual.num).find('#divBuscar4').show();   
	   $('#drag'+ventanaActual.num).find('#divBuscar5').hide();
   }

	   buscarJuan('listPersonasPlanAccion', '/clinica/paginas/accionesXml/buscar_xml.jsp');    
}








 







function presentarTablaListadoST2(casoList){ 
    ancho= ($('#drag'+ventanaActual.num).find("#divEditar").width())*92/100;

	var pag='/clinica/paginas/accionesXml/buscar_xml.jsp';
		valores_a_mandar=pag+"?accion="+casoList;
		valores_a_mandar=valores_a_mandar+"&tipoServic="+$('#drag'+ventanaActual.num).find('#tipoServic').val();
		valores_a_mandar=valores_a_mandar+"&tipoIdPaciente="+$('#drag'+ventanaActual.num).find('#tipoIdPacienteCita').val();
		valores_a_mandar=valores_a_mandar+"&idPaciente="+$('#drag'+ventanaActual.num).find('#idPacienteCita').val();	

		$('#drag'+ventanaActual.num).find('#'+casoList).jqGrid({  
		                url:valores_a_mandar, 
						 //url:pag+"?accion=controlescredesa", 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Tipo','Fecha Hora','idTipo','Examen','descripciones','tipoIdProfes','idProfes','Profesional' ], 
						 colModel :[ 
						   {name:'tipo',index:'tipo',hidden:true},
						   {name:'fechaHora', index:'fechaHora', width:((ancho*10)/100), align:'left'},
						   {name:'idTipoExa', index:'idTipoExa',hidden:true},  						   						   
						   {name:'examen', index:'examen', width:((ancho*20)/100), align:'left'},  
						   {name:'descripciones', index:'descripciones', width:((ancho*50)/100), align:'left'},  						   
						   {name:'tipoIdProfes', index:'tipoIdProfes', hidden:true},  						   
						   {name:'idProfes', index:'idProfes', align:'left',hidden:true},  
						   {name:'nomProfesional', index:'nomProfesional', width:((ancho*20)/100),  align:'left'},  

							],  
						 pager: jQuery('#pager'+casoList), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'FechaHora', 
						 sortorder: "FechaHora", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 ondblClickRow: function(rowid) { 										 
						 var datosDelRegistro = jQuery("#"+casoList).getRowData(rowid);   //trae los datos de esta fila con este idrow   
						 },
						 ondblClickRow: function(rowid) {
								var datosDelRegistro = jQuery('#drag'+ventanaActual.num).find('#'+casoList).getRowData(rowid);   
								if($.trim(datosDelRegistro.tipo)!='0'){  //si es de tipo  0 no se puede modificar porque pertenece al historia
										$('#drag'+ventanaActual.num).find('#cmb'+casoList).val(datosDelRegistro.idTipoExa);
										$('#drag'+ventanaActual.num).find('#txtDesc'+casoList).val(datosDelRegistro.descripciones);

										bandblclick=true;
								}else alert('El registro seleccionado es de s�lo lectura y no puede ser modificado.');	
						 },
						 height: 120,
						 width: ancho,
						// imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 //caption: 'Resultados'
					 //});
			});//.navGrid("#pagerAntF",{edit:false,add:false,del:false});	
}

function listadoTabla(caso){ 
    var lastsel2;
	var pag='/clinica/paginas/accionesXml/buscar_xml.jsp';
    ancho= ($('#drag'+ventanaActual.num).find("#divEditar").width())-50;	
	switch (caso){

        case 'listPlanAccion': 	
        valores_a_mandar=pag+"?accion="+caso;
	    valores_a_mandar=valores_a_mandar+"&sidx=1&IdEvento="+$('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html();
		//alert(valores_a_mandar);
		$('#drag'+ventanaActual.num).find('#'+caso).jqGrid({  
			url:valores_a_mandar, 
			 //url:pag+"?accion=controlescredesa", 
			 datatype: 'xml', 
			 mtype: 'GET', 
			 colNames:['Tipo','idISO','Caracter','CaracterDesc','C�mo realizar la acci�n o tarea','Cuando','quienRealizaAccion','NombQuienRealizaAccion' ], 
			 colModel :[ 
			   {name:'tipo',index:'tipo',hidden:true},
			   {name:'idISO',index:'idISO',hidden:true},
			   {name:'Caracter', index:'Caracter',hidden:true}, 
			   {name:'CaracterDesc', index:'CaracterDesc',  width:((ancho*10)/100), align:'left'},
			   {name:'RealizarAcciones', index:'RealizarAcciones', width:((ancho*55)/100), align:'left'}, 
			   {name:'FechaCuando', index:'FechaCuando',  width:((ancho*10)/100), align:'left'},  						   
			   {name:'quienRealizaAccion', index:'quienRealizaAccion',hidden:true}, 
			   {name:'NombQuienRealizaAccion', index:'NombQuienRealizaAccion',  width:((ancho*15)/100),  align:'left'},  						   
				],  
			 pager: jQuery('#pager'+caso), 
			 rowNum:10, 
			 rowList:[20,50,100], 
			 sortname: 'FechaHora', 
			 sortorder: "FechaHora", 
			 viewrecords: true, 
			 hidegrid: false, 
									 
			 ondblClickRow: function(rowid) { 										 
			 var datosDelRegistro = jQuery("#"+caso).getRowData(rowid);   //trae los datos de esta fila con este idrow   
			 },
			 ondblClickRow: function(rowid) {
					var datosDelRegistro = jQuery('#drag'+ventanaActual.num).find('#'+caso).getRowData(rowid);   
					if($.trim(datosDelRegistro.tipo)!='0'){  //si es de tipo  0 no se puede modificar porque pertenece al historia
							$('#drag'+ventanaActual.num).find('#cmbCaracter').val(datosDelRegistro.Caracter);
							$('#drag'+ventanaActual.num).find('#txtRealizarAcciones').val(datosDelRegistro.RealizarAcciones);										
							$('#drag'+ventanaActual.num).find('#txtFechaCuando').val(datosDelRegistro.FechaCuando);
							$('#drag'+ventanaActual.num).find('#cmbQuienRealizaAccion').val(datosDelRegistro.quienRealizaAccion);

							bandblclick=true;
					}else alert('El registro seleccionado es de s�lo lectura y no puede ser modificado.');	
			 },
			 height: 180,
			 width: ancho-50,
			// imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
			 //caption: 'Resultados'
		 //});
			});//.navGrid("#pagerAntF",{edit:false,add:false,del:false});		
		    $('#drag'+ventanaActual.num).find("#"+caso).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 		
	
		break;	
 		
		
    
			
       
	
	}
}	


//---------------------------------------------LAS FUNCIONES ST2 SIRBEN PARA EDITAR AUTOMATICAMENTE LOS LISTADOS QUE TIENEN COMBO Y DESCRIPCION INCLUIDOS CODIGOS Y PROFESIONALES

function modifiDatosListasST2(casoList){	
	if(bandblclick){
		var id = jQuery('#drag'+ventanaActual.num).find('#'+casoList).getGridParam('selrow');
		var ret = jQuery('#drag'+ventanaActual.num).find('#'+casoList).getRowData(id);
				if($('#drag'+ventanaActual.num).find('#cmb'+casoList).val()=='0') 
					alert('Seleccione un tipo');
				else if($('#drag'+ventanaActual.num).find('#txtDesc'+casoList).val()=='') 
					alert('Digite una descripci�n');
				else { 
					var vm='';

                    var su=jQuery('#drag'+ventanaActual.num).find('#'+casoList).setRowData(id,{descripcion:$('#drag'+ventanaActual.num).find('#txtDescripcion').val(),
					 tipo:'1',	 
					 fechaHora:$('#drag'+ventanaActual.num).find('#lblFechaAt').text()+ ' '+$('#drag'+ventanaActual.num).find('#lblHoraAt').text(),	 
					 idTipoExa:$('#drag'+ventanaActual.num).find('#cmb'+casoList).val(),
					 examen:$('#drag'+ventanaActual.num).find('#cmb'+casoList+' :selected').text(),
					 descripciones:$('#drag'+ventanaActual.num).find('#txtDesc'+casoList).val(),
		  
					 tipoIdProfes:$('#drag'+ventanaActual.num).find('#hidtipo_id_profesio').val(),
					 idProfes:$('#drag'+ventanaActual.num).find('#hidid_profesio').val(),
					 nomProfesional:$('#drag'+ventanaActual.num).find('#hid_nombre_profesio').val()	 
					});//amount:"333.00",tax:"33.00",total:"366.00",note:"<img src='images/user1.gif'/>"});
					if(su){ //alert("Se modific� el registro satisfactoriamente"); 
						bandblclick=false;
					}else alert("No se puede modificar el registro");
		        }
	}
}	
function quitarDatosListasST2(lista){ 

	var id = jQuery('#drag'+ventanaActual.num).find('#'+lista).getGridParam('selrow');
	if( id != null ){ 
		var ret = jQuery('#drag'+ventanaActual.num).find('#'+lista).getRowData(id);		
		jQuery("#"+lista).delRowData(id);	
		iCant = jQuery('#drag'+ventanaActual.num).find('#'+lista).getRecords();

/*		for(var i=1; i<=iCant; i++){
		  while(jQuery('#drag'+ventanaActual.num).find('#'+lista).delRowData(i)){continue;}
		}	
*/		
		
		
	}  
	//else alert("Error!");
	
	

	
}	

function limpiarDatosListasST2(combo,descrip){

   $('#drag'+ventanaActual.num).find('#'+descrip).text("");	 
   $('#drag'+ventanaActual.num).find('#'+descrip).val("");
   $("#"+combo+"  option[value='00']").attr('selected', 'selected');	
}
//-------------------------------------------------------------------------------fin funciones ST2
function limpiarRegistrosDeListados(){

				  var ids = jQuery("#"+arg).getDataIDs();
				  for(var i=0;i<ids.length;i++){ 
					  var c = ids[i];
					  var datosDelRegistro = jQuery("#"+arg).getRowData(c);  
					 // if($.trim(datosDelRegistro.tipo)=='1'){
						 if( $.trim(datosDelRegistro.IdPersona) == $('#drag'+ventanaActual.num).find('#cmbPersonaQuienRealizaAccion').val() ){	
						  ban=1;
						  alert('La persona ya existe en el listado');
					 }
				  }
 }				  
//---------------------------------------------LAS FUNCIONES ST GRAL SIRVEN PARA PARA EDITAR AUTOMATICAMENTE LOS LISTADOS QUE TIENEN VARIOS ELEMENTOS IMPUT
function agregarElementoListGral(arg){   
 ban=0;
   switch(arg){
	 case 'listDxIntegralInicial':	
			if($('#drag'+ventanaActual.num).find('#txtDxIntegralInicial').val()=='') 
					alert('Digite su Objetivo');
			else {
			   //var datoscadena=$('#drag'+ventanaActual.num).find('#txtDescExaPeriodont').val().split('-');	 
			   var ids = jQuery("#"+arg).getDataIDs();
			   cant = ids.length;		 
			   proximo=cant+1;    	// pilas toca asi, o si no hay problemas en el momento de consultar y agregar uno nuevo
			   var datarow = {		 	 
			   DescripDxIni:$('#drag'+ventanaActual.num).find('#txtDxIntegralInicial').val(),
			   idServicio:'01',
			   Servicio:'Psicologia',			   
			   idProfesional:'CC984251',
			   nomProfesional:'Paola Arturo',
			   };
			   var su=jQuery('#'+arg).addRowData(proximo,datarow);
			   $('#drag'+ventanaActual.num).find('#txtDxIntegralInicial').val("");
	  		 } 
	  break; 	   
	
  
     	  
	  
   }
}


	
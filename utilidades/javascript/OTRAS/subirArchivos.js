// JavaScript Document

var archivosVarios = '';

function comprobarExcelFacturacion() {
    if (verificarCamposGuardar('subirExcelFacturacion')) {
        archivosVarios = "excelFacturacion";
        var nomJas = document.getElementById("fileExcel").value;
        if (nomJas.substring(nomJas.length - 4, nomJas.length) == "xlsx"
            || nomJas.substring(nomJas.length - 3, nomJas.length) == "xls") {

            comprobarSiElCandadoEstaAbierto('txt', 554);

        } else {
            alert("El archivo a enviar no es EXCEL o el campo esta vacio");
            document.getElementById("fileExcel").focus();
        }
    }
}

function subirExcelFacturacion() {
    var fileSize = $('#fileExcel')[0].files[0].size; 
    var archivoTamano = Math.round(parseFloat(fileSize / 1024).toFixed(2));
    var nomJas = document.getElementById("fileExcel").value;
    var ruta = "";
    ruta = "evento=excelFacturacion" + "&subCarpeta=excel" + '&usuario_crea=' + LoginSesion() + '&archivoTamano=' + archivoTamano;
    ruta = ruta + "&nombreArchivo=excel" + "&var1=" + valorAtributo('txtIdPlan');

    document.forms['formExcel'].action = '/clinica/servlet/UPC?' + ruta;
    console.log("ruta: " + ruta);
    document.forms['formExcel'].submit();
}



function comprobarYSubirArchivos(opcionBoton) {
    archivosVarios = opcionBoton
    var nomJas = document.getElementById("fileUpc").value;
    if (nomJas.substring(nomJas.length - 3, nomJas.length) == "pdf") { // si el archivo es de extencion pdf
        if (!valorAtributo('cmbTipoArchivoSubir') == 'ayudaDx' || !valorAtributo('cmbTipoArchivoSubir') == 'orden_programacion')
            comprobarSiElCandadoEstaAbierto('txt', 542)
        else
            comprobarSiElCandadoEstaAbierto('txt', 554)
    } else {
        alert("El archivo a enviar no es PDF o el campo está vacio");
        document.getElementById("fileUpc").focus();
    }
}


function comprobarYSubirArchivosS(opcionBoton) {
    archivosVarios = opcionBoton
    var nomJas = document.getElementById("fileUpcs").value;
    if (nomJas.substring(nomJas.length - 3, nomJas.length) == "pdf") { // si el archivo es de extencion pdf
        if (!valorAtributo('cmbTipoArchivoSubir') == 'ayudaDx' || !valorAtributo('cmbTipoArchivoSubir') == 'orden_programacion')
            comprobarSiElCandadoEstaAbierto('txt', 542)
        else
            comprobarSiElCandadoEstaAbierto('txt', 554)
    } else {
        alert("El archivo a enviar no es PDF o el campo está vacio");
        document.getElementById("fileUpcs").focus();
    }
}

function comprobarSiElCandadoEstaAbierto(elementoDestino, idQueryCombo) {   //alert('idQueryCombo='+idQueryCombo)
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=0";
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacomprobarSiElCandadoEstaAbierto;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacomprobarSiElCandadoEstaAbierto() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');
            if (c.length > 0) {
                if (c[0].firstChild.data == 'CERRADO') {
                    alert('4.UN USUARIO ESTA SUBIENDO ARCHIVO, POR FAVOR ESPERE E INTENTE NUEVAMENTE')

                } else {
                    if (archivosVarios == 'no_varios') {
                        subirArchivoAdjunto()
                    }
                    else if (archivosVarios === "excelFacturacion") {
                        subirExcelFacturacion()
                    } else if (archivosVarios === "orden_programacion") {
                        subirArchivoApoyoDiagnostico();
                    }
                    else if(archivosVarios == "anexos"){                        
                        subirArchivoAdjuntosAnexos()
                    }
                    else {
                        subirArchivoAdjuntosVarios()
                    }
                }
            } else {
                alert('Problema al cargar respuestacargarElementoGRAL..')
            }
        } else {
            alert("Problemas de conexion con servidor: respuestacargarElementoGRAL. " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}
/*función para subir archivos varios al sistema*/

function subirArchivoAdjuntosVarios() {

    var fileSize = $('#fileUpc')[0].files[0].size;
    var archivoTamano = Math.round(parseFloat(fileSize / 1024).toFixed(2));

    if (valorAtributo('cmbIdTipoArchivosVarios') != '') {
        var ruta = "";
        ruta = "evento=crear" +
            "&subCarpeta=otros&usuario_crea=" + LoginSesion() +
            "&id_paciente=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] +
            "&tipo_id=" + valorAtributo("lblPacienteAdjuntos").split("-")[1] +
            "&identificacion=" + valorAtributo("lblPacienteAdjuntos").split("-")[2] +
            "&id_cita=" + valorAtributo("lblIdCitaAdjuntos") +
            "&id_admision=" + valorAtributo("lblIdAdmisionAdjuntos") +
            "&id_evolucion=" + valorAtributo("lblIdEvolucionAdjuntos");
        ruta = ruta + "&nombreArchivo=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] + valorAtributo('cmbIdTipoArchivosVarios') + "&var1=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] + "&var2=" + valorAtributo('cmbIdTipoArchivosVarios') + "&var3=" + valorAtributo('txtObservacionAdjuntosVarios') + '&archivoTamano=' + archivoTamano;

        document.forms['formUpc'].action = '/clinica/servlet/UPC?' + ruta;
        document.forms['formUpc'].submit();

    }
}

function subirArchivoAdjuntosAnexos() {

    var fileSize = $('#fileUpcs')[0].files[0].size;
    var archivoTamano = Math.round(parseFloat(fileSize / 1024).toFixed(2));

    console.log(archivoTamano)

    if (valorAtributo('cmbIdTipoArchivosAnexo') != '') {
        var ruta = "";
        ruta = "evento=crear" +
            "&subCarpeta=anexos&usuario_crea=" + LoginSesion() +
            "&id_paciente=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] +            
            "&id_evolucion=" + valorAtributo("lblIdEvolucionAdjuntos");
        ruta = ruta + "&nombreArchivo=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] + valorAtributo('cmbIdTipoArchivosAnexo') + "&var1=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] + "&var2=" + valorAtributo('cmbIdTipoArchivosAnexo') + "&var3=" + valorAtributo('txtObservacionAdjuntosVarios') + '&archivoTamano=' + archivoTamano;

        document.forms['formUpc'].action = '/clinica/servlet/UPC?' + ruta;
        document.forms['formUpc'].submit();
    }
    else{
        alert('SELECCIONE UN TIPO DE ANEXO');
    }
}

function subirArchivoAdjuntosFirmas() {
    alert(9999)

    var fileSize = $('#fileUpc')[0].files[0].size;
    var archivoTamano = Math.round(parseFloat(fileSize / 1024).toFixed(2));

    alert('archivoTamano::' + archivoTamano)


    if (valorAtributo('cmbIdTipoArchivosVarios') != '') {
        var nomJas = document.getElementById("fileUpc").value;
        var ruta = "";

        ruta = "evento=crear" + "&subCarpeta=otros&usuario_crea=" + LoginSesion();
        ruta = ruta + "&nombreArchivo=" + valorAtributoIdAutoCompletar('txtIdBusPaciente') + valorAtributo('cmbIdTipoArchivosVarios') + "&var1=" + valorAtributoIdAutoCompletar('txtIdBusPaciente') + "&var2=" + valorAtributo('cmbIdTipoArchivosVarios') + "&var3=" + valorAtributo('txtObservacionAdjuntosVarios') + '&archivoTamano=' + archivoTamano;
        var comboOtros = document.getElementById("cmbIdTipoArchivosVarios");
        var opcionSelect = comboOtros.options[comboOtros.selectedIndex].text;


        document.forms['formUpc'].action = '/clinica/servlet/UPC?' + ruta;
        document.forms['formUpc'].submit();
    }

}


/*fin de la función otros archivos adjuntos*/

function subirArchivoAdjunto() {  // para  enviar archivo  UPC

    var fileSize = $('#fileUpc')[0].files[0].size;
    var archivoTamano = Math.round(parseFloat(fileSize / 1024).toFixed(2));
    var nomJas = document.getElementById("fileUpc").value;
    var ruta = "";
    ruta = "evento=crear" + "&subCarpeta=" + valorAtributo('cmbTipoArchivoSubir') + '&usuario_crea=' + LoginSesion() + '&archivoTamano=' + archivoTamano;

    switch (valorAtributo('cmbTipoArchivoSubir')) {
        case 'fisico':
            if (valorAtributo('txtIdBusPaciente') != '') {
                ruta = ruta + "&nombreArchivo=" + valorAtributoIdAutoCompletar('txtIdBusPaciente') + "&var1=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
            } else {
                ruta = "";
                alert('SELECCIONE PACIENTE')
            }
            break;
        case 'identificacion':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') != '') {
                ruta = ruta + "&nombreArchivo=" + valorAtributo('cmbTipoId') + valorAtributo('txtIdentificacion') + "&var1=" + valorAtributoIdAutoCompletar('txtIdBusPaciente') + "&var2=" + valorAtributo('cmbTipoId') + "&var3=" + valorAtributo('txtIdentificacion');
            } else {
                ruta = "";
                alert('SELECCIONE PACIENTE')
            }
            break;
        case 'remision':
            if (valorAtributo('lblIdAgendaDetalle') != '') {
                ruta = ruta + "&nombreArchivo=" + valorAtributo('lblIdAgendaDetalle') + "&var1=" + valorAtributo('lblIdAgendaDetalle');
                console.log(ruta)
            } else {
                ruta = "";
                alert('SELECCIONE CITA')
            }
            break;
        case 'carnet':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') != '') {
                ruta = ruta + "&nombreArchivo=" + valorAtributo('lblIdAgendaDetalle') + "&var1=" + valorAtributo('lblIdAgendaDetalle') + "&var2=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
            } else {
                ruta = "";
                alert('SELECCIONE PACIENTE')
            }
            break;
        case 'ayudaDx':
            if (valorAtributo('txtIdEsdadoDocumento') == 1) {
                ruta = "";
                alert('EL ESTADO DEL DOCUMENTO NO PERMITE ADJUNTAR .1')
            } else if (valorAtributo('txtIdEsdadoDocumento') == 8) {
                ruta = "";
                alert('EL ESTADO DEL DOCUMENTO NO PERMITE ADJUNTAR .8')
            } else {
                if (valorAtributo('txtIdProfesionalElaboro') == IdSesion()) {

                    if (valorAtributo('lblIdDocumento') != '') {
                        ruta = ruta + "&nombreArchivo=" + valorAtributo('lblIdDocumento') + valorAtributo('cmbIdSitioAdjunto') + "&var1=" + valorAtributo('lblIdDocumento') + "&var2=" + valorAtributo('lblIdPaciente') + "&var3=" + valorAtributo('txtObservacionAdjunto') + "&var4=" + valorAtributo('cmbIdSitioAdjunto');
                    } else {
                        ruta = "";
                        alert('SELECCIONE FOLIO')
                    }
                } else {
                    ruta = "";
                    alert('USTED NO ES EL AUTOR DE ESTE FOLIO \n NO PODRA ELIMINAR')
                }
            }
            break;
    }
    if (ruta != '') {

        if (valorAtributo('cmbTipoArchivoSubir') == 'ayudaDx') {
            document.forms['formUpc'].action = '/clinica/servlet/UPC2?' + ruta;
        } else {
            document.forms['formUpc'].action = '/clinica/servlet/UPC?' + ruta;
        }
        console.log("ruta: " + ruta);
        document.forms['formUpc'].submit();
    }
}


function subirArchivoApoyoDiagnostico() {  // para  enviar archivo  UPC
    var fileSize = $('#fileUpc')[0].files[0].size;
    var archivoTamano = Math.round(parseFloat(fileSize / 1024).toFixed(2));
    var nomJas = document.getElementById("fileUpc").value;
    var ruta = "";
    ruta = "evento=crear" + "&subCarpeta=" + 'ordenProgramacion' + '&usuario_crea=' + LoginSesion() + '&archivoTamano=' + archivoTamano;

    ruta = ruta + "&nombreArchivo=" + valorAtributo('txtIdPlanEvolucion') + "&var1=" + valorAtributo('txtIdBusPaciente') + "&var2=" + valorAtributo('txtObservacionAdjunto');

    document.forms['formUpc'].action = '/clinica/servlet/UPC2?' + ruta;
   
    console.log("ruta: " + ruta);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", '/clinica/servlet/UPC2?' + ruta);
    xhr.onload = function (event) {
        var respuesta = new String(event.target.response);
        respuesta = respuesta.replace('<SCRIPT language=javascript>','');
        respuesta = respuesta.replace('</SCRIPT>','');
        console.log(respuesta);
        eval(respuesta)
        if(respuesta.includes("window.open")){
            modificarCRUD('actualizarOrdenProgramacion')
        }
    };
    var formData = new FormData(document.getElementById("formUpc"));
    xhr.send(formData);
}




function eliminarArchivoAdjunto() {  // para  eliminar archivo  UPC

    if (confirm('Seguro de Eliminar archivo? ')) {
        switch (tabActivo) {
            case 'divArchivosAdjuntos':
                //if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=elimina' + '&subCarpeta=fisico';
                document.forms['formUpcElimina'].submit();
                //} else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA ELIMINAR')
                break;
            case 'divListadoIdentificacion':
                //if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=elimina' + '&subCarpeta=identificacion';
                document.forms['formUpcElimina'].submit();
                //} else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA ELIMINAR')
                break;
            case 'divListadoRemision':
                if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=elimina' + '&subCarpeta=remision';
                    document.forms['formUpcElimina'].submit();
                } else
                    alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA ELIMINAR');
                break;
            case 'divListadoCarnet':
                if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=elimina' + '&subCarpeta=carnet';
                    document.forms['formUpcElimina'].submit();
                } else
                    alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA ELIMINAR')
                break;
            case 'divListadoAyudasDiagnosticas':
                if (valorAtributo('txtIdEsdadoDocumento') == 1) {
                    alert('EL ESTADO DEL DOCUMENTO NO PERMITE ELIMINAR.1')
                } else if (valorAtributo('txtIdEsdadoDocumento') == 8) {
                    alert('EL ESTADO DEL DOCUMENTO NO PERMITE ELIMINAR.8')
                } else {
                    //if(valorAtributo('txtIdProfesionalElaboro')==IdSesion()){ 
                    if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                        document.forms['formUpcElimina'].action = "/clinica/servlet/UPC2?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=elimina' + '&subCarpeta=ayudaDx';
                        document.forms['formUpcElimina'].submit();

                    } else
                        alert('USTED NO ES EL AUTOR DE ESTE FOLIO \n NO PODRA ELIMINAR')

                }
                break;
            case 'divListadoOtrosDocumentos':
                if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=elimina' + '&subCarpeta=otros';
                    document.forms['formUpcElimina'].submit();
                } else
                    alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA ELIMINAR')
                break;

            case 'divAyudasDiagnosticas':
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC2?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=elimina' + '&subCarpeta=ayudaDx';
                document.forms['formUpcElimina'].submit();
                break;

            case 'divSubirArchivo':
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC2?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=elimina' + '&subCarpeta=ordenProgramacion';
                document.forms['formUpcElimina'].submit();
                break;
        }
    }

}

function cerrarVentanitaArchivosAdjuntos() {
    ocultar('divVentanitaArchivosAdjuntos');
    switch (tabActivo) {
        case 'divArchivosAdjuntos':
            buscarHistoria('listArchivosAdjuntos')
            break;
        case 'divListadoIdentificacion':
            buscarHistoria('listArchivosAdjuntosIdentificacion')
            break;
        case 'divListadoRemision':
            buscarHistoria('listArchivosAdjuntosRemision')
            break;
        case 'divListadoCarnet':
            buscarHistoria('listArchivosAdjuntosCarnet')
            break;
        case 'divListadoAyudasDiagnosticas':
            buscarHistoria('listAyudasDiagnost')
            break;
        case 'divListadoOtrosDocumentos':
            buscarHistoria('listArchivosAdjuntosVarios')
            break;

    }
}




function dibujarCanvas_() {

    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext("2d");
    var textarea = document.getElementById('code');
    var reset = document.getElementById('reset');
    var edit = document.getElementById('edit');
    var code = textarea.value;

    ctx.clearRect(0, 0, 300, 100);
    ctx.fillStyle = 'blue';

    eval(textarea.value);
}


function dibujarCanvas() {
    alert('0000')
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    // Dibujamos algo sencillo en el Canvas para exportarlo
    ctx.fillStyle = "rgb(255,0,0)";
    ctx.fillRect(5, 5, 50, 50);

    ctx.fillStyle = "rgb(0,255,0)";
    ctx.fillRect(8, 8, 50, 50);

}

function pasarImg() { //alert(7777)
    var img = document.getElementById("laimagen");
    //  var png = document.getElementById("png");
    /*	  png.addEventListener("click",function(){	
              img.src = canvas.toDataURL("image/png");	
          },false);	*/
    img.src = canvas.toDataURL("image/png");
}


function ponerImagen() {
    //constants
    var MAX_WIDHT = 200,
        MAX_HEIGHT = 200;
    var URL = window.webkitURL || window.URL;
    var inputFile = document.getElementById('fileUpc');
    alert(URL)
    alert(inputFile)
}

function cambiar() {
    var URL = window.webkitURL || window.URL;
    //      var file = event.target.files[0];

    //elements
    var canvas = document.getElementById('preview'),
        ctx = canvas.getContext('2d'),
        url = URL.createObjectURL(file);

    var img = new Image();
}

function loadin() {

    var width = img.width,
        height = img.height;

    //resize
    if (width > height) {
        if (width > MAX_WIDHT) {
            height *= MAX_WIDHT / width;
            width = MAX_WIDHT;
        }
        else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
    }

    canvas.width = width;
    canvas.height = height;

    ctx.drawImage(img, 0, 0, width, height);

    //Form data (POST) 

    console.log(file);

    //name
    var imageName = id('imageName');
    imageName.value = file.name;

    //contentType
    var contentType = id('contentType');
    contentType.value = file.type;

    //image data
    var imageData = id('imageData'),
        dataUrl = id('preview').toDataURL('image/png').replace('data:image/png;base64,', '');

    imageData.value = dataUrl;
    img.src = url;

}




var wrapper = document.getElementById("signature-pad");
/*
var canvas = wrapper.querySelector("canvas");
var signaturePad = new SignaturePad(canvas, {
  backgroundColor: 'rgb(255, 255, 255)'
});
function probando()
{
    alert('hola')
    if (signaturePad.isEmpty()) {
    alert("Please provide a signature first.");
  } else {
    //var dataURL = signaturePad.toDataURL();
    var img = document.getElementById("imgCanvas");
    var png = document.getElementById("png");
    img.src = canvas.toDataURL("image/png");

    }
 }

function enviarImg()
{
    window.open("http://181.48.155.146:8383/clinica/paginas/hc/prueba.jsp");
}

function dibujarCanvas2() {
    alert('0000')
    var canvas = document.getElementById("miicanvas");
    var ctx = canvas.getContext("2d");
    // Dibujamos algo sencillo en el Canvas para exportarlo
    ctx.fillStyle = "rgb(255,0,0)";
    ctx.fillRect(5,5,50,50);

    ctx.fillStyle = "rgb(0,255,0)";
    ctx.fillRect(8,8,50,50);

}

var canvasC = document.getElementById("miicanvas");
var w = window.innerWidth;
var h = window.innerHeight;

canvasC.width = w;
canvasC.height = h/2.5;

var signaturePad = new SignaturePad(canvasC,{
    dotSize: 1
});
function subirCanvas()
{
    alert('subir')
       var imageURI = signaturePad.toDataURL();
    document.getElementById("preview").src = imageURI;
}

function subirCanvasAimg()
{
    alert('mover')
   signaturePad.clear();
}




var canvas3 = document.getElementById("miicanvas");
var ctx = canvas3.getContext("2d");
ctx.fillStyle = "#FF0000";
ctx.fillRect(50, 25, 100, 50);
$( '#imagen' ).hide();

function guardar() {
    alert(111)
    var imagen = canvas3.toDataURL("image/png");
    /* Envío la petición XHR al servidor con los datos de la imagen
    $.ajax({
        url: "<?= $_SERVER['PHP_SELF'] ?>",
        method: 'post',
        data: { imagen: imagen},
    }).done(function(retorno) {
        alert(retorno);
        $( '#imagen' ).show();
    });
}

*/
// JavaScript para la programacion de los botones


/** funciones para la accion de busqueda*/

var idTablaReportesExcel ="";
var idListadoExcel ="";


function opcionesDeImprimirDoc(){ 
	mostrar('divVentanitaOpcImpresion')		
	
}
function opcionesDeImprimirDoc2(){     
	asignaAtributo('cmbOpcImpresion', 'Resumen',0); 
	caseImprimirDoc();
}
function caseImprimirDoc(){ 
  ocultar('divVentanitaOpcImpresion')	
  ocultar('divVentanitaPdf')
  switch (valorAtributo('cmbOpcImpresion')){	  
    case 'Resumen': 
  	    document.getElementById('documentoPDF').lastChild.removeChild(document.getElementById('idRevisionSistemas'));
  	    document.getElementById('documentoPDF').lastChild.removeChild(document.getElementById('idAntecedentesReport'));
  	    document.getElementById('documentoPDF').lastChild.removeChild(document.getElementById('idExamenFisicReport'));
		imprimirCC('documentoPDF')		
	break;
    case 'ResumenFacturacion': 
  	    document.getElementById('documentoPDF').lastChild.removeChild(document.getElementById('idRevisionSistemas'));   
  	    document.getElementById('documentoPDF').lastChild.removeChild(document.getElementById('idAntecedentesReport'));  
  	    document.getElementById('documentoPDF').lastChild.removeChild(document.getElementById('idExamenFisicReport'));  
  	    document.getElementById('documentoPDFInfoPaciente').lastChild.removeChild(document.getElementById('listPaciente3PDF'));	  	
  	    document.getElementById('idInfoTitulo').lastChild.removeChild(document.getElementById('idTrInfoTitulo'));	  		
  	    document.getElementById('idInfoTitulo2').lastChild.removeChild(document.getElementById('idTrInfoTitulo2'));	  					
  	    document.getElementById('idInfoTitulo3').lastChild.removeChild(document.getElementById('idTrInfoTitulo3'));	 				
  	    document.getElementById('idInfoTitulo4').lastChild.removeChild(document.getElementById('idTrInfoTitulo4'));	  
  	    document.getElementById('idInfoTituloEXAMEN').lastChild.removeChild(document.getElementById('idTrInfoTituloEXAMEN'));  			
		
  	   // document.getElementById('idInfoTitulo4').lastChild.removeChild(document.getElementById('idTrInfoTitulo5'));		
		
		borrarRegistrosTabla('listPlanPDF')	
        llenarTablaDesdeUnReporte(138,valorAtributo('lblIdDocumento'),6,'listPlanPDF')  					
		alert('Se imprimiran sin laboratorios')						
	 	imprimirCC('documentoPDF')		
	break;	
	case 'Todo':
		imprimirCC('documentoPDF')			
	break;
  }	
}



function borrarTablaImprimir(){
	borrarRegistrosTabla('idRevisionSistemas'); 
	borrarRegistrosTabla('idAntecedentesReport');
	borrarRegistrosTabla('idExamenFisicReport');	
}

function llenarTablaDeReporteExcelDispensar(idReporte,parametro1,cantColumnasQuery, tablaReportesExcel,idListExcel){ 

    if(verificarCamposGuardar('cerrarTransaccionesDocumentoInvUDReportesExcel')){	   
		 if(idReporte!='' & parametro1!=''  ){
		   cantColumnasP = cantColumnasQuery; 
		   idTablaReportesExcel =  tablaReportesExcel;	 
		   idListadoExcel = idListExcel;
		   varajaxInit=crearAjax();  
		   varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarReportes_xml.jsp',true);
		   varajaxInit.onreadystatechange=respuestallenarTablaDeReporteExcel;
		   
		   varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');	
		   valores_a_mandar="accion=listadoReporteExcel"; 
		   valores_a_mandar=valores_a_mandar+"&id="+idReporte;
		   valores_a_mandar=valores_a_mandar+"&parametro1="+parametro1;//+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
		   varajaxInit.send(valores_a_mandar); 
		 }
		 else alert('Falta seleccionar el Elemnto a Exportar')
    }
}

/*******************************************************************************************/
function llenarTablaDesdeUnReporteAnexo3(idReporte,parametro1,cantColumnasQuery, tablaReportesDestino){ 
   
 /*  if(idReporte!='' & parametro1!=''  ){*/
	 cantColumnasP = cantColumnasQuery; 
	 idTablaReportesExcel =  tablaReportesDestino;	 
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarReportes_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestallenarTablaDesdeReporteAnexo3;
	 
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');	
	 valores_a_mandar="accion=listadoReporteExcel"; 
	 valores_a_mandar=valores_a_mandar+"&id="+idReporte;
     valores_a_mandar=valores_a_mandar+"&parametro1="+parametro1;//+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
	 varajaxInit.send(valores_a_mandar); 
  /* }   else alert('Falta seleccionar el Elemnto a Exportar')*/
}

/*2- llamar */
function respuestallenarTablaDesdeReporteAnexo3(){   
	
	if(varajaxInit.readyState == 4 ){      
	 //   VentanaModal.cerrar();
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement; 

				  if(raiz.getElementsByTagName('rows')!=null){  
				  
					   totalRegistros=raiz.getElementsByTagName('c1').length;  	
					 

					   if(totalRegistros){    
						  agregarDatosTablaDeReporteBlanco(raiz,totalRegistros);	
						  opcionSincronic++;
						  imprimirSincronicoPdfAnexo3(opcionSincronic)						  
					   }
					   else{
						   alert("Sin datos en el reporte, cierre y vuelva a generarlo");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//	abrirVentana(260, 100);
			  //  VentanaModal.setSombra(true);
		}
}


/***************************************************************************************************/
/*1- llamar */
function llenarTablaDesdeUnReporte(idReporte,parametro1,cantColumnasQuery, tablaReportesDestino){ 
   
   if(idReporte!='' & parametro1!=''  ){
	 cantColumnasP = cantColumnasQuery; 
	 idTablaReportesExcel =  tablaReportesDestino;	 
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarReportes_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestallenarTablaDesdeReporte;
	 
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');	
	 valores_a_mandar="accion=listadoReporteExcel"; 
	 valores_a_mandar=valores_a_mandar+"&id="+idReporte;
     valores_a_mandar=valores_a_mandar+"&parametro1="+parametro1;//+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
	 varajaxInit.send(valores_a_mandar); 
   }
   else alert('Falta seleccionar el Elemnto a Exportar')
}

/*2- llamar */
function respuestallenarTablaDesdeReporte(){   
	
	if(varajaxInit.readyState == 4 ){      
	 //   VentanaModal.cerrar();
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement; 

				  if(raiz.getElementsByTagName('rows')!=null){  
				  
					   totalRegistros=raiz.getElementsByTagName('c1').length;  	
					 

					   if(totalRegistros){    
						  agregarDatosTablaDeReporteBlanco(raiz,totalRegistros);	
						 // opcionSincronic++;

						     recorrerImpresion();						

					   }
					   else{
						   alert("Sin datos en el reporte, cierre y vuelva a generarlo");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//	abrirVentana(260, 100);
			  //  VentanaModal.setSombra(true);
		}
}
/*3- llamar */
function agregarDatosTablaDeReporteBlanco(raiz, totalRegistros){    
         borrarRegistrosTabla(idTablaReportesExcel);	    
		 var total=0,numRow=-1, cantFilas=0, cantColumnas=0, estilo='inputExcel';
         tablabody=document.getElementById(idTablaReportesExcel).lastChild;
		 tablabody.className='inputBlanco';
		 tablabody.border = 1;		 
         
		 cantFilas = totalRegistros / cantColumnasP;
		 
		 if(cantFilas >1)

			 for(i=1;i<=cantFilas;i++){	 
				  Nregistro=document.createElement("TR");
				  Nregistro.id=i;
				  Nregistro.className=estilo;
											 
				  for(j=0;j<cantColumnasP;j++){   
					  numRow++;
					  TD=document.createElement("TD"); 
					  TD.innerHTML= raiz.getElementsByTagName('c1')[numRow].firstChild.data;

					  if(idTablaReportesExcel=='listPiePaginaPDF'){
					  	TD.style.fontStyle = "italic";
					  }


					  Nregistro.appendChild(TD);							 
				  }
				  estilo='inputBlanco';
				  tablabody.appendChild(Nregistro); 




			 }
	      else{  borrarRegistrosTabla(idTablaReportesExcel);	
				  Nregistro=document.createElement("TR");
				  Nregistro.id=i;
				  Nregistro.className='inputRojoAlerta';
					  TD=document.createElement("TD"); 
					  TD.innerHTML= 'NO';
					  TD.align='CENTER'
					  Nregistro.appendChild(TD);							 				  
				  tablabody.appendChild(Nregistro); 
				  
				  if(idTablaReportesExcel=='listDiagnosticosPDF' || idTablaReportesExcel =='listSignosVitalesPDF'){
					  habilitar('btnFinalizarDocumento',0)		
				  }
		  }
	
}
/***************************************************************************************************/
function agregarDatosTablaHistoricosAtencion(raiz, totalRegistros){
	 borrarRegistrosTabla(idTablaReportesExcel);	  
	 var total=0,numRow=-1, cantFilas=0, cantColumnas=0, estilo='inputExcel'; 
	 tablabody=document.getElementById(idTablaReportesExcel).lastChild; 
	 tablabody.className='inputBlancoHistAtenc';
	 tablabody.border = 1;		 
	 tablabody.setAttribute('width','100%');  
	 
	// cantColumnasPar = 13;// $('#drag'+ventanaActual.num).find('#CantColumnas').val(); 	
	 cantFilas = totalRegistros / cantColumnasP;	 	 
	 if(cantFilas >1)

	 for(i=1;i<=cantFilas;i++){	 
		  Nregistro=document.createElement("TR");
		  Nregistro.id='ID_TR'+i; 
		  if (i==1)  Nregistro.className='tituloHistoricoHis';
          else{ 
		      Nregistro.className='overs';		
		  }
		  for(j=0;j<cantColumnasP;j++){   
			  numRow++;
			  TD=document.createElement("TD"); 
			  
			  if(j==4 || j==6 || j==8 || j==10 || j==12 || j==13 || j==14 || j==15 || j==16 || j>17) {
				 TD.style.display = "none";
			  }		
			  TD.id='ID_TD'+j;			

			  if(j==17 && i!= 1){
				  
				  Ncampo1=document.createElement("IMG");
				  Ncampo1.setAttribute('src','/clinica/utilidades/imagenes/acciones/buscar.png');
				  Ncampo1.setAttribute('width','16');
				  Ncampo1.setAttribute('height','16');
				  Ncampo1.setAttribute('align','left');
				  Ncampo1.setAttribute('onclick',"sacarDatosOnClicVistaPrevia("+i+");");
				  TD.appendChild(Ncampo1);	
			  }
			  else{ 
			     TD.id='ID_TD'+j; 
			     TD.innerHTML = raiz.getElementsByTagName('c1')[numRow].firstChild.data;	
 		         if(i!=1)
				 TD.setAttribute('onclick',"sacarDatosOnClic("+i+");");	
			  }

			  Nregistro.appendChild(TD);							 
		  }
		  estilo='inputBlancoHistAtenc';
		  tablabody.appendChild(Nregistro); 
	 }
}

function sacarDatosOnClicVistaPrevia(i){ 
 
  tabla = document.getElementById('listDocumentosHistoricos');
  fila = tabla.getElementsByTagName('tr')[i-1];

  tipo_evolucion = fila.getElementsByTagName('td')[1].innerHTML  
  id_evolucion = fila.getElementsByTagName('td')[0].innerHTML
  titulo_evolucion = fila.getElementsByTagName('td')[2].innerHTML  
  idAdmision = fila.getElementsByTagName('td')[4].innerHTML   
  estado_evolucion = fila.getElementsByTagName('td')[10].innerHTML    
      
  asignaAtributo('txtTipoDocumentoVistaPrevia',tipo_evolucion,0) 
  asignaAtributo('txtIdDocumentoVistaPrevia',id_evolucion,0)     
  asignaAtributo('lblTituloDocumentoVistaPrevia',titulo_evolucion,0)      
  asignaAtributo('lblIdAdmisionVistaPrevia',idAdmision,0)         
//  asignaAtributo('lblEstadoDocumentoVistaPrevia',estado_evolucion,0)   
  imprimirHCPdf()
}

function administracionMedicamentos(){ 
	id_evolucion = fila.getElementsByTagName('td')[0].innerHTML
	asignaAtributo('txtIdDocumentoVistaPrevia',id_evolucion,0)
	imprimirPDFMedicamentos()
  }

function sacarDatosOnClicVistaPreviaParaFinalizar(i){ 
 
  tabla = document.getElementById('listDocumentosHistoricos');
  fila = tabla.getElementsByTagName('tr')[i-1];

  tipo_evolucion = fila.getElementsByTagName('td')[1].innerHTML  
  id_evolucion = fila.getElementsByTagName('td')[0].innerHTML
  titulo_evolucion = fila.getElementsByTagName('td')[2].innerHTML  
  idAdmision = fila.getElementsByTagName('td')[4].innerHTML   
  estado_evolucion = fila.getElementsByTagName('td')[10].innerHTML     
      
  asignaAtributo('txtTipoDocumentoVistaPrevia',tipo_evolucion,0) 
  asignaAtributo('txtIdDocumentoVistaPrevia',id_evolucion,0)     
  asignaAtributo('lblTituloDocumentoVistaPrevia',titulo_evolucion,0)      
  asignaAtributo('lblIdAdmisionVistaPrevia',idAdmision,0)        
  asignaAtributo('lblEstadoDocumentoVistaPrevia',estado_evolucion,0)   
  imprimirHCPdf()
}

function sacarDatosOnClic(i){ 
// marcarFilaTabla();
 asignaAtributo('vistaPrevia','NO',0);
// addTableRolloverEffect('listDocumentosHistoricos','overs','tableRowClickEffect');
 tabla = document.getElementById('listDocumentosHistoricos');
 fila = tabla.getElementsByTagName('tr')[i-1];
// fila.className='overs';
	
 idEstado = fila.getElementsByTagName('td')[10].innerHTML
 Estado = fila.getElementsByTagName('td')[11].innerHTML
 tipo_evolucion = fila.getElementsByTagName('td')[1].innerHTML
 nombre_evolucion = fila.getElementsByTagName('td')[2].innerHTML
 id_evolucion = fila.getElementsByTagName('td')[0].innerHTML
 Fecha = fila.getElementsByTagName('td')[5].innerHTML
 idAdmision = fila.getElementsByTagName('td')[4].innerHTML 
 idProfesionalElaboro = fila.getElementsByTagName('td')[8].innerHTML
// examenFisico = fila.getElementsByTagName('td')[xxxxx].innerHTML
// Comentario = fila.getElementsByTagName('td')[yyyy].innerHTML
 motivoConsulta = fila.getElementsByTagName('td')[13].innerHTML    
 enfermedadActual = fila.getElementsByTagName('td')[14].innerHTML    
 sintomaticoPiel = fila.getElementsByTagName('td')[15].innerHTML    
 sintomaticoRespira = fila.getElementsByTagName('td')[16].innerHTML  

 seleccionandoHistoricoAtencion(idEstado, Estado, tipo_evolucion, nombre_evolucion, id_evolucion, Fecha, idAdmision, idProfesionalElaboro,  motivoConsulta, enfermedadActual, sintomaticoPiel, sintomaticoRespira)
 mostrar('divPadreDeFolios'); 
}

function seleccionHistoricosAtencion(id_evolucion, tipo_evolucion, nombre_evolucion, idEstado, Estado, Fecha, idAdmision, idProfesionalElaboro, examenFisico, Comentario, motivoConsulta, enfermedadActual, sintomaticoPiel, sintomaticoRespira){
	ocultar('divDocumentosHistoricos')
	ocultar('divAntecedentes')	
	ocultar('divRevisionPorSistemas')	
	ocultar('divProcedimientosCir')								  
	ocultar('divExamenFisico')
	ocultar('divContenidos')	
	ocultar('divDiagnosticos')	
	ocultar('divProcedimientos')
	ocultar('divArchivosAdjuntos')
	ocultar('divAdmMedicamentos')		
	
	ocultar('divHojaGastos')		
	ocultar('divMonitorizacion')
	
	desHabilitar('btnProcedimiento',idEstado)		
	
	limpiaAtributo('lblIdDocumento',0); 			  		  
	limpiaAtributo('txtIdEsdadoDocumento',0); 			  		  			  
	limpiaAtributo('cmbTipoDocumento',0); 			  
	limpiarDivEditarJuan('datosDeHistoriaClinicaPaciente')

	estad=idEstado;  
	asignaAtributo('txtIdEsdadoDocumento',idEstado,estad);			  			  
	asignaAtributo('lblNomEstadoDocumento',Estado,estad);			  			  				
	asignaAtributo('lblTipoDocumento',tipo_evolucion,estad);			  
	asignaAtributo('lblDescripcionTipoDocumento',nombre_evolucion,estad);			  
	//asignaAtributo('lblTipoDocumento',datosRow.Tipo,estad);			  				
	//asignaAtributo('lblDescripcionTipoDocumento',datosRow.Descripcion,estad);	
	asignaAtributo('txtIdTipoDocumento',id_evolucion,estad);		  				
	asignaAtributo('lblIdDocumento',id_evolucion,estad); 
	asignaAtributo('lbl_idFecha',Fecha,estad);		// PARA LA IMPRESION EN PDF		
	asignaAtributo('lblIdAdmision',idAdmision,estad);					
	asignaAtributo('txtIdProfesionalElaboro',idProfesionalElaboro,estad);	
	

	asignaAtributo('txtExamenFisico',examenFisico,estad)
	asignaAtributo('txtComentario',Comentario,estad)				
	asignaAtributo('txtMotivoConsulta',motivoConsulta,estad)				
	asignaAtributo('txtEnfermedadActual',enfermedadActual,estad)	
	asignaAtributo('cmb_SintomaticoPiel',sintomaticoPiel,estad)				
	asignaAtributo('cmb_SintomaticoRespira',sintomaticoRespira,estad)				
	
	llenarTablaAlAcordion(id_evolucion, tipo_evolucion, nombre_evolucion);			
}
/***************************************************************************************************/



function llenarTablaDeReporteExcel(idReporte,parametro1,cantColumnasQuery, tablaReportesExcel,idListExcel){ 
   
   if(idReporte!='' & parametro1!=''  ){
	 cantColumnasP = cantColumnasQuery; 
	 idTablaReportesExcel =  tablaReportesExcel;	 
	 idListadoExcel = idListExcel;
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarReportes_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestallenarTablaDeReporteExcel;
	 
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');	
	 valores_a_mandar="accion=listadoReporteExcel"; 
	 valores_a_mandar=valores_a_mandar+"&id="+idReporte;
     valores_a_mandar=valores_a_mandar+"&parametro1="+parametro1;//+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
     varajaxInit.send(valores_a_mandar); 
   }
   else alert('Falta seleccionar el Elemnto a Exportar')
}
function respuestallenarTablaDeReporteExcel(){   
	
	if(varajaxInit.readyState == 4 ){      
	 //   VentanaModal.cerrar();
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement; 

				  if(raiz.getElementsByTagName('rows')!=null){  
				  
					   totalRegistros=raiz.getElementsByTagName('c1').length;  	
					 //  alert('totalRegistros= '+totalRegistros);
					
					   if(totalRegistros>0){    
						  agregarDatosTablaDeReporteExcel(raiz,totalRegistros);	
						  formatoExcelCC(idListadoExcel)
					   }
					   else{
						   alert("Caramba sin datos en el reporte");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//	abrirVentana(260, 100);
			  //  VentanaModal.setSombra(true);
		}
}
var cantColumnasP =0;
function agregarDatosTablaDeReporteExcel(raiz, totalRegistros){    
         borrarRegistrosTabla(idTablaReportesExcel);	  
		 var total=0,numRow=-1, cantFilas=0, cantColumnas=0, estilo='inputExcel';
         tablabody=document.getElementById(idTablaReportesExcel).lastChild;
		 tablabody.className='inputExcel';
		 tablabody.border = 1;		 
         
		// cantColumnasPar = 13;// $('#drag'+ventanaActual.num).find('#CantColumnas').val(); JJJJJJJJJJJJJJJJJJJJJJ
		 cantFilas = totalRegistros / cantColumnasP;

		 for(i=1;i<=cantFilas;i++){	 
			  Nregistro=document.createElement("TR");
			  Nregistro.id=i;
			  Nregistro.className=estilo;
			  
  				  TH=document.createElement("TD");
				  TH.className=estilo;
				  if(i==1){
				     TH.innerHTML= 'To';				  				  
				     Nregistro.appendChild(TH);	
				  }
				  else{
				     TH.innerHTML= ++total;				  				  
				     Nregistro.appendChild(TH);							 
				  
				  }
	  		  for(j=0;j<cantColumnasP;j++){   
  			      numRow++;
				  TD=document.createElement("TD"); 
				  TD.innerHTML= raiz.getElementsByTagName('c1')[numRow].firstChild.data;
				  Nregistro.appendChild(TD);							 
			  }
			  estilo='inputExcel';
			  tablabody.appendChild(Nregistro); 
		 }
	
}
/**************************************************************************************************************************************/



function llenarTablaDeReporte(){  
//abrirVentanaModalCC('/clinica/paginas/hc/platin/administrarPlatin.jsp',15,12,'dddd')
     $('#cargando').show();
	 $('#cargando').html('<img src="/clinica/utilidades/imagenes/ajax-loader.gif"/>');	 
	 
	 borrarRegistrosTabla('tablaReportes');	  
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarReportes_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestallenarTablaDeReporte;
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  
	
	 valores_a_mandar="accion=listadoReporte"; 
	 valores_a_mandar=valores_a_mandar+"&id="+$('#drag'+ventanaActual.num).find('#lblId').html();	
     valores_a_mandar=valores_a_mandar+"&fechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
  	 valores_a_mandar=valores_a_mandar+"&fechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();	
	 valores_a_mandar=valores_a_mandar+"&cantFiltros="+cParapetros;	
	 valores_a_mandar=valores_a_mandar+"&parametrosFiltros=";	
	 	 
	 if(cParapetros > 0 ){			
		for(i=1;i<=cParapetros;i++){
		  val_cm = $('#cmb_'+i).val();		
		  add_valores_a_mandar(valorAtributo('cmb_'+i));		  
		}		
	 }else{		 
		valores_a_mandar=valores_a_mandar+"&parametrosFiltros=" 	 
	 }
	 //alert(valores_a_mandar);
	varajaxInit.send(valores_a_mandar); 
	$('#listado').hide();	
	$('#divEditar').hide();	
}


function respuestallenarTablaDeReporte(){   
	
	if(varajaxInit.readyState == 4 ){      
	 //   VentanaModal.cerrar();
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement; 

				  if(raiz.getElementsByTagName('rows')!=null){  
				  
					   totalRegistros=raiz.getElementsByTagName('c1').length;  	
					 //  alert('totalRegistros= '+totalRegistros);
					
					   if(totalRegistros>0){    
						  agregarDatosTablaDeReporte(raiz,totalRegistros);						   
					   }
					   else{
						   alert("Caramba sin datos en el reporte");
					   }				   

				 }	                           
			 }else{
					alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:"+varajaxInit.status);
			 }
		 }
		if(varajaxInit.readyState == 1){
			//	abrirVentana(260, 100);
			  //  VentanaModal.setSombra(true);
		}
}

function agregarDatosTablaDeReporte(raiz, totalRegistros){  
		 var total=0,numRow=-1, cantFilas=0, cantColumnas=0, estilo='tituloReporte'; 
         tablabody=document.getElementById('tablaReportes').lastChild;
		 tablabody.className='grillaReporte1';
		 tablabody.border = 1;		 
         
		 cantColumnas = $('#drag'+ventanaActual.num).find('#CantColumnas').val();
		 cantFilas = totalRegistros / cantColumnas;

		 for(i=1;i<=cantFilas;i++){	 
			  Nregistro=document.createElement("TR");
			  Nregistro.id=i;
			  Nregistro.className=estilo;
			  
  				  TH=document.createElement("TD");
				  TH.className=estilo;
				  if(i==1){
				     TH.innerHTML= 'Tot';				  				  
				     Nregistro.appendChild(TH);							 
				  }
				  else{
				     TH.innerHTML= ++total;				  				  
				     Nregistro.appendChild(TH);							 
				  
				  }
	  		  for(j=0;j<cantColumnas;j++){   
  			      numRow++;
				  TD=document.createElement("TD"); 
				  //TD.className=estilo;
				  strong = document.createElement("strong");
				  strong.innerHTML= raiz.getElementsByTagName('c1')[numRow].firstChild.data;
//				  strong.className=estilo;
				  TD.appendChild(strong);							 				  
				  Nregistro.appendChild(TD);							 
			  }
			  estilo='grillaReporte1';
			  tablabody.appendChild(Nregistro); 
		 }
	$('#listado').show() ;
	$('#cargando').hide() ;
	$('#divEditar').show();	
}


var cParapetros = 0;
function buscarReportes(arg, pag){ 
	pagina=pag;
    ancho= ($('#drag'+ventanaActual.num).find("#divContenido").width());

//    alto= ($('#drag'+ventanaActual.num).find("#divContenido").width())*92/100;	
	 switch (arg){	
		/******************************************************************* ini  reportes **************************/
		case 'report':

		     ancho= 1100; 
			 pag ='/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
		 	 valores_a_mandar=pag;
			 valores_a_mandar=valores_a_mandar+"?idQuery=100"+"&parametros=";
			 add_valores_a_mandar(valorAtributo('cmbBusFiltro'));	
			 add_valores_a_mandar(valorAtributo('txtBusNomReporte'));				 		 
			 add_valores_a_mandar(valorAtributo('cmbBusTipo'));	
			 add_valores_a_mandar(valorAtributo('cmbBusTipo'));			 		 			 

			 $('#drag'+ventanaActual.num).find("#list_reportes").jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
               colNames:[ 'C','Id','Nombre Reporte','explicacion','Tipo','CantColumnas','IREPORTS','totparametro'],			 
               colModel :[  
               {name:'contador', index:'contador',  align:'left', width: anchoP(ancho,5)},  
               {name:'id', index:'id',  align:'left', width: anchoP(ancho,5)},  
               {name:'nomReporte', index:'nomReporte', align:'left', width: anchoP(ancho,45)},  	
               {name:'explicacion', index:'explicacion', align:'left', width: anchoP(ancho,35)},  				   
               {name:'tipo', index:'tipo', width: anchoP(ancho,10)},  
			   {name:'CantColumnas', index:'CantColumnas', hidden:true},  
			   {name:'IREPORTS', index:'IREPORTS', hidden:true },  			   
			   {name:'totparametro', index:'totparametro', width: anchoP(ancho,2) },  			   
			   ], 
			   height: 500, 
			   autoWidth: true,
			  onSelectRow: function(rowid) { 
								 
  		      var datosDelRegistro = jQuery('#drag'+ventanaActual.num).find("#list_reportes").getRowData(rowid);  

              $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();	
              $('#drag'+ventanaActual.num).find('#divEditar').css('height','700px'); 
              $('#drag'+ventanaActual.num).find('#lblId').html($.trim(datosDelRegistro.id)); 			  
              $('#drag'+ventanaActual.num).find('#lblNombre').html($.trim(datosDelRegistro.nomReporte));	
              $('#drag'+ventanaActual.num).find('#lblExplicacion').html($.trim(datosDelRegistro.explicacion));				  
			  $('#drag'+ventanaActual.num).find('#CantColumnas').val($.trim(datosDelRegistro.CantColumnas));  
			  $('#drag'+ventanaActual.num).find('#CantParametros').val($.trim(datosDelRegistro.totparametro));  
			  
			  cParapetros = $('#drag'+ventanaActual.num).find('#CantParametros').val();
			  var id_rep = $('#drag'+ventanaActual.num).find('#lblId').html(); 	  
			  
			  var URLFiltros="reportes/filtros.jsp" //?numParam="+cParapetros; 			  
			  
				$("#filtros_reportes").load(URLFiltros,{numParam:cParapetros, idReporte:id_rep}, function(response, status, xhr) {
				  if (status == "error") {
					var msg = "El reporte no tiene ningun filtro establecido !";
					//var msg = "Error!, algo ha sucedido: ";
					//$("#filtros_reportes").html(msg + xhr.status + " " + xhr.statusText);					
					$("#filtros_reportes").html(msg);
				  }
				});


			  if(datosDelRegistro.IREPORTS=='0'){
			    ocultar('REPORTE_EXCEL_IREPORT')
			    mostrar('REPORTE_EXCEL')
				ocultar('REPORTE_RECIBOS')	
				ocultar('REPORTE_TXT_IREPORT')		
				ocultar('REPORTE_CSV_IREPORT')	
				if (datosDelRegistro.id == '179'){mostrar('REPORTE_RECIBOS');ocultar('REPORTE_EXCEL')}				
			  }
			  else if(datosDelRegistro.IREPORTS=='1'){
			    mostrar('REPORTE_EXCEL_IREPORT')
			    ocultar('REPORTE_EXCEL')
				ocultar('REPORTE_RECIBOS')	
				ocultar('REPORTE_TXT_IREPORT')			
				ocultar('REPORTE_CSV_IREPORT')				
			  }else if(datosDelRegistro.IREPORTS =='2'){
				ocultar('REPORTE_EXCEL_IREPORT')
			    ocultar('REPORTE_EXCEL')
				ocultar('REPORTE_RECIBOS')
				mostrar('REPORTE_TXT_IREPORT')
				ocultar('REPORTE_CSV_IREPORT')	
			  }else if(datosDelRegistro.IREPORTS =='3'){
			  	ocultar('REPORTE_EXCEL_IREPORT')
			    ocultar('REPORTE_EXCEL')
				ocultar('REPORTE_RECIBOS')
				ocultar('REPORTE_TXT_IREPORT')
				mostrar('REPORTE_CSV_IREPORT')	
			  }	

			  if (datosDelRegistro.id == '179'){mostrar('REPORTE_RECIBOS');ocultar('REPORTE_EXCEL')}	

			  
		      borrarRegistrosTabla('tablaReportes');	
			  }		   
         });  
	    $('#drag'+ventanaActual.num).find("#list_reportes").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 		
			 		
        break;
		 case 'reportes':

		     ancho= 1100; 
			 pag ='/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
		 	 valores_a_mandar=pag;
			 valores_a_mandar=valores_a_mandar+"?idQuery=26"+"&parametros=";
			 add_valores_a_mandar(valorAtributo('cmbBusProceso'));	
			 add_valores_a_mandar(valorAtributo('txtBusNomReporte'));				 		 
			 add_valores_a_mandar(valorAtributo('cmbBusTipo'));				 		 			 

			 $('#drag'+ventanaActual.num).find("#list_reportes").jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
               colNames:[ 'C','Id','Nombre Reporte','explicacion','Tipo','CantColumnas','IREPORTS'],			 
               colModel :[  
               {name:'contador', index:'contador',  align:'left', width: anchoP(ancho,5)},  
               {name:'id', index:'id',  align:'left', width: anchoP(ancho,5)},  
               {name:'nomReporte', index:'nomReporte', align:'left', width: anchoP(ancho,45)},  	
               {name:'explicacion', index:'explicacion', align:'left', width: anchoP(ancho,35)},  				   
               {name:'tipo', index:'tipo', width: anchoP(ancho,10)},  
			   {name:'CantColumnas', index:'CantColumnas', width: anchoP(ancho,2)},  
			   {name:'IREPORTS', index:'IREPORTS', hidden:true },  			   
			   ], 
			   height: 900, 
			   width: 1100,	
			  onSelectRow: function(rowid) { 
								 
  		      var datosDelRegistro = jQuery('#drag'+ventanaActual.num).find("#list_"+arg).getRowData(rowid);  

              $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();	
              $('#drag'+ventanaActual.num).find('#divEditar').css('height','400px'); 
              $('#drag'+ventanaActual.num).find('#lblId').html($.trim(datosDelRegistro.id)); 			  
              $('#drag'+ventanaActual.num).find('#lblNombre').html($.trim(datosDelRegistro.nomReporte));	
              $('#drag'+ventanaActual.num).find('#lblExplicacion').html($.trim(datosDelRegistro.explicacion));				  
			  $('#drag'+ventanaActual.num).find('#CantColumnas').val($.trim(datosDelRegistro.CantColumnas));  

			  if(datosDelRegistro.IREPORTS=='0'){
			    ocultar('REPORTE_EXCEL_IREPORT')
			    mostrar('REPORTE_EXCEL')				
			  }
			  else{
			    mostrar('REPORTE_EXCEL_IREPORT')
			    ocultar('REPORTE_EXCEL')				
			  }
			  
		      borrarRegistrosTabla('tablaReportes');	
              },			   
         });  
	    $('#drag'+ventanaActual.num).find("#list_reportes").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 		
			 		
        break;			
        case 'reportes_': 
		       limpiarDivEditarJuan(arg); 
		       borrarRegistrosTabla('tablaReportes');	
 			   ancho= 1000;		 

			   valores_a_mandar=pag; 
			   valores_a_mandar=valores_a_mandar+"?idQuery=26"+"&parametros=";	
			   add_valores_a_mandar(valorAtributo('cmbBusProceso'));
			   add_valores_a_mandar(valorAtributo('txtBusNomReporte'));
			   add_valores_a_mandar(valorAtributo('cmbBusTipo'));
			   alert(valores_a_mandar)			   			   
					
			   $('#drag'+ventanaActual.num).find("#list_"+arg).jqGrid({ 
			   url:valores_a_mandar, 	
			   datatype: 'xml', 
			   mtype: 'GET', 
               colNames:['', 'Id','Id Proceso','Nombre Reporte','explicacion','Tipo','CantColumnas','ireports'],			 
               colModel :[  
               {name:'contador', index:'contador', hidden:true}, 			   
               {name:'id', index:'id',  align:'left', width: anchoP(ancho,5)},  
               {name:'idProceso', index:'idProceso', width: anchoP(ancho,15) },  	   
               {name:'nomReporte', index:'nomReporte', align:'left', width: anchoP(ancho,45)},  	
               {name:'explicacion', index:'explicacion', align:'left', width: anchoP(ancho,35)},  				   
               {name:'tipo', index:'tipo', width: anchoP(ancho,10)},  
			   {name:'CantColumnas', index:'CantColumnas', width: anchoP(ancho,2)},  
			   {name:'ireports', index:'ireports', width: anchoP(ancho,10)},  			   
			   ], 
			   height: 200, 
			   width: 1100,				   
			   onSelectRow: function(rowid){ 
				 var datosRow = jQuery('#drag'+ventanaActual.num).find('#'+arg).getRowData(rowid);
				 asignaAtributo('cmbIdTipoAfiliado', datosRow.ID_TIPO_AFILIADO, 0)
				 asignaAtributo('cmbIdRango', datosRow.RANGO, 0)
				 asignaAtributo('txtCopago', datosRow.COPAGO, 0)		 		 
				 asignaAtributo('txtCuotaModeradora', datosRow.CUOTA_MODERADORA, 0)		 
			   },		   
		
		   });  
	       $('#drag'+ventanaActual.num).find("#list_"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
		
		break;		  
	  		
		
		
		
		
		/********************************************************************************************** fin  reportes **************************/	
		/**********************************************************************************************  inicio busqueda indicadores **************************/			
        case 'fichaTecnica':   
		     limpiarDivEditarJuan(arg); 
		      //borrarRegistrosTabla('tablaReportes');	
			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','500px');				 
		 	 valores_a_mandar=pag+"?accion="+arg;
			 valores_a_mandar=valores_a_mandar+"&cmbBusProceso="+$('#drag'+ventanaActual.num).find('#cmbBusProceso').val();			 
			 valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();			 			 
			 valores_a_mandar=valores_a_mandar+"&cmbBusTipo="+$('#drag'+ventanaActual.num).find('#cmbBusTipo').val();	
			 valores_a_mandar=valores_a_mandar+"&cmbBusEstado="+$('#drag'+ventanaActual.num).find('#cmbBusEstado').val();				 
			 //alert(valores_a_mandar);

			 $('#drag'+ventanaActual.num).find("#list_"+arg).jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:[ 'Id','IdTipo','Id Proceso','Nombre','objetivo','Estado',
					  'IdPeriodicidad','FechaVigenciaIni','DescripcionResponsables','IdUnidadMedida','LineaBase','Meta','DenominadorRequerido',
					  'DescripcionDenominador','DescripcionNumerador','IdOrigenInfo','InfoAdicional','IdModoComparacion','Tot Indicadores','IdElaboro'
					  
					  ],			 
             colModel :[  
               {name:'id', index:'id',  align:'left', width: anchoP(ancho,5)},  
               {name:'IdTipo', index:'idTipo',  align:'left', width: anchoP(ancho,5)},  			   
               {name:'idProceso', index:'idProceso', width: anchoP(ancho,15) },  	   
               {name:'Nombre', index:'Nombre', align:'left', width: anchoP(ancho,45)},  	
               {name:'Objetivo', index:'Objetivo', align:'left', width: anchoP(ancho,35)},  				   
               {name:'Estado', index:'Estado', width: anchoP(ancho,18)},  

               {name:'IdPeriodicidad', index:'IdPeriodicidad', hidden:true},  
               {name:'FechaVigenciaIni', index:'FechaVigenciaIni', hidden:true},  
               {name:'DescripcionResponsables', index:'DescripcionResponsables', hidden:true},  
               {name:'IdUnidadMedida', index:'IdUnidadMedida', hidden:true},  
               {name:'LineaBase', index:'LineaBase', hidden:true},  
               {name:'Meta', index:'Meta', hidden:true},  
               {name:'DenominadorRequerido', index:'DenominadorRequerido', hidden:true},  
               {name:'DescripcionDenominador', index:'DescripcionDenominador', hidden:true},  
               {name:'DescripcionNumerador', index:'DescripcionNumerador', hidden:true},  
               {name:'IdOrigenInfo', index:'IdOrigenInfo', hidden:true},  
               {name:'InfoAdicional', index:'InfoAdicional', hidden:true},  
               {name:'IdModoComparacion', index:'IdModoComparacion', hidden:true},  
               {name:'totIndicadores', index:'totIndicadores'},  			   
               {name:'IdElaboro', index:'IdElaboro', hidden:true},  	
			   ], 
		 
			 height: 400, 
			 width: 940,			
			 ondblClickRow: function(rowid) { 
								 
  		     var datosRow = jQuery('#drag'+ventanaActual.num).find("#list_"+arg).getRowData(rowid);  

              $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();	
              $('#drag'+ventanaActual.num).find('#divEditar').css('height','400px'); 
			  
			  
			  asignaAtributo('lblId',datosRow.id,0);
			  asignaAtributo('txtNombre',datosRow.Nombre,0)			  			  
			  asignaAtributo('cmbTipo',datosRow.IdTipo,0)			  
			  asignaAtributo('cmbProceso',datosRow.idProceso,0)			  			  
			  asignaAtributo('txtObjetivo',datosRow.Objetivo,0)			  			  
			  asignaAtributo('cmbEstado',datosRow.Estado,0)			  			  			  
			  asignaAtributo('cmbPeriodicidad',datosRow.IdPeriodicidad,0)			  			  			  			  
			  asignaAtributo('txtFechaVigenciaIni',datosRow.FechaVigenciaIni,0)			  			  			  			  			  
			  asignaAtributo('cmbOrigenInfo',datosRow.IdOrigenInfo,0)			  			  			  			  			  			  
			  asignaAtributo('cmbUnidadMedida',datosRow.IdUnidadMedida,0);
			  asignaAtributo('txtDescripcionResponsable',datosRow.DescripcionResponsables,0);			  			  
			  asignaAtributo('txtLineaBase',datosRow.LineaBase,0);			  			  			  
			  asignaAtributo('txtMeta',datosRow.Meta,0);			  			  			  			  

			  
              asignaAtributo('txtDescripcionDenominador',datosRow.DescripcionDenominador,0);		
			  asignaAtributo('txtDescripcionNumerador',datosRow.DescripcionNumerador,0);	
			  asignaAtributo('txtInfoAdicional',datosRow.InfoAdicional,0);		
			  asignaAtributo('lblTotIndicadores',datosRow.totIndicadores,0);				  

              },

             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
            // caption: 'Resultados' 
         });  
	    $('#drag'+ventanaActual.num).find("#list_"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;	
        case 'indicador':   
		     limpiarDivEditarJuan(arg); 
		      //borrarRegistrosTabla('tablaReportes');	
			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','500px');				 
		 	 valores_a_mandar=pag+"?accion="+arg;
			 valores_a_mandar=valores_a_mandar+"&cmbBusProceso="+$('#drag'+ventanaActual.num).find('#cmbBusProceso').val();			 
			 valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();			 			 
			 valores_a_mandar=valores_a_mandar+"&cmbBusTipo="+$('#drag'+ventanaActual.num).find('#cmbBusTipo').val();	
			 valores_a_mandar=valores_a_mandar+"&cmbBusEstado="+$('#drag'+ventanaActual.num).find('#cmbBusEstado').val();				 
			 valores_a_mandar=valores_a_mandar+"&cmbBusMisIndicadores="+$('#drag'+ventanaActual.num).find('#cmbBusMisIndicadores').val();				 			 


			 $('#drag'+ventanaActual.num).find("#list_"+arg).jqGrid({ 
             url:valores_a_mandar, 	
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:[ 'Tot','IdIndicador','Tipo','Proceso','Id','Periodo','Nombre','objetivo',
					  'DescripcionNumerador','ValNumerador','ValExclusionNumerador','CriterioExclusionNumerador','DescripcionDenominador','ValDenominador','ValExclusionDenominador','CriterioExclusionDenominador',
					  'L.Base','Meta','Resultado','Cumplimiento','origenInfo','FechaVigenciaIni','DescripcionResponsble','Periodicidad','InfoAdicional','Und. Medida','Fecha.Vigenc.Fin','Atrasado','IdEstado','Estado','Analisis','idElaboro','fechaElaboro'					  
					  ],			 
             colModel :[  
               {name:'Tot', index:'Tot',  align:'left', width: anchoP(ancho,3)},  
               {name:'IdIndicador', index:'IdIndicador',  align:'left', width: anchoP(ancho,3)},  			   
               {name:'Tipo', index:'Tipo',  align:'left', width: anchoP(ancho,10)},  			   			   
               {name:'Proceso', index:'Proceso',  align:'left', width: anchoP(ancho,15)},  			   
               {name:'Id', index:'Id', width: anchoP(ancho,2) },  	   
               {name:'Periodo', index:'Periodo', width: anchoP(ancho,7)},  			   
               {name:'Nombre', index:'Nombre', align:'left', width: anchoP(ancho,45)},  	
               {name:'Objetivo', index:'Objetivo', align:'left', hidden:true},  				   
               {name:'DescripcionNumerador', index:'DescripcionNumerador', hidden:true},  
               {name:'ValNumerador', index:'ValNumerador', hidden:true},  
               {name:'ValExclusionNumerador', index:'ValExclusionNumerador', hidden:true},  
               {name:'CriterioExclusionNumerador', index:'CriterioExclusionNumerador', hidden:true},  
               {name:'DescripcionDenominador', index:'DescripcionDenominador', hidden:true},  			   
               {name:'ValDenominador', index:'ValDenominador', hidden:true},  
               {name:'ValExclusionDenominador', index:'ValExclusionDenominador', hidden:true},  
               {name:'CriterioExclusionDenominador', index:'CriterioExclusionDenominador', hidden:true},  
               {name:'LineaBase', index:'LineaBase', width: anchoP(ancho,5)},  
               {name:'Meta', index:'Meta', width: anchoP(ancho,5)},  
               {name:'Resultado', index:'Resultado', width: anchoP(ancho,10)}, 
               {name:'Cumplimiento', index:'Cumplimiento'},  			   			   			   
               {name:'origenInfo', index:'origenInfo', hidden:true},  			   			   
               {name:'FechaVigenciaIni', index:'FechaVigenciaIni', hidden:true},  			   
               {name:'DescripcionResponsble', index:'DescripcionResponsble', hidden:true},  	
               {name:'Periodicidad', index:'Periodicidad', width: anchoP(ancho,10)},  			   
               {name:'InfoAdicional', index:'InfoAdicional', hidden:true},  
               {name:'UnidMedida', index:'UnidMedida', hidden:true},  
               {name:'FechaVigenciaFin', index:'FechaVigenciaFin'},  			   
               {name:'Atrasado', index:'Atrasado', width: anchoP(ancho,10)},  
               {name:'IdEstado', index:'IdEstado', hidden:true},  			   
               {name:'desEstado', index:'desEstado'},  	
               {name:'Analisis', index:'Analisis', hidden:true},  				   
               {name:'idElaboro', index:'idElaboro', hidden:true},  	
               {name:'fechaElaboro', index:'fechaElaboro', hidden:true},  				   
			   ], 
		 
			 height: 400, 
			 width: 1040,			
			 ondblClickRow: function(rowid) { 
								 
  		     var datosRow = jQuery('#drag'+ventanaActual.num).find("#list_"+arg).getRowData(rowid);  

              $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();	
              $('#drag'+ventanaActual.num).find('#divEditar').css('height','400px'); 
			  
			  
			  asignaAtributo('lblTipo',datosRow.Tipo,0); 
			  asignaAtributo('lblIdIndicador',datosRow.IdIndicador,0);
			  asignaAtributo('lblProceso',datosRow.Proceso,0)	
			  asignaAtributo('lblId',datosRow.Id,0);			  
			  asignaAtributo('lblNombre',datosRow.Nombre,0)			  			  
			  asignaAtributo('lblObjetivo',datosRow.Objetivo,0)	
			  asignaAtributo('txtIdEstado',datosRow.IdEstado,0)	
		      asignaAtributo('cmbEstado',datosRow.IdEstado,datosRow.IdEstado)	
			  
			  asignaAtributo('lblFechaVigenciaIni',datosRow.FechaVigenciaIni,0)			  			  			  			  			  
			  asignaAtributo('lblOrigenInfo',datosRow.origenInfo,0)			  			  			  			  			  			  
			  asignaAtributo('lblDescripcionResponsables',datosRow.DescripcionResponsble,0);	
			  
			  asignaAtributo('lblUnidadMedida',datosRow.UnidMedida,0);			  
			  asignaAtributo('lblLineaBase',datosRow.LineaBase,0);			  			  			  
			  asignaAtributo('lblMeta',datosRow.Meta,0);	
			  asignaAtributo('lblPeriodicidad',datosRow.Periodicidad,0)			  			  			  			  			  
			  asignaAtributo('lblInfoAdicional',datosRow.InfoAdicional,0);	
			  
			  asignaAtributo('lblDescripcionNumerador',datosRow.DescripcionNumerador,0);
			  asignaAtributo('txtValNumerador',datosRow.ValNumerador,0);
			  asignaAtributo('txtValExclusionNumerador',datosRow.ValExclusionNumerador,0);	 	
 		      asignaAtributo('txtCriterioExclusionNumerador',datosRow.CriterioExclusionNumerador,0);	
	
			  if(  datosRow.DescripcionDenominador=='') inactivo=1; else  inactivo =0;

              asignaAtributo('lblDescripcionDenominador',datosRow.DescripcionDenominador,inactivo);
			  asignaAtributo('txtValDenominador',datosRow.ValDenominador,inactivo);
			  asignaAtributo('txtValExclusionDenominador',datosRow.ValExclusionDenominador,inactivo);	 	
			  asignaAtributo('txtCriterioExclusionDenominador',datosRow.CriterioExclusioDenominador,inactivo);	 				  
			  
              asignaAtributo('lblPeriodo',datosRow.Periodo,0);
              asignaAtributo('txtAnalisis',datosRow.Analisis,0);				  
			  asignaAtributo('txtDescripcionNumerador',datosRow.DescripcionNumerador,0);	
			  asignaAtributo('cmbTendencia',datosRow.IdTendencia,0);		
			  
			  
			  AjaxRelacionEventoOM('lblIdIndicador','in');

              },

             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
            // caption: 'Resultados' 
         });  
	    $('#drag'+ventanaActual.num).find("#list_"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;			
	 }
}













	
// JavaScript para la programacion de los botones


function irLiquidarCirugia() {

    if (reglaRutasCirugia()) {
        limpiarDivEditarJuan('abreVentanitaLiquidacion')
        mostrar('divVentanitaLiquidacionCirugia');
        limpiaAtributo('cmbIdTipoAnestesia', 0);
        limpiaAtributo('lblIdLiquidacion');
        buscarFacturacion('listLiquidacionQx')
    }

}



function ocuparCandadoPaReporte() {
    var_ajaxMenu = crearAjax();
    valores_a_mandar = "";
    var_ajaxMenu.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    var_ajaxMenu.onreadystatechange = llenarocuparCandadoPaReporte;
    var_ajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=ocuparCandadoReporte";
    var_ajaxMenu.send(valores_a_mandar);

}
var bandFormOftal = '';
function llenarocuparCandadoPaReporte() {
    if (var_ajaxMenu.readyState == 4) {
        if (var_ajaxMenu.status == 200) {
            raiz = var_ajaxMenu.responseXML.documentElement;

            if (valorAtributo('txtIdDocumentoVistaPrevia') == '')
                TIPO_DOCUMENTO = valorAtributo('lblTipoDocumento')
            else
                TIPO_DOCUMENTO = tipo_evolucion

            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'true')
                if (TIPO_DOCUMENTO == 'EVOP' || TIPO_DOCUMENTO == 'EXOP') {// alert('bandFormOftal='+bandFormOftal)

                    if (bandFormOftal == 'SI') {
                        recorrerImpresion();
                        //imprimirSincronicoPdfAnexoOptalmica(1)											
                        bandFormOftal = '';
                    } else
                        recorrerImpresion()

                } else
                    recorrerImpresion();

            //else ocultar('divVentanitaPdf')  
            else {
                alert('Limpiando..., intente nuevamente')
                ocultar('divVentanitaPdf')
            }
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + var_ajaxMenu.status);
        }
    }
    if (var_ajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function LiberarCandadoPaReporte() {
    var_ajaxMenu = crearAjax();
    valores_a_mandar = "";
    var_ajaxMenu.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    var_ajaxMenu.onreadystatechange = llenarLiberarCandadoPaReporte;
    var_ajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=liberarCandadoReporte";
    var_ajaxMenu.send(valores_a_mandar);
}
function llenarLiberarCandadoPaReporte() {
    if (var_ajaxMenu.readyState == 4) {
        if (var_ajaxMenu.status == 200) {
            raiz = var_ajaxMenu.responseXML.documentElement;

            //if(raiz.getElementsByTagName('dato')[0].firstChild.data=='true')

        } else {
            alert("ppProblemas de conexion con servidor: Reinicie el aplicativo " + var_ajaxMenu.status);
        }
    }
    if (var_ajaxMenu.readyState == 1) {
        alert("pppProblemas de conexion con Servidor: Reinicie el aplicativo")
    }
}





function vistaPreviaDocumentosPostconsulta() {
    imprimirHCPdf();
    setTimeout("habilitar('btnFinalizarDocumento',0)", 2000)

}


function cargarTableProcedimientosRealizados() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/facturacion/admisiones/procedimientosRealizados.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarTablePlanTratamiento;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}
function llenarcargarTablePlanTratamiento() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaPlanDeTratamiento").innerHTML = varajaxMenu.responseText;
            $("#tabsPrincipalConductaYTratamiento").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
            TabActivoConductaTratamiento('divProcedimiento')
            desHabilitar('btnProcedimiento', valorAtributo('txtIdEsdadoDocumento'))

            asignaAtributo('lblNomAdministradora2', valorAtributo('lblNomAdministradora'))

            setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoP','lblIdDocumento')", 500);

        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

/*
 
 function cargarTablePlanTratamiento(){ 
 varajaxMenu=crearAjax();
 valores_a_mandar="";
 varajaxMenu.open("POST",'/clinica/paginas/hc/hc/procedimientos.jsp',true);
 varajaxMenu.onreadystatechange=llenarcargarTablePlanTratamiento;
 varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
 varajaxMenu.send(valores_a_mandar);   
 }	
 function llenarcargarTablePlanTratamiento(){ 
 if(varajaxMenu.readyState == 4 ){
 if(varajaxMenu.status == 200){
 document.getElementById("divParaPlanDeTratamiento").innerHTML = varajaxMenu.responseText;
 $("#tabsPrincipalConductaYTratamiento").tabs().find(".ui-tabs-nav").sortable({axis:'x'});	
 TabActivoConductaTratamiento('divProcedimiento')
 alert(valorAtributo('txtIdEsdadoDocumento'))
 if(valorAtributo('txtIdEsdadoDocumento') == '1' || valorAtributo('txtIdEsdadoDocumento') == '12')			  
 desHabilitar('btnProcedimiento',1)	
 else 	 desHabilitar('btnProcedimiento',0)		 
 
 asignaAtributo('lblNomAdministradora2',valorAtributo('lblNomAdministradora'))
 }else{
 alert("Problemas de conexion con servidor: Reinicie el aplicativo "+varajaxMenu.status);
 }
 } 
 if(varajaxMenu.readyState == 1){
 alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
 }
 } 
 
 */

function cargarTrabajos() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/hc/trabajos.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarTrabajos;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}
function llenarcargarTrabajos() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaTrabajos").innerHTML = varajaxMenu.responseText;
            buscarEvento('listEventoEnHc')

        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}



function cargarTablePlanTratamiento() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/hc/procedimientos.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarTablePlanTratamiento;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}
function llenarcargarTablePlanTratamiento() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaPlanDeTratamiento").innerHTML = varajaxMenu.responseText;
            $("#tabsPrincipalConductaYTratamiento").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
            TabActivoConductaTratamiento('divProcedimiento')

            if (valorAtributo('txtIdEsdadoDocumento') == '0' || valorAtributo('txtIdEsdadoDocumento') == '12') {
                desHabilitar('btnProcedimiento', 0);
            } else
                desHabilitar('btnProcedimiento', 1);
            asignaAtributo('lblNomAdministradora2', valorAtributo('lblNomAdministradora'))
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}


function cargarTableProcedimientosProgramados() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/hc/procedimientoCirugia.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarTableProcedimientosProgramados;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}
function llenarcargarTableProcedimientosProgramados() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaProcedimientosProgramacion").innerHTML = varajaxMenu.responseText;
            $("#tabsPrincipalProcedimientosCir").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
            setTimeout("TabActivoConductaTratamiento('divPreOperatorio')", 300);

            //desHabilitar('btnProcedimiento',valorAtributo('txtIdEsdadoDocumento'))		
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function cargarDocumentosYTransaccionesDespachosProcedimientos() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/suministros/transacciones/documentoTransacciones.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarDocumentosYTransaccionesDespachosProcedimientos;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}
function llenarcargarDocumentosYTransaccionesDespachosProcedimientos() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaDocumentosTransacciones").innerHTML = varajaxMenu.responseText;
            buscarSuministros('listGrillaDocumentosBodegaProgramacion')
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function cargarTableHojaDeGastos() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/hc/HojaDeGastos.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarTableHojaDeGastos;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}
function llenarcargarTableHojaDeGastos() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaHojaDeGastos").innerHTML = varajaxMenu.responseText;
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function calcularCosto() {
    if (valorAtributo('txtConsumo') != '') {
        if (valorAtributo('txtConsumo') != 0) {
            var valor_total = parseFloat(valorAtributo('txtCostoHG'));
            var cant_consumo = parseFloat(valorAtributo('lblCantidadConsumo'));
            var cant_factor = parseFloat(valorAtributo('txtConsumo'));
            var valor_costo = valor_total / cant_consumo;

            var total_costo = valor_costo * cant_factor;

            asignaAtributo('lblCostoHG', total_costo.toFixed(2) + '', 0);
        } else {
            alert('El valor no puede ser 0');
            asignaAtributo('txtConsumo', '', 0)
        }
    } else {
        asignaAtributo('lblCostoHG', '0', 0);
    }
}

function verificaUnidadConsumo() {
    /*funcion para validad la cantidad de consumo (factor de converci�n del articulo) */
    if (valorAtributo('txtConsumo') != '' && valorAtributo('lblCantidadConsumo') != '') {
        var cantidad_consumo = valorAtributo('lblCantidadConsumo');
        var txt_cant_consumo = valorAtributo('txtConsumo');
        var total_consumo = parseInt(cantidad_consumo) - parseInt(txt_cant_consumo);
        if (total_consumo >= 0) {
            habilitar('btnTraerArticulo', 1);
        } else {
            alert('La cantidad no puede se mayor a ' + cantidad_consumo + ',\n Por favor ingrese un valor menor o igual.');
            $("#txtConsumo").focus();
            habilitar('btnTraerArticulo', 0);
        }
    }
}

function validadNovedad() {
    var cantidad_devolucion = parseInt(valorAtributo('lblCantNovedad'));
    if (cantidad_devolucion > 0) {
        if (valorAtributo('cmbNovedad') == 1) {
            alert('Debe registrar la novedad');
            return;
        } else {
            modificarCRUD('traerArticulo', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
        }
    } else {
        if ((valorAtributo('cmbNovedad') == 2) && (cantidad_devolucion <= 0)) {
            alert('El valor de la devolucion no puede ser Menor o igual a cero.\n Modifique la cantidad de consumo.');
            return;
        } else {
            modificarCRUD('traerArticulo', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
        }
    }
}

function validaUltimoConsumo() {
    var cantidad_devolucion = parseInt(valorAtributo('lblCantNovedad'));
    if (cantidad_devolucion > 0) {
        if (valorAtributo('cmbNovedad') == 1) {
            alert('Debe registrar la novedad');
            return;
        } else {
            modificarCRUD('traerUltimoArticulo', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
        }
    } else {
        if ((valorAtributo('cmbNovedad') == 2) && (cantidad_devolucion <= 0)) {
            alert('El valor de la devolucion no puede ser Menor o igual a cero.\n Modifique la cantidad de consumo.');
            return;
        } else {
            modificarCRUD('traerUltimoArticulo', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
        }
    }
}

function seleccionarNovedad() {
    asignaAtributo('cmbCantConsumo', '0', 0)
    asignaAtributo('txtConsumo', '0', 0)
    calcularCantidadNovedad()

}

function calcularCantidadNovedad() {
    var cantDespacho = valorAtributo('lblCantDespacho');
    var cantConsumo = valorAtributo('cmbCantConsumo');
    var cantDevolucion = parseInt(cantDespacho) - parseInt(cantConsumo);
    //alert (cantDevolucion);

    if (cantDevolucion >= 0) {
        habilitar('btnTraerArticulo', 1);
        if (valorAtributo('txtBanGrilla') == '2') {
            habilitar('btnTraerUltimoArticulo', 1);
        }
    } else {
        alert('La cantidad consumida no puede superar la cantidad despachada,\n Por favor ingrese un valor menor o igual a la cantidad de despacho.');
        $("#cmbCantConsumo").focus();
        habilitar('btnTraerArticulo', 0);
        habilitar('btnTraerUltimoArticulo', 0);
    }
    if (valorAtributo('txtBanGrilla') == '1') {
        asignaAtributo('lblCantNovedad', cantDevolucion + '', 0);
    } else {
        asignaAtributo('lblCantNovedad', '0', 0);
    }

}

function calcularImpuesto(idElemento) {
    if (valorAtributo('lblNaturaleza') == 'I') {
        var valorUnitario = valorAtributo('txtValorU');
    } else {
        var valorUnitario = valorAtributo('lblValorUnitario');
    }

    var iva = valorAtributo('cmbIva');
    var cantidad = valorAtributo('txtCantidad');
    var subTotal = 0;
    var Total = 0;
    var vlrImpuesto = ((valorUnitario * iva) / 100);

    subTotal = parseFloat(valorUnitario) + parseFloat(vlrImpuesto);
    total = subTotal.toFixed(2) * cantidad;

    //asignaAtributo(idElemento,Math.ceil(vlrImpuesto.toFixed(2), -1)+'',0);	
    asignaAtributo(idElemento, vlrImpuesto.toFixed(3) + '', 0);
    subTotal = number_format(subTotal, 0);
    asignaAtributo('lblSubTotal', subTotal, 0);
    total = number_format(total, 0);
    asignaAtributo('lblTotal', total, 0);
    //alert(subTotal);

}

function validadCantidadDevCompra() {
    if (valorAtributo('txtCantidad') != '' && valorAtributo('txtCantidadTr') != '') {
        var cant = parseInt(valorAtributo('txtCantidad'));
        var cantTr = parseInt(valorAtributo('txtCantidadTr'));
        if (cant > cantTr) {
            alert('La cantidad no puede ser mayor a la compra realizada. \nPor Favor ingrese una cantidad menor o igual a la de la compra.');
            asignaAtributo('txtCantidad', '', 0);
            return;
        }

    }

}

function calcularImpuestoSalida() {
    if (valorAtributoIdAutoCompletar('txtIdArticulo') != '') {
        if (valorAtributo('lblExistencias') == '') {
            $("#cmbIdLote").focus();
            return;
        }
        var cantidad = parseInt(valorAtributo('txtCantidad'));
        if (cantidad <= 0) {
            alert('La cantidad no puede ser cero.');
            return;
        }
        var existencias = parseInt(valorAtributo('lblExistencias'));
        if (valorAtributo('txtCantidad') != '') {
            if (cantidad <= existencias) {
                var valorUnitario = valorAtributo('lblValorUnitario');
                var val_iva = valorAtributo('lblIva');
                var subTotal = (valorUnitario / ((val_iva / 100) + 1)) * cantidad;
                //asignaAtributo('lblSubTotal',  subTotal.toFixed(2) ,0);  

                var lblValorImpuesto = subTotal.toFixed(2) * ((val_iva / 100));
                asignaAtributo('lblValorImpuesto', lblValorImpuesto.toFixed(2), 0);


                subTotal = number_format(subTotal, 2);
                asignaAtributo('lblSubTotal', subTotal, 0);

                var total = valorUnitario * cantidad;
                vrlTotal = number_format(total, 2);
                //asignaAtributo('lblTotal',total+'' , 0);
                asignaAtributo('lblTotal', vrlTotal, 0);
            } else {
                alert('La cantidad ingresada supera las existencias en bodega. \n Por favor Ingrese una cantidad menor o igual a las existencias');
                asignaAtributo('txtCantidad', '', 0);
                $("#txtCantidad").focus();
            }
        }
    }
}
function calcularImpuestoDevolucion() {
    if (valorAtributoIdAutoCompletar('txtIdArticulo') != '') {
        var cantidad = parseInt(valorAtributo('txtCantidad'));
        if (cantidad <= 0) {
            alert('La cantidad no puede ser cero.');
            return;
        }
        if (valorAtributo('txtCantidad') != '') {

            var valorUnitario = valorAtributo('lblValorUnitario');
            var val_iva = valorAtributo('lblIva');
            var subTotal = (valorUnitario / ((val_iva / 100) + 1)) * cantidad;
            //asignaAtributo('lblSubTotal',  subTotal.toFixed(2) ,0);  

            var lblValorImpuesto = subTotal.toFixed(2) * ((val_iva / 100));
            asignaAtributo('lblValorImpuesto', lblValorImpuesto.toFixed(2), 0);


            subTotal = number_format(subTotal, 2);
            asignaAtributo('lblSubTotal', subTotal, 0);

            var total = valorUnitario * cantidad;
            vrlTotal = number_format(total, 2);
            //asignaAtributo('lblTotal',total+'' , 0);
            asignaAtributo('lblTotal', vrlTotal, 0);


        } else {
            alert('La cantidad No puesde ser vacia');
        }
    }
}
function calcularImpuestoDevolucionCompra() {
    if (valorAtributoIdAutoCompletar('txtIdArticulo') != '') {
        var cantidad = parseInt(valorAtributo('txtCantidad'));
        if (cantidad <= 0) {
            alert('La cantidad no puede ser cero.');
            return;
        }
        if (valorAtributo('txtCantidad') != '') {

            var valorUnitario = valorAtributo('lblValorUnitario');
            var val_iva = valorAtributo('lblIva');
            var subTotal = (valorUnitario / ((val_iva / 100) + 1)) * cantidad;
            //asignaAtributo('lblSubTotal',  subTotal.toFixed(2) ,0);  

            var lblValorImpuesto = subTotal.toFixed(2) * ((val_iva / 100));
            asignaAtributo('lblValorImpuesto', lblValorImpuesto.toFixed(2), 0);


            subTotal = number_format(subTotal, 2);
            asignaAtributo('lblSubTotal', subTotal, 0);

            var total = valorUnitario * cantidad;
            vrlTotal = number_format(total, 2);
            //asignaAtributo('lblTotal',total+'' , 0);
            asignaAtributo('lblTotal', vrlTotal, 0);


        } else {
            alert('La cantidad No puesde ser vacia');
        }
    }
}

function cargarIva() {
    var idQuery = "";
    switch (valorAtributo('lblNaturaleza')) {
        case 'S':
            idQuery = "138";
            break;
        case 'I':
            idQuery = "137";
            break;
        default:
            idQuery = "-1"

            break;
    }
    cargarComboGRALCondicion1('cmbIdBodega', 'xxx', 'cmbIva', idQuery, valorAtributoIdAutoCompletar('txtIdArticulo'))
}



function traerValorUnitario() { //precio de venta
    if (valorAtributoIdAutoCompletar('txtIdArticulo') != '') {
        limpiarDivEditarJuan('limpCamposDeArticulo');
        cargarElementoCondicionUno('lblValorUnitario', 505, valorAtributoIdAutoCompletar('txtIdArticulo'))
    }
}

function traerValorUnitarioProcedimientoCuenta() {
    if (valorAtributoIdAutoCompletar('txtIdProcedimientoCex') != '') {
        //  if(valorAtributo('lblIdCuenta')!='Q' )			
        //	cargarElementoCondicionDos('lblValorUnitarioProc',523,valorAtributo('lblIdPlan'), valorAtributoIdAutoCompletar('txtIdProcedimientoCex'))
        // if(valorAtributo('lblIdCuenta')=='Q' ){	
        //	    alert('ahi vamos cirugia')
        cargarElementoCondicionDos('lblValorUnitarioProc', 529, valorAtributo('lblIdPlan'), valorAtributoIdAutoCompletar('txtIdProcedimientoCex'))
        // }
    }
}
function traerElementosCodBarras() {
    if (valorAtributo('txtCodBarrasBus') != '' && valorAtributo('cmbIdBodegaBus') != '') {
        cargarElementoCondicionDosCodigoBarras('lblTraidosCodBarras', 538, valorAtributo('cmbIdBodegaBus'), valorAtributo('txtCodBarrasBus'))
    } else
        alert('EL CODIGO DE BARRAS DEBE SER UN VALOR REAL')
}
function traerElementosCodigoBarras() {
    if (valorAtributo('txtIdBarCode') != '' && valorAtributo('lblIdEstado') == '0') {
        cargarElementoCondicionDosCodigoBarras('lblTraidosCodBarras', 538, valorAtributo('cmbIdBodega'), valorAtributo('txtIdBarCode'))
    } else
        alert('EL DOCUMENTO DEBE ESTAR ABIERTO Y EL CODIGO DE BARRAS DEBE SER UN VALOR REAL')
}

var registrosDeUnCombo = 0;
function cargarLote() {
    if (valorAtributoIdAutoCompletar('txtIdArticulo') != '') {
        limpiaAtributo('cmbIdLote', 0);
        cargarComboGRALCondicion2('txtIdArticulo', 'xxx', 'cmbIdLote', 139, valorAtributoIdAutoCompletar('txtIdArticulo'), valorAtributo('cmbIdBodega'))
    }
}
function cargarSerial() {
    if (valorAtributoIdAutoCompletar('txtIdArticulo') != '') {
        limpiaAtributo('cmbSerial', 0);
        cargarComboGRALCondicion2('txtIdArticulo', 'xxx', 'cmbIdLote', 139, valorAtributoIdAutoCompletar('txtIdArticulo'), valorAtributo('cmbIdBodega'))
    }
}

function traerExistenciaLote() {
    if (valorAtributoIdAutoCompletar('txtIdArticulo') != '') {
        if (valorAtributo('cmbIdLote') != '') {
            cargarElementoCondicionTres('lblExistencias', 504, valorAtributoIdAutoCompletar('txtIdArticulo'), valorAtributo('cmbIdBodega'), valorAtributo('cmbIdLote'))
            setTimeout("cargarElementoCondicionTres('txtFV',513,valorAtributoIdAutoCompletar('txtIdArticulo'), valorAtributo('cmbIdBodega'), valorAtributo('cmbIdLote'))", 400);
        } else {
            if ((document.getElementById('cmbIdLote').options.length) == 1) {
                cargarElementoCondicionDos('lblExistencias', 506, valorAtributoIdAutoCompletar('txtIdArticulo'), valorAtributo('cmbIdBodega'))
            } else {
                alert('DEBE SELECCIONAR ALGUN LOTE')
                asignaAtributo('lblExistencias', 0)
                document.getElementById('txtCantidad').focus()
            }
        }
    }
}

/** funciones para la accion de busqueda*/
function seleccionarSistema(id) {
    asignaAtributo('txtSistema', id, 1)
}
function seleccionarAntecedenteFarmacologico(id) {
    asignaAtributo('txtAntecedenteFarmacologico', id, 1)
}

function cargarAntecedentesGrupo(idElemento) {
    cargarComboGRALCondicion1('cmbPadre', '1', 'cmbAntecedenteGrupo', 122, valorAtributo(idElemento));
}

function cargarElementosDeMedicamento(idElemento) {
    cargarComboGRALCondicionOrdenesMedicaVia('txtIdArticulo', 'xxx', 'cmbIdVia', 50, valorAtributoIdAutoCompletar(idElemento))
}

function cargarElementosDeMedicamentoEditar(idElemento) {
    asignaAtributoCombo2('cmbIdViaEditar', '', '');
    cargarComboGRALCondicionOrdenesMedicaVia('lblIdElementoEditar', 'xxx', 'cmbIdViaEditar', 50, valorAtributo(idElemento));
}


function cargarElementosDeMedicamentoAdm(idElemento) {
    cargarComboGRALCondicionOrdenesMedicaVia('txtIdMedicamento', 'xxx', 'cmbIdViaMed', 50, valorAtributoIdAutoCompletar(idElemento))
}

function cargarPresentacionDeMedicamento(idElemento) {
    setTimeout("cargarComboGRALCondicion1('cmbPadre','1','cmbIdPresentacion',55,'" + valorAtributoIdAutoCompletar('txtIdArticulo') + "')", 800);
}
/**/
function cargarDocumentosDeBodega(idElemento, idElemento2) {
    //cargarComboGRALCondicionDocumentosBodega('lblId', 'xxx', 'cmbIdTipoDocumento', 136, valorAtributo(idElemento)) 
    cargarComboGRALCondicion2('cmbIdBodega', 'xxx', 'cmbIdTipoDocumento', 136, valorAtributo(idElemento), valorAtributo(idElemento2))
}

function cargarDescuentoArticulo(idElemento, idElemento2) {
    //cargarComboGRALCondicionDocumentosBodega('lblId', 'xxx', 'cmbIdTipoDocumento', 136, valorAtributo(idElemento)) 	
    if (valorAtributo('txtIdTipoBodega') == 4) {
        cargarComboGRALCondicion2('cmbPadre', 'xxx', 'cmbIdDescuento', 552, valorAtributoIdAutoCompletar(idElemento), valorAtributo(idElemento2))
    } else {
        cargarComboGRALCondicion1('cmbPadre', '1', 'cmbIdDescuento', 553, '0');
    }
}

function calcularDescuento() {
    if (valorAtributo('txtIdTipoBodega') == 4) {
        desc = parseInt(valorAtributo('cmbIdDescuento'));
        if (!isNaN(desc)) {
            vlr_un = parseInt(valorAtributo('txtVlrUnitarioAnterior'));
            asignaAtributo('lblValorUnitario', '', 0);
            //alert(vlr_un +'--'+ desc);		
            valor_descuento = parseInt(((vlr_un * desc) / 100));
            //alert (valor_descuento);		
            vlr_total = vlr_un - valor_descuento;
            //alert(vlr_total);
            asignaAtributo('lblValorUnitario', vlr_total + '', 0);
        }
    }
}

function modificarTransaccionSalida() {
    if (valorAtributo('txtIdTipoBodega') == 4) {
        if (valorAtributo('cmbIdDescuento') != '') {
            modificarCRUD('modificaTransaccionOptica');
        } else {
            alert('Debe seleccionar un valor de descuento');
        }
    } else {
        modificarCRUD('modificaTransaccion');
    }
}

function cargarDiagnosticoRelacionado(combo, idElemento) {

    cargarComboGRALCondicion1('cmbPadre', '1', combo, 544, valorAtributo(idElemento));
}

function traerOrdenesAnteriores(idElemento) {

    if (valorAtributo('cmbTipoDocumento') != 'RCOD') {
        if (valorAtributo('txtIdEditable') == 0) {
            //	document.getElementById('divTraerOrdenes').style.top = '500';     	
            //	$('#drag'+ventanaActual.num).find("#divTraerOrdenes").style.top = '500';     
            mostrar('divTraerOrdenes')
            limpiarListadosTotales('listDocumentosHistoricosTraer');
            limpiarListadosTotales('listOrdenesMedicamentosTraer');
            limpiarListadosTotales('listOrdenesMedicamentosTratamiento');
            buscarHC('listDocumentosHistoricosTraer', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
        } else
            alert('ESTE DOCUMENTO NO ES EDITABLE, ES HISTORICO O VIGENTE')
    } else
        alert('ESTE TIPO DE DOCUMENTO NO PERMITE TRAER MEDICACION DE EVOLUCIONES ANTERIORES')
}

function buscarHistoria(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {


        case 'listGrillaPlantilla':

            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=437&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodPlantilla'));
            add_valores_a_mandar(valorAtributo('txtNomPlantilla'));
            // add_valores_a_mandar( valorAtributo('cmbVigenteBus')  );				 			 
            $('#drag' + ventanaActual.num).find("#listGrillaPlantilla").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'COD', 'NOMBRE', 'CONTENIDO', 'TIPO', 'ID_TIPO'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 5) },
                    { name: 'COD', index: 'COD', width: anchoP(ancho, 10) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 75) },
                    { name: 'CONTENIDO', index: 'CONTENIDO', width: anchoP(ancho, 10) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 10) },
                    { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPlantilla').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblCodigo', datosRow.COD, 1);
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0);
                    asignaAtributo('txtContenido', datosRow.CONTENIDO, 0);
                    asignaAtributo('cmbTipo', datosRow.ID_TIPO, 0);

                    buscarHistoria('listGrillaPlantillaElemento')
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaPlantillaElemento':

            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=704&parametros=";
            add_valores_a_mandar(valorAtributo('lblCodigo'));
            $('#drag' + ventanaActual.num).find("#listGrillaPlantillaElemento").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID_PLANTILLA', 'PLANTILLA', 'ORDEN', 'DESCRIPCION', 'ETIQUETA', 'REFERENCIA','ID_PADRE_ORDEN'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_PLANTILLA', index: 'ID_PLANTILLA', hidden: true },
                    { name: 'PLANTILLA', index: 'PLANTILLA', hidden: true },
                    { name: 'ORDEN', index: 'ORDEN', width: anchoP(ancho, 5) },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 40) },
                    { name: 'ETIQUETA', index: 'ETIQUETA', width: anchoP(ancho, 27) },
                    { name: 'REFERENCIA', index: 'REFERENCIA', width: anchoP(ancho, 27)},
                    { name: 'ID_PADRE_ORDEN', index: 'ID_PADRE_ORDEN', width: anchoP(ancho, 8)}
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //           limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPlantillaElemento').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('txtOrden', datosRow.ORDEN, 0);
                    asignaAtributo('txtDescripcion', datosRow.DESCRIPCION, 0);
                    asignaAtributo('txtEtiqueta', datosRow.ETIQUETA, 0);
                    asignaAtributo('txtReferencia', datosRow.REFERENCIA, 0);

                    asignaAtributo('lblOrden', datosRow.ORDEN, 0);
                    asignaAtributo('lblExamen', datosRow.ID_PLANTILLA, 0);
                    asignaAtributo('txtOrdenPadre', datosRow.ID_PADRE_ORDEN, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listGrillaExamen':
            // limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=705&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('txt_EXLA_C1'));
            // add_valores_a_mandar( valorAtributo('cmbVigenteBus')  );				 			 
            $('#drag' + ventanaActual.num).find("#listGrillaExamen").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'ORDEN', 'ID_PLANTILLA', 'DESCRIPCION', 'RESULTADO', 'ETIQUETA', 'REFERENCIA', 'OBSERVACION','FOLIO'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ORDEN', index: 'ORDEN', hidden: true },
                    { name: 'ID_PLANTILLA', index: 'ID_PLANTILLA', hidden: true },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 30) },
                    { name: 'RESULTADO', index: 'RESULTADO', width: anchoP(ancho, 10) },
                    { name: 'ETIQUETA', index: 'ETIQUETA', width: anchoP(ancho, 20) },
                    { name: 'REFERENCIA', index: 'REFERENCIA', width: anchoP(ancho, 20) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 40) },
                    { name: 'FOLIO', index: 'FOLIO', hidden: true }
                ],
                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaExamen').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdPlantilla', datosRow.ID_PLANTILLA, 0);
                    asignaAtributo('lblOrden', datosRow.ORDEN, 0);
                    asignaAtributo('lblDescripcion', datosRow.DESCRIPCION, 0);
                    asignaAtributo('txtResultado', datosRow.RESULTADO, 0);
                    asignaAtributo('lblEtiqueta', datosRow.ETIQUETA, 0);
                    asignaAtributo('lblReferencia', datosRow.REFERENCIA, 0);
                    asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0);
                    
                    if(datosRow.FOLIO == ''){
                        habilitar('btnRegistrar',1)
                        habilitar('btnModificar',0)
                    }else{
                        habilitar('btnRegistrar',0)
                        habilitar('btnModificar',1)
                    }                    
                    mostrarVentanita('divVentanitaExamen');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
        break;
        case 'listGrillaVentana':
            limpiarDivEditarJuan(arg);
            pag = '/clinica/paginas/accionesXml/buscarGrillaVentana_xml.jsp';
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodBus'));
            add_valores_a_mandar(valorAtributo('txtNomBus'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'OBSERVACION'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 5) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 70) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 25) }
                ],
                height: 200,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo(elemInputDestino, concatenarCodigoNombre(datosRow.ID, datosRow.NOMBRE), 0)
                    ocultar('divVentanitaPuesta')
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaVentanaCondicion1':
            if (valorAtributo('lblIdCondicion1') != '') {
                limpiarDivEditarJuan(arg);
                pag = '/clinica/paginas/accionesXml/buscarGrillaVentana_xml.jsp';
                ancho = 1000;
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdCondicion1'));
                add_valores_a_mandar(valorAtributo('txtCodBus'));
                add_valores_a_mandar(valorAtributo('txtNomBus'));

                $('#drag' + ventanaActual.num).find("#listGrillaVentanaCondicion1").jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['contador', 'ID', 'NOMBRE', 'OBSERVACION'],
                    colModel: [
                        { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                        { name: 'ID', index: 'ID', width: anchoP(ancho, 8) },
                        { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 80) },
                        { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 15) }
                    ],
                    height: 200,
                    width: ancho,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaVentanaCondicion1').getRowData(rowid);
                        estad = 0;

                        asignaAtributo(elemInputDestino, concatenarCodigoNombre(datosRow.ID, datosRow.NOMBRE), 0)

                        ocultar('divVentanitaPuestaCondicion1')
                    },

                });
                $('#drag' + ventanaActual.num).find("#listGrillaVentanaCondicion1").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            } else
                alert('FALTA CONDICION FILTRO')
            break;
        case 'listGrillaVentanaArticulo':
            limpiarDivEditarJuan(arg);
            pag = '/clinica/paginas/accionesXml/buscarGrillaVentana_xml.jsp';
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 10;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));
            add_valores_a_mandar(valorAtributo('txtCodBus'));
            add_valores_a_mandar(valorAtributo('txtNomBus'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'PRECIO_VENTA', 'PORC_IVA_VENTA'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 5) },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 15) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 80) },
                    { name: 'PRECIO_VENTA', index: 'PRECIO_VENTA', hidden: true },
                    { name: 'PORC_IVA_VENTA', index: 'PORC_IVA_VENTA', hidden: true }
                ],
                height: 200,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo(elemInputDestino, concatenarCodigoNombre(datosRow.ID, datosRow.NOMBRE), 0)
                    if (valorAtributo('txtBanderaDevolucion') != 'SI') {
                        asignaAtributo('lblValorUnitario', datosRow.PRECIO_VENTA, 0)
                        asignaAtributo('lblIva', datosRow.PORC_IVA_VENTA, 0)
                    }
                    ocultar('divVentanitaPuestaArticulo')
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaDiagnostico':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=34&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodDiagnosticoBus'));
            add_valores_a_mandar(valorAtributo('txtNomDiagnosticoBus'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            $('#drag' + ventanaActual.num).find("#listGrillaDiagnostico").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'CIE', 'DIAGNOSTICO', 'INDICACION', 'ID_ESTADO', 'ESTADO'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 5) },
                    { name: 'CIE', index: 'CIE', width: anchoP(ancho, 10) },
                    { name: 'DIAGNOSTICO', index: 'DIAGNOSTICO', width: anchoP(ancho, 75) },
                    { name: 'INDICACION', index: 'INDICACION', width: anchoP(ancho, 75) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', width: anchoP(ancho, 10) },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaDiagnostico').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblId', datosRow.CIE, 1);
                    asignaAtributo('lblNombre', datosRow.DIAGNOSTICO, 1);
                    asignaAtributo('txtIndicacion', datosRow.INDICACION, 0);
                    asignaAtributo('cmbVigente', datosRow.ID_ESTADO, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaDiagnostico").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaElemento':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=36&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodDiagnosticoBus'));
            add_valores_a_mandar(valorAtributo('txtNomDiagnosticoBus'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'NOMBRE', 'ESTADO',
                    'RESUMEN_HC', 'ALTERNATIVA_POS', 'POSIBILIDAD_TERAPEUTICA', 'MOTIVO_DE_USO', 'DIAGNOSTICOS', 'JUSTIFICACION'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 5) },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 5) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 50) },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 5) },

                    { name: 'RESUMEN_HC', index: 'RESUMEN_HC', width: anchoP(ancho, 5) },
                    { name: 'ALTERNATIVA_POS', index: 'ALTERNATIVA_POS', width: anchoP(ancho, 5) },
                    { name: 'POSIBILIDAD_TERAPEUTICA', index: 'POSIBILIDAD_TERAPEUTICA', width: anchoP(ancho, 5) },
                    { name: 'MOTIVO_DE_USO', index: 'MOTIVO_DE_USO', width: anchoP(ancho, 5) },
                    { name: 'DIAGNOSTICOS', index: 'DIAGNOSTICOS', width: anchoP(ancho, 5) },
                    { name: 'JUSTIFICACION', index: 'JUSTIFICACION', width: anchoP(ancho, 5) }

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblId', datosRow.ID, 1);
                    asignaAtributo('lblNombre', datosRow.NOMBRE, 1);
                    asignaAtributo('cmbVigente', datosRow.ESTADO, 0);
                    asignaAtributo('txtResumenHC', datosRow.RESUMEN_HC, 0);
                    asignaAtributo('txtAlternativaPos', datosRow.ALTERNATIVA_POS, 0);
                    asignaAtributo('txtPosibilidadTerapeutica', datosRow.POSIBILIDAD_TERAPEUTICA, 0);
                    asignaAtributo('txtMotivoUso', datosRow.MOTIVO_DE_USO, 0);
                    asignaAtributo('txtDx', datosRow.DIAGNOSTICOS, 0);
                    asignaAtributo('txtJustificacion', datosRow.JUSTIFICACION, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaElemento2':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=38&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodDiagnosticoBus'));
            add_valores_a_mandar(valorAtributo('txtNomDiagnosticoBus'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'NOMBRE', 'ESTADO',
                    'RESUMEN_ENF', 'MEDICAMENTO_UTILIZADO', 'MEDICAMENTO_PRESENTACION', 'MEDICAMENTO_DOSIS', 'INDICACION_TERAPEUTICA', 'EXPLIQUE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 5) },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 5) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 45) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 5) },

                    { name: 'RESUMEN_ENF', index: 'RESUMEN_ENF', width: anchoP(ancho, 5) },
                    { name: 'MEDICAMENTO_UTILIZADO', index: 'MEDICAMENTO_UTILIZADO', width: anchoP(ancho, 5) },
                    { name: 'MEDICAMENTO_PRESENTACION', index: 'MEDICAMENTO_PRESENTACION', width: anchoP(ancho, 5) },
                    { name: 'MEDICAMENTO_DOSIS', index: 'MEDICAMENTO_DOSIS', width: anchoP(ancho, 5) },
                    { name: 'INDICACION_TERAPEUTICA', index: 'INDICACION_TERAPEUTICA', width: anchoP(ancho, 5) },
                    { name: 'EXPLIQUE', index: 'EXPLIQUE', width: anchoP(ancho, 5) }

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblId', datosRow.ID, 1);
                    asignaAtributo('lblNombre', datosRow.NOMBRE, 1);
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0);
                    asignaAtributo('txtResumenEnf', datosRow.RESUMEN_ENF, 0);
                    asignaAtributo('txtUtilizado', datosRow.MEDICAMENTO_UTILIZADO, 0);
                    asignaAtributo('txtPresentacion', datosRow.MEDICAMENTO_PRESENTACION, 0);
                    asignaAtributo('txtDosis', datosRow.MEDICAMENTO_DOSIS, 0);
                    asignaAtributo('txtIndicacion', datosRow.INDICACION_TERAPEUTICA, 0);
                    asignaAtributo('txtExplique', datosRow.EXPLIQUE, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listTerceros':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=43&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodTercero'));
            add_valores_a_mandar(valorAtributo('txtNomTercero'));
            add_valores_a_mandar(valorAtributo('txtNomTercero'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'NOMBRE', 'ID_FABRICANTE', 'NOMBRE_FABRICANTE', 'CLASE', 'VIGENTE', 'ID_GRUPO', 'ID_CLASE', 'ID_SUBCLASE', 'ID_PRODUCTO', 'DESCRIPCION', 'DESCRIPCION_ABREVIADA',
                    'ID_UNIDAD', 'PORC_IVA', 'PORC_IVA_VENTA', 'CODIGO_INVIMA', 'CODIGO_BARRAS', 'CODIGO_ATC', 'SW_FECHA_VENCIMIENTO', 'SW_PROVEEDOR_UNICO', 'atc'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 2) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 35) },
                    { name: 'ID_FABRICANTE', index: 'ID_FABRICANTE', hidden: true },
                    { name: 'NOMBRE_FABRICANTE', index: 'NOMBRE_FABRICANTE', width: anchoP(ancho, 5) },
                    { name: 'CLASE', index: 'CLASE', width: anchoP(ancho, 5) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 5) },
                    { name: 'ID_GRUPO', index: 'ID_GRUPO', hidden: true },
                    { name: 'ID_CLASE', index: 'ID_CLASE', hidden: true },
                    { name: 'ID_SUBCLASE', index: 'ID_SUBCLASE', hidden: true },
                    { name: 'ID_PRODUCTO', index: 'ID_PRODUCTO', hidden: true },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', hidden: true },
                    { name: 'DESCRIPCION_ABREVIADA', index: 'DESCRIPCION_ABREVIADA', hidden: true },

                    { name: 'ID_UNIDAD', index: 'ID_UNIDAD', hidden: true },
                    { name: 'PORC_IVA', index: 'PORC_IVA', hidden: true },
                    { name: 'PORC_IVA_VENTA', index: 'PORC_IVA_VENTA', hidden: true },
                    { name: 'CODIGO_INVIMA', index: 'CODIGO_INVIMA', hidden: true },
                    { name: 'CODIGO_BARRAS', index: 'CODIGO_BARRAS', hidden: true },
                    { name: 'CODIGO_ATC', index: 'CODIGO_ATC', hidden: true },
                    { name: 'SW_FECHA_VENCIMIENTO', index: 'SW_FECHA_VENCIMIENTO', hidden: true },
                    { name: 'SW_PROVEEDOR_UNICO', index: 'SW_PROVEEDOR_UNICO', hidden: true },
                    { name: 'atc', index: 'atc', hidden: true },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('txtIdArticulo', datosRow.ID, 1);
                    asignaAtributo('txtNombreArticulo', datosRow.NOMBRE, 0);
                    asignaAtributo('cmbIdGrupo', datosRow.ID_GRUPO, 0);
                    asignaAtributo('cmbIdClase', datosRow.CLASE, 0);
                    asignaAtributo('cmbIdSubClase', datosRow.ID_SUBCLASE, 0);
                    asignaAtributo('txtCodArticulo', datosRow.ID_PRODUCTO, 0);
                    asignaAtributo('txtDescripcionCompleta', datosRow.DESCRIPCION, 0);
                    asignaAtributo('txtDescripcionAbreviada', datosRow.DESCRIPCION_ABREVIADA, 0);
                    asignaAtributo('txtIdFabricante', concatenarCodigoNombre(datosRow.ID_FABRICANTE, datosRow.NOMBRE_FABRICANTE), 0)

                    asignaAtributo('cmbIdUnidad', datosRow.ID_UNIDAD, 0);
                    asignaAtributo('txtIVA', datosRow.PORC_IVA, 0);
                    asignaAtributo('txtIVAVenta', datosRow.PORC_IVA_VENTA, 0);
                    asignaAtributo('txtInvima', datosRow.CODIGO_INVIMA, 0);
                    asignaAtributo('txtBarras', datosRow.CODIGO_BARRAS, 0);
                    asignaAtributo('txtATC', datosRow.CODIGO_ATC, 0);
                    asignaAtributo('txtCodAtc', datosRow.atc, 0);
                    asignaAtributo('cmbIdFechaVencimiento', datosRow.SW_FECHA_VENCIMIENTO, 0);
                    asignaAtributo('cmbIdProveedorUnico', datosRow.SW_PROVEEDOR_UNICO, 0);
                    asignaAtributo('cmbVigente', datosRow.SW_PROVEEDOR_UNICO, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'ordenesHOS':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
            idQuery = 580;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdArea'));
            add_valores_a_mandar(valorAtributo('cmbIdHabitacion'));

            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id cita', 'Hora', 'Espera Admision', 'id Paciente', 'Identificacion', 'HC_pdf',
                    'Paciente', 'Edad', 'idEspecialidad', 'Especialidad', 'Sub Especialidad', 'idTipo', 'Tipo', 'id_Administradora', 'Administradora',
                    'Admision', 'IdAdmision', 'idTipoAdmision', 'TipoAdmision', 'Auditado'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id_cita', index: 'id_cita', hidden: true },
                    { name: 'Hora', index: 'Hora', width: anchoP(ancho, 3) },
                    { name: 'Espera', index: 'Espera', width: anchoP(ancho, 6) },
                    { name: 'idPaciente', index: 'idPaciente', hidden: true },
                    { name: 'identificacion_Paciente', index: 'identificacion_Paciente', width: anchoP(ancho, 6) },
                    { name: 'HC_pdf', index: 'HC_pdf', width: anchoP(ancho, 2) },
                    { name: 'NomCompleto', index: 'NomCompleto', width: anchoP(ancho, 15) },
                    { name: 'edad', index: 'edad', width: anchoP(ancho, 2) },
                    { name: 'idEspecialidad', index: 'idEspecialidad', hidden: true },
                    { name: 'nomEspecialidad', index: 'nomEspecialidad', hidden: true },
                    { name: 'nomSubEspecialidad', index: 'nomSubEspecialidad', hidden: true },
                    { name: 'idTipo', index: 'idTipo', hidden: true },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 6) },
                    { name: 'id_Administradora', index: 'id_Administradora', hidden: true },
                    { name: 'Administradora', index: 'Administradora', width: anchoP(ancho, 7) },
                    { name: 'Admision', index: 'Admision', width: anchoP(ancho, 9) },
                    { name: 'idAdmision', index: 'idAdmision', hidden: true },
                    { name: 'idTipoAdmision', index: 'idTipoAdmision', hidden: true },
                    { name: 'TipoAdmision', index: 'TipoAdmision', hidden: true },
                    { name: 'Auditado', index: 'Auditado', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 320,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPacientes').getRowData(rowid);

                    estad = 0;
                    asignaAtributo('lblIdPaciente', datosRow.idPaciente, estad);
                    asignaAtributo('lblIdHc', datosRow.id_hc, 1);
                    asignaAtributo('lblIdentificacionPaciente', datosRow.identificacion_Paciente, 1);
                    asignaAtributo('lblNombrePaciente', datosRow.NomCompleto, 1);
                    asignaAtributo('lblIdEspecialidad', datosRow.idEspecialidad, 1);
                    asignaAtributo('lblNombreEspecialidad', datosRow.nomEspecialidad, 1);
                    asignaAtributo('lblIdAdmision', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdAdmisionAgen', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdTipoAdmision', datosRow.idTipoAdmision, 1);
                    asignaAtributo('lblTipoAdmision', datosRow.TipoAdmision, 1);
                    asignaAtributo('lblIdAdministradora', datosRow.id_Administradora, 1);
                    asignaAtributo('lblNomAdministradora', datosRow.Administradora, 1);
                    asignaAtributo('lblEdadPaciente', datosRow.edad, 1);
                    buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                    // agregarTablaHistoricosAtencion(1,valorAtributo('lblIdPaciente'),18, 'listDocumentosHistoricos'); 	  

                    ocultar('divBuscar')// pa que no estorbe 	
                    ocultar('divAntecedentes')
                    ocultar('divRevisionPorSistemas')
                    ocultar('divExamenFisico')
                    ocultar('divContenidos')
                    ocultar('divDiagnosticos')
                    ocultar('divProcedimientos')
                    ocultar('divArchivosAdjuntos')
                    ocultar('divHojaGastos')
                    limpiaAtributo('lblIdDocumento', 0);
                    limpiaAtributo('txtIdEsdadoDocumento', 0);
                    limpiaAtributo('lblNomEstadoDocumento', 0);
                    limpiaAtributo('cmbTipoDocumento', 0);
                    habilitar('btnProcedimiento', 1)

                    limpiarDivEditarJuan('datosDeHistoriaClinicaPaciente')

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'ordenesCEX':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
            idQuery = 320;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(IdSesion());
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id cita', 'Hora', 'Espera Admision', 'id Paciente', 'Identificacion', 'DOC_pdf',
                    'Paciente', 'Edad', 'idEspecialidad', 'Especialidad', 'Sub Especialidad', 'idTipo', 'Tipo', 'id_Administradora', 'Administradora',
                    'Admision', 'IdAdmision', 'idTipoAdmision', 'TipoAdmision', 'Auditado'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id_cita', index: 'id_cita', hidden: true },
                    { name: 'Hora', index: 'Hora', width: anchoP(ancho, 3) },
                    { name: 'Espera', index: 'Espera', width: anchoP(ancho, 6) },
                    { name: 'idPaciente', index: 'idPaciente', hidden: true },
                    { name: 'identificacion_Paciente', index: 'identificacion_Paciente', width: anchoP(ancho, 6) },
                    { name: 'DOC_pdf', index: 'DOC_pdf', width: anchoP(ancho, 2) },
                    { name: 'NomCompleto', index: 'NomCompleto', width: anchoP(ancho, 15) },
                    { name: 'edad', index: 'edad', width: anchoP(ancho, 2) },
                    { name: 'idEspecialidad', index: 'idEspecialidad', hidden: true },
                    { name: 'nomEspecialidad', index: 'nomEspecialidad', hidden: true },
                    { name: 'nomSubEspecialidad', index: 'nomSubEspecialidad', hidden: true },
                    { name: 'idTipo', index: 'idTipo', hidden: true },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 6) },
                    { name: 'id_Administradora', index: 'id_Administradora', hidden: true },
                    { name: 'Administradora', index: 'Administradora', width: anchoP(ancho, 7) },
                    { name: 'Admision', index: 'Admision', width: anchoP(ancho, 9) },
                    { name: 'idAdmision', index: 'idAdmision', hidden: true },
                    { name: 'idTipoAdmision', index: 'idTipoAdmision', hidden: true },
                    { name: 'TipoAdmision', index: 'TipoAdmision', hidden: true },
                    { name: 'Auditado', index: 'Auditado', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 320,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPacientes').getRowData(rowid);

                    estad = 0;
                    asignaAtributo('lblIdPaciente', datosRow.idPaciente, estad);
                    asignaAtributo('lblIdHc', datosRow.id_hc, 1);
                    asignaAtributo('lblIdentificacionPaciente', datosRow.identificacion_Paciente, 1);
                    asignaAtributo('lblNombrePaciente', datosRow.NomCompleto, 1);
                    asignaAtributo('lblIdEspecialidad', datosRow.idEspecialidad, 1);
                    asignaAtributo('lblNombreEspecialidad', datosRow.nomEspecialidad, 1);
                    asignaAtributo('lblIdAdmision', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdAdmisionAgen', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdTipoAdmision', datosRow.idTipoAdmision, 1);
                    asignaAtributo('lblTipoAdmision', datosRow.TipoAdmision, 1);
                    asignaAtributo('lblIdAdministradora', datosRow.id_Administradora, 1);
                    asignaAtributo('lblNomAdministradora', datosRow.Administradora, 1);
                    asignaAtributo('lblEdadPaciente', datosRow.edad, 1);


                    /*if(datosRow.DOC_pdf == ''){
                    	alert('FALTA DOCUMENTO IDENTIFICACION PACIENTE.')
                    }*/
                    $('#divPdfIdentificacion').html(datosRow.DOC_pdf);


                    buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                    // agregarTablaHistoricosAtencion(1,valorAtributo('lblIdPaciente'),18, 'listDocumentosHistoricos'); 	  

                    ocultar('divBuscar')// pa que no estorbe 	
                    ocultar('divAntecedentes')
                    ocultar('divRevisionPorSistemas')
                    ocultar('divExamenFisico')
                    ocultar('divContenidos')
                    ocultar('divDiagnosticos')
                    ocultar('divProcedimientos')
                    ocultar('divArchivosAdjuntos')
                    ocultar('divHojaGastos')
                    limpiaAtributo('lblIdDocumento', 0);
                    limpiaAtributo('txtIdEsdadoDocumento', 0);
                    limpiaAtributo('lblNomEstadoDocumento', 0);
                    limpiaAtributo('cmbTipoDocumento', 0);
                    habilitar('btnProcedimiento', 1)

                    limpiarDivEditarJuan('datosDeHistoriaClinicaPaciente')

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'ordenesCIR':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
            idQuery = 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id cita', 'Hora', 'Espera Admision', 'id Paciente', 'Identificacion', 'DOC_pdf',
                    'Paciente', 'edad', 'idEspecialidad', 'Especialidad', 'Sub Especialidad', 'idTipo', 'Tipo', 'id_Administradora', 'Administradora',
                    'Admision', 'IdAdmision', 'idTipoAdmision', 'TipoAdmision', 'Auditado'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id_cita', index: 'id_cita', hidden: true },
                    { name: 'Hora', index: 'Hora', width: anchoP(ancho, 3) },
                    { name: 'Espera', index: 'Espera', width: anchoP(ancho, 6) },
                    { name: 'idPaciente', index: 'idPaciente', hidden: true },
                    { name: 'identificacion_Paciente', index: 'identificacion_Paciente', width: anchoP(ancho, 6) },
                    { name: 'DOC_pdf', index: 'DOC_pdf', width: anchoP(ancho, 2) },
                    { name: 'NomCompleto', index: 'NomCompleto', width: anchoP(ancho, 17) },
                    { name: 'edad', index: 'edad', width: anchoP(ancho, 3) },
                    { name: 'idEspecialidad', index: 'idEspecialidad', hidden: true },
                    { name: 'nomEspecialidad', index: 'nomEspecialidad', hidden: true },
                    { name: 'nomSubEspecialidad', index: 'nomSubEspecialidad', hidden: true },
                    { name: 'idTipo', index: 'idTipo', hidden: true },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 6) },
                    { name: 'id_Administradora', index: 'id_Administradora', hidden: true },
                    { name: 'Administradora', index: 'Administradora', width: anchoP(ancho, 7) },
                    { name: 'Admision', index: 'Admision', width: anchoP(ancho, 11) },
                    { name: 'idAdmision', index: 'idAdmision', hidden: true },
                    { name: 'idTipoAdmision', index: 'idTipoAdmision', width: anchoP(ancho, 3) },
                    { name: 'TipoAdmision', index: 'TipoAdmision', hidden: true },
                    { name: 'Auditado', index: 'Auditado', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 320,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPacientes').getRowData(rowid);

                    estad = 0;
                    asignaAtributo('lblIdPaciente', datosRow.idPaciente, estad);
                    asignaAtributo('lblIdHc', datosRow.id_hc, 1);
                    asignaAtributo('lblIdentificacionPaciente', datosRow.identificacion_Paciente, 1);
                    asignaAtributo('lblNombrePaciente', datosRow.NomCompleto, 1);
                    asignaAtributo('lblIdEspecialidad', datosRow.idEspecialidad, 1);
                    asignaAtributo('lblNombreEspecialidad', datosRow.nomEspecialidad, 1);
                    asignaAtributo('lblIdAdmision', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdAdmisionAgen', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdTipoAdmision', datosRow.idTipoAdmision, 1);
                    asignaAtributo('lblTipoAdmision', datosRow.TipoAdmision, 1);
                    asignaAtributo('lblIdCita', datosRow.ID_AGENDA_DETALLE, 1);
                    asignaAtributo('lblIdAdministradora', datosRow.id_Administradora, 1);
                    asignaAtributo('lblNomAdministradora', datosRow.Administradora, 1);
                    asignaAtributo('lblEdadPaciente', datosRow.edad, 1);


                    /*if(datosRow.DOC_pdf == ''){
                    	alert('FALTA DOCUMENTO IDENTIFICACION PACIENTE.')
                    }*/
                    $('#divPdfIdentificacion').html(datosRow.DOC_pdf);

                    buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                    ocultar('divBuscar')// pa que no estorbe 	
                    ocultar('divAntecedentes')
                    ocultar('divRevisionPorSistemas')
                    ocultar('divExamenFisico')
                    ocultar('divContenidos')
                    ocultar('divDiagnosticos')
                    ocultar('divProcedimientos')
                    ocultar('divProcedimientosCir')
                    ocultar('divHojaGastos')
                    limpiaAtributo('lblIdDocumento', 0);
                    limpiaAtributo('txtIdEsdadoDocumento', 0);
                    limpiaAtributo('lblNomEstadoDocumento', 0);
                    limpiaAtributo('cmbTipoDocumento', 0);

                    limpiarDivEditarJuan('datosDeHistoriaClinicaPaciente')

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'ordenesAYD':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
            idQuery = 474;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbTipoFolio'));
            add_valores_a_mandar(valorAtributo('cmbIdEstadoFolio'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributo('txtFechaDesdeFolio'));
            add_valores_a_mandar(valorAtributo('txtFechaHastaFolio'));

            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id cita', 'Hora', 'Espera Admision', 'id Paciente', 'Identificacion', 'HC_pdf',
                    'Paciente', 'Edad', 'idEspecialidad', 'Especialidad', 'Sub Especialidad', 'idTipo', 'Tipo', 'id_Estado', 'Estado',
                    'Admision', 'IdAdmision', 'fec_finaliza', 'TipoAdmision'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id_cita', index: 'id_cita', hidden: true },
                    { name: 'Hora', index: 'Hora', width: anchoP(ancho, 3) },
                    { name: 'Espera', index: 'Espera', width: anchoP(ancho, 6) },
                    { name: 'idPaciente', index: 'idPaciente', hidden: true },
                    { name: 'identificacion_Paciente', index: 'identificacion_Paciente', width: anchoP(ancho, 6) },
                    { name: 'HC_pdf', index: 'HC_pdf', width: anchoP(ancho, 2) },
                    { name: 'NomCompleto', index: 'NomCompleto', width: anchoP(ancho, 17) },
                    { name: 'edad', index: 'edad', width: anchoP(ancho, 3) },
                    { name: 'idEspecialidad', index: 'idEspecialidad', hidden: true },
                    { name: 'nomEspecialidad', index: 'nomEspecialidad', hidden: true },
                    { name: 'nomSubEspecialidad', index: 'nomSubEspecialidad', width: anchoP(ancho, 7) },
                    { name: 'idTipo', index: 'idTipo', hidden: true },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 6) },
                    { name: 'id_Estado', index: 'auditado', hidden: true },
                    { name: 'Estado', index: 'auditado', width: anchoP(ancho, 7) },
                    { name: 'Admision', index: 'Admision', width: anchoP(ancho, 8) },
                    { name: 'idAdmision', index: 'idAdmision', hidden: true },
                    { name: 'idTipoAdmision', index: 'idTipoAdmision', width: anchoP(ancho, 8) },
                    { name: 'TipoAdmision', index: 'TipoAdmision', hidden: true },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 320,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPacientes').getRowData(rowid);

                    estad = 0;
                    asignaAtributo('lblIdPaciente', datosRow.idPaciente, estad);
                    asignaAtributo('lblIdHc', datosRow.id_hc, 1);
                    asignaAtributo('lblIdentificacionPaciente', datosRow.identificacion_Paciente, 1);
                    asignaAtributo('lblNombrePaciente', datosRow.NomCompleto, 1);
                    asignaAtributo('lblIdEspecialidad', datosRow.idEspecialidad, 1);
                    asignaAtributo('lblNombreEspecialidad', datosRow.nomEspecialidad, 1);
                    asignaAtributo('lblIdAdmision', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdAdmisionAgen', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdTipoAdmision', datosRow.idTipoAdmision, 1);
                    asignaAtributo('lblTipoAdmision', datosRow.TipoAdmision, 1);

                    buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                    ocultar('divBuscar')// pa que no estorbe 	
                    ocultar('divAntecedentes')
                    ocultar('divRevisionPorSistemas')
                    ocultar('divExamenFisico')
                    ocultar('divContenidos')
                    ocultar('divDiagnosticos')
                    ocultar('divProcedimientos')
                    ocultar('divProcedimientosCir')
                    ocultar('divHojaGastos')
                    limpiaAtributo('lblIdDocumento', 0);
                    limpiaAtributo('txtIdEsdadoDocumento', 0);
                    limpiaAtributo('lblNomEstadoDocumento', 0);
                    limpiaAtributo('cmbTipoDocumento', 0);

                    limpiarDivEditarJuan('datosDeHistoriaClinicaPaciente')

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'ordenesCPR':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
            idQuery = 480;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar('1');
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributo('txtFechaDesdeFolio'));
            add_valores_a_mandar(valorAtributo('txtFechaHastaFolio'));
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id cita', 'Hora', 'Espera Admision', 'id Paciente', 'Identificacion', 'HC_pdf',
                    'Paciente', 'idEspecialidad', 'Especialidad', 'Sub Especialidad', 'idTipo', 'Tipo', 'id_Estado', 'Estado',
                    'Admision', 'IdAdmision', 'idTipoAdmision', 'TipoAdmision'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id_cita', index: 'id_cita', hidden: true },
                    { name: 'Hora', index: 'Hora', width: anchoP(ancho, 3) },
                    { name: 'Espera', index: 'Espera', width: anchoP(ancho, 6) },
                    { name: 'idPaciente', index: 'idPaciente', hidden: true },
                    { name: 'identificacion_Paciente', index: 'identificacion_Paciente', width: anchoP(ancho, 6) },
                    { name: 'HC_pdf', index: 'HC_pdf', width: anchoP(ancho, 2) },
                    { name: 'NomCompleto', index: 'NomCompleto', width: anchoP(ancho, 17) },
                    { name: 'idEspecialidad', index: 'idEspecialidad', hidden: true },
                    { name: 'nomEspecialidad', index: 'nomEspecialidad', hidden: true },
                    { name: 'nomSubEspecialidad', index: 'nomSubEspecialidad', width: anchoP(ancho, 9) },
                    { name: 'idTipo', index: 'idTipo', hidden: true },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 6) },
                    { name: 'id_Estado', index: 'auditado', hidden: true },
                    { name: 'Estado', index: 'auditado', width: anchoP(ancho, 7) },
                    { name: 'Admision', index: 'Admision', width: anchoP(ancho, 11) },
                    { name: 'idAdmision', index: 'idAdmision', hidden: true },
                    { name: 'idTipoAdmision', index: 'idTipoAdmision', width: anchoP(ancho, 3) },
                    { name: 'TipoAdmision', index: 'TipoAdmision', hidden: true },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 320,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPacientes').getRowData(rowid);

                    estad = 0;
                    asignaAtributo('lblIdPaciente', datosRow.idPaciente, estad);
                    asignaAtributo('lblIdHc', datosRow.id_hc, 1);
                    asignaAtributo('lblIdentificacionPaciente', datosRow.identificacion_Paciente, 1);
                    asignaAtributo('lblNombrePaciente', datosRow.NomCompleto, 1);
                    asignaAtributo('lblIdEspecialidad', datosRow.idEspecialidad, 1);
                    asignaAtributo('lblNombreEspecialidad', datosRow.nomEspecialidad, 1);
                    asignaAtributo('lblIdAdmision', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdAdmisionAgen', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdTipoAdmision', datosRow.idTipoAdmision, 1);
                    asignaAtributo('lblTipoAdmision', datosRow.TipoAdmision, 1);

                    buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                    ocultar('divBuscar')// pa que no estorbe 	
                    ocultar('divAntecedentes')
                    ocultar('divRevisionPorSistemas')
                    ocultar('divExamenFisico')
                    ocultar('divContenidos')
                    ocultar('divDiagnosticos')
                    ocultar('divProcedimientos')
                    ocultar('divProcedimientosCir')
                    ocultar('divHojaGastos')
                    limpiaAtributo('lblIdDocumento', 0);
                    limpiaAtributo('txtIdEsdadoDocumento', 0);
                    limpiaAtributo('lblNomEstadoDocumento', 0);
                    limpiaAtributo('cmbTipoDocumento', 0);

                    limpiarDivEditarJuan('datosDeHistoriaClinicaPaciente')

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'ordenesINT':

            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
            idQuery = 482;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id cita', 'Hora', 'Espera Admisi�n', 'id Paciente', 'Identificaci�n', 'HC_pdf',
                    'Paciente', 'idEspecialidad', 'Especialidad', 'Sub Especialidad', 'idTipo', 'Tipo', 'id_Estado', 'id_administradora', 'administradora', 'Estado',
                    'Admision', 'IdAdmision', 'idTipoAdmision', 'TipoAdmision'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id_cita', index: 'id_cita', hidden: true },
                    { name: 'Hora', index: 'Hora', width: anchoP(ancho, 3) },
                    { name: 'Espera', index: 'Espera', width: anchoP(ancho, 6) },
                    { name: 'idPaciente', index: 'idPaciente', hidden: true },
                    { name: 'identificacion_Paciente', index: 'identificacion_Paciente', width: anchoP(ancho, 6) },
                    { name: 'HC_pdf', index: 'HC_pdf', width: anchoP(ancho, 2) },
                    { name: 'NomCompleto', index: 'NomCompleto', width: anchoP(ancho, 17) },
                    { name: 'idEspecialidad', index: 'idEspecialidad', hidden: true },
                    { name: 'nomEspecialidad', index: 'nomEspecialidad', hidden: true },
                    { name: 'nomSubEspecialidad', index: 'nomSubEspecialidad', width: anchoP(ancho, 9) },
                    { name: 'idTipo', index: 'idTipo', hidden: true },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 6) },
                    { name: 'id_Estado', index: 'auditado', hidden: true },
                    { name: 'id_administradora', index: 'id_administradora', hidden: true },
                    { name: 'administradora', index: 'administradora', hidden: true },
                    { name: 'Estado', index: 'auditado', width: anchoP(ancho, 7) },
                    { name: 'Admision', index: 'Admision', width: anchoP(ancho, 11) },
                    { name: 'idAdmision', index: 'idAdmision', hidden: true },
                    { name: 'idTipoAdmision', index: 'idTipoAdmision', hidden: true },
                    { name: 'TipoAdmision', index: 'TipoAdmision', width: anchoP(ancho, 3) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 320,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPacientes').getRowData(rowid);

                    estad = 0;
                    asignaAtributo('lblIdPaciente', datosRow.idPaciente, estad);
                    asignaAtributo('lblIdHc', datosRow.id_hc, 1);
                    asignaAtributo('lblIdentificacionPaciente', datosRow.identificacion_Paciente, 1);
                    asignaAtributo('lblNombrePaciente', datosRow.NomCompleto, 1);
                    asignaAtributo('lblIdEspecialidad', datosRow.idEspecialidad, 1);
                    asignaAtributo('lblNombreEspecialidad', datosRow.nomEspecialidad, 1);
                    asignaAtributo('lblIdAdmision', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdTipoAdmision', datosRow.idTipoAdmision, 1);
                    asignaAtributo('lblTipoAdmision', datosRow.TipoAdmision, 1);

                    asignaAtributo('lblNomAdministradora', datosRow.administradora, 1);
                    asignaAtributo('lblIdAdministradora', datosRow.id_administradora, 1);

                    buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                    ocultar('divBuscar')// pa que no estorbe 	
                    ocultar('divAntecedentes')
                    ocultar('divRevisionPorSistemas')
                    ocultar('divExamenFisico')
                    ocultar('divContenidos')
                    ocultar('divDiagnosticos')
                    ocultar('divProcedimientos')
                    ocultar('divAdmMedicamentos')
                    ocultar('divHojaGastos')
                    ocultar('divMonitorizacion')
                    ocultar('divArchivosAdjuntos')
                    limpiaAtributo('lblIdDocumento', 0);
                    limpiaAtributo('txtIdEsdadoDocumento', 0);
                    limpiaAtributo('lblNomEstadoDocumento', 0);
                    limpiaAtributo('cmbTipoDocumento', 0);

                    limpiarDivEditarJuan('datosDeHistoriaClinicaPaciente')

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'ordenesLAP':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
            idQuery = 670;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id cita', 'Hora', 'Espera Admision', 'id Paciente', 'Identificacion', 'DOC_pdf',
                    'Paciente', 'edad', 'idEspecialidad', 'Especialidad', 'Sub Especialidad', 'idTipo', 'Tipo', 'id_Administradora', 'Administradora',
                    'Admision', 'IdAdmision', 'idTipoAdmision', 'TipoAdmision', 'Auditado'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id_cita', index: 'id_cita', hidden: true },
                    { name: 'Hora', index: 'Hora', width: anchoP(ancho, 3) },
                    { name: 'Espera', index: 'Espera', width: anchoP(ancho, 6) },
                    { name: 'idPaciente', index: 'idPaciente', hidden: true },
                    { name: 'identificacion_Paciente', index: 'identificacion_Paciente', width: anchoP(ancho, 6) },
                    { name: 'DOC_pdf', index: 'DOC_pdf', width: anchoP(ancho, 2) },
                    { name: 'NomCompleto', index: 'NomCompleto', width: anchoP(ancho, 17) },
                    { name: 'edad', index: 'edad', width: anchoP(ancho, 3) },
                    { name: 'idEspecialidad', index: 'idEspecialidad', hidden: true },
                    { name: 'nomEspecialidad', index: 'nomEspecialidad', hidden: true },
                    { name: 'nomSubEspecialidad', index: 'nomSubEspecialidad', hidden: true },
                    { name: 'idTipo', index: 'idTipo', hidden: true },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 6) },
                    { name: 'id_Administradora', index: 'id_Administradora', hidden: true },
                    { name: 'Administradora', index: 'Administradora', width: anchoP(ancho, 7) },
                    { name: 'Admision', index: 'Admision', width: anchoP(ancho, 11) },
                    { name: 'idAdmision', index: 'idAdmision', hidden: true },
                    { name: 'idTipoAdmision', index: 'idTipoAdmision', width: anchoP(ancho, 3) },
                    { name: 'TipoAdmision', index: 'TipoAdmision', hidden: true },
                    { name: 'Auditado', index: 'Auditado', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 320,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPacientes').getRowData(rowid);

                    estad = 0;
                    asignaAtributo('lblIdPaciente', datosRow.idPaciente, estad);
                    asignaAtributo('lblIdHc', datosRow.id_hc, 1);
                    asignaAtributo('lblIdentificacionPaciente', datosRow.identificacion_Paciente, 1);
                    asignaAtributo('lblNombrePaciente', datosRow.NomCompleto, 1);
                    asignaAtributo('lblIdEspecialidad', datosRow.idEspecialidad, 1);
                    asignaAtributo('lblNombreEspecialidad', datosRow.nomEspecialidad, 1);
                    asignaAtributo('lblIdAdmision', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdAdmisionAgen', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdTipoAdmision', datosRow.idTipoAdmision, 1);
                    asignaAtributo('lblTipoAdmision', datosRow.TipoAdmision, 1);
                    asignaAtributo('lblIdCita', datosRow.ID_AGENDA_DETALLE, 1);
                    asignaAtributo('lblIdAdministradora', datosRow.id_Administradora, 1);
                    asignaAtributo('lblNomAdministradora', datosRow.Administradora, 1);
                    asignaAtributo('lblEdadPaciente', datosRow.edad, 1);


                    /*if(datosRow.DOC_pdf == ''){
                    	alert('FALTA DOCUMENTO IDENTIFICACION PACIENTE.')
                    }*/
                    $('#divPdfIdentificacion').html(datosRow.DOC_pdf);

                    buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                    ocultar('divBuscar')// pa que no estorbe 	
                    ocultar('divAntecedentes')
                    ocultar('divRevisionPorSistemas')
                    ocultar('divExamenFisico')
                    ocultar('divContenidos')
                    ocultar('divDiagnosticos')
                    ocultar('divProcedimientos')
                    ocultar('divProcedimientosCir')
                    ocultar('divHojaGastos')
                    limpiaAtributo('lblIdDocumento', 0);
                    limpiaAtributo('txtIdEsdadoDocumento', 0);
                    limpiaAtributo('lblNomEstadoDocumento', 0);
                    limpiaAtributo('cmbTipoDocumento', 0);

                    limpiarDivEditarJuan('datosDeHistoriaClinicaPaciente')

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'ordenesIndividuales':

            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
            idQuery = 64;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id cita', 'Hora', 'Espera Admision', 'id Paciente', 'Identificacion', 'DOC_pdf',
                    'Paciente', 'Edad', 'idEspecialidad', 'Especialidad', 'Sub Especialidad', 'idTipo', 'Tipo', 'id_Estado', 'Estado',
                    'Admision', 'IdAdmision', 'idTipoAdmision', 'TipoAdmision'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id_cita', index: 'id_cita', hidden: true },
                    { name: 'Hora', index: 'Hora', width: anchoP(ancho, 3) },
                    { name: 'Espera', index: 'Espera', width: anchoP(ancho, 6) },
                    { name: 'idPaciente', index: 'idPaciente', hidden: true },
                    { name: 'identificacion_Paciente', index: 'identificacion_Paciente', width: anchoP(ancho, 6) },
                    { name: 'DOC_pdf', index: 'DOC_pdf', width: anchoP(ancho, 3) },
                    { name: 'NomCompleto', index: 'NomCompleto', width: anchoP(ancho, 17) },
                    { name: 'edad', index: 'edad', width: anchoP(ancho, 3) },
                    { name: 'idEspecialidad', index: 'idEspecialidad', hidden: true },
                    { name: 'nomEspecialidad', index: 'nomEspecialidad', hidden: true },
                    { name: 'nomSubEspecialidad', index: 'nomSubEspecialidad', width: anchoP(ancho, 9) },
                    { name: 'idTipo', index: 'idTipo', hidden: true },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 6) },
                    { name: 'id_Estado', index: 'auditado', hidden: true },
                    { name: 'Estado', index: 'auditado', width: anchoP(ancho, 7) },
                    { name: 'Admision', index: 'Admision', width: anchoP(ancho, 11) },
                    { name: 'idAdmision', index: 'idAdmision', hidden: true },
                    { name: 'idTipoAdmision', index: 'idTipoAdmision', hidden: true },
                    { name: 'TipoAdmision', index: 'TipoAdmision', width: anchoP(ancho, 3) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 320,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPacientes').getRowData(rowid);

                    estad = 0;
                    asignaAtributo('lblIdPaciente', datosRow.idPaciente, estad);
                    asignaAtributo('lblIdHc', datosRow.id_hc, 1);
                    asignaAtributo('lblIdentificacionPaciente', datosRow.identificacion_Paciente, 1);
                    asignaAtributo('lblNombrePaciente', datosRow.NomCompleto, 1);
                    asignaAtributo('lblIdEspecialidad', datosRow.idEspecialidad, 1);
                    asignaAtributo('lblNombreEspecialidad', datosRow.nomEspecialidad, 1);
                    asignaAtributo('lblIdAdmision', datosRow.idAdmision, 1);
                    asignaAtributo('lblIdTipoAdmision', datosRow.idTipoAdmision, 1);
                    asignaAtributo('lblTipoAdmision', datosRow.TipoAdmision, 1);

                    asignaAtributo('lblNomAdministradora', datosRow.id_Estado, 1);
                    asignaAtributo('lblIdAdministradora', datosRow.Tipo, 1);

                    /*if(datosRow.DOC_pdf == ''){
                    	alert('FALTA DOCUMENTO IDENTIFICACION PACIENTE.')
                    }*/
                    $('#divPdfIdentificacion').html(datosRow.DOC_pdf);

                    buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                    ocultar('divBuscar')// pa que no estorbe 	
                    ocultar('divAntecedentes')
                    ocultar('divRevisionPorSistemas')
                    ocultar('divExamenFisico')
                    ocultar('divContenidos')
                    ocultar('divDiagnosticos')
                    ocultar('divProcedimientos')
                    ocultar('divAdmMedicamentos')
                    ocultar('divHojaGastos')
                    ocultar('divMonitorizacion')
                    ocultar('divArchivosAdjuntos')
                    limpiaAtributo('lblIdDocumento', 0);
                    limpiaAtributo('txtIdEsdadoDocumento', 0);
                    limpiaAtributo('lblNomEstadoDocumento', 0);
                    limpiaAtributo('cmbTipoDocumento', 0);

                    limpiarDivEditarJuan('datosDeHistoriaClinicaPaciente')

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listArchivosAdjuntos':
            //ancho = 700;
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=373&parametros="; //SUSTITUYE 16

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') != '')
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            else
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'Imagen', 'Nombre Identificacion', 'Fecha elaboro', 'Usuario elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 5) },
                    { name: 'imagen', index: 'imagen', width: anchoP(ancho, 5) },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 62) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 14) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', width: anchoP(ancho, 12) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', datosRow.Nombre, 0)
                    asignaAtributo('lblImagen', datosRow.imagen, 0)
                    asignaAtributo('lblUsuarioElaboro', datosRow.usuario_elaboro, 0)

                    mostrarVentanita('divVentanitaArchivosAdjuntos');
                    $('#drag' + ventanaActual.num).find("#idSubVentanita").css('top', 5 + 'px');
                },
                height: 200,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listArchivosAdjuntosIdentificacion':
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=675&parametros="; //SUSTITUYE 178
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') != '')
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            else
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'Imagen', 'Nombre Identificacion', 'Fecha elaboro', 'Usuario elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 5) },
                    { name: 'imagen', index: 'imagen', width: anchoP(ancho, 5) },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 62) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 14) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', width: anchoP(ancho, 12) }
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', datosRow.Nombre, 0)
                    asignaAtributo('lblImagen', datosRow.imagen, 0)
                    asignaAtributo('lblUsuarioElaboro', datosRow.usuario_elaboro, 0)


                    mostrarVentanita('divVentanitaArchivosAdjuntos');
                    $('#drag' + ventanaActual.num).find("#idSubVentanita").css('top', 5 + 'px');
                },
                height: 200,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listArchivosAdjuntosRemision':
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=676&parametros="; //SUSTITUYE 177
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') != '')
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            else
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id Cita', 'Imagen', 'Nombre Remision', 'No Remision', 'Fecha elaboro', 'Usuario elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_Cita', index: 'id_Cita', width: anchoP(ancho, 5) },
                    { name: 'imagen', index: 'imagen', width: anchoP(ancho, 5) },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 42) },
                    { name: 'Remision', index: 'Remision', width: anchoP(ancho, 21) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 14) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', width: anchoP(ancho, 12) }
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', datosRow.Nombre, 0)
                    asignaAtributo('lblImagen', datosRow.imagen, 0)
                    asignaAtributo('lblUsuarioElaboro', datosRow.usuario_elaboro, 0)


                    mostrarVentanita('divVentanitaArchivosAdjuntos');
                    $('#drag' + ventanaActual.num).find("#idSubVentanita").css('top', 5 + 'px');
                },
                height: 200,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listArchivosAdjuntosCarnet':
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=677&parametros="; //SUSTITUYE 176
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') != '')
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            else
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'Imagen', 'Nombre Consentimiento', 'Fecha elaboro', 'Usuario elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 5) },
                    { name: 'imagen', index: 'imagen', width: anchoP(ancho, 5) },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 62) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 14) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', width: anchoP(ancho, 12) }
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', datosRow.Nombre, 0)
                    asignaAtributo('lblImagen', datosRow.imagen, 0)
                    asignaAtributo('lblUsuarioElaboro', datosRow.usuario_elaboro, 0)


                    mostrarVentanita('divVentanitaArchivosAdjuntos');
                    $('#drag' + ventanaActual.num).find("#idSubVentanita").css('top', 5 + 'px');
                },
                height: 200,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listAyudasDiagnost':
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=678&parametros="; //SUSTITUYE 476
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') != '')
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            else
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'Imagen', 'Nombre', 'Tipo', 'Sitio', 'Observacion', 'Estado', 'Fecha elaboro', 'Usuario elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 6) },
                    { name: 'imagen', index: 'imagen', width: anchoP(ancho, 6) },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 15) },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 17) },
                    { name: 'Sitio', index: 'Sitio', width: anchoP(ancho, 9) },
                    { name: 'Observacion', index: 'Observacion', width: anchoP(ancho, 18) },
                    { name: 'Estado', index: 'Estado', width: anchoP(ancho, 18) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 15) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', width: anchoP(ancho, 12) }
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', datosRow.Nombre, 0)
                    asignaAtributo('lblImagen', datosRow.imagen, 0)
                    asignaAtributo('lblUsuarioElaboro', datosRow.usuario_elaboro, 0)


                    mostrarVentanita('divVentanitaArchivosAdjuntos');
                    $('#drag' + ventanaActual.num).find("#idSubVentanita").css('top', 5 + 'px');
                },
                height: 200,
                width: 1100,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listAdjuntos':
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=373&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'Nombre', 'Fecha elaboro', 'Usuario elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 10) },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 60) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 15) },
                    { name: 'usuario_crea', index: 'usuario_crea', width: anchoP(ancho, 15) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', datosRow.Nombre, 0)
                    //			 abrirVentanita('divVentanitaAnestesia');
                    mostrarVentanita('divVentanitaAnestesia');
                    $('#drag' + ventanaActual.num).find("#idSubVentanita").css('top', 555 + 'px');
                },
                height: 100,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listArchivosAdjuntosVarios':
            //ancho=700;
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=505&parametros=";
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') != '')
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            else
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'ID_PACIENTE', 'ID_TIPO', 'Imagen', 'Nombre Documento', 'Observacion', 'Fecha elaboro', 'Usuario elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 4) },
                    { name: 'id_paciente', index: 'id_paciente', hidden: true },
                    { name: 'id_tipo', index: 'id_tipo', hidden: true },
                    { name: 'imagen', index: 'imagen', width: anchoP(ancho, 2) },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 30) },
                    { name: 'observacion', index: 'observacion', width: anchoP(ancho, 28) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 18) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', width: anchoP(ancho, 18) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', datosRow.id_paciente, 0);
                    asignaAtributo('lblImagen', datosRow.imagen, 0)
                    asignaAtributo('lblUsuarioElaboro', datosRow.usuario_elaboro, 0)

                    mostrarVentanita('divVentanitaArchivosAdjuntos');
                    $('#drag' + ventanaActual.num).find("#idSubVentanita").css('top', 5 + 'px');
                },
                height: 200,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listPreOperatorio':
            limpiarDivEditarJuan(arg);
            //			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','400px');				 
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=386&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdAdmision'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'ID_SITIO', 'SITIO', 'ID_PROCEDIMIENTO', 'NOMBRE_PROCEDIMI', 'OBSERVACION'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_SITIO', index: 'ID_SITIO', hidden: true },
                    { name: 'SITIO', index: 'SITIO', width: anchoP(ancho, 20) },
                    { name: 'ID_PROCEDIMIENTO', index: 'ID_PROCEDIMIENTO', width: anchoP(ancho, 20) },
                    { name: 'NOMBRE_PROCEDIMI', index: 'NOMBRE_PROCEDIMI', width: anchoP(ancho, 40) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 20) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdSitioPre', datosRow.ID_SITIO, 0);
                    asignaAtributo('lblNomSitioPre', datosRow.SITIO, 0);
                    asignaAtributo('lblIdProcedimiento', datosRow.ID_PROCEDIMIENTO, 0);

                    asignaAtributo('lblNombreElemento', datosRow.NOMBRE_PROCEDIMI, 0)

                    mostrarVentanita('divVentanitaPreO');

                    $('#drag' + ventanaActual.num).find("#idSubVentanita").css('top', 555 + 'px');
                },
                height: 100,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listPosOperatorio':
            limpiarDivEditarJuan(arg);
            //			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','400px');				 
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=709&parametros="; //SUSTITUYE 16 //sustituye a 390
            add_valores_a_mandar(valorAtributo('lblIdAdmision'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'FOLIO','ID', 'ID_SITIO', 'SITIO', 'ID_PROCEDIMIENTO', 'NOMBRE_PROCEDIMIENTO', 'OBSERVACION'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_FOLIO', index: 'ID_FOLIO', width: anchoP(ancho, 8) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_SITIO', index: 'ID_SITIO', hidden: true },
                    { name: 'SITIO', index: 'SITIO', width: anchoP(ancho, 15) },
                    { name: 'ID_PROCEDIMIENTO', index: 'ID_PROCEDIMIENTO', width: anchoP(ancho, 15) },
                    { name: 'NOMBRE_PROCEDIMI', index: 'NOMBRE_PROCEDIMIENTO', width: anchoP(ancho, 40) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 50) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.ID, 0);
                    asignaAtributo('lblNombreElemento', concatenarCodigoNombre(datosRow.ID_PROCEDIMIENTO, datosRow.NOMBRE_PROCEDIMI), 0)
                    //			 abrirVentanita('divVentanitaAnestesia');
                    desHabilitar('btnEliminarDx', valorAtributo('txtIdEsdadoDocumento'));
                    mostrarVentanita('divVentanitaAnestesia');

                    $('#drag' + ventanaActual.num).find("#idSubVentanita").css('top', 555 + 'px');
                },
                height: 100,
                width: ancho + 80,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listPersonalProc':
            limpiarDivEditarJuan(arg);
            //			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','400px');				 
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=393&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdAdmision'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'ID', 'ID_PROFESION', 'PROFESION', 'ID_PERSONAL', 'PERSONAL', 'OBSERVACION'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_PROFESION', index: 'ID_PROFESION', hidden: true },
                    { name: 'PROFESION', index: 'PROFESION', width: anchoP(ancho, 20) },
                    { name: 'ID_PERSONAL', index: 'ID_PERSONAL', width: anchoP(ancho, 20) },
                    { name: 'PERSONAL', index: 'PERSONAL', width: anchoP(ancho, 40) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 20) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.ID, 0);
                    asignaAtributo('lblNombreElemento', concatenarCodigoNombre(datosRow.PROFESION, datosRow.PERSONAL), 0)
                    //			 abrirVentanita('divVentanitaAnestesia');

                    mostrarVentanita('divVentanitaPersonalProc');

                    $('#drag' + ventanaActual.num).find("#divVentanitaPersonalProc").css('top', 5 + 'px');
                },
                height: 100,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listAnestesia':
            limpiarDivEditarJuan(arg);
            //			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','400px');				 
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=61&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'Tipo', 'id_grupo', 'grupo', 'Hora', 'Observaciones', 'Fecha elabor�'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 10) },
                    { name: 'id_grupo', index: 'id_grupo', hidden: true },
                    { name: 'grupo', index: 'grupo', width: anchoP(ancho, 20) },
                    { name: 'Hora', index: 'Hora', width: anchoP(ancho, 10) },
                    { name: 'observacion', index: 'observacion', width: anchoP(ancho, 25) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', concatenarCodigoNombre(datosRow.Tipo, datosRow.grupo), 0)
                    //			 abrirVentanita('divVentanitaAnestesia');

                    mostrarVentanita('divVentanitaAnestesia');

                    $('#drag' + ventanaActual.num).find("#idSubVentanita").css('top', 555 + 'px');
                },
                height: 100,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listMonitoreo':
            limpiarDivEditarJuan(arg);
            //			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','400px');				 
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=326&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'Tipo', 'id_grupo', 'grupo', 'Tiene', 'Controlada', 'Observaciones', 'Fecha elabor�'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 10) },
                    { name: 'id_grupo', index: 'id_grupo', hidden: true },
                    { name: 'grupo', index: 'grupo', width: anchoP(ancho, 20) },
                    { name: 'tiene', index: 'tiene', width: anchoP(ancho, 5) },
                    { name: 'controlada', index: 'controlada', width: anchoP(ancho, 5) },
                    { name: 'observacion', index: 'observacion', width: anchoP(ancho, 30) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', concatenarCodigoNombre(datosRow.Tipo, datosRow.grupo), 0)
                    abrirVentanita('divVentanitaAntecedentes');
                },
                height: 100,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listGrillaProcedimientoGestion':
            ancho = 1030;
            valores_a_mandar = pag;
            if (valorAtributo('txt_bandejaClase') == 'gestionProcedimiento') {
                valores_a_mandar = valores_a_mandar + "?idQuery=2" + "&parametros=";
            } else
                valores_a_mandar = valores_a_mandar + "?idQuery=488" + "&parametros=";

            add_valores_a_mandar(valorAtributo('txtBusIdProcedimientoBus'));

            add_valores_a_mandar(valorAtributo('txtFechaDesdeP'));
            add_valores_a_mandar(valorAtributo('txtFechaHastaP'));

            add_valores_a_mandar(valorAtributo('cmbClaseBus'));
            add_valores_a_mandar(valorAtributo('cmbTipoBus'));
            add_valores_a_mandar(valorAtributo('cmbEstadoBus'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'ID', 'ID_PLAN_EVOLUCION', 'ID_PACIENTE', 'IDENTIFICACION', 'PACIENTE', 'ID PROC', 'NOMBRE', 'CANTIDAD', 'ID_SITIO', 'SITIO', 'INDICACION', 'ID_CLASE', 'CLASE', 'ID_PROFESIONAL', 'NOM_PROFESIONAL', 'TIPO', 'ID_ESTADO', 'ESTADO', 'OBSERVACION_GESTION', 'FECHA CREACION'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_PLAN_EVOLUCION', index: 'ID_PLAN_EVOLUCION', width: anchoP(ancho, 2) },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 10) },
                    { name: 'PACIENTE', index: 'PACIENTE', width: anchoP(ancho, 20) },

                    { name: 'ID_PROC', index: 'ID_PROC', hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 30) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', hidden: true },
                    { name: 'ID_SITIO', index: 'ID_SITIO', hidden: true },
                    { name: 'SITIO', index: 'SITIO', width: anchoP(ancho, 10) },
                    { name: 'INDICACION', index: 'INDICACION', hidden: true },
                    { name: 'ID_CLASE', index: 'ID_CLASE', hidden: true },
                    { name: 'CLASE', index: 'CLASE', hidden: true },
                    { name: 'ID_PROFESIONAL', index: 'ID_PROFESIONAL', hidden: true },
                    { name: 'NOM_PROFESIONAL', index: 'NOM_PROFESIONAL', width: anchoP(ancho, 10) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 8) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', width: anchoP(ancho, 8) },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 8) },
                    { name: 'OBSERVACION_GESTION', index: 'OBSERVACION_GESTION', width: anchoP(ancho, 8) },
                    { name: 'FECHA_CREACION', index: 'FECHA_CREACION', width: anchoP(ancho, 8) },
                ],
                height: 500,
                width: ancho,

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdPaciente', datosRow.ID_PACIENTE, 0)
                    asignaAtributo('lblIdentificacionPaciente', datosRow.IDENTIFICACION, 0)
                    asignaAtributo('lblNombrePaciente', datosRow.PACIENTE, 0)

                    asignaAtributo('lblId', datosRow.ID, 0)
                    asignaAtributo('lblIdPlanE', datosRow.ID_PLAN_EVOLUCION, 0)
                    asignaAtributo('lblIdProc', datosRow.ID_PROC, 0)
                    asignaAtributo('lblNombreProc', datosRow.NOMBRE, 0)
                    asignaAtributo('lblIdSitio', datosRow.ID_SITIO, 0)
                    asignaAtributo('lblSitio', datosRow.SITIO, 0)
                    asignaAtributo('lblClase', datosRow.CLASE, 0)
                    asignaAtributo('lblCantidad', datosRow.CANTIDAD, 0)
                    asignaAtributo('lblIndicacion', datosRow.INDICACION, 0)
                    asignaAtributo('lblTipo', datosRow.TIPO, 0)
                    asignaAtributo('lblIdProfRemite', datosRow.ID_PROFESIONAL, 0)
                    asignaAtributo('cmbEstado', datosRow.ID_ESTADO, 0)

                    limpiaAtributo('txt_observacionGestion', 0)
                    mostrar('divVentanitaEditGestionProcedimientos')
                    cargarTableListaEsperaCirugia();
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaGestionProceSolicitud':
            ancho = 1300;
            valores_a_mandar = pag;
            if (valorAtributo('cmbTipoSolicitud') == 'I') {
                valores_a_mandar = valores_a_mandar + "?idQuery=629" + "&parametros=";
                add_valores_a_mandar(valorAtributo('txtBusIdProcedimientoBus'));

                add_valores_a_mandar(valorAtributo('txtFechaDesdeP'));
                add_valores_a_mandar(valorAtributo('txtFechaHastaP'));
                add_valores_a_mandar(valorAtributo('cmbClaseBus'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(valorAtributo('cmbTipoBus'));
                add_valores_a_mandar(valorAtributo('cmbEstadoBus'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                add_valores_a_mandar(valorAtributo('txtBusSolicitudBus'));
                add_valores_a_mandar(valorAtributo('txtBusRadicado'));
                add_valores_a_mandar(valorAtributo('txtBusAutorizacion'));

            } else {
                valores_a_mandar = valores_a_mandar + "?idQuery=631" + "&parametros=";
                add_valores_a_mandar(valorAtributo('txtFechaDesdeP'));
                add_valores_a_mandar(valorAtributo('txtFechaHastaP'));
            }

            $('#drag' + ventanaActual.num).find("#listGrillaProcedimientoGestion").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'ID', 'ID_PLAN_EVOLUCION', 'ID_PACIENTE', 'IDENTIFICACION', 'PACIENTE', 'ID PROC', 'NOMBRE', 'CANTIDAD', 'ID_SITIO', 'SITIO',
                    'INDICACION', 'ID_CLASE', 'CLASE', 'ID_PROFESIONAL', 'NOM_PROFESIONAL', 'TIPO', 'ID_ESTADO', 'ESTADO', 'OBSERVACION_GESTION', 'FECHA CREACION',
                    'ID SOLICITUD', 'FECHA_SOLICITUD', 'ESTADO_SOLICITUD',
                    'No radicado', 'No autorizacion', 'Fecha autorizacion', 'diasEspera'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_PLAN_EVOLUCION', index: 'ID_PLAN_EVOLUCION', width: anchoP(ancho, 2) },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 8) },
                    { name: 'PACIENTE', index: 'PACIENTE', width: anchoP(ancho, 20) },

                    { name: 'ID_PROC', index: 'ID_PROC', hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 30) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', hidden: true },
                    { name: 'ID_SITIO', index: 'ID_SITIO', hidden: true },
                    { name: 'SITIO', index: 'SITIO', width: anchoP(ancho, 10) },
                    { name: 'INDICACION', index: 'INDICACION', hidden: true },
                    { name: 'ID_CLASE', index: 'ID_CLASE', hidden: true },
                    { name: 'CLASE', index: 'CLASE', hidden: true },
                    { name: 'ID_PROFESIONAL', index: 'ID_PROFESIONAL', hidden: true },
                    { name: 'NOM_PROFESIONAL', index: 'NOM_PROFESIONAL', width: anchoP(ancho, 10) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 5) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 8) },
                    { name: 'OBSERVACION_GESTION', index: 'OBSERVACION_GESTION', width: anchoP(ancho, 8) },
                    { name: 'FECHA_CREACION', index: 'FECHA_CREACION', width: anchoP(ancho, 8) },
                    { name: 'ID_SOLICITUD', index: 'ID_SOLICITUD', width: anchoP(ancho, 5) },
                    { name: 'FECHA_SOLICITUD', index: 'FECHA_SOLICITUD', width: anchoP(ancho, 8) },
                    { name: 'ESTADO_SOLICITUD', index: 'ESTADO_SOLICITUD', width: anchoP(ancho, 8) },

                    { name: 'numero_radicado', index: 'numero_radicado', width: anchoP(ancho, 8) },
                    { name: 'numero_autorizacion', index: 'numero_autorizacion', width: anchoP(ancho, 8) },
                    { name: 'fecha_autorizacion', index: 'fecha_autorizacion', width: anchoP(ancho, 8) },
                    { name: 'diasEspera', index: 'diasEspera', width: anchoP(ancho, 8) },
                ],
                height: 500,
                width: ancho + 100,

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaProcedimientoGestion').getRowData(rowid);


                    asignaAtributo('lblIdSolicitud', datosRow.ID_SOLICITUD, 0)
                    asignaAtributo('lblIdAutorizacion', datosRow.numero_autorizacion, 0)

                    asignaAtributo('lblIdPaciente', datosRow.ID_PACIENTE, 0)
                    asignaAtributo('lblIdentificacionPaciente', datosRow.IDENTIFICACION, 0)
                    asignaAtributo('lblNombrePaciente', datosRow.PACIENTE, 0)

                    asignaAtributo('lblId', datosRow.ID, 0)
                    asignaAtributo('lblIdPlanE', datosRow.ID_PLAN_EVOLUCION, 0)
                    asignaAtributo('lblIdProc', datosRow.ID_PROC, 0)
                    asignaAtributo('lblNombreProc', datosRow.NOMBRE, 0)
                    asignaAtributo('lblIdSitio', datosRow.ID_SITIO, 0)
                    asignaAtributo('lblSitio', datosRow.SITIO, 0)
                    asignaAtributo('lblClase', datosRow.CLASE, 0)
                    asignaAtributo('lblCantidad', datosRow.CANTIDAD, 0)
                    asignaAtributo('lblIndicacion', datosRow.INDICACION, 0)
                    asignaAtributo('lblTipo', datosRow.TIPO, 0)
                    asignaAtributo('lblIdProfRemite', datosRow.ID_PROFESIONAL, 0)
                    asignaAtributo('cmbEstado', datosRow.ID_ESTADO, 0)

                    limpiaAtributo('txt_observacionGestion', 0)
                    mostrar('divVentanitaEditGestionProcedimientos')
                    cargarTableListaEsperaCirugia();
                },

            });
            $('#drag' + ventanaActual.num).find("#listGrillaProcedimientoGestion").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

    }
}

function buscarHC(arg, pag) {
    pagina = pag;
    ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
    //    alto= ($('#drag'+ventanaActual.num).find("#divContenido").width())*92/100;	
    switch (arg) {

        case 'listTratamientoPorDiente':
            ancho = ($('#drag' + ventanaActual.num).find("#idTablaTratamEjec").width()) - 10;
            limpiarDivEditarJuan(arg);

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=701&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#listTratamientoPorDiente").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'Diente', 'Descripcion'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'diente', index: 'diente', width: anchoP(ancho, 10) },
                    { name: 'descripcion', index: 'descripcion', width: anchoP(ancho, 80) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdTnsDienteTratamien', datosRow.id, 0)
                    asignaAtributo('lblIdDienteTratamien', datosRow.diente, 0)
                    asignaAtributo('txtDesTratamientoPorDiente', datosRow.descripcion, 0)
                },
                height: 140,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#listTratamientoPorDiente").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            $('#drag' + ventanaActual.num).find("#listTratamientoPorDiente").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listHojaGastosDespachos':

            // limpiarDivEditarJuan(arg); 
            ancho = 1000
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=485&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAdmision'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID_TR', 'ID_DOCUMENTO', 'ID_ARTICULO', 'NOMBRE_GENERICO', 'FACTOR_CONVERSION', 'FORMA_FARMACEUTICA', 'CANTIDAD', 'VALOR_UNITARIO', 'IVA'
                    , 'VALOR_IMPUESTO', 'ID_NATURALEZA', 'LOTE', 'PRECIO_VENTA', 'FECHA_VENCIMIENTO', 'EXITO'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 2) },
                    { name: 'ID_DOCUMENTO', index: 'ID_DOCUMENTO', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 2) },
                    { name: 'NOMBRE_GENERICO', index: 'NOMBRE_GENERICO', width: anchoP(ancho, 60) },
                    { name: 'FACTOR_CONVERSION', index: 'FACTOR_CONVERSION', hidden: true },
                    { name: 'FORMA_FARMACEUTICA', index: 'FORMA_FARMACEUTICA', hidden: true },
                    { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 10) },
                    { name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', hidden: true },
                    { name: 'IVA', index: 'IVA', hidden: true },
                    { name: 'VALOR_IMPUESTO', index: 'VALOR_IMPUESTO', hidden: true },
                    { name: 'ID_NATURALEZA', index: 'ID_NATURALEZA', hidden: true },
                    { name: 'LOTE', index: 'LOTE', width: anchoP(ancho, 10) },
                    { name: 'PRECIO_VENTA', index: 'PRECIO_VENTA', hidden: true },
                    { name: 'FECHA_VENCIMIENTO', index: 'FECHA_VENCIMIENTO', hidden: true },
                    { name: 'EXITO', index: 'EXITO', hidden: true }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 10,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    limpiaAtributo('lblCostoHG', '', 0);
                    limpiaAtributo('txtConsumo', '', 0);
                    asignaAtributo('lblIdTransaccion', datosRow.ID, 0);
                    asignaAtributo('lblIdArticulo', datosRow.ID_ARTICULO, 0);
                    asignaAtributo('lblNombreArticulo', datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('lblCantDespacho', datosRow.CANTIDAD, 0);
                    asignaAtributo('cmbCantConsumo', datosRow.CANTIDAD, 0);
                    asignaAtributo('txtCostoHG', datosRow.VALOR_UNITARIO, 0);
                    /* asignaAtributo('txtConsumo',datosRow.FACTOR_CONVERSION,0); */
                    asignaAtributo('txtConsumo', datosRow.CANTIDAD, 0);
                    setTimeout("calcularCosto()", 100);

                    asignaAtributo('lblCantidadConsumo', datosRow.FACTOR_CONVERSION, 0);
                    asignaAtributo('lblIdUnidad', datosRow.FORMA_FARMACEUTICA, 0);
                    asignaAtributo('lblCantNovedad', '0', 0);
                    asignaAtributo('cmbNovedad', '1', 0);
                    asignaAtributo('txtBanGrilla', '1', 0);
                    habilitar('btnTraerArticulo', 1);
                    habilitar('btnTraerUltimoArticulo', 0);
                    mostrar('divVentanitaTraerArticulo');

                    setTimeout("document.getElementById('txtConsumo').focus()", 300);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listMonitorizacion':
            limpiarDivEditarJuan(arg);
            //			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','400px');				 
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=467&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdAdmision'));

            $('#drag' + ventanaActual.num).find("#listMonitorizacionHH").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'Id_evo', 'Sistolica', 'Diastolica', 'F. Cardiaca', 'F. Respiratoria', 'Temperatura', 'Sat de Oxigeno', 'Hora Monitoreo', 'Observacion', 'Usuario elaboro', 'Fecha elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_EVOLUCION', index: 'ID_EVOLUCION', hidden: true },
                    { name: 'SISTOLICA', index: 'SISTOLICA', width: anchoP(ancho, 8) },
                    { name: 'DIASTOLICA', index: 'DIASTOLICA', width: anchoP(ancho, 8) },
                    { name: 'PULSO', index: 'PULSO', width: anchoP(ancho, 8) },
                    { name: 'RESPIRACION', index: 'RESPIRACION', width: anchoP(ancho, 8) },
                    { name: 'TEMPERATURA', index: 'TEMPERATURA', width: anchoP(ancho, 8) },
                    { name: 'SATURACION_OXIGENO', index: 'SATURACION_OXIGENO', width: anchoP(ancho, 8) },
                    { name: 'HORA_MONITORIZACION', index: 'HORA_MONITORIZACION', width: anchoP(ancho, 20) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 15) },
                    { name: 'USUARIO_ELABORO', index: 'USUARIO_ELABORO', width: anchoP(ancho, 8) },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listMonitorizacionHH').getRowData(rowid);
                    asignaAtributo('lblIdMHH', datosRow.ID, 0);
                    asignaAtributo('lblIdElemento', datosRow.ID_EVOLUCION, 0);
                    asignaAtributo('lblNombreElemento', 'ELIMINAR REGISTRO ', 0)
                    abrirVentanita('divVentanitaMHH');
                },
                height: 100,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#listMonitorizacionHH").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listProcedimientosOrdenes':
            limpiarDivEditarJuan(arg);

            ancho = ($('#drag' + ventanaActual.num).find("#idTablaVentanitaIntercambio").width()) - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=624&parametros=";

            //			 add_valores_a_mandar(valorAtributo('cmbEstadoBusProc'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#listProcedimientosOrdenesIntercambio").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'IdProc', 'observaciones_concepto', 'cod', 'Pos', 'Nombre', 'Cantidad', 'idSitio', 'Sitio', 'Indicaci�n', 'id_est_orden', 'Estado Orden', 'observacion_gestion', 'esperar', 'Envia Autoriza', 'Semaforo', 'Solicitud', 'Fecha_solicitud', 'estado',
                    'Radicado', 'Autorizacion', 'Fecha autorizacion'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'observaciones_concepto', index: 'observaciones_concepto', hidden: true },
                    { name: 'cod', index: 'cod', width: anchoP(ancho, 8) },
                    { name: 'pos', index: 'pos', width: anchoP(ancho, 5) },
                    { name: 'nombre', index: 'nombre', width: anchoP(ancho, 40) },
                    { name: 'cantidad', index: 'cantidad', width: anchoP(ancho, 7) },
                    { name: 'idSitio', index: 'idSitio', hidden: true },
                    { name: 'Sitio', index: 'Sitio', width: anchoP(ancho, 5) },
                    { name: 'Indicacion', index: 'Indicacion', width: anchoP(ancho, 10) },
                    { name: 'id_est_orden', index: 'id_es_orden', hidden: true },
                    { name: 'nom_est_orden', index: 'nom_est_orden', width: anchoP(ancho, 10) },
                    { name: 'observacion_gestion', index: 'observacion_gestion', hidden: true },
                    { name: 'esperar', index: 'esperar', width: anchoP(ancho, 5) },
                    { name: 'envia_autoriza', index: 'envia_autoriza', width: anchoP(ancho, 8) },
                    { name: 'Semaforo', index: 'Semaforo', width: anchoP(ancho, 5) },

                    { name: 'numero_solicitud', index: 'numero_solicitud', width: anchoP(ancho, 8) },
                    { name: 'fecha_solicitud', index: 'fecha_solicitud', hidden: true },
                    { name: 'estado', index: 'estado', width: anchoP(ancho, 7) },

                    { name: 'numero_radicado', index: 'numero_radicado', width: anchoP(ancho, 8) },
                    { name: 'numero_autorizacion', index: 'numero_autorizacion', width: anchoP(ancho, 8) },
                    { name: 'fecha_autorizacion', index: 'fecha_autorizacion', hidden: true },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listProcedimientosOrdenesIntercambio').getRowData(rowid);

                    asignaAtributo('lblIdProcedimientoAutoriza', datosRow.id, 0);
                    asignaAtributo('lblCodigoProcedimientoAutoriza', datosRow.cod, 0);
                    asignaAtributo('lblNomProcedimientoAutoriza', datosRow.nombre + '-' + datosRow.nombre, 0);
                    asignaAtributo('cmbEsperarProcAutoriza', datosRow.esperar, 0);
                    asignaAtributo('cmbIdEnviado', datosRow.envia_autoriza, 0);
                    asignaAtributo('txtJustificacionFolio', datosRow.observaciones_concepto, 0);
                    asignaAtributo('lblIdAutorizacion', datosRow.numero_autorizacion, 0);

                    asignaAtributo('cmbEstadoEditProc', datosRow.id_est_orden, 0);
                    asignaAtributo('txt_observacionGestion', datosRow.observacion_gestion, 0);

                    asignaAtributo('lblIdSolicitud', datosRow.numero_solicitud, 0);
                    asignaAtributo('lblIdAutorizacion', datosRow.numero_autorizacion, 0);




                    if (datosRow.numero_solicitud == '') {
                        habilitar('idBtnSolicitAutoriza', 1)
                    } else {
                        habilitar('idBtnSolicitAutoriza', 0)
                    }
                },
                height: 200,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#listProcedimientosOrdenesIntercambio").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listDocumentosEvolucion':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#idContenedorGrillaDoc").width() - 60);
            valores_a_mandar = pag;
            var qry = "";
            if (valorAtributo('cmbIdEstado') == '0') {
                qry = '54';
            } else {
                qry = '16';
            }
            valores_a_mandar = valores_a_mandar + "?idQuery=" + qry + "&parametros=";

            add_valores_a_mandar(valorAtributo('txtDocFechaDesde'));
            add_valores_a_mandar(valorAtributo('txtDocFechaHasta'));

            add_valores_a_mandar(valorAtributo('cmbIdEspecialidadBus'));
            add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidadBus'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('cmbIdEstado'));
            add_valores_a_mandar(valorAtributo('cmbIdAuditado'));

            $('#drag' + ventanaActual.num).find("#listDocumentosEvolucion").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'idAdmision', 'Fecha Ingreso', 'idPaciente', 'Identificacion', 'Nombre',
                    'idEspecialidad', 'Especialidad', 'idSubEspecialidad', 'SubEspecialidad', 'idProfesional', 'Profesional',
                    'id_evo', 'tipo', 'Nombre Documento',
                    'Auditado', 'nomAuditado', 'Usuario Auditor', 'Observacion_Auditoria', 'Fecha_Auditoria',
                    'idEstadoDoc', 'Estado', 'Administradora',
                    'var1', 'var2', 'var3'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'idAdmision', index: 'idAdmision', hidden: true },
                    { name: 'fecha_Ingreso', index: 'fecha_Ingreso', width: anchoP(ancho, 8) },
                    { name: 'idPaciente', index: 'idPaciente', hidden: true },
                    { name: 'identificacion', index: 'identificacion', hidden: true },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'idEspecialidad', index: 'idEspecialidad', hidden: true },
                    { name: 'Especialidad', index: 'Especialidad', hidden: true },
                    { name: 'idSubEspecialidad', index: 'idSubEspecialidad', hidden: true },
                    { name: 'SubEspecialidad', index: 'SubEspecialidad', width: anchoP(ancho, 10) },
                    { name: 'idProfesional', index: 'idProfesional', hidden: true },
                    { name: 'Profesional', index: 'Profesional', width: anchoP(ancho, 14) },

                    { name: 'id_evo', index: 'id_evo', hidden: true },
                    { name: 'tipo', index: 'tipo', hidden: true },
                    { name: 'NombreDocumento', index: 'NombreDocumento', width: anchoP(ancho, 10) },

                    { name: 'Auditado', index: 'Auditado', hidden: true },
                    { name: 'nomAuditado', index: 'nomAuditado', width: anchoP(ancho, 10) },
                    { name: 'Usuario_Auditor', index: 'Usuario_Auditor', width: anchoP(ancho, 5) },
                    { name: 'Observacion_Auditoria', index: 'Observacion_Auditoria', width: anchoP(ancho, 5) },
                    { name: 'Fecha_Auditoria', index: 'Fecha_Auditoria', width: anchoP(ancho, 5) },
                    { name: 'idEstadoDoc', index: 'idEstadoDoc', hidden: true },
                    { name: 'Estado', index: 'Estado', width: anchoP(ancho, 10) },
                    { name: 'administradora', index: 'administradora', hidden: true },
                    { name: 'var1', index: 'var1', width: anchoP(ancho, 5) },
                    { name: 'var2', index: 'var2', width: anchoP(ancho, 5) },
                    { name: 'var3', index: 'var3', width: anchoP(ancho, 5) }
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listDocumentosEvolucion').getRowData(rowid);
                    estad = 0;

                    //  asignaAtributo('txtIdBusPaciente',datosRow.idPaciente+'-'+datosRow.identificacion+' '+datosRow.Nombre ,0);				  
                    asignaAtributo('lblIdPaciente', datosRow.idPaciente, 1);
                    asignaAtributo('lblIdentificacion', datosRow.identificacion, estad);
                    asignaAtributo('lblNombrePaciente', datosRow.Nombre, estad);
                    asignaAtributo('lblIdEstadoFolio', datosRow.idEstadoDoc, estad)
                    asignaAtributo('txtIdEsdadoDocumento', datosRow.idEstadoDoc, estad)
                    asignaAtributo('lblIdDocumento', datosRow.id_evo, estad);
                    asignaAtributo('lblTipoDocumento', datosRow.tipo, 1);
                    asignaAtributo('lblIdAdmision', datosRow.idAdmision, 1);
                    asignaAtributo('lblAdministradora', datosRow.administradora, 1);
                    asignaAtributo('lblDescripcionTipoDocumento', datosRow.NombreDocumento, estad);
                    asignaAtributo('lblEspecialidad', datosRow.Especialidad, estad);

                    asignaAtributo('cmbAccionAuditoria', datosRow.Auditado, estad);
                    asignaAtributo('txtAuditado', datosRow.Observacion_Auditoria, estad);

                    cargarTablePlanTratamiento()

                    setTimeout("habilitar('btnProcedimiento',0)", 2000);
                    switch (valorAtributo('lblTipoDocumento')) {
                        case 'EXOP':
                            setTimeout("habilitar('btnFormulaOftalmica',1)", 1000);
                            break;
                        case 'EVOP':
                            setTimeout("habilitar('btnFormulaOftalmica',1)", 1000);
                            break;
                        case 'EXBV':
                            setTimeout("habilitar('btnFormulaOftalmica',1)", 1000);
                            break;
                        default:
                            setTimeout("habilitar('btnFormulaOftalmica',0)", 1000);
                            break;
                    }
                    setTimeout("buscarHC('listDocumentosHistoricosTodo','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')", 2000);
                },
                height: 400,
                width: ancho,

            });
            $('#drag' + ventanaActual.num).find("#listDocumentosEvolucion").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listTransaccionesProcedimientos':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=231&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProfesional'));
            add_valores_a_mandar(valorAtributo('cmbCentroCosto'));
            add_valores_a_mandar(valorAtributo('cmbIdClase'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdPaciente'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora'));
            add_valores_a_mandar(valorAtributo('txtFechaCierreDesde') + ' ' + valorAtributo('cmbHoraCierreDesde') + ':00');
            add_valores_a_mandar(valorAtributo('txtFechaCierreHasta') + ' ' + valorAtributo('cmbHoraCierreHasta') + ':00');

            $('#drag' + ventanaActual.num).find("#listTransaccionesProcedimientos").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'ID DOC', 'Fecha Elaboro DOC', 'Id Profesional', 'Profesional', 'idAdministradora',
                    'Administradora', 'IdClase', 'Nombre Clase', 'Id Procedimiento', 'Cups', 'Nombre Procedimiento', 'Cantidad',
                    'Id Paciente', 'Nombre Paciente', 'Centro Costos'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID_DOC', index: 'ID ID_DOC', width: anchoP(ancho, 3) },
                    { name: 'fecha_elaboro_doc', index: 'ID fecha_elaboro_doc', width: anchoP(ancho, 8) },
                    { name: 'id_profesional', index: 'id_profesional', hidden: true },
                    { name: 'Profesional', index: 'Profesional', width: anchoP(ancho, 8) },
                    { name: 'idAdministradora', index: 'idAdministradora', hidden: true },
                    { name: 'Administradora', index: 'Administradora', width: anchoP(ancho, 10) },
                    { name: 'IdClase', index: 'IdClase', hidden: true },
                    { name: 'NombreClase', index: 'NombreClase', width: anchoP(ancho, 8) },
                    { name: 'IdProcedimiento', index: 'IdProcedimiento', width: anchoP(ancho, 3) },
                    { name: 'Cups', index: 'Cups', width: anchoP(ancho, 4) },
                    { name: 'Procedimiento', index: 'Procedimiento', width: anchoP(ancho, 10) },
                    { name: 'Cantidad', index: 'Cantidad', width: anchoP(ancho, 3) },
                    { name: 'Id Paciente', index: 'Id Paciente', width: anchoP(ancho, 4) },
                    { name: 'NombrePaciente', index: 'NombrePaciente', width: anchoP(ancho, 15) },
                    { name: 'centroCostos', index: 'centroCostos', width: anchoP(ancho, 8) },
                ],
                height: 500,
                width: ancho + 100,
            });
            $('#drag' + ventanaActual.num).find("#listTransaccionesProcedimientos").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listProcedimientos':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=181&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdClase'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'id_clase', 'Clase', 'id_origen_atencion', 'Origen Atencion', 'id_tipo_servicio_solicitado', 'Servicio Solicitado', 'id_prioridad', 'Prioridad', 'id_guia', 'Guia', 'Justificacion', 'Fecha elabor�', 'Id Evolucion'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 5) },
                    { name: 'id_clase', index: 'id_clase', hidden: true },
                    { name: 'clase', index: 'clase', width: anchoP(ancho, 10) },
                    { name: 'id_origen_atencion', index: 'id_origen_atencion', hidden: true },
                    { name: 'origen_atencion', index: 'origen_atencion', hidden: true },
                    { name: 'id_tipo_servicio_solicitado', index: 'id_tipo_servicio_solicitado', hidden: true },
                    { name: 'servicio_solicitado', index: 'servicio_solicitado', hidden: true },
                    { name: 'id_prioridad', index: 'id_prioridad', hidden: true },
                    { name: 'prioridad', index: 'prioridad', hidden: true },
                    { name: 'id_guia', index: 'id_guia', hidden: true },
                    { name: 'guia', index: 'guia', hidden: true },
                    { name: 'justificacion', index: 'justificacion', width: anchoP(ancho, 45) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 10) },
                    { name: 'id_evolucion', index: 'id_evolucion', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdProcedimiento', datosRow.id, 0);

                    asignaAtributo('cmbIdClase', datosRow.id_clase, 0)
                    asignaAtributo('cmbIdOrigenAtencion', datosRow.id_origen_atencion, 0)
                    asignaAtributo('cmbIdTipoServicioSolicitado', datosRow.id_tipo_servicio_solicitado, 0)
                    asignaAtributo('cmbIdPrioridad', datosRow.id_prioridad, 0)
                    asignaAtributo('cmbIdGuia', datosRow.id_guia, 0)
                    asignaAtributo('txtJustificacion', datosRow.justificacion, 0)

                    asignaAtributo('lblOrigenAtencionPdf', datosRow.origen_atencion, 0)
                    asignaAtributo('lblTipoServiciosSolicitadosPdf', datosRow.servicio_solicitado, 0)
                    asignaAtributo('lblPrioridadAtencionPdf', datosRow.prioridad, 0)
                    asignaAtributo('lblGuiaPdf', datosRow.guia, 0)


                    buscarHC('listProcedimientosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listProcedimientosDetalle':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            //ancho = 900;
            valores_a_mandar = pag;
            //valores_a_mandar=valores_a_mandar+"?idQuery=58&parametros=";
            //add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));
            valores_a_mandar = valores_a_mandar + "?idQuery=672&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('txtIdClase'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'cod', 'Pos', 'Nombre', 'Cantidad', 'idSitio', 'Sitio', 'Indicacion', 'Id_estado', 'Estado', 'idDiagnostico', 'Diagnostico'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'cod', index: 'cod', hidden: true },
                    { name: 'pos', index: 'pos', width: anchoP(ancho, 5) },
                    { name: 'nombre', index: 'nombre', width: anchoP(ancho, 52) },
                    { name: 'cantidad', index: 'cantidad', width: anchoP(ancho, 6) },
                    { name: 'idSitio', index: 'idSitio', hidden: true },
                    { name: 'Sitio', index: 'Sitio', width: anchoP(ancho, 8) },
                    { name: 'Indicacion', index: 'Indicacion', width: anchoP(ancho, 20) },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'nom_estado', index: 'nom_estado', width: anchoP(ancho, 14) },
                    { name: 'id_diagnostico', index: 'id_diagnostico', hidden: true },
                    { name: 'diagnostico', index: 'diagnostico', hidden: true },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', datosRow.cod + '-' + datosRow.nombre, 0);

                    //campos para ventana editar
                    asignaAtributo('txtTratamientoEditar', datosRow.nombre, 1);
                    asignaAtributo('cmbCantidadTratamientoEditar', datosRow.cantidad, 0);
                    asignaAtributo('cmbIdSitioQuirurgicoTratamientoEditar', datosRow.idSitio, 0);
                    asignaAtributo('txtIndicacionTratamientoEditar', datosRow.Indicacion, 0);
                    asignaAtributoCombo('cmbProcedimientoTratamientoEditar', datosRow.id_diagnostico, datosRow.diagnostico);

                    mostrar('divVentanitaEditProcedimientos')
                    if (datosRow.pos == 'NOPOS')
                        habilitar('btnProcedimientoImprimir', 1)
                    else
                        habilitar('btnProcedimientoImprimir', 0)

                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listAyudasDiagnosticas':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=181&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdClaseAD'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'id_clase', 'Clase', 'id_origen_atencion', 'Origen Atencion', 'id_tipo_servicio_solicitado', 'Servicio Solicitado', 'id_prioridad', 'Prioridad', 'id_guia', 'Guia', 'Justificacion', 'Fecha elabor�', 'Id Evolucion'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 5) },
                    { name: 'id_clase', index: 'id_clase', hidden: true },
                    { name: 'clase', index: 'clase', width: anchoP(ancho, 10) },
                    { name: 'id_origen_atencion', index: 'id_origen_atencion', hidden: true },
                    { name: 'origen_atencion', index: 'origen_atencion', hidden: true },
                    { name: 'id_tipo_servicio_solicitado', index: 'id_tipo_servicio_solicitado', hidden: true },
                    { name: 'servicio_solicitado', index: 'servicio_solicitado', hidden: true },
                    { name: 'id_prioridad', index: 'id_prioridad', hidden: true },
                    { name: 'prioridad', index: 'prioridad', hidden: true },
                    { name: 'id_guia', index: 'id_guia', hidden: true },
                    { name: 'guia', index: 'guia', hidden: true },
                    { name: 'justificacion', index: 'justificacion', width: anchoP(ancho, 45) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 10) },
                    { name: 'id_evolucion', index: 'id_evolucion', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdProcedimiento', datosRow.id, 0);
                    asignaAtributo('txtIdClaseAD', datosRow.id_clase, 0)
                    asignaAtributo('cmbIdTipoServicioSolicitadoAD', datosRow.id_tipo_servicio_solicitado, 0)
                    asignaAtributo('cmbIdPrioridadAD', datosRow.id_prioridad, 0)
                    asignaAtributo('cmbIdGuiaAD', datosRow.id_guia, 0)
                    asignaAtributo('txtJustificacionAD', datosRow.justificacion, 0)

                    asignaAtributo('lblOrigenAtencionPdf', datosRow.origen_atencion, 0)
                    asignaAtributo('lblTipoServiciosSolicitadosPdf', datosRow.servicio_solicitado, 0)
                    asignaAtributo('lblPrioridadAtencionPdf', datosRow.prioridad, 0)
                    asignaAtributo('lblGuiaPdf', datosRow.guia, 0)
                    buscarHC('listAyudasDiagnosticasDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listAyudasDiagnosticasDetalle':
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            //valores_a_mandar=valores_a_mandar+"?idQuery=58&parametros=";
            //add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));
            valores_a_mandar = valores_a_mandar + "?idQuery=672&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('txtIdClaseAD'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'cod', 'Pos', 'Nombre', 'Cantidad', 'idSitio', 'Sitio', 'Indicacion', 'Id_estado', 'Estado', 'idDiagnostico', 'Diagnostico'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'cod', index: 'cod', hidden: true },
                    { name: 'pos', index: 'pos', width: anchoP(ancho, 5) },
                    { name: 'nombre', index: 'nombre', width: anchoP(ancho, 52) },
                    { name: 'cantidad', index: 'cantidad', width: anchoP(ancho, 6) },
                    { name: 'idSitio', index: 'idSitio', hidden: true },
                    { name: 'Sitio', index: 'Sitio', width: anchoP(ancho, 8) },
                    { name: 'Indicacion', index: 'Indicacion', width: anchoP(ancho, 20) },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'nom_estado', index: 'nom_estado', width: anchoP(ancho, 14) },
                    { name: 'id_diagnostico', index: 'id_diagnostico', hidden: true },
                    { name: 'diagnostico', index: 'diagnostico', hidden: true },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', datosRow.cod + '-' + datosRow.nombre, 0);

                    //campos para ventana editar
                    asignaAtributo('txtTratamientoEditar', datosRow.nombre, 1);
                    asignaAtributo('cmbCantidadTratamientoEditar', datosRow.cantidad, 0);
                    asignaAtributo('cmbIdSitioQuirurgicoTratamientoEditar', datosRow.idSitio, 0);
                    asignaAtributo('txtIndicacionTratamientoEditar', datosRow.Indicacion, 0);
                    asignaAtributoCombo('cmbProcedimientoTratamientoEditar', datosRow.id_diagnostico, datosRow.diagnostico);

                    mostrar('divVentanitaEditProcedimientos')
                    if (datosRow.pos == 'NOPOS')
                        habilitar('btnProcedimientoImprimir', 1)
                    else
                        habilitar('btnProcedimientoImprimir', 0)

                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listMedicacion':   /* en historia clinica */
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=690&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'ID_EVOLUCION', 'POS', 'ID_ARTICULO', 'Articulo', 'id_via', 'Via', 'id_forma', 'Forma',
                    'Dosis', 'id_frecuencia', 'Frecuencia', 'Duracion Tratamiento', 'id_cantidad', 'unidad', 'Cantidad',
                    'Indicacion', 'Fecha_elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_EVOLUCION', index: 'ID_EVOLUCION', hidden: true },
                    { name: 'POS', index: 'POS', width: anchoP(ancho, 5) },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', hidden: true },
                    { name: 'NOMBRE_ARTICULO', index: 'NOMBRE_ARTICULO', width: anchoP(ancho, 30) },
                    { name: 'ID_VIA', index: 'ID_VIA', hidden: true },
                    { name: 'NOMBRE_VIA', index: 'NOMBRE_VIA', width: anchoP(ancho, 10) },
                    { name: 'ID_FORMA', index: 'ID_FORMA', hidden: true },
                    { name: 'NOMBRE_FORMA', index: 'NOMBRE_FORMA', width: anchoP(ancho, 10) },
                    { name: 'DOSIS', index: 'DOSIS', width: anchoP(ancho, 8) },
                    { name: 'ID_FRECUENCIA', index: 'ID_FRECUENCIA', hidden: true },
                    { name: 'FRECUENCIA', index: 'FRECUENCIA', width: anchoP(ancho, 12) },
                    { name: 'DIAS', index: 'DIAS', width: anchoP(ancho, 8) },
                    { name: 'ID_CANTIDAD', index: 'ID_CANTIDAD', hidden: true },
                    { name: 'UNIDAD', index: 'UNIDAD', hidden: true },
                    { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 8) },
                    { name: 'INDICACION', index: 'INDICACION', width: anchoP(ancho, 20) },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', hidden: true },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.ID, 0);
                    asignaAtributo('lblIdEvolucion', datosRow.ID_EVOLUCION, 0);
                    asignaAtributo('lblIdElemento', datosRow.ID_ARTICULO, 0);
                    asignaAtributo('lblNombreElemento', datosRow.NOMBRE_ARTICULO, 0);

                    //ventanita editar
                    asignaAtributo('lblIdEditar', datosRow.ID, 1);
                    //asignaAtributo('txtIdElementoEditar', datosRow.ID_ARTICULO, 0);
                    asignaAtributo('lblIdElementoEditar', datosRow.ID_ARTICULO, 0);
                    asignaAtributo('lblNombreElementoEditar', datosRow.NOMBRE_ARTICULO, 0);

                    asignaAtributoCombo2('cmbIdViaEditar', datosRow.ID_VIA, datosRow.NOMBRE_VIA);
                    asignaAtributoCombo2('cmbUnidadEditar', datosRow.ID_FORMA, datosRow.NOMBRE_FORMA);
                    asignaAtributo('cmbCantEditar', datosRow.DOSIS, 0);
                    asignaAtributo('cmbFrecuenciaEditar', datosRow.ID_FRECUENCIA, 0);
                    asignaAtributo('cmbDiasEditar', datosRow.DIAS, 0);
                    asignaAtributo('txtCantidadEditar', datosRow.ID_CANTIDAD, 0);
                    asignaAtributo('cmbUnidadFarmaceuticaEditar', datosRow.UNIDAD, 0);
                    asignaAtributo('txtIndicacionesEditar', datosRow.INDICACION, 0);

                    mostrarVentanita('divVentanitaMedicacion');



                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listMedicacionOrdenada':
            limpiarDivEditarJuan(arg);
            // ancho= ($('#drag'+ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width()-50);			 	 
            ancho = 650;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=344&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'ID_EVOLUCION', 'POS', 'ID_ARTICULO', 'Articulo', 'Via', 'Unidad', 'Cantidad', 'Frecuencia', 'Dias', 'Indicacion', 'Fecha_elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_EVOLUCION', index: 'ID_EVOLUCION', hidden: true },
                    { name: 'POS', index: 'POS', width: anchoP(ancho, 5) },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', hidden: true },
                    { name: 'NOMBRE_ARTICULO', index: 'NOMBRE_ARTICULO', width: anchoP(ancho, 30) },
                    { name: 'NOMBRE_VIA', index: 'NOMBRE_VIA', width: anchoP(ancho, 10) },
                    { name: 'NOMBRE_UNIDAD', index: 'NOMBRE_UNIDAD', width: anchoP(ancho, 10) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 5) },
                    { name: 'FRECUENCIA', index: 'FRECUENCIA', width: anchoP(ancho, 12) },
                    { name: 'DIAS', index: 'DIAS', width: anchoP(ancho, 5) },
                    { name: 'INDICACION', index: 'INDICACION', width: anchoP(ancho, 20) },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', hidden: true },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    /* asignaAtributo('lblId', datosRow.ID, 0); 
                     asignaAtributo('lblIdEvolucion', datosRow.ID_EVOLUCION, 0);
                     asignaAtributo('lblIdElemento', datosRow.ID_ARTICULO, 0);
                     asignaAtributo('lblNombreElemento', datosRow.NOMBRE_ARTICULO, 0);
                     mostrarVentanita('divVentanitaMedicacion') */

                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listLaboratorios':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=181&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdClaseLaboratorio'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'id_clase', 'Clase', 'id_origen_atencion', 'Origen Atencion', 'id_tipo_servicio_solicitado', 'Servicio Solicitado', 'id_prioridad', 'Prioridad', 'id_guia', 'Guia', 'Justificacion', 'Fecha elabor�', 'Id Evolucion'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 5) },
                    { name: 'id_clase', index: 'id_clase', hidden: true },
                    { name: 'clase', index: 'clase', width: anchoP(ancho, 10) },
                    { name: 'id_origen_atencion', index: 'id_origen_atencion', hidden: true },
                    { name: 'origen_atencion', index: 'origen_atencion', hidden: true },
                    { name: 'id_tipo_servicio_solicitado', index: 'id_tipo_servicio_solicitado', hidden: true },
                    { name: 'servicio_solicitado', index: 'servicio_solicitado', hidden: true },
                    { name: 'id_prioridad', index: 'id_prioridad', hidden: true },
                    { name: 'prioridad', index: 'prioridad', hidden: true },
                    { name: 'id_guia', index: 'id_guia', hidden: true },
                    { name: 'guia', index: 'guia', hidden: true },
                    { name: 'justificacion', index: 'justificacion', width: anchoP(ancho, 45) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 10) },
                    { name: 'id_evolucion', index: 'id_evolucion', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdProcedimiento', datosRow.id, 0);
                    asignaAtributo('cmbIdClaseLaboratorio', datosRow.id_clase, 0)
                    asignaAtributo('cmbIdOrigenAtencionLaboratorio', datosRow.id_origen_atencion, 0)
                    asignaAtributo('cmbIdTipoServicioSolicitadoLaboratorio', datosRow.id_tipo_servicio_solicitado, 0)
                    asignaAtributo('cmbIdPrioridadLaboratorio', datosRow.id_prioridad, 0)
                    asignaAtributo('cmbIdGuiaLaboratorio', datosRow.id_guia, 0)
                    asignaAtributo('txtJustificacionLaboratorio', datosRow.justificacion, 0)
                    buscarHC('listLaboratoriosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listLaboratoriosDetalle':
            limpiarDivEditarJuan(arg);
            //ancho = 900;
            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);

            valores_a_mandar = pag;
            //valores_a_mandar=valores_a_mandar+"?idQuery=58&parametros=";
            //add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));
            valores_a_mandar = valores_a_mandar + "?idQuery=672&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('txtIdClaseLaboratorio'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'cod', 'Pos', 'Nombre', 'Cantidad', 'idSitio', 'Sitio', 'Indicacion', 'Id_estado', 'Estado', 'idDiagnostico', 'Diagnostico'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'cod', index: 'cod', hidden: true },
                    { name: 'pos', index: 'pos', width: anchoP(ancho, 5) },
                    { name: 'nombre', index: 'nombre', width: anchoP(ancho, 52) },
                    { name: 'cantidad', index: 'cantidad', width: anchoP(ancho, 6) },
                    { name: 'idSitio', index: 'idSitio', hidden: true },
                    { name: 'Sitio', index: 'Sitio', width: anchoP(ancho, 8) },
                    { name: 'Indicacion', index: 'Indicacion', width: anchoP(ancho, 20) },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'nom_estado', index: 'nom_estado', width: anchoP(ancho, 14) },
                    { name: 'id_diagnostico', index: 'id_diagnostico', hidden: true },
                    { name: 'diagnostico', index: 'diagnostico', hidden: true },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', datosRow.cod + '-' + datosRow.nombre, 0);

                    //campos para ventana editar
                    asignaAtributo('txtTratamientoEditar', datosRow.nombre, 1);
                    asignaAtributo('cmbCantidadTratamientoEditar', datosRow.cantidad, 0);
                    asignaAtributo('cmbIdSitioQuirurgicoTratamientoEditar', datosRow.idSitio, 0);
                    asignaAtributo('txtIndicacionTratamientoEditar', datosRow.Indicacion, 0);
                    asignaAtributoCombo('cmbProcedimientoTratamientoEditar', datosRow.id_diagnostico, datosRow.diagnostico);

                    mostrar('divVentanitaEditProcedimientos')
                    if (datosRow.pos == 'NOPOS')
                        habilitar('btnProcedimientoImprimir', 1)
                    else
                        habilitar('btnProcedimientoImprimir', 0)

                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listTerapia':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=181&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdClaseTerapia'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'id_clase', 'Clase', 'id_origen_atencion', 'Origen Atencion', 'id_tipo_servicio_solicitado', 'Servicio Solicitado', 'id_prioridad', 'Prioridad', 'id_guia', 'Guia', 'Justificacion', 'Fecha elabor�', 'Id Evolucion'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 5) },
                    { name: 'id_clase', index: 'id_clase', hidden: true },
                    { name: 'clase', index: 'clase', width: anchoP(ancho, 10) },
                    { name: 'id_origen_atencion', index: 'id_origen_atencion', hidden: true },
                    { name: 'origen_atencion', index: 'origen_atencion', hidden: true },
                    { name: 'id_tipo_servicio_solicitado', index: 'id_tipo_servicio_solicitado', hidden: true },
                    { name: 'servicio_solicitado', index: 'servicio_solicitado', hidden: true },
                    { name: 'id_prioridad', index: 'id_prioridad', hidden: true },
                    { name: 'prioridad', index: 'prioridad', hidden: true },
                    { name: 'id_guia', index: 'id_guia', hidden: true },
                    { name: 'guia', index: 'guia', hidden: true },
                    { name: 'justificacion', index: 'justificacion', width: anchoP(ancho, 45) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 10) },
                    { name: 'id_evolucion', index: 'id_evolucion', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdProcedimiento', datosRow.id, 0);
                    asignaAtributo('cmbIdClaseTerapia', datosRow.id_clase, 0)
                    asignaAtributo('cmbIdOrigenAtencionTerapia', datosRow.id_origen_atencion, 0)
                    asignaAtributo('cmbIdTipoServicioSolicitadoTerapia', datosRow.id_tipo_servicio_solicitado, 0)
                    asignaAtributo('cmbIdPrioridadTerapia', datosRow.id_prioridad, 0)
                    asignaAtributo('cmbIdGuiaTerapia', datosRow.id_guia, 0)
                    asignaAtributo('txtJustificacionTerapia', datosRow.justificacion, 0)
                    buscarHC('listTerapiaDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listTerapiaDetalle':
            limpiarDivEditarJuan(arg);
            //ancho = 900;
            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            valores_a_mandar = pag;
            //valores_a_mandar=valores_a_mandar+"?idQuery=58&parametros=";
            //add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));
            valores_a_mandar = valores_a_mandar + "?idQuery=672&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('txtIdClaseTerapia'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'cod', 'Pos', 'Nombre', 'Cantidad', 'idSitio', 'Sitio', 'Indicacion', 'Id_estado', 'Estado', 'idDiagnostico', 'Diagnostico'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'cod', index: 'cod', hidden: true },
                    { name: 'pos', index: 'pos', width: anchoP(ancho, 5) },
                    { name: 'nombre', index: 'nombre', width: anchoP(ancho, 52) },
                    { name: 'cantidad', index: 'cantidad', width: anchoP(ancho, 6) },
                    { name: 'idSitio', index: 'idSitio', hidden: true },
                    { name: 'Sitio', index: 'Sitio', width: anchoP(ancho, 8) },
                    { name: 'Indicacion', index: 'Indicacion', width: anchoP(ancho, 20) },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'nom_estado', index: 'nom_estado', width: anchoP(ancho, 14) },
                    { name: 'id_diagnostico', index: 'id_diagnostico', hidden: true },
                    { name: 'diagnostico', index: 'diagnostico', hidden: true },
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', datosRow.cod + '-' + datosRow.nombre, 0);

                    //campos para ventana editar
                    asignaAtributo('txtTratamientoEditar', datosRow.nombre, 1);
                    asignaAtributo('cmbCantidadTratamientoEditar', datosRow.cantidad, 0);
                    asignaAtributo('cmbIdSitioQuirurgicoTratamientoEditar', datosRow.idSitio, 0);
                    asignaAtributo('txtIndicacionTratamientoEditar', datosRow.Indicacion, 0);
                    asignaAtributoCombo('cmbProcedimientoTratamientoEditar', datosRow.id_diagnostico, datosRow.diagnostico);

                    mostrar('divVentanitaEditProcedimientos')
                    if (datosRow.pos == 'NOPOS')
                        habilitar('btnProcedimientoImprimir', 1)
                    else
                        habilitar('btnProcedimientoImprimir', 0)

                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listAntecedentes':
            limpiarDivEditarJuan(arg);
            //			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','400px');				 
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=326&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Id', 'Tipo', 'id_grupo', 'grupo', 'Tiene', 'Controlada', 'Observaciones', 'Fecha elabor�'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 10) },
                    { name: 'id_grupo', index: 'id_grupo', hidden: true },
                    { name: 'grupo', index: 'grupo', width: anchoP(ancho, 20) },
                    { name: 'tiene', index: 'tiene', width: anchoP(ancho, 5) },
                    { name: 'controlada', index: 'controlada', width: anchoP(ancho, 5) },
                    { name: 'observacion', index: 'observacion', width: anchoP(ancho, 30) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id, 0);
                    asignaAtributo('lblNombreElemento', concatenarCodigoNombre(datosRow.Tipo, datosRow.grupo), 0)
                    abrirVentanita('divVentanitaAntecedentes');
                },
                height: 90,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listAntecedentesFarmacologicos':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#idTablaRevSistem").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=511&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'id_documento', 'Id', 'Antecedente', 'Medicamentos', 'Fecha elabor�'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_documento', index: 'id_documento', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'antecedente', index: 'antecedente', width: anchoP(ancho, 10) },
                    { name: 'medicamento', index: 'medicamento', width: anchoP(ancho, 20) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id_documento, 0);
                    asignaAtributo('lblIdElemento', datosRow.id, 0)
                    asignaAtributo('lblNombreElemento', datosRow.antecedente, 0)
                    abrirVentanita('divVentanitaAntecedentesFarmacologicos');
                },
                height: 100,
                width: ancho - 250,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listAntFarmacologicos':
            limpiarDivEditarJuan(arg);
            ancho = 750;
            //ancho= ($('#drag'+ventanaActual.num).find("#idTablaRevSistem").width()-50);	
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=511&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'id_documento', 'Id', 'Antecedente', 'Medicamentos', 'Fecha elabor�'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_documento', index: 'id_documento', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'antecedente', index: 'antecedente', width: anchoP(ancho, 10) },
                    { name: 'medicamento', index: 'medicamento', width: anchoP(ancho, 20) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    /*  asignaAtributo('lblId', datosRow.id_documento, 0);
                     asignaAtributo('lblIdElemento', datosRow.id, 0)	
                     asignaAtributo('lblNombreElemento', datosRow.antecedente, 0)		
                     abrirVentanita('divVentanitaAntecedentesFarmacologicos');	 */
                },
                height: 100,
                width: ancho - 250,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listSistemas':
            limpiarDivEditarJuan(arg);
            ancho = 950;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=346&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'id_documento', 'Id', 'sistema', 'hallazgo', 'Fecha elabor�'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_documento', index: 'id_documento', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'sistema', index: 'sistema', width: anchoP(ancho, 10) },
                    { name: 'hallazgo', index: 'hallazgo', width: anchoP(ancho, 20) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id_documento, 0);
                    asignaAtributo('lblIdElemento', datosRow.id, 0)
                    asignaAtributo('lblNombreElemento', datosRow.sistema, 0)
                    abrirVentanita('divVentanitaSistemas');
                },
                height: 100,
                width: ancho - 250,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listDiagnosticos':
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=348&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'id_evolucion', 'Id CIE', 'Nombre Diagn&oacute;stico', 'Tipo', 'Clase', 'Sitio', 'observacion', 'Fecha elabor�'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_evolucion', index: 'id_evolucion', hidden: true },
                    { name: 'id_cie', index: 'id_cie', width: anchoP(ancho, 5) },
                    { name: 'nombre', index: 'nombre', width: anchoP(ancho, 35) },
                    { name: 'tipo', index: 'tipo', width: anchoP(ancho, 10) },
                    { name: 'clase', index: 'clase', width: anchoP(ancho, 8) },
                    { name: 'Sitio', index: 'Sitio', width: anchoP(ancho, 10) },
                    { name: 'observacion', index: 'observacion', width: anchoP(ancho, 12) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 15) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.id_evolucion, 0);
                    asignaAtributo('lblIdElemento', datosRow.id_cie, 0)
                    asignaAtributo('lblNombreElemento', datosRow.nombre, 0)
                    mostrarVentanita('divVentanitaDx');

                },
                height: 100,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listAdministracionMedicacion':
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=441&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'ID', 'ID_ARTICULO', 'NOMBRE', 'VIA', 'UNIDAD', 'CANTIDAD', 'Sitio', 'Indicacion', 'Fecha Elaboro', 'ID_EVOLUCION'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 5) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 35) },
                    { name: 'VIA', index: 'VIA', width: anchoP(ancho, 10) },
                    { name: 'UNIDAD', index: 'UNIDAD', width: anchoP(ancho, 8) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 12) },
                    { name: 'Sitio', index: 'Sitio', width: anchoP(ancho, 10) },
                    { name: 'Indicacion', index: 'Indicacion', width: anchoP(ancho, 12) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 15) },
                    { name: 'ID_EVOLUCION', index: 'ID_EVOLUCION', hidden: true },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.ID, 0);
                    asignaAtributo('lblIdEvolucion', datosRow.ID_EVOLUCION, 0);
                    asignaAtributo('lblIdElemento', datosRow.ID_ARTICULO, 0)
                    asignaAtributo('lblNombreElemento', datosRow.NOMBRE, 0)
                    mostrarVentanita('divVentanitaMedicacion');

                },
                height: 100,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listHojaGastos':
            limpiarDivEditarJuan(arg);
            ancho = 1000
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=470&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAdmision'));
            //add_valores_a_mandar(valorAtributo('lblIdAdmisionAgen'));			 				 		 

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'ID', 'ID_EVOLUCION', 'ID_ARTICULO', 'NOMBRE', 'CANT', 'Indicacion', 'ID_TRANSACCION', 'ID_NOVEDAD', 'NOVEDAD',
                    'CANT_NOVEDAD', 'ID_USUARIO', 'USUARIO_ELABORO', 'FECHA_ELABORO', 'ULT_CONSUMO'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_EVOLUCION', index: 'ID_EVOLUCION', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 3) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 35) },
                    { name: 'CANT', index: 'CANT', width: anchoP(ancho, 5) },
                    { name: 'Indicacion', index: 'Indicacion', width: anchoP(ancho, 15) },
                    { name: 'ID_TRANSACCION', index: 'ID_TRANSACCION', hidden: true },
                    { name: 'ID_NOVEDAD', index: 'ID_NOVEDAD', hidden: true },
                    { name: 'NOVEDAD', index: 'NOVEDAD', width: anchoP(ancho, 5) },
                    { name: 'CANT_NOVEDAD', index: 'CANT_NOVEDAD', width: anchoP(ancho, 5) },
                    { name: 'ID_USUARIO', index: 'ID_USUARIO', hidden: true },
                    { name: 'USUARIO', index: 'USUARIO', width: anchoP(ancho, 8) },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', width: anchoP(ancho, 12) },
                    { name: 'SW_ULT_CONSUMO', index: 'SW_ULT_CONSUMO', width: anchoP(ancho, 5) },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdHG', datosRow.ID, 0);
                    asignaAtributo('lblIdElemento', datosRow.ID_ARTICULO, 0)
                    asignaAtributo('lblNombreElemento', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbCantModificar', datosRow.CANT, 0);
                    asignaAtributo('txtUConsumo', datosRow.SW_ULT_CONSUMO, 0);
                    /*  if(datosRow.SW_ULT_CONSUMO=='S'){
                     habilitar('btnConsumoElimina',0)
                     }else {habilitar('btnConsumoElimina',1)} */
                    mostrar('divVentanitaHojaGastos');

                },
                height: 400,
                width: ancho,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listDiagnosticosHistorico':
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=382&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'id_evolucion', 'Id CIE', 'Nombre Diagn&oacute;stico', 'Tipo', 'Clase', 'Sitio', 'observacion', 'Fecha elabor�'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_evolucion', index: 'id_evolucion', hidden: true },
                    { name: 'id_cie', index: 'id_cie', width: anchoP(ancho, 5) },
                    { name: 'nombre', index: 'nombre', width: anchoP(ancho, 35) },
                    { name: 'tipo', index: 'tipo', width: anchoP(ancho, 10) },
                    { name: 'clase', index: 'clase', width: anchoP(ancho, 8) },
                    { name: 'Sitio', index: 'Sitio', width: anchoP(ancho, 10) },
                    { name: 'observacion', index: 'observacion', width: anchoP(ancho, 12) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 15) },
                ],

                height: 400,
                width: ancho + 40,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listProcedimientosHistoricos':
            limpiarDivEditarJuan(arg);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=383&parametros="; //SUSTITUYE 16
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'CLASE', 'ID PROC', 'TRATAMIENTO', 'CANTIDAD', 'SITIO', 'INDICACION', 'FECHA ELABORO'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'CLASE', index: 'CLASE', width: anchoP(ancho, 10) },
                    { name: 'IDPROC', index: 'IDPROC', hidden: true },
                    { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 35) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 5) },
                    { name: 'SITIO', index: 'SITIO', width: anchoP(ancho, 8) },
                    { name: 'INDICACION', index: 'INDICACION', width: anchoP(ancho, 10) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 10) },
                ],
                height: 400,
                width: ancho + 20,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listSignosVitales':
            limpiarDivEditarJuan(arg);
            if (valorAtributo('lblEdadPaciente') <= 10) {
                asignaAtributo('txtTemperatura', '', 0)
                asignaAtributo('txtSistolica', '', 0)
                asignaAtributo('txtDiastolica', '', 0)
                asignaAtributo('txtPulso', '', 0)
                asignaAtributo('txtRespiracion', '', 0)
                asignaAtributo('txtPeso', '', 0)
                asignaAtributo('txtTalla', '', 0)
            }
            valores_a_mandar = pag;

            /* if(valorAtributo('lblIdAdmisionAgen')!=''){
             valores_a_mandar=valores_a_mandar+"?idQuery=193&parametros="; 			 
             add_valores_a_mandar(valorAtributo('lblIdAdmisionAgen'));	
             }else{ */
            valores_a_mandar = valores_a_mandar + "?idQuery=630&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            //}


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'Temperatura', 'Sistolica', 'Diastolica', 'Pulso', 'Respiraci�n', 'Peso', 'Talla', 'IMC', 'Observacion', 'Fecha elabor�', 'Usuario elabor�', 'Id documento', 'id'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'Temperatura', index: 'Temperatura', width: anchoP(ancho, 8) },
                    { name: 'Sistolica', index: 'Sistolica', width: anchoP(ancho, 6) },
                    { name: 'Diastolica', index: 'Diastolica', width: anchoP(ancho, 6) },
                    { name: 'Pulso', index: 'Pulso', width: anchoP(ancho, 5) },
                    { name: 'Respiracion', index: 'Respiracion', width: anchoP(ancho, 10) },
                    { name: 'Peso', index: 'Peso', width: anchoP(ancho, 8) },
                    { name: 'Talla', index: 'Talla', width: anchoP(ancho, 8) },
                    { name: 'IMC', index: 'IMC', width: anchoP(ancho, 5) },
                    { name: 'Observacion', index: 'Observacion', width: anchoP(ancho, 20) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 9) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', width: anchoP(ancho, 10) },
                    { name: 'id_documento', index: 'id_documento', width: anchoP(ancho, 5) },
                    { name: 'id', index: 'id', hidden: true }

                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    if (datosRow.id_documento == valorAtributo('lblIdDocumento')) {
                        asignaAtributo('lblId', datosRow.id_documento, 0);
                        asignaAtributo('lblNombreElemento', datosRow.nombre, 0)
                        asignaAtributo('lblNombreElementoP', datosRow.id, 0)
                        abrirVentanita('divVentanitaP');
                    }
                },
                height: 100,
                width: ancho + 30,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        /********************************************************************************************** ini  platin **************************/
        case 'listGrillaHomologacionIdentifica':
            limpiarDivEditarJuan(arg);
            //$('#drag'+ventanaActual.num).find('#'+arg).css('height','400px');				 
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=330&parametros=";
            add_valores_a_mandar(valorAtributo('lblId'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'TipoId Anterior', 'Identificacion Anterior', 'TipoId Nueva', 'Identificacion Nueva', 'Fecha cambio', 'Usuario cambio', 'Accion'],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2) },
                    { name: 'id', index: 'id', align: 'center', hidden: true },
                    { name: 'TipoIdAnt', index: 'TipoIdAnt', align: 'center', width: anchoP(ancho, 5) },
                    { name: 'identificacionAnt', index: 'identificacionAnt', width: anchoP(ancho, 25) },
                    { name: 'TipoIdNueva', index: 'TipoIdNueva', align: 'center', width: anchoP(ancho, 5) },
                    { name: 'identificacionNueva', index: 'identificacionNueva', width: anchoP(ancho, 25) },
                    { name: 'fecha_cambio', index: 'fecha_cambio', width: anchoP(ancho, 15) },
                    { name: 'usuario_cambio', index: 'usuario_cambio', width: anchoP(ancho, 10) },
                    { name: 'accion', index: 'accion', width: anchoP(ancho, 10) },
                ],
                height: 150,
                width: ancho + 50,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'paciente':
            limpiarDivEditarJuan(arg);
            $('#drag' + ventanaActual.num).find('#divContenido').css('height', '400px');
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=327&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            $('#drag' + ventanaActual.num).find("#listGrilla").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'TipoId', 'Identificacion', 'Nombre1', 'Nombre2', 'Apellido1', 'Apellido2',
                    'Fecha Nacimiento', 'Sexo', 'Direccion', 'Municipio Residencia',
                    'Telefonos', 'celular1', 'celular2', 'id_administradora', 'administradora', 'email'],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2) },
                    { name: 'id', index: 'id', align: 'center', hidden: true },
                    { name: 'TipoId', index: 'TipoId', align: 'center', width: anchoP(ancho, 5) },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 15) },
                    { name: 'Nombre1', index: 'Nombre1', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'Nombre2', index: 'Nombre2', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'Apellido1', index: 'Apellido1', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'Apellido2', index: 'Apellido2', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'FechaNacimiento', index: 'FechaNacimiento', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'Sexo', index: 'Sexo', width: anchoP(ancho, 5), align: 'left' },
                    { name: 'Direccion', index: 'Direccion', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'MunicipioResi', index: 'MunicipioResi', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'Telefono', index: 'Telefono', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'celular1', index: 'celular1', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'celular2', index: 'celular2', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'id_administradora', index: 'id_administradora', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'administradora', index: 'administradora', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'email', index: 'email', align: 'center', hidden: true },
                ],
                height: 300,
                width: ancho,
                onSelectRow: function (rowid) {
                    mostrar('divEditar')
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrilla').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblId', datosRow.id, estad);
                    asignaAtributo('cmbTipoIdEdit', datosRow.TipoId, 1);
                    asignaAtributo('txtIdPaciente', datosRow.identificacion, 1);
                    asignaAtributo('txtNombre1', datosRow.Nombre1, estad);
                    asignaAtributo('txtNombre2', datosRow.Nombre2, estad);
                    asignaAtributo('txtApellido1', datosRow.Apellido1, estad);
                    asignaAtributo('txtApellido2', datosRow.Apellido2, estad);
                    asignaAtributo('cmbSexo', datosRow.Sexo, estad);
                    asignaAtributo('txtFechaNacimiento', datosRow.FechaNacimiento, estad);
                    asignaAtributo('txtTelefono', datosRow.Telefono, estad);
                    asignaAtributo('txtCelular1', datosRow.celular1, estad);
                    asignaAtributo('txtCelular2', datosRow.celular2, estad);
                    asignaAtributo('txtAdministradora1', datosRow.id_administradora + '-' + datosRow.administradora, estad);
                    asignaAtributo('txtMunicipioResi', datosRow.MunicipioResi, estad);
                    asignaAtributo('txtDireccion', datosRow.Direccion, estad);
                    asignaAtributo('txtObservaciones', datosRow.Observaciones, estad);
                    asignaAtributo('lblIdAnteriores', datosRow.idAnteriores, estad);
                    asignaAtributo('txtEmail', datosRow.email, estad);


                    buscarHC('listGrillaHomologacionIdentifica', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                },
            });
            $('#drag' + ventanaActual.num).find("#listGrilla").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listMedicaDiagnosticos':
            limpiarDivEditarJuan(arg);
            //			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','400px');				 
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=35&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));
            add_valores_a_mandar(valorAtributo('lblFechaEvento'));
            // alert(valores_a_mandar);


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id CIE', 'Nombre CIE', 'Fecha elabor� Dx', 'Profesional elabor� Dx'],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2) },
                    { name: 'id_cie', index: 'id_cie', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'nom_cie', index: 'nom_cie', align: 'left', width: anchoP(ancho, 25) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 25) },
                    { name: 'Profesional', index: 'Profesional', align: 'left', width: anchoP(ancho, 20) },
                ],

                pager: jQuery('#pagerGrilla'),
                height: 150,
                //			 width: ancho+80,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        /********************************************************************************************** ini  orden **************************/
        case 'listDevolucion':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=57&parametros=";
            //    add_valores_a_mandar(valorAtributo('cmbIdUnidad'));	
            //             add_valores_a_mandar(valorAtributoIdAutoCompletar('txtBusIdPaciente'));
            //			 alert(valores_a_mandar);

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['contador', 'id_orden_medicamento', 'Fecha Programada', 'id_hora_programada', 'Hora M',
                    'id_articulo', 'Nombre Articulo', 'id_inmediato', 'Inmediato', 'Cantidad Despachada', 'id_cantidad', 'CANTIDAD ORDENADA', 'id_tipo_Dosis', 'Tipo Dosis', 'Indicaci�n',
                    'Id Transac',
                    'Id Admin', 'Id EStado', 'Estado', 'Cant Devoluci�n', 'Usuario Admin', 'id_elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', hidden: true },
                    { name: 'id_orden_medicamento', index: 'id_orden_medicamento', hidden: true },
                    { name: 'fecha_programada', index: 'fecha_programada', hidden: true },
                    { name: 'id_hora_programada', index: 'id_hora_programada', hidden: true },
                    { name: 'hora_militar', index: 'hora_militar', width: anchoP(ancho, 4), align: 'center' },
                    { name: 'id_articulo', index: 'id_articulo', hidden: true },
                    { name: 'nombre_articulo', index: 'nombre_articulo', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'id_inmediato', index: 'id_inmediato', hidden: true, align: 'left' },
                    { name: 'Inmediato', index: 'Inmediato', width: anchoP(ancho, 4), align: 'left' },

                    { name: 'cantidad_Despachada', index: 'cantidad_Despachada', width: anchoP(ancho, 9), align: 'left' },
                    { name: 'id_cantidad', index: 'id_cantidad', hidden: true },
                    { name: 'CANTIDAD_ORDENADA', index: 'CANTIDAD_ORDENADA', width: anchoP(ancho, 9), align: 'left' },

                    { name: 'id_tipo_Dosis', index: 'id_tipo_Dosis', hidden: true, align: 'left' },
                    { name: 'tipo_Dosis', index: 'tipo_Dosis', width: anchoP(ancho, 4), align: 'left' },
                    { name: 'Observacion', index: 'Observacion', width: anchoP(ancho, 10), align: 'left' },

                    { name: 'Id_Transac', index: 'Id_Transac', hidden: true, align: 'left' },

                    { name: 'Id_Admin', index: 'Id_Admin', hidden: true },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'Administrado', index: 'Administrado', width: anchoP(ancho, 10), align: 'left' },
                    { name: 'cantNovedad', index: 'cantNovedad', width: anchoP(ancho, 5), align: 'left' },
                    { name: 'usuario', index: 'usuario', width: anchoP(ancho, 2), align: 'left' },
                    { name: 'id_elaboro', index: 'id_elaboro', hidden: true },
                ],
                //  pager: jQuery('#pagerGrilla'), 
                height: 200,
                width: ancho + 15,

                onSelectRow: function (rowid) {
                    // alert(' clic derecho')
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    if (datosRow.Id_Transac != '') {

                        if (datosRow.id_estado == '') {	  /*solo si esta en estado pendiente se puede dar clic contrario para novedad*/
                            asignaAtributo('lblIdOrden', datosRow.id_orden_medicamento, 0);
                            asignaAtributo('txtIdCantidad', datosRow.id_cantidad, 0)
                            asignaAtributo('lblCantidad', datosRow.Cantidad, 0)
                            asignaAtributo('lblFechaProgramada', datosRow.fecha_programada, 0)
                            asignaAtributo('txtIdHora', datosRow.id_hora_programada, 0)
                            asignaAtributo('lblIdHoraMilitar', datosRow.hora_militar, 0)
                            asignaAtributo('lblCantidad', datosRow.CANTIDAD_ORDENADA, 0)


                            abrirVentanitaNovedadesAdministracion(datosRow.id_articulo, datosRow.nombre_articulo);
                        }
                    } else
                        alert('NO EXISTE SUMINISTRO POR FARMACIA')
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;





        case 'listDocumentosHistoricosTodos':
            agregarTablaHistoricosAtencion(1, valorAtributoIdAutoCompletar('txtIdBusPaciente'), 18, 'listDocumentosHistoricos')
            break;
        case 'listDocumentosHistoricosTodo':
            agregarTablaHistoricosAtencion(1, valorAtributo('lblIdPaciente'), 18, 'listDocumentosHistoricos')
            break;
        case 'listDocumentosHistoricos':
            agregarTablaHistoricosAtencion(1, valorAtributo('lblIdPaciente'), 18, 'listDocumentosHistoricos')

            break;


        case 'listOrdenesMedicamentos':
            limpiarDivEditarJuan(arg);
            cerrarVentanitaInmediatoMedicamentos();
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=40&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            // alert(valores_a_mandar);

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id solicitud', 'id documento', 'Tipo', 'idArticulo', 'Articulo', 'id_via', 'Via', 'id_tipo_dosis', 'Detalle', 'Tipo Dosis',
                    'id_repeticion_programada', 'Duraci�n', 'Intervalo', 'Indicaciones', 'id_inmediato', 'Inmediato', 'Fecha Planeada', 'id_profesional_elaboro', 'Fecha Hora Inicio', 'Profesional'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id', index: 'id', align: 'left', hidden: true },
                    { name: 'documento', index: 'documento', hidden: true },
                    { name: 'tipo', index: 'tipo', width: anchoP(ancho, 2) },
                    { name: 'idArticulo', index: 'Articulo', hidden: true },
                    { name: 'Articulo', index: 'Articulo', width: anchoP(ancho, 16) },
                    { name: 'id_via', index: 'id_via', hidden: true },
                    { name: 'nombre_via', index: 'nombre_via', align: 'left', width: anchoP(ancho, 4) },
                    { name: 'id_tipo_dosis', index: 'id_tipo_dosis', hidden: true },
                    { name: 'detalle_dosis_frecuencia', index: 'detalle_dosis_frecuencia', align: 'left', width: anchoP(ancho, 10) },
                    { name: 'nombre_tipo_dosis', index: 'nombre_tipo_dosis', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'id_repeticion_programada', index: 'id_repeticion_programada', hidden: true },
                    { name: 'nombre_repeticion_programada', index: 'nombre_repeticion_programada', align: 'left', width: anchoP(ancho, 7) },
                    { name: 'Intervalo', index: 'Intervalo', align: 'left', width: anchoP(ancho, 4) },
                    { name: 'observaciones', index: 'observaciones', align: 'left', width: anchoP(ancho, 10) },
                    { name: 'id_inmediato', index: 'id_inmediato', hidden: true },
                    { name: 'nombre_inmediato', index: 'nombre_inmediato', align: 'left', width: anchoP(ancho, 4) },
                    { name: 'fecha_planeada', index: 'fecha_planeada', align: 'left', hidden: true },
                    { name: 'id_profesional_elaboro', index: 'id_profesional_elaboro', hidden: true },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', align: 'left', width: anchoP(ancho, 8) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', align: 'left', width: anchoP(ancho, 1) },
                ],
                //             pager: jQuery('#listOrdenesMedicamentos'), 
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    //  			     if(datosRow.Id_Transac != ''){	  

                    asignaAtributo('lblIdOrden', datosRow.id, 0);
                    asignaAtributo('lblIdArticulo', datosRow.idArticulo, 0)
                    asignaAtributo('lblNombreArticulo', datosRow.Articulo, 0)


                    asignaAtributo('txtIdArticulo', datosRow.idArticulo + '-' + datosRow.Articulo, 0)
                    asignaAtributo('cmbIdVia', datosRow.id_via, 0)
                    asignaAtributo('cmbIdTipoDosis', datosRow.id_tipo_dosis, 0)
                    asignaAtributo('cmbIdRepeticionProgramada', datosRow.id_repeticion_programada, 0)
                    asignaAtributo('cmbIdIntervalo', datosRow.Intervalo, 0)
                    asignaAtributo('txtObservaciones', datosRow.observaciones, 0)
                    asignaAtributo('chkInmediato', datosRow.id_inmediato, 0)


                    asignaAtributo('chkInmediato', datosRow.id_inmediato, 0)
                    // $('#drag'+ventanaActual.num).find('#txtIdArticulo').focus();						   


                    abrirVentanitaOrdenMedicamentos();
                    //		    }else alert('NO EXISTE SUMINISTRO POR FARMACIA')				   
                },

                height: 250,
                width: ancho + 20,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listTrazabilidadMedicamento':
            //  limpiarDivEditarJuan(arg); 
            //aquilimpiara? cerrarVentanitaInmediatoMedicamentos();
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=208&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));
            add_valores_a_mandar(valorAtributo('lblIdArticulo'));
            add_valores_a_mandar(valorAtributo('lblIdOrden'));

            // alert(valores_a_mandar);

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id solicitud', 'id documento', 'Tipo', 'idArticulo', 'Articulo', 'id_via', 'Via', 'id_tipo_dosis', 'Detalle', 'Tipo Dosis Nombre',
                    'id_repeticion_programada', 'Duraci�n', 'Intervalo', 'observaciones', 'id_inmediato', 'Inmediato', 'Fecha Planeada', 'id_profesional_elaboro', 'Fecha Inicio Consumo', 'Profesional'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'documento', index: 'documento', hidden: true },
                    { name: 'tipo', index: 'tipo', width: anchoP(ancho, 2) },
                    { name: 'idArticulo', index: 'Articulo', hidden: true },
                    { name: 'Articulo', index: 'Articulo', width: anchoP(ancho, 16) },
                    { name: 'id_via', index: 'id_via', hidden: true },
                    { name: 'nombre_via', index: 'nombre_via', align: 'left', width: anchoP(ancho, 4) },
                    { name: 'id_tipo_dosis', index: 'id_tipo_dosis', hidden: true },
                    { name: 'detalle_dosis_frecuencia', index: 'detalle_dosis_frecuencia', align: 'left', width: anchoP(ancho, 10) },
                    { name: 'nombre_tipo_dosis', index: 'nombre_tipo_dosis', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'id_repeticion_programada', index: 'id_repeticion_programada', hidden: true },
                    { name: 'nombre_repeticion_programada', index: 'nombre_repeticion_programada', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'Intervalo', index: 'Intervalo', align: 'left', width: anchoP(ancho, 4) },
                    { name: 'observaciones', index: 'observaciones', align: 'left', width: anchoP(ancho, 10) },
                    { name: 'id_inmediato', index: 'id_inmediato', hidden: true },
                    { name: 'nombre_inmediato', index: 'nombre_inmediato', align: 'left', width: anchoP(ancho, 4) },
                    { name: 'fecha_planeada', index: 'fecha_planeada', hidden: true },
                    { name: 'id_profesional_elaboro', index: 'id_profesional_elaboro', hidden: true },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', align: 'left', width: anchoP(ancho, 10) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', align: 'left', width: anchoP(ancho, 1) },
                ],
                //             pager: jQuery('#listOrdenesMedicamentos'), 
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    //  			     if(datosRow.Id_Transac != ''){	  

                    asignaAtributo('lblIdOrden', datosRow.id, 0);
                    asignaAtributo('lblIdArticulo', datosRow.idArticulo, 0)
                    asignaAtributo('lblNombreArticulo', datosRow.Articulo, 0)
                    abrirVentanitaOrdenMedicamentos();
                    //		    }else alert('NO EXISTE SUMINISTRO POR FARMACIA')				   
                },

                height: 200,
                width: ancho + 20,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listDocumentosHistoricosTraer':
            limpiarDivEditarJuan(arg);
            //			 $('#drag'+ventanaActual.num).find('#divContenido').css('height','400px');				 
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=198&parametros=";
            add_valores_a_mandar(valorAtributo('cmbTodoTipo'));
            add_valores_a_mandar(valorAtributo('lblIdHc'));
            //			 alert(valores_a_mandar);

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Tipo', 'Descripci�n', 'Fecha elabor�', 'Profesional elabor�', 'idEstadoImpr', 'idEstadoDoc', 'Estado', 'Auditado', 'Auditor', 'id Documento'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'Tipo', index: 'Tipo', align: 'left', hidden: true },
                    { name: 'Descripcion', index: 'Descripcion', align: 'left', width: anchoP(ancho, 30) },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 8) },
                    { name: 'Profesional', index: 'Profesional', align: 'left', width: anchoP(ancho, 10) },
                    { name: 'idEstadoImpr', index: 'idEstadoImpr', hidden: true },
                    { name: 'idEstadoDoc', index: 'idEstadoDoc', hidden: true },
                    { name: 'Estado', index: 'Estado', align: 'left', width: anchoP(ancho, 7) },
                    { name: 'Auditado', index: 'Auditado', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'Auditor', index: 'Auditor', hidden: true },
                    { name: 'idDocumento', index: 'idDocumento', align: 'left', width: anchoP(ancho, 5) },
                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdDocumentoTraer', datosRow.idDocumento, 0)

                    buscarHC('listOrdenesMedicamentosTraer', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')


                },
                height: 80,
                width: ancho + 50,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listOrdenesMedicamentosTraer':
            limpiarDivEditarJuan(arg);
            cerrarVentanitaInmediatoMedicamentos();
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=225&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumentoTraer'));
            // alert(valores_a_mandar);

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id solicitud', 'id documento', 'Tipo', 'idArticulo', 'Articulo', 'id_via', 'Via', 'id_tipo_dosis', 'Detalle', 'Tipo Dosis',
                    'id_repeticion_programada', 'Duraci�n', 'Dias Transcurridos', 'Dias Faltan', 'id_solicitud_medicamento_inicial', 'Intervalo', 'Indicaciones', 'id_inmediato', 'Inmediato', 'Fecha Planeada',
                    'id_profesional_elaboro', 'Inicio Cumplimiento de Orden', 'Fin del cumplimiento de Orden'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id', index: 'id', align: 'left', hidden: true },
                    { name: 'documento', index: 'documento', hidden: true },
                    { name: 'tipo', index: 'tipo', hidden: true },
                    { name: 'idArticulo', index: 'Articulo', hidden: true },
                    { name: 'Articulo', index: 'Articulo', width: anchoP(ancho, 17) },
                    { name: 'id_via', index: 'id_via', hidden: true },
                    { name: 'nombre_via', index: 'nombre_via', align: 'left', width: anchoP(ancho, 3) },
                    { name: 'id_tipo_dosis', index: 'id_tipo_dosis', hidden: true },
                    { name: 'detalle_dosis_frecuencia', index: 'detalle_dosis_frecuencia', align: 'left', width: anchoP(ancho, 10) },
                    { name: 'nombre_tipo_dosis', index: 'nombre_tipo_dosis', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'id_repeticion_programada', index: 'id_repeticion_programada', hidden: true },
                    { name: 'nombre_repeticion_programada', index: 'nombre_repeticion_programada', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'cuantosDias', index: 'cuantosDias', align: 'left', width: anchoP(ancho, 8) },
                    { name: 'diasFaltan', index: 'diasFaltan', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'id_solicitud_medicamento_inicial', index: 'id_solicitud_medicamento_inicial', hidden: true },

                    { name: 'Intervalo', index: 'Intervalo', align: 'left', width: anchoP(ancho, 4) },
                    { name: 'observaciones', index: 'observaciones', align: 'left', width: anchoP(ancho, 10) },
                    { name: 'id_inmediato', index: 'id_inmediato', hidden: true },
                    { name: 'nombre_inmediato', index: 'nombre_inmediato', align: 'left', width: anchoP(ancho, 4) },
                    { name: 'fecha_planeada', index: 'fecha_planeada', hidden: true },
                    { name: 'id_profesional_elaboro', index: 'id_profesional_elaboro', hidden: true },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', align: 'left', width: anchoP(ancho, 10) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', width: anchoP(ancho, 10) },
                ],
                //             pager: jQuery('#listOrdenesMedicamentos'), 
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    if (datosRow.id_inmediato == 0) {

                        asignaAtributo('lblIdArticuloTraer', datosRow.idArticulo)
                        asignaAtributo('lblNombreArticuloTraer', datosRow.Articulo)
                        asignaAtributo('lblIdSolicitudTraer', datosRow.id, 0)
                        asignaAtributo('cmbIdNuevaDuracion', datosRow.diasFaltan, 0)
                        asignaAtributo('lblIdSolicitudMedicamentoInicial', datosRow.id_solicitud_medicamento_inicial, 0)

                        //abrirVentanita('divCambioDuracion');	

                        buscarHC('listOrdenesMedicamentosTratamiento', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

                        // modificarCRUD('traerMedicamentosOrdenesAnteriores', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')
                    } else
                        alert('LOS INMEDIATOS NO SE PUEDEN TRAER')

                },

                height: 280,
                width: ancho + 20,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listOrdenesMedicamentosTratamiento':
            limpiarDivEditarJuan(arg);
            cerrarVentanitaInmediatoMedicamentos();
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=226&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdSolicitudMedicamentoInicial'));
            add_valores_a_mandar(valorAtributo('lblIdSolicitudMedicamentoInicial'));
            // alert(valores_a_mandar);

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id solicitud', 'id documento', 'Tipo', 'idArticulo', 'Articulo', 'id_via', 'Via', 'id_tipo_dosis', 'Detalle', 'Tipo Dosis',
                    'id_repeticion_programada', 'Duraci�n', 'Dias Transcurridos', 'Dias Faltan', 'id_solicitud_medicamento_inicial', 'Intervalo', 'Indicaciones', 'id_inmediato', 'Inmediato', 'Fecha Planeada',
                    'id_profesional_elaboro', 'Inicio Cumplimiento de Orden', 'Profesional'],

                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', align: 'left', width: anchoP(ancho, 3) },
                    { name: 'documento', index: 'documento', hidden: true },
                    { name: 'tipo', index: 'tipo', hidden: true },
                    { name: 'idArticulo', index: 'Articulo', hidden: true },
                    { name: 'Articulo', index: 'Articulo', width: anchoP(ancho, 20) },
                    { name: 'id_via', index: 'id_via', hidden: true },
                    { name: 'nombre_via', index: 'nombre_via', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'id_tipo_dosis', index: 'id_tipo_dosis', hidden: true },
                    { name: 'detalle_dosis_frecuencia', index: 'detalle_dosis_frecuencia', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'nombre_tipo_dosis', index: 'nombre_tipo_dosis', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'id_repeticion_programada', index: 'id_repeticion_programada', hidden: true },
                    { name: 'nombre_repeticion_programada', index: 'nombre_repeticion_programada', align: 'left', width: anchoP(ancho, 7) },
                    { name: 'cuantosDias', index: 'cuantosDias', align: 'left', width: anchoP(ancho, 7) },
                    { name: 'diasFaltan', index: 'diasFaltan', hidden: true },
                    { name: 'id_solicitud_medicamento_inicial', index: 'id_solicitud_medicamento_inicial', hidden: true },

                    { name: 'Intervalo', index: 'Intervalo', align: 'left', width: anchoP(ancho, 5) },
                    { name: 'observaciones', index: 'observaciones', align: 'left', width: anchoP(ancho, 10) },
                    { name: 'id_inmediato', index: 'id_inmediato', hidden: true },
                    { name: 'nombre_inmediato', index: 'nombre_inmediato', hidden: true },
                    { name: 'fecha_planeada', index: 'fecha_planeada', hidden: true },
                    { name: 'id_profesional_elaboro', index: 'id_profesional_elaboro', hidden: true },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', align: 'left', width: anchoP(ancho, 8) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', hidden: true },
                ],
                //             pager: jQuery('#listOrdenesMedicamentos'), 
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    if (datosRow.id_inmediato == 0) {
                        asignaAtributo('lblIdSolicitudTraer', datosRow.id, 0)

                        asignaAtributo('lblDiasPlaneadaDuracion', datosRow.id_repeticion_programada)
                        asignaAtributo('lblDiasTranscurridos', datosRow.cuantosDias)
                        asignaAtributo('cmbIdNuevaDuracion', datosRow.diasFaltan, 0)
                        //  abrirVentanita('divCambioDuracion');	

                        buscarHC('listOrdenesMedicamentosTratamiento', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

                        // modificarCRUD('traerMedicamentosOrdenesAnteriores', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')
                    } else
                        alert('LOS INMEDIATOS NO SE PUEDEN TRAER')

                },

                height: 100,
                width: ancho + 20,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        /*
         case 'listOrdenesMedicamentosTraerAuditoria':   
         limpiarDivEditarJuan(arg); 
         cerrarVentanitaInmediatoMedicamentos();
         valores_a_mandar=pag;
         valores_a_mandar=valores_a_mandar+"?idQuery=201&parametros=";
         add_valores_a_mandar(valorAtributo('lblIdDocumentoTraer'));	
         add_valores_a_mandar(valorAtributo('lblIdArticulo'));				 
         // alert(valores_a_mandar);
         
         $('#drag'+ventanaActual.num).find("#"+arg).jqGrid({ 
         url:valores_a_mandar, 	
         datatype: 'xml', 
         mtype: 'GET', 
         colNames:[ 'contador','Id Orden','id documento','Tipo','idArticulo','Articulo','id_via', 'Via', 'id_tipo_dosis', 'Detalle','Dosis', 
         'id_repeticion_programada', 'Programada','Intervalo','observaciones','id_inmediato','Inmediato','Fecha Planeada', 'id_profesional_elaboro', 'Fecha elaboro','Profesional' 
         ,'ID_ORNED_INICIA_PROYEC'],			 
         colModel :[ 
         {name:'contador', index:'contador', hidden:true},  
         {name:'id', index:'id',  align:'left', width: anchoP(ancho,3)},  			   
         {name:'documento', index:'documento', hidden:true},  
         {name:'tipo', index:'tipo',  width: anchoP(ancho,3)},  			   
         {name:'idArticulo', index:'Articulo',  hidden:true},  
         {name:'Articulo', index:'Articulo', width: anchoP(ancho,20)},  			   
         {name:'id_via', index:'id_via', hidden:true},  	
         {name:'nombre_via', index:'nombre_via', align:'left', width: anchoP(ancho,5)},  				   
         {name:'id_tipo_dosis', index:'id_tipo_dosis', hidden:true},  				   
         {name:'detalle_dosis_frecuencia', index:'detalle_dosis_frecuencia', align:'left', width: anchoP(ancho,5)},  				   
         {name:'nombre_tipo_dosis', index:'nombre_tipo_dosis', align:'left', width: anchoP(ancho,5)},  				   
         {name:'id_repeticion_programada', index:'id_repeticion_programada', hidden:true},  
         {name:'nombre_repeticion_programada', index:'nombre_repeticion_programada', align:'left', width: anchoP(ancho,7)},  	
         {name:'Intervalo', index:'Intervalo', align:'left', width: anchoP(ancho,5)},  				   			   			   
         {name:'observaciones', index:'observaciones',hidden:true},  				   
         {name:'id_inmediato', index:'id_inmediato', hidden:true},  				   
         {name:'nombre_inmediato', index:'nombre_inmediato', hidden:true},  				   
         {name:'fecha_planeada', index:'fecha_planeada', hidden:true},  				   
         {name:'id_profesional_elaboro', index:'id_profesional_elaboro', hidden:true},  				   			   
         {name:'fecha_elaboro', index:'fecha_elaboro',  hidden:true},  				   			   			   			   
         {name:'usuario_elaboro', index:'usuario_elaboro' , hidden:true},  
         
         {name:'ID_ORNED_INICIA_PROYEC', index:'ID_ORNED_INICIA_PROYEC',  width: anchoP(ancho,3)},
         
         ], 			 
         //             pager: jQuery('#listOrdenesMedicamentos'), 
         onSelectRow: function(rowid) { 
         
         var datosRow = jQuery('#drag'+ventanaActual.num).find('#'+arg).getRowData(rowid); 					  
         
         if(datosRow.id_inmediato==0){
         
         asignaAtributo('lblIdOrdenIniciaProyeccion', datosRow.ID_ORNED_INICIA_PROYEC, 0)
         asignaAtributo('lblIdOrdenHistorico', datosRow.id, 0)
         asignaAtributo('lblIdDocumentoHistorico', datosRow.idDocumento, 0)
         
         buscarHC('listProyeccionOrdenHistorico' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )
         
         }
         else alert('LOS INMEDIATOS NO SE PUEDEN PROYECTAR')
         
         },	
         
         height: 100, 
         width: ancho+20,
         
         });  
         $('#drag'+ventanaActual.num).find("#"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
         break;	
         */
        case 'listProyeccionOrdenHistorico':

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=202&parametros=";
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['contador', 'Id Doc', 'Id Solicitud', 'Dias', 'Cantidad Ordenada', 'id_tipo_dosis', 'Dosis', 'Cantidad Despachada', 'Fecha Programada', 'Hora', 'Enfermeria'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 5) },
                    { name: 'id_Doc', index: 'id_Doc', width: anchoP(ancho, 10) },
                    { name: 'S', index: 'S', width: anchoP(ancho, 10) },
                    { name: 'cuantos_dias', index: 'cuantos_dias', width: anchoP(ancho, 10) },
                    { name: 'id_ordenes_dosis_cantidad', index: 'id_ordenes_dosis_cantidad', width: anchoP(ancho, 15) },
                    { name: 'id_tipo_dosis', index: 'id_tipo_dosis', hidden: true },
                    { name: 'nombre_tipo_dosis', index: 'nombre_tipo_dosis', width: anchoP(ancho, 15) },
                    { name: 'cantidad_despachada', index: 'cantidad_despachada', width: anchoP(ancho, 20) },
                    { name: 'fecha_programada', index: 'fecha_programada', width: anchoP(ancho, 15) },
                    { name: 'hora', index: 'hora', width: anchoP(ancho, 5) },
                    { name: 'Administrado', index: 'Administrado', width: anchoP(ancho, 15) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 300,
                width: ancho + 20,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


    }
}


function nuevaIdentificacion(atributo) {

    if ($('#drag' + ventanaActual.num).find('#' + atributo).attr('checked')) {
        if (valorAtributo('txtIdPaciente') != '') {
            $('#drag' + ventanaActual.num).find('#txtNuevaIdPaciente').attr('disabled', '');
            mostrar('divEditarIdNuevaPaciente')
        } else {
            $('#drag' + ventanaActual.num).find('#' + atributo).attr('checked', '')
            alert('Debe seleccionar una Persona')
            ocultar('divEditarIdNuevaPaciente')
        }
    } else {
        $('#drag' + ventanaActual.num).find('#txtNuevaIdPaciente').attr('disabled', 'disabled');
        ocultar('divEditarIdNuevaPaciente')
    }



    //	if(valorAtributo(atributo))

}

function mostrarNuevoCRUD(arg) {

    limpiarDivEditarJuan(arg); // pa crear un elemento nuevo hay que limpiar primero 
    switch (arg) {
        case 'xxx':

            break;
    }
}
function crearCRUD(arg, pag) {
    pagina = pag;
    paginaActual = arg;

    switch (arg) {
        case 'paciente':
            if (verificarCamposGuardar(arg)) {
                if (valorAtributo('lblId') == "") {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=32&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbTipoId'));
                    add_valores_a_mandar(valorAtributo('cmbTipoId') + valorAtributo('txtIdPaciente'));
                    add_valores_a_mandar(valorAtributo('cmbTipoId') + valorAtributo('txtIdPaciente'));
                    add_valores_a_mandar(valorAtributo('txtNombre1'));
                    add_valores_a_mandar(valorAtributo('txtNombre2'));
                    add_valores_a_mandar(valorAtributo('txtApellido1'));
                    add_valores_a_mandar(valorAtributo('txtApellido2'));

                    if (valorAtributo('txtNombre2') == '') {
                        if (valorAtributo('txtApellido2') == '') {
                            add_valores_a_mandar(valorAtributo('txtNombre1') + ' ' + valorAtributo('txtApellido1'));
                        } else
                            add_valores_a_mandar(valorAtributo('txtNombre1') + ' ' + valorAtributo('txtApellido1') + ' ' + valorAtributo('txtApellido2'));
                    } else {
                        if (valorAtributo('txtApellido2') == '') {
                            add_valores_a_mandar(valorAtributo('txtNombre1') + ' ' + valorAtributo('txtNombre2') + ' ' + valorAtributo('txtApellido1'));
                        } else {
                            add_valores_a_mandar(valorAtributo('txtNombre1') + ' ' + valorAtributo('txtNombre2') + ' ' + valorAtributo('txtApellido1') + ' ' + valorAtributo('txtApellido2'));
                        }
                    }

                    add_valores_a_mandar(valorAtributo('cmbSexo'));
                    add_valores_a_mandar(valorAtributo('txtFechaNacimiento'));
                    add_valores_a_mandar(valorAtributo('txtTelefono'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipioResi'));
                    add_valores_a_mandar(valorAtributo('txtDireccion'));
                    add_valores_a_mandar(valorAtributo('txtObservaciones'));
                    ajaxModificar();
                } else
                    alert('No se puede crear, debe ir a Nuevo');
            }
            break;
        case 'listjjjjjjjjjjjj':

            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=32&parametros=";
            add_valores_a_mandar(valorAtributo('cmbTipoId'));

            add_valores_a_mandar(valorAtributo('cmbSexo'));
            add_valores_a_mandar(valorAtributo('txtFechaNacimiento'));
            add_valores_a_mandar(valorAtributo('txtTelefono'));
            add_valores_a_mandar(valorAtributo('txtDireccion'));
            add_valores_a_mandar(valorAtributo('txtObservaciones'));
            ajaxModificar();
            break;




    }
}



function modificarCRUD(arg, pag) {

    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;
    switch (arg) {

        case 'eliminarDatosPlantillaEvolucion':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=711&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            ajaxModificar();
        break;

        case 'eliminarTratamientoPorDiente':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=702&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdTnsDienteTratamien'));
                ajaxModificar();
            }
            break;
        case 'tratamientoPorDiente':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=700&parametros=";
                add_valores_a_mandar($('#drag' + ventanaActual.num).find('#idDienteElegido').html());
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('txtDesTratamientoPorDiente'));
                ajaxModificar();
            }
            break;

        case 'dejarTratamientoInicial':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=699&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                ajaxModificar();
            }
        break;
        case 'eliminarTratamientoInicial':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=710&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                ajaxModificar();
            }
        break;		
        case 'limpiarDiente':
            if (verificarCamposGuardar(arg)) {
                var id_trataDiente = 0
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=698&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                add_valores_a_mandar(valorAtributo('lblIdTipoTratamiento'));
                add_valores_a_mandar($('#drag' + ventanaActual.num).find('#idDienteElegido').html());

                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                add_valores_a_mandar(valorAtributo('lblIdTipoTratamiento'));
                add_valores_a_mandar($('#drag' + ventanaActual.num).find('#idDienteElegido').html());
                add_valores_a_mandar('0'); //v
                add_valores_a_mandar('0'); //m
                add_valores_a_mandar('0'); //l
                add_valores_a_mandar('0'); //d
                add_valores_a_mandar('0'); //o								
                id_trataDiente = 0
                add_valores_a_mandar(id_trataDiente); //idtratam
                //alert('valores_a_mandar='+valores_a_mandar)
                ajaxModificar();
            }
        break;
        case 'guardarDienteYTratamiento':
            if (verificarCamposGuardar(arg)) {
                var id_trataDiente = 0

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=698&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                add_valores_a_mandar('S');
                add_valores_a_mandar($('#drag' + ventanaActual.num).find('#idDienteElegido').html());

                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
                add_valores_a_mandar('S');
                add_valores_a_mandar($('#drag' + ventanaActual.num).find('#idDienteElegido').html());
                add_valores_a_mandar(diente[100].V); //v
                add_valores_a_mandar(diente[100].M); //m
                add_valores_a_mandar(diente[100].L); //l
                add_valores_a_mandar(diente[100].D); //d
                add_valores_a_mandar(diente[100].O); //o				

                if (diente[100].sellante == 'sellante')
                    id_trataDiente = 1
                else if (diente[100].sellanteXhacer == 'sellanteXhacer')
                    id_trataDiente = 12
                else if (diente[100].protesis == 'protesis')
                    id_trataDiente = 2
                else if (diente[100].protesisXhacer == 'protesisXhacer')
                    id_trataDiente = 3
                else if (diente[100].protesisRemovible == 'protesisRemovible')
                    id_trataDiente = 13

                if (diente[100].corona == 'corona')
                    id_trataDiente = 4
                else if (diente[100].coronaXhacer == 'coronaXhacer')
                    id_trataDiente = 5
                if (diente[100].nucleo == 'nucleo')
                    id_trataDiente = 15
                else if (diente[100].nucleoXhacer == 'nucleoXhacer')
                    id_trataDiente = 16
                else if (diente[100].endodoncia == 'endodoncia')
                    id_trataDiente = 6
                else if (diente[100].endodonciaXhacer == 'endodonciaXhacer')
                    id_trataDiente = 7
                else if (diente[100].exodonciaIndi == 'exodonciaIndi')
                    id_trataDiente = 8
                else if (diente[100].extraido == 'extraido')
                    id_trataDiente = 9
                else if (diente[100].incluido == 'incluido')
                    id_trataDiente = 14
                else if (diente[100].sinErupcionar == 'sinErupcionar')
                    id_trataDiente = 10
                else if (diente[100].erupcion == 'erupcion')
                    id_trataDiente = 11

                add_valores_a_mandar(id_trataDiente); //idtratam
                //	alert('valores_a_mandar='+valores_a_mandar)
                ajaxModificar();
            }
            break;
        case 'crearExamenFisicoDescripcion':
            if (valorAtributo('lblIdDocumento') != '') {

                if (valorAtributo('txtIdEsdadoDocumento') == 0 || valorAtributo('txtIdEsdadoDocumento') == 2 || valorAtributo('txtIdEsdadoDocumento') == 3 || valorAtributo('txtIdEsdadoDocumento') == 4 || valorAtributo('txtIdEsdadoDocumento') == 5 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 12) {

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=695&parametros=";


                    add_valores_a_mandar(valorAtributo('txtExamenFisicoDescripcion'));
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    ajaxModificar();

                } else alert('EL ESTADO DEL FOLIO NO PERMITE MODIFICAR')
            } else alert('SELECCIONE UN DOCUMENTO')
            break;
        case 'listMedicamentosEditar':

            if (valorAtributo('txtIdEsdadoDocumento') == '1') {
                alert('EL ESTADO DEL FOLIO NO PERMITE EDITAR')
            } else if (valorAtributo('txtIdEsdadoDocumento') == '8') {
                alert('EL ESTADO DEL FOLIO NO PERMITE EITAR')
            } else if (valorAtributo('txtIdEsdadoDocumento') == '9') {
                alert('EL ESTADO DEL FOLIO NO PERMITE EDITAR')
            } else {

                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=691&parametros=";

                    add_valores_a_mandar(valorAtributo('cmbIdViaEditar'));
                    add_valores_a_mandar(valorAtributo('cmbUnidadEditar'));
                    add_valores_a_mandar(valorAtributo('cmbCantEditar'));
                    add_valores_a_mandar(valorAtributo('cmbFrecuenciaEditar'));
                    add_valores_a_mandar(valorAtributo('cmbDiasEditar'));
                    add_valores_a_mandar(valorAtributo('txtCantidadEditar'));
                    add_valores_a_mandar(valorAtributo('cmbUnidadFarmaceuticaEditar'));
                    add_valores_a_mandar(valorAtributo('txtIndicacionesEditar'));
                    add_valores_a_mandar(valorAtributo('lblIdEditar'));

                    ajaxModificar();

                }
            }

            break;

        case 'verificarEmbarazoProcedimientoSi':
            guardarRespuestaEmbarazo(arg, 'SI', 'lblIdDocumento');
            break;
        case 'verificarEmbarazoProcedimientoNoSabe':
            guardarRespuestaEmbarazo(arg, 'NO SABE', 'lblIdDocumento');
            break;
        case 'verificarEmbarazoProcedimientoNo':
            guardarRespuestaEmbarazo(arg, 'NO', 'lblIdDocumento');
            break;

        case 'verificarEmbarazoAyudaDiagnosticaSi':
            guardarRespuestaEmbarazo(arg, 'SI', 'lblIdDocumento');
            break;
        case 'verificarEmbarazoAyudaDiagnosticaNoSabe':
            guardarRespuestaEmbarazo(arg, 'NO SABE', 'lblIdDocumento');
            break;
        case 'verificarEmbarazoAyudaDiagnosticaNo':
            guardarRespuestaEmbarazo(arg, 'NO', 'lblIdDocumento');
            break;

        case 'verificarEmbarazoTerapiaSi':
            guardarRespuestaEmbarazo(arg, 'SI', 'lblIdDocumento');
            break;
        case 'verificarEmbarazoTerapiaNoSabe':
            guardarRespuestaEmbarazo(arg, 'NO SABE', 'lblIdDocumento');
            break;
        case 'verificarEmbarazoTerapiaNo':
            guardarRespuestaEmbarazo(arg, 'NO', 'lblIdDocumento');
            break;

        case 'verificarEmbarazoLaboratorioSi':
            guardarRespuestaEmbarazo(arg, 'SI', 'lblIdDocumento');
            break;
        case 'verificarEmbarazoLaboratorioNoSabe':
            guardarRespuestaEmbarazo(arg, 'NO SABE', 'lblIdDocumento');
            break;
        case 'verificarEmbarazoLaboratorioNo':
            guardarRespuestaEmbarazo(arg, 'NO', 'lblIdDocumento');
            break;







        case 'listProcedimientosDetalleEditar':

            if (valorAtributo('txtIdEsdadoDocumento') == '1') {
                alert('EL ESTADO DEL FOLIO NO PERMITE EDITAR')
            } else if (valorAtributo('txtIdEsdadoDocumento') == '8') {
                alert('EL ESTADO DEL FOLIO NO PERMITE EITAR')
            } else if (valorAtributo('txtIdEsdadoDocumento') == '9') {
                alert('EL ESTADO DEL FOLIO NO PERMITE EDITAR')
            } else {

                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=673&parametros=";

                    add_valores_a_mandar(valorAtributo('cmbCantidadTratamientoEditar'));
                    add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgicoTratamientoEditar'));
                    add_valores_a_mandar(valorAtributo('txtIndicacionTratamientoEditar'));
                    add_valores_a_mandar(valorAtributo('cmbProcedimientoTratamientoEditar'));
                    add_valores_a_mandar(valorAtributo('lblId'));
                    ajaxModificar();
                }
            }

            break;


        case 'editaAsistenciaPaciente':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=655&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdAsisteEdit'));
                add_valores_a_mandar(valorAtributo('cmbIdPreferencialEdit'));
                add_valores_a_mandar(valorAtributo('txtObservacionEdit'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('lblIdCitaEdit'));
                ajaxModificar();
            }
            break;
        case 'eliminarDescuento':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=649&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdArtDescuento'));
                add_valores_a_mandar(valorAtributo('lblSerialDescuento'));
                add_valores_a_mandar(valorAtributo('txtDescuentoArt'));

                ajaxModificar();
            }
            break;
        case 'crearDescuento':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=648&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdArtDescuento'));
                add_valores_a_mandar(valorAtributo('txtSerial'));
                add_valores_a_mandar(valorAtributo('txtDescuentoArt'));

                ajaxModificar();
            }
            break;
        case 'traerTrEgresos':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=641&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));	 /* idDoc destino */
                add_valores_a_mandar(valorAtributo('txtDocEgreso'));		/* doc origen */

                ajaxModificar();
            }
            break;
        case 'crearArchivosRips':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=639&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdEps'));
                add_valores_a_mandar(valorAtributo('cmbIdAnio'));
                add_valores_a_mandar(valorAtributo('cmbIdMes'));

                ajaxModificar();
            }
            break;
        case 'eliminarNotificacionDestino':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=622&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdNotificacionEnviada'));
                ajaxModificar();
            }
            break;
        case 'crearNotificacion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=618&parametros=";

                add_valores_a_mandar(valorAtributo('txtAsunto'));
                add_valores_a_mandar(valorAtributo('txtDescripcion'));
                add_valores_a_mandar(IdSesion());
                ajaxModificar();
            }
            break;
        case 'leerNotificacion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=617&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdNotDestino'));
                ajaxModificar();
            }
            break;
        case 'guardarNoLente':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=602&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('txt_BIOM_C10'));
                add_valores_a_mandar(LoginSesion());

                add_valores_a_mandar(valorAtributo('txt_BIOM_C10'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));

                ajaxModificar();
            }
            break;
        case 'crearnotificacionPersona':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=620&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdNotificacion'));
                add_valores_a_mandar(valorAtributo('cmbIdPersona'));

                ajaxModificar();
            }
            break;
        case 'eliminarGrupoUsuario':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=600&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdGrupo'));
                add_valores_a_mandar(valorAtributo('txtIdentificacion'));

                ajaxModificar();
            }
            break;
        case 'crearGrupoUsuario':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=599&parametros=";

                add_valores_a_mandar(valorAtributo('cmbIdGrupo'));
                add_valores_a_mandar(valorAtributo('txtIdentificacion'));

                ajaxModificar();
            }
            break;

        case 'modificarUsuario':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=597&parametros=";

                add_valores_a_mandar(valorAtributo('txtPNombre'));
                add_valores_a_mandar(valorAtributo('txtSNombre'));
                add_valores_a_mandar(valorAtributo('txtPApellido'));
                add_valores_a_mandar(valorAtributo('txtSApellido'));
                add_valores_a_mandar(valorAtributo('txtNomPersonal'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoProfesion'));
                add_valores_a_mandar(valorAtributo('txtRegistro'));

                add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                add_valores_a_mandar(valorAtributo('cmbTipoId'));

                /* usuario */
                add_valores_a_mandar(valorAtributo('txtUSuario'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbTipoId'));
                add_valores_a_mandar(valorAtributo('txtIdentificacion'));

                ajaxModificar();
            }
            break;
        case 'crearUsuario':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=596&parametros=";
                add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                add_valores_a_mandar(valorAtributo('cmbTipoId'));
                add_valores_a_mandar(valorAtributo('txtPNombre'));
                add_valores_a_mandar(valorAtributo('txtSNombre'));
                add_valores_a_mandar(valorAtributo('txtPApellido'));
                add_valores_a_mandar(valorAtributo('txtSApellido'));
                add_valores_a_mandar(valorAtributo('txtNomPersonal'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoProfesion'));
                add_valores_a_mandar(valorAtributo('txtRegistro'));
                /* usuario */
                add_valores_a_mandar(valorAtributo('cmbTipoId'));
                add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                add_valores_a_mandar(valorAtributo('txtUSuario'));
                ajaxModificar();
            }
            break;
        case 'modificarFechaDocumentoInventario':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=586&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributo('txtFechaModifica'));
                add_valores_a_mandar(valorAtributo('txtFechaModifica'));
                add_valores_a_mandar(valorAtributo('txtFechaModifica'));
                ajaxModificar();
            }
            break;
        case 'modificaTransaccionSerial':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=578&parametros=";

                add_valores_a_mandar(valorAtributo('txtSerial'));
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                ajaxModificar();
            }
            break;
        case 'anularRecibo':
            if (valorAtributo('lblIdEstadoRecibo') != 'A') {
                if (valorAtributo('cmbIdMotivoAnulacionRecibo') != '') {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=570&parametros=";
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributo('cmbIdMotivoAnulacionRecibo'));
                    add_valores_a_mandar(valorAtributo('lblIdRecibo'));
                    ajaxModificar();
                } else
                    alert('SELECCIONE MOTIVO DE LA ANULACION')
            } else
                alert('No se puede anular porque ya se encuentra anulado')
            break;
        case 'crearRecibo':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=568&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdEditPacienteRecibo'));
                add_valores_a_mandar(valorAtributo('txtValorUnidadRecibo'));
                add_valores_a_mandar(valorAtributo('lblValorRecibo'));
                add_valores_a_mandar(valorAtributo('cmbCantidadRecibo'));

                add_valores_a_mandar(valorAtributo('cmbIdConceptoRecibo'));
                add_valores_a_mandar(valorAtributo('txtObservacionRecibo'));
                add_valores_a_mandar(IdSesion());
                ajaxModificar();
            }
            break;
        case 'anularFactura':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                //valores_a_mandar = valores_a_mandar + "&idQuery=564&parametros=";
                valores_a_mandar = valores_a_mandar + "&idQuery=697&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('cmbIdMotivoAnulacion'));
                add_valores_a_mandar(valorAtributoCombo('cmbIdMotivoAnulacion'));

                /*add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('cmbIdMotivoAnulacion'));
                add_valores_a_mandar(valorAtributoCombo('cmbIdMotivoAnulacion'));*/


                ajaxModificar();



            }
            break;

        case 'abrirFactura':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=689&parametros=";
                //abrir factura

                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('cmbIdMotivoAbrir'));
                add_valores_a_mandar(valorAtributoCombo('cmbIdMotivoAbrir'));
                add_valores_a_mandar(valorAtributo('lblIdFactura'));

                //abrir cuenta
                /*   add_valores_a_mandar(LoginSesion());
                   add_valores_a_mandar(valorAtributo('cmbIdMotivoAbrir'));
                   add_valores_a_mandar(valorAtributoCombo('cmbIdMotivoAbrir'));
                   add_valores_a_mandar(valorAtributo('lblIdFactura'));*/

                ajaxModificar();

            }
            break;


        case 'cerrarFactura':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=696&parametros=";
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                //add_valores_a_mandar(valorAtributo('lblIdFactura'));
                ajaxModificar();

            }
            break;

        case 'modificarFechaAdmision':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=563&parametros=";
                add_valores_a_mandar(valorAtributo('txtFechaAdmision'));
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                ajaxModificar();
            }
            break;
        case 'modificarProfesiAdmision':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=562&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                ajaxModificar();
            }
            break;
        case 'eliminarLiquidacionCirugia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=558&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdLiquidacion'));
                add_valores_a_mandar(valorAtributo('lblIdLiquidacion'));
                add_valores_a_mandar(valorAtributo('lblIdLiquidacion'));
                ajaxModificar();
            }
            break;
        case 'modificarProcLiquidacion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=547&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdLateralLiquiProced'));
                add_valores_a_mandar(valorAtributo('cmbIdViaLiquiProced'));
                add_valores_a_mandar(valorAtributo('lblIdLiquidacion'));
                add_valores_a_mandar(valorAtributo('lblIdLiquiProced'));
                ajaxModificar();
            }
            break;
        case 'eliminarProcLiquidacion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=546&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdLiquidacion'));
                add_valores_a_mandar(valorAtributo('lblIdLiquiProced'));
                ajaxModificar();
            }
            break;
        case 'eliminarProcLiquidacionDerecho':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=585&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdLiquidacion'));
            add_valores_a_mandar(valorAtributo('lblIdLiquiProced'));
            add_valores_a_mandar(valorAtributo('lblIdLiquiProcedDerecho'));
            ajaxModificar();
            break;
        case 'crearLiquidacionCirugia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=543&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdPlan'));
                add_valores_a_mandar(valorAtributo('lblIdCuenta'));
                add_valores_a_mandar(valorAtributo('cmbIdFinalidad'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoAtencion'));
                add_valores_a_mandar(valorAtributo('cmbIdAnestesiologo'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoAnestesia'));

                add_valores_a_mandar(valorAtributo('chk_Cirujano'));
                add_valores_a_mandar(valorAtributo('chk_Anestesiologo'));
                add_valores_a_mandar(valorAtributo('chk_Ayudante'));
                add_valores_a_mandar(valorAtributo('chk_Sala'));
                add_valores_a_mandar(valorAtributo('chk_Materiales'));

                add_valores_a_mandar(valorAtributo('lblIdCita'));
                //alert('ajaxModificar='+valores_a_mandar)
                ajaxModificar();
            }
            break;
        case 'modificarLiquidacionCirugia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=544&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdLiquidacion'));
                add_valores_a_mandar(valorAtributo('lblIdPlan'));

                add_valores_a_mandar(valorAtributo('cmbIdFinalidad'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoAtencion'));
                add_valores_a_mandar(valorAtributo('cmbIdAnestesiologo'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoAnestesia'));
                add_valores_a_mandar(valorAtributo('chk_Cirujano'));
                add_valores_a_mandar(valorAtributo('chk_Anestesiologo'));
                add_valores_a_mandar(valorAtributo('chk_Ayudante'));
                add_valores_a_mandar(valorAtributo('chk_Sala'));
                add_valores_a_mandar(valorAtributo('chk_Materiales'));

                ajaxModificar();
            }
            break;
        case 'facturarLiquidacionCirugia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=559&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('lblIdCuenta'));
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdLiquidacion'));
                ajaxModificar();
            }
            break;
        case 'eliminarUltimoConsumo':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;

                valores_a_mandar = valores_a_mandar + "&idQuery=103&parametros=";

                add_valores_a_mandar(valorAtributo('cmbCantModificar'));
                add_valores_a_mandar(valorAtributo('lblIdElemento'));
                add_valores_a_mandar(valorAtributo('lblIdHG'));
                add_valores_a_mandar(valorAtributo('lblIdHG'));
                //
                add_valores_a_mandar(valorAtributo('lblIdElemento'));
                add_valores_a_mandar(valorAtributo('lblIdHG'));

                add_valores_a_mandar(valorAtributo('lblIdElemento'));
                add_valores_a_mandar(valorAtributo('lblIdHG'));

                /* PARA INVENTARIO */
                /*
                 add_valores_a_mandar(valorAtributo('lblIdHG'));	
                 add_valores_a_mandar(valorAtributo('lblIdElemento'));
                 add_valores_a_mandar(valorAtributo('lblIdHG'));	
                 add_valores_a_mandar(valorAtributo('lblIdElemento'));
                 add_valores_a_mandar(valorAtributo('lblIdElemento'));
                 */
                add_valores_a_mandar(valorAtributo('lblIdHG'));

                ajaxModificar();
            }
            break;
        case 'modificaKardex': /* modificar los nuevos parametros */
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                if (valorAtributo('txtLote') == '') {
                    idQuery = '534';
                } else {
                    idQuery = '533';
                }
                valores_a_mandar = valores_a_mandar + "&idQuery=" + idQuery + "&parametros=";

                if (valorAtributo('txtLote') == '') {
                    add_valores_a_mandar(valorAtributo('txtExistenciasBodega'));
                    add_valores_a_mandar(valorAtributo('lblIdArticulo'));
                    add_valores_a_mandar(valorAtributo('txtIdBodega'));

                    add_valores_a_mandar(valorAtributo('txtValorUnitario'));
                    add_valores_a_mandar(valorAtributo('lblIdArticulo'));
                    add_valores_a_mandar(valorAtributo('txtValorUnitario'));
                    add_valores_a_mandar(valorAtributo('lblIdArticulo'));
                    add_valores_a_mandar(valorAtributo('lblIdArticulo'));

                } else {
                    add_valores_a_mandar(valorAtributo('txtExistenciasBodega'));
                    add_valores_a_mandar(valorAtributo('lblIdArticulo'));
                    add_valores_a_mandar(valorAtributo('txtIdBodega'));
                    add_valores_a_mandar(valorAtributo('txtLote'));

                    add_valores_a_mandar(valorAtributo('lblIdArticulo'));
                    add_valores_a_mandar(valorAtributo('txtIdBodega'));
                    add_valores_a_mandar(valorAtributo('lblIdArticulo'));
                    add_valores_a_mandar(valorAtributo('txtIdBodega'));

                    add_valores_a_mandar(valorAtributo('txtValorUnitario'));
                    add_valores_a_mandar(valorAtributo('lblIdArticulo'));
                    add_valores_a_mandar(valorAtributo('txtValorUnitario'));
                    add_valores_a_mandar(valorAtributo('lblIdArticulo'));
                    add_valores_a_mandar(valorAtributo('lblIdArticulo'));
                }

                ajaxModificar();
            }
            break;
        case 'valorMaximoEvento':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=537&parametros=";
                add_valores_a_mandar(valorAtributo('txtValorMaximoEvento'));
                add_valores_a_mandar(valorAtributo('lblValorExedente'));
                add_valores_a_mandar(valorAtributo('lblIdCuenta'));
                add_valores_a_mandar(valorAtributo('lblIdCuenta'));
                ajaxModificar();
            }
            break;
        case 'adicionaDescuentoFactura':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=687&parametros=";
                add_valores_a_mandar(valorAtributo('txtValorDescuentoCuenta'));
                add_valores_a_mandar(valorAtributo('cmbIdQuienAutoriza'));
                add_valores_a_mandar(valorAtributo('cmbIdMotivoAutoriza'));
                add_valores_a_mandar(valorAtributo('txtObservacionDescuento'));
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                ajaxModificar();
            }
            break;

        case 'revertirDescuentoFactura':
            if (verificarCamposGuardar(arg)) {

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=688&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                ajaxModificar();

            }
            break;


        /*case 'revertirDescuentoCuentaAutomatico':
        if (verificarCamposGuardar(arg)) {

            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=688&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdCuenta'));
            ajaxModificar();
    	
        }
        break;*/


        case 'crearViaArticulo':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=524&parametros=";

                add_valores_a_mandar(valorAtributo('cmbViaArticulo'));
                add_valores_a_mandar(valorAtributo('txtIdArticulo'));
                ajaxModificar();
            }
            break;
        case 'eliminarViaArticulo':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=525&parametros=";

                add_valores_a_mandar(valorAtributo('txtIdViaArticulo'));
                add_valores_a_mandar(valorAtributo('txtIdArticulo'));

                ajaxModificar();
            }
            break;
        case 'eliminarTransaccionDevHG':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=102&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('lblIdDoc'));

                ajaxModificar();
            }
            break;
        case 'eliminaSGasto':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=609&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdSGasto'));
                ajaxModificar();
            }
            break;
        case 'modificaSGasto':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=608&parametros=";


                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticuloSGastos'));
                add_valores_a_mandar(valorAtributo('txtCantidadSGasto'));
                add_valores_a_mandar(valorAtributo('txtObservacionSGasto'));
                add_valores_a_mandar(IdSesion());

                add_valores_a_mandar(valorAtributo('lblIdSGasto'));
                ajaxModificar();
            }
            break;
        case 'crearSGasto':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;

                /* valida si hay admision */
                if (valorAtributo('lblIdAdmisionAgen') != '') {
                    id_admi = valorAtributo('lblIdAdmisionAgen'); /* solicitud por trg */
                } else {
                    id_admi = 0; /* es solicitud  */
                }

                valores_a_mandar = valores_a_mandar + "&idQuery=607&parametros=";

                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticuloSGastos'));
                add_valores_a_mandar(valorAtributo('txtCantidadSGasto'));
                add_valores_a_mandar(valorAtributo('txtObservacionSGasto'));
                add_valores_a_mandar(IdSesion());

                ajaxModificar();
            }
            break;
        case 'eliminaSolicitud':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=492&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdSolicitud'));
                ajaxModificar();
            }
            break;
        case 'modificaSolicitud':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=491&parametros=";


                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticuloSolicitudes'));
                add_valores_a_mandar(valorAtributo('txtCantidadSolicitud'));
                add_valores_a_mandar(valorAtributo('txtObservacionSolicitud'));
                add_valores_a_mandar(IdSesion());

                add_valores_a_mandar(valorAtributo('lblIdSolicitud'));
                ajaxModificar();
            }
            break;
        case 'crearSolicitud':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=490&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdAdmisionAgen'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticuloSolicitudes'));
                add_valores_a_mandar(valorAtributo('txtCantidadSolicitud'));
                add_valores_a_mandar(valorAtributo('txtObservacionSolicitud'));
                add_valores_a_mandar(IdSesion());

                ajaxModificar();
            }
            break;
        case 'cerrarDocumentoBodegaSalida':
            if (confirm('EL DOCUMENTO SE FINALIZARA Y YA NO SE PODRA MODIFICAR.\n� DESEA CONTINUAR ?')) {
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=94&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdDoc'));
                    add_valores_a_mandar(valorAtributo('txtIdUsuarioSesion'));

                    ajaxModificar();
                }
            }
            break;

        case 'cerrarDocumentoBodegaConsumo':
            if (confirm('EL DOCUMENTO SE FINALIZARA Y YA NO SE PODRA MODIFICAR.\n� DESEA CONTINUAR ?')) {
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=506&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdDoc'));
                    add_valores_a_mandar(IdSesion());
                    ajaxModificar();
                }
            }
            break;
        case 'cerrarDocumentoDevolucion':
            if (confirm('EL DOCUMENTO SE FINALIZARA Y YA NO SE PODRA MODIFICAR.\n� DESEA CONTINUAR ?')) {
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=91&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdDoc'));
                    add_valores_a_mandar(IdSesion());
                    ajaxModificar();
                }
            }
            break;

        case 'cerrarDocumentoBodega': //documentos de entrada 
            if (confirm('EL DOCUMENTO SE FINALIZARA Y YA NO SE PODRA MODIFICAR.\n� DESEA CONTINUAR ?')) {
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=91&parametros=";

                    add_valores_a_mandar(valorAtributo('lblIdDoc'));
                    add_valores_a_mandar(valorAtributo('txtIdUsuarioSesion'));

                    ajaxModificar();
                }
            }
            break;
        case 'eliminaTransaccion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=90&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                ajaxModificar();
            }
            break;
        case 'reemplazarCodigoBarras':
            if (verificarCamposGuardar(arg)) {
                if (confirm('ESTA SEGURO DE MODIFICAR EL CODIGO ANTERIOR POR EL NUEVO ?')) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=582&parametros=";
                    add_valores_a_mandar(valorAtributo('txtCodBarrasNuevo'));
                    //add_valores_a_mandar( LoginSesion() );					  
                    add_valores_a_mandar(valorAtributo('txtCodBarrasBus'));
                    //
                    add_valores_a_mandar(valorAtributo('txtCodBarrasNuevo'));
                    add_valores_a_mandar(valorAtributo('txtCodBarrasBus'));
                    ajaxModificar();
                }
            }
            break;
        case 'eliminaTransaccionSolicitud':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=499&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                ajaxModificar();
            }
            break;
        case 'eliminaTransaccionDevolucion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=503&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                ajaxModificar();
            }
            break;
        case 'crearTransaccionDevolucionCompras':
            if (valorAtributo('lblValorImpuesto') == '') {
                if (valorAtributo('lblNaturaleza') == 'I') {
                    calcularImpuesto('lblValorImpuesto')
                } else {
                    calcularImpuestoSalida()
                }
            }
            if (verificarCamposGuardar(arg)) {
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        var vlrUnitario = valorAtributo('txtValorU');
                        var iva = valorAtributo('cmbIva');
                        var lote = valorAtributo('txtLote');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = valorAtributo('txtPrecioVenta');
                        var fechaDeVencimiento = valorAtributo('txtFechaVencimiento');
                        var valorPromedio = 0;
                        var medida = valorAtributo('cmbMedida');
                        break;
                    case "S":
                        var vlrUnitario = valorAtributo('lblValorUnitario');
                        var iva = valorAtributo('lblIva');
                        //var lote = valorAtributo('cmbIdLote');						
                        var lote = valorAtributo('lblIdLote');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = 0;
                        var fechaDeVencimiento = valorAtributo('txtFV');
                        var valorPromedio = vlrUnitario;
                        var medida = '';// valorAtributo('lblMedida');
                        break;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=89&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(valorAtributo('lblNaturaleza'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);
                add_valores_a_mandar(valorPromedio);
                add_valores_a_mandar(medida);
                ajaxModificar();
            }
            break;
        case 'crearTransaccionDevHG':
            if (valorAtributo('lblValorImpuesto') == '') {
                if (valorAtributo('lblNaturaleza') == 'I') {
                    calcularImpuesto('lblValorImpuesto')
                } else {
                    calcularImpuestoSalida()
                }
            }
            if (verificarCamposGuardar(arg)) {
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        var vlrUnitario = valorAtributo('txtValorU');
                        var iva = valorAtributo('cmbIva');
                        var lote = valorAtributo('txtLote');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = valorAtributo('txtPrecioVenta');
                        var fechaDeVencimiento = valorAtributo('txtFechaVencimiento');
                        break;
                    case "S":
                        var vlrUnitario = valorAtributo('lblValorUnitario');
                        var iva = valorAtributo('lblIva');
                        var lote = valorAtributo('cmbIdLote');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = 0;
                        var fechaDeVencimiento = valorAtributo('txtFV');
                        break;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=101&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(valorAtributo('lblNaturaleza'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);

                /* insertar relacion de  tr hg con doc nuevo */
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributo('lblIdTransaccionDev'));

                ajaxModificar();
            }
            break;
        case 'crearTransaccion':
            if (valorAtributo('lblValorImpuesto') == '') {
                if (valorAtributo('lblNaturaleza') == 'I') {
                    calcularImpuesto('lblValorImpuesto')
                } else {
                    calcularImpuestoSalida()
                }
            }
            if (verificarCamposGuardar(arg)) {
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        var vlrUnitario = valorAtributo('txtValorU');
                        var iva = valorAtributo('cmbIva');
                        var lote = valorAtributo('txtLote');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = valorAtributo('txtPrecioVenta');
                        var fechaDeVencimiento = valorAtributo('txtFechaVencimiento');
                        var medida = valorAtributo('cmbMedida');
                        var valorPromedio = 0;
                        break;
                    case "S":
                        var vlrUnitario = valorAtributo('lblValorUnitario');
                        var iva = valorAtributo('lblIva');
                        var lote = valorAtributo('cmbIdLote');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = 0;
                        var fechaDeVencimiento = valorAtributo('txtFV');
                        var valorPromedio = vlrUnitario;
                        if (valorAtributo('cmbIdBodega') == 1)
                            var medida = '';//valorAtributo('lblMedida');
                        else var medida = valorAtributo('lblMedida');
                        break;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=89&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(valorAtributo('lblNaturaleza'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);
                add_valores_a_mandar(valorPromedio);
                add_valores_a_mandar(medida);
                ajaxModificar();
            }
            break;
        case 'crearTransaccion_dos':
            if (valorAtributo('lblValorImpuesto') == '') {
                if (valorAtributo('lblNaturaleza') == 'I') {
                    calcularImpuesto('lblValorImpuesto')
                } else {
                    calcularImpuestoSalida()
                }
            }
            if (verificarCamposGuardar(arg)) {
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        var vlrUnitario = valorAtributo('txtValorU');
                        var iva = valorAtributo('cmbIva');
                        var lote = valorAtributo('txtLote');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = valorAtributo('txtPrecioVenta');
                        var fechaDeVencimiento = valorAtributo('txtFechaVencimiento');
                        var valorPromedio = 0;
                        var medida = valorAtributo('cmbMedida');
                        break;
                    case "S":
                        var id_articulo = valorAtributo('txtIdArticulo_dos');
                        var vlrUnitario = valorAtributo('txtValorUnitario_dos');
                        var iva = valorAtributo('lblIva');
                        var lote = valorAtributo('txtIdLote_dos');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = 0;
                        var fechaDeVencimiento = valorAtributo('txtFV_dos');
                        var valorPromedio = vlrUnitario;
                        var medida = valorAtributo('lblMedida');
                        break;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=89&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(id_articulo);
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(valorAtributo('lblNaturaleza'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);
                add_valores_a_mandar(valorPromedio);
                add_valores_a_mandar(medida);
                ajaxModificar();
            }
            break;
        case 'crearTransaccionSolicitud':
            if (valorAtributo('lblValorImpuesto') == '') {
                if (valorAtributo('lblNaturaleza') == 'I') {
                    calcularImpuesto('lblValorImpuesto')
                } else {
                    calcularImpuestoSalida()
                }
            }
            if (verificarCamposGuardar(arg)) {
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        var vlrUnitario = valorAtributo('txtValorU');
                        var lote = valorAtributo('txtLote');
                        var precioVenta = valorAtributo('txtPrecioVenta');
                        var fechaDeVencimiento = valorAtributo('txtFechaVencimiento');
                        var iva = valorAtributo('cmbIva');
                        break;
                    case "S":
                        var vlrUnitario = valorAtributo('lblValorUnitario');
                        var iva = valorAtributo('lblIva');
                        var lote = valorAtributo('cmbIdLote');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = 0;
                        var fechaDeVencimiento = valorAtributo('txtFV');
                        break;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=497&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(valorAtributo('lblNaturaleza'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);


                add_valores_a_mandar(valorAtributo('lblIdSolicitud'));

                ajaxModificar();
            }
            break;
        case 'crearTransaccionDevolucionCompra':
            if (valorAtributo('lblValorImpuesto') == '') {
                calcularImpuestoDevolucionCompra()
            }
            if (verificarCamposGuardar(arg)) {
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        var vlrUnitario = valorAtributo('txtValorU');
                        var iva = valorAtributo('cmbIva');
                        var lote = valorAtributo('txtLote');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = valorAtributo('txtPrecioVenta');
                        var fechaDeVencimiento = valorAtributo('txtFechaVencimiento');
                        var valorPromedio = 0;
                        var medida = valorAtributo('cmbMedida');
                        break;
                    case "S":
                        var vlrUnitario = valorAtributo('lblValorUnitario');
                        var iva = valorAtributo('lblIva');
                        var lote = valorAtributo('lblIdLote');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = 0;
                        var fechaDeVencimiento = valorAtributo('txtFV');
                        var valorPromedio = vlrUnitario;
                        var medida = valorAtributo('lblMedida');
                        break;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=89&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(valorAtributo('lblNaturaleza'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);
                add_valores_a_mandar(valorPromedio);
                add_valores_a_mandar(medida);
                ajaxModificar();
            }
            break;
        case 'crearTransaccionDevolucion':
            if (valorAtributo('lblValorImpuesto') == '') {
                calcularImpuestoDevolucion()
            }
            if (verificarCamposGuardar(arg)) {
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        var vlrUnitario = valorAtributo('lblValorUnitario');
                        var iva = valorAtributo('lblIva');
                        var lote = valorAtributo('lblIdLote');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = 0;
                        var fechaDeVencimiento = valorAtributo('txtFV');
                        break;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=502&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(valorAtributo('lblNaturaleza'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);


                add_valores_a_mandar(valorAtributo('lblIdDevolucion'));

                ajaxModificar();
            }
            break;
        case 'creaSerialesTransaccion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=581&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                ajaxModificar();
            }
            break;
        case 'modificaTransaccion':
            if (verificarCamposGuardar(arg)) {

                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        var vlrUnitario = valorAtributo('txtValorU');
                        var lote = valorAtributo('txtLote');
                        var precioVenta = valorAtributo('txtPrecioVenta');
                        var fechaDeVencimiento = valorAtributo('txtFechaVencimiento');
                        var iva = valorAtributo('cmbIva');
                        var serial = valorAtributo('txtSerial');
                        var medida = valorAtributo('cmbMedida');
                        break;
                    case "S":
                        var vlrUnitario = valorAtributo('lblValorUnitario');
                        var lote = valorAtributo('cmbIdLote');
                        var iva = valorAtributo('lblIva');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = 0;
                        var fechaDeVencimiento = valorAtributo('txtFV');
                        var medida = valorAtributo('lblMedida');
                        break;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=88&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);
                add_valores_a_mandar(medida);

                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                ajaxModificar();
            }
            break;



        case 'modificaTransaccionTraslado':
            if (verificarCamposGuardar(arg)) {

                var vlrUnitario = valorAtributo('lblValorUnitario');
                var lote = valorAtributo('cmbIdLote');
                var iva = valorAtributo('lblIva');
                var serial = valorAtributo('txtSerial');
                var precioVenta = 0;
                var fechaDeVencimiento = valorAtributo('txtFV');
                var medida = '';

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=88&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);
                add_valores_a_mandar(medida);

                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                ajaxModificar();
            }
            break;





        case 'modificaTransaccionOptica':
            if (verificarCamposGuardar(arg)) {

                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        var vlrUnitario = valorAtributo('txtValorU');
                        var lote = valorAtributo('txtLote');
                        var precioVenta = valorAtributo('txtPrecioVenta');
                        var fechaDeVencimiento = valorAtributo('txtFechaVencimiento');
                        var iva = valorAtributo('cmbIva');
                        var serial = valorAtributo('txtSerial');
                        var medida = valorAtributo('cmbMedida');
                        break;
                    case "S":
                        var vlrUnitario = valorAtributo('lblValorUnitario');
                        var lote = valorAtributo('cmbIdLote');
                        var iva = valorAtributo('lblIva');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = 0;
                        var fechaDeVencimiento = valorAtributo('txtFV');

                        var valor_unitario_ant = valorAtributo('txtVlrUnitarioAnterior');
                        var porcent_descuento = valorAtributo('cmbIdDescuento');
                        var medida = valorAtributo('lblMedida');
                        break;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=650&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);

                add_valores_a_mandar(valor_unitario_ant);
                add_valores_a_mandar(porcent_descuento);
                add_valores_a_mandar(medida);


                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                ajaxModificar();
            }
            break;
        case 'modificaTransaccionDevolucion':
            if (verificarCamposGuardar(arg)) {
                var vlrUnitario = valorAtributo('lblValorUnitario');
                /* var lote = valorAtributo('cmbIdLote');	 */
                var lote = valorAtributo('lblIdLote');
                var serial = valorAtributo('txtSerial');
                var fechaDeVencimiento = valorAtributo('txtFV');

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=500&parametros=";
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(valorAtributo('lblIva'));
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(fechaDeVencimiento);

                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                ajaxModificar();
            }
            break;
        case 'modificaTransaccionSolicitud':
            if (verificarCamposGuardar(arg)) {
                var vlrUnitario = valorAtributo('lblValorUnitario');
                var lote = valorAtributo('cmbIdLote');
                var serial = valorAtributo('txtSerial');
                var fechaDeVencimiento = valorAtributo('txtFV');

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=500&parametros=";
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(valorAtributo('lblIva'));
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(fechaDeVencimiento);

                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                ajaxModificar();
            }
            break;
        case 'crearDocumento':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=86&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdTipoDocumento'));
                add_valores_a_mandar(valorAtributo('txtObservacion'));
                add_valores_a_mandar(valorAtributo('cmbIdBodega'));

                add_valores_a_mandar(valorAtributo('txtNumero'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdTercero'));
                add_valores_a_mandar(valorAtributo('txtFechaDocumento'));
                add_valores_a_mandar(valorAtributo('txtIdUsuarioSesion'));


                ajaxModificar();
            }
            break;
        case 'modificaDocumento':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=85&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdTipoDocumento'));
                add_valores_a_mandar(valorAtributo('txtObservacion'));
                add_valores_a_mandar(valorAtributo('lblIdEstado'));
                add_valores_a_mandar(valorAtributo('cmbIdBodega'));
                add_valores_a_mandar(valorAtributo('txtNumero'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdTercero'));
                add_valores_a_mandar(valorAtributo('txtFechaDocumento'));
                add_valores_a_mandar(valorAtributo('txtValorFlete'));

                add_valores_a_mandar(valorAtributo('lblIdDoc'));

                ajaxModificar();
            }
            break;
        case 'modificaDocumentoFecha':
            //if(verificarCamposGuardar(arg)){
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=79&parametros=";
            add_valores_a_mandar(valorAtributo('txtObservacion'));
            add_valores_a_mandar(valorAtributo('txtFechaDocumento'));
            add_valores_a_mandar(valorAtributo('txtNumero'));
            add_valores_a_mandar(valorAtributo('lblIdDoc'));

            ajaxModificar();
            //}		
            break;
        case 'crearBodega':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=82&parametros=";
                add_valores_a_mandar(valorAtributo('txtDescripcion'));
                add_valores_a_mandar(valorAtributo('cmbIdEmpresa'));
                add_valores_a_mandar(valorAtributo('cmbIdCentroCosto'));
                add_valores_a_mandar(valorAtributo('cmbIdIdResponsable'));
                add_valores_a_mandar(valorAtributo('cmbEstado'));
                add_valores_a_mandar(valorAtributo('cmbSWRestitucion'));
                add_valores_a_mandar(valorAtributo('txtAutorizacion'));
                add_valores_a_mandar(valorAtributo('cmbSWRestriccionStock'));
                add_valores_a_mandar(valorAtributo('cmbIdBodegaTipo'));
                add_valores_a_mandar(valorAtributo('cmbSWConsignacion'));
                add_valores_a_mandar(valorAtributo('cmbSWAprovechamiento'));
                add_valores_a_mandar(valorAtributo('cmbSWAfectaCosto'));

                ajaxModificar();
            }
            break;
        case 'modificaBodega':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=81&parametros=";
                add_valores_a_mandar(valorAtributo('txtDescripcion'));
                add_valores_a_mandar(valorAtributo('cmbIdEmpresa'));
                add_valores_a_mandar(valorAtributo('cmbIdCentroCosto'));
                add_valores_a_mandar(valorAtributo('cmbIdIdResponsable'));
                add_valores_a_mandar(valorAtributo('cmbEstado'));
                add_valores_a_mandar(valorAtributo('cmbSWRestitucion'));
                add_valores_a_mandar(valorAtributo('txtAutorizacion'));
                add_valores_a_mandar(valorAtributo('cmbSWRestriccionStock'));
                add_valores_a_mandar(valorAtributo('cmbIdBodegaTipo'));
                add_valores_a_mandar(valorAtributo('cmbSWConsignacion'));
                add_valores_a_mandar(valorAtributo('cmbSWAprovechamiento'));
                add_valores_a_mandar(valorAtributo('cmbSWAfectaCosto'));


                add_valores_a_mandar(valorAtributo('cmbIdBodega'));

                ajaxModificar();
            }
            break;
        case 'modificarListHojaGasto':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=473&parametros=";
            add_valores_a_mandar(valorAtributo('cmbCantModificar'));
            add_valores_a_mandar(valorAtributo('lblIdHG'));

            ajaxModificar();
            break;

        case 'eliminarListHojaGasto':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=471&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdHG'));

            ajaxModificar();
            break;
        case 'eliminarMHHListado':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=469&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdMHH'));

            ajaxModificar();
            break;
        case 'traerArticulo':
            if (verificarCamposGuardar(arg)) {
                hoy = fecha_actual();
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=472&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('lblIdArticulo'));
                add_valores_a_mandar(valorAtributo('cmbCantConsumo'));
                add_valores_a_mandar(valorAtributo('txtConsumo'));
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));
                add_valores_a_mandar(valorAtributo('cmbNovedad'));
                add_valores_a_mandar(valorAtributo('lblCantNovedad'));
                //add_valores_a_mandar(valorAtributo('lblIdAdmisionAgen'));					 
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblCostoHG'));

                //
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));

                ajaxModificar();
            }
            break;
        case 'traerUltimoArticulo':
            if (verificarCamposGuardar(arg)) {
                hoy = fecha_actual();
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=517&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));	/*DOC ANT*/
                add_valores_a_mandar(valorAtributo('lblIdArticulo'));
                add_valores_a_mandar(valorAtributo('cmbCantConsumo'));
                add_valores_a_mandar(valorAtributo('txtConsumo'));
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));
                add_valores_a_mandar(valorAtributo('cmbNovedad'));
                add_valores_a_mandar(valorAtributo('lblCantNovedad'));
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblCostoHG'));
                //
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                //
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                /* CERRAR EL DOC DE SALIDA*/
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('cmbIdBodega')); /*idbodega*/
                add_valores_a_mandar(valorAtributo('cmbCantConsumo'));	/*cant transaccion*/
                add_valores_a_mandar(valorAtributo('cmbCantConsumo'));	/*cant transaccion*/
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));



                ajaxModificar();
            }
            break;
        case 'crearMHH':
            hoy = fecha_actual();
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=468&parametros=";
            if (valorAtributo('lblIdDocumento') != '') {
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('txtMSistolica'));
                add_valores_a_mandar(valorAtributo('txtMDiastolica'));
                add_valores_a_mandar(valorAtributo('txtFCardiaca'));
                add_valores_a_mandar(valorAtributo('txtFRespiracion'));
                add_valores_a_mandar(valorAtributo('txtTemperaturaMH'));
                add_valores_a_mandar(hoy + ' ' + valorAtributo('cmbHoraMHH') + ':' + valorAtributo('cmbMinutoMHH'));
                add_valores_a_mandar(valorAtributo('txtMHObservacion'));
                add_valores_a_mandar(LoginSesion());

                add_valores_a_mandar(0.0);
                add_valores_a_mandar(0);
                add_valores_a_mandar(0.0);
                add_valores_a_mandar(valorAtributo('txtSaturacionDeOxigeno'));

                ajaxModificar();
            } else
                alert('Selecciones un documento clinico');
            break;

        case 'crearHojaGasto':

            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=464&parametros=";
            if (valorAtributo('lblIdDocumento') != '') {
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticuloHG'));
                add_valores_a_mandar(valorAtributo('cmbCantidadHG'));
                add_valores_a_mandar(valorAtributo('txtIndicacionesHG'));
                add_valores_a_mandar(0);

                ajaxModificar();
            } else
                alert('Selecciones un documento clinico');
            break;
        case 'crearCanastaTransaccion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=477&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributo('lblCodigoCanasta'));
                ajaxModificar();
            }
            break;

        case 'crearCanastaTrasladoBodega':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=477&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributo('lblCodigoCanasta'));
                ajaxModificar();
            }
            break;
        case 'eliminarArticuloCanasta':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=462&parametros=";

                add_valores_a_mandar(valorAtributo('lblIdDetalle'));

                ajaxModificar();
            }
            break;
        case 'adicionarArticuloCanasta':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=461&parametros=";
                if (valorAtributo('lblCodigoCanasta') != '') {
                    if (verificarCamposGuardar(arg)) {
                        add_valores_a_mandar(valorAtributo('lblCodigoCanasta'));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                        add_valores_a_mandar(valorAtributo('cmbCantidad'));
                        ajaxModificar();

                    }
                } else
                    alert('Debe seleccionar una canasta !');

            }
            break;
        case 'modificarPlantillaCanasta':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=448&parametros=";

                add_valores_a_mandar(valorAtributo('txtNombreCanasta'));
                add_valores_a_mandar(valorAtributo('txtContenidoCanasta'));
                add_valores_a_mandar(valorAtributo('cmbTipoCanasta'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('lblCodigoCanasta'));


                ajaxModificar();
            }
            break;
        case 'crearPlantillaCanasta':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=447&parametros=";

                add_valores_a_mandar(valorAtributo('txtNombreCanasta'));
                add_valores_a_mandar(valorAtributo('txtContenidoCanasta'));
                add_valores_a_mandar(valorAtributo('cmbTipoCanasta'));

                ajaxModificar();
            }
            break;


        case 'eliminarAdmiMedicacion':

            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=445&parametros=";

            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('lblIdElemento'));


            ajaxModificar();

            break;
        case 'AdicionarAdmMedicamentos':

            hoy = fecha_actual();

            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=442&parametros=";
            if (valorAtributo('lblIdDocumento') != '') {


                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdMedicamento'));
                add_valores_a_mandar(valorAtributo('cmbIdViaMed'));
                add_valores_a_mandar(valorAtributo('cmbUnidadAdm'));
                add_valores_a_mandar(valorAtributo('cmbCantAdm'));
                add_valores_a_mandar(hoy + ' ' + valorAtributo('cmbHoraInicioM') + ':' + valorAtributo('cmbMinutoInicioM'));
                add_valores_a_mandar(valorAtributo('cmbSitioAdm'));
                add_valores_a_mandar(valorAtributo('txtIndicaciones'));

                ajaxModificar();
            } else
                alert('Seleccione un documento clinico')
            break;
        case 'modificarPlantilla':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=439&parametros=";

            add_valores_a_mandar(valorAtributo('txtNombre'));
            add_valores_a_mandar(valorAtributo('txtContenido'));
            add_valores_a_mandar(valorAtributo('cmbTipo'));
            add_valores_a_mandar(valorAtributo('lblCodigo'));

            ajaxModificar();
            break;
        case 'crearPlantilla':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=438&parametros=";
            //add_valores_a_mandar(valorAtributo('txtCodigo'));
            add_valores_a_mandar(valorAtributo('txtNombre'));
            add_valores_a_mandar(valorAtributo('txtContenido'));
            add_valores_a_mandar(valorAtributo('cmbTipo'));

            ajaxModificar();
            break;


        case 'crearPlantillaElemento':

            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=703&parametros=";
                add_valores_a_mandar(valorAtributo('lblCodigo'));
                add_valores_a_mandar(valorAtributo('lblCodigo'));
                add_valores_a_mandar(valorAtributo('txtDescripcion'));
                add_valores_a_mandar(valorAtributo('txtEtiqueta'));
                add_valores_a_mandar(valorAtributo('txtReferencia'));
                add_valores_a_mandar(valorAtributo('txtOrdenPadre'));

                ajaxModificar();
            }

            break;



            case 'modificarPlantillaElemento':

            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=707&parametros=";
                add_valores_a_mandar(valorAtributo('lblCodigo'));
                add_valores_a_mandar(valorAtributo('txtDescripcion'));
                add_valores_a_mandar(valorAtributo('txtEtiqueta'));
                add_valores_a_mandar(valorAtributo('txtReferencia'));
                add_valores_a_mandar(valorAtributo('txtOrdenPadre'));
                add_valores_a_mandar(valorAtributo('lblOrden'));
                add_valores_a_mandar(valorAtributo('lblExamen'));

                ajaxModificar();
            }

            break;

        case 'crearPlantillaEvolucion':

        if (verificarCamposGuardar(arg)) {
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=706&parametros=";
            add_valores_a_mandar(valorAtributo('lblOrden'));
            add_valores_a_mandar(valorAtributo('lblIdPlantilla'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('txtResultado'));
            add_valores_a_mandar(valorAtributo('txtObservacion'));

            ajaxModificar();
        }
            break;

            case 'modificarPlantillaEvolucion':
            if (verificarCamposGuardar(arg)) {
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=708&parametros=";
            add_valores_a_mandar(valorAtributo('txtResultado'));
            add_valores_a_mandar(valorAtributo('txtObservacion'));
            add_valores_a_mandar(valorAtributo('lblOrden'));
            add_valores_a_mandar(valorAtributo('lblIdPlantilla'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));           
            ajaxModificar();
            }
            break;

        case 'entregarEvento':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=70&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdCalificacion'));
            add_valores_a_mandar(valorAtributo('cmbIdPersonalRecibe'));
            add_valores_a_mandar(valorAtributo('txtObservacionRecibe'));
            add_valores_a_mandar(valorAtributo('txtFechaRecibe'));
            add_valores_a_mandar(4);

            add_valores_a_mandar(valorAtributo('txtIdEvento'));

            ajaxModificar();
            break;
        case 'aplazarEvento':
            if (IdSesion() == valorAtributo('cmbIdIdResponsable')) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=69&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdMotivoAplaza'));
                add_valores_a_mandar(valorAtributo('txtFechaAplazada'));
                add_valores_a_mandar(valorAtributo('txtObservacionAplaza'));
                add_valores_a_mandar(valorAtributo('txtIdEvento'));
                ajaxModificar();
            } else
                alert('SOLO EL RESPONSABLE PUEDE APLAZAR')
            break;
        case 'priorizarEvento':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=68&parametros=";

                add_valores_a_mandar(valorAtributo('cmbIdTipoEvento'));
                add_valores_a_mandar(valorAtributo('cmbIdIdProceso'));
                add_valores_a_mandar(valorAtributo('txtObservacion'));
                add_valores_a_mandar(valorAtributo('cmbIdIdResponsable'));
                add_valores_a_mandar(valorAtributo('txtPriorizacion'));
                add_valores_a_mandar(valorAtributo('txtNoDocumento'));
                add_valores_a_mandar(valorAtributo('txtDescDocumento'));
                add_valores_a_mandar(valorAtributo('txtFechaInicia'));
                add_valores_a_mandar(valorAtributo('txtFechaEntrega'));
                add_valores_a_mandar(2);

                add_valores_a_mandar(valorAtributo('txtIdEvento'));
                ajaxModificar();
            }
            break;
        case 'anuladoEvento':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=68&parametros=";

            add_valores_a_mandar(valorAtributo('cmbIdTipoEvento'));
            add_valores_a_mandar(valorAtributo('cmbIdIdProceso'));
            add_valores_a_mandar(valorAtributo('txtObservacion'));
            add_valores_a_mandar(valorAtributo('cmbIdIdResponsable'));
            add_valores_a_mandar(valorAtributo('txtPriorizacion'));
            add_valores_a_mandar(valorAtributo('txtNoDocumento'));
            add_valores_a_mandar(valorAtributo('txtDescDocumento'));
            add_valores_a_mandar(valorAtributo('txtFechaInicia'));
            add_valores_a_mandar(valorAtributo('txtFechaEntrega'));
            add_valores_a_mandar(5);

            add_valores_a_mandar(valorAtributo('txtIdEvento'));
            ajaxModificar();
            break;
        case 'listClasificarEA':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=552&parametros=";
            add_valores_a_mandar(valorAtributo('cmbBusClaseEA'));
            add_valores_a_mandar(valorAtributo('cmbTipoEA'));
            add_valores_a_mandar(valorAtributo('txtCronologia'));
            add_valores_a_mandar(valorAtributo('txtRecomendacion'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoServicio'));
            add_valores_a_mandar(valorAtributo('txtIdEvento'));

            ajaxModificar();
            break;
        case 'eliminarClasificarEA':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=554&parametros=";
            add_valores_a_mandar(valorAtributo('lblClasifiEA'));
            ajaxModificar();
            break;
        case 'listAnalisisEA':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=548&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdEvento'));
            add_valores_a_mandar(valorAtributo('txtAccionInsegura'));
            add_valores_a_mandar(valorAtributo('cmbFactContributivo'));

            ajaxModificar();
            break;
        case 'eliminaAnalisisEA':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=550&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAnalisisEA'));
            ajaxModificar();
            break;
        case 'adicionarTarea':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=528&parametros=";
            add_valores_a_mandar(valorAtributo('txtTarea'));
            add_valores_a_mandar(valorAtributo('txtFechaTarea'));
            add_valores_a_mandar(valorAtributo('cmbTareaResponsable'));
            add_valores_a_mandar(valorAtributo('txtCostoTarea'));
            add_valores_a_mandar(valorAtributo('cmbHoras'));
            add_valores_a_mandar(valorAtributo('txtIdEvento'));
            ajaxModificar();
            break;
        case 'guardarSeguimientoTarea':
            valores_a_mandar = 'accion=' + arg;
            if (verificarCamposGuardar('guardarSeguimientoTarea')) {
                valores_a_mandar = valores_a_mandar + "&idQuery=652&parametros=";
                add_valores_a_mandar(valorAtributo('cmbCumple'));
                add_valores_a_mandar(valorAtributo('txtSeguimiento'));
                add_valores_a_mandar(valorAtributo('lblIdTareaPlanAccion'));
                ajaxModificar();
            }
            break;
        case 'cambiarEstadoTarea':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=555&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdEstadoTareaEdit'));
            add_valores_a_mandar(valorAtributo('lblIdTareaPlanAccion'));
            ajaxModificar();
            break;
        case 'eliminarTarea':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=556&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdTareaPlanAccion'));
            ajaxModificar();
            break;
        case 'asociarPacienteEA':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=532&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('txtIdEvento'));
            ajaxModificar();
            break;
        case 'adicionarEspinaPescado':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=526&parametros=";
            add_valores_a_mandar(valorAtributo('cmbCalifica'));
            add_valores_a_mandar(valorAtributo('txtIdEvento'));
            add_valores_a_mandar(valorAtributo('cmbCriterioCausa'));
            add_valores_a_mandar(valorAtributo('txtAnalisisFactor'));
            ajaxModificar();
            break;
        case 'eliminarEspinaPescado':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=551&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdEspinaPezEA'));
            ajaxModificar();
            break;
        case 'crearEvento':

            if (valorAtributo('cmbIdTipoEvento') == '') {
                alert("DEBE SELECCIONAR TIPO DE EVENTO");
            } else if (valorAtributo('txtDescripcion') == '') {
                alert("DEBE ESCRIBIR EL EVENTO");
            } else {
                valores_a_mandar = 'accion=' + arg;
                switch (valorAtributo('cmbIdTipoEvento')) {
                    case '8': //SEGURIDAD DEL PACIENTE NECESITA IDENTIFICACION	
                        if (valorAtributo('txtIdentificacion') != '') {
                            valores_a_mandar = valores_a_mandar + "&idQuery=653&parametros=";
                            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                            add_valores_a_mandar(valorAtributo('txtDescripcion'));
                            add_valores_a_mandar(valorAtributo('cmbIdTipoEvento'));
                            add_valores_a_mandar(IdSesion());	//id estado					 
                            ajaxModificar();

                        } else {
                            alert('DEBE SELECCIONAR AL PACIENTE');
                            break;
                        }
                        break;
                    default:
                        valores_a_mandar = valores_a_mandar + "&idQuery=51&parametros=";
                        add_valores_a_mandar(valorAtributo('txtDescripcion'));
                        add_valores_a_mandar(valorAtributo('cmbIdTipoEvento'));

                        if (valorAtributo('lblIdEventoARelacio') != '')
                            add_valores_a_mandar(valorAtributo('lblIdEventoARelacio'));
                        else add_valores_a_mandar('0');
                        add_valores_a_mandar(IdSesion());	//id estado					 
                        ajaxModificar();
                        break;
                }

            }
            break;

        case 'crearEventoEnHc':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=612&parametros=";
            add_valores_a_mandar(valorAtributo('txtDescripcion'));
            add_valores_a_mandar(10);//tipo evento  10= trabajos
            add_valores_a_mandar(1);//id proceso
            add_valores_a_mandar(1);	//id estado	
            add_valores_a_mandar(IdSesion());	//id estado	
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
            ajaxModificar();
            break;

        case 'relacionarEvento':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=680&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdEvento'));
            add_valores_a_mandar(valorAtributo('txtIdEventoARelacionar'));
            ajaxModificar();
            break;

        case 'eliminarProcedimientoCuenta':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=428&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdTransac'));
                ajaxModificar();
            }
            break;


        case 'traerTarifarioPlanContratacion':
            if (confirm('RECUERDE QUE LOS PROCEDIMIENTOS DE ESTE PLAN \nSE ELIMINIRAN Y SE VOLVERAN A INGRESAR.\n� DESEA CONTINUAR ?')) {

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=415&parametros=";

                //MODIFICAR EL CAMPO ID TARIFARIO  BASE  Y PORCENTAJE //HACER EL TRIGGER DE AUDITORIA
                add_valores_a_mandar(valorAtributo('cmbIdTarifario'));
                add_valores_a_mandar(valorAtributo('txtPorcentajeIncremento'));
                add_valores_a_mandar(valorAtributo('txtIdPlan'));

                //add_valores_a_mandar(valorAtributo('txtIdPlan'));

                //INSERTAR EL PLAN DETALLE
                add_valores_a_mandar(valorAtributo('txtIdPlan'));
                add_valores_a_mandar(valorAtributo('txtPorcentajeIncremento'));
                add_valores_a_mandar(valorAtributo('cmbIdTarifario'));
                // add_valores_a_mandar(valorAtributo('cmbAdmiteRedondeo'));

                ajaxModificar();
            }
            /*  */
            break;

        case 'crearArticulo':
            switch (valorAtributo('cmbIdClase')) {
                case 'MD':
                    if (verificarCamposGuardar('crearMedicamento')) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=49&parametros=";
                        add_valores_a_mandar(valorAtributo('txtNombreArticulo'));
                        add_valores_a_mandar(valorAtributo('txtNombreComercial'));
                        add_valores_a_mandar(valorAtributo('cmbIdClase'));
                        add_valores_a_mandar(valorAtributo('cmbIdGrupo'));
                        add_valores_a_mandar(valorAtributo('cmbPresentacion'));
                        add_valores_a_mandar(valorAtributo('txtConcentracion'));
                        add_valores_a_mandar(valorAtributo('txtCodArticulo'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionCompleta'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionAbreviada'));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdFabricante'));
                        add_valores_a_mandar(valorAtributo('cmbIdUnidad'));
                        add_valores_a_mandar(valorAtributo('txtValorUnidad'));
                        add_valores_a_mandar(valorAtributo('cmbML'));	//			 
                        add_valores_a_mandar(valorAtributo('txtIVA'));
                        add_valores_a_mandar(valorAtributo('txtIVAVenta'));
                        add_valores_a_mandar(valorAtributo('txtInvima'));
                        add_valores_a_mandar(valorAtributo('txtVigInvima'));
                        add_valores_a_mandar(valorAtributo('txtATC'));
                        add_valores_a_mandar(valorAtributo('cmbIdFechaVencimiento'));
                        add_valores_a_mandar(valorAtributo('cmbIdProveedorUnico'));
                        add_valores_a_mandar(valorAtributo('cmbPOS'));
                        add_valores_a_mandar(valorAtributo('cmbVigente'));
                        add_valores_a_mandar(valorAtributo('cmbIdClaseRiesgo'));
                        add_valores_a_mandar(valorAtributo('cmbIdVidaUtil'));
                        add_valores_a_mandar(valorAtributo('txtMarca'));
                        add_valores_a_mandar(valorAtributo('txtCodAtc')); /**/

                        /* MEDICAMENTO */
                        add_valores_a_mandar(valorAtributo('cmbAnatomofarmacologico'));
                        add_valores_a_mandar(valorAtributo('cmbPrincipioActivo'));
                        add_valores_a_mandar(valorAtributo('cmbFormaFarmacologica'));
                        add_valores_a_mandar(valorAtributo('txtFactorConv'));
                        add_valores_a_mandar(valorAtributo('cmbIdUnidadAdministracion'));
                        // alert(valores_a_mandar)									

                        /*FORMA FARMACEUTICA*/
                        add_valores_a_mandar(valorAtributo('cmbIdUnidadAdministracion'));

                        ajaxModificar();

                    }
                    break;
                case 'DM':
                    if (verificarCamposGuardar(arg)) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=496&parametros=";
                        add_valores_a_mandar(valorAtributo('txtNombreArticulo'));
                        add_valores_a_mandar(valorAtributo('txtNombreComercial'));
                        add_valores_a_mandar(valorAtributo('cmbIdClase'));
                        add_valores_a_mandar(valorAtributo('cmbIdGrupo'));
                        add_valores_a_mandar(valorAtributo('cmbPresentacion'));
                        add_valores_a_mandar(valorAtributo('txtConcentracion'));
                        add_valores_a_mandar(valorAtributo('txtCodArticulo'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionCompleta'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionAbreviada'));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdFabricante'));
                        add_valores_a_mandar(valorAtributo('cmbIdUnidad'));
                        add_valores_a_mandar(valorAtributo('txtValorUnidad'));
                        add_valores_a_mandar(valorAtributo('cmbML'));
                        add_valores_a_mandar(valorAtributo('txtIVA'));
                        add_valores_a_mandar(valorAtributo('txtIVAVenta'));
                        add_valores_a_mandar(valorAtributo('txtInvima'));
                        add_valores_a_mandar(valorAtributo('txtVigInvima'));
                        add_valores_a_mandar(valorAtributo('txtATC'));
                        add_valores_a_mandar(valorAtributo('cmbIdFechaVencimiento'));
                        add_valores_a_mandar(valorAtributo('cmbIdProveedorUnico'));
                        add_valores_a_mandar(valorAtributo('cmbPOS'));
                        add_valores_a_mandar(valorAtributo('cmbVigente'));
                        add_valores_a_mandar(valorAtributo('cmbIdClaseRiesgo'));
                        add_valores_a_mandar(valorAtributo('cmbIdVidaUtil'));
                        add_valores_a_mandar(valorAtributo('txtMarca'));
                        add_valores_a_mandar(valorAtributo('txtCodAtc')); /**/

                        /* MEDICAMENTO */
                        add_valores_a_mandar(valorAtributo('txtFactorConv'));
                        add_valores_a_mandar(valorAtributo('cmbIdUnidadAdministracion'));

                        /*FORMA FARMACEUTICA*/
                        add_valores_a_mandar(valorAtributo('cmbIdUnidadAdministracion'));

                        ajaxModificar();
                    }
                    break;
                default:
                    if (verificarCamposGuardar(arg)) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=47&parametros=";
                        add_valores_a_mandar(valorAtributo('txtNombreArticulo'));
                        add_valores_a_mandar(valorAtributo('txtNombreComercial'));
                        add_valores_a_mandar(valorAtributo('cmbIdClase'));
                        add_valores_a_mandar(valorAtributo('cmbIdGrupo'));
                        add_valores_a_mandar(valorAtributo('cmbPresentacion'));
                        add_valores_a_mandar(valorAtributo('txtConcentracion'));
                        add_valores_a_mandar(valorAtributo('txtCodArticulo'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionCompleta'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionAbreviada'));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdFabricante'));
                        add_valores_a_mandar(valorAtributo('cmbIdUnidad'));
                        add_valores_a_mandar(valorAtributo('txtValorUnidad'));
                        add_valores_a_mandar(valorAtributo('cmbML'));
                        add_valores_a_mandar(valorAtributo('txtIVA'));
                        add_valores_a_mandar(valorAtributo('txtIVAVenta'));
                        add_valores_a_mandar(valorAtributo('txtInvima'));
                        add_valores_a_mandar(valorAtributo('txtVigInvima'));
                        add_valores_a_mandar(valorAtributo('cmbIdFechaVencimiento'));
                        add_valores_a_mandar(valorAtributo('cmbIdProveedorUnico'));
                        add_valores_a_mandar(valorAtributo('cmbPOS'));
                        add_valores_a_mandar(valorAtributo('cmbVigente'));
                        add_valores_a_mandar(valorAtributo('cmbIdClaseRiesgo'));
                        add_valores_a_mandar(valorAtributo('cmbIdVidaUtil'));
                        add_valores_a_mandar(valorAtributo('txtMarca'));


                        /*FORMA FARMACEUTICA*/
                        add_valores_a_mandar(valorAtributo('cmbIdUnidadAdministracion'));
                        ajaxModificar();
                    }
                    break;
            }
            break;
        case 'modificaArticulo':
            switch (valorAtributo('cmbIdClase')) {
                case 'MD':

                    if (verificarCamposGuardar('modificaMedicamento')) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=48&parametros=";
                        add_valores_a_mandar(valorAtributo('txtNombreArticulo'));
                        add_valores_a_mandar(valorAtributo('txtNombreComercial'));
                        add_valores_a_mandar(valorAtributo('cmbIdClase'));
                        add_valores_a_mandar(valorAtributo('cmbIdGrupo'));
                        //add_valores_a_mandar(valorAtributo('cmbIdTipo'));	
                        //add_valores_a_mandar(valorAtributo('cmbIdSubTipo'));	

                        add_valores_a_mandar(valorAtributo('cmbPresentacion'));
                        add_valores_a_mandar(valorAtributo('txtConcentracion'));

                        add_valores_a_mandar(valorAtributo('txtCodArticulo'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionCompleta'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionAbreviada'));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdFabricante'));
                        add_valores_a_mandar(valorAtributo('cmbIdUnidad'));
                        add_valores_a_mandar(valorAtributo('txtValorUnidad'));
                        add_valores_a_mandar(valorAtributo('cmbML'));
                        add_valores_a_mandar(valorAtributo('txtIVA'));
                        add_valores_a_mandar(valorAtributo('txtIVAVenta'));
                        add_valores_a_mandar(valorAtributo('txtInvima'));
                        add_valores_a_mandar(valorAtributo('txtVigInvima'));
                        add_valores_a_mandar(valorAtributo('txtATC'));
                        add_valores_a_mandar(valorAtributo('cmbIdFechaVencimiento'));
                        add_valores_a_mandar(valorAtributo('cmbIdProveedorUnico'));
                        add_valores_a_mandar(valorAtributo('cmbPOS'));
                        add_valores_a_mandar(valorAtributo('cmbVigente'));
                        add_valores_a_mandar(valorAtributo('cmbIdClaseRiesgo'));
                        add_valores_a_mandar(valorAtributo('cmbIdVidaUtil'));
                        add_valores_a_mandar(valorAtributo('txtMarca'));
                        add_valores_a_mandar(valorAtributo('txtCodAtc'));  /**/

                        add_valores_a_mandar(valorAtributo('txtIdArticulo'));


                        /*MODIFICAR MEDICAMENTO */

                        add_valores_a_mandar(valorAtributo('cmbAnatomofarmacologico'));
                        add_valores_a_mandar(valorAtributo('cmbPrincipioActivo'));
                        add_valores_a_mandar(valorAtributo('cmbFormaFarmacologica'));
                        add_valores_a_mandar(valorAtributo('txtFactorConv'));
                        add_valores_a_mandar(valorAtributo('cmbIdUnidadAdministracion'));

                        add_valores_a_mandar(valorAtributo('txtIdArticulo'));

                        ajaxModificar();
                    }
                    break;
                case 'DM':
                    if (verificarCamposGuardar(arg)) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=498&parametros=";
                        add_valores_a_mandar(valorAtributo('txtNombreArticulo'));
                        add_valores_a_mandar(valorAtributo('txtNombreComercial'));
                        add_valores_a_mandar(valorAtributo('cmbIdClase'));
                        add_valores_a_mandar(valorAtributo('cmbIdGrupo'));
                        //add_valores_a_mandar(valorAtributo('cmbIdTipo'));	
                        //add_valores_a_mandar(valorAtributo('cmbIdSubTipo'));	

                        add_valores_a_mandar(valorAtributo('cmbPresentacion'));
                        add_valores_a_mandar(valorAtributo('txtConcentracion'));

                        add_valores_a_mandar(valorAtributo('txtCodArticulo'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionCompleta'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionAbreviada'));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdFabricante'));
                        add_valores_a_mandar(valorAtributo('cmbIdUnidad'));
                        add_valores_a_mandar(valorAtributo('txtValorUnidad'));
                        add_valores_a_mandar(valorAtributo('cmbML'));
                        add_valores_a_mandar(valorAtributo('txtIVA'));
                        add_valores_a_mandar(valorAtributo('txtIVAVenta'));
                        add_valores_a_mandar(valorAtributo('txtInvima'));
                        add_valores_a_mandar(valorAtributo('txtVigInvima'));
                        add_valores_a_mandar(valorAtributo('txtATC'));
                        add_valores_a_mandar(valorAtributo('cmbIdFechaVencimiento'));
                        add_valores_a_mandar(valorAtributo('cmbIdProveedorUnico'));
                        add_valores_a_mandar(valorAtributo('cmbPOS'));
                        add_valores_a_mandar(valorAtributo('cmbVigente'));
                        add_valores_a_mandar(valorAtributo('cmbIdClaseRiesgo'));
                        add_valores_a_mandar(valorAtributo('cmbIdVidaUtil'));
                        add_valores_a_mandar(valorAtributo('txtMarca'));
                        add_valores_a_mandar(valorAtributo('txtCodAtc'));  /**/

                        add_valores_a_mandar(valorAtributo('txtIdArticulo'));


                        /*MODIFICAR MEDICAMENTO */

                        //add_valores_a_mandar(valorAtributo('cmbAnatomofarmacologico'));
                        //add_valores_a_mandar(valorAtributo('cmbPrincipioActivo'));
                        //add_valores_a_mandar(valorAtributo('cmbFormaFarmacologica'));					
                        add_valores_a_mandar(valorAtributo('txtFactorConv'));
                        add_valores_a_mandar(valorAtributo('cmbIdUnidadAdministracion'));

                        add_valores_a_mandar(valorAtributo('txtIdArticulo'));

                        ajaxModificar();
                    }
                    break;
                default:
                    if (verificarCamposGuardar(arg)) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=45&parametros=";
                        add_valores_a_mandar(valorAtributo('txtNombreArticulo'));
                        add_valores_a_mandar(valorAtributo('txtNombreComercial'));
                        add_valores_a_mandar(valorAtributo('cmbIdClase'));
                        add_valores_a_mandar(valorAtributo('cmbIdGrupo'));
                        //add_valores_a_mandar(valorAtributo('cmbIdTipo'));	
                        //add_valores_a_mandar(valorAtributo('cmbIdSubTipo'));	

                        add_valores_a_mandar(valorAtributo('cmbPresentacion'));
                        add_valores_a_mandar(valorAtributo('txtConcentracion'));

                        add_valores_a_mandar(valorAtributo('txtCodArticulo'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionCompleta'));
                        add_valores_a_mandar(valorAtributo('txtDescripcionAbreviada'));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdFabricante'));
                        add_valores_a_mandar(valorAtributo('cmbIdUnidad'));
                        add_valores_a_mandar(valorAtributo('txtValorUnidad'));
                        add_valores_a_mandar(valorAtributo('cmbML'));
                        add_valores_a_mandar(valorAtributo('txtIVA'));
                        add_valores_a_mandar(valorAtributo('txtIVAVenta'));
                        add_valores_a_mandar(valorAtributo('txtInvima'));
                        add_valores_a_mandar(valorAtributo('txtVigInvima'));

                        //add_valores_a_mandar(valorAtributo('txtATC'));	

                        add_valores_a_mandar(valorAtributo('cmbIdFechaVencimiento'));
                        add_valores_a_mandar(valorAtributo('cmbIdProveedorUnico'));
                        add_valores_a_mandar(valorAtributo('cmbPOS'));
                        add_valores_a_mandar(valorAtributo('cmbVigente'));
                        add_valores_a_mandar(valorAtributo('cmbIdClaseRiesgo'));
                        add_valores_a_mandar(valorAtributo('cmbIdVidaUtil'));
                        add_valores_a_mandar(valorAtributo('txtMarca'));

                        add_valores_a_mandar(valorAtributo('txtIdArticulo'));

                        ajaxModificar();
                    }
                    break;
            }
            break;
        case 'EliminarPlanContratacion':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=417&parametros=";

            add_valores_a_mandar(valorAtributo('txtIdPlan'));

            ajaxModificar();
            break;

            case 'crearArticuloTemporal':
                    if (verificarCamposGuardar(arg)) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=713&parametros=";
                        add_valores_a_mandar(valorAtributo('txtNombreComercial'));

                        var articulo = valorAtributo('txtNombreArticulo');
                        articulo = articulo.split('-')[1];
                        articulo = articulo.split('%20%20%3A%3A%20%20')[0].trim();

                        add_valores_a_mandar(articulo);
                        add_valores_a_mandar(valorAtributo('cmbIdClase'));
                        add_valores_a_mandar(valorAtributo('cmbPresentacion'));
                        add_valores_a_mandar(valorAtributo('txtConcentracion'));
                        add_valores_a_mandar(valorAtributo('cmbPOS'));
                        add_valores_a_mandar(valorAtributo('txtATC'));
                        add_valores_a_mandar(valorAtributo('txtCodAtc'));

                        var via = '';

                        var ids = jQuery("#listGrillaViaArticulo").getDataIDs();    

                        for(var i=0;i<ids.length;i++){ 
                                  var c = ids[i];
                                  via +=c+',';}

                        add_valores_a_mandar(via);   
                        add_valores_a_mandar(valorAtributo('cmbFormaFarmacologica'));

                        ajaxModificar();

                    }
            break;




        case 'modificaPlanContratacion':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=414&parametros=";

            add_valores_a_mandar(valorAtributo('txtDescPlan'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdTercero'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoRegimen'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoCliente'));
            add_valores_a_mandar(valorAtributo('txtNumeroContrato'));
            add_valores_a_mandar(valorAtributo('txtFechaInicio'));
            add_valores_a_mandar(valorAtributo('txtFechaFinal'));
            add_valores_a_mandar(valorAtributo('txtMontoContrato'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoPlan'));
            add_valores_a_mandar(valorAtributo('cmbIdTarifario'));
            add_valores_a_mandar(valorAtributo('txtPorcentajeIncremento'));
            add_valores_a_mandar(valorAtributo('cmbAdmiteRedondeo'));
            add_valores_a_mandar(valorAtributo('txtObservacion'));
            add_valores_a_mandar(valorAtributo('cmbSolicitaAutorizacionAdmision'));
            add_valores_a_mandar(valorAtributo('cmbIdEstadoEdit'));

            add_valores_a_mandar(valorAtributo('txtIdPlan'));

            ajaxModificar();
            break;
        case 'crearPlanContratacion':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=416&parametros=";

            add_valores_a_mandar(valorAtributo('txtIdPlan'));
            add_valores_a_mandar(valorAtributo('txtDescPlan'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdTercero'));


            add_valores_a_mandar(valorAtributo('cmbIdTipoRegimen'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoCliente'));
            add_valores_a_mandar(valorAtributo('txtNumeroContrato'));
            add_valores_a_mandar(valorAtributo('txtFechaInicio'));
            add_valores_a_mandar(valorAtributo('txtFechaFinal'));
            add_valores_a_mandar(valorAtributo('txtMontoContrato'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoPlan'));
            add_valores_a_mandar(valorAtributo('cmbIdTarifario'));
            add_valores_a_mandar(valorAtributo('txtPorcentajeIncremento'));
            add_valores_a_mandar(valorAtributo('cmbAdmiteRedondeo'));
            add_valores_a_mandar(valorAtributo('txtObservacion'));
            add_valores_a_mandar(valorAtributo('cmbSolicitaAutorizacionAdmision'));
            add_valores_a_mandar(valorAtributo('cmbIdEstadoEdit'));

            ajaxModificar();
            break;

        case 'adicionarRangoPlan':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=411&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdPlan'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoAfiliado'));
            add_valores_a_mandar(valorAtributo('cmbIdRango'));
            add_valores_a_mandar(valorAtributo('txtCopago'));
            add_valores_a_mandar(valorAtributo('txtCuotaModeradora'));
            ajaxModificar();
            break;
        case 'eliminaRangoPlan':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=412&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdPlan'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoAfiliado'));
            add_valores_a_mandar(valorAtributo('cmbIdRango'));
            ajaxModificar();
            break;
        case 'modificaRangoPlan':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=413&parametros=";
            add_valores_a_mandar(valorAtributo('txtCopago'));
            add_valores_a_mandar(valorAtributo('txtCuotaModeradora'));
            add_valores_a_mandar(valorAtributo('txtIdPlan'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoAfiliado'));
            add_valores_a_mandar(valorAtributo('cmbIdRango'));
            ajaxModificar();
            break;
        case 'adicionarMedicamentoPlan':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=587&parametros=";
                add_valores_a_mandar(valorAtributo('txtIdPlan'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txtIdPrecioArticulo'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoPlan'));
                ajaxModificar();
            }
            break;
        case 'modificarMedicamentoPlan':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=591&parametros=";
                add_valores_a_mandar(valorAtributo('txtIdPrecioArticulo'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoPlan'));
                add_valores_a_mandar(valorAtributo('txtIdPlan'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                ajaxModificar();
            }
            break;
        case 'eliminarMedicamentoPlan':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=589&parametros=";
                add_valores_a_mandar(valorAtributo('txtIdPlan'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                ajaxModificar();
            }
            break;
        case 'adicionarProcedimientoPlan':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=409&parametros=";
                add_valores_a_mandar(valorAtributo('txtIdPlan'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));
                add_valores_a_mandar(valorAtributo('txtPrecioProcedimiento'));
                add_valores_a_mandar(valorAtributo('cmbTarifarioBase'));
                add_valores_a_mandar(valorAtributo('txtPrecioTarifarioBase'));
                add_valores_a_mandar(valorAtributo('txtPorcentDescuento'));
                add_valores_a_mandar(valorAtributo('txtCodigoExterno'));
                ajaxModificar();
            }
            break;
        case 'modificarProcedimientoPlan':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=430&parametros=";
                add_valores_a_mandar(valorAtributo('txtPrecioProcedimiento'));
                add_valores_a_mandar(valorAtributo('cmbTarifarioBase'));
                add_valores_a_mandar(valorAtributo('txtPrecioTarifarioBase'));
                add_valores_a_mandar(valorAtributo('txtPorcentDescuento'));
                add_valores_a_mandar(valorAtributo('txtCodigoExterno'));
                add_valores_a_mandar(valorAtributo('txtIdPlan'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));
                ajaxModificar();
            }
            break;
        case 'eliminarProcedimientoPlan':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=419&parametros=";

            add_valores_a_mandar(valorAtributo('txtIdPlan'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));
            ajaxModificar();
            break;
        case 'eliminaEquivalenciaCargoTarifario':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=405&parametros=";

            add_valores_a_mandar(valorAtributo('lblCargo'));
            add_valores_a_mandar(valorAtributo('lblIdEquivalenciaCups'));
            ajaxModificar();
            break;
        case 'eliminaCargoTarifario':

            if (confirm('ESTA SEGURO DE ELIMINAR EL CARGO:  ' + valorAtributo('txtDescCargoTarifario') + ' ?')) {
                $("#cmbIdRepeticionProgramada option[value='1']").attr('selected', 'selected');

                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=404&parametros=";

                    add_valores_a_mandar(valorAtributo('lblCargo'));
                    add_valores_a_mandar(valorAtributo('lblIdTarifario'));

                    add_valores_a_mandar(valorAtributo('lblCargo'));
                    add_valores_a_mandar(valorAtributo('lblIdTarifario'));

                    ajaxModificar();
                }
            }
            break;
        case 'modificaCargoTarifario':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=403&parametros=";

                add_valores_a_mandar(valorAtributo('txtDescCargoTarifario'));
                add_valores_a_mandar(valorAtributo('cmbIdGrupoTarifario'));
                add_valores_a_mandar(valorAtributo('cmbIdSubGrupoTarifario'));

                add_valores_a_mandar(valorAtributo('cmbIdGrupoTipoCargo'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoCargo'));
                add_valores_a_mandar(valorAtributo('cmbIdConceptoRips'));
                add_valores_a_mandar(valorAtributo('cmbIdNivelAtencion'));
                add_valores_a_mandar(valorAtributo('txtUnidadPrecio'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoUnidad'));
                add_valores_a_mandar(valorAtributo('txtGravamen'));
                add_valores_a_mandar(valorAtributo('cmbHonorarios'));
                add_valores_a_mandar(valorAtributo('cmbExigeCantidad'));

                add_valores_a_mandar(valorAtributo('cmbVigente'));

                add_valores_a_mandar(valorAtributo('lblCargo'));
                add_valores_a_mandar(valorAtributo('lblIdTarifario'));

                ajaxModificar();
            }
            break;
        case 'crearCargoTarifario':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=402&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdTarifario'));
                add_valores_a_mandar(valorAtributo('txtIdCargoNuevo'));
                add_valores_a_mandar(valorAtributo('txtDescCargoTarifario'));
                add_valores_a_mandar(valorAtributo('cmbIdGrupoTarifario'));
                add_valores_a_mandar(valorAtributo('cmbIdSubGrupoTarifario'));

                add_valores_a_mandar(valorAtributo('cmbIdGrupoTipoCargo'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoCargo'));
                add_valores_a_mandar(valorAtributo('cmbIdConceptoRips'));
                add_valores_a_mandar(valorAtributo('cmbIdNivelAtencion'));
                add_valores_a_mandar(valorAtributo('txtUnidadPrecio'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoUnidad'));
                add_valores_a_mandar(valorAtributo('txtGravamen'));
                add_valores_a_mandar(valorAtributo('cmbHonorarios'));
                add_valores_a_mandar(valorAtributo('cmbExigeCantidad'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                ajaxModificar();
            }
            break;
        case 'listGrillaTarifarioEquivalencia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=401&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdTarifario'));
                add_valores_a_mandar(valorAtributo('lblCargo'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdEquivalenciaCups'));
                ajaxModificar();
            }
            break;
        case 'modificarAutorizacionArchivo':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=33&parametros=";
            add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
            add_valores_a_mandar(valorAtributo('txtFechaVigencia'));
            add_valores_a_mandar(LoginSesion());
            add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
            ajaxModificar();
            break;
        case 'modificarAutorizacion':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=33&parametros=";
            add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
            add_valores_a_mandar(valorAtributo('txtFechaVigencia'));
            add_valores_a_mandar(LoginSesion());
            add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
            ajaxModificar();
            break;

        case 'activarCIE':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=379&parametros=";
            add_valores_a_mandar(valorAtributo('cmbVigente'));
            add_valores_a_mandar(valorAtributo('txtIndicacion'));
            add_valores_a_mandar(valorAtributo('lblId'));
            ajaxModificar();
            break;
        case 'editarPlantillaProcedimientoNoPos':
            if (valorAtributo('lblId') != '') {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=380&parametros=";
                add_valores_a_mandar(valorAtributo('txtResumenHC'));
                add_valores_a_mandar(valorAtributo('txtAlternativaPos'));
                add_valores_a_mandar(valorAtributo('txtPosibilidadTerapeutica'));
                add_valores_a_mandar(valorAtributo('txtMotivoUso'));
                add_valores_a_mandar(valorAtributo('txtDx'));
                add_valores_a_mandar(valorAtributo('txtJustificacion'));
                add_valores_a_mandar(valorAtributo('lblId'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('lblId'));

                ajaxModificar();
            }
            break;
        case 'editarPlantillaMedicamentoNoPos':
            if (valorAtributo('lblId') != '') {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=396&parametros=";
                add_valores_a_mandar(valorAtributo('txtResumenEnf'));
                add_valores_a_mandar(valorAtributo('txtUtilizado'));
                add_valores_a_mandar(valorAtributo('txtPresentacion'));
                add_valores_a_mandar(valorAtributo('txtDosis'));
                add_valores_a_mandar(valorAtributo('txtIndicacion'));
                add_valores_a_mandar(valorAtributo('txtExplique'));
                add_valores_a_mandar(valorAtributo('lblId'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('lblId'));
                ajaxModificar();
            }
            break;
        case 'editarAnexoPDF1':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=40&parametros=";
            add_valores_a_mandar(valorAtributo('txt_resumen_enf'));
            add_valores_a_mandar(valorAtributo('lblIdEvolucion'));
            add_valores_a_mandar(valorAtributo('lblIdElemento'));
            ajaxModificar();
            break;
        case 'editarAnexoPDF2':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=41&parametros=";
            add_valores_a_mandar(valorAtributo('txt_indicacion_terapeutica'));
            add_valores_a_mandar(valorAtributo('lblIdEvolucion'));
            add_valores_a_mandar(valorAtributo('lblIdElemento'));
            ajaxModificar();
            break;
        case 'editarAnexoPDF3':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=42&parametros=";
            add_valores_a_mandar(valorAtributo('txt_explicaciones'));
            add_valores_a_mandar(valorAtributo('lblIdEvolucion'));
            add_valores_a_mandar(valorAtributo('lblIdElemento'));
            ajaxModificar();
            break;

        case 'listAnestesia': 		/*cirugia*/
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=62&parametros=";
                add_valores_a_mandar(valorAtributo('cmbAnestesiasGrupo'));
                add_valores_a_mandar(valorAtributo('txtHoraAnestesia') + ':' + valorAtributo('txtMinAnestesia') + ' ' + valorAtributo('cmbAMPM'));
                add_valores_a_mandar(valorAtributo('txtAnesteciaObservaciones'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                ajaxModificar();
            }
            break;
        case 'procedimientosGestionEdit':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=3&parametros=";
                add_valores_a_mandar(valorAtributoCombo('cmbEstadoEditProc') + ' ' + valorAtributo('txt_observacionGestion') + ' ' + LoginSesion());
                add_valores_a_mandar(valorAtributo('cmbEstadoEditProc'));

                add_valores_a_mandar(valorAtributo('lblId'));
                ajaxModificar();
            }
            break;
        case 'procedimientosGestionEdit_2':   // solo para integracion en folios de postconsulta
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=3&parametros=";
                add_valores_a_mandar(valorAtributoCombo('cmbEstadoEditProc') + ' ' + valorAtributo('txt_observacionGestion') + ' ' + LoginSesion());
                add_valores_a_mandar(valorAtributo('cmbEstadoEditProc'));

                add_valores_a_mandar(valorAtributo('lblIdProcedimientoAutoriza'));
                ajaxModificar();
            }
            break;
        case 'procedimientosGestionEditListado': /*  */
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=632&parametros=";
                add_valores_a_mandar(valorAtributoCombo('cmbEstadoEditProc') + ' ' + valorAtributo('txt_observacionGestion') + ' ' + LoginSesion());
                add_valores_a_mandar(valorAtributo('cmbEstadoEditProc'));

                add_valores_a_mandar(valorAtributo('lblId'));

                /* sacar listado de procedimientos  */
                sacarValoresColumnasDeGrilla('listCitaCirugiaProcedimientoLEGestion');
                add_valores_a_mandar(valorAtributo('txtColumnaIdSitioQuirur'));
                add_valores_a_mandar(valorAtributo('txtColumnaIdProced'));
                add_valores_a_mandar(valorAtributo('txtColumnaObservacion'));

                ajaxModificar();
            }
            break;
        case 'procedimientoAdd':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=44&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));
            add_valores_a_mandar(valorAtributo('txtNombre'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoRips'));
            add_valores_a_mandar(valorAtributo('cmbIdCentroCosto'));
            add_valores_a_mandar(valorAtributo('cmbNivel'));
            add_valores_a_mandar(valorAtributo('txtCups'));
            add_valores_a_mandar(valorAtributo('txtCodContable'));
            add_valores_a_mandar(valorAtributo('txtUsuarioElaboro'));
            add_valores_a_mandar(valorAtributo('cmbClase'));
            add_valores_a_mandar(valorAtributo('cmbTipoServicio'));
            add_valores_a_mandar(valorAtributo('cmbTipo'));
            add_valores_a_mandar(valorAtributo('cmbVigente'));

            ajaxModificar();
            break;
        case 'procedimientosEdit':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=24&parametros=";
            add_valores_a_mandar(valorAtributo('txtNombre'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoRips'));
            add_valores_a_mandar(valorAtributo('cmbIdCentroCosto'));
            add_valores_a_mandar(valorAtributo('cmbNivel'));
            add_valores_a_mandar(valorAtributo('txtCups'));
            add_valores_a_mandar(valorAtributo('txtCodContable'));
            add_valores_a_mandar(valorAtributo('txtUsuarioElaboro'));

            add_valores_a_mandar(valorAtributo('cmbClase'));
            add_valores_a_mandar(valorAtributo('cmbTipoServicio'));
            add_valores_a_mandar(valorAtributo('cmbTipo'));
            add_valores_a_mandar(valorAtributo('cmbVigente'));
            add_valores_a_mandar(valorAtributo('txtId'));
            ajaxModificar();
            break;
        case 'motivoEnfermedad':
            //			 if(LoginSesion() == valorAtributo('lblUsuarioCrea')){
            if (valorAtributo('lblIdDocumento') != '') {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=14&parametros=";
                add_valores_a_mandar(valorAtributo('txtMotivoConsulta'));
                add_valores_a_mandar(valorAtributo('txtEnfermedadActual'));
                add_valores_a_mandar(valorAtributo('cmb_SintomaticoPiel'));
                add_valores_a_mandar(valorAtributo('cmb_SintomaticoRespira'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                ajaxModificar();
            } else
                alert('SELECCIONE FOLIO')
            break;
        case 'modificarEstadoCita':
            //			 if(LoginSesion() == valorAtributo('lblUsuarioCrea')){ 
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=357&parametros=";
                add_valores_a_mandar(valorAtributo('cmbEstadoCitaEdit'));
                add_valores_a_mandar(valorAtributo('cmbMotivoConsultaEdit'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('cmbEstadoCitaEdit'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('lblIdCitaEdit'));
                //add_valores_a_mandar(valorAtributo('lblIdCitaEdit'));	/*  jjjj345 quitar si no funciona*/				 			 
                ajaxModificar();
            }
            //			 } 
            //			 else alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO')
            break;
        case 'eliminarAdmision':
            if (LoginSesion() == valorAtributo('lblUsuarioCrea')) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=355&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                ajaxModificar();
            } else
                alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO')
            break;
        case 'modificarAdmisionCuenta':
            if (verificarCamposGuardar(arg)) {

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=521&parametros=";

                //actualiza admision
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIPSRemite'));
                add_valores_a_mandar(valorAtributo('txtObservacion'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoRegimen'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
                add_valores_a_mandar(valorAtributo('cmbTipoAdmision'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));

                //elimina recibo y detalle factura
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                add_valores_a_mandar(valorAtributo('lblIdFactura'));

                //actualiza factura
                add_valores_a_mandar(valorAtributo('lblIdPlanContratacion'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoAfiliado'));
                add_valores_a_mandar(valorAtributo('cmbIdRango'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                ajaxModificar();

            }
            break;
        case 'eliminarAdmisionCuenta':

            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=522&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                ajaxModificar();
            }
            break;
        case 'modificarAdmision':
            // if(LoginSesion() == valorAtributo('lblUsuarioCrea')){
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=385&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradoraEdit'));
            add_valores_a_mandar(valorAtributo('cmbTipoAdmisionEdit'));
            add_valores_a_mandar(valorAtributo('lblIdAdmision'));
            ajaxModificar();
            /* } 
             else alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO')*/
            break;
        case 'modificarAdmisionCama':
            // if(LoginSesion() == valorAtributo('lblUsuarioCrea')){
            valores_a_mandar = 'accion=' + arg;
            if (valorAtributo('lblIdAdmision') != '') {
                valores_a_mandar = valores_a_mandar + "&idQuery=635&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('cmbIdCamaEdit'));
                ajaxModificar();
            } else {
                valores_a_mandar = valores_a_mandar + "&idQuery=636&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                add_valores_a_mandar(valorAtributo('cmbIdCamaEdit'));
                ajaxModificar();
            }
            /* } 
             else alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO')*/
            break;
        case 'modificarObservacionListaEspera':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=343&parametros=";
            add_valores_a_mandar(valorAtributo('cmbGradoNecesidadMod'));
            add_valores_a_mandar(valorAtributo('cmbEstadoLEMod'));
            add_valores_a_mandar(valorAtributo('txtNoAutorizacionLE'));
            add_valores_a_mandar(valorAtributo('txtFechaVigenciaLE'));
            add_valores_a_mandar(valorAtributo('txtObservacionLE'));
            //				 add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('lblListaEspera'));
            ajaxModificar();
            break;
        case 'eliminarListCirugiaProcedimientos':
            //	 if(LoginSesion() == valorAtributo('lblUsuarioElaboro')){  
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=341&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
            add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));
            ajaxModificar();
            /* }
             else alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO')
             */
            break;
        case 'modificarListCirugiaProcedimientos':
            //	 if(LoginSesion() == valorAtributo('lblUsuarioElaboro')){  
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=32&parametros=";
            add_valores_a_mandar(valorAtributo('txtObservacionProc'));
            add_valores_a_mandar(valorAtributo('lblId'));
            ajaxModificar();
            /* }
             else alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO')
             */
            break;
        case 'eliminarListaEsperaProcedimientos':
            if (LoginSesion() == valorAtributo('lblUsuarioElaboro')) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=333&parametros=";
                add_valores_a_mandar(valorAtributo('lblListaEsperaProcedimiento'));
                ajaxModificar();
            } else
                alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO')
            break;
        case 'adicionarListaEsperaProcedimientos':
            if (verificarCamposGuardar(arg)) {
                if (LoginSesion() == valorAtributo('lblUsuarioElaboro')) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=332&parametros=";
                    add_valores_a_mandar(valorAtributo('lblListaEspera'));
                    add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgico'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));
                    add_valores_a_mandar(valorAtributo('txtObservacionLEsp'));
                    ajaxModificar();
                } else
                    alert('NO SE PUEDE ADICIONAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO')
            }
            break;
        case 'adicionarHcCirugiaProcedimientosPos':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=389&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgicoPosO'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoPosO'));
                add_valores_a_mandar(valorAtributo('txtObservacionProcedimientoCirugiaPosO'));
                ajaxModificar();
            }
            break;
        case 'adicionarPersonalProc':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=392&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('cmbIdProfesiones'));
                add_valores_a_mandar(valorAtributo('cmbIdProfesionalesProc'));
                add_valores_a_mandar(valorAtributo('txtObservacionPersonalProc'));
                limpiaAtributo('cmbIdProfesiones', 0)
                limpiaAtributo('cmbIdProfesionalesProc', 0)
                limpiaAtributo('txtObservacionPersonalProc', 0)
                ajaxModificar();
            }
            break;
        case 'listPersonalProcEliminar':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=394&parametros=";
                add_valores_a_mandar(valorAtributo('lblId'));
                ajaxModificar();
            }
            break;
        case 'traerHcCirugiaProcedimientosPreAPos':
            if (verificarCamposGuardar(arg)) {
                //			   if(LoginSesion() == valorAtributo('lblUsuarioElaboro')){   
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=389&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('lblIdSitioPre'));
                add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));
                add_valores_a_mandar(valorAtributo('txtObserProTraer'));
                ajaxModificar();
                //		   } else alert('NO SE PUEDE ADICIONAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO')				 
            }
            break;
        case 'listCitaCirugiaProcedimiento':
            if (verificarCamposGuardar(arg)) {
                var ids = jQuery("#" + arg).getDataIDs();
                cant = ids.length;
                proximo = cant + 1;    	// pilas toca asi, o si no hay problemas en el momento de consultar y agregar uno nuevo
                var datarow = {
                    tipo: '0',
                    idSitioQuirur: valorAtributo('cmbIdSitioQuirurgico'),
                    nombreSitioQuirur: valorAtributoCombo('cmbIdSitioQuirurgico'),
                    idProcedimiento: valorAtributoIdAutoCompletar('txtIdProcedimiento'),
                    nombreProcedimiento: valorAtributoNomAutoCompletar('txtIdProcedimiento'),
                    Observaciones: valorAtributo('txtObservacionProcedimientoCirugia'),
                };
                var su = jQuery('#' + arg).addRowData(proximo, datarow);
            }
            break;
        case 'adicionarProcedimientosDeFactura':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=418&parametros=";
                //add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoCex'));
                add_valores_a_mandar(valorAtributo('cmbCantidad'));
                add_valores_a_mandar(valorAtributo('lblValorUnitarioProc'));

                add_valores_a_mandar(valorAtributo('lblValorUnitarioProc'));
                 add_valores_a_mandar(valorAtributo('cmbCantidad'));
                add_valores_a_mandar(valorAtributo('lblValorUnitarioProc'));

                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdPlan'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoCex'));
                ajaxModificar();

            }
            break;
        /*case 'generarFactura':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=429&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdCuenta'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(IdSesion());
                ajaxModificar();
            }
            break;*/

        case 'ajustarMaximoCuenta':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=684&parametros=";
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                ajaxModificar();
            }
            break;

        case 'ajustarCuentaAnual':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=686&parametros=";
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                ajaxModificar();
            }
            break;

        case 'actualizarFacturaGenerada':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=685&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdFactura'));
                ajaxModificar();
            }
            break;


        case 'listCitaCirugiaProcedimientoLE':
            if (verificarCamposGuardar(arg)) {
                var ids = jQuery("#" + arg).getDataIDs();
                cant = ids.length;
                proximo = cant + 1;    	// pilas toca asi, o si no hay problemas en el momento de consultar y agregar uno nuevo
                var datarow = {
                    tipo: '0',
                    idSitioQuirur: valorAtributo('cmbIdSitioQuirurgicoLE'),
                    nombreSitioQuirur: valorAtributoCombo('cmbIdSitioQuirurgicoLE'),
                    idProcedimiento: valorAtributoIdAutoCompletar('txtIdProcedimientoLE'),
                    nombreProcedimiento: valorAtributoNomAutoCompletar('txtIdProcedimientoLE'),
                    Observaciones: ($('#drag' + ventanaActual.num).find('#txtObservacionProcedimientoCirugiaLE').val()).trim(),
                };
                var su = jQuery('#' + arg).addRowData(proximo, datarow);
            }
            limpiaAtributo('cmbIdSitioQuirurgicoLE', 0)
            limpiaAtributo('txtIdProcedimientoLE', 0)
            limpiaAtributo('txtObservacionProcedimientoCirugiaLE', 0)
            break;
        case 'listCitaCirugiaProcedimientoLEGestion':
            if (verificarCamposGuardar(arg)) {
                var ids = jQuery("#" + arg).getDataIDs();
                cant = ids.length;
                proximo = cant + 1;    	// pilas toca asi, o si no hay problemas en el momento de consultar y agregar uno nuevo
                var datarow = {
                    tipo: '0',
                    idSitioQuirur: valorAtributo('cmbIdSitioQuirurgicoLE'),
                    nombreSitioQuirur: valorAtributoCombo('cmbIdSitioQuirurgicoLE'),
                    idProcedimiento: valorAtributoIdAutoCompletar('txtIdProcedimientoLE'),
                    nombreProcedimiento: valorAtributoNomAutoCompletar('txtIdProcedimientoLE'),
                    Observaciones: ($('#drag' + ventanaActual.num).find('#txtObservacionProcedimientoCirugiaLE').val()).trim(),
                };
                var su = jQuery('#' + arg).addRowData(proximo, datarow);
            }
            limpiaAtributo('cmbIdSitioQuirurgicoLE', 0)
            limpiaAtributo('txtIdProcedimientoLE', 0)
            limpiaAtributo('txtObservacionProcedimientoCirugiaLE', 0)
            break;
        case 'adicionarCirugiaProcedimientosACitaDesdeLETraer':/*al hacer clic en la le traer de cirugia se copian*/
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=360&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
                add_valores_a_mandar(valorAtributo('lblIdListaEspera'));

                ajaxModificar();
            }
            break;
        case 'eliminaListaEspera':
            if (verificarCamposGuardar(arg)) {
                if (LoginSesion() == valorAtributo('lblUsuarioElaboro')) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=322&parametros=";
                    add_valores_a_mandar(valorAtributo('lblListaEspera'));
                    ajaxModificar();
                } else
                    alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO')
            }
            break;
        case 'modificarListaEspera':
            // if(verificarCamposGuardar(arg)){  
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=319&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1LE'));
            add_valores_a_mandar(valorAtributo('txtObservacionLE'));
            // add_valores_a_mandar(valorAtributo('cmbIdEspecialidadLE'));
            add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidadLE'));
            //add_valores_a_mandar(valorAtributo('txtNoAutorizacionLE')); 	
            //add_valores_a_mandar(valorAtributo('txtFechaVencimientoLE')); 	
            add_valores_a_mandar(valorAtributo('cmbTipoCitaLEM'));
            add_valores_a_mandar(valorAtributo('cmbNecesidadLE'));
            add_valores_a_mandar(valorAtributo('cmbEstadoLEM'));
            add_valores_a_mandar(LoginSesion());
            add_valores_a_mandar(valorAtributo('lblListaEsperaModificar'));
            ajaxModificar();
            // }
            break;
        case 'crearListaEspera':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                //valores_a_mandar=valores_a_mandar+"&idQuery=29&parametros=";
                valores_a_mandar = valores_a_mandar + "&idQuery=665&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));

                add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
                add_valores_a_mandar(valorAtributo('txtFechaVigencia'));
                add_valores_a_mandar(valorAtributo('txtObservacion'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbDiscapaciFisica'));
                add_valores_a_mandar(valorAtributo('cmbEmbarazo'));
                add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                add_valores_a_mandar(valorAtributo('cmbEsperar'));
                add_valores_a_mandar(valorAtributo('cmbNecesidad'));
                add_valores_a_mandar(valorAtributo('cmbEstado'));
                add_valores_a_mandar(valorAtributo('txtClaseEntrada'));
                add_valores_a_mandar(LoginSesion());
                /**/
                add_valores_a_mandar(valorAtributo('cmbSede'));

                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
                add_valores_a_mandar(valorAtributo('txtTelefonos'));
                add_valores_a_mandar(valorAtributo('txtCelular1'));
                add_valores_a_mandar(valorAtributo('txtCelular2'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                ajaxModificar();
            }
            break;
        case 'crearListaEsperaCirugia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                //valores_a_mandar=valores_a_mandar+"&idQuery=334&parametros=";	  
                valores_a_mandar = valores_a_mandar + "&idQuery=436&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
                add_valores_a_mandar(valorAtributo('txtFechaVigencia'));
                add_valores_a_mandar(valorAtributo('txtObservacion'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                add_valores_a_mandar(valorAtributo('cmbDiscapaciFisica'));
                add_valores_a_mandar(valorAtributo('cmbEmbarazo'));
                add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                add_valores_a_mandar(valorAtributo('cmbEsperar'));
                add_valores_a_mandar(valorAtributo('cmbNecesidad'));
                add_valores_a_mandar(valorAtributo('cmbEstado'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('cmbSede'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIPSRemite'));
                /**/

                sacarValoresColumnasDeGrilla('listCitaCirugiaProcedimientoLE')
                add_valores_a_mandar(valorAtributo('txtColumnaIdSitioQuirur'));
                add_valores_a_mandar(valorAtributo('txtColumnaIdProced'));
                add_valores_a_mandar(valorAtributo('txtColumnaObservacion'));

                ajaxModificar();
            }
            break;
        case 'crearListaEsperaCirugiaProgramacion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=4&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
                add_valores_a_mandar(valorAtributo('txtFechaVigencia'));
                add_valores_a_mandar(valorAtributo('txtObservacion'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                add_valores_a_mandar(valorAtributo('lblIdProfRemite'));
                add_valores_a_mandar(valorAtributo('cmbDiscapaciFisica'));
                add_valores_a_mandar(valorAtributo('cmbEmbarazo'));
                add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                add_valores_a_mandar(valorAtributo('cmbEsperar'));
                add_valores_a_mandar(valorAtributo('cmbNecesidad'));
                add_valores_a_mandar(valorAtributo('cmbEstado'));
                add_valores_a_mandar(LoginSesion());

                sacarValoresColumnasDeGrillaGestion('listCitaCirugiaProcedimientoLEGestion')
                add_valores_a_mandar(valorAtributo('txtColumnaIdSitioQuirur'));
                add_valores_a_mandar(valorAtributo('txtColumnaIdProced'));
                add_valores_a_mandar(valorAtributo('txtColumnaObservacion'));
                add_valores_a_mandar(valorAtributo('txtColumnaID'));

                ajaxModificar();
            }
            break;
        case 'pacienteAtendido':
            if (valorAtributo('lblIdTipoAdmision') != '') {
                if (confirm('Esta seguro de atender a:\n\n\n' + valorAtributo('lblNombrePaciente') + ' ?')) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=20&parametros=";
                    add_valores_a_mandar(LoginSesion());
                    add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                    ajaxModificar();
                }

            } else
                alert('NO SE PUEDE ATENDER PORQUE DEBE TENER UNA ADMISION')
            break;
        case 'folioCorregidoAuditoria':
            if (valorAtributo('lblIdDocumento') != '' && valorAtributo('txtIdEsdadoDocumento') == '12') {
                if (confirm('ESTA SEGURO DE RESPONDER LA CORRECCION DE LA AUDITORIA DEL FOLIO?')) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=601&parametros=";
                    //				 add_valores_a_mandar(LoginSesion());				 				 		
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    ajaxModificar();
                }
            } else
                alert('EL FOLIO DEBE ESTAR SELECCIONADO Y EN ESTADO PRE FINALIZANDO')
            break;
        //abre desde la HC	
        case 'abrirDocumento':
            if (valorAtributo('lblIdDocumento') != '') {
                if (confirm('Esta seguro de Abrir a:\n\n\n' + valorAtributo('lblIdDocumento') + ' ?')) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=374&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(IdSesion());

                    ajaxModificar();
                }

            } else
                alert('NO SE PUEDE ATENDER PORQUE DEBE TENER UNA ADMISION')
            break;
        case 'accionesAlEstadoFolio':
            if (valorAtributo('lblIdDocumento') != '') {
                if (confirm('SOLO MODIFICARA EL ESTADO DEL FOLIO \nSE REGISTRARA ESTA ACCION A SU NOMBRE CON FECHA Y HORA \n\nESTA SEGURO? ')) {
                    valores_a_mandar = 'accion=' + arg;


                    if (valorAtributo('cmbAccionFolio') != '2') {
                        if (valorAtributo('txtMotivoAccion') != '') {
                            valores_a_mandar = valores_a_mandar + "&idQuery=384&parametros=";
                            add_valores_a_mandar(valorAtributo('cmbAccionFolio'));
                            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                            add_valores_a_mandar(valorAtributo('cmbAccionFolio'));
                            add_valores_a_mandar(valorAtributo('txtMotivoAccion'));
                            add_valores_a_mandar(LoginSesion());
                        } else alert('SELECCIONE MOTIVO DE LA ACCION')
                    } else {
                        /*SI NO SE VA A CLONAR*/alert('SE VA A CLONAR ESTE FOLIO')
                        valores_a_mandar = valores_a_mandar + "&idQuery=531&parametros=";
                        add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                        add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
                        add_valores_a_mandar(valorAtributo('lblIdAdmision'));


                        add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                        add_valores_a_mandar(valorAtributo('cmbAccionFolio'));
                        add_valores_a_mandar(valorAtributo('txtMotivoAccion'));
                        add_valores_a_mandar(LoginSesion());
                    }
                    ajaxModificar();
                }

            } else
                alert('NO SE PUEDE ATENDER PORQUE DEBE TENER UNA ADMISION')
            break;
        case 'auditarFolio':
            if (valorAtributo('lblIdDocumento') != '' && valorAtributo('lblIdEstadoFolio') == '12') {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=593&parametros=";
                +
                    add_valores_a_mandar(valorAtributo('cmbAccionAuditoria'))
                add_valores_a_mandar(valorAtributo('txtAuditado'))
                add_valores_a_mandar(LoginSesion())
                add_valores_a_mandar(valorAtributo('lblIdDocumento'))
                ajaxModificar()
            } else
                alert('1.2FOLIO DEBE ESTAR EN PRE FINALIZADO O DEBE SELECCIONAR FOLIO')
            break;
        case 'enviaAutorizaIntercambio':
            if (valorAtributo('lblIdDocumento') != '' /*&& valorAtributo('lblIdEstadoFolio')=='12'*/) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=628&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdEnviado'))
                add_valores_a_mandar(valorAtributo('cmbEsperarProcAutoriza'))
                add_valores_a_mandar(valorAtributo('lblIdProcedimientoAutoriza'))
                ajaxModificar()
            } else
                alert('1.3 FOLIO DEBE ESTAR EN PRE FINALIZADO O DEBE SELECCIONAR FOLIO')
            break;
        case 'limpiaSolicitudIntercambio':
            if (valorAtributo('lblIdDocumento') != '' /*&& valorAtributo('lblIdEstadoFolio')=='12'*/) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=651&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdSolicitud'))
                add_valores_a_mandar(valorAtributo('lblIdSolicitud'))
                ajaxModificar()
            } else
                alert('1.3 FOLIO DEBE ESTAR EN PRE FINALIZADO O DEBE SELECCIONAR FOLIO')
            break;

        case 'modificaJustificacionConcepto':
            if (valorAtributo('lblIdSolicitud') == '' /*&& valorAtributo('lblIdEstadoFolio')=='12'*/) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=645&parametros=";
                add_valores_a_mandar(valorAtributo('txtJustificacionFolio'))
                add_valores_a_mandar(valorAtributo('lblIdDocumento'))
                ajaxModificar()
            } else
                alert('1.4.5 SELECCIONA SOLICITUD(QUEDA PARA REVISION EN EL 2018)')
            break;

        case 'pacienteDilatando':
            if (valorAtributo('lblIdTipoAdmision') != '') {
                if (confirm('Esta seguro de poner en estado DILATANDO  a:\n\n\n' + valorAtributo('lblNombrePaciente') + ' ?')) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=21&parametros=";
                    add_valores_a_mandar(LoginSesion());
                    add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                    ajaxModificar();
                }

            } else
                alert('NO SE PUEDE ATENDER PORQUE DEBE TENER UNA ADMISION')
            break;
        case 'pacientePreconsulta':
            if (document.getElementById('txt_ECOF_C9')) {
                var vlr = document.getElementById('txt_ECOF_C9').value;
            }
            if (document.getElementById('txt_EXOF_C26')) {
                var vlr = document.getElementById('txt_EXOF_C26').value;
            }
            if (valorAtributo('lblIdTipoAdmision') != '') {
                if (vlr != '') {

                    if (confirm('Esta seguro de poner en estado Preconsulta  a:\n\n\n' + valorAtributo('lblNombrePaciente') + ' ?')) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=25&parametros=";
                        add_valores_a_mandar(LoginSesion());
                        add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                        ajaxModificar();
                    }
                } else
                    alert('PARA CONTINUAR INGRESE INFORMACION EN EL CAMPO PUPILAS');
            } else
                alert('NO SE PUEDE ATENDER PORQUE DEBE TENER UNA ADMISION')
            break;
        case 'crearAdmision':
            if (verificarCamposGuardar(arg)) {
                if (valorAtributo('lblIdCita') == '') { /*ingreso directo sin cita*/
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=658&parametros=";
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                    add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
                    add_valores_a_mandar(valorAtributo('txtTelefonos'));
                    add_valores_a_mandar(valorAtributo('txtCelular1'));
                    add_valores_a_mandar(valorAtributo('txtCelular2'));
                    add_valores_a_mandar(valorAtributo('txtEmail'));
                    add_valores_a_mandar(valorAtributo('cmbIdEtnia'));
                    add_valores_a_mandar(valorAtributo('cmbIdNivelEscolaridad'));
                    add_valores_a_mandar(valorAtributo('cmbIdOcupacion'));
                    add_valores_a_mandar(valorAtributo('cmbIdEstrato'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));

                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                    add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
                    add_valores_a_mandar(LoginSesion());
                } else {   /*ingreso  con cita*/
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=659&parametros=";
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                    add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
                    add_valores_a_mandar(valorAtributo('txtTelefonos'));
                    add_valores_a_mandar(valorAtributo('txtCelular1'));
                    add_valores_a_mandar(valorAtributo('txtCelular2'));
                    add_valores_a_mandar(valorAtributo('txtEmail'));
                    add_valores_a_mandar(valorAtributo('cmbIdEtnia'));
                    add_valores_a_mandar(valorAtributo('cmbIdNivelEscolaridad'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdOcupacion'));
                    add_valores_a_mandar(valorAtributo('cmbIdEstrato'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));	/*PK*/
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    add_valores_a_mandar(valorAtributo('cmbTipoId') + valorAtributo('txtIdentificacion'));	/*IDENTIFICACION ACTUAL DEL PACIENTE*/
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIPSRemite'));
                    add_valores_a_mandar(valorAtributo('cmbIdTipoRegimen'));
                    add_valores_a_mandar(valorAtributo('cmbTipoAdmision'));
                    add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
                    add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
                    add_valores_a_mandar(valorAtributo('txtObservacion'));
                    add_valores_a_mandar(LoginSesion());
                    add_valores_a_mandar(valorAtributo('lblIdCita')); // PARA CREAR  FACTURACION.ADMISION_AGENDA_DETALLE
                }
                ajaxModificar();
            }
            break;
        case 'crearAdmisionCirugia':
            if (verificarCamposGuardar(arg)) {
                if (valorAtributo('lblIdCita') == '') { /*ingreso directo sin cita*/
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=660&parametros=";
                    /*                    add_valores_a_mandar(valorAtributo('cmbTipoId'));
                                        add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                                        add_valores_a_mandar(valorAtributo('txtApellido1'));
                                        add_valores_a_mandar(valorAtributo('txtApellido2'));
                                        add_valores_a_mandar(valorAtributo('txtNombre1'));
                                        add_valores_a_mandar(valorAtributo('txtNombre2'));
                                        add_valores_a_mandar(valorAtributo('txtFechaNac'));
                                        add_valores_a_mandar(valorAtributo('cmbSexo'));
                    */
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                    add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
                    add_valores_a_mandar(valorAtributo('txtTelefonos'));
                    add_valores_a_mandar(valorAtributo('txtCelular1'));
                    add_valores_a_mandar(valorAtributo('txtCelular2'));
                    add_valores_a_mandar(valorAtributo('cmbIdEtnia'));
                    add_valores_a_mandar(valorAtributo('cmbIdNivelEscolaridad'));
                    add_valores_a_mandar(valorAtributo('cmbIdOcupacion'));
                    add_valores_a_mandar(valorAtributo('cmbIdEstrato'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));

                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));

                    add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
                    add_valores_a_mandar(LoginSesion());
                } else {   /*ingreso  con cita*/
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=661&parametros=";
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                    add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
                    add_valores_a_mandar(valorAtributo('txtTelefonos'));
                    add_valores_a_mandar(valorAtributo('txtCelular1'));
                    add_valores_a_mandar(valorAtributo('txtCelular2'));
                    add_valores_a_mandar(valorAtributo('cmbIdEtnia'));
                    add_valores_a_mandar(valorAtributo('cmbIdNivelEscolaridad'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdOcupacion'));
                    add_valores_a_mandar(valorAtributo('cmbIdEstrato'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));

                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));	/*PK*/

                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    add_valores_a_mandar(valorAtributo('cmbTipoId') + valorAtributo('txtIdentificacion'));	/*IDENTIFICACION ACTUAL DEL PACIENTE*/
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIPSRemite'));
                    add_valores_a_mandar(valorAtributo('cmbIdTipoRegimen'));
                    add_valores_a_mandar(valorAtributo('cmbTipoAdmision'));
                    add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
                    add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
                    add_valores_a_mandar(valorAtributo('txtObservacion'));
                    add_valores_a_mandar(LoginSesion());

                    add_valores_a_mandar(valorAtributo('lblIdCita')); // PARA CREAR  FACTURACION.ADMISION_AGENDA_DETALLE

                    sacarValoresColumnasDeGrilla('listCitaCirugiaProcedimiento');
                    //	   add_valores_a_mandar(valorAtributo('lblIdCita'));   //PK
                    add_valores_a_mandar(valorAtributo('txtColumnaIdSitioQuirur'));
                    add_valores_a_mandar(valorAtributo('txtColumnaIdProced'));
                    add_valores_a_mandar(valorAtributo('txtColumnaObservacion'));

                }
                ajaxModificar();
            }
            break;

        case 'crearAdmisionCuentaSinCita':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=567&parametros=";
            add_valores_a_mandar(valorAtributo('cmbTipoId'));
            add_valores_a_mandar(valorAtributo('txtIdentificacion'));
            add_valores_a_mandar(valorAtributo('txtApellido1'));
            add_valores_a_mandar(valorAtributo('txtApellido2'));
            add_valores_a_mandar(valorAtributo('txtNombre1'));
            add_valores_a_mandar(valorAtributo('txtNombre2'));
            add_valores_a_mandar(valorAtributo('txtFechaNac'));
            add_valores_a_mandar(valorAtributo('cmbSexo'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
            add_valores_a_mandar(valorAtributo('txtDireccionRes'));
            add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
            add_valores_a_mandar(valorAtributo('txtTelefonos'));
            add_valores_a_mandar(valorAtributo('txtCelular1'));
            add_valores_a_mandar(valorAtributo('txtCelular2'));
            add_valores_a_mandar(valorAtributo('cmbIdEtnia'));
            add_valores_a_mandar(valorAtributo('cmbIdNivelEscolaridad'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdOcupacion'));
            add_valores_a_mandar(valorAtributo('cmbIdEstrato'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));	/*PK*/

            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('cmbTipoId') + valorAtributo('txtIdentificacion'));	/*IDENTIFICACION ACTUAL DEL PACIENTE*/
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoRegimen'));
            add_valores_a_mandar(valorAtributo('cmbTipoAdmision'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIPSRemite'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
            add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
            add_valores_a_mandar(valorAtributo('txtObservacion'));
            add_valores_a_mandar(LoginSesion());
            // add_valores_a_mandar(valorAtributo('lblIdCita')); // PARA CREAR  FACTURACION.ADMISION_AGENDA_DETALLE

            add_valores_a_mandar(valorAtributo('lblIdPlanContratacion'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoAfiliado'));
            add_valores_a_mandar(valorAtributo('cmbIdRango'));
            add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
            ajaxModificar();
            break;
        case 'crearAdmisionCuenta':
            valores_a_mandar = 'accion=' + arg;
            //if (valorAtributo('lblIdCuentaAntigua') == '') {
            valores_a_mandar = valores_a_mandar + "&idQuery=422&parametros=";
            add_valores_a_mandar(valorAtributo('cmbTipoId'));
            add_valores_a_mandar(valorAtributo('txtIdentificacion'));
            add_valores_a_mandar(valorAtributo('txtApellido1'));
            add_valores_a_mandar(valorAtributo('txtApellido2'));
            add_valores_a_mandar(valorAtributo('txtNombre1'));
            add_valores_a_mandar(valorAtributo('txtNombre2'));
            add_valores_a_mandar(valorAtributo('txtFechaNac'));
            add_valores_a_mandar(valorAtributo('cmbSexo'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
            add_valores_a_mandar(valorAtributo('txtDireccionRes'));
            add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
            add_valores_a_mandar(valorAtributo('txtTelefonos'));
            add_valores_a_mandar(valorAtributo('txtCelular1'));
            add_valores_a_mandar(valorAtributo('txtCelular2'));
            add_valores_a_mandar(valorAtributo('cmbIdEtnia'));
            add_valores_a_mandar(valorAtributo('cmbIdNivelEscolaridad'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdOcupacion'));
            add_valores_a_mandar(valorAtributo('cmbIdEstrato'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));	/*PK*/

            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('cmbTipoId') + valorAtributo('txtIdentificacion'));	/*IDENTIFICACION ACTUAL DEL PACIENTE*/
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoRegimen'));
            add_valores_a_mandar(valorAtributo('cmbTipoAdmision'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIPSRemite'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
            add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
            add_valores_a_mandar(valorAtributo('txtObservacion'));
            add_valores_a_mandar(LoginSesion());
            add_valores_a_mandar(valorAtributo('lblIdCita')); // PARA CREAR  FACTURACION.ADMISION_AGENDA_DETALLE
            add_valores_a_mandar(valorAtributo('lblCitaFutura'));
            add_valores_a_mandar(valorAtributo('lblIdCita'));

            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('lblIdPlanContratacion'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoAfiliado'));
            add_valores_a_mandar(valorAtributo('cmbIdRango'));
            add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
            add_valores_a_mandar(IdSesion());

            ajaxModificar();
            /*} else {   
            	//CUANDO LA CUENTA YA ESXISTE ANTIGUA
                valores_a_mandar = valores_a_mandar + "&idQuery=576&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdCuentaAntigua'));

                add_valores_a_mandar(valorAtributo('cmbTipoId'));
                add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                add_valores_a_mandar(valorAtributo('txtApellido1'));
                add_valores_a_mandar(valorAtributo('txtApellido2'));
                add_valores_a_mandar(valorAtributo('txtNombre1'));
                add_valores_a_mandar(valorAtributo('txtNombre2'));
                add_valores_a_mandar(valorAtributo('txtFechaNac'));
                add_valores_a_mandar(valorAtributo('cmbSexo'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
                add_valores_a_mandar(valorAtributo('txtTelefonos'));
                add_valores_a_mandar(valorAtributo('txtCelular1'));
                add_valores_a_mandar(valorAtributo('txtCelular2'));
                add_valores_a_mandar(valorAtributo('cmbIdEtnia'));
                add_valores_a_mandar(valorAtributo('cmbIdNivelEscolaridad'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdOcupacion'));
                add_valores_a_mandar(valorAtributo('cmbIdEstrato'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));	//PK

                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                add_valores_a_mandar(valorAtributo('cmbTipoId') + valorAtributo('txtIdentificacion'));	//IDENTIFICACION ACTUAL DEL PACIENTE
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoRegimen'));
                add_valores_a_mandar(valorAtributo('cmbTipoAdmision'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIPSRemite'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
                add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
                add_valores_a_mandar(valorAtributo('txtObservacion'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('lblIdCita')); // PARA CREAR  FACTURACION.ADMISION_AGENDA_DETALLE
                add_valores_a_mandar(valorAtributo('lblCitaFutura'));
               
                ajaxModificar();
            }*/

            break;
        case 'crearAdmisionCirugiaCuenta':
            if (verificarCamposGuardar(arg)) {

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=388&parametros=";
                add_valores_a_mandar(valorAtributo('cmbTipoId'));
                add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                add_valores_a_mandar(valorAtributo('txtApellido1'));
                add_valores_a_mandar(valorAtributo('txtApellido2'));
                add_valores_a_mandar(valorAtributo('txtNombre1'));
                add_valores_a_mandar(valorAtributo('txtNombre2'));
                add_valores_a_mandar(valorAtributo('txtFechaNac'));
                add_valores_a_mandar(valorAtributo('cmbSexo'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
                add_valores_a_mandar(valorAtributo('txtTelefonos'));
                add_valores_a_mandar(valorAtributo('txtCelular1'));
                add_valores_a_mandar(valorAtributo('txtCelular2'));
                add_valores_a_mandar(valorAtributo('cmbIdEtnia'));
                add_valores_a_mandar(valorAtributo('cmbIdNivelEscolaridad'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdOcupacion'));
                add_valores_a_mandar(valorAtributo('cmbIdEstrato'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));

                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));	/*PK*/

                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                add_valores_a_mandar(valorAtributo('cmbTipoId') + valorAtributo('txtIdentificacion'));	/*IDENTIFICACION ACTUAL DEL PACIENTE*/
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoRegimen'));
                add_valores_a_mandar(valorAtributo('cmbTipoAdmision'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
                add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
                add_valores_a_mandar(valorAtributo('txtObservacion'));
                add_valores_a_mandar(LoginSesion());

                add_valores_a_mandar(valorAtributo('lblIdCita')); // PARA CREAR  FACTURACION.ADMISION_AGENDA_DETALLE

                sacarValoresColumnasDeGrilla('listCitaCirugiaProcedimiento');
                //	   add_valores_a_mandar(valorAtributo('lblIdCita'));   //PK
                add_valores_a_mandar(valorAtributo('txtColumnaIdSitioQuirur'));
                add_valores_a_mandar(valorAtributo('txtColumnaIdProced'));
                add_valores_a_mandar(valorAtributo('txtColumnaObservacion'));

                ajaxModificar();
            }
            break;
        case 'modificaCitaArchivo':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=375&parametros="; /*crea en estado asignado*/
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
                add_valores_a_mandar(valorAtributo('cmbMotivoConsulta'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));

                add_valores_a_mandar(valorAtributo('txtFechaPacienteCita'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIPSRemite'));
                add_valores_a_mandar(' ' + LoginSesion() + ' ' + document.getElementById('lblFechaDeHoy').lastChild.nodeValue + ' ' + document.getElementById('lblHoraServidor').lastChild.nodeValue);
                add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle')); //PK			

                add_valores_a_mandar(valorAtributo('cmbTipoId'));
                add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                add_valores_a_mandar(valorAtributo('txtApellido1'));
                add_valores_a_mandar(valorAtributo('txtApellido2'));
                add_valores_a_mandar(valorAtributo('txtNombre1'));
                add_valores_a_mandar(valorAtributo('txtNombre2'));
                add_valores_a_mandar(valorAtributo('txtFechaNac'));
                add_valores_a_mandar(valorAtributo('cmbSexo'));

                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
                add_valores_a_mandar(valorAtributo('txtTelefonos'));
                add_valores_a_mandar(valorAtributo('txtCelular1'));
                add_valores_a_mandar(valorAtributo('txtCelular2'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                ajaxModificar();
            }
            break;
        case 'crearCita':
            if (valorAtributo('cmbEstadoCita') == 'A' || valorAtributo('cmbEstadoCita') == 'C' || valorAtributo('cmbEstadoCita') == 'F') {
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    if (valorAtributo('cmbEstadoCita') == 'A')
                        valores_a_mandar = valores_a_mandar + "&idQuery=663&parametros=";/*crea en estado asignado*/
                    else
                        valores_a_mandar = valores_a_mandar + "&idQuery=662&parametros=";/*los demas estados*/

                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                    add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
                    add_valores_a_mandar(valorAtributo('cmbMotivoConsulta'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                    add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));

                    if (valorAtributo('cmbEstadoCita') == 'A')
                        add_valores_a_mandar(valorAtributo('txtFechaPacienteCita'));

                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIPSRemite'));
                    add_valores_a_mandar('Estado:' + valorAtributo('cmbEstadoCita') + '-' + valorAtributo('txtObservacion') + ' ' + LoginSesion() + ' ' + document.getElementById('lblFechaDeHoy').lastChild.nodeValue/* +' '+document.getElementById('lblHoraServidor').lastChild.nodeValue*/);
                    add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
                    add_valores_a_mandar(LoginSesion());

                    if (valorAtributo('cmbEstadoCita') == 'C')
                        add_valores_a_mandar(valorAtributo('txtObservacionConfirma') + ' ' + LoginSesion() + ' ' + document.getElementById('lblFechaDeHoy').lastChild.nodeValue + ' ' + document.getElementById('lblHoraServidor').lastChild.nodeValue);

                    add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                    add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
                    add_valores_a_mandar(valorAtributo('txtTelefonos'));
                    add_valores_a_mandar(valorAtributo('txtCelular1'));
                    add_valores_a_mandar(valorAtributo('txtCelular2'));
                    add_valores_a_mandar(valorAtributo('txtEmail'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    ajaxModificar();
                }
            } else { /*L-N-R FALLIDAS*/
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=315&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
                    add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
                    add_valores_a_mandar(valorAtributo('cmbMotivoConsulta'));
                    add_valores_a_mandar('Estado:' + valorAtributo('cmbEstadoCita') + '-' + valorAtributo('txtObservacion') + ' ' + LoginSesion() + ' ' + document.getElementById('lblFechaDeHoy').lastChild.nodeValue/* +' '+document.getElementById('lblHoraServidor').lastChild.nodeValue*/);
                    add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
                    ajaxModificar();
                }
            }

            break;
        case 'crearCitaCirugia':
            if (valorAtributo('cmbEstadoCita') == 'A' || valorAtributo('cmbEstadoCita') == 'C') {
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=664&parametros=";

                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                    add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
                    add_valores_a_mandar(valorAtributo('cmbMotivoConsulta'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                    add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
                    add_valores_a_mandar(valorAtributo('txtFechaPacienteCita'));

                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIPSRemite'));
                    //  add_valores_a_mandar(valorAtributo('txtObservacion'));
                    add_valores_a_mandar('Estado:' + valorAtributo('cmbEstadoCita') + '-' + valorAtributo('txtObservacion') + ' ' + LoginSesion() + ' ' + document.getElementById('lblFechaDeHoy').lastChild.nodeValue + ' ' + document.getElementById('lblHoraServidor').lastChild.nodeValue);


                    add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
                    add_valores_a_mandar(LoginSesion());

                    /*INICIO CIRUGIA*/
                    add_valores_a_mandar(valorAtributo('txtNoLente'));
                    add_valores_a_mandar(valorAtributo('cmbTipoAnestesia'));
                    add_valores_a_mandar(valorAtributo('txtAnestesiologo'));
                    add_valores_a_mandar(valorAtributo('txtInstrumentador'));
                    add_valores_a_mandar(valorAtributo('txtCirculante'));

                    /*INICIO CIRUGIA*/
                    add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));  //PK
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                    add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
                    add_valores_a_mandar(valorAtributo('txtTelefonos'));
                    add_valores_a_mandar(valorAtributo('txtCelular1'));
                    add_valores_a_mandar(valorAtributo('txtCelular2'));
                    add_valores_a_mandar(valorAtributo('txtEmail'));

                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));

                    sacarValoresColumnasDeGrilla('listCitaCirugiaProcedimiento')
                    add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));  //PK
                    add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));  //PK
                    add_valores_a_mandar(valorAtributo('txtColumnaIdSitioQuirur'));
                    add_valores_a_mandar(valorAtributo('txtColumnaIdProced'));
                    add_valores_a_mandar(valorAtributo('txtColumnaObservacion'));
                    ajaxModificar();
                }
            } else { /*L-N-R*/
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=315&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
                    add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
                    add_valores_a_mandar(valorAtributo('cmbMotivoConsulta'));
                    add_valores_a_mandar('Estado:' + valorAtributo('cmbEstadoCita') + '-' + valorAtributo('txtObservacion') + ' ' + LoginSesion() + ' ' + document.getElementById('lblFechaDeHoy').lastChild.nodeValue + ' ' + document.getElementById('lblHoraServidor').lastChild.nodeValue);
                    add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
                    ajaxModificar();
                }
            }

            break;
        case 'listAgendaCopiarDia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=305&parametros=";
                add_valores_a_mandar(valorAtributo('lblFechaSeleccionada'));
                add_valores_a_mandar(valorAtributo('txtFechaDestino'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbSede'));
                add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                add_valores_a_mandar(valorAtributo('txtIdUsuario'));
                ajaxModificar();
            }
            break;
        case 'eliminarAgendaDetalle':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=304&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
            ajaxModificar();
            break;
        case 'dejarDisponibleAgendaDetalle':
            valores_a_mandar = 'accion=' + arg;
            if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
                valores_a_mandar = valores_a_mandar + "&idQuery=59&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
                add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
            } else {
                valores_a_mandar = valores_a_mandar + "&idQuery=365&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
            }
            ajaxModificar();
            break;
        case 'activarDiaAgenda':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=303&parametros=";
            add_valores_a_mandar(valorAtributo('lblFechaSeleccionada'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
            add_valores_a_mandar(valorAtributo('txtIdUsuario'));
            ajaxModificar();
            break;
        case 'subDividirAgenda':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=306&parametros=";
                add_valores_a_mandar(valorAtributo('cmbDuracionMinutosSubDividir'));			 //id2				 
                add_valores_a_mandar(valorAtributo('lblDuracion'));		//id6		 
                add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
                //id5
                ajaxModificar();
            }
            break;
        case 'listAgenda':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=426&parametros=";
                add_valores_a_mandar(valorAtributo('cmbDuracionMinutos'));
                add_valores_a_mandar(valorAtributo('lblFechaSeleccionada'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                add_valores_a_mandar(valorAtributo('txtIdUsuario'));
                add_valores_a_mandar(valorAtributo('cmbSede'));
                add_valores_a_mandar(valorAtributo('lblFechaSeleccionada') + ' ' + valorAtributo('cmbHoraInicio') + ':' + valorAtributo('cmbMinutoInicio'));
                add_valores_a_mandar(valorAtributo('cmbCuantas'));
                ajaxModificar();
            }
            break;
        //***************************************************** fin clinica 		
        case 'meterDatosParaListarLaProyeccion':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=201&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));
            add_valores_a_mandar(valorAtributo('lblIdArticulo'));
            ajaxModificar();

            break;

        case 'continuarProyeccionOrden':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=203&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));
            add_valores_a_mandar(valorAtributo('lblIdArticulo'));
            add_valores_a_mandar(valorAtributo('lblIdOrden')); 	//Esta orden que esta activa
            ajaxModificar();
            break;

        case 'traerMedicamentosOrdenesAnteriores':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=197&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('txtIdUsuario'));
            add_valores_a_mandar(valorAtributo('txtLogin'));
            add_valores_a_mandar(valorAtributo('lblIdDocumentoTraer'));
            add_valores_a_mandar(valorAtributo('lblIdSolicitudTraer'));
            add_valores_a_mandar(valorAtributo('lblIdSolicitudTraer'));
            ajaxModificar();
            break;
        case 'traerMedicamentosOrdenesAnterioresContinuar':
            if (valorAtributo('cmbIdNuevaDuracion') != '') {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=227&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('cmbIdNuevaDuracion'));	 /*1.1 duracion nueva lo que falta en dias*/
                add_valores_a_mandar(valorAtributo('txtIdUsuario'));
                add_valores_a_mandar(valorAtributo('txtLogin'));
                add_valores_a_mandar(valorAtributo('lblIdDocumentoTraer'));
                add_valores_a_mandar(valorAtributo('lblIdSolicitudTraer'));
                add_valores_a_mandar(valorAtributo('lblIdSolicitudTraer'));
                add_valores_a_mandar(valorAtributo('lblIdSolicitudMedicamentoInicial'));
                ajaxModificar();
            } else
                alert('Seleccione la cantidad de Dias para continuar')
            break;

        case 'proyectarDocumentoInventario':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=187&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdOrden'))
                add_valores_a_mandar(valorAtributo('lblIdDocInv'))
                add_valores_a_mandar(valorAtributo('lblIdOrden'))
                add_valores_a_mandar(valorAtributo('lblIdDocInv'))

                add_valores_a_mandar(valorAtributo('cmbIdPresentacion'))
                add_valores_a_mandar(valorAtributo('lblCantidadDespachada'))

                add_valores_a_mandar(valorAtributo('lblIdDocInv'))
                add_valores_a_mandar(valorAtributo('cmbIdBodega'))
                add_valores_a_mandar(valorAtributo('lblIdOrden'))

                add_valores_a_mandar(valorAtributo('lblIdOrden'))
                add_valores_a_mandar(valorAtributo('lblIdDocInv'))
                add_valores_a_mandar(valorAtributo('cmbIdPresentacion'))
                add_valores_a_mandar(valorAtributo('cmbIdBodega'))
                ajaxModificar();
            }
            break;
        case 'proyectarDocumentoInventarioEliminar':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=207&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdOrden'))
                add_valores_a_mandar(valorAtributo('lblIdDocInv'))
                add_valores_a_mandar(valorAtributo('lblIdOrden'))
                add_valores_a_mandar(valorAtributo('lblIdDocInv'))
                add_valores_a_mandar(valorAtributo('lblIdDocInv')) //PARA EL  SELECT ? ,


                add_valores_a_mandar(valorAtributo('txtFechaConsumo') + ' ' + valorAtributo('cmbHoraConsumoDesde') + ':00');
                add_valores_a_mandar(valorAtributo('txtFechaConsumo2') + ' ' + valorAtributo('cmbHoraConsumoHasta') + ':00');


                add_valores_a_mandar(valorAtributo('lblCodItem'))
                add_valores_a_mandar(valorAtributo('cmbUbicacion'));
                if (confirm('ESTA SEGURO DE LA ELIMINACION DEL DOCUMENTO\n Tenga en cuenta que para este Art�culo se adicionar� al documento seg�n los parametros arriba escogidos ?')) {
                    ajaxModificar();
                }
            }
            break;
        case 'eliminarDocumentoInventario':
            if (valorAtributo('lblIdEstadoDocumento') == 0) {
                if (valorAtributo('txtIdUsuario') == valorAtributo('lblIdAutorDocumento')) {
                    if (confirm('ESTA SEGURO DE LA ELIMINACION DEL DOCUMENTO?')) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=186&parametros=";
                        add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                        add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                        ajaxModificar();
                    }
                } else
                    alert('IMPOSIBLE ELIMINAR porque Usted no es el autor');
            } else
                alert('IMPOSIBLE ELIMINAR Documentos Cerrados !!!')
            break;
        case 'eliminarDocumentoInventarioTraslado':
            if (valorAtributo('lblIdEstadoDocumento') == 0) {
                if (valorAtributo('txtIdUsuario') == valorAtributo('lblIdAutorDocumento')) {
                    if (confirm('ESTA SEGURO DE LA ELIMINACION DEL DOCUMENTO TRASLADO ?')) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=221&parametros=";
                        add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                        add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                        ajaxModificar();
                    }
                } else
                    alert('IMPOSIBLE ELIMINAR porque Usted no es el autor');
            } else
                alert('IMPOSIBLE ELIMINAR Documentos Cerrados !!!')
            break;
        case 'eliminarDocumentoInventarioEntradas':
            if (valorAtributo('lblIdEstadoDocumento') == 0) {
                if (valorAtributo('txtIdUsuario') == valorAtributo('lblIdAutorDocumento')) {
                    if (confirm('ESTA SEGURO DE LA ELIMINACION DEL DOCUMENTO?')) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=216&parametros=";
                        add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                        add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                        add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                        ajaxModificar();
                    }
                } else
                    alert('IMPOSIBLE ELIMINAR porque Usted no es el autor');
            } else
                alert('IMPOSIBLE ELIMINAR Documentos Cerrados !!!')
            break;
        case 'eliminarDocumentoInventarioDevoluciones':
            if (valorAtributo('lblIdEstadoDocumento') == 0) {
                if (valorAtributo('txtIdUsuario') == valorAtributo('lblIdAutorDocumento')) {
                    if (confirm('ESTA SEGURO DE LA ELIMINACION DEL DOCUMENTO?')) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=217&parametros=";
                        add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                        add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                        ajaxModificar();
                    }
                } else
                    alert('IMPOSIBLE ELIMINAR porque Usted no es el autor');
            } else
                alert('IMPOSIBLE ELIMINAR Documentos Cerrados !!!')
            break;
        case 'eliminarProcedimientosListado':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=184&parametros=";
            add_valores_a_mandar(valorAtributo('lblId'));
            add_valores_a_mandar(valorAtributo('lblIdElementoP'));
            ajaxModificar();
            break;
        case 'eliminarSignosVitalesListado':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=196&parametros=";
            add_valores_a_mandar(valorAtributo('lblNombreElementoP'));
            ajaxModificar();
            break;
        case 'guardaConciliacion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=514&parametros=";
                add_valores_a_mandar(valorAtributo('cmbExiste'));
                add_valores_a_mandar(valorAtributo('txtConciliacionDescripcion'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                ajaxModificar();
            }
            break;
        case 'listAntecedentesFarmacologicos':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=512&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAntecedenteFarmacologico'));
                add_valores_a_mandar(valorAtributo('txtAntecedenteMedicamento'));
                ajaxModificar();
            }
            break;
        case 'listSistemas':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=347&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtSistema'));
                add_valores_a_mandar(valorAtributo('txtHallazgo'));
                ajaxModificar();
            }
            break;
        case 'listProcedimientosPosOperatoriosEliminar':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=391&parametros=";
            add_valores_a_mandar(valorAtributo('lblId'));
            ajaxModificar();
            break;
        case 'listProcedimientos':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=183&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('txtIdClase'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoServicioSolicitado'))
                add_valores_a_mandar(valorAtributo('cmbIdPrioridad'));
                add_valores_a_mandar(valorAtributo('cmbIdGuia'));
                add_valores_a_mandar(valorAtributo('txtJustificacion'));
                ajaxModificar();
            }
            break;
        case 'listProcedimientosDetalle':
            //if (verificarCamposGuardar(arg)) {
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=366&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));
            add_valores_a_mandar(valorAtributo('cmbCantidad'));
            add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgico'));
            add_valores_a_mandar(valorAtributo('txtIndicacion'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('txtIdClase'));
            add_valores_a_mandar(valorAtributo('cmbIdDxRelacionadoP'));

            ajaxModificar();
            //}
            break;
        case 'listProcedimientoConNoPOS':


            if (tabActivo == 'divProcedimiento') {
                if (verificarCamposGuardar('listProcedimientoConNoPOS')) {
                    valores_a_mandar = 'accion=listProcedimientoConNoPOS';
                    //valores_a_mandar=valores_a_mandar+"&idQuery=6&parametros=";
                    valores_a_mandar = valores_a_mandar + "&idQuery=434&parametros=";
                    // add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));						 
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));
                    add_valores_a_mandar(valorAtributo('cmbCantidad'));
                    add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgico'));
                    add_valores_a_mandar(valorAtributo('txtIndicacion'));
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(valorAtributo('txtIdClase'));

                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));

                }
            } else if (tabActivo == 'divAyudasDiagnosticas') {
                if (verificarCamposGuardar('listProcedimientoConNoPOS')) {
                    valores_a_mandar = 'accion=' + arg;
                    //valores_a_mandar=valores_a_mandar+"&idQuery=6&parametros=";
                    valores_a_mandar = valores_a_mandar + "&idQuery=434&parametros=";
                    //add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));						 
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica'));
                    add_valores_a_mandar(valorAtributo('cmbCantidadAD'));
                    add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgicoAD'));
                    add_valores_a_mandar(valorAtributo('txtIndicacionAD'));
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(valorAtributo('txtIdClaseAD'));

                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica'));
                }
            } else if (tabActivo == 'divTerapias') {
                if (verificarCamposGuardar('listProcedimientoConNoPOS')) {
                    valores_a_mandar = 'accion=' + arg;
                    //valores_a_mandar=valores_a_mandar+"&idQuery=6&parametros=";
                    valores_a_mandar = valores_a_mandar + "&idQuery=434&parametros=";
                    //add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));						 
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdTerapia'));
                    add_valores_a_mandar(valorAtributo('cmbCantidadTerapia'));
                    add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgicoTerapia'));
                    add_valores_a_mandar(valorAtributo('txtIndicacionTerapia'));
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(valorAtributo('txtIdClaseTerapia'));

                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdTerapia'));
                }
            } else if (tabActivo == 'divLaboratorioClinico') {
                if (verificarCamposGuardar('listProcedimientoConNoPOS')) {
                    valores_a_mandar = 'accion=' + arg;
                    //valores_a_mandar=valores_a_mandar+"&idQuery=6&parametros=";
                    valores_a_mandar = valores_a_mandar + "&idQuery=434&parametros=";
                    //add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));						 
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdLaboratorio'));
                    add_valores_a_mandar(valorAtributo('cmbCantidadLaboratorio'));
                    add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgicoLaboratorio'));
                    add_valores_a_mandar(valorAtributo('txtIndicacionLaboratorio'));
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(valorAtributo('txtIdClaseLaboratorio'));

                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdLaboratorio'));
                }
            }

            add_valores_a_mandar(valorAtributo('txt_Tratamiento'));
            add_valores_a_mandar(valorAtributo('txt_ResumenHC'));
            add_valores_a_mandar(valorAtributo('txt_AlternativaPos'));
            add_valores_a_mandar(valorAtributo('txt_PosibilidadTerapeutica'));
            add_valores_a_mandar(valorAtributo('txt_MotivoUso'));
            add_valores_a_mandar(valorAtributo('txt_DiagnostProcPos'));
            add_valores_a_mandar(valorAtributo('txt_JustificacionPos'));
            add_valores_a_mandar(valorAtributo('txt_pregunta1'));
            add_valores_a_mandar(valorAtributo('txt_pregunta2'));
            ajaxModificar();
            break;
        case 'listProcedimientosModJustificacion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=378&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdPrioridad'));
                add_valores_a_mandar(valorAtributo('cmbIdGuia'));
                add_valores_a_mandar(valorAtributo('txtJustificacion'));
                add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));
                ajaxModificar();
            }
            break;
        case 'listAyudasDiagnosticas':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=183&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('txtIdClaseAD'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoServicioSolicitadoAD'));
                add_valores_a_mandar(valorAtributo('cmbIdPrioridadAD'));
                add_valores_a_mandar(valorAtributo('cmbIdGuiaAD'));
                add_valores_a_mandar(valorAtributo('txtJustificacionAD'));
                ajaxModificar();
            }
            break;
        case 'listAyudasDiagnosticasDetalle':
            //if (verificarCamposGuardar(arg)) {
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=366&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica'));
            add_valores_a_mandar(valorAtributo('cmbCantidadAD'));
            add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgicoAD'));
            add_valores_a_mandar(valorAtributo('txtIndicacionAD'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('txtIdClaseAD'));
            add_valores_a_mandar(valorAtributo('cmbIdDxRelacionadoA'));

            ajaxModificar();
            //}
            break;
        case 'listAyudasDiagnosticasModJustificacion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=378&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdPrioridadAD'));
                add_valores_a_mandar(valorAtributo('cmbIdGuiaAD'));
                add_valores_a_mandar(valorAtributo('txtJustificacionAD'));
                add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));	//valor fijo				 
                ajaxModificar();
            }
            break;
        case 'listMedicacion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=625&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('cmbIdVia'));
                add_valores_a_mandar(valorAtributo('cmbUnidad'));
                add_valores_a_mandar(valorAtributo('cmbCant'));
                add_valores_a_mandar(valorAtributo('cmbFrecuencia'));
                add_valores_a_mandar(valorAtributo('cmbDias'));
                add_valores_a_mandar(valorAtributo('txtIndicaciones'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(valorAtributo('cmbUnidadFarmaceutica'));	/**/
                ajaxModificar();
            }
            break;
        case 'listMedicacionConNoPOS':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=626&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('cmbIdVia'));
                add_valores_a_mandar(valorAtributo('cmbUnidad'));
                add_valores_a_mandar(valorAtributo('cmbCant'));
                add_valores_a_mandar(valorAtributo('cmbFrecuencia'));
                add_valores_a_mandar(valorAtributo('cmbDias'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(valorAtributo('txtIndicaciones'));
                add_valores_a_mandar(valorAtributo('cmbUnidadFarmaceutica'));	/**/

                add_valores_a_mandar(valorAtributo('lblIdDocumento'));		/*PARA AUTORIZACION FORMATO NO POS*/
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txt_Tratamiento'));
                add_valores_a_mandar(valorAtributo('txt_ResumenHC'));
                add_valores_a_mandar(valorAtributo('txt_MedicamentoDelPOS1'));
                add_valores_a_mandar(valorAtributo('txt_PresentacionConcentracion1'));
                add_valores_a_mandar(valorAtributo('txt_DosisFrecuencia1'));
                add_valores_a_mandar(valorAtributo('txt_MedicamentoDelPOS2'));
                add_valores_a_mandar(valorAtributo('txt_PresentacionConcentracion2'));
                add_valores_a_mandar(valorAtributo('txt_DosisFrecuencia2'));
                add_valores_a_mandar(valorAtributo('txt_IndicacionTerapeutica'));
                add_valores_a_mandar(valorAtributo('txt_pregunta1'));
                add_valores_a_mandar(valorAtributo('txt_pregunta2'));
                add_valores_a_mandar(valorAtributo('txt_pregunta3'));
                add_valores_a_mandar(valorAtributo('txt_pregunta4'));
                add_valores_a_mandar(valorAtributo('txt_preguntaTexto5'));
                add_valores_a_mandar(valorAtributo('txt_ExpliquePos'));
                ajaxModificar();
            }
            break;

        case 'listLaboratorios':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=183&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('txtIdClaseLaboratorio'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoServicioSolicitadoLaboratorio'));
                add_valores_a_mandar(valorAtributo('cmbIdPrioridadLaboratorio'));
                add_valores_a_mandar(valorAtributo('cmbIdGuiaLaboratorio'));
                add_valores_a_mandar(valorAtributo('txtJustificacionLaboratorio'));
                ajaxModificar();
            }
            break;
        case 'listLaboratoriosDetalle':
            //if (verificarCamposGuardar(arg)) {

            valores_a_mandar = 'accion=' + arg;
            if (valorAtributoIdAutoCompletar('txtIdLaboratorio') == '000001' || valorAtributoIdAutoCompletar('txtIdLaboratorio') == '000002') {
                valores_a_mandar = valores_a_mandar + "&idQuery=604&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('txtIdClaseLaboratorio'));
                add_valores_a_mandar(valorAtributo('cmbIdDxRelacionadoL'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdLaboratorio'));
            } else {
                valores_a_mandar = valores_a_mandar + "&idQuery=366&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdLaboratorio'));
                add_valores_a_mandar(valorAtributo('cmbCantidadLaboratorio'));
                add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgicoLaboratorio'));
                add_valores_a_mandar(valorAtributo('txtIndicacionLaboratorio'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('txtIdClaseLaboratorio'));
                add_valores_a_mandar(valorAtributo('cmbIdDxRelacionadoL'));
            }
            ajaxModificar();
            //}
            break;
        case 'listLaboratoriosModJustificacion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=378&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdPrioridadLaboratorio'));
                add_valores_a_mandar(valorAtributo('cmbIdGuiaLaboratorio'));
                add_valores_a_mandar(valorAtributo('txtJustificacionLaboratorio'));
                add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));	//valor fijo				 
                ajaxModificar();
            }
            break;
        case 'listTerapia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=183&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('lblIdAdmision'));
                add_valores_a_mandar(valorAtributo('txtIdClaseTerapia'));
                add_valores_a_mandar(valorAtributo('cmbIdTipoServicioSolicitadoTerapia'));
                add_valores_a_mandar(valorAtributo('cmbIdPrioridadTerapia'));
                add_valores_a_mandar(valorAtributo('cmbIdGuiaTerapia'));
                add_valores_a_mandar(valorAtributo('txtJustificacionTerapia'));
                ajaxModificar();
            }
            break;
        case 'listTerapiaDetalle':
            //if (verificarCamposGuardar(arg)) {
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=366&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdTerapia'));
            add_valores_a_mandar(valorAtributo('cmbCantidadTerapia'));
            add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgicoTerapia'));
            add_valores_a_mandar(valorAtributo('txtIndicacionTerapia'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));
            add_valores_a_mandar(valorAtributo('txtIdClaseTerapia'));
            add_valores_a_mandar(valorAtributo('cmbIdDxRelacionadoT'));
            ajaxModificar();
            //}
            break;
        case 'listTerapiaModJustificacion':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=378&parametros=";
                add_valores_a_mandar(valorAtributo('cmbIdPrioridadTerapia'));
                add_valores_a_mandar(valorAtributo('cmbIdGuiaTerapia'));
                add_valores_a_mandar(valorAtributo('txtJustificacionTerapia'));
                add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));	//valor fijo				 
                ajaxModificar();
            }
            break;
        case 'listProcedimientosEliminar':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=191&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));
                ajaxModificar();
            }
            break;
        case 'listProcedimientosDetalleEliminar':



            if (valorAtributo('txtIdEsdadoDocumento') == '1') {
                alert('EL ESTADO DEL FOLIO NO PERMITE ELIMINAR')
            } else if (valorAtributo('txtIdEsdadoDocumento') == '8') {
                alert('EL ESTADO DEL FOLIO NO PERMITE ELIMINAR')
            } else if (valorAtributo('txtIdEsdadoDocumento') == '9') {
                alert('EL ESTADO DEL FOLIO NO PERMITE ELIMINAR')
            } else {

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=192&parametros=";
                add_valores_a_mandar(valorAtributo('lblId'));
                add_valores_a_mandar(valorAtributo('lblId'));
                add_valores_a_mandar(valorAtributo('lblId'));
                ajaxModificar();

            }

            break;
        case 'listAyudasDiagnosticasEliminar':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=191&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));
                ajaxModificar();
            }
            break;

        case 'listLaboratoriosEliminar':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=191&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));
                ajaxModificar();
            }
            break;

        case 'listTerapiaEliminar':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=191&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdProcedimiento'));
                ajaxModificar();
            }
            break;
        case 'eliminarMedicacion':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=351&parametros=";
            add_valores_a_mandar(valorAtributo('lblId'));
            ajaxModificar();
            break;
        case 'eliminarRemision':

            if (verificarCamposGuardar(arg)) {

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=369&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdRemi'));
                ajaxModificar();

            }


            break;

        case 'listRemision':



            if (verificarCamposGuardar(arg)) {

                valores_a_mandar = 'accion=' + arg;

                if (valorAtributo('lblIdAdmision') != "NO_HC") { /* DESDE EVOLUCION DE LA HISTORIA CLINICA*/
                    //valores_a_mandar = valores_a_mandar + "&idQuery=395&parametros=";
                    valores_a_mandar = valores_a_mandar + "&idQuery=692&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbTipoEvento'));
                    add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                } else {
                    //valores_a_mandar = valores_a_mandar + "&idQuery=368&parametros=";
                    valores_a_mandar = valores_a_mandar + "&idQuery=693&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbTipoEvento'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                }

                add_valores_a_mandar(valorAtributo('cmbServicioSolicita'));
                add_valores_a_mandar(valorAtributo('cmbServicioParaCual'));
                add_valores_a_mandar(valorAtributo('cmbPrioridad'));
                add_valores_a_mandar(valorAtributo('cmbNivelRemis'));
                // add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDxRemision'));
                add_valores_a_mandar(valorAtributo('txt_InformacionRemision'));
                add_valores_a_mandar(valorAtributo('cmbIdProfesionalRemision'));

                add_valores_a_mandar(valorAtributo('cmbTipoDocResponsable'));
                add_valores_a_mandar(valorAtributo('txtDocResponsable'));
                add_valores_a_mandar(valorAtributo('txtPrimerApeResponsable'));
                add_valores_a_mandar(valorAtributo('txtSegundoApeResponsable'));
                add_valores_a_mandar(valorAtributo('txtPrimerNomResponsable'));
                add_valores_a_mandar(valorAtributo('txtSegundoNomResponsable'));
                add_valores_a_mandar(valorAtributo('txtTelefonoResponsable'));
                add_valores_a_mandar(valorAtributo('txtDireccionResponsable'));
                add_valores_a_mandar(valorAtributo('txtDeparResponsable'));
                add_valores_a_mandar(valorAtributo('txtMunicResponsable'));

                //preguntas
                add_valores_a_mandar(valorAtributo('cmbVerificacionPaciente'));
                add_valores_a_mandar(valorAtributo('cmbFormatoDiligenciado'));
                add_valores_a_mandar(valorAtributo('cmbResumenHc'));
                add_valores_a_mandar(valorAtributo('cmbDocumentoIdentidad'));
                add_valores_a_mandar(valorAtributo('cmbImagenDiagnostica'));
                add_valores_a_mandar(valorAtributo('cmbPacienteManilla'));

                ajaxModificar();
            }



            break;
        case 'eliminarAntecedenteFarmacologico':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=513&parametros=";
            add_valores_a_mandar(valorAtributo('lblId'));
            add_valores_a_mandar(valorAtributo('lblIdElemento'));
            ajaxModificar();
            break;
        case 'eliminarSistemaListado':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=362&parametros=";
            add_valores_a_mandar(valorAtributo('lblId'));
            add_valores_a_mandar(valorAtributo('lblIdElemento'));
            ajaxModificar();
            break;
        case 'eliminarDxListado':


            //valores_a_mandar = 'accion=' + arg;
            //valores_a_mandar = valores_a_mandar + "&idQuery=350&parametros=";
            //add_valores_a_mandar(valorAtributo('lblId'));
            //add_valores_a_mandar(valorAtributo('lblIdElemento'));
            //ajaxModificar();

            //alert("Prueba de eliminar")

            if (valorAtributo('txtIdEsdadoDocumento') == '1') {
                alert('EL ESTADO DEL FOLIO NO PERMITE ADICIONAR')
            } else if (valorAtributo('txtIdEsdadoDocumento') == '8') {
                alert('EL ESTADO DEL FOLIO NO PERMITE ADICIONAR')
            } else if (valorAtributo('txtIdEsdadoDocumento') == '9') {
                alert('EL ESTADO DEL FOLIO NO PERMITE ADICIONAR')
            } else {

                verificarNotificacionAntesDeGuardar('verificarDxRelacionados');

            }


            break;
        case 'eliminarAntecedentes':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=364&parametros=";
            add_valores_a_mandar(valorAtributo('lblId'));
            ajaxModificar();
            break;
        case 'listAntecedentes':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=349&parametros=";
                add_valores_a_mandar(valorAtributo('cmbAntecedenteGrupo'));
                add_valores_a_mandar(valorAtributo('cmbTiene'));
                add_valores_a_mandar(valorAtributo('cmbControlada'));
                add_valores_a_mandar(valorAtributo('txtAntecedenteObservaciones'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                ajaxModificar();
            }
            break;
        case 'listDiagnosticos':
            if (verificarCamposGuardar(arg)) {

                if (valorAtributo('txtIdEsdadoDocumento') == '1') {
                    alert('EL ESTADO DEL FOLIO NO PERMITE ADICIONAR')
                } else if (valorAtributo('txtIdEsdadoDocumento') == '8') {
                    alert('EL ESTADO DEL FOLIO NO PERMITE ADICIONAR')
                } else if (valorAtributo('txtIdEsdadoDocumento') == '9') {
                    alert('EL ESTADO DEL FOLIO NO PERMITE ADICIONAR')
                } else {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=361&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
                    add_valores_a_mandar(valorAtributo('cmbDxTipo'));
                    add_valores_a_mandar(valorAtributo('cmbDxClase'));
                    add_valores_a_mandar(valorAtributo('cmbIdSitioQuirurgicoDx'));
                    add_valores_a_mandar(valorAtributo('txtDxObservacion'));
                    add_valores_a_mandar(LoginSesion());
                    ajaxModificar();
                }

            }
            break;
        case 'listSistemas':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=179&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('lblIdHc'));
                add_valores_a_mandar(valorAtributo('cmbDxTipo'));
                add_valores_a_mandar(valorAtributo('cmbDxClase'));
                add_valores_a_mandar(valorAtributo('cmbConducta'));
                ajaxModificar();
            }
            break;
        case 'listSignosVitales':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=363&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                if (valorAtributo('lblIdAdmisionAgen') != '' || valorAtributo('lblIdAdmisionAgen') != 'null') {
                    add_valores_a_mandar(valorAtributo('lblIdAdmisionAgen'));
                } else {
                    add_valores_a_mandar('');
                }
                add_valores_a_mandar(valorAtributo('txtTemperatura'));
                add_valores_a_mandar(valorAtributo('txtSistolica'));
                add_valores_a_mandar(valorAtributo('txtDiastolica'));
                add_valores_a_mandar(valorAtributo('txtPulso'));
                add_valores_a_mandar(valorAtributo('txtRespiracion'));
                add_valores_a_mandar(valorAtributo('txtPeso'));
                add_valores_a_mandar(valorAtributo('txtTalla'));
                add_valores_a_mandar(valorAtributo('lblIMC'));
                add_valores_a_mandar(LoginSesion());
                ajaxModificar();
            }
            break;

        case 'guardarNovedadTns':
            if (valorAtributo('cmbIdConcepto') != '' && valorAtributo('cmbIdBodega') == '') {
                if (confirm("Est� seguro de CREAR LA NOVEDAD?, \n La bodega quedar� en vacia y no ser� contabilizada la transacci�n")) {

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=72&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbIdConcepto'));
                    add_valores_a_mandar(valorAtributo('txtObservacionNovedad'));
                    add_valores_a_mandar(valorAtributo('lblIdTransaccion'));
                    ajaxModificar();
                }
            } else
                alert('Debe seleccionar Una Novedad y la bodega debe estar en  Vacia')

            break;
        case 'listCrearBodega':
            if (valorAtributo('txtNombreBodega') != '' && valorAtributo('cmbCentroCosto') != '' && valorAtributo('cmbAfectaInventario') != '' && valorAtributo('cmbPrincipal') != '' && valorAtributo('txtPropietario') != '') {
                if (confirm("Est� seguro de CREAR LA BODEGA")) {

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=70&parametros=";
                    add_valores_a_mandar(valorAtributo('txtNombreBodega'));
                    add_valores_a_mandar(valorAtributo('cmbCentroCosto'));
                    add_valores_a_mandar(valorAtributo('cmbAfectaInventario'));
                    add_valores_a_mandar(valorAtributo('cmbPrincipal'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPropietario'));
                    ajaxModificar();
                }
            }
            break;
        case 'cerrarTransaccionesDocumentoDevoluciones':
            if (verificarCamposGuardar(arg)) {

                if (confirm("Est� seguro de CERRAR EL DOCUMENTO No: " + valorAtributo('lblIdDocumento') + " quedar� de su autor�a?")) {

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=77&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key			
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key		
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key pa cerrar transacciones solicitudes									 						 						 
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key pa cerrar documento									 						 
                    ajaxModificar();
                }
            }
            break;
        case 'cerrarTransaccionesDocumentoInvUD':
            if (verificarCamposGuardar(arg)) {
                if (confirm("Est� seguro de CERRAR EL DOCUMENTO No: " + valorAtributo('lblIdDocumento') + " quedar� de su autor�a?")) {

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=73&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key			
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key		
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key pa cerrar transacciones solicitudes									 						 						 
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key pa cerrar documento									 						 
                    ajaxModificar();
                }

            }
            break;

        case 'cerrarTransaccionesDocumentoInv':
            if (verificarCamposGuardar(arg)) {

                if (confirm("Est� seguro de CERRAR EL DOCUMENTO No: " + valorAtributo('lblIdDocumento') + " quedar� de su autor�a?")) {

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=68&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key			
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key									 
                    ajaxModificar();
                }
            }
            break;

        case 'cerrarTransaccionesDocumentoInvTraslado':
            if (verificarCamposGuardar(arg)) {

                if (confirm("Est� seguro de CERRAR EL DOCUMENTO No: " + valorAtributo('lblIdDocumento') + " quedar� de su autor�a?")) {

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=68&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key			
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key									 
                    ajaxModificar();
                }
            }
            break;

        case 'auditarDocumentoClinico':
            if (verificarCamposGuardar(arg)) {

                if (confirm("Est� seguro de AUDITAR EL DOCUMENTO No: " + valorAtributo('lblIdDocumento') + " quedar� de su autor�a?")) {

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=53&parametros=";
                    add_valores_a_mandar(valorAtributo('txtObservacionAuditoria'));
                    add_valores_a_mandar(valorAtributo('cmbIdEstadoAuditado'));

                    add_valores_a_mandar(valorAtributo('txtLogin'));
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key	
                    ajaxModificar();
                }
            }
            break;
        case 'cerrarDocumentoClinico':
            //  if(verificarCamposGuardar(arg)){						

            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=211&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key									 
            add_valores_a_mandar(valorAtributo('lblIdHc'));

            add_valores_a_mandar(valorAtributo('txtIdUsuario'));
            add_valores_a_mandar(valorAtributo('txtLogin'));
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key		

            /*quitar cuando todos los documentos activos esten en nuevos*/
            //                         add_valores_a_mandar(valorAtributo('lblIdContacto'));
            /*fin quitar cuando todos los documentos activos esten en nuevos*/

            ajaxModificar();
            //		}
            break;
        case 'actualizaInformacionPaciente':
            if (verificarCamposGuardar(arg)) {

                if (confirm("Est� seguro de Modificar la informaci�n, quedar� de su autor�a?")) {

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=229&parametros=";
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtOrigenAtencion'));
                    //                         add_valores_a_mandar(valorAtributo('txtIdUsuario'));
                    //                         add_valores_a_mandar(valorAtributo('txtLogin'));						 

                    add_valores_a_mandar(valorAtributo('lblIdContacto'));
                    ajaxModificar();
                }

            }
            break;
        case 'cerrarDocumento':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=440&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdUsuario'));
            add_valores_a_mandar(valorAtributo('lblIdEspecialidad'));
            add_valores_a_mandar(profesionUsuario());
            add_valores_a_mandar(valorAtributo('lblIdAuxiliar'));		// AUXILIAR DE CONSULTORIO QUE FINALIZA			 
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key	
            ajaxModificar();
            break;
        case 'cerrarFolioJasper':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=440&parametros=";
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdEspecialidad'));
                add_valores_a_mandar(profesionUsuario());
                add_valores_a_mandar(valorAtributo('lblIdAuxiliar'));		// AUXILIAR DE CONSULTORIO QUE FINALIZA
                add_valores_a_mandar(valorAtributo('lblIdFolioJasper'));		// primary key	
                ajaxModificar();
            }
            break;
        case 'cerrarDocumentoSinImp':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=440&parametros=";
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdEspecialidad'));
                add_valores_a_mandar(profesionUsuario());
                add_valores_a_mandar(valorAtributo('lblIdAuxiliar'));		// AUXILIAR DE CONSULTORIO QUE FINALIZA
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key	
                ajaxModificar();
            }
            break;
        case 'cerrarDocumentoSinCambioAutor':
            //   if(verificarCamposGuardar(arg)){									 
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=584&parametros=";
            add_valores_a_mandar(IdSesion());
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key	
            ajaxModificar();
            /// }  
            break;
        case 'finalizaFolioConFecha':
            if (valorAtributo('lblIdEstadoFolio') == '12') {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=594&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));		// primary key	
                ajaxModificar();
            } else
                alert('EL ESTADO DEL FOLIO DEBE SER PRE FINALIZADO')
            break;
        case 'cambioEstadoFolio':
            valores_a_mandar = 'accion=' + arg;
            switch (valorAtributo('cmbIdEstadoFolioEdit')) {
                case '10':
                    valores_a_mandar = valores_a_mandar + "&idQuery=579&parametros=";
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributo('lblIdAuxiliar'));		// AUXILIAR DE CONSULTORIO QUE FINALIZA			 
                    add_valores_a_mandar(valorAtributo('cmbIdEstadoFolioEdit'));
                    add_valores_a_mandar(valorAtributo('cmbIdMotivoEstadoEdit'));
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));

                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributo('cmbIdEstadoFolioEdit'));

                    /**/
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    // add_valores_a_mandar(valorAtributo('lblIdDocumento'));				 			 

                    break;
                default:
                    valores_a_mandar = valores_a_mandar + "&idQuery=475&parametros=";
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributo('lblIdAuxiliar'));		// AUXILIAR DE CONSULTORIO QUE FINALIZA			 
                    add_valores_a_mandar(valorAtributo('cmbIdEstadoFolioEdit'));
                    add_valores_a_mandar(valorAtributo('cmbIdMotivoEstadoEdit'));
                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));

                    add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributo('cmbIdEstadoFolioEdit'));
                    break;
            }
            ajaxModificar();
            break;
        case 'gestionPacienteFolio':
            if (valorAtributo('lblIdDocumento') != '') {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=481&parametros=";
                add_valores_a_mandar(valorAtributo('txt_gestionarPaciente'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                ajaxModificar();
            } else
                alert('DEBE SELECCIONAR UN FOLIO')
            break;
        case 'eliminarMedicamentoListado':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=50&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdOrden'));
            add_valores_a_mandar(valorAtributo('lblIdOrden'));
            add_valores_a_mandar(valorAtributo('lblIdArticulo'));
            ajaxModificar();
            break;
        case 'adicionarSolicitudADocumento':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=236&parametros=";
                //                         add_valores_a_mandar(valorAtributo('lblIdEstadoDocumento'));	
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar(valorAtributo('lblIdPaciente'));

                add_valores_a_mandar(valorAtributo('lblIdOrden'));
                add_valores_a_mandar(valorAtributo('lblFechaProgramada'));
                add_valores_a_mandar(valorAtributo('lblHoraProgramada'));
                ajaxModificar();
            }

            break;
        case 'eliminarInsumoListado':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=51&parametros=";
            //                         add_valores_a_mandar(valorAtributo('lblIdOrden'));	
            add_valores_a_mandar(valorAtributo('lblIdOrden'));
            add_valores_a_mandar(valorAtributo('lblIdArticulo'));
            ajaxModificar();
            break;

        case 'paciente':
            if (verificarCamposGuardar(arg)) {
                if (valorAtributo('lblId') != "") {


                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=328&parametros=";
                    //	   add_valores_a_mandar(valorAtributo('cmbTipoId'));						 
                    //		   add_valores_a_mandar(valorAtributo('txtIdPaciente'));						 
                    add_valores_a_mandar(valorAtributo('txtNombre1'));
                    add_valores_a_mandar(valorAtributo('txtNombre2'));
                    add_valores_a_mandar(valorAtributo('txtApellido1'));
                    add_valores_a_mandar(valorAtributo('txtApellido2'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipioResi'));
                    add_valores_a_mandar(valorAtributo('txtFechaNacimiento'));
                    add_valores_a_mandar(valorAtributo('cmbSexo'));
                    add_valores_a_mandar(valorAtributo('txtDireccion'));
                    add_valores_a_mandar(valorAtributo('txtTelefono'));
                    add_valores_a_mandar(valorAtributo('txtCelular1'));
                    add_valores_a_mandar(valorAtributo('txtCelular2'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                    add_valores_a_mandar(valorAtributo('txtEmail'));
                    add_valores_a_mandar(LoginSesion());
                    add_valores_a_mandar(valorAtributo('lblId'));		// primary key		
                    ajaxModificar();
                } else
                    alert('No se puede modificar porque a�n no ha sido creado');
            }
            break;
        case 'pacienteModifica':
            if (valorAtributo('txtIdBusPaciente') != "") {
                alert('TIPO IDENTIFICACION E IDENTIFICACION NO SE GUARDARAN')
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=666&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                add_valores_a_mandar(valorAtributo('txtTelefonos'));
                add_valores_a_mandar(valorAtributo('txtCelular1'));
                add_valores_a_mandar(valorAtributo('txtCelular2'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(valorAtributo('txtEmail'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));		// primary key		
                ajaxModificar();
            } else
                alert('No se puede modificar porque no se ha seleccionado');
            break;
        case 'pacienteModificaArchivo':
            if (valorAtributo('txtIdBusPaciente') != "") {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=667&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                add_valores_a_mandar(valorAtributo('txtTelefonos'));
                add_valores_a_mandar(valorAtributo('txtCelular1'));
                add_valores_a_mandar(valorAtributo('txtCelular2'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));		// primary key
                add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                ajaxModificar();
            } else
                alert('No se puede modificar porque no se ha seleccionado');
            break;
        case 'crearPacienteNuevo':   /*desde la ventana de paciente*/
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=336&parametros=";
                add_valores_a_mandar(valorAtributo('cmbTipoIdEdit'));
                add_valores_a_mandar(valorAtributo('txtIdPaciente'));
                add_valores_a_mandar(valorAtributo('txtNombre1'));
                add_valores_a_mandar(valorAtributo('txtNombre2'));
                add_valores_a_mandar(valorAtributo('txtApellido1'));
                add_valores_a_mandar(valorAtributo('txtApellido2'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipioResi'));
                add_valores_a_mandar(valorAtributo('txtFechaNacimiento'));
                add_valores_a_mandar(valorAtributo('cmbSexo'));
                add_valores_a_mandar(valorAtributo('txtDireccion'));
                add_valores_a_mandar(valorAtributo('txtTelefono'));
                add_valores_a_mandar(valorAtributo('txtCelular1'));
                add_valores_a_mandar(valorAtributo('txtCelular2'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('txtEmail'));
                ajaxModificar();
            }
            break;
        case 'eliminarPaciente':   /*desde la ventana de paciente*/
            if (confirm('ESTA SEGURO DE ELIMINAR A ESTE PACIENTE ')) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=337&parametros=";
                add_valores_a_mandar(valorAtributo('lblId'));
                ajaxModificar();
            }

            break;
        case 'crearNuevoPaciente':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=311&parametros=";
                add_valores_a_mandar(valorAtributo('cmbTipoId'));
                add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                add_valores_a_mandar(valorAtributo('txtApellido1'));
                add_valores_a_mandar(valorAtributo('txtApellido2'));
                add_valores_a_mandar(valorAtributo('txtNombre1'));
                add_valores_a_mandar(valorAtributo('txtNombre2'));
                add_valores_a_mandar(valorAtributo('txtFechaNac'));
                add_valores_a_mandar(valorAtributo('cmbSexo'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributo('txtDireccionRes'));
                add_valores_a_mandar(valorAtributo('txtNomAcompanante'));
                add_valores_a_mandar(valorAtributo('txtTelefonos'));
                add_valores_a_mandar(valorAtributo('txtCelular1'));
                add_valores_a_mandar(valorAtributo('txtCelular2'));
                add_valores_a_mandar(valorAtributo('txtEmail'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtAdministradora1'));
                ajaxModificar();
            }
            break;
        case 'btn_nueva_Id':
            if (verificarCamposGuardar(arg)) {


                //if (valorAtributo('lblId') != "") {  //esta verficacion ya esta en verificarCamposGuardar
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=329&parametros=";
                add_valores_a_mandar(valorAtributo('cmbTipoIdNueva'));
                add_valores_a_mandar(valorAtributo('txtIdentificacionNueva'));
                add_valores_a_mandar(valorAtributo('lblId')); // id paciente							 


                /*para homologacion*/
                add_valores_a_mandar(valorAtributo('lblId')); // id paciente	
                add_valores_a_mandar(valorAtributo('cmbTipoIdEdit'));
                add_valores_a_mandar(valorAtributo('txtIdPaciente'));
                add_valores_a_mandar(valorAtributo('cmbTipoIdNueva'));
                add_valores_a_mandar(valorAtributo('txtIdentificacionNueva'));
                add_valores_a_mandar(LoginSesion());
                ajaxModificar();
                //} else   alert('No se puede modificar porque aun no ha sido creado');
            }
            break;
        case 'correccion_id_historico':   /* realiza la correccion de la identificacion en historicos */
            if (confirm("Se modificara todos los registros historicos con la nueva identificacion. \n Desea continuar ?")) {
                if (verificarCamposGuardar(arg)) {
                    //if (valorAtributo('lblId') != "") {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=640&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbTipoIdNueva'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacionNueva'));
                    add_valores_a_mandar(valorAtributo('lblId')); // id paciente							 

                    /* para historicos */
                    add_valores_a_mandar(valorAtributo('lblId')); // id paciente							 
                    add_valores_a_mandar(valorAtributo('lblId')); // id paciente							 
                    //add_valores_a_mandar(valorAtributo('cmbTipoIdEdit')+valorAtributo('txtIdPaciente')); // tipoid paciente							 					   


                    /*para homologacion*/
                    add_valores_a_mandar(valorAtributo('lblId')); // id paciente	
                    add_valores_a_mandar(valorAtributo('cmbTipoIdEdit'));
                    add_valores_a_mandar(valorAtributo('txtIdPaciente'));
                    add_valores_a_mandar(valorAtributo('cmbTipoIdNueva'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacionNueva'));
                    add_valores_a_mandar(LoginSesion());
                    ajaxModificar();
                    //} else        alert('No se puede modificar porque a�n no ha sido creado');
                }
            }
            break;
        case 'citaDejarDisponible':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=204&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdCita'));
            if (confirm('ESTA SEGURO DE LA ELIMINAR ESTA CITA, PERO VERIZ BIEN?')) {
                ajaxModificar();
            }
            break;

        /*********************************************************************************************************************************/
        // cases de documentosHistoriaClinica
        /*********************************************************************************************************************************/




    }
}

function respuestaModificarCRUD(arg, xmlraiz) {


    if (xmlraiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {

        switch (arg) {

            case 'eliminarDatosPlantillaEvolucion':

            break;

            case 'eliminarTratamientoPorDiente':
                cargarTratamientoPorDiente()
                break;
            case 'tratamientoPorDiente':
                cargarTratamientoPorDiente()
                break;
            case 'eliminarTratamientoInicial':
                cargarOdontogramaInicial()
            break;
            case 'dejarTratamientoInicial':
                cargarOdontogramaInicial()
            break;
            case 'limpiarDiente':
                cargarOdontograma()
                break;
            case 'guardarDienteYTratamiento':
                //pasarDienGrandeAPequeno()
                cargarOdontograma()
                break;
            case 'crearExamenFisicoDescripcion':

                alert('SE GUARDO EXITOSAMENTE')

                break;


            case 'listProcedimientosDetalleEditar':
                ocultar('divVentanitaEditTratamientos');


                if (tabActivo == 'divProcedimiento')
                    buscarHC('listProcedimientosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                else if (tabActivo == 'divAyudasDiagnosticas')
                    buscarHC('listAyudasDiagnosticasDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                else if (tabActivo == 'divTerapias')
                    buscarHC('listTerapiaDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                else if (tabActivo == 'divLaboratorioClinico')
                    buscarHC('listLaboratoriosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

                break;


            case 'leerNotificacion':
                asignaAtributo('lblIdNotDestino', '', 0);
                asignaAtributo('lblNomEmisor', '', 0);
                asignaAtributo('lblAsunto', '', 0);
                asignaAtributo('lblContenido', '', 0);
                asignaAtributo('lblNomEstado', '', 0);
                buscarNotificacion('listGrillaLeerNotificacion')
                break;
            case 'editaAsistenciaPaciente':
                asignaAtributo('lblIdentificacionEdit', '', 0);
                asignaAtributo('lblIdCitaEdit', '', 0);
                asignaAtributo('lblPacienteEdit', '', 0);
                asignaAtributo('cmbIdPreferencialEdit', 'NO', 0);
                asignaAtributo('cmbIdAsisteEdit', 'NO', 0);
                asignaAtributo('txtObservacionEdit', '', 0);

                asignaAtributo('cmbEstadoCitaEdit', '', 0);
                asignaAtributo('cmbMotivoConsultaEdit', '', 0);



                buscarFacturacion('listGrillaAsistenciaPacientes');
                break;
            case 'eliminarDescuento':
                limpiarDivEditarJuan('DescuentoArticulo');
                buscarSuministros('listDescuentosArticulo');

                break;
            case 'crearDescuento':
                limpiarDivEditarJuan('DescuentoArticulo');
                buscarSuministros('listDescuentosArticulo');

                break;
            case 'traerTrEgresos':
                limpiarDivEditarJuan('clonarDocumentoEgreso');
                buscarSuministros('listGrillaTransaccionDocumento');
                alert('Transacciones creadas.');
                break;
            case 'crearArchivosRips':
                buscarFacturacion('listGrillaRips');
                alert('Se Grabaron los archivos');
                break;
            case 'crearNotificacion':
                limpiarDivEditarJuan('notificacion');
                alert('Creado exitosamente');
                buscarNotificacion('listGrillaNotificaciones');
                break;
            case 'guardarNoLente':
                alert('Numero de lente actualizado');
                break;
            case 'eliminarNotificacionDestino':
                buscarNotificacion('listGrillaNotificacionDestino');
                alert('Eliminado exitosamente');
                limpiarDivEditarJuan('notificacionDestino');
                break;
            case 'crearnotificacionPersona':
                buscarNotificacion('listGrillaNotificacionDestino');
                alert('Creado exitosamente');
                limpiarDivEditarJuan('notificacionDestino');
                break;
            case 'eliminarGrupoUsuario':
                buscarUsuario('listGrillaGrupoUsuarios')
                alert('Eliminado exitosamente');
                limpiarDivEditarJuan('limpiarGrupoUsuario');
                break;
            case 'crearGrupoUsuario':
                buscarUsuario('listGrillaGrupoUsuarios')
                alert('Creado exitosamente');
                limpiarDivEditarJuan('limpiarGrupoUsuario');
                break;
            case 'modificarUsuario':
                buscarUsuario('listGrillaUsuarios')
                alert('Modificado exitosamente');
                limpiarDivEditarJuan('limpiarCrearUsuario');
                break;
            case 'crearUsuario':
                buscarUsuario('listGrillaUsuarios')
                alert('Creado exitosamente');
                limpiarDivEditarJuan('limpiarCrearUsuario');
                break;
            case 'modificaKardex':
                alert('Modificado exitosamente');
                buscarSuministros('listGrillaKardex')
                limpiarDivEditarJuan('Kardex');
                break;
            case 'modificarProfesiAdmision':
                alert('Modificado exitosamente');
                buscarFacturacion('listGrillaFacturas')
                break;
            case 'anularRecibo':
                alert('Anulado exitosamente');
                buscarFacturacion('listGrillaRecibos')
                break;
            case 'crearRecibo':
                buscarFacturacion('listGrillaRecibos')
                break;
            case 'anularFactura':
                alert('FACTURA ANULADA EXISTOSAMENTE');

                limpiaAtributo('lblIdFactura', 0)
                limpiaAtributo('lblNumeroFactura', 0)
                limpiaAtributo('lblIdEstadoFactura', 0)
                limpiaAtributo('lblEstadoFactura', 0)
                limpiaAtributo('lblValorTotalFactura', 0)
                limpiaAtributo('lblIdPaciente', 0)
                limpiaAtributo('lblNomPaciente', 0)
                limpiaAtributo('lblIdUsuario', 0)
                limpiaAtributo('lblNomUsuario', 0)
                limpiaAtributo('txtFechaAdmision', 0)
                limpiaAtributo('cmbIdProfesionales', 0)
                limpiaAtributo('cmbIdMotivoAnulacion', 0)
                limpiaAtributo('cmbIdMotivoAbrir', 0)
                limpiaAtributo('lblIdAdmision', 0)

                buscarFacturacion('listGrillaFacturas')
                break;

            case 'abrirFactura':
                alert('FACTURA ABRIO EXISTOSAMENTE');

                limpiaAtributo('lblIdFactura', 0)
                limpiaAtributo('lblNumeroFactura', 0)
                limpiaAtributo('lblIdEstadoFactura', 0)
                limpiaAtributo('lblEstadoFactura', 0)
                limpiaAtributo('lblValorTotalFactura', 0)
                limpiaAtributo('lblIdPaciente', 0)
                limpiaAtributo('lblNomPaciente', 0)
                limpiaAtributo('lblIdUsuario', 0)
                limpiaAtributo('lblNomUsuario', 0)
                limpiaAtributo('txtFechaAdmision', 0)
                limpiaAtributo('cmbIdProfesionales', 0)
                limpiaAtributo('cmbIdMotivoAnulacion', 0)
                limpiaAtributo('cmbIdMotivoAbrir', 0)
                limpiaAtributo('lblIdAdmision', 0)

                buscarFacturacion('listGrillaFacturas')



                break;


            case 'cerrarFactura':
                alert('FACTURA CERRO EXISTOSAMENTE');

                limpiaAtributo('lblIdFactura', 0)
                limpiaAtributo('lblNumeroFactura', 0)
                limpiaAtributo('lblIdEstadoFactura', 0)
                limpiaAtributo('lblEstadoFactura', 0)
                limpiaAtributo('lblValorTotalFactura', 0)
                limpiaAtributo('lblIdPaciente', 0)
                limpiaAtributo('lblNomPaciente', 0)
                limpiaAtributo('lblIdUsuario', 0)
                limpiaAtributo('lblNomUsuario', 0)
                limpiaAtributo('txtFechaAdmision', 0)
                limpiaAtributo('cmbIdProfesionales', 0)
                limpiaAtributo('cmbIdMotivoAnulacion', 0)
                limpiaAtributo('cmbIdMotivoAbrir', 0)
                limpiaAtributo('lblIdAdmision', 0)

                buscarFacturacion('listGrillaFacturas')



                break;

            case 'modificarFechaAdmision':
                alert('Modificado exitosamente');
                buscarFacturacion('listGrillaFacturas')
                break;
            case 'eliminarProcLiquidacion':
                buscarFacturacion('listLiquidacionQxProcedimientos')
                ocultar('divVentanitaLiquidacionProcedimientos')
                break;
            case 'eliminarProcLiquidacionDerecho':
                buscarFacturacion('listLiquidacionQxProcedimientosDerechos')
                ocultar('divVentanitaLiquidacionProcedimientosDerechos')
                break;
            case 'modificarProcLiquidacion':
                buscarFacturacion('listLiquidacionQxProcedimientos')
                ocultar('divVentanitaLiquidacionProcedimientos')
                break;
            case 'eliminarLiquidacionCirugia':
                limpiarDivEditarJuan('abreVentanitaLiquidacion')
                // limpiarDivEditarJuan('listLiquidacionQx');					
                // limpiarDivEditarJuan('listLiquidacionQxProcedimientos');
                //buscarFacturacion('listLiquidacionQxProcedimientos')
                asignaAtributo('lblIdLiquidacion', '', 0);
                asignaAtributo('cmbIdTipoAnestesia', '', 0);
                break;
            case 'crearLiquidacionCirugia':
                buscarFacturacion('listLiquidacionQx')
                break;
            case 'modificarLiquidacionCirugia':
                buscarFacturacion('listLiquidacionQx')
                break;
            case 'facturarLiquidacionCirugia':
                buscarFacturacion('listProcedimientosDeFactura')

                break;
            case 'crearViaArticulo':
                alert('Creado exitosamente');
                buscarSuministros('listGrillaViaArticulo')
                break;
            case 'eliminarViaArticulo':
                alert('Eliminado exitosamente');
                buscarSuministros('listGrillaViaArticulo')
                break;
            case 'eliminaSGasto':
                buscarSuministros('listGrillaSolicitudGastos')
                alert('Eliminado exitosamente');
                limpiarDivEditarJuan('solicitudesFGastos');
                break;
            case 'modificaSGasto':
                buscarSuministros('listGrillaSolicitudGastos')
                alert('Modificado exitosamente');
                limpiarDivEditarJuan('solicitudesFGastos');
                break;
            case 'crearSGasto':
                buscarSuministros('listGrillaSolicitudGastos')
                alert('Adicionado exitosamente');
                limpiarDivEditarJuan('solicitudesFGastos');
                break;
            case 'eliminaSolicitud':
                alert('Eliminado exitosamente');
                buscarSuministros('listGrillaSolicitudFarmacia')
                limpiarDivEditarJuan('solicitudesFarmacia');
                break;
            case 'modificaSolicitud':
                alert('Modificado exitosamente');
                buscarSuministros('listGrillaSolicitudFarmacia')
                limpiarDivEditarJuan('solicitudesFarmacia');
                break;
            case 'crearSolicitud':
                alert('Adicionado exitosamente');
                buscarSuministros('listGrillaSolicitudFarmacia')
                limpiarDivEditarJuan('solicitudesFarmacia');
                break;
            case 'eliminarTransaccionDevHG':
                alert('Eliminado exitosamente');
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        buscarSuministros('listGrillaTransaccionDocumento')
                        break;
                    case "S":
                        buscarSuministros('listGrillaTransaccionSalida')
                        break;
                }
                limpiarDivEditarJuan('limpCamposTransaccion');
                break;
            case 'eliminaTransaccion':
                alert('Eliminado exitosamente');
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        buscarSuministros('listGrillaTransaccionDocumento')
                        break;
                    case "S":
                        buscarSuministros('listGrillaTransaccionSalida')
                        break;
                }
                limpiarDivEditarJuan('limpCamposTransaccion');
                break;
            case 'reemplazarCodigoBarras':
                alert('Reemplazado exitosamente');
                asignaAtributo('txtCodBarrasBus', '', 0);
                asignaAtributo('txtCodBarrasNuevo', '', 0);
                asignaAtributo('lblIdArticulo', '', 0);
                asignaAtributo('lblValorUnitarioCodBarra', '', 0);
                asignaAtributo('lblSinIvaCodBarra', '', 0);
                asignaAtributo('lblValorIvaCodBarra', '', 0);
                document.getElementById('txtCodBarrasBus').focus()
                break;

            case 'creaSerialesTransaccion':
                alert('Modificado exitosamente');
                buscarSuministros('listGrillaTransaccionDocumento')
                limpiarDivEditarJuan('limpCamposTransaccion');
                break;

            case 'modificaTransaccion':
                alert('Modificado exitosamente');
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        buscarSuministros('listGrillaTransaccionDocumento')
                        break;
                    case "S":
                        buscarSuministros('listGrillaTransaccionSalida')
                        break;
                }
                limpiarDivEditarJuan('limpCamposTransaccion');
                break;

            case 'modificaTransaccionTraslado':
                alert('Modificado exitosamente');
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        buscarSuministros('listGrillaTransaccionDocumento')
                        break;
                    case "S":
                        buscarSuministros('listGrillaTransaccionSalida')
                        break;
                }
                limpiarDivEditarJuan('limpCamposTransaccion');
                break;


            case 'modificaTransaccionOptica':
                alert('Modificado exitosamente');
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        buscarSuministros('listGrillaTransaccionDocumento')
                        break;
                    case "S":
                        buscarSuministros('listGrillaTransaccionSalida')
                        break;
                }
                limpiarDivEditarJuan('limpCamposTransaccion');
                break;
            case 'modificaTransaccionSerial':
                // alert('Modificado exitosamente');
                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        buscarSuministros('listGrillaTransaccionDocumento')
                        break;
                    case "S":
                        buscarSuministros('listGrillaTransaccionSalida')
                        break;
                }
                limpiarDivEditarJuan('limpCamposTransaccion');
                break;

            case 'crearTransaccionDevHG':

                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        buscarSuministros('listGrillaTransaccionDocumento')
                        break;
                    case "S":
                        buscarSuministros('listGrillaTransaccionSalida');
                        setTimeout("buscarSuministros('listGrillaDevolucionHG')", 500);
                        break;
                }
                limpiarDivEditarJuan('limpCamposTransaccion');
                break;
            case 'crearTransaccion':

                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        buscarSuministros('listGrillaTransaccionDocumento')
                        break;
                    case "S":
                        buscarSuministros('listGrillaTransaccionSalida')
                        break;
                }
                limpiarDivEditarJuan('limpCamposTransaccion');
                break;
            case 'crearTransaccion_dos':

                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        buscarSuministros('listGrillaTransaccionDocumento')
                        break;
                    case "S":
                        buscarSuministros('listGrillaTransaccionSalida')
                        break;
                }
                limpiarDivEditarJuan('limpCamposTransaccion_dos');
                break;
            case 'modificarFechaDocumentoInventario':
                alert('Documento Modificado exitosamente');
                buscarSuministros('listGrillaTransaccionDocumentoHistorico');
                break;
            case 'eliminaTransaccionDevolucion':
                alert('Eliminado exitosamente');
                buscarSuministros('listGrillaTransaccionDocumento')
                limpiarDivEditarJuan('limpCamposDevolucionTransaccion');
                setTimeout("buscarSuministros('listGrillaDevoluciones')", 500);
                break;
            case 'modificaTransaccionDevolucion':
                alert('Modificado exitosamente');
                buscarSuministros('listGrillaTransaccionDocumento')
                limpiarDivEditarJuan('limpCamposDevolucionTransaccion');
                setTimeout("buscarSuministros('listGrillaDevoluciones')", 500);
                break;
            case 'crearTransaccionDevolucion':
                alert('Creada exitosamente');
                buscarSuministros('listGrillaTransaccionDocumento')
                limpiarDivEditarJuan('limpCamposDevolucionTransaccion');
                setTimeout("buscarSuministros('listGrillaDevoluciones')", 500);
                break;
            case 'eliminaTransaccionSolicitud':
                buscarSuministros('listGrillaTransaccionSalida')
                alert('Eliminado exitosamente');
                limpiarDivEditarJuan('limpCamposTransaccion');
                if (valorAtributo('cmbIdGastos') == 'S') {
                    setTimeout("buscarSuministros('listGrillaDespachoSGastos')", 500);
                } else {
                    setTimeout("buscarSuministros('listGrillaDespachoSolicitudes')", 500);
                }
                break;
            case 'crearTransaccionSolicitud':
                buscarSuministros('listGrillaTransaccionSalida')
                alert('Creada exitosamente');
                limpiarDivEditarJuan('limpCamposSolicitudTransaccion');
                if (valorAtributo('cmbIdGastos') == 'S') {
                    setTimeout("buscarSuministros('listGrillaDespachoSGastos')", 500);
                } else {
                    setTimeout("buscarSuministros('listGrillaDespachoSolicitudes')", 500);
                }
                break;

            case 'modificaTransaccionSolicitud':
                buscarSuministros('listGrillaTransaccionSalida')
                alert('Modificado exitosamente');
                limpiarDivEditarJuan('limpCamposTransaccion');
                if (valorAtributo('cmbIdGastos') == 'S') {
                    setTimeout("buscarSuministros('listGrillaDespachoSGastos')", 500);
                } else {
                    setTimeout("buscarSuministros('listGrillaDespachoSolicitudes')", 500);
                }
                break;
            case 'cerrarDocumentoBodegaSalida':
                alert('El documento se ha modificado.Por favor Revice el estado del documento ');
                buscarSuministros('listGrillaDocumentosBodega')
                limpiarDivEditarJuan('limpCamposDocumento');
                break;
            case 'cerrarDocumentoBodegaConsumo':
                asignaAtributo('lblIdEstado', '1', 0);
                alert('Finalizado exitosamente');
                buscarSuministros('listGrillaDocumentosBodegaConsumo')
                //limpiarDivEditarJuan('limpCamposDocumentoDevolucion');
                break;
            case 'crearTransaccionDevolucionCompra':

                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        buscarSuministros('listGrillaTransaccionDocumento')
                        break;
                    case "S":
                        buscarSuministros('listGrillaTransaccionSalida')
                        break;
                }
                limpiarDivEditarJuan('limpCamposTransaccionDevolucionCompra');
                break;
            case 'cerrarDocumentoDevolucion':
                asignaAtributo('lblIdEstado', '1', 0);
                alert('Finalizado exitosamente');
                buscarSuministros('listGrillaDocumentosBodegaConsumo')
                //limpiarDivEditarJuan('limpCamposDocumentoDevolucion');
                break;
            case 'cerrarDocumentoBodega':

                alert('Finalizado exitosamente');
                buscarSuministros('listGrillaDocumentosBodega')
                limpiarDivEditarJuan('limpCamposDocumento');
                setTimeout("buscarSuministros('listGrillaTransaccionDocumento')", 500);
                break;
            case 'modificaDocumento':
                alert('Modificado exitosamente');
                buscarSuministros('listGrillaDocumentosBodega')
                limpiarDivEditarJuan('limpCamposDocumento');
                break;
            case 'modificaDocumentoFecha':
                buscarSuministros('listGrillaDocumentosBodega')
                limpiarDivEditarJuan('limpCamposDocumento');
                break;
            case 'cerrarDocumento':
                alert('Cerrado exitosamente');
                buscarSuministros('listGrillaDocumentosBodega')
                limpiarDivEditarJuan('limpCamposDocumento');
                break;
            case 'crearDocumento':
                alert('Creado exitosamente');
                buscarSuministros('listGrillaDocumentosBodega')
                limpiarDivEditarJuan('limpCamposDocumento');
                break;
            case 'crearBodega':
                alert('Creado exitosamente');
                buscarSuministros('listGrillaBodega')
                limpiarDivEditarJuan('limpCamposBodega');
                break;
            case 'modificaBodega':
                alert('Modificado exitosamente');
                buscarSuministros('listGrillaBodega')
                limpiarDivEditarJuan('limpCamposBodega');
                break;
            case 'eliminarMHHListado':
                buscarHC('listMonitorizacion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                limpiarDivEditarJuan('monitorizacionHH')
                ocultar('divVentanitaMHH')
                break;
            case 'crearMHH':
                buscarHC('listMonitorizacion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                limpiarDivEditarJuan('monitorizacionHH')

                break;
            case 'modificarListHojaGasto':
                buscarHC('listHojaGastos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                limpiarDivEditarJuan('hojaGastos')
                ocultar('divVentanitaHojaGastos')
                break;
            case 'eliminarUltimoConsumo':
                buscarHC('listHojaGastos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                setTimeout("buscarSuministros('listGrillaArticulosBodegaConsumo' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 500);
                limpiarDivEditarJuan('hojaGastos')
                ocultar('divVentanitaHojaGastos')
                break;
            case 'eliminarListHojaGasto':
                buscarHC('listHojaGastos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                setTimeout("buscarHC('listHojaGastosDespachos' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 500);
                setTimeout("buscarSuministros('listGrillaArticulosBodegaConsumo')", 700);
                limpiarDivEditarJuan('hojaGastos')
                ocultar('divVentanitaHojaGastos')
                break;
            case 'traerArticulo':
                buscarHC('listHojaGastos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                setTimeout("buscarHC('listHojaGastosDespachos' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 500);
                setTimeout("buscarSuministros('listGrillaArticulosBodegaConsumo')", 700);
                limpiarDivEditarJuan('hojaGastos')
                ocultar('divVentanitaTraerArticulo');
                break;
            case 'traerUltimoArticulo':
                buscarHC('listHojaGastos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                setTimeout("buscarHC('listHojaGastosDespachos' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 500);
                setTimeout("buscarSuministros('listGrillaArticulosBodegaConsumo')", 700);
                limpiarDivEditarJuan('hojaGastos')
                ocultar('divVentanitaTraerArticulo');
                break;

            case 'crearHojaGasto':
                buscarHC('listHojaGastos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                //alert ('Adicion Exitosamente')	;		   
                limpiarDivEditarJuan('hojaGastos')
                break;
            case 'eliminarArticuloCanasta':
                buscarSuministros('listGrillaCanastaDetalle');
                //alert ('Adicion Exitosamente')	;		   
                limpiarDivEditarJuan('plantillaCanastaDetalle')
                break;
            case 'adicionarArticuloCanasta':
                buscarSuministros('listGrillaCanastaDetalle');
                //alert ('Adicion Exitosamente')	;		   
                limpiarDivEditarJuan('plantillaCanastaDetalle')
                break;
            case 'crearCanastaTransaccion':
                buscarSuministros('listGrillaCanastaDetalleProgramacion');
                alert('Elementos adicionados Exitosamente');
                buscarSuministros('listGrillaTransaccionSalida');
                break;

            case 'crearCanastaTrasladoBodega':
                buscarSuministros('listGrillaCanastaTrasladoBodega');
                alert('Elementos adicionados Exitosamente');
                buscarSuministros('listGrillaTransaccionDocumento');
                break;


            case 'modificarPlantillaCanasta':
                buscarSuministros('listGrillaPlantillaCanasta');
                //alert ('Adicion Exitosamente')	;		   
                limpiarDivEditarJuan('plantillaCanasta')
                break;

            case 'crearPlantillaCanasta':
                buscarSuministros('listGrillaPlantillaCanasta');
                //alert ('Adicion Exitosamente')	;		   
                limpiarDivEditarJuan('plantillaCanasta')
                break;
            case 'eliminarAdmiMedicacion':
                buscarHC('listAdministracionMedicacion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                //alert ('Adicion Exitosamente')	;
                ocultar('divVentanitaMedicacion')
                limpiarDivEditarJuan('AdmiMedicamento')
                break;
            case 'AdicionarAdmMedicamentos':
                buscarHC('listAdministracionMedicacion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                //alert ('Adicion Exitosamente')	;

                limpiarDivEditarJuan('AdmiMedicamento')
                break;

            case 'modificarPlantilla':

                buscarHistoria('listGrillaPlantilla');
                alert('Modificado Exitosamente');

                //buscarHistoria('listGrillaPlantilla');
                limpiarDivEditarJuan('plantillaFormulario')
                break;
            case 'crearPlantilla':
                limpiarDivEditarJuan('plantillaFormulario')
                buscarHistoria('listGrillaPlantilla');
                alert('Creado Exitosamente');
                break;

            case 'crearPlantillaElemento':
                buscarHistoria('listGrillaPlantillaElemento');
                //alert('CREADO EXITOSAMENTE');
                limpiaAtributo('txtDescripcion',0);
                limpiaAtributo('txtEtiqueta',0);
                limpiaAtributo('txtReferencia',0);
                limpiaAtributo('lblExamen',0);
                limpiaAtributo('lblOrden',0);
                limpiaAtributo('txtOrdenPadre', 0);

                break;

                case 'modificarPlantillaElemento':
                buscarHistoria('listGrillaPlantillaElemento');
                limpiaAtributo('cmbExamen', 0);
                limpiaAtributo('txtOrden', 0);
                limpiaAtributo('txtDescripcion', 0);
                limpiaAtributo('txtEtiqueta', 0);
                limpiaAtributo('txtReferencia', 0);
                limpiaAtributo('txtOrdenPadre', 0);

                break;

            case 'crearPlantillaEvolucion':
                buscarHistoria('listGrillaExamen');
                ocultar('divVentanitaExamen');
                break;

            case 'modificarPlantillaEvolucion':
                buscarHistoria('listGrillaExamen');
                ocultar('divVentanitaExamen');
                break;

            case 'entregarEvento':
                limpiarDivEditarJuan('GestionEvento')
                buscarHistoria('GestionEvento');
                alert('EVENTO ENTREGADO');
                break;
            case 'aplazarEvento':
                limpiarDivEditarJuan('GestionEvento')
                buscarHistoria('GestionEvento');
                alert('EVENTO APLAZADO');
                break;
            case 'priorizarEvento':
                limpiarDivEditarJuan('GestionEvento')
                buscarHistoria('GestionEvento');
                alert('EVENTO PRIORIZADO');
                break;
            case 'anuladoEvento':
                limpiarDivEditarJuan('GestionEvento')
                buscarHistoria('GestionEvento');
                alert('EVENTO ANULADO');
                break;
            case 'asociarPacienteEA':
                //           buscarEvento('listEspinaPescado' )
                alert('PACIENTE MODIFICADO ASOCIADO AL EVENTO SATISFACTORIAMENTE')
                asignaAtributo('lblIdPaciente', valorAtributoIdAutoCompletar('txtIdBusPaciente'))
                break;
            case 'adicionarEspinaPescado':
                buscarEvento('listEspinaPescado')
                break;
            case 'eliminarEspinaPescado':
                buscarEvento('listEspinaPescado')
                break;
            case 'listAnalisisEA':
                buscarEvento('listAnalisisEA')
                break;
            case 'eliminaAnalisisEA':
                buscarEvento('listAnalisisEA')
                break;
            case 'listClasificarEA':
                buscarEvento('listClasificarEA')
                break;
            case 'eliminarClasificarEA':
                buscarEvento('listClasificarEA')
                break;
            case 'adicionarTarea':
                buscarEvento('listPlanAccion')
                break;
            case 'cambiarEstadoTarea':
                ocultar('divVentanitaPlanAccion')
                buscarEvento('listPlanAccion')
                break;

            case 'guardarSeguimientoTarea':
                ocultar('divVentanitaPlanAccion')
                buscarEvento('listPlanAccion')
                break;

            case 'eliminarTarea':
                ocultar('divVentanitaPlanAccion')
                buscarEvento('listPlanAccion')
                break;
            case 'crearEvento':
                limpiarDivEditarJuan('evento')
                buscarHistoria('evento');
                alert('CREADO EXITOSAMENTECC');
                break;
            case 'crearEventoEnHc':
                buscarEvento('listEventoEnHc')
                break;
            case 'relacionarEvento':
                buscarEvento('listEventoRelacionado')
                limpiaAtributo('txtIdEventoARelacionar', 0)
                break;
            case 'eliminarProcedimientoCuenta':

                buscarFacturacion('listProcedimientosDeFactura')
                limpiaAtributo('txtIdProcedimientoCex', 0)
                limpiaAtributo('lblValorUnitarioProc', 0)
                limpiaAtributo('cmbCantidad', 0)
                ocultar('divVentanitaProcedimCuenta')

                /* if(valorAtributo('lblIdEstado') == ''){
                     setTimeout("guardarYtraerDatoAlListado('consultarValoresCuenta')",200);
                 }else{*/
                guardarYtraerDatoAlListado('validaNumeracionFactura');
                //}

                break;
            case 'adicionaDescuentoFactura':
                ocultar('divVentanitaProcedimTotal')
                limpiaAtributo('cmbPorcentajeDescuentoCuenta', 0)
                limpiaAtributo('txtValorDescuentoCuenta', 0)
                limpiaAtributo('cmbIdQuienAutoriza', 0)
                limpiaAtributo('cmbIdMotivoAutoriza', 0)
                limpiaAtributo('txtObservacionDescuento', 0)
                buscarFacturacion('listProcedimientosDeFactura')
                /*if(valorAtributo('lblIdEstado') == ''){
                	setTimeout("guardarYtraerDatoAlListado('consultarValoresCuenta')",200);
                }else{*/
                guardarYtraerDatoAlListado('validaNumeracionFactura')
                //}
                break;


            case 'revertirDescuentoFactura':
                ocultar('divVentanitaProcedimTotal')
                limpiaAtributo('cmbPorcentajeDescuentoCuenta', 0)
                limpiaAtributo('txtValorDescuentoCuenta', 0)
                limpiaAtributo('cmbIdQuienAutoriza', 0)
                limpiaAtributo('cmbIdMotivoAutoriza', 0)
                limpiaAtributo('txtObservacionDescuento', 0)
                buscarFacturacion('listProcedimientosDeFactura')
                /*if(valorAtributo('lblIdEstado') == ''){
                	setTimeout("guardarYtraerDatoAlListado('consultarValoresCuenta')",200);
                }else{*/
                guardarYtraerDatoAlListado('validaNumeracionFactura')
                //}
                break;


            /* case 'revertirDescuentoCuentaAutomatico':
                 console.log('Revirtio descuento automatico.')
             break;
             */

            case 'valorMaximoEvento':
                ocultar('divVentanitaProcedimTotal')
                buscarFacturacion('listProcedimientosDeFactura')
                break;
            /*case 'generarFactura':
                buscarFacturacion('listProcedimientosDeFactura')
                //asignaAtributo('lblIdEstadoCuenta', 'C', 0);
                //asignaAtributo('lblNomEstadoCuenta', 'Contabilizada', 0);
                break;*/


            case 'actualizarFacturaGenerada':
                buscarFacturacion('listProcedimientosDeFactura')
                break;

            case 'ajustarMaximoCuenta':
            	/*if(valorAtributo('lblIdEstado') == ''){
					modificarCRUD('generarFactura');	
				} 
				else if (valorAtributo('lblIdEstado') == 'P' || valorAtributo('lblIdEstado') == 'T'){*/
                modificarCRUD('actualizarFacturaGenerada');
                //}
                break;

            case 'ajustarCuentaAnual':
            	/*if(valorAtributo('lblIdEstado') == ''){
					modificarCRUD('generarFactura');	
				} 
				else if (valorAtributo('lblIdEstado') == 'P' || valorAtributo('lblIdEstado') == 'T'){*/
                modificarCRUD('actualizarFacturaGenerada');
                //}

                break;

            case 'adicionarProcedimientosDeFactura':
                limpiaAtributo('txtIdProcedimientoCex', 0)
                limpiaAtributo('cmbCantidad', 0)
                limpiaAtributo('lblValorUnitarioProc', 0)
                buscarFacturacion('listProcedimientosDeFactura')

                /*if(valorAtributo('lblIdEstado') == ''){
                	setTimeout("guardarYtraerDatoAlListado('consultarValoresCuenta')",200);
                }else{*/
                guardarYtraerDatoAlListado('validaNumeracionFactura')
                //}


                break;
            case 'crearArticulo':
                alert('Creado exitosamente');
                limpiarDivEditarJuan('articulo');
                buscarSuministros('listGrillaElemento3')
                break;
            case 'modificaArticulo':
                alert('Modificado exitosamente');
                limpiarDivEditarJuan('articulo');
                buscarSuministros('listGrillaElemento3')
                break;
            case 'traerTarifarioPlanContratacion':
                buscarFacturacion('listGrillaPlanesDetalle')
                alert('Se ha traido el tarifario exitosamente');
                break;
            case 'EliminarPlanContratacion':
                limpiarDivEditarJuan('planContratacion')
                buscarFacturacion('listGrillaPlanes')
                alert('Eliminado exitosamente');
                break;
            case 'modificaPlanContratacion':
                limpiarDivEditarJuan('planContratacion')
                buscarFacturacion('listGrillaPlanes')
                alert('Moificado exitosamente');
                break;
            case 'crearPlanContratacion':
                limpiarDivEditarJuan('planContratacion')
                buscarFacturacion('listGrillaPlanes')
                alert('Creado exitosamente');
                break;

            case 'adicionarRangoPlan':
                buscarFacturacion('listGrillaRangos')
                break;
            case 'eliminaRangoPlan':
                buscarFacturacion('listGrillaRangos')
                break;
            case 'modificaRangoPlan':
                buscarFacturacion('listGrillaRangos')
                break;
            case 'adicionarMedicamentoPlan':
                buscarFacturacion('listGrillaPlanesMedicamentos')
                limpiaAtributo('txtIdArticulo', 0)
                limpiaAtributo('txtIdPrecioArticulo', 0)
                limpiaAtributo('txtIdProcedimientoPlan', 0)
                break;
            case 'modificarMedicamentoPlan':
                buscarFacturacion('listGrillaPlanesMedicamentos')
                limpiaAtributo('txtIdArticulo', 0)
                limpiaAtributo('txtIdPrecioArticulo', 0)
                limpiaAtributo('txtIdProcedimientoPlan', 0)
                break;
            case 'eliminarMedicamentoPlan':
                buscarFacturacion('listGrillaPlanesMedicamentos')
                limpiaAtributo('txtIdArticulo', 0)
                limpiaAtributo('txtIdPrecioArticulo', 0)
                limpiaAtributo('txtIdProcedimientoPlan', 0)
                break;
            case 'adicionarProcedimientoPlan':
                limpiarDivEditarJuan('limpiarProcedimientoPlan')
                buscarFacturacion('listGrillaPlanesDetalle')
                break;
            case 'modificarProcedimientoPlan':
                limpiarDivEditarJuan('limpiarProcedimientoPlan')
                buscarFacturacion('listGrillaPlanesDetalle')
                break;
            case 'eliminarProcedimientoPlan':
                limpiarDivEditarJuan('limpiarProcedimientoPlan')
                buscarFacturacion('listGrillaPlanesDetalle')
                break;
            case 'eliminaEquivalenciaCargoTarifario':
                buscarFacturacion('listGrillaTarifarioEquivalencia')
                ocultar('divVentanitaEquivalencias')

                break;
            case 'eliminaCargoTarifario':
                limpiarDivEditarJuan('tarifarioDetalles')
                buscarFacturacion('listGrillaTarifarioDetalle')
                alert('ELIMINADO EXITOSAMENTE !!!')
                break;
            case 'modificaCargoTarifario':
                limpiarDivEditarJuan('tarifarioDetalles')
                buscarFacturacion('listGrillaTarifarioDetalle')
                alert('MODIFICADO EXITOSAMENTE !!!')
                break;
            case 'crearCargoTarifario':
                limpiarDivEditarJuan('tarifarioDetalles')
                buscarFacturacion('listGrillaTarifarioDetalle')
                alert('CREADO EXITOSAMENTE !!!')
                break;

                case 'crearArticuloTemporal':
                ocultar('divVentanitaActivarMedicacion');
                limpiarDivEditarJuan('articuloTemporal');
            break;

                
            case 'listGrillaTarifarioEquivalencia':
                limpiaAtributo('txtIdEquivalenciaCups', 0)
                buscarFacturacion('listGrillaTarifarioEquivalencia')
                break;
            case 'activarCIE':
                alert('DIAGNOSTICO MODIFICADO EXITOSAMENTE');
                buscarHistoria('listGrillaDiagnostico');
                break;
            case 'editarPlantillaProcedimientoNoPos':
                alert('PROCEDIMIENTO NO POS MODIFICADO EXITOSAMENTE');
                limpiaAtributo('lblId', 0);
                limpiaAtributo('lblNombre', 0);
                limpiaAtributo('txtResumenHC', 0);
                limpiaAtributo('txtAlternativaPos', 0);
                limpiaAtributo('txtPosibilidadTerapeutica', 0);
                limpiaAtributo('txtMotivoUso', 0);
                limpiaAtributo('txtDx', 0);
                limpiaAtributo('txtJustificacion', 0);
                buscarHistoria('listGrillaElemento');
                break;
            case 'editarPlantillaMedicamentoNoPos':
                alert('MEDICAMENTO NO POS MODIFICADO EXITOSAMENTE');
                limpiaAtributo('lblId', 0);
                limpiaAtributo('lblNombre', 0);
                limpiaAtributo('txtResumenEnf', 0);
                limpiaAtributo('txtUtilizado', 0);
                limpiaAtributo('txtPresentacion', 0);
                limpiaAtributo('txtDosis', 0);
                limpiaAtributo('txtIndicacion', 0);
                limpiaAtributo('txtExplique', 0);
                buscarHistoria('listGrillaElemento2');
                break;

            case 'editarAnexoPDF1':
                alert('ANEXO..PDF MODIFICADO EXITOSAMENTE');
                imprimirAnexoNoPos();
                break;
            case 'editarAnexoPDF2':
                alert('INDICACION TERAPEUTICA MODIFICADA EXITOSAMENTE');
                imprimirAnexoNoPos();
                break;
            case 'editarAnexoPDF3':
                alert('ANEXO..PDF MODIFICADO EXITOSAMENTE');
                imprimirAnexoNoPos();
                break;

            case 'cerrarDocumento':
                valorAtributo('txtIdEsdadoDocumento', 1)
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                //alert('FINALIZADO EXITOSAMENTE')
                imprimirHCPdf()

                break;
            case 'cerrarFolioJasper':
                asignaAtributo('txtIdEsdadoDocumento', '1')
                asignaAtributo('lblNomEstadoDocumento', 'FINALIZADO')
                alert('Folio Finalizado Exitosamente !!!')
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;

            case 'cerrarDocumentoSinImp':
                asignaAtributo('txtIdEsdadoDocumento', '1')
                asignaAtributo('lblNomEstadoDocumento', 'FINALIZADO')
                alert('Documento Finalizado Exitosamente !!!')
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;

            case 'finalizaFolioConFecha':
                buscarHC('listDocumentosEvolucion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
                break;
            case 'cerrarDocumentoSinCambioAutor':
                asignaAtributo('txtIdEsdadoDocumento', '1')
                asignaAtributo('lblNomEstadoDocumento', 'FINALIZADO')
                alert('Documento Finalizado Exitosamente !!!')
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'soloFinalizarDocum':
                valorAtributo('txtIdEsdadoDocumento', 1)
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'cambioEstadoFolio':
                asignaAtributo('txtIdEsdadoDocumento', valorAtributo('cmbIdEstadoFolioEdit'))
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'gestionPacienteFolio':
                alert('FOLIO GESTIONADO EXITOSAMENTE !!')
                BuscarYcambiarIdQueryPorServicio()

                break;
            case 'modificarObservacionListaEspera':
                ocultar('divVentanitaEliminarListaEspera')
                buscarAGENDA('listHistoricoListaEspera')
                break;
            case 'eliminarListCirugiaProcedimientos':
                buscarAGENDA('listCitaCirugiaProcedimiento')
                break;
            case 'modificarListCirugiaProcedimientos':
                ocultar('divVentanitaEliminarCirugiaProcedimientos')
                buscarAGENDA('listCitaCirugiaProcedimiento')
                break;
            case 'adicionarCirugiaProcedimientosACitaDesdeLETraer':
                alert('LOS PROCEDIMIENTOS SE TRAERAN DE LA LISTA DE ESPERA')
                buscarAGENDA('listCitaCirugiaProcedimiento')
                break;
            /*		  case 'traerProcediDeListaEsperaACirugia':		  
             buscarAGENDA('listCitaCirugiaProcedimiento')
             limpiaAtributo('cmbIdSitioQuirurgico',0);	
             limpiaAtributo('txtIdProcedimiento',0);
             limpiaAtributo('txtObservacionProcedimientoCirugia',0);
             break;		*/
            case 'adicionarCirugiaProcedimientos':
                buscarAGENDA('listCitaCirugiaProcedimiento')
                limpiaAtributo('cmbIdSitioQuirurgico', 0);
                limpiaAtributo('txtIdProcedimiento', 0);
                limpiaAtributo('txtObservacionProcedimientoCirugia', 0);
                break;
            case 'eliminarListaEsperaProcedimientos':
                buscarAGENDA('listListaEsperaProcedimiento')
                ocultar('divVentanitaListaEsperaProcedimientos')
                break;
            case 'adicionarListaEsperaProcedimientos':
                buscarAGENDA('listListaEsperaProcedimiento')
                break;
            case 'adicionarHcCirugiaProcedimientosPos':
                buscarHistoria('listPosOperatorio')
                break;
            case 'adicionarPersonalProc':
                ocultar('adicionarPersonalProc')
                buscarHistoria('listPersonalProc')
                break;
            case 'listPersonalProcEliminar':
                ocultar('divVentanitaPersonalProc')
                buscarHistoria('listPersonalProc')
                break;
            case 'traerHcCirugiaProcedimientosPreAPos':
                ocultar('divVentanitaPreO')
                alert('PROCEDIMIENTO COPIADO A POS OPERATORIO EXITOSAMENTE')
                break;
            case 'eliminaListaEspera':
                buscarAGENDA('listHistoricoListaEspera')
                break;
            case 'modificarListaEspera':
                buscarAGENDA('listListaEsperaTraer')
                break;
            case 'crearListaEspera':
                buscarAGENDA('listHistoricoListaEspera');
                limpiaAtributo('cmbSede', 0);
                break;
            case 'crearListaEsperaCirugia':
                buscarAGENDA('listHistoricoListaEspera');
                limpiaAtributo('cmbSede', 0);
                break;
            case 'crearListaEsperaCirugiaProgramacion':
                buscarAGENDA('listHistoricoListaEspera');
                break;
            case 'crearNuevoPaciente':
                limpiaAtributo('cmbTipoId', 0);
                limpiaAtributo('txtIdentificacion', 0);
                limpiaAtributo('txtApellido1', 0);
                limpiaAtributo('txtApellido2', 0);
                limpiaAtributo('txtNombre1', 0);
                limpiaAtributo('txtNombre2', 0);
                limpiaAtributo('txtIdBusPaciente', 0);

                limpiaAtributo('txtFechaNac', 0);
                limpiaAtributo('cmbSexo', 0);
                limpiaAtributo('txtMunicipio', 0);
                limpiaAtributo('txtDireccionRes', 0);
                limpiaAtributo('txtTelefonos', 0);
                limpiaAtributo('txtCelular1', 0);
                limpiaAtributo('txtCelular2', 0);
                limpiaAtributo('txtNomAcompanante', 0);
                limpiaAtributo('txtAdministradora1', 0);
                limpiaAtributo('txtEmail', 0);
                alert('PACIENTE CREADO EXITOSAMENTE')
                break;
            case 'modificarEstadoCita':

                alert("admision: " + valorAtributo('txtSoloAdmision'));

                if (valorAtributo('txtSoloAdmision') == 'SI') {
                    buscarAGENDA('listAgendaAdmision');
                    ocultar('divVentanitaEditarAgenda');
                } else {


                    asignaAtributo('lblIdentificacionEdit', '', 0);
                    asignaAtributo('lblIdCitaEdit', '', 0);
                    asignaAtributo('lblPacienteEdit', '', 0);
                    asignaAtributo('cmbIdPreferencialEdit', 'NO', 0);
                    asignaAtributo('cmbIdAsisteEdit', 'NO', 0);
                    asignaAtributo('txtObservacionEdit', '', 0);



                    asignaAtributo('cmbEstadoCitaEdit', '', 0);
                    asignaAtributo('cmbMotivoConsultaEdit', '', 0);
                    buscarFacturacion('listGrillaAsistenciaPacientes');
                }

                break;

            case 'procedimientosGestionEdit_2':
                buscarHC('listProcedimientosOrdenes', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'procedimientosGestionEdit':
                ocultar('divVentanitaEditGestionProcedimientos')
                buscarHistoria('listGrillaProcedimientoGestion')
                break;
            case 'procedimientosGestionEditListado':
                ocultar('divVentanitaEditGestionProcedimientos')
                alert("Listado modificado exitosamente");
                buscarHistoria('listGrillaProcedimientoGestion')
                break;
            case 'procedimientoAdd':
                limpiaAtributo('txtId', 0);
                limpiaAtributo('txtNombre', 0);
                limpiaAtributo('cmbIdTipoRips', 0);
                limpiaAtributo('cmbIdCentroCosto', 0);
                limpiaAtributo('cmbNivel', 0);
                limpiaAtributo('txtCups', 0);
                limpiaAtributo('txtCodContable', 0);
                limpiaAtributo('txtUsuarioElaboro', 0);
                limpiaAtributo('cmbClase', 0);
                limpiaAtributo('cmbTipoServicio', 0);
                limpiaAtributo('cmbTipo', 0);
                limpiaAtributo('cmbVigente', 0);

                buscarFacturacion('listGrillaProcedimiento')
                alert('Adicionado exitosamente');
                break;
            case 'procedimientosEdit':
                limpiaAtributo('txtId', 0);
                limpiaAtributo('txtNombre', 0);
                limpiaAtributo('cmbIdTipoRips', 0);
                limpiaAtributo('cmbIdCentroCosto', 0);
                limpiaAtributo('cmbNivel', 0);
                limpiaAtributo('txtCups', 0);
                limpiaAtributo('txtCodContable', 0);
                limpiaAtributo('txtUsuarioElaboro', 0);
                limpiaAtributo('cmbClase', 0);
                limpiaAtributo('cmbTipoServicio', 0);
                limpiaAtributo('cmbTipo', 0);
                limpiaAtributo('cmbVigente', 0);

                buscarFacturacion('listGrillaProcedimiento')
                alert('Modificado exitosamente');
                break;
            case 'motivoEnfermedad':
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'eliminarAdmision':
                buscarAGENDA('listAdmision');
                ocultar('divVentanitaEditarAdmision')
                break;

            case 'modificarAdmisionCama':
                buscarAGENDA('listUbicacionCama');
                ocultar('divVentanitaAgenda')
                break;
            case 'modificarAdmision':
                buscarAGENDA('listAdmision');
                ocultar('divVentanitaEditarAdmision')
                break;
            case 'crearAdmisionCuentaSinCita':
                buscarAGENDA('listAdmisionCuenta')
                break;
            case 'crearAdmisionCuenta':
                buscarAGENDA('listAdmisionCuenta')
                break;
            case 'modificarAdmisionCuenta':
                buscarAGENDA('listAdmisionCuenta');
                limpiarListadosTotales('listCitaCirugiaProcedimiento')
                limpiarListadosTotales('listProcedimientosDeFactura');


                limpiaAtributo('lblIdAdmision', 0);
                limpiaAtributo('lblIdTipoAdmision', 0);
                limpiaAtributo('lblNomTipoAdmision', 0);
                limpiaAtributo('lblIdEpsPlan', 0);
                limpiaAtributo('lblIdPlan', 0);
                limpiaAtributo('lblNomPlan', 0);
                limpiaAtributo('lblIdRegimen', 0);
                limpiaAtributo('lblNomRegimen', 0);
                limpiaAtributo('lblIdTipoAfiliado', 0);
                limpiaAtributo('lblNomTipoAfiliado', 0);

                limpiaAtributo('lblIdRango', 0);
                limpiaAtributo('lblNomRango', 0);
                limpiaAtributo('lblUsuarioCrea', 0);


                limpiaAtributo('txtIdProcedimientoCex', 0);
                limpiaAtributo('lblValorUnitarioProc', 0);
                limpiaAtributo('cmbCantidad', 0);


                limpiaAtributo('lblIdFactura', 0);
                limpiaAtributo('lblIdEstadoFactura', 0);
                limpiaAtributo('lblEstadoFactura', 0);
                limpiaAtributo('lblNumeroFactura', 0);
                limpiaAtributo('lblIdRecibo', 0);
                limpiaAtributo('lblTotalFactura', 0);
                limpiaAtributo('lblValorNoCubierto', 0);
                limpiaAtributo('lblValorCubierto', 0);
                limpiaAtributo('lblDescuento', 0);
                break;
            case 'eliminarAdmisionCuenta':
                buscarAGENDA('listAdmisionCuenta');
                limpiarDatosAdmision();
                /*limpiaAtributo('lblIdAdmision', 0);
                limpiaAtributo('lblIdCuenta', 0);
                limpiaAtributo('lblIdTipoAdmision', 0);
                limpiaAtributo('lblNomTipoAdmision', 0);
                limpiaAtributo('lblIdPlan', 0);
                limpiaAtributo('lblNomPlan', 0);
                limpiaAtributo('lblIdTipoAfiliado', 0);
                limpiaAtributo('lblNomTipoAfiliado', 0);
                limpiaAtributo('lblIdRango', 0);
                limpiaAtributo('lblNomRango', 0);
                limpiaAtributo('lblIdEstadoCuenta', 0);
                limpiaAtributo('lblNomEstadoCuenta', 0);
                limpiaAtributo('lblUsuarioCrea', 0);
                limpiaAtributo('txtIdAdminisAdmision', 0);
                limpiarListadosTotales('listProcedimientosDeFactura');*/
                break;
            case 'crearAdmision':
                setTimeout(" buscarAGENDA('listAdmision')", 5);
                break;
            case 'crearAdmisionCirugia':
                setTimeout(" buscarAGENDA('listAdmision')", 5);
                break;
            case 'pacienteAtendido':
                BuscarYcambiarIdQueryPorServicio()
                break;
            case 'folioCorregidoAuditoria':
                alert('FOLIO CORREGIDO PARA AUDITORIA')
                break;
            case 'abrirDocumento':
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;

            case 'accionesAlEstadoFolio':
                alert('TRANSACCION EXITOSA, FOLIO ACCIONADO.');
                asignaAtributo('txtIdEsdadoDocumento', valorAtributo('cmbAccionFolio'), 1)
                limpiaAtributo('cmbAccionFolio', 0);
                limpiaAtributo('txtMotivoAccion', 0);
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'auditarFolio':
                alert('TRANSACCION EXITOSA, FOLIO AUDITADO.');
                asignaAtributo('txtIdEsdadoDocumento', valorAtributo('cmbAccionFolio'), 1)
                limpiaAtributo('cmbAccionAuditoria', 0);
                limpiaAtributo('txtAuditado', 0);
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'enviaAutorizaIntercambio':
                buscarHC('listProcedimientosOrdenes', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'limpiaSolicitudIntercambio':
                buscarHC('listProcedimientosOrdenes', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'modificaJustificacionConcepto':
                buscarHC('listProcedimientosOrdenes', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'pacienteDilatando':
                BuscarYcambiarIdQueryPorServicio()
                break;
            case 'modificaCitaArchivo':
                ocultar('divVentanitaAgenda')
                setTimeout(" buscarAGENDA('listAgendaArchivo')", 5);
                break;
            case 'modificarAutorizacion': /********************call center y cirugia*/
                ocultar('divVentanitaAgenda')
                alert('MODIFICACION EXITOSA DE AUTORIZACION')
                setTimeout(" buscarAGENDA('listAgenda')", 5);
                break;
            case 'modificarAutorizacionArchivo': /******************** archivo*/
                ocultar('divVentanitaAgenda')
                alert('MODIFICACION EXITOSA DE AUTORIZACION')
                setTimeout(" buscarAGENDA('listAgendaArchivo')", 5);
                break;
            case 'crearCita':
                ocultar('divVentanitaAgenda')
                setTimeout(" buscarAGENDA('listAgenda')", 5);
                break;
            case 'crearCitaCirugia':
                alert('TRANSACCION EXITOSA')
                ocultar('divVentanitaAgenda')
                setTimeout(" buscarAGENDA('listAgenda')", 5);
                break;
            case 'listAgendaCopiarDia':
                alert('Copiada al dia : ' + valorAtributo('txtFechaDestino'))
                setTimeout(" buscarAGENDA('listAgenda')", 5);
                break;
            case 'dejarDisponibleAgendaDetalle':
                setTimeout(" buscarAGENDA('listAgenda')", 5);
                break;
            case 'eliminarAgendaDetalle':
                setTimeout(" buscarAGENDA('listAgenda')", 5);
                break;
            case 'activarDiaAgenda':
                setTimeout(" buscarAGENDA('listAgenda')", 5);
                break;
            case 'subDividirAgenda':
                setTimeout(" buscarAGENDA('listAgenda')", 5);
                break;
            case 'listAgenda':
                setTimeout(" buscarAGENDA('listAgenda')", 5);
                break;

            case 'meterDatosParaListarLaProyeccion':
                setTimeout("buscarHC('listProyeccionOrdenHistorico' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 10);
                break;
            case 'citaDejarDisponible':
                llenarCitas();
                ocultar('divSubVentanaAgendaNoPlaneada');
                break;
            case 'continuarProyeccionOrden':
                modificarCRUD('meterDatosParaListarLaProyeccion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
                break;

            case 'traerMedicamentosOrdenesAnteriores':
                buscarHC('listOrdenesMedicamentos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'traerMedicamentosOrdenesAnterioresContinuar':
                buscarHC('listOrdenesMedicamentos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;

            case 'eliminarSignosVitalesListado':
                buscarHC('listSignosVitales', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
                ocultar('divVentanitaP')
                break;
            case 'proyectarDocumentoInventario':
                //  buscarHC('listProyeccionOrden' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )

                ocultar('divTransaccionesInventarioConsolidado');
                ocultar('divTransaccionesInventarioPendientes');

                buscarSuministros('listTransaccionesInventario', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                //setTimeout(" buscarSuministros('listTransaccionesInventarioProyectado' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )",2000);				  

                break;
            case 'proyectarDocumentoInventarioEliminar':
                buscarHC('listProyeccionOrden', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

                ocultar('divTransaccionesInventarioConsolidado');
                ocultar('divTransaccionesInventarioPendientes');

                setTimeout(" buscarSuministros('listTransaccionesInventario' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 1000);
                setTimeout(" buscarSuministros('listTransaccionesInventarioProyectado' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 2000);

                break;
            case 'eliminarDocumentoInventario':
                limpiaAtributo('lblIdDocumento', 0);
                limpiarListadosTotales('listTransaccionesInventario');
                limpiarListadosTotales('listDocumentosHistoricosInventario');
                limpiarListadosTotales('listTransaccionesInventarioPendientes');
                limpiarListadosTotales('listTransaccionesInventarioConsolidado');

                buscarSuministros('listDocumentosHistoricosInventario', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                cerrarVentanitaNovedadesDocumentosInventario()
                break;
            case 'eliminarDocumentoInventarioTraslado':
                limpiaAtributo('lblIdDocumento', 0);

                buscarSuministros('listDocumentosInventarioTraslado', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                setTimeout(" buscarSuministros('listTransaccionesInventarioTraslado' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 1000);

                break;

            case 'eliminarDocumentoInventarioEntradas':
                limpiaAtributo('lblIdDocumento', 0);
                buscarSuministros('listDocumentosInventarioDevoluciones', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                setTimeout(" buscarSuministros('listTransaccionesInventarioDevoluciones' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 1000);

                break;
            case 'eliminarDocumentoInventarioDevoluciones':
                limpiaAtributo('lblIdDocumento', 0);
                buscarSuministros('listDocumentosInventarioDevoluciones', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                setTimeout(" buscarSuministros('listTransaccionesInventarioDevoluciones' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 1000);

                break;
            case 'listAnestesia':
                limpiaAtributo('cmbAnestesiasGrupo', 0);
                limpiaAtributo('txtHoraAnestesia', 0);
                limpiaAtributo('txtMinAnestesia', 0);
                limpiaAtributo('txtAnesteciaObservaciones', 0);
                buscarHistoria('listAnestesia')
                break;
            case 'listAntecedentes':
                limpiaAtributo('cmbAntecedenteGrupo', 0);
                limpiaAtributo('cmbTiene', 0);
                limpiaAtributo('cmbControlada', 0);
                limpiaAtributo('txtAntecedenteObservaciones', 0);
                buscarHC('listAntecedentes', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'guardaConciliacion':
                alert('CONCILIACION GUARDADA EXITOSAMENTE')
                // limpiaAtributo('cmbExiste',0);
                //limpiaAtributo('txtConciliacionDescripcion',0);	
                // buscarHC('listAntecedentesFarmacologicos' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )
                break;
            case 'listAntecedentesFarmacologicos':
                limpiaAtributo('txtAntecedenteFarmacologico', 0);
                limpiaAtributo('txtAntecedenteMedicamento', 0);
                buscarHC('listAntecedentesFarmacologicos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listSistemas':
                limpiaAtributo('txtSistema', 0);
                limpiaAtributo('txtHallazgo', 0);
                buscarHC('listSistemas', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;

            case 'listProcedimientosPosOperatoriosEliminar':
                buscarHistoria('listPosOperatorio')
                break;
            case 'listProcedimientosEliminar':
                limpiaAtributo('lblIdProcedimiento', 0);
                limpiarListadosTotales('listProcedimientosDetalle');
                buscarHC('listProcedimientos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listProcedimientosDetalleEliminar':
                ocultar('divVentanitaEditProcedimientos')

                if (tabActivo == 'divProcedimiento')
                    buscarHC('listProcedimientosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                else if (tabActivo == 'divAyudasDiagnosticas')
                    buscarHC('listAyudasDiagnosticasDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                else if (tabActivo == 'divTerapias')
                    buscarHC('listTerapiaDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                else if (tabActivo == 'divLaboratorioClinico')
                    buscarHC('listLaboratoriosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

                break;
            case 'listAyudasDiagnosticasEliminar':
                limpiaAtributo('lblIdProcedimiento', 0);
                limpiarListadosTotales('listAyudasDiagnosticas');
                buscarHC('listAyudasDiagnosticas', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listAyudaDiagnosticaDetalleEliminar':
                buscarHC('listAyudasDiagnosticasDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listLaboratoriosEliminar':
                limpiaAtributo('lblIdProcedimiento', 0);
                limpiarListadosTotales('listLaboratorios');
                limpiarListadosTotales('listLaboratoriosDetalle');
                buscarHC('listLaboratorios', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listLaboratoriosDetalleEliminar':
                buscarHC('listLaboratoriosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listTerapiaEliminar':
                limpiaAtributo('lblIdProcedimiento', 0);
                limpiarListadosTotales('listTerapia');
                limpiarListadosTotales('listTerapiaDetalle');
                buscarHC('listTerapia', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listTerapiaDetalleEliminar':
                buscarHC('listTerapiaDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listProcedimientos':
                limpiaAtributo('txtIdProcedimiento', 0);
                limpiaAtributo('cmbCantidad', 0);
                limpiaAtributo('cmbIdSitioQuirurgico', 0);
                limpiaAtributo('txtJustificacion', 0);
                buscarHC('listProcedimientos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listProcedimientoConNoPOS':

                if (tabActivo == 'divProcedimiento') {
                    limpiaAtributo('txtIdProcedimiento', 0);
                    limpiaAtributo('cmbCantidad', 0);
                    limpiaAtributo('cmbIdSitioQuirurgico', 0);
                    limpiaAtributo('txtIndicacion', 0);
                    ocultar('divVentanitaMedicamentosNoPos')
                    buscarHC('listProcedimientosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                } else if (tabActivo == 'divAyudasDiagnosticas') {
                    limpiaAtributo('txtIdAyudaDiagnostica', 0);
                    limpiaAtributo('cmbCantidadAD', 0);
                    limpiaAtributo('cmbIdSitioQuirurgicoAD', 0);
                    limpiaAtributo('txtIndicacionAD', 0);
                    ocultar('divVentanitaMedicamentosNoPos')
                    buscarHC('listAyudasDiagnosticasDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                } else if (tabActivo == 'divTerapias') {
                    limpiaAtributo('txtIdTerapias', 0);
                    limpiaAtributo('cmbCantidadTerapias', 0);
                    limpiaAtributo('cmbIdSitioQuirurgicoTerapias', 0);
                    limpiaAtributo('txtIndicacionTerapias', 0);
                    ocultar('divVentanitaMedicamentosNoPos')
                    buscarHC('listTerapiaDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                } else if (tabActivo == 'divLaboratorioClinico') {
                    limpiaAtributo('txtIdLaboratorio', 0);
                    limpiaAtributo('cmbCantidadLaboratorio', 0);
                    limpiaAtributo('cmbIdSitioQuirurgicoLaboratorio', 0);
                    limpiaAtributo('txtIndicacionLaboratorio', 0);
                    ocultar('divVentanitaMedicamentosNoPos')
                    buscarHC('listLaboratoriosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                }
                break;
            case 'listProcedimientosDetalle':
                limpiaAtributo('txtIdProcedimiento', 0);
                limpiaAtributo('cmbCantidad', 0);
                limpiaAtributo('cmbIdSitioQuirurgico', 0);
                limpiaAtributo('txtIndicacion', 0);
                buscarHC('listProcedimientosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listProcedimientosModJustificacion':
                //alert('Se ha modificado la justificacion de la terapia');
                // buscarHC('listTerapia' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' ) //actualiza el listado donde sale la justificacion
                buscarHC('listProcedimientos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listAyudasDiagnosticas':
                limpiaAtributo('txtIdProcedimiento', 0);
                limpiaAtributo('cmbCantidad', 0);
                limpiaAtributo('cmbIdSitioQuirurgicoAD', 0);
                limpiaAtributo('txtJustificacion', 0);
                buscarHC('listAyudasDiagnosticas', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listAyudasDiagnosticasDetalle':
                limpiaAtributo('txtIdAyudaDiagnostica', 0);
                limpiaAtributo('cmbCantidadAD', 0);
                limpiaAtributo('cmbIdSitioQuirurgicoAD', 0);
                limpiaAtributo('txtIndicacionAD', 0);
                buscarHC('listAyudasDiagnosticasDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listMedicacionConNoPOS':
                limpiaAtributo('txtIdArticulo', 0);
                limpiaAtributo('cmbIdVia', 0);
                limpiaAtributo('cmbUnidad', 0);
                limpiaAtributo('cmbCant', 0);
                limpiaAtributo('cmbFrecuencia', 0);
                limpiaAtributo('cmbDias', 0);
                limpiaAtributo('txtIndicaciones', 0);
                ocultar('divVentanitaMedicamentosNoPos')
                buscarHC('listMedicacion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listMedicacion':
                limpiaAtributo('txtIdArticulo', 0);
                limpiaAtributo('cmbIdVia', 0);
                limpiaAtributo('cmbUnidad', 0);
                limpiaAtributo('cmbCant', 0);
                limpiaAtributo('cmbFrecuencia', 0);
                limpiaAtributo('cmbDias', 0);
                limpiaAtributo('txtIndicaciones', 0);
                buscarHC('listMedicacion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;


            case 'listMedicamentosEditar':
                limpiaAtributo('lblIdEditar', 0);
                limpiaAtributo('lblIdElementoEditar', 0);
                limpiaAtributo('lblNombreElementoEditar', 0);
                limpiaAtributo('cmbIdViaEditar', 0);
                limpiaAtributo('cmbUnidadEditar', 0);
                limpiaAtributo('cmbCantEditar', 0);
                limpiaAtributo('cmbFrecuenciaEditar', 0);
                limpiaAtributo('cmbDiasEditar', 0);
                limpiaAtributo('txtCantidadEditar', 0);
                limpiaAtributo('txtIndicacionesEditar', 0);
                buscarHC('listMedicacion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
                ocultar('divVentanitaEditMedicamentos');
                break;


            case 'listLaboratorios':
                limpiaAtributo('txtIdLaboratorio', 0);
                limpiaAtributo('cmbCantidadLaboratorio', 0);
                limpiaAtributo('cmbIdSitioQuirurgicoLaboratorio', 0);
                limpiaAtributo('txtJustificacionLaboratorio', 0);
                buscarHC('listLaboratorios', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listLaboratoriosDetalle':
                limpiaAtributo('txtIdLaboratorio', 0);
                limpiaAtributo('cmbCantidadLaboratorio', 0);
                limpiaAtributo('cmbIdSitioQuirurgicoLaboratorio', 0);
                limpiaAtributo('txtIndicacionLaboratorio', 0);
                buscarHC('listLaboratoriosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listTerapia':
                limpiaAtributo('txtIdTerapia', 0);
                limpiaAtributo('cmbCantidadTerapia', 0);
                limpiaAtributo('cmbIdSitioQuirurgicoTerapia', 0);
                limpiaAtributo('txtJustificacionTerapia', 0);
                buscarHC('listTerapia', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listTerapiaDetalle':
                limpiaAtributo('txtIdTerapia', 0);
                limpiaAtributo('cmbCantidadTerapia', 0);
                limpiaAtributo('cmbIdSitioQuirurgicoTerapia', 0);
                limpiaAtributo('txtIndicacionTerapia', 0);
                buscarHC('listTerapiaDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listTerapiaModJustificacion':
                //alert('Se ha modificado la justificacion de la terapia');
                buscarHC('listTerapia', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp') //actualiza el listado donde sale la justificacion
                break;
            case 'eliminarMedicacion':
                ocultar('divVentanitaMedicacion');
                buscarHC('listMedicacion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'eliminarRemision':

                buscarReferencia('listRemision');
                ocultar('divVentanitaRemision');

                limpiaAtributo('cmbTipoEvento', 0);
                limpiaAtributo('cmbServicioSolicita', 0);
                limpiaAtributo('cmbServicioParaCual', 0);
                limpiaAtributo('txtDocResponsable', 0);
                limpiaAtributo('txtPrimerApeResponsable', 0);
                limpiaAtributo('txtSegundoApeResponsable', 0);
                limpiaAtributo('txtPrimerNomResponsable', 0);
                limpiaAtributo('txtSegundoNomResponsable', 0);
                limpiaAtributo('txtTelefonoResponsable', 0);
                limpiaAtributo('txtDireccionResponsable', 0);
                limpiaAtributo('txtDeparResponsable', 0);
                limpiaAtributo('txtMunicResponsable', 0);
                limpiaAtributo('txt_InformacionRemision', 0);
                limpiaAtributo('cmbIdProfesionalRemision', 0);
                asignaAtributo('cmbVerificacionPaciente', 'SI', 0);
                asignaAtributo('cmbFormatoDiligenciado', 'SI', 0);
                asignaAtributo('cmbResumenHc', 'SI', 0);
                asignaAtributo('cmbDocumentoIdentidad', 'SI', 0);
                limpiaAtributo('cmbImagenDiagnostica', 0);
                limpiaAtributo('cmbPacienteManilla', 0);


                break;
            case 'listRemision':
                buscarReferencia('listRemision')
                ocultar('divVentanitaRemision')

                limpiaAtributo('cmbTipoEvento', 0);
                limpiaAtributo('cmbServicioSolicita', 0);
                limpiaAtributo('cmbServicioParaCual', 0);
                limpiaAtributo('txtDocResponsable', 0);
                limpiaAtributo('txtPrimerApeResponsable', 0);
                limpiaAtributo('txtSegundoApeResponsable', 0);
                limpiaAtributo('txtPrimerNomResponsable', 0);
                limpiaAtributo('txtSegundoNomResponsable', 0);
                limpiaAtributo('txtTelefonoResponsable', 0);
                limpiaAtributo('txtDireccionResponsable', 0);
                limpiaAtributo('txtDeparResponsable', 0);
                limpiaAtributo('txtMunicResponsable', 0);
                limpiaAtributo('txt_InformacionRemision', 0);
                limpiaAtributo('cmbIdProfesionalRemision', 0);
                asignaAtributo('cmbVerificacionPaciente', 'SI', 0);
                asignaAtributo('cmbFormatoDiligenciado', 'SI', 0);
                asignaAtributo('cmbResumenHc', 'SI', 0);
                asignaAtributo('cmbDocumentoIdentidad', 'SI', 0);
                limpiaAtributo('cmbImagenDiagnostica', 0);
                limpiaAtributo('cmbPacienteManilla', 0);

                break;
            case 'eliminarAntecedenteFarmacologico':
                ocultar('divVentanitaAntecedentesFarmacologicos');
                buscarHC('listAntecedentesFarmacologicos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'eliminarSistemaListado':
                ocultar('divVentanitaSistemas');
                buscarHC('listSistemas', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'eliminarDxListado':
                ocultar('divVentanitaDx');
                buscarHC('listDiagnosticos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');


                if (tabActivo == 'divProcedimiento')
                    setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoP', 'lblIdDocumento')", 500);
                else if (tabActivo == 'divAyudasDiagnosticas')
                    setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoA', 'lblIdDocumento')", 500);
                else if (tabActivo == 'divTerapias')
                    setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoT', 'lblIdDocumento')", 500);
                else if (tabActivo == 'divLaboratorioClinico')
                    setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoL', 'lblIdDocumento')", 500);


                break;
            case 'listDiagnosticos':
                limpiaAtributo('txtIdDx', 0);
                limpiaAtributo('cmbDxTipo', 0);
                limpiaAtributo('cmbDxClase', 0);
                limpiaAtributo('cmbIdSitioQuirurgicoDx', 0);
                limpiaAtributo('txtDxObservacion', 0);
                buscarHC('listDiagnosticos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'eliminarAntecedentes':
                ocultar('divVentanitaAntecedentes')
                buscarHC('listAntecedentes', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listAntecedentes':
                limpiaAtributo('cmbAntecedenteGrupo', 0);
                limpiaAtributo('txtAntecedenteObservaciones', 0);
                buscarHC('listAntecedentes', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'listSignosVitales':
                limpiaAtributo('txtTemperatura', 0);
                limpiaAtributo('txtSistolica', 0);
                limpiaAtributo('txtDiastolica', 0);
                limpiaAtributo('txtPulso', 0);
                limpiaAtributo('txtRespiracion', 0);
                limpiaAtributo('txtPeso', 0);
                limpiaAtributo('txtTalla', 0);
                buscarHC('listSignosVitales', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'guardarNovedadTns':
                buscarSuministros('listTransaccionesInventario', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
                setTimeout("buscarSuministros('listTransaccionesInventarioConsolidado' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')", 500);
                //	 setTimeout( "buscarSuministros('listTransaccionesInventarioPendientes' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )",1300);
                ocultar('divTransaccionesInventarioPendientes');
                break;

            case 'listCrearBodega':
                buscarSuministros('listBodegas', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'cerrarTransaccionesDocumentoDevoluciones':
                buscarSuministros('listDocumentosInventarioDevoluciones', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
                setTimeout("buscarSuministros('listTransaccionesInventarioDevoluciones' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')", 1000);
                break;

            case 'cerrarTransaccionesDocumentoInvUD':

                buscarSuministros('listDocumentosHistoricosInventario', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
                setTimeout("buscarSuministros('listTransaccionesInventario' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')", 500);
                setTimeout("buscarSuministros('listTransaccionesInventarioConsolidado' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')", 1500);
                //						 setTimeout( "buscarSuministros('listTransaccionesInventarioPendientes' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )",1300);
                ocultar('divTransaccionesInventarioPendientes');
                asignaAtributo('lblIdEstadoDocumento', '1', 0);
                break;
            case 'cerrarTransaccionesDocumentoInv':
                buscarSuministros('listDocumentosInventario', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                setTimeout(" buscarSuministros('listTransaccionesInventarioBodega' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 500);
                setTimeout(" buscarSuministros('listArticulosBodega' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 1500);
                break;
            case 'cerrarTransaccionesDocumentoInvTraslado':
                buscarSuministros('listDocumentosInventarioTraslado', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                setTimeout(" buscarSuministros('listTransaccionesInventarioTraslado' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 500);

                break;

            case 'auditarDocumentoClinico':
                buscarHC('listDocumentosHistoricosAuditoria', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;

            case 'cerrarDocumentoClinico':
                asignaAtributo('txtIdEditable', '1', 0)
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                habilitar('imgAdicionListadoArticulos', 0)
                habilitar('btn_cerrarDocumentoHC', 0)
                habilitar('btnEliminarMedicamentoListado', 0)
                habilitar('imgAdicionListadoDx', 0)
                habilitar('btnEliminarProcedimientos', 0)
                habilitar('btnEliminarDx', 0)
                habilitar('btnProcedimiento', 0)
                break;
            case 'cerrarDocumentoClinicoRCOD':
                asignaAtributo('txtIdEditable', '1', 0)
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                habilitar('imgAdicionListadoArticulos', 0)
                habilitar('btn_cerrarDocumentoHC', 0)
                habilitar('btnEliminarMedicamentoListado', 0)
                habilitar('imgAdicionListadoDx', 0)
                habilitar('btnEliminarProcedimientos', 0)
                habilitar('btnEliminarDx', 0)
                habilitar('btnProcedimiento', 0)
                break;
            case 'cerrarDocumentoClinicoGeneral':
                asignaAtributo('txtIdEditable', '1', 0)
                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                habilitar('imgAdicionListadoArticulos', 0)
                habilitar('btn_cerrarDocumentoHC', 0)
                habilitar('btnEliminarMedicamentoListado', 0)
                habilitar('imgAdicionListadoDx', 0)
                habilitar('btnEliminarProcedimientos', 0)
                habilitar('btnEliminarDx', 0)
                habilitar('btnProcedimiento', 0)
                break;

            case 'eliminarMedicamentoListado':
                buscarHC('listOrdenesMedicamentos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                cerrarVentanitaOrdenMedicamentos();
                break;
            case 'adicionarSolicitudADocumento':
                buscarSuministros('listTransaccionesInventario', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                ocultar('divAdicionOrdenADocumento');
                break;
            case 'eliminarInsumoListado':
                buscarHC('listOrdenesInsumos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                cerrarVentanitaOrdenInsumos();
                break;
            case 'crearPacienteNuevo':
                alert("CREACION EXITOSA !");
                limpiarDivEditarJuan('paciente')
                break;
            case 'pacienteModifica':
                alert("MODIFICACION EXITOSA !");
                buscarInformacionPacienteExistenteLE()
                break;
            case 'pacienteModificaArchivo':
                alert("Informacion del Paciente Modificada Exitosamente !");
                buscarInformacionPacienteExistenteLE();
                setTimeout(" buscarAGENDA('listAgendaArchivo')", 5);
                break;
            case 'eliminarPaciente':
                alert("ELIMINACION EXITOSA !");
                limpiarDivEditarJuan('paciente')
                break;
            case 'paciente':
                alert("MODIFICACION EXITOSA !");
                buscarHC('paciente', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;
            case 'btn_nueva_Id':
                alert("MODIFICACION EXITOSA !");
                setTimeout("llamarAutocomIdDescripcion('txtBusIdPaciente',15)", 1);
                alert('Cierre la ventana para refrescar la identificacion de esta persona')
                arg = 'paciente';
                break;
            case 'correccion_id_historico':
                alert("MODIFICACION EXITOSA !");
                setTimeout("llamarAutocomIdDescripcion('txtBusIdPaciente',15)", 1);
                alert('Cierre la ventana para refrescar la identificacion de esta persona')
                arg = 'paciente';
                break;
            default:

                break;

        }
        limpiarDivEditarJuan(arg);	// una vez se modifique se limpia	 los atributos del formulario		   		
    }

}

function guardarRespuestaEmbarazo(arg, respuesta, id_folio) {

    valores_a_mandar = 'accion=' + arg;
    valores_a_mandar = valores_a_mandar + "&idQuery=674&parametros=";
    add_valores_a_mandar(respuesta);
    add_valores_a_mandar(valorAtributo(id_folio));
    ajaxModificar();


    if (respuesta == 'SI') {
        alert('Este procedimiento no se puede ordenar porque la paciente se encuentra en embarazo o no sabe.')
    } else if (respuesta == 'NO SABE') {
        alert('Este procedimiento no se puede ordenar porque la paciente se encuentra en embarazo o no sabe.')
    }


}


function limpiarCombosMedicacionHC() {
    limpiaAtributo('cmbIdVia', 0);
    limpiaAtributo('cmbUnidad', 0);
}


function fecha_actual() {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1; //hoy es 0!
    var yyyy = hoy.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    hoy = dd + '/' + mm + '/' + yyyy;
    return hoy;
}

function number_format(amount, decimals) {

    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0)
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}

var mensaje = "";
var valores_a_mandar = "";
var paginaActual = ""; // ubicar el nombre de la pagina actual
var setResultados = new Array();
var posicionActual = 0; //definir en que posicion de la busqueda me encuentro
var totalRegistros = 0;
var parametros = new Array();
var totalRegistros = 0;
var tabActivo = '1';
var rutaFotos = '/clinica/utilidades/fotos/';
var rutaFirmas = '/clinica/utilidades/firmas/';
var rutaNombres = '/clinica/utilidades/firmas/nombres/';
var idQuery = '';
var elemInputDestino = '';
var version = 1.0;


function prepararVentana() {
    document.getElementById('divContenidoListaEsperaHist').innerHTML = '<table id="listHistoricoListaEsperaEnAgenda" class="scroll"></table>';
}


function copiarCitas() {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxInit.onreadystatechange = respuestaCopiarCitas;

    if (valorAtributo('cmbTipoCopiado') == 1) {
        fechas = [];
        ids = $("#listDias").getDataIDs();

        for (var i = 0; i < ids.length; i++) {
            var c = "jqg_listDias_" + ids[i];
            if ($("#" + c).is(":checked")) {
                datosRow = $("#listDias").getRowData(ids[i]);
                fechas.push(datosRow.Fecha2);
            }
        }

        citas = [];
        ids = $("#listAgenda").getDataIDs();

        for (var i = 0; i < ids.length; i++) {
            var c = "jqg_listAgenda_" + ids[i];
            if ($("#" + c).is(":checked")) {
                datosRow = $("#listAgenda").getRowData(ids[i]);
                citas.push(datosRow.ID);
            }
        }

        if (citas.length <= 0) {
            alert("DEBE SELECCIONAR AL MENOS UNA CITA")
            return
        }

        if (fechas.length <= 0) {
            alert("DEBE SELECCIONAR AL MENOS UN DIA")
            return
        }
        valores_a_mandar = "accion=copiarCitas";
        valores_a_mandar = valores_a_mandar + "&fechas=" + fechas + "&citas=" + citas;
        varajaxInit.send(valores_a_mandar);
    } else if (valorAtributo('cmbTipoCopiado') == 2) {
        modificarCRUD('copiarCitasFrecuencia')
    } else {
        alert("Ninguna accion para el tipo seleccionado");
    }
}

function respuestaCopiarCitas() {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            alert("Accion terminada.")
            buscarAGENDA("listAgenda")
            $("#copiarCitas").removeAttr('checked');
            ocultar('divOpcionesCopiado')
            ocultar('divOpcionesFrecuencia')
            setTimeout(() => {
                buscarAGENDA("listDias")
            }, 200);
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
}

function verificarEnvioFactura() {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=auditarFactura";
    valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
    varajaxInit.send(valores_a_mandar);
}

function tabActivoAsignacionCita(idDiv){
    switch (idDiv) {
        case 'divCitaUnica':
            setTimeout(() => {
                
            }, 500);
            break;
        
        case 'divProgramacionTerapias':
            buscarAGENDA('listCoordinacionTerapiaOrdenadosAgenda')
            setTimeout(() => {
                buscarAGENDA('listaProgramacionTerapiasAgenda')
            }, 300);
            break;
    }
}


function tabActivoFoliosParametros(idDiv) {
    switch (idDiv) {
        case 'divFoliosTipoAdmision':
            setTimeout(() => {
                buscarHC('listGrillaTiposCita', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                setTimeout(() => {
                    buscarHC('listGrillaFolioProcedimientos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                }, 200);
            }, 500);
            break;
        
        case 'divFoliosProfesion':
            buscarHC('listGrillaProfesiones', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            break;
    }
}

function tabActivoPlanesContratacion(idDiv) {
    switch (idDiv) {
        case 'divProcedimientosPlan':
            buscarFacturacion('listGrillaPlanesDetalle')
            setTimeout(() => {
                buscarFacturacion('listGrillaPlanExcepcion')
            }, 300);
            break;

        case 'divArticulosPlan':
            buscarFacturacion('listGrillaPlanesMedicamentos')
            break;

        case 'divRangos':
            buscarFacturacion('listGrillaRangos')
            break;

        case 'divLiquidacion':
            buscarFacturacion('listGrillaContratos')
            break;

        case 'divLiquidacionProcedimientos':
            buscarFacturacion('listGrillaProcedimientos')
            break;

        case 'divMunicipiosPlan':
            buscarFacturacion('listGrillaMunicipios')
            break;

        case 'divSedesPlan':
            buscarFacturacion('listSedePlan')
            break;
    }
}

function reiniciarElementosNuevoPacienteEncuesta() {
    $("#sw_nuevo_paciente").removeAttr("checked");
    ocultar("elementosNuevoPaciente")
    limpiaAtributo("cmbTipoIdUrgencias", 0)
    limpiaAtributo("txtIdentificacionUrgencias", 0)
}

function elementosNuevoPacienteEncuesta() {
    if ($('#sw_nuevo_paciente').attr('checked')) {
        mostrar("elementosNuevoPaciente")
    } else {
        ocultar("elementosNuevoPaciente")
    }
}

function abrirvideollamada() {
    var url = "";
    url = "http://18.211.197.195:8383/clinica/login_crm,jsp"
    window.open(url);
}

function reaccionAEvento(idElemento) {
    switch (idElemento) {
        case 'chkFrecuencia':
            if($('#chkFrecuencia').attr('checked')){
                mostrar("tdFrecuencia")
            } else {
                ocultar("tdFrecuencia")
            }
            break;

        case 'cmbTipoCopiado':
            if (valorAtributo(idElemento) == '2') {
                mostrar('divOpcionesFrecuencia')
            } else {
                ocultar('divOpcionesFrecuencia')
            }
            break;

        case 'copiarCitas':
            if (valorAtributo('cmbIdProfesionales') != '') {
                setTimeout(() => {
                    buscarAGENDA("listDias")
                    buscarAGENDA("listAgenda")
                }, 200);

                if ($('#copiarCitas').attr('checked')) {
                    mostrar('divOpcionesCopiado');
                    if (valorAtributo('cmbTipoCopiado') == '2') {
                        mostrar('divOpcionesFrecuencia')
                    } else {
                        ocultar('divOpcionesFrecuencia')
                    }
                } else {
                    ocultar('divOpcionesCopiado');
                    ocultar('divOpcionesFrecuencia')
                }
            } else {
                alert("FALTA SELECCIONAR AGENDA")
                if ($('#copiarCitas').attr('checked')) {
                    $("#copiarCitas").attr('checked', false)
                } else {
                    $("#copiarCitas").attr('checked', true)
                }
            }
            break;

        case 'cmbTipoAlta':
            if (valorAtributo("cmbTipoAlta") == "6") {
                mostrar("trInfoMuerte")
            } else {
                ocultar("trInfoMuerte")
            }
            break;

        case 'cmbOrdenenesAdmisionURG':
            if (valorAtributo("cmbOrdenenesAdmisionURG") == "1") {
                cargarComboGRAL('', '', 'cmbTipoAlta', 161);
            } else {
                asignaAtributoCombo("cmbTipoAlta", "", "");
            }
            ocultar("trInfoMuerte");
            break;

        case 'sw_fecha_actual':
            if ($('#sw_fecha_actual').attr('checked')) {
                $("#txtFechaMuerte").attr('disabled', 'disabled');
                $("#txtHoraMuerte").attr('disabled', 'disabled');
                $("#txtMinutoMuerte").attr('disabled', 'disabled');
                $("#cmbPeriodoMuerte").attr('disabled', 'disabled');
            } else {
                $("#txtFechaMuerte").removeAttr('disabled');
                $("#txtHoraMuerte").removeAttr('disabled');
                $("#txtMinutoMuerte").removeAttr('disabled');
                $("#cmbPeriodoMuerte").removeAttr('disabled');
            }
            break;
    }
}

function limpiarCamposURG(arg) {
    switch (arg) {
        case "limpiarDatosPaciente":
            limpiaAtributo("txtIdBusPacienteUrgencias", 0)
            limpiaAtributo("lblIdPacienteUrgencias", 0)
            break;

        case "limpiarDatosBusquedaURG":
            limpiaAtributo("cmbTipoIdUrgencias", 0)
            limpiaAtributo("txtIdentificacionUrgencias", 0)
            limpiaAtributo("txtIdBusPacienteUrgencias", 0)
            limpiaAtributo("lblIdPacienteUrgencias", 0)
            limpiaAtributo("cmbIdTipoServicioUrgencias", 0)
            limpiaAtributo("cmbEstadoAdmisionURG", 0)
            limpiaAtributo("cmbPreferencialURG", 0)
            break;

        default:
            break;
    }
}

function traerIdUltimoPacienteURG(tipo_id) {
    cantColumnasP = 1;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarReportes_xml.jsp', true);
    varajaxInit.onreadystatechange = respuestaTraerIdUltimoPacienteURG;

    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=listadoReporteExcel";
    valores_a_mandar = valores_a_mandar + "&id=3";
    valores_a_mandar = valores_a_mandar + "&parametro1=" + tipo_id;
    varajaxInit.send(valores_a_mandar);
}

function respuestaTraerIdUltimoPacienteURG() {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('rows') != null) {
                max_id = raiz.getElementsByTagName('c1')[1].firstChild.data;
                asignaAtributo("txtIdentificacionUrgencias", max_id, 0)
            } else {
                alert("Error al crear paciente. Intente nuevamente")
            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
}




function traerAdmisionPacienteURG() {
    ocultar("divmodificarAdmisionPacienteURG")
    ocultar("divVentanitaAdmisionUrgencias")
    asignaAtributo("txtIdBusPaciente", valorAtributo("lblIdPacienteModificarURG") + "-" + valorAtributo("lblNombrePacienteModificarURG"), 0)
    setTimeout(() => {
        buscarInformacionBasicaPaciente()
        setTimeout(() => {
            buscarAGENDA('listAdmisionCuenta')
            setTimeout(() => {
                datos = $("#listAdmisionCuenta").getDataIDs()
                for (i = 1; i <= datos.length; i++) {
                    if (valorAtributo("lblIdAdmisionModificarURG") == $("#listAdmisionCuenta").getRowData(i).ID) {
                        $("#listAdmisionCuenta").jqGrid('setSelection', i, true);
                        return
                    }
                }
            }, 800);
        }, 200);
    }, 200);

}

function traerInfoServicio() {
    if (true) {
        informacionBasicaServicio(valorAtributo("cmbIdTipoServicio"));
    }
}

function informacionBasicaServicio(servicio) {
    cantColumnasP = 1;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarReportes_xml.jsp', true);
    varajaxInit.onreadystatechange = respuestaInformacionBasicaServicio;

    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=listadoReporteExcel";
    valores_a_mandar = valores_a_mandar + "&id=2";
    valores_a_mandar = valores_a_mandar + "&parametro1=" + servicio;
    varajaxInit.send(valores_a_mandar);
}

function traerPlanesDeContratacionAgendaOrdenes(idElemDestino, idQuery, condicion1, condicion2) {
    id_administradora = valorAtributoIdAutoCompletar(condicion1)
    id_regimen = valorAtributo(condicion2).split("-")[0]
    id_tipo_plan = valorAtributo(condicion2).split("-")[1]
    sede = valorAtributo("cmbSede")

    cargarComboGRALCondicion4('', '', idElemDestino, idQuery, id_administradora, id_regimen, id_tipo_plan, sede)
}


function traerPlanesDeContratacionAgenda(idElemDestino, idQuery, condicion1, condicion2) {
    numero_cita_seleccionada = $("#listAgenda").jqGrid('getGridParam', 'selrow')

    id_administradora = valorAtributoIdAutoCompletar(condicion1)
    id_regimen = valorAtributo(condicion2).split("-")[0]
    id_tipo_plan = valorAtributo(condicion2).split("-")[1]
    sede = $("#listAgenda").getRowData(numero_cita_seleccionada).ID_SEDE

    cargarComboGRALCondicion4('', '', idElemDestino, idQuery, id_administradora, id_regimen, id_tipo_plan, sede)
}


function traerPlanesDeContratacion(idElemDestino, idQuery, condicion1, condicion2) {
    id_administradora = valorAtributoIdAutoCompletar(condicion1)
    id_regimen = valorAtributo(condicion2).split("-")[0]
    id_tipo_plan = valorAtributo(condicion2).split("-")[1]
    sede = IdSede()

    cargarComboGRALCondicion4('', '', idElemDestino, idQuery, id_administradora, id_regimen, id_tipo_plan, sede)
}

function cargarComboGRALCondicion4(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1, condicion2, condicion3, condicion4) {
    if (condicion1 != '' && condicion2 != '') {
        idcombo = comboDestino;
        porDefecto = deptoDefecto;
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=4&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3 + "&condicion4=" + condicion4;
        varajax1 = crearAjax();
        varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
        varajax1.onreadystatechange = respuestacargarComboGRAL;
        varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajax1.send(valores_a_mandar);
    } else {
        $("#" + comboDestino).empty();
        $("#" + comboDestino).append(new Option("[ SELECCIONE ]", "", true, true));
    }
}

function respuestaInformacionBasicaServicio() {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('rows') != null) {

                totalRegistros = raiz.getElementsByTagName('c1').length;

                if (totalRegistros) {
                    agregarDatosTablaHistoricosAtencion(raiz, totalRegistros);
                } else {
                    alert("Sin datos en el reporte, cierre y vuelva a generarlo");
                }

            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
}

function copiarContenido(idElemento) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(valorAtributo(idElemento).replace(/%20/g, " ")).select();
    document.execCommand("copy");
    $temp.remove();

    $("#dialog").dialog({
        open: setTimeout(() => {
            $("#dialog").dialog("destroy");
        }, 1000),
        title: "Elemento copiado",
        height: 0,
        minHeight: 0
    });
}

function mostrarMotivosAbrir() {
    if (valorAtributo('cmbAccionFolio') != 0) {
        document.getElementById('cmbMotivosAbrir').innerHTML = '<option value=""></option>';
    } else {
        cargarComboGRALCondicion1('cmbPadre', '1', 'cmbMotivosAbrir', 155, valorAtributo('lblIdAdmision'));
    }
}

function fe() {
    select = document.getElementById("año");
    for (i = 2000; i <= 2050; i++) {
        option = document.createElement("option");
        option.value = i;
        option.text = i;
        select.appendChild(option);
    }
}

function mostrarDivVentanaDescuento() {

    if (verificarCamposGuardar('mostrarDivVentanaDescuento')) {

        if (valorAtributo('cmbIdTipoRegimen').split('-')[0] == 'A') {
            asignaAtributo('lblValorBaseDescuento', valorAtributo('lblValorCubierto'), 0)
        } else {
            asignaAtributo('lblValorBaseDescuento', valorAtributo('lblValorNoCubierto'), 0)
        }
        mostrar('divVentanitaDescuentoFactura')
    }

}

function checkKey2(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }

    if (unicode == 13) {
        guardarYtraerDatoAlListado('buscarIdentificacionPaciente')
    }
}

function checkKey3(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }

    if (unicode == 13) {
        if (verificarCamposGuardar("nuevoIdPacienteUrgencias")) {
            guardarYtraerDatoAlListado('buscarIdentificacionPacienteUrgencias')
        }
    }
}

function nuevoPacienteEncuesta() {
    if (verificarCamposGuardar("nuevoIdPacienteUrgencias")) {
        guardarYtraerDatoAlListado('buscarIdPacienteEncuesta')
    }
}

function mostrarDivVentanaDescuentoArticulo() {
    if (verificarCamposGuardar('mostrarDivVentanaDescuento')) {
        asignaAtributo('lblValorBaseDescuento', valorAtributo('lblValorCubierto'), 0)
        mostrar('divVentanitaDescuentoFactura')
    }
}

function numerarFactura() {
    if (valorAtributo('lblIdFactura') != '') {
        var idFactura = valorAtributo('lblIdFactura');
        var idEstadoFactura = valorAtributo('lblIdEstadoFactura');
        var idDoc = "";
        if (valorAtributo('lblIdDoc')) {
            idDoc = valorAtributo('lblIdDoc');
        }
        formatoPDFFactura(idFactura, idEstadoFactura, idDoc);
    } else {
        alert('SELECCIONE UNA ADMISION CON FACTURA');
    }
}

function numerarFacturaArticulo() {
    if (valorAtributo('lblIdFactura') != '') {

        var ids = jQuery("#listArticulosDeFactura").getDataIDs();

        if (ids.length != 0) {

            var idFactura = valorAtributo('lblIdFactura');
            var idEstadoFactura = valorAtributo('lblIdEstadoFactura');
            //var idCuenta = valorAtributo('lblIdCuenta');

            //formatoPDFFactura(idFactura, idEstadoFactura,idCuenta);
            formatoPDFFactura(idFactura, idEstadoFactura);
        } else {
            alert('DEBE AGREGAR ALGUN ARTICULO PARA VER LA FACTURA');
        }
    } else {
        alert('SELECCIONE UN DOCUMENTO');
    }
}


function eliminarElementoGrilla(list, rowid) {

    var ids = jQuery('#' + list).getDataIDs();
    var data = [''];
    var p;

    for (var i = 0; i < ids.length; i++) {
        if (ids[i] === rowid) {
            p = i;
        } else {
            data.push(jQuery('#' + list).getRowData(ids[i]));
        }
    }

    ids.splice(p, 1)

    jQuery('#' + list).clearGridData()

    for (var i = 0; i < ids.length; i++) {
        jQuery('#' + list).addRowData(i, data[i + 1]);
    }
}


function verificaTipo() {

    if (valorAtributo('cmbIdTipoDocumento') == '36') {
        asignaAtributo('txtDocEgreso', '', 0);
    } else {
        asignaAtributo('txtDocEgreso', '', 1);
    }
}

function llamarIndicacionAdx() {
    var texto = "CON MEDIO DE CONTRASTE ( FLUORESCEINA SODICA)";
    var valor = valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica');
    if (valor == '951201') {
        asignaAtributo('txtIndicacionAD', texto, 0);
    }
}

function enviarConsultaCita() { /*ESTA FUNCION SE ENCUENTRA DUPLICADA POR CONCEPTOS DE BODY, EN LA PAGINA DE consultaCitas.jsp*/
    var nuevaURL = "";
    //    if( document.getElementById('cmbTipoId').value =='' && document.getElementById('txtIdentificacionPaciente').value =='' ){ 

    var tipoId = document.getElementById('cmbTipoId').value;
    var identificacion = document.getElementById('txtIdentificacionPaciente').value;

    nuevaURL = "/clinica/paginas/ireports/citas/generaCitas.jsp?reporte=consultaCitas&tipoId=" + tipoId + "&id=" + identificacion.trim();
    window.open(nuevaURL, "", "width=1000, height=700, top=80, left=40, scrollbars=yes, Resizable=No, Directories=No, Location=No, Menubar=No, Status=No, Titlebar=No, Toolbar=No, fullscreen=yes");

    //	    }		else alert('DEBE ESCRIBIR SU TIPO Y NUMERO DE IDENTIDAD')
}

function consultaSolicitudAutorizacion(caso) {
    var nuevaURL = "";
    switch (caso) {
        case 'solicitud':
            if (valorAtributo('lblIdSolicitud') != '') {
                var id = valorAtributo('lblIdSolicitud')
                nuevaURL = "/clinica/paginas/ireports/solicitudes/generaIntercambio.jsp?reporte=solicitud&id=" + id;
                window.open(nuevaURL, "", "width=1000, height=700, top=80, left=40, scrollbars=yes, Resizable=No, Directories=No, Location=No, Menubar=No, Status=No, Titlebar=No, Toolbar=No, fullscreen=yes");
            } else alert('DEBE SELECCIONAR LA SOLICITUD')
            break;
        case 'autorizacion':
            if (valorAtributo('lblIdAutorizacion') != '') {
                var id = valorAtributo('lblIdAutorizacion')
                nuevaURL = "/clinica/paginas/ireports/solicitudes/generaIntercambio.jsp?reporte=autorizacion&id=" + id;
                window.open(nuevaURL, "", "width=1000, height=700, top=80, left=40, scrollbars=yes, Resizable=No, Directories=No, Location=No, Menubar=No, Status=No, Titlebar=No, Toolbar=No, fullscreen=yes");
            } else alert('DEBE SELECCIONAR LA AUTORIZACION')
            break;

    }

}

function formatoReportPDFIntercambio() {
    if (valorAtributo('lblIdEstadoFolio') == '1') {
        var openedWindow;
        var idReporte = valorAtributo('lblIdDocumento');
        var reporte = valorAtributo('lblTipoDocumento');
        var nuevaURL = "/clinica/paginas/ireports/hc/to_pdf.jsp?reporte=" + reporte + "&evo=" + idReporte;
        //openedWindow=window.open(nuevaURL);  ../../ireports/hc/to_pdf.jsp	
        window.open(nuevaURL, '_blank');
        window.open(this.href, '_self');
    } else alert('FOLIO DEBE ESTAR FINALIZADO')

}

function formatoReportPDFIntercambio2() {
    //    if (valorAtributo('lblIdEstadoFolio') == '1') {
    var openedWindow;
    var idReporte = valorAtributo('lblIdDocumento');
    var reporte = valorAtributo('lblTipoDocumento');
    var imagenEnvio = valorAtributo('imgCanvas');
    alert(imagenEnvio)
    var nuevaURL = "/clinica/paginas/hc/guardaFirma.jsp?reporte=" + reporte + "&evo=" + idReporte + "&imgEnvio=" + imagenEnvio;

    window.open(nuevaURL, '_blank');
    window.open(this.href, '_self');
    //  } else alert('FOLIO DEBE ESTAR FINALIZADO')

}

function verificarSerialMod() {
    var serial = valorAtributo('txtSerial');
    if (serial != '') {
        cargarElementCondicionDosCodBarras('txtRespuestaSerial', 540, serial, 'N');
    }
}

function respuestaVSerialMod() {
    vSerial = valorAtributo('txtRespuestaSerial');
    if (vSerial == 0) {
        modificarCRUD('modificaTransaccionSerial')
    } else {
        alert('El serial ya se encuentra registrado.. ! ');
    }
}
/* fn llamada desde entrada btn crear y desde fn registrarSerial */
function verificarSerial() {
    var serial = valorAtributo('txtSerial');
    if (serial != '') {
        cargarElementCondicionDosCodigoBarras('txtRespuestaSerial', 540, serial, 'N');
    } else {
        modificarCRUD('crearTransaccion');
    }
}

/* fn llamada en la respuesta de cargarElementCondicionDosCodigoBarras */
function respuestaVSerial() {
    vSerial = valorAtributo('txtRespuestaSerial');
    if (vSerial == 0) {
        modificarCRUD('crearTransaccion');
    } else {
        alert('El serial ya se encuentra registrado ! ');
    }
}

/* funcion para entradas accionada desde txtSerial */
function registrarSerial() {
    if (valorAtributo('lblIdEstado') == '0') {
        if (valorAtributo('lblIdTransaccion') != '') {
            verificarSerialMod()
        } else {
            verificarSerial()
        }
    } else alert('EL DOCUMENTO DEBE ESTAR ABIERTO')
}
/* solo para salidas llamada desde cargarElementoCondicionDosCodigoBarras*/
function codigoBarras() {
    valorCodigo = valorAtributo('lblTraidosCodBarras');
    if (valorCodigo != '') {
        var codigoB = valorCodigo.split('_-_');
        id_art = codigoB[0];
        articulo = codigoB[1];
        lot = codigoB[2];
        existencia = codigoB[3];
        vlr_unitario = codigoB[4];
        f_venc = codigoB[5];
        medida = codigoB[6];

        if (valorAtributo('txtVenCodBarras') == 1) {
            asignaAtributo('txtIdArticulo', id_art + '-' + articulo, 0);
            asignaAtributo('lblValorUnitario', vlr_unitario, 0);

            // valores en div oculto para el registro de la salida
            asignaAtributo('txtIdArticulo_dos', id_art, 0);
            asignaAtributo('txtIdLote_dos', lot, 0);
            asignaAtributo('txtValorUnitario_dos', vlr_unitario, 0);
            asignaAtributo('txtFV_dos', f_venc, 0);
            asignaAtributo('lblMedida', medida, 0);

        } else {
            asignaAtributo('lblIdArticulo', articulo, 0);

            //1
            asignaAtributo('lblValorUnitarioCodBarra', vlr_unitario, 0);
            //2
            var vlrSinIva = (Math.round((vlr_unitario / 1.19).toFixed(1)));
            asignaAtributo('lblSinIvaCodBarra', vlrSinIva + '', 0);
            //3
            var vlrIva = (Math.round(((vlrSinIva * 19) / 100).toFixed(1)));
            asignaAtributo('lblValorIvaCodBarra', vlrIva + '', 0);

        }

        if ($('#txtSerial').length) {
            asignaAtributo('txtSerial', valorAtributo('txtIdBarCode'));
        }

        asignaAtributo('txtIdLote1', lot, 0);
        asignaAtributo('txtIdLoteBan', 'SI', 0);
        asignaAtributo('txtCantidad', '1', 0);
        asignaAtributo('lblIva', '0', 0);
        asignaAtributo('txtFV', f_venc, 0);
        var cantidad = parseInt(valorAtributo('txtCantidad'));
        var valorUnitario = valorAtributo('txtValorUnitario_dos');
        var val_iva = valorAtributo('lblIva');
        var subTotal = (valorUnitario / ((val_iva / 100) + 1)) * cantidad;
        var lblValorImpuesto = subTotal.toFixed(2) * ((val_iva / 100));
        asignaAtributo('lblValorImpuesto', lblValorImpuesto.toFixed(2), 0);
        setTimeout("$('#txtIdBarCode').focus();", 400);
        setTimeout("modificarCRUD('crearTransaccion_dos')", 300);

        if ($('#txtCodBarrasBus').length) {
            limpiaAtributo('txtCodBarrasBus', 0);
        }
    }
}

function codigoBarrasVentas() {
    valorCodigo = valorAtributo('lblDatosCodigoBarraVentas');
    if (valorCodigo != '') {
        var codigoB = valorCodigo.split('_-_');
        id_art = codigoB[0];
        articulo = codigoB[1];
        lot = codigoB[2];
        existencia = codigoB[3];
        vlr_unitario = codigoB[4];
        f_venc = codigoB[5];
        medida = codigoB[6];


        asignaAtributo('lblIdArticulo', id_art + '-' + articulo, 0);
        asignaAtributo('lblCantidad', '1', 0);
        asignaAtributo('lblValorUnitario', vlr_unitario, 0);
        asignaAtributo('lblIva', '0', 0);
        asignaAtributo('lblValorImpuesto', vlr_unitario, 0);
        asignaAtributo('lblIdLote', lot, 0);
        asignaAtributo('lblFechaVencimiento', f_venc, 0);
        asignaAtributo('lblMedida', medida, 0);
        asignaAtributo('lblNaturaleza', 'S', 0);



        setTimeout("$('#txtIdCodigoBarrasVentas').focus();", 400);
        setTimeout("modificarCRUD('crearTransaccionVentas')", 300);

    }
}

function mostrarVentanaTipificacionRips(id_factura) {
    asignaAtributo("lblIdFactura");
    setTimeout(() => {
        mostrar('divVentanitaArchivosTipificacion');
        buscarFacturacion('listArchivosTificacion')
    }, 100);
}

function obtenerListaFacturasTabla(idTabla) {
    var lista_facturas = [];

    var ids = $("#" + idTabla).getDataIDs();

    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_" + idTabla + "_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosRow = $("#" + idTabla).getRowData(ids[i]);
            lista_facturas.push(datosRow.ID_FACTURA);
        }
    }
    return lista_facturas;
}

function obtenerListaSedes(idTabla){
    var lista_sedes = [];
    var ids = $("#"+idTabla).getDataIDs();
    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_" + idTabla + "_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosSedeRow = $("#" + idTabla).getRowData(ids[i]);
            lista_sedes.push(datosSedeRow.id_sede);
        }
    }
    return lista_sedes;
}

function agregarSede(lista_sedes)
{
    tam = lista_sedes.length;
    if (lista_sedes <= 0) {
        alert("DEBE SELECCIONAR AL MENOS UNA SEDE");
    } 
    else {
        varajaxInit = crearAjax();
        valores_a_mandar = "";
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', true);
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.onreadystatechange = function () { respuestaAgregarSede() }
        valores_a_mandar = 'accion=agregarsede';
        valores_a_mandar = valores_a_mandar + "&idQuery=939&parametros=";
        add_valores_a_mandar(valorAtributo('txtIdentificacion'));
        add_valores_a_mandar(lista_sedes);        
        varajaxInit.send(valores_a_mandar);
    }
}

function respuestaAgregarSede()
{    
    if (varajaxInit.readyState == 4) {        
        buscarUsuario('sede');
        setTimeout(() => {
            buscarUsuario('sedes');
        }, 200);
        alert('Sedes Agregadas');
        limpiaAtributo('lblIdSede');     
    }
}

function obtenerListaFacturasTipificacionRips() {
    var lista_facturas = [];

    var ids = $("#listGrillaDetallesArchivoRips").getDataIDs();

    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_listGrillaDetallesArchivoRips_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosRow = $("#listGrillaDetallesArchivoRips").getRowData(ids[i]);
            lista_facturas.push(datosRow.ID_FACTURA);
        }
    }

    var ids = $("#listGrillaDetallesPendientesArchivoRips").getDataIDs();

    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_listGrillaDetallesPendientesArchivoRips_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosRow = $("#listGrillaDetallesPendientesArchivoRips").getRowData(ids[i]);
            lista_facturas.push(datosRow.ID_FACTURA);
        }
    }
    return lista_facturas
}

function tipificarFacturas(lista_facturas, unidadProgreso, progreso) {
    if (lista_facturas <= 0) {
        alert("DEBE SELECCIONAR AL MENOS UNA FACTURA");
    } else {
        $("#btnTipificacion").hide();
        $("#loaderTipificacion").show();

        if (unidadProgreso === undefined) {
            unidadProgreso = Math.ceil(100 / lista_facturas.length)
        }

        if (progreso === undefined) {
            progreso = 0
        }

        //id_factura = lista_facturas.shift();

        varajaxInit = crearAjax();
        valores_a_mandar = "";
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.onreadystatechange = function () { respuestaTipificacion() }
        valores_a_mandar = "accion=tipificarFactura";
        valores_a_mandar = valores_a_mandar + "&lblIdFacturas=" + lista_facturas;
        varajaxInit.send(valores_a_mandar);
    }
}

function respuestaTipificacion() {
    if(varajaxInit.readyState == 4){
        alert("PROCESO DE TIPIFICACION TERMINADO.")
        $("#btnTipificacion").show();
        $("#loaderTipificacion").hide();
        $("#progreso").text(0);


        switch (ventanaActual.opc) {
            case 'rips':
                buscarFacturacion("listGrillaDetallesArchivoRips",
                buscarFacturacion("listGrillaDetallesPendientesArchivoRips"))
                break;

            case 'Facturas':
                buscarFacturacion('listGrillaFacturas')
                break;
        }
    }
}

function respuestaTipificarFacturas(lista_facturas, unidadProgreso, progreso) {
    console.log(varajaxInit.readyState)
    if (varajaxInit.readyState == 4) {

        progreso = progreso + unidadProgreso
        if (progreso > 100) {
            progreso = 100
        }

        $("#progreso").text(progreso);

        if (lista_facturas.length > 0) {
            setTimeout(() => {
                tipificarFacturas(lista_facturas, unidadProgreso, progreso)
            }, 500);
        } else {
            alert("PROCESO DE TIPIFICACION TERMINADO.")
            $("#btnTipificacion").show();
            $("#loaderTipificacion").hide();
            $("#progreso").text(0);

            var progreso;
            var unidadProgreso;

            switch (ventanaActual.opc) {
                case 'rips':
                    buscarFacturacion("listGrillaDetallesArchivoRips",
                    buscarFacturacion("listGrillaDetallesPendientesArchivoRips"))
                    break;

                case 'Facturas':
                    buscarFacturacion('listGrillaFacturas')
                    break;
            }
        }
    }
}

function tipificarFactura(id_factura, callback) {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=tipificarFactura";
    valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + id_factura;
    varajaxInit.send(valores_a_mandar);
}

function verificarEnvioFactura() {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=auditarFactura";
    valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
    varajaxInit.send(valores_a_mandar);
}

function verificarEnvioRecibo() {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=auditarRecibo";
    valores_a_mandar = valores_a_mandar + "&lblIdRecibo=" + valorAtributo('lblIdRecibo');
    varajaxInit.send(valores_a_mandar);
}


function buscarInformacionBasicaPacienteParaAdmisiones(tipoServicio) {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestabuscarInformacionBasicaPacienteParaAdmisiones(tipoServicio) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=InformacionBasicaDelPaciente";
    valores_a_mandar = valores_a_mandar + "&id=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
    varajaxInit.send(valores_a_mandar);
}

function respuestabuscarInformacionBasicaPacienteParaAdmisiones(tipoServicio) {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('infoBasicaPaciente') != null) {
                totalRegistros = raiz.getElementsByTagName('identificacion').length;

                if (totalRegistros > 0) {
                    TipoIdPaciente = raiz.getElementsByTagName('TipoIdPaciente')[0].firstChild.data;
                    identificacion = raiz.getElementsByTagName('identificacion')[0].firstChild.data;
                    nombre1 = raiz.getElementsByTagName('Nombre1')[0].firstChild.data;
                    nombre2 = raiz.getElementsByTagName('Nombre2')[0].firstChild.data;
                    apellido1 = raiz.getElementsByTagName('Apellido1')[0].firstChild.data;
                    apellido2 = raiz.getElementsByTagName('Apellido2')[0].firstChild.data;
                    idMunicipio = raiz.getElementsByTagName('idMunicipio')[0].firstChild.data;
                    nomMunicipio = raiz.getElementsByTagName('nomMunicipio')[0].firstChild.data;
                    Direccion = raiz.getElementsByTagName('Direccion')[0].firstChild.data;
                    Telefonos = raiz.getElementsByTagName('Telefonos')[0].firstChild.data;
                    Celular1 = raiz.getElementsByTagName('Celular1')[0].firstChild.data;
                    Celular2 = raiz.getElementsByTagName('Celular2')[0].firstChild.data;
                    email = raiz.getElementsByTagName('email')[0].firstChild.data;
                    Etnia = raiz.getElementsByTagName('Etnia')[0].firstChild.data;
                    NivelEscolar = raiz.getElementsByTagName('NivelEscolar')[0].firstChild.data;
                    idOcupacion = raiz.getElementsByTagName('idOcupacion')[0].firstChild.data;
                    Ocupacion = raiz.getElementsByTagName('Ocupacion')[0].firstChild.data;
                    Estrato = raiz.getElementsByTagName('Estrato')[0].firstChild.data;

                    fechaNac = raiz.getElementsByTagName('fechaNac')[0].firstChild.data;
                    edad = raiz.getElementsByTagName('edad')[0].firstChild.data;
                    sexo = raiz.getElementsByTagName('sexo')[0].firstChild.data;
                    acompanante = raiz.getElementsByTagName('acompanante')[0].firstChild.data;
                    administradora = raiz.getElementsByTagName('administradora')[0].firstChild.data;

                    asignaAtributo('cmbTipoId', TipoIdPaciente, 0)
                    asignaAtributo('txtIdentificacion', identificacion, 0)
                    asignaAtributo('txtNombre1', nombre1, 0)
                    asignaAtributo('txtNombre2', nombre2, 0)
                    asignaAtributo('txtApellido1', apellido1, 0)
                    asignaAtributo('txtApellido2', apellido2, 0)
                    asignaAtributo('txtMunicipio', idMunicipio + '-' + nomMunicipio, 0)
                    asignaAtributo('txtDireccionRes', Direccion, 0)
                    asignaAtributo('txtTelefonos', Telefonos, 0)
                    asignaAtributo('txtCelular1', Celular1, 0)
                    asignaAtributo('txtCelular2', Celular2, 0)
                    asignaAtributo('txtEmail', email, 0)
                    asignaAtributo('cmbIdEtnia', Etnia, 0)
                    asignaAtributo('cmbIdNivelEscolaridad', NivelEscolar, 0)
                    asignaAtributo('txtIdOcupacion', idOcupacion + '-' + Ocupacion, 0)
                    asignaAtributo('cmbIdEstrato', Estrato, 0)

                    asignaAtributo('txtNomAcompanante', acompanante, 0);
                    asignaAtributo('txtFechaNac', fechaNac, 0)
                    asignaAtributo('cmbSexo', sexo, 0)
                    asignaAtributo('txtAdministradoraPaciente', administradora, 0)	

                    verificarNotificacionAntesDeGuardar('validarIdentificacionPaciente')
                } else {
                    alert('PACIENTE NO EXISTE')
                }
            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}

function calcularValorTotalRecibo(idElemento) {
    var valor = parseInt(valorAtributo('txtValorUnidadRecibo'));
    var canti = parseInt(valorAtributo('cmbCantidadRecibo'));
    subTotal = valor * canti;
    subTotal = subTotal.toFixed(0)
    asignaAtributo('lblValorRecibo', subTotal, 0);
}

function cargarSitioLab() {
    $('#cmbIdSitioQuirurgicoLaboratorio > option[value="10"]').attr('selected', 'selected');
}

function cargarSitioTer() {
    $('#cmbIdSitioQuirurgicoTerapia > option[value="10"]').attr('selected', 'selected');
}

function cargarSitioADX() {
    switch (valorAtributo('cmbCantidadAD')) {
        case '2':
            $('#cmbIdSitioQuirurgicoAD > option[value="3"]').attr('selected', 'selected');
            break;
    }
}

function cargarCantidad() {

    switch (valorAtributo('cmbIdSitioQuirurgicoAD')) {
        case '3':
            $('#cmbCantidadAD > option[value="2"]').attr('selected', 'selected');
            break;
    }
}

function setIndicacion() {
    if (valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica') == '95140101') {
        asignaAtributo('txtIndicacionAD', 'OCT ', 0)
    }
}

function calcularValorExedenteCuenta() {
    var noCubi = parseInt(valorAtributo('lblValorNoCubierto'));
    var valMax = parseInt(valorAtributo('txtValorMaximoEvento'));
    if (noCubi > valMax)
        asignaAtributo('lblValorExedente', parseFloat(noCubi - valMax).toFixed(0), 0)
    else {
        alert('EL VALOR NO PUEDE SER MAYOR DEL VALOR NO CUBIERTO')
        asignaAtributo('txtValorMaximoEvento', '', 0)
        asignaAtributo('lblValorExedente', '', 0)
    }
}

function calculoDescuentoPorcentajeCuenta() {

    if (valorAtributo('cmbPorcentajeDescuentoCuenta') == '') {
        asignaAtributo('txtValorDescuentoCuenta', '', 0)
    } else {

        var tot = parseInt(valorAtributo('lblValorBaseDescuento'));
        var porc = parseInt(valorAtributo('cmbPorcentajeDescuentoCuenta'));
        asignaAtributo('txtValorDescuentoCuenta', parseFloat(tot * (porc / 100)).toFixed(2), 1)
    }
}


function calculoDescuentoPorcentajeVentas() {

    if (valorAtributo('cmbPorcentajeDescuentoVentanita') == '') {
        asignaAtributo('txtValorDescuentoVentanita', '', 1)
    } else {

        var tot = parseInt(valorAtributo('lblValorBaseVentanita'));
        var porc = parseInt(valorAtributo('cmbPorcentajeDescuentoVentanita'));
        asignaAtributo('txtValorDescuentoVentanita', parseFloat(tot * (porc / 100)).toFixed(2), 1)
    }
}

function calculoDescuentoPorcentajeVentasOtro() {

    if (valorAtributo('cmbPorcentajeDescuentoOtroVentanita') == '') {
        asignaAtributo('txtValorDescuentoOtroVentanita', '', 0)
    } else {

        var tot = parseInt(valorAtributo('lblValorBaseOtroVentanita'));
        var porc = parseInt(valorAtributo('cmbPorcentajeDescuentoOtroVentanita'));
        asignaAtributo('txtValorDescuentoOtroVentanita', parseFloat(tot * (porc / 100)).toFixed(2), 1)
    }
}


function consultarGrillasDiasPrincipalDespachoProgramacion() {
    buscarAGENDA('listDiasProgramacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
}

function selectConciliacionMedicamentosa() {
    if (valorAtributo('cmbExiste') == 'SI') {
        asignaAtributo('txtConciliacionDescripcion', '', 0)
    } else asignaAtributo('txtConciliacionDescripcion', 'LOS MEDICAMNETOS FORMULADOS, NO GENERAN NINGUNA INTERACCION MEDICAMENTOSA', 0)

}

function guardarContenidoDocumentoAYD() {

    if (IdSesion() == '1020401863' && valorAtributo('lblTipoDocumento') == 'ADAN') { /*DR ROJAS*/
        asignaAtributo('txtIdProfesionalElaboro', '1020401863')
        //		alert('2.txtIdProfesionalElaboroJJJJ='+  valorAtributo('txtIdProfesionalElaboro')   +' IdSesion= '+IdSesion() )		
    }
    if (IdSesion() == '13061897' && valorAtributo('lblTipoDocumento') == 'ADAN') { /*DR CARLOS*/
        asignaAtributo('txtIdProfesionalElaboro', '13061897')
        //		alert('2.txtIdProfesionalElaboroJJJJ='+  valorAtributo('txtIdProfesionalElaboro')   +' IdSesion= '+IdSesion() )		
    }
    if (IdSesion() == '13061897' && valorAtributo('lblTipoDocumento') == 'ADAG') {
        asignaAtributo('txtIdProfesionalElaboro', '13061897')
        //		alert('2.txtIdProfesionalElaboroJJJJ='+  valorAtributo('txtIdProfesionalElaboro')   +' IdSesion= '+IdSesion() )		
    }



    if (valorAtributo('txtIdProfesionalElaboro') == IdSesion()) {

        if (valorAtributo('txtIdEsdadoDocumento') == 1) {
            alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 8) {
            alert('EL ESTADO FINALIZADO URGENTE DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 7) {
            alert('EL ESTADO CANCELADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 9) {
            alert('EL ESTADO CANCELADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else guardarContenidoDocumento()

    } else alert('USTED NO ES EL AUTOR DE ESTE FOLIO \n NO PODRA GUARDAR LOS CAMBIOS')

}

function copiarJustificacionAnexo() {
    //    document.getElementById("lbl_JUST_NOPOS").value=document.getElementById("txt_JUST").value;

    asignaAtributo('lbl_JUST_NOPOS', '', 0)
    asignaAtributo('lbl_JUST_NOPOS', $('#drag' + ventanaActual.num).find('#txt_JUST').val().trim(), 0)

}

function traerTextoPredefinidoBiometria() {
    asignaAtributo('txt_gestionarPaciente', '\nOJO DERECHO\n1.Constante=\n2.Observaciones=  \n\nOJO IZQUIERDO\n1.Constante=\n2.Observaciones=\n-', 0)
}

function traerTextoPredefinidoAngio(idTextArea) {

    switch (idTextArea) {
        case '10':
            asignaAtributo('txt_ADAN_C10', 'DISCO ROSADO, BORDES DEFINIDOS EXCAVACION 2/10  VASOS DE EMERGENCIA CENTRAL DE FORMA Y CONFIGURACION NORMAL RETINA APLICADA, MACULA NORMAL.', 0)
            break;
        case '11':
            asignaAtributo('txt_ADAN_C11', 'CONFIRMA LOS HALLAZGOS.', 0)
            break;
        case '12':
            asignaAtributo('txt_ADAN_C12', 'TIEMPO DE LLENADO VENOSO Y COROIDEO EN LIMITES NORMALES.  DESDE ETAPAS ARTERIOVENOSAS INICIALES SE APRECIA FOCOS PUNTIFORMES DE HIPERFLUORESCENCIA PERIMACULAR QUE NO AUMENTAN NI FUGAN EN ETAPAS TARDIAS SECUNDARIO A ATROFIA DEL EPR PERIMACULAR', 0)
            break;
        case '13':
            asignaAtributo('txt_ADAN_C13', '', 0)
            break;
        case '14':
            asignaAtributo('txt_ADAN_C14', '', 0)
            break;
        case '15':
            asignaAtributo('txt_ADAN_C15', '', 0)
            break;
        case '16':
            asignaAtributo('txt_ADAN_C16', 'SE SIGIERE CORRACIONAR CON LA CLINICA', 0)
            break;
    }
}

function getAbsoluteElementPositionX(element) {

    var elemento = document.getElementById(element)

    if (typeof element == "string")
        element = document.getElementById(element)

    if (!element) return { top: 0, left: 0 };

    var y = 0;
    var x = 0;
    while (element.offsetParent) {
        x += element.offsetLeft;
        y += element.offsetTop;
        element = element.offsetParent;
    }
    return { left: x };
}

function getAbsoluteElementPositionY(element) {

    var elemento = document.getElementById(element)

    if (typeof element == "string")
        element = document.getElementById(element)

    if (!element) return { top: 0, left: 0 };

    var y = 0;
    var x = 0;
    while (element.offsetParent) {
        x += element.offsetLeft;
        y += element.offsetTop;
        element = element.offsetParent;
    }
    return { top: y };
}

function traerVentanitaHos(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {

    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hospitalizacion/ventanita.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaHos(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}


function llenartraerVentanitaHos(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function llenartraerVentanitaFuncionesHCPlantilla(divPaVentanita, id, name, value) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            guardarDatosPlantilla(id, name, value);
            limpiarListadosTotales('listGrillaVentana');
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}



function llenartraerVentanitaFuncionesHC(divPaVentanita, accion) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            asignaAtributo("txtAccionVentintaHC", accion, 0)
            limpiarListadosTotales('listGrillaVentana');
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function traerVentanitaUrgencias(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/facturacion/admisiones/ventanita.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanita(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function traerVentanitaFuncionesHcPlantilla(idLupitaVentanita, accion, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaFuncionesHCPlantilla.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaFuncionesHC(divPaVentanita, accion) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function traerVentanitaFuncionesHc(idLupitaVentanita, accion, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaFuncionesHC.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaFuncionesHC(divPaVentanita, accion) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function traerVentanita(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanita.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanita(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function traerVentanitaPaciente(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaPaciente.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaPaciente(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}
function llenartraerVentanitaPerfiles(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#cmbEstado').focus();
            limpiarListadosTotales('listGrillaOpcionRoles');
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}


function traerVentanitaPerfiles(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaPerfiles.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaPerfiles(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}


function traerVentanitaFirmaCliente(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {

    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaFirmaCliente.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanita(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenartraerVentanitaHos(divPaVentanita, accion) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            asignaAtributo("txtAccionVentintaHC", accion, 0)
            limpiarListadosTotales('listGrillaVentana');
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function llenartraerVentanita(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function llenartraerVentanitaPaciente(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');
            asignaAtributo("lblVentanita", divPaVentanita, 0)

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaPaciente');
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}


function traerVentanitaCondicion1Autocompletar(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {

    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top - 200;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaCondicion.jsp?condicion1=' + valorAtributoIdAutoCompletar(filtro), true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaCondicion1(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function traerVentanitaCondicion1(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {

    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaCondicion.jsp?condicion1=' + valorAtributo(filtro), true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaCondicion1(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}
function traerVentanitaEmpresa(idLupitaVentanita, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;
    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaCondicionEmpresa.jsp?condicion1=' + IdEmpresa(), true);
    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaCondicion1(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenartraerVentanitaCondicion1(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuestaCondicion1');

            var d = document.getElementById('ventanitaHijaCondicion1');
            d.style.position = "fixed";
            //alert(d.style.top)
            //d.style.position = "absolute";
            //d.style.top = topY + 'px';

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}




var idArticulo;

function traerVentanitaArticulo(elemInputDes, query) {
    if (valorAtributo('cmbIdBodega') != '6') {
        idQuery = query;
        elemInputDestino = elemInputDes;
        varajaxMenu = crearAjax();
        valores_a_mandar = "";
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaArticulo.jsp', true);
        varajaxMenu.onreadystatechange = llenartraerVentanitaArticulo;
        varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxMenu.send(valores_a_mandar);
    } else alert('SOLO SE PERMITE A BODEGAS DIFERENTES DE OPTICA')
}

function llenartraerVentanitaArticulo() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaVentanitaArticulo").innerHTML = varajaxMenu.responseText;
            asignaAtributo('txtCodBus', idArticulo, 0);
            mostrar('divParaVentanitaArticulo');
            mostrar('divVentanitaPuestaArticulo');
            if (valorAtributo('txtCodBus') != '') { setTimeout("buscarHistoria('listGrillaVentanaArticulo')", 400); }
            // document.getElementById("txtNomBus").onfocus
            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();

        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}


function abrirVistaPreviaEvolucion(idEvolucion) {
    alert(idEvolucion)
}


function agregarTablaHistoricosAtencion(idReporte, parametro1, cantColumnasQuery, tablaReportesDestino) {
    mostrar('divDocumentosHistoricos')
    cantColumnasP = cantColumnasQuery;
    idTablaReportesExcel = tablaReportesDestino;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarReportes_xml.jsp', true);
    varajaxInit.onreadystatechange = respuestallenarTablaHistoricosAtencion;

    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=listadoReporteExcel";
    valores_a_mandar = valores_a_mandar + "&id=" + idReporte;
    valores_a_mandar = valores_a_mandar + "&parametro1=" + parametro1; //+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
    varajaxInit.send(valores_a_mandar);
}


function respuestallenarTablaHistoricosAtencion() {
    if (varajaxInit.readyState == 4) {
        //   VentanaModal.cerrar();
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('rows') != null) {

                totalRegistros = raiz.getElementsByTagName('c1').length;

                if (totalRegistros) {
                    agregarDatosTablaHistoricosAtencion(raiz, totalRegistros);
                } else {
                    alert("Sin datos en el reporte, cierre y vuelva a generarlo");
                }
            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) {
        //	abrirVentana(260, 100);
        //  VentanaModal.setSombra(true);
    }
}


function agregarTablaDeVistaPrevia() {

    borrarRegistrosTabla('listDocumentosHistoricos');
    var cantFilas = 0,
        estilo = 'inputBlanco';

    tablabody = document.getElementById('listDocumentosHistoricos').lastChild;
    Nregistro = document.createElement("TR");
    TD = document.createElement("TD");
    Ncampo1 = document.createElement("IMG");
    Ncampo1.setAttribute('src', '/clinica/utilidades/imagenes/acciones/abajoFlecha.png');
    Ncampo1.setAttribute('TITLE', 'VER VISTA PREVIA');
    Ncampo1.setAttribute('width', '16');
    Ncampo1.setAttribute('height', '16');
    Ncampo1.setAttribute('align', 'left');
    TD.appendChild(Ncampo1);
    Nregistro.appendChild(TD);
    tablabody.appendChild(Nregistro);


    var ids = jQuery("#listDocumentosHistoricos").getDataIDs();
    for (var i = 0; i < ids.length; i++) {
        var c = ids[i];
        var datosRow = jQuery("#listDocumentosHistoricos").getRowData(c);


        Nregistro = document.createElement("TR");
        Nregistro.id = datosRow.id_evolucion;
        Nregistro.className = estilo;

        TD = document.createElement("TD");
        Ncampo1 = document.createElement("IMG");
        Ncampo1.setAttribute('src', '/clinica/utilidades/imagenes/acciones/buscar.png');
        Ncampo1.setAttribute('width', '16');
        Ncampo1.setAttribute('TITLE', datosRow.id_evolucion);
        Ncampo1.setAttribute('height', '16');
        Ncampo1.setAttribute('align', 'left');
        Ncampo1.setAttribute('onclick', "abrirVistaPreviaEvolucion(" + datosRow.id_evolucion + ");");
        TD.appendChild(Ncampo1);
        Nregistro.appendChild(TD);
        tablabody.appendChild(Nregistro);
    }

}



/* Para la impresion de los documentos con el array */
var ordenImpresion = new Array();
var POSICION_VECTOR = 0;
var POSICION_VECTOR_MAX = 0;
/*la impresion empieza cn esta funcion que es llamada en la linea 1205*/
function llenarArregloImpresion() {

    POSICION_VECTOR = 0;
    if (bandFormOftal == 'SI') {
        POSICION_VECTOR_MAX = 6;
    } else POSICION_VECTOR_MAX = 14;


    ordenImpresion[1] = 1;
    ordenImpresion[2] = 2;
    ordenImpresion[3] = 3;
    ordenImpresion[4] = 4;


    if (valorAtributo('txtIdDocumentoVistaPrevia') == '')
        TIPO_DOCUMENTO = valorAtributo('lblTipoDocumento')
    else
        TIPO_DOCUMENTO = tipo_evolucion

    //alert('TIPO_DOCUMENTO= '+TIPO_DOCUMENTO)

    //TIPO_DOCUMENTO = valorAtributo('lblTipoDocumento') 

    if (TIPO_DOCUMENTO == 'HQUI' || TIPO_DOCUMENTO == 'HQLA' || TIPO_DOCUMENTO == 'LICH' || TIPO_DOCUMENTO == 'NENF' || TIPO_DOCUMENTO == 'RANA' || TIPO_DOCUMENTO == 'RPRE' || TIPO_DOCUMENTO == 'EPIC') {
        ordenImpresion[5] = 5;

    } else ordenImpresion[5] = 55; /* validar el query de hqui y hqla cuando hay dos hc*/

    ordenImpresion[6] = 6;

    if (TIPO_DOCUMENTO == 'HQUI' || TIPO_DOCUMENTO == 'HQLA' || TIPO_DOCUMENTO == 'LICH' || TIPO_DOCUMENTO == 'NENF' || TIPO_DOCUMENTO == 'RANA' || TIPO_DOCUMENTO == 'RPRE' || TIPO_DOCUMENTO == 'EPIC' || TIPO_DOCUMENTO == 'EVME')
        ordenImpresion[7] = 111;
    else ordenImpresion[7] = 7;
    ordenImpresion[8] = 8;

    if (TIPO_DOCUMENTO == 'LICH' || TIPO_DOCUMENTO == 'NENF') {
        ordenImpresion[9] = 111;

    } else ordenImpresion[9] = 9;


    if (TIPO_DOCUMENTO == 'LICH' || TIPO_DOCUMENTO == 'NENF') {
        ordenImpresion[10] = 111;
    } else ordenImpresion[10] = 10;

    ordenImpresion[11] = 11;
    ordenImpresion[12] = 12;
    ordenImpresion[13] = 13;

    ocuparCandadoPaReporte(); /*inserta en candado y llama a  recorrerImpresion()*/
    //recorrerImpresion();	
}

function recorrerImpresion() {

    POSICION_VECTOR++; //alert(POSICION_VECTOR)
    if (POSICION_VECTOR < POSICION_VECTOR_MAX) {
        if (bandFormOftal == 'SI') {
            // alert('impr anexo, ban: '+bandFormOftal);
            imprimirSincronicoPdfAnexoOptalmica(ordenImpresion[POSICION_VECTOR]);
        } else imprimirSincronicoPdf(ordenImpresion[POSICION_VECTOR]);
    } else {
        LiberarCandadoPaReporte();
        POSICION_VECTOR = 0;
        POSICION_VECTOR_MAX = 0;
        asignaAtributo('txtIdDocumentoVistaPrevia', '')
        asignaAtributo('lblIdAdmisionVistaPrevia', '')


        if (totalRegistrosTabla('listSistemasPDF') == 1)
            ocultar('idDivRevisionSistemas')
        if (totalRegistrosTabla('listDiagnosticosPDF') == 1)
            ocultar('idDivDiagnosticos')

        if (totalRegistrosTabla('listPlanPDF') == 1)
            ocultar('idDivPlan')
        if (totalRegistrosTabla('listMedicamentosPDF') == 1)
            ocultar('idDivMedicamentos')

    }
}

/**/


function cargarDxHistoricos() {
    if (valorAtributo('lblIdDocumento') != '') {
        mostrar('divVentanitaDxHistorico')
        buscarHC('listDiagnosticosHistorico', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
    } else alert('SELECCIONE DOCUMENTO CLINICO')

}

function cargarProcedimientosHistoricos() {
    if (valorAtributo('lblIdDocumento') != '') {
        mostrar('divVentanitaProcedimCuentaientosHistoricos')
        buscarHC('listProcedimientosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
    } else alert('SELECCIONE DOCUMENTO CLINICO')

}

function cargarRefrescaDocClinico() { // para  enviar archivo  UPC
    if (valorAtributo('lblIdDocumento') != '') {
        mostrar('divContenidos')
        llenarTablaAlAcordion(valorAtributo('lblIdDocumento'), valorAtributo('lblTipoDocumento'), valorAtributo('lblDescripcionTipoDocumento'))
    } else alert('SELECCIONE DOCUMENTO CLINICO')

}


function ventanaVerificacionAdmision() {
    limpiaAtributo('cmbTipoIdVerifica', 0)
    limpiaAtributo('txtIdentificacionVerifica', 0)
    mostrar('divVentanitaVerificacion')
}

function ventanaVerificacionAdmisionFactura() {

    if (valorAtributo('cmbIdTipoServicio') == 'HOS' ||
        valorAtributo('cmbIdTipoServicio') == 'URG' ||
        valorAtributo('cmbIdTipoServicio') == 'LAB' ||
        valorAtributo('cmbIdTipoServicio') == 'HME' ||
        valorAtributo('cmbIdTipoServicio') == 'ORD' ||
        valorAtributo('cmbIdTipoServicio') == 'APT') { /*validar aqui y el resto es transparente, pone bandera*/
        asignaAtributo('lblIdCita', 'sin_cita')
    }

    if (verificarCamposGuardar('crearAdmisionFactura')) {
        numero_cita_seleccionada = $("#listAgendaAdmision").jqGrid('getGridParam', 'selrow')

        limpiaAtributo('cmbTipoIdVerifica', 0)
        limpiaAtributo('txtIdentificacionVerifica', 0)

        asignaAtributo('cmbTipoIdVerifica', $("#listAgendaAdmision").getRowData(numero_cita_seleccionada).TIPO_ID, 0)
        asignaAtributo('txtIdentificacionVerifica', $("#listAgendaAdmision").getRowData(numero_cita_seleccionada).IDENTIFICACION, 0)

        verificarNotificacionAntesDeGuardar('validarTipoCitaFactura')
    }
}


function verificacionCrearAdmision() {

    if (valorAtributo('cmbTipoId') == valorAtributo('cmbTipoIdVerifica')) {
        if (valorAtributo('txtIdentificacion') == valorAtributo('txtIdentificacionVerifica')) {

            var lista = jQuery("#listCitaCirugiaProcedimiento").getDataIDs().length;

            if (lista == 0) {
                verificarNotificacionAntesDeGuardar('crearAdmision')
            } else {
                verificarNotificacionAntesDeGuardar('crearAdmisionCirugia')
            }
            ocultar('divVentanitaVerificacion')
        } else {
            alert('RECTIFIQUE NUMERO IDENTIFICACION')
            limpiaAtributo('txtIdentificacion', 0)
            ponerFoco('txtIdentificacion', 0)
            ocultar('divVentanitaVerificacion')
        }
    } else {
        alert('RECTIFIQUE TIPO DE DOCUMENTO')
        limpiaAtributo('cmbTipoId', 0)
        ponerFoco('cmbTipoId', 0)
        ocultar('divVentanitaVerificacion')
    }

}

function crearAdmisionSinFactura() {

    if (valorAtributo('cmbTipoId') === valorAtributo('cmbTipoIdVerifica') &&
        valorAtributo('txtIdentificacion') === valorAtributo('txtIdentificacionVerifica')) {
        //verificarNotificacionAntesDeGuardar('crearAdmisionSinFactura');    
        modificarCRUD('crearAdmisionSinFactura');
    } else {
        alert('RECTIFIQUE NUMERO DE IDENTIFICACION')
        ponerFoco('txtIdentificacion', 0)
    }

    ocultar('divVentanitaVerificacion')
}

function crearAdmisionConFactura() {
    if (valorAtributo('cmbTipoId') === valorAtributo('cmbTipoIdVerifica') &&
        valorAtributo('txtIdentificacion') === valorAtributo('txtIdentificacionVerifica')) {
        verificarNotificacionAntesDeGuardar('crearAdmisionFactura');
    } else {
        alert('RECTIFIQUE NUMERO DE IDENTIFICACION')
        ponerFoco('txtIdentificacion', 0)
    }
    ocultar('divVentanitaVerificacion')
}

function limpiarArchivoAdjunto() { // para  eliminar archivo  UPC
    if (confirm('Seguro de LIMPIAR :: ' + valorAtributo('cmbTipoArchivoSubir') + ' de la base de datos ?')) {
        switch (tabActivo) {
            case 'divArchivosAdjuntos':
                //if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=limpia' + '&subCarpeta=fisico';
                document.forms['formUpcElimina'].submit();
                //} else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA LIMPIAR');
                break;
            case 'divListadoIdentificacion':
                //if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=limpia' + '&subCarpeta=identificacion';
                document.forms['formUpcElimina'].submit();
                //} else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA LIMPIAR');
                break;
            case 'divListadoRemision':
                if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=limpia' + '&subCarpeta=remision';
                    document.forms['formUpcElimina'].submit();
                } else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA LIMPIAR');
                break;
            case 'divListadoCarnet':
                if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=limpia' + '&subCarpeta=carnet';
                    document.forms['formUpcElimina'].submit();
                } else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA LIMPIAR');
                break;
            case 'divListadoAyudasDiagnosticas':
                if (valorAtributo('txtIdEsdadoDocumento') == 1) {
                    alert('EL ESTADO DEL DOCUMENTO NO PERMITE ELIMINAR.1')
                } else if (valorAtributo('txtIdEsdadoDocumento') == 8) {
                    alert('EL ESTADO DEL DOCUMENTO NO PERMITE ELIMINAR.8')
                } else {
                    if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                        //if(valorAtributo('txtIdProfesionalElaboro')==IdSesion()){ 
                        document.forms['formUpcElimina'].action = "/clinica/servlet/UPC2?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=limpia' + '&subCarpeta=ayudaDx';
                        document.forms['formUpcElimina'].submit();

                    } else alert('USTED NO ES EL AUTOR DE ESTE FOLIO \n NO PODRA LIMPIAR')

                }
                break;
            case 'divListadoOtrosDocumentos':
                if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=limpia' + '&subCarpeta=otros';
                    document.forms['formUpcElimina'].submit();
                } else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA LIMPIAR');
                break;
        }
    }
}

function formatearFecha(fecha) {
    var f;
    if (fecha != '') {
        f = fecha.split('-');
        return f[2] + '/' + f[1] + '/' + f[0];
    } else {
        return '';
    }
}

function desformatearFecha(fecha) {
    var f;
    if (fecha != '') {
        f = fecha.split('/');
        return f[2] + '-' + f[1] + '-' + f[0];
    } else {
        return '';
    }
}

function deDDMMYYYY_a_MMDDYYYY(fecha) {
    var f;
    if (fecha != '') {
        f = fecha.split('/');
        return f[1] + '/' + f[0] + '/' + f[2];
    } else {
        return '';
    }
}

function calcula_utilizado(id) {
    alert(id)
}

function calculoIMC() {
    if (valorAtributo('txtPeso') != '' && valorAtributo('txtTalla')) {
        var peso = parseInt(valorAtributo('txtPeso'));
        var talla = parseInt(valorAtributo('txtTalla'));
        asignaAtributo('lblIMC', parseFloat(peso / ((talla * 0.01) * (talla * 0.01))).toFixed(2), 0)
    } else asignaAtributo('lblIMC', '', 0)
}


/**************************************************************/

function llenarTablaAlAcordion(idEvolucion, tipoEvolucion, nombreEvolucion) {
    ocultarAcordionesSegunTipoDocumento(tipoEvolucion)
    buscarHC('listAntFarmacologicos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
    ruta = '/clinica/paginas/hc/documentosHistoriaClinica/'
    pagina = tipoEvolucion + '.jsp';
    idDivTitulo = 'divContenidos';
    $('#drag' + ventanaActual.num).find('#' + idDivTitulo + idDivTitulo).html($.trim(nombreEvolucion)); /*para el titulo del acordion*/
    cargarMenuTable(ruta + pagina, tipoEvolucion, idEvolucion);
    setTimeout(() => {
        cargarFormulas(tipoEvolucion, idEvolucion);
    }, 200);
    setTimeout(() => {
        cargarValidaciones(tipoEvolucion);
    }, 300);
    setTimeout(() => {
        cargarValoresValidaciones(tipoEvolucion, idEvolucion);
    }, 400);

}

function cargarMenuTable(pagina, tipoEvolucion, idEvolucion) {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", pagina, true);
    varajaxMenu.onreadystatechange = function () { llenarinfoPagina(tipoEvolucion, idEvolucion) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);

}

function llenarinfoPagina(tipoEvolucion, idEvolucion) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaPagina").innerHTML = varajaxMenu.responseText;

            traerContenidoDocumentoSeleccionado(tipoEvolucion, idEvolucion, 'txt_')


            if (tipoEvolucion == 'ADAN') {
                calendario('txt_ADAN_C18', 0);
            }

            if (tipoEvolucion == 'ECOO') {
                calendario('txt_ECOO_C1', 0);
                calendario('txt_ECOO_C2', 0);
            }

            if (tipoEvolucion == 'PSIE') {
                calendario('txt_PSIE_C2', 0);
            }
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

/**************************************************************/

function cargarFormulas(tipoEvolucion, idEvolucion) {
    //pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width() - 50;
    valores_a_mandar = pag;
    valores_a_mandar = valores_a_mandar + "?idQuery=1801&parametros=";
    add_valores_a_mandar(tipoEvolucion);
    add_valores_a_mandar(tipoEvolucion);
    add_valores_a_mandar(idEvolucion);
    $("#drag" + ventanaActual.num)
        .find("#listGrillaFormulas")
        .jqGrid({
            url: valores_a_mandar,
            datatype: "xml",
            mtype: "GET",

            colNames: [
                "Cont", "CAMPO", "VALOR", "FORMULAS"
            ],
            colModel: [
                { name: "contador", index: "contador", width: anchoP(ancho, 5) },
                { name: "CAMPO", index: "CAMPO", width: anchoP(ancho, 15) },
                { name: "VALOR", index: "VALOR", width: anchoP(ancho, 40) },
                { name: "FORMULAS", index: "FORMULAS", width: anchoP(ancho, 40) }
            ],

            //  pager: jQuery('#pagerGrilla'),
            height: 250,
            width: ancho + 40,

            /*onSelectRow: function (rowid) {
                var datosRow = jQuery("#drag" + ventanaActual.num)
                    .find("#listGrillaFolios")
                    .getRowData(rowid);
            },*/
        });
    $("#drag" + ventanaActual.num)
        .find("#listGrillaFormulas")
        .setGridParam({ url: valores_a_mandar })
        .trigger("reloadGrid");
}

/**************************************************************/

function cargarValidaciones(tipoEvolucion) {
    //pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width() - 50;
    valores_a_mandar = pag;
    valores_a_mandar = valores_a_mandar + "?idQuery=1802&parametros=";
    add_valores_a_mandar(tipoEvolucion);
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValidaciones")
        .jqGrid({
            url: valores_a_mandar,
            datatype: "xml",
            mtype: "GET",

            colNames: [
                "Cont", "CODIGO", "CAMPO", "OPERADOR", "VALOR", "CAMPO_COMPARAR", "TIPO"
            ],
            colModel: [
                { name: "contador", index: "contador", width: anchoP(ancho, 5) },
                { name: "CODIGO", index: "CODIGO", width: anchoP(ancho, 15) },
                { name: "CAMPO", index: "CAMPO", width: anchoP(ancho, 15) },
                { name: "OPERADOR", index: "OPERADOR", width: anchoP(ancho, 15) },
                { name: "VALOR", index: "VALOR", width: anchoP(ancho, 15) },
                { name: "CAMPO_COMPARAR", index: "CAMPO_COMPARAR", width: anchoP(ancho, 15) },
                { name: "TIPO", index: "TIPO", width: anchoP(ancho, 15) }
            ],
            height: 250,
            width: ancho + 40,
            gridview: true,
            rowattr: function (rd) {
                /*if (rd.GroupHeader === "1") { // verify that the testing is correct in your case
                    return {"class": "myAltRowClass"};
                }*/
                return { "class": rd.CODIGO }
            }
        });
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValidaciones")
        .setGridParam({ url: valores_a_mandar })
        .trigger("reloadGrid");
}

/**************************************************************/

function cargarValoresValidaciones(tipoEvolucion, idEvolucion) {
    //pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width() - 50;
    valores_a_mandar = pag;
    valores_a_mandar = valores_a_mandar + "?idQuery=1804&parametros=";
    add_valores_a_mandar(tipoEvolucion);
    add_valores_a_mandar(idEvolucion);
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValoresValidaciones")
        .jqGrid({
            url: valores_a_mandar,
            datatype: "xml",
            mtype: "GET",

            colNames: [
                "Cont", "CAMPO", "VALOR"
            ],
            colModel: [
                { name: "contador", index: "contador", width: anchoP(ancho, 5) },
                { name: "CAMPO", index: "CAMPO", width: anchoP(ancho, 25) },
                { name: "VALOR", index: "VALOR", width: anchoP(ancho, 70) }
            ],
            height: 250,
            width: ancho + 40,
            gridview: true
        });
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValoresValidaciones")
        .setGridParam({ url: valores_a_mandar })
        .trigger("reloadGrid");
}

/**************************************************************/

function tabActivoDireccionamiento(idTab) {
    switch (idTab) {
        case "divListaEspera":
            calendario('txtFechaCita', 0);
            buscarHC("listaEsperaProgramacion", "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp")
            setTimeout(() => {
                buscarHC("citaProgramacion", "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp")
            }, 200);
            break;
    }
}

function TabActivoConductaTratamiento(idTab) {
    tabActivo = idTab;
    switch (idTab) {
        case 'divAdministracionPaciente':
            buscarHC('listOrdenesPacienteURG', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            calendario("txtFechaMuerte", 1)
            setTimeout(() => {
                cargarComboGRALCondicion1('', '', 'cmbOrdenenesAdmisionURG', 160, valorAtributo('lblIdAdmision'))
            }, 200);
            break;

        case 'divConsentimientos':
            if (valorAtributo('txtIdEsdadoDocumento') == 0 || valorAtributo('txtIdEsdadoDocumento') == 12) {
                document.getElementById('btnCrearConsentimiento').disabled = false;
            } else {
                document.getElementById('btnCrearConsentimiento').disabled = true;
            }
            buscarHC('listConsentimientosPaciente', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            break;

        case 'divProcedimiento':
            buscarHC('listaProcedimientoConductaTratamiento', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoP','lblIdDocumento')", 500);

            break;

        // case 'divIncapacidad':
        //     buscarHC('listProcedimientosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
        //     setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoP','lblIdDocumento')", 500);
        //     break;
        case 'divAyudasDiagnosticas':
            buscarHC('listAyudasDiagnosticasDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoA','lblIdDocumento')", 500);
            break;
        case 'divConciliacionMedicamento':
            guardarYtraerDatoAlListado('buscarConciliacion')
            break;
        case 'divMedicacion':
            buscarHC('listMedicacion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            break;
        case 'divLaboratorioClinico':
            buscarHC('listLaboratoriosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoL','lblIdDocumento')", 500);
            break;
        case 'divTerapias':
            buscarHC('listTerapiaDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoT','lblIdDocumento')", 500);
            break;
        case 'listRemision':
            buscarReferencia('listRemision')
            break;
        // case 'listIncapacidad':
        //     buscarParametros('listIncapacidad')
        //     break;
        case 'divControl':
            cargarTableListaEspera();
            break;
        case 'divEventosAnestesia':
            /*RANA CIRUGIA ANASTESIA*/
            cargarUnaPagina('/clinica/paginas/hc/cirugia/anestesia.jsp', 'divParaPaginaEventosAnestesia');
            break;
        case 'divMonitoreoAnestesia':
            /*RANA CIRUGIA ANASTESIA*/
            cargarUnaPagina('/clinica/paginas/hc/cirugia/monitoreoAnestesia.jsp', 'divParaPaginaMonitoreoAnestesia');
            break;
        case 'divAdjuntos':
            /*RANA CIRUGIA ANASTESIA*/
            cargarUnaPagina('/clinica/paginas/hc/cirugia/adjuntos.jsp', 'divParaAdjuntos');
            break;
        case 'divArchivosAdjuntos':
            /*RANA CIRUGIA ANASTESIA*/
            buscarHistoria('listArchivosAdjuntos')
            break;
        case 'divListadoIdentificacion':
            buscarHistoria('listArchivosAdjuntosIdentificacion')
            break;
        case 'divListadoRemision':
            buscarHistoria('listArchivosAdjuntosRemision')
            break;
        case 'divListadoCarnet':
            buscarHistoria('listArchivosAdjuntosCarnet')
            break;
        case 'divListadoAyudasDiagnosticas':
            buscarHistoria('listAyudasDiagnost')
            break;
        case 'divListadoOtrosDocumentos':
            buscarHistoria('listArchivosAdjuntosVarios')
            break;

        case 'divPreOperatorio':
            /*RANA CIRUGIA ANASTESIA*/
            cargarUnaPagina('/clinica/paginas/hc/cirugia/preOperatorio.jsp', 'divParaPreOperatorio');
            break;
        case 'divPosOperatorio':
            /*RANA CIRUGIA ANASTESIA*/
            cargarUnaPagina('/clinica/paginas/hc/cirugia/posOperatorio.jsp', 'divParaPosOperatorio');
            break;
        case 'divPersonalProc':
            /*RANA CIRUGIA ANASTESIA*/
            cargarUnaPagina('/clinica/paginas/hc/cirugia/personalProc.jsp', 'divParaPersonalProc');
            break;

        case 'divOrdenesSubidas':
            buscarHistoria('listOrdenesArchivo');
            break;

        case 'divInterpretacion':
            buscarHistoria('listInterpretaciones');
            break;

        case 'divNuevaInterpretacion':
            buscarHistoria('listInterpretaciones');
            //buscarHistoria('listOrdenesArchivo');
            break;

        case 'divProgramacionTerapias':
            buscarAGENDA('listaProgramacionTerapias');
            calendario('txtFechaInicioTerapia', 1);
            calendario('txtFechaInicioTerapiaEditar', 1);
            break;

        case 'divTipificacion':
            buscarFacturacion("listArchivosTificacion");
            break;

        case 'divResultadosExternos':
            buscarHC('listaProcedimientoResultadosExternos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            break;

        case 'divSolicitudes':
            asignaAtributo("lblPacienteAdjuntos", valorAtributo("lblIdPaciente") + "-" + valorAtributo("lblIdentificacionPaciente") + "-" + valorAtributo("lblNombrePaciente"), 0)
            asignaAtributo("lblIdAdmisionAdjuntos", valorAtributo("lblIdAdmision"), 0)
            asignaAtributo("lblIdEvolucionAdjuntos", valorAtributo("lblIdDocumento"), 0)         
            buscarHC('listaProcedimientoConductaTratamientoS', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            buscarHistoria('listArchivosAdjuntosAnexos','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            break;

        // case 'divEducacionPaciente':
        //     buscarParametros('listGrillaEducacionPac')
        //     break;
    }
}
/***************************** cargar una pagina ******************************************/

function cargarUnaPagina(rutaPagina, divDestino) {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", rutaPagina, true);
    varajaxMenu.onreadystatechange = llenarcargarUnaPagina;
    varajaxMenu.onreadystatechange = function () { llenarcargarUnaPagina(divDestino) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarUnaPagina(divDestino) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divDestino).innerHTML = varajaxMenu.responseText;

            switch (divDestino) {
                case 'divParaAdjuntos':
                    buscarHistoria('listAdjuntos')
                    break;
                case 'divParaPaginaEventosAnestesia':
                    buscarHistoria('listAnestesia')
                    break;
                case 'divParaPaginaMonitoreoAnestesia':
                    buscarHistoria('listMonitoreo')
                    break;
                case 'divParaArchivosAdjuntos':
                    $("#tabsArchivosAdjuntos").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    buscarHistoria('listArchivosAdjuntos')
                    break;
                case 'divParaPreOperatorio':
                    buscarHistoria('listPreOperatorio')
                    break;
                case 'divParaPosOperatorio':
                    buscarHistoria('listPosOperatorio')
                    break;
                case 'divParaPersonalProc':
                    buscarHistoria('listPersonalProc')
                    break;
            }

        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}
/***************************** fin cargar una pagina ******************************************/


function adicionarSolicitudesADocumentos(id_Paciente, NomCompleto, id_articulo, nombre_articulo, id_orden, Fecha_Programada, Hora, esdatoOrden, Vigencia) {


    asignaAtributo('lblIdPaciente', id_Paciente, 1);
    asignaAtributo('lblNombrePaciente', NomCompleto, 1);

    asignaAtributo('lblIdArticulo', id_articulo, 1);
    asignaAtributo('lblNombreArticulo', nombre_articulo, 1);

    asignaAtributo('lblIdOrden', id_orden, 1);
    asignaAtributo('lblFechaProgramada', Fecha_Programada, 1);
    asignaAtributo('lblHoraProgramada', Hora, 1);
    asignaAtributo('lblEstadoOrden', esdatoOrden, 1);
    asignaAtributo('lblVigencia', Vigencia, 1);



    mostrar('divAdicionOrdenADocumento');

}


function abrirVentanitaAuditoria() {
    mostrar('divNovedadesAuditoria');
    // BORRAR   buscarHC('listDocumentosHistoricosTraerAuditoria' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )           

    modificarCRUD('meterDatosParaListarLaProyeccion');

}

function cerrarVentanitaAuditoria() {
    ocultar('divNovedadesTransaccion')
    limpiaAtributo('lblCodItem', 0)
    limpiaAtributo('lblNombreItem', 0)
    limpiaAtributo('lblIdOrden', 0)
    limpiaAtributo('lblFechaProgramada', 0)
    limpiaAtributo('lblIdHoraMilitar', 0)
    limpiaAtributo('cmbIdConcepto', 0)
    limpiaAtributo('cmbIdCantidadNovedad', 0)
    limpiaAtributo('txtObservacionNovedad', 0)
}

function calendario(idElemento, conFechaDeSesion) {
    if (valorAtributo(idElemento) != 'undefined') {
        $("#drag" + ventanaActual.num).find('#' + idElemento).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-78:+78"
        });

        if (conFechaDeSesion == 1) {
            asignaAtributo(idElemento, document.getElementById('lblFechaDeHoy').lastChild.nodeValue, 0);
        }
    }

    /*if (valorAtributo(idElemento) != 'undefined' && valorAtributo(idElemento) != 'null') {
        var y = document.getElementById(idElemento);
       y.setAttribute("type", "date");
        if (conFechaDeSesion == 1) {
            var today = new Date();
            var fecha_hoy = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);

            y.setAttribute("value", fecha_hoy);
        }
    }*/
}

function chequeadoInmediato() {
    if ($('#drag' + ventanaActual.num).find('#chkInmediato').attr('checked') == true) abrirVentanitaInmediatoMedicamentos()
}

function abrirVentanitaInmediatoMedicamentos() {
    mostrar('divInmediatoMedicamentos')
}

function cerrarVentanitaInmediatoMedicamentos() {
    ocultar('divInmediatoMedicamentos')
    $('#drag' + ventanaActual.num).find('#chkInmediato').attr('checked', '');
    limpiaAtributo('cmb_inmediato_horas', 0)
    limpiaAtributo('cmb_inmediato_dosis', 0)

}


function cerrarVentanita(idVentanita) {
    ocultar(idVentanita)
}

function abrirVentanita(idVentanita) {
    mostrar(idVentanita)
    //	document.getElementById('divInmediatoMedicamentos').style.top = topY;  

}

function cerrarDocumentClinico() {
    if (confirm("Esta seguro de FINALIZAR EL DOCUMENTO No:" + valorAtributo('lblIdDocumento') + "? \n Quedar� de su autor�a ")) {
        modificarCRUD('cerrarDocumento');
    }
}

function cerrarDocumentClinicoSinImp() {
    if (confirm("Esta seguro de FINALIZAR EL DOCUMENTO No:" + valorAtributo('lblIdDocumento') + "? \n Quedar� de su autor�a, \n la fecha de finalizacion se registrar� este momento al igual que el auxiliar que finaliza")) {
        modificarCRUD('cerrarDocumentoSinImp');
    }
}

function cerrarDocumentSinCambioAutor() {
    if (confirm("ESTA SEGURO DE FINALIZAR EL FOLIO? \nLa fecha de finalizacion se registrara este momento al igual que Ud sera el auxiliar que finaliza")) {
        modificarCRUD('cerrarDocumentoSinCambioAutor');
    }
}

function finalizaFolioConFecha() {
    if (confirm("ESTA SEGURO DE FINALIZAR EL FOLIO? \nLa fecha de finalizacion se registrara este momento al igual que el estado de finaliza")) {
        modificarCRUD('finalizaFolioConFecha');
    }
}

function finalizarFolio() {
    if (confirm("Esta seguro de FINALIZAR EL FOLIO No:" + valorAtributo('lblIdFolioJasper') + "? \n Quedara de su autoria, \n la fecha de finalizacion se registrara este momento al igual que el auxiliar que finaliza")) {
        modificarCRUD('cerrarFolioJasper');
    }
}


function cambioEstadoFolio() {
    if (confirm("SEGURO DE CAMBIAR EL ESTADO DEL FOLIO? \n DESEA CAMBIARLO A: " + valorAtributoCombo('cmbIdEstadoFolioEdit'))) {
        //	if(valorAtributo('txtIdProfesionalElaboro')==IdSesion())
        //	else alert('USTED NO ES EL AUTOR DE ESTE FOLIO \n NO PODRA CAMBIAR DE ESTADO')	

        if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 10) {
            alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 8) {
            alert('EL ESTADO FINALIZADO URGENTE DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 7) {
            alert('EL ESTADO CANCELADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 9) {
            alert('EL ESTADO CANCELADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else modificarCRUD('cambioEstadoFolio');

    }
}

function eliminaConsumo() {
    if (valorAtributo('txtUConsumo') != 'S') {
        modificarCRUD('eliminarListHojaGasto');
    } else {
        modificarCRUD('eliminarUltimoConsumo');
    }

}

function eliminarTransaccionDevolucion() {
    comboB = valorAtributo('cmbIdBodega');
    if (comboB == '1') {
        modificarCRUD('eliminaTransaccion');
    } else {
        modificarCRUD('eliminarTransaccionDevHG');


    }
}

function insertarTransaccionDevolucion() {
    comboB = valorAtributo('cmbIdBodega');
    if (comboB == '1') {
        modificarCRUD('crearTransaccion');
    } else {
        modificarCRUD('crearTransaccionDevHG');


    }
}

function cargarHG(valor) {
    if (valor != '1') {
        $('#listaDevolucionesHG').show();
        buscarSuministros('listGrillaDevolucionHG');
    } else {
        $('#listaDevolucionesHG').hide();
    }

    cargarCanasta(valorAtributo('cmbIdBodegaDestino'));

}

function cargarCanasta(valor) {
    if (valorAtributo('cmbIdBodega') == '1' && valor == '3') {
        $('#listaCanastaTraslado').show();
    } else {
        $('#listaCanastaTraslado').hide();
    }
}




function cambiarOpcionIdQuery(id) {
    idQuery = id;

}



function comboCargarBodegaVentas(combo) {
    cargarComboGRALCondicion1('lblId', 'xxx', combo, 57, IdSesion())
    //cargarComboGRALCondicion1('cmbPadre', '1', 'cmbUnidadEditar', 124, valorAtributo('lblIdElementoEditar'))
}

function comboCargarElementos1(combo, idQuery, condicion) {
    cargarComboGRALCondicion1('lblId', 'xxx', combo, idQuery, valorAtributo(condicion))
}

function comboBodegasConsumos(combo) {
    cargarComboGRALCondicion1('lblId', 'xxx', combo, 515, IdSesion())
}

function comboBodegasDespachos(combo, idQuery) {
    cargarComboGRALCondicion1('lblId', 'xxx', combo, idQuery, IdSesion())
}

function comboCreacionDedocumentos() {
    cargarComboGRALCondicion2('cmbPadre', '1', 'cmbTipoDocumento', 80, valorAtributo('lblIdTipoAdmision'), document.getElementById('lblIdProfesion').lastChild.nodeValue)
}

function comboTiposDeFolios() {
    cargarComboGRALCondicion1('lblId', 'xxx', 'cmbTipoFolio', 507, valorAtributo('cmbIdTipoServicio'))
}

function comboFormaFarmaceutica() {
    cargarComboGRALCondicion1('cmbPadre', '1', 'cmbUnidad', 124, valorAtributoIdAutoCompletar('txtIdArticulo'))
}

function comboFormaFarmaceuticaEditar() {
    asignaAtributoCombo2('cmbUnidadEditar', '', '');
    cargarComboGRALCondicion1('cmbPadre', '1', 'cmbUnidadEditar', 124, valorAtributo('lblIdElementoEditar'))
}



function comboFormaFarmaceuticaAdm() {
    cargarComboGRALCondicion1('cmbPadre', '1', 'cmbUnidadAdm', 124, valorAtributoIdAutoCompletar('txtIdMedicamento'))
}

function activarODesactivarElemServicio(idSelec) {
    limpiaAtributo('cmbTipoId', 0)
    limpiaAtributo('txtIdentificacion', 0)
    limpiaAtributo('txtIdBusPaciente', 0)
    limpiaAtributo('cmbIdProfesionales', 0)
    limpiaAtributo('cmbIdArea', 0)
    limpiaAtributo('cmbIdHabitacion', 0)
    limpiaAtributo('cmbIdEstadoCama', 0)

    ocultar('idElemHOSDOM')
    ocultar('idElemHOS')
    ocultar('idElemAYD')

    if (valorAtributo('cmbIdTipoServicio') == 'AYD' || valorAtributo('cmbIdTipoServicio') == 'CPR' || valorAtributo('cmbIdTipoServicio') == 'LAB') {
        mostrar('idElemAYD')
    }

    if (["HDP", "HDC"].indexOf(valorAtributo('cmbIdTipoServicio')) != -1) {
        limpiaAtributo('cmbIdDepartamento', 0)
        limpiaAtributo('cmbIdMunicipio', 0)
        limpiaAtributo('cmbIdLocalidad', 0)
        setTimeout(() => {
            mostrar('idElemHOSDOM')
        }, 100);
    }

    if (["HOS"].indexOf(valorAtributo('cmbIdTipoServicio')) != -1) {
        mostrar('idElemHOS')
    }

    setTimeout(() => {
        contenedorBuscarPaciente()
    }, 100);

}

function cambiarEstadoFolioAyudaDiagnostica() {

    if (valorAtributo('cmbIdProfesionales') != '') {
        limpiarListadosTotales('listGrillaPacientes');

        if (valorAtributo('cmbIdEstadoFolio') != '') {
            asignaAtributo('txtFechaDesdeFolio', '01/01/2016', 0)
            buscarHistoria('ordenesAYD')
        }
    } else alert('DEBE SELECCIONAR EL PROFESIONAL QUE ELABORO.')
}

function contenedorBuscarPaciente() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/hc/contenidoBuscarPacientes.jsp?', true);
    varajaxMenu.onreadystatechange = llenarcontenedorBuscarPaciente;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcontenedorBuscarPaciente() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaContenedorBuscarPaciente").innerHTML = varajaxMenu.responseText;
            BuscarYcambiarIdQueryPorServicio()
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}


function BuscarYcambiarIdQueryPorServicio() { //alert(valorAtributo('cmbIdTipoServicio'))
    $("#divListado table").remove();
    contenedorDiv = document.getElementById('divListado');
    tabla = document.createElement('table');  // se crean los div hijos
    tabla.id = 'listGrillaPacientes'
    tabla.className = 'scroll';
    contenedorDiv.appendChild(tabla);
    //document.getElementById('divListado').innerHTML = '<table id="listGrillaPacientes" class="scroll"></table>';

    habilitar('cmbIdUnidad', 1)
    if (valorAtributo('txtIdBusPaciente') == '') {
        if (valorAtributo('cmbIdTipoServicio') == 'LAP') {
            if (valorAtributo('cmbIdTipoServicio') == 'LAP') {
                buscarHistoria('ordenesLAP')
                habilitar('cmbIdUnidad', 0)
            }
        } else if (valorAtributo('cmbIdTipoServicio') == 'CEX') {
            buscarHistoria('ordenesCEX')
            habilitar('cmbIdUnidad', 0)
        } else if (valorAtributo('cmbIdTipoServicio') == 'HME') {
            buscarHistoria('ordenesHME')
            habilitar('cmbIdUnidad', 0)
        } else if (valorAtributo('cmbIdTipoServicio') == 'CIR') {
            buscarHistoria('ordenesCIR')
            habilitar('cmbIdUnidad', 0)
        } else if (valorAtributo('cmbIdTipoServicio') == 'AYD') {
            if (valorAtributo('cmbIdProfesionales') != '') {
                buscarHistoria('ordenesAYD')
                habilitar('cmbIdUnidad', 0)
            } else alert('DEBE SELECCIONAR EL PROFESIONAL QUE ELABORO')
        } else if (valorAtributo('cmbIdTipoServicio') == 'CPR') {
            if (valorAtributo('cmbIdProfesionales') != '') {
                buscarHistoria('ordenesCPR')
                habilitar('cmbIdUnidad', 0)
            } else alert('DEBE SELECCIONAR EL PROFESIONAL QUE ELABORO')
        } else if (valorAtributo('cmbIdTipoServicio') == 'LAB') {
            if (valorAtributo('cmbIdProfesionales') != '') {
                buscarHistoria('ordenesAYD')
                habilitar('cmbIdUnidad', 0)
            } else alert('DEBE SELECCIONAR EL PROFESIONAL QUE ELABORO')
        } else if (['HOS'].indexOf(valorAtributo("cmbIdTipoServicio")) != -1) {
            buscarHistoria('ordenesHOS')
        } else if (['HDP', 'HDC', 'ALT', 'CPP'].indexOf(valorAtributo("cmbIdTipoServicio")) != -1) {
            buscarHistoria('ordenesHD')
        } else if (['URG', 'ORI', 'TRG', 'TRC'].indexOf(valorAtributo("cmbIdTipoServicio")) != -1) {
            buscarHistoria('ordenesURG')
        } else if (['CPP'].indexOf(valorAtributo("cmbIdTipoServicio")) != -1) {
            buscarHistoria('ordenesCPP')
        } else if (['TMC'].indexOf(valorAtributo("cmbIdTipoServicio")) != -1) {
            buscarHistoria('ordenesTMC')
        }
    } else {
        if (valorAtributo('cmbIdTipoServicio') == 'INT') {
            if (valorAtributo('txtIdBusPaciente') != '') {
                buscarHistoria('ordenesINT')
            } else {
                alert('SELECCIONE UN PACIENTE')
            }
        } else {
            buscarHistoria('ordenesIndividuales', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
        }

    }

}

function BuscarYcambiarIdQueryPorServicioAuditoria() {

    limpiarListadosTotales('listGrillaPacientes');
    habilitar('cmbIdUnidad', 1)

    if (valorAtributo('txtBusIdPaciente') == '') {
        if (valorAtributo('cmbIdTipoServicio') == 'HSP' && valorAtributo('cmbIdUnidad') != '') {
            buscarHC('ordenesAuditoria', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
        } else if (valorAtributo('cmbIdTipoServicio') == 'URG' && valorAtributo('cmbIdUnidad') != '') {
            buscarHC('ordenesAuditoria', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
        } else if (valorAtributo('cmbIdTipoServicio') == 'OTR' && valorAtributo('cmbIdUnidad') != '') {
            buscarHC('ordenes', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
        } else if (valorAtributo('cmbIdTipoServicio') == 'CEX') {
            buscarHC('ordenesCEX', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            habilitar('cmbIdUnidad', 0)
        }
    } else { buscarHC('ordenesIndividualesAuditoria', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp') }

}



function abrirVentanitaArticulosBodega() {

    if (valorAtributo('txtVentanaTrasladoBodegas') != 'undefined') { // solo entra desde la ventana de traslado de bodegas

        cargarComboGRALCondicion1('cmbPadre', 2, 'cmbBodegaDestino', 71, valorAtributo('lblId')); // id bodega origen que se excluira

        mostrar('divArticulosBodega')
        document.getElementById('divArticulosBodega').style.top = topY;


    }
}


function abrirVentanitaOrdenInsumos() {
    mostrar('divNovedadesOrdenInsumos')
    document.getElementById('divNovedadesOrdenInsumos').style.top = topY;

}

function cerrarVentanitaOrdenInsumos() {
    ocultar('divNovedadesOrdenInsumos')

    limpiaAtributo('lblIdOrden', 0)
    limpiaAtributo('idArticulo', 0)
    limpiaAtributo('lblNombreItem', 0)
}

function abrirVentanitaOrdenMedicamentos() {
    mostrar('divNovedadesOrdenMedicamentos')
    document.getElementById('divNovedadesOrdenMedicamentos').style.top = topY;
    buscarHC('listTrazabilidadMedicamento', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
}

function cerrarVentanitaOrdenMedicamentos() {
    ocultar('divNovedadesOrdenMedicamentos')

    //   limpiaAtributo('lblIdOrden', 0)
    limpiaAtributo('idArticulo', 0)
    limpiaAtributo('lblNombreItem', 0)
}

function abrirVentanitaNovedadesAdministracion(codItem, NomItem) {
    mostrar('divNovedadesAdministracion')
    $('#drag' + ventanaActual.num).find('#lblCodItem').html(codItem)
    $('#drag' + ventanaActual.num).find('#lblNombreItem').html(NomItem)

    $('#drag' + ventanaActual.num).find('#txtValorConteo').val('');
    $('#drag' + ventanaActual.num).find('#txtValorConteo').focus();

    document.getElementById('divNovedadesAdministracion').style.top = topY;

}

function cerrarVentanitaNovedadesAdministracion() {
    ocultar('divNovedadesAdministracion')
    limpiaAtributo('lblCodItem', 0)
    limpiaAtributo('lblNombreItem', 0)
    limpiaAtributo('lblIdOrden', 0)
    limpiaAtributo('lblFechaProgramada', 0)
    limpiaAtributo('lblIdHoraMilitar', 0)
    limpiaAtributo('cmbIdConcepto', 0)
    limpiaAtributo('cmbIdCantidadNovedad', 0)
    limpiaAtributo('txtObservacionNovedad', 0)
}

function abrirVentanitaNovedadesTransaccion(codItem, NomItem) {
    mostrar('divNovedadesTransaccion');

    $('#drag' + ventanaActual.num).find('#lblCodItem').html(codItem)
    $('#drag' + ventanaActual.num).find('#lblNombreItem').html(NomItem)

    //   setTimeout("buscarHC('listProyeccionOrden' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )",500);   
}

function cerrarVentanitaNovedadesTransaccion() {
    ocultar('divNovedadesTransaccion')
    limpiaAtributo('lblCodItem', 0)
    limpiaAtributo('lblNombreItem', 0)
    limpiaAtributo('lblIdOrden', 0)
    limpiaAtributo('lblFechaProgramada', 0)
    limpiaAtributo('lblIdHoraMilitar', 0)
    limpiaAtributo('cmbIdConcepto', 0)
    limpiaAtributo('cmbIdCantidadNovedad', 0)
    limpiaAtributo('txtObservacionNovedad', 0)
}



function horasDosis() {
    this.idCombo;
    this.valueCombo;
}
var horasCombo = new Array();



function sumarALaDosisOrdenMedica() { //alert( idElem );
    suma = 0;
    for (i = 0; i <= 23; i++) {
        valorElemento = $('#drag' + ventanaActual.num).find('#cmb_' + i + ' option:selected').text();
        horasCombo[i] = new horasDosis();
        if (valorElemento > 0) {
            horasCombo[i].idCombo = i;
            horasCombo[i].valueCombo = parseFloat(valorElemento);
            suma = suma + horasCombo[i].valueCombo;
        } else {
            horasCombo[i].idCombo = i;
            horasCombo[i].valueCombo = 0;
        }
        //alert( horasCombo[i].idCombo +' -- '+horasCombo[i].valueCombo);				

    }

    $('#drag' + ventanaActual.num).find('#lblDosis').html(suma); // asigna valor a sumatoria de la dosisi
    $('#drag' + ventanaActual.num).find('#lblTotUnidades').html($.trim($('#drag' + ventanaActual.num).find('#cmbIdRepeticionProgramada').val()) * suma); //asigna valor a sumatoria de la dosis por el numero de dias repetidos



}

function limpiarTodosLosCombosDeLaDosisOrdenMedica() { //alert( idElem );

    for (i = 0; i <= 23; i++) {
        $('#drag' + ventanaActual.num).find('#cmb_' + i).val('');
    }
}

function sumarALaDosisOrdenMedicaInsumo() { //alert( idElem );
    suma = 0;
    $('#drag' + ventanaActual.num).find('#lblTotUnidadesInsumo').html($.trim($('#drag' + ventanaActual.num).find('#cmbDosisCantInsumo').val()) * $.trim($('#drag' + ventanaActual.num).find('#cmbIdRepeticionProgramadaInsumo').val())); //asigna valor a sumatoria de la dosis por el numero de dias repetidos

}

function seleccionadoAlgunoTodosLosCombosDeLaDosisOrdenMedica() {
    for (i = 0; i <= 23; i++) {
        if ($('#drag' + ventanaActual.num).find('#cmb_' + i).val() != '') // al menos uno
            return true;
    }
    return false;

}



function limpiaCache() {
    if ('Navigator' == navigator.appName)
        document.forms[0].reset()

}

function abrirTutorial(paginaAbrir) {
    window.open(paginaAbrir, "", "width=1000, height=700, top=80, left=40, scrollbars=yes, Resizable=No, Directories=No, Location=No, Menubar=No, Status=No, Titlebar=No, Toolbar=No, fullscreen=yes");

}



function respuestaeliminarAlListado(opcion) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('raiz') != null) {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) {
                    listadoTablaHC(opcion);
                    limpiarElementosDeSeleccionParaLista(opcion);
                } else {
                    alert("No se pudo ingresar la informaci�n");
                }

            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}


function add_valores_a_mandar(valor) {

    if (tipoElem == 'cmb') {
        switch (valor) {
            case '':
                valores_a_mandar = valores_a_mandar + '_-x.X.x';
                break;
            default:
                valores_a_mandar = valores_a_mandar + '_-' + valor;
        }
    }
    if (tipoElem == 'txt') {
        switch (valor) {
            case '':
                valores_a_mandar = valores_a_mandar + '_-x.X.x';
                break;
            default:
                valores_a_mandar = valores_a_mandar + '_-' + valor;
        }
    }
    if (tipoElem == 'lbl') {
        switch (valor) {
            case '':
                valores_a_mandar = valores_a_mandar + '_-x.X.x';
                break;
            default:
                valores_a_mandar = valores_a_mandar + '_-' + valor;
                
        }
    }
    if (tipoElem == 'chk') { //alert('aqui llega='+tipoElem); alert('aqui valor='+valor)
        switch (valor) {
            case 'true':
                valores_a_mandar = valores_a_mandar + '_-1'; //alert('aqui valores_a_mandar='+valores_a_mandar)
                break;
            case 'false':
                valores_a_mandar = valores_a_mandar + '_-0';
                break;
        }
    }

}


function guardarAlListado(opcion) {
    banderita = 1;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestaguardarAlListado(opcion) };

    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    valores_a_mandar = "accion=" + opcion;


    switch (opcion) {
        /***************************************************************************** INICIO  HISTORIA CLINICA *************************************************/
        case 'listOrdenesMedicamentos':
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&lblIdDocumento=" + valorAtributo('lblIdDocumento');
                valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdArticulo');
                valores_a_mandar = valores_a_mandar + "&cmbIdVia=" + valorAtributo('cmbIdVia');
                valores_a_mandar = valores_a_mandar + "&cmbIdTipoDosis=" + valorAtributo('cmbIdTipoDosis');
                valores_a_mandar = valores_a_mandar + "&cmbIdRepeticionProgramada=" + valorAtributo('cmbIdRepeticionProgramada');
                valores_a_mandar = valores_a_mandar + "&cmbIdIntervalo=" + valorAtributo('cmbIdIntervalo');
                valores_a_mandar = valores_a_mandar + "&txtObservaciones=" + valorAtributo('txtObservaciones');
                valores_a_mandar = valores_a_mandar + "&chkInmediato=" + valorAtributo('chkInmediato');
                valores_a_mandar = valores_a_mandar + "&lblIdPaciente=" + valorAtributo('lblIdPaciente');



                valores_a_mandar = valores_a_mandar + "&HorasCombo="

                if (valorAtributo('chkInmediato') == 'false') {
                    for (i = 0; i <= 23; i++) { // lee todos los valores de los combos de dosis de medicamento
                        if (horasCombo[i].valueCombo > 0) {
                            // alert( horasCombo[i].idCombo +' -- '+horasCombo[i].valueCombo);		
                            valores_a_mandar = valores_a_mandar + horasCombo[i].idCombo + '_-' + horasCombo[i].valueCombo + ',-';
                        }
                    }
                    valores_a_mandar = valores_a_mandar + "&txtFechaOrdenMed=" + valorAtributo('txtFechaOrdenMed');
                    valores_a_mandar = valores_a_mandar + "&cmbHoraOrdenMed=" + valorAtributo('cmbHoraOrdenMed');

                } else {
                    valores_a_mandar = valores_a_mandar + valorAtributo('cmb_inmediato_horas') + '_-' + valorAtributo('cmb_inmediato_dosis') + ',-';
                    valores_a_mandar = valores_a_mandar + "&txtFechaOrdenMed=" + valorAtributo('txtFechaOrdenMed');
                    valores_a_mandar = valores_a_mandar + "&cmbHoraOrdenMed=" + valorAtributoCombo('cmb_inmediato_horas');


                }

            } else banderita = 0;
            break;
        case 'listOrdenesMedicamentosModificar':
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&lblIdDocumento=" + valorAtributo('lblIdDocumento');
                valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdArticulo');
                valores_a_mandar = valores_a_mandar + "&cmbIdVia=" + valorAtributo('cmbIdVia');
                valores_a_mandar = valores_a_mandar + "&cmbIdTipoDosis=" + valorAtributo('cmbIdTipoDosis');
                valores_a_mandar = valores_a_mandar + "&cmbIdRepeticionProgramada=" + valorAtributo('cmbIdRepeticionProgramada');
                valores_a_mandar = valores_a_mandar + "&cmbIdIntervalo=" + valorAtributo('cmbIdIntervalo');
                valores_a_mandar = valores_a_mandar + "&txtObservaciones=" + valorAtributo('txtObservaciones');
                valores_a_mandar = valores_a_mandar + "&chkInmediato=" + valorAtributo('chkInmediato');
                valores_a_mandar = valores_a_mandar + "&txtFechaOrdenMed=" + valorAtributo('txtFechaOrdenMed');
                valores_a_mandar = valores_a_mandar + "&cmbHoraOrdenMed=" + valorAtributo('cmbHoraOrdenMed');
                valores_a_mandar = valores_a_mandar + "&lblIdSolicitud=" + valorAtributo('lblIdOrden');

                valores_a_mandar = valores_a_mandar + "&lblIdPaciente=" + valorAtributo('lblIdPaciente');

                //alert(valores_a_mandar);
                valores_a_mandar = valores_a_mandar + "&HorasCombo="

                if (valorAtributo('chkInmediato') == 'false') {
                    for (i = 0; i <= 23; i++) { // lee todos los valores de los combos de dosis de medicamento
                        if (horasCombo[i].valueCombo > 0) {
                            // alert( horasCombo[i].idCombo +' -- '+horasCombo[i].valueCombo);		
                            valores_a_mandar = valores_a_mandar + horasCombo[i].idCombo + '_-' + horasCombo[i].valueCombo + ',-';
                        }
                    }
                } else {
                    valores_a_mandar = valores_a_mandar + valorAtributo('cmb_inmediato_horas') + '_-' + valorAtributo('cmb_inmediato_dosis') + ',-';
                }
                //	alert('listOrdenesMedicamentos '+valores_a_mandar);
            } else banderita = 0;
            break;

        /***************************************************************************** FIN   HISTORIA CLINICA *************************************************/
        case 'listProfesionalesFicha':
            valores_a_mandar = valores_a_mandar + "&idPlatin=" + valorAtributo('lblIdPlatin');
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + valorAtributo('cmbIdAreaDerecho');
            valores_a_mandar = valores_a_mandar + "&idProfesional=" + valorAtributo('cmbProfesional');
            break;
        case 'listDxIntegralInicial':
            valores_a_mandar = valores_a_mandar + "&idPlatin=" + valorAtributo('lblIdPlatin');
            valores_a_mandar = valores_a_mandar + "&dxIntegralInicial=" + valorAtributo('txtDxIntegralInicial');
            break;
        case 'listSituaActual':
            valores_a_mandar = valores_a_mandar + "&idPlatin=" + valorAtributo('lblIdPlatin');
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
                valores_a_mandar = valores_a_mandar + "&descripcion=" + valorAtributo('txtSituacionActual_' + tabActivo);
            }
            break;
        case 'listObjMeta':
            valores_a_mandar = valores_a_mandar + "&idPlatin=" + valorAtributo('lblIdPlatin');
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&objetivo=" + valorAtributo('txtObjetivo_' + tabActivo);
            valores_a_mandar = valores_a_mandar + "&meta=" + valorAtributo('txtMeta_' + tabActivo);
            break;
        case 'listActividad':
            valores_a_mandar = valores_a_mandar + "&idPlatin=" + valorAtributo('lblIdPlatin');
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&descripcion=" + valorAtributo('txtActividad_' + tabActivo);
            break;


    }
    //	   alert('guardarAlListado...'+valores_a_mandar);  
    if (banderita == '1')
        varajaxInit.send(valores_a_mandar);

}

function respuestaguardarAlListado(opcion) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('raiz') != null) {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) { //  alert('respuesta= '+raiz.getElementsByTagName('respuesta')[0].firstChild.data)
                    listadoTablaHC(opcion);
                    limpiarElementosDeSeleccionParaLista(opcion);
                } else {
                    alert("No se pudo ingresar la informaci�n");
                }

            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}


/*function generarFactura(){

	if(valorAtributo('lblIdCuenta') !=''){

		var ids = jQuery("#listProcedimientosDeFactura").getDataIDs();			  
		
		if(ids.length != 0){

		if(valorAtributo('lblIdFactura') == ''){
			guardarYtraerDatoAlListado('validaNumeracionFactura');
		}else{
			alert('LA FACTURA YA SE ENCUENTRA GENERADA.');
		}


		}else{
			alert('DEBE AGREGAR ALGUN PROCEDIMIENTO.');
		}

		}else{
			alert('SELECCIONE UNA CUENTA PARA GENERAR LA FACTURA.');
	

	}

}*/




function guardarYtraerDatoAlListado(opcion) { //para guardar en la grilla 

    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestaguardarYtraerDatoAlListado(opcion) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=" + opcion;

    switch (opcion) {

        case 'enviarSF':
        valores_a_mandar = valores_a_mandar + "&lblIdEvolucion=" + valorAtributo('lblIdEvolucion');
        break;

        case 'auditarRecibo':
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&lblIdRecibo=" + valorAtributo('lblIdRecibo');
            }
            break;

        case 'validaNumeracionFactura':
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
                valores_a_mandar = valores_a_mandar + "&txtIdPaciente=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
            }
            break;

        case 'auditarFactura':
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
            }
            break;

        case 'anularFacturaWS':
            valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
            break;


        case 'cerrarDocumentoProgramacion':
            if (valorAtributo('lblIdEstado') != '') {
                if (valorAtributo('lblIdEstado') != '1') {
                    valores_a_mandar = valores_a_mandar + "&lblIdDoc=" + valorAtributo('lblIdDoc');
                } else { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            } else { alert('SELECCIONE EL DOCUMENTO'); return false; }
            break;
        case 'cerrarDocumentoSolicitudesInventario':
            if (valorAtributo('lblIdEstado') != '') {
                if (valorAtributo('lblIdEstado') != '1') {
                    valores_a_mandar = valores_a_mandar + "&lblIdDoc=" + valorAtributo('lblIdDoc');
                } else { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            } else { alert('SELECCIONE EL DOCUMENTO'); return false; }
            break;
        case 'modificarTransaccionDocumento':
            if (valorAtributo('lblIdEstado') != '') {
                if (valorAtributo('lblIdEstado') == '1' && valorAtributo('lblNaturaleza') == 'I') {
                    valores_a_mandar = valores_a_mandar + "&lblIdTr=" + valorAtributo('lblIdTr');
                    valores_a_mandar = valores_a_mandar + "&txtTrCantidadAnt=" + valorAtributo('txtTrCantidadAnt');
                    valores_a_mandar = valores_a_mandar + "&txtTrCantidad=" + valorAtributo('txtTrCantidad');
                    valores_a_mandar = valores_a_mandar + "&txtVlrTr=" + valorAtributo('txtVlrTr');
                    valores_a_mandar = valores_a_mandar + "&lblVlrImpuesto=" + valorAtributo('lblVlrImpuesto');
                    valores_a_mandar = valores_a_mandar + "&cmbVlrIva=" + valorAtributo('cmbVlrIva');
                    valores_a_mandar = valores_a_mandar + "&txtLoteTr=" + valorAtributo('txtLoteTr');
                    valores_a_mandar = valores_a_mandar + "&txtFechaTr=" + valorAtributo('txtFechaTr');
                    valores_a_mandar = valores_a_mandar + "&txtLoteAnt=" + valorAtributo('txtLoteAnt');
                } else { alert('EL DOCUMENTO NO ESTA FINALIZADO'); return false; }
            } else { alert('SELECCIONE EL DOCUMENTO'); return false; }
            break;
        case 'eliminarTransaccionDocumento':
            if (valorAtributo('lblIdEstado') != '') {
                if (valorAtributo('lblIdEstado') == '1' && valorAtributo('lblNaturaleza') == 'I') {
                    valores_a_mandar = valores_a_mandar + "&lblIdTr=" + valorAtributo('lblIdTr');
                    valores_a_mandar = valores_a_mandar + "&txtTrCantidadAnt=" + valorAtributo('txtTrCantidadAnt');
                    valores_a_mandar = valores_a_mandar + "&txtTrCantidad=" + valorAtributo('txtTrCantidad');
                    valores_a_mandar = valores_a_mandar + "&txtVlrTr=" + valorAtributo('txtVlrTr');
                    valores_a_mandar = valores_a_mandar + "&lblVlrImpuesto=" + valorAtributo('lblVlrImpuesto');
                    valores_a_mandar = valores_a_mandar + "&cmbVlrIva=" + valorAtributo('cmbVlrIva');
                    valores_a_mandar = valores_a_mandar + "&txtLoteTr=" + valorAtributo('txtLoteTr');
                    valores_a_mandar = valores_a_mandar + "&txtFechaTr=" + valorAtributo('txtFechaTr');
                    valores_a_mandar = valores_a_mandar + "&txtLoteAnt=" + valorAtributo('txtLoteAnt');
                } else { alert('EL DOCUMENTO NO ESTA FINALIZADO'); return false; }
            } else { alert('SELECCIONE EL DOCUMENTO'); return false; }
            break;
        case 'verificaTransaccionesInventario':
            if (valorAtributo('lblIdEstado') != '') {
                if (valorAtributo('lblIdEstado') != '1') {
                    valores_a_mandar = valores_a_mandar + "&lblIdDoc=" + valorAtributo('lblIdDoc');
                } else { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            } else { alert('SELECCIONE EL DOCUMENTO'); return false; }
            break;
        case 'verificaTransaccionesInventarioTrasladoBodega':
            if (valorAtributo('lblIdDoc') != '') {
                if (valorAtributo('cmbIdBodegaDestino') != '') {
                    if (valorAtributo('cmbIdBodega') != valorAtributo('cmbIdBodegaDestino')) {

                        if (valorAtributo('lblIdEstado') != '1') {

                            if (confirm('SE REALIZARA EL TRASLADO A : ' + valorAtributoCombo('cmbIdBodegaDestino') +
                                "\nDESEA CONTINUAR?")) {
                                valores_a_mandar = valores_a_mandar + "&lblIdDoc=" + valorAtributo('lblIdDoc');
                                valores_a_mandar = valores_a_mandar + "&cmbIdBodegaDestino=" + valorAtributo('cmbIdBodegaDestino');
                            } else {
                                return false;
                            }


                        } else { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }

                    } else { alert('NO PUEDEN SER ORIGEN Y DESTINO LA MISMA BODEGA'); return false; }
                } else { alert('SELECCIONE BODEGA DESTINO'); return false; }
            } else { alert('SELECCIONE UN DOCUMENTO'); return false; }
            break;



        case 'consultaExamenFisico':
            if (valorAtributo('lblIdDocumento') != '') {
                valores_a_mandar = valores_a_mandar + "&lblIdDocumento=" + valorAtributo('lblIdDocumento');
            } else {
                return;
            }
            break;


        case 'elementosDelFolio':
            if (valorAtributo('lblIdDocumento') != '') {
                if (valorAtributo('txtIdEsdadoDocumento') != '1') {
                    valores_a_mandar = valores_a_mandar + "&lblTipoDocumento=" + valorAtributo('lblTipoDocumento');
                    valores_a_mandar = valores_a_mandar + "&lblIdDocumento=" + valorAtributo('lblIdDocumento');
                } else { alert('EL FOLIO YA SE ENCUENTRA FINALIZADO'); return false; }
            } else { alert('SELECCIONE EL FOLIO'); return false; }
            break;

        case 'buscarIdentificacionPaciente':
            if (verificarCamposGuardar('buscarIdentificacionPaciente')) {
                valores_a_mandar = valores_a_mandar + "&cmbTipoId=" + valorAtributo('cmbTipoId');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');
            }
            break;

        case 'buscarIdPacienteEncuesta':
            valores_a_mandar = valores_a_mandar + "&cmbTipoId=" + valorAtributo('cmbTipoIdUrgencias');
            valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacionUrgencias');
            valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacionUrgencias');
            break;

        case 'buscarIdentificacionPacienteUrgencias':
            valores_a_mandar = valores_a_mandar + "&cmbTipoId=" + valorAtributo('cmbTipoIdUrgencias');
            valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacionUrgencias');
            valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacionUrgencias');
            break;

        case 'creaValorCuentaFacturaCirugia':
            if (verificarCamposGuardar('buscarIdentificacionPaciente')) {
                valores_a_mandar = valores_a_mandar + "&cmbTipoId=" + valorAtributo('cmbTipoId');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');
            }
            break;

        case 'buscarSiEsNoPos':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdArticulo');
            break;

        case 'buscarConciliacion':
            valores_a_mandar = valores_a_mandar + "&lblIdDocumento=" + valorAtributo('lblIdDocumento');
            break;

        case 'buscarSiEsNoPosProcedim':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdProcedimiento');
            break;

        case 'buscarSiEsNoPosProcedimientoNotificacion':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdProcedimiento');
            break;

        case 'buscarSiEsNoPosAyudaDiagnostica':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica');
            break;
        case 'buscarSiEsNoPosAyudaDiagnosticaNotificacion':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica');
            break;
        case 'buscarSiEsNoPosLaboratorio':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdLaboratorio');
            break;
        case 'buscarSiEsNoPosLaboratorioNotificacion':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdLaboratorio');
            break;
        case 'buscarSiEsNoPosTerapia':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdTerapia');
            break;
        case 'buscarSiEsNoPosTerapiaNotificacion':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdTerapia');
            break;
        case 'consultarDisponibleAgendaSinLE':
            valores_a_mandar = valores_a_mandar + "&lblIdAgendaDetalle=" + valorAtributo('lblIdAgendaDetalle');
            break;

        case 'consultarDisponibleDesdeAgenda':
            //				  valores_a_mandar=valores_a_mandar+"&txtEstadoCita="+valorAtributo('txtEstadoCita');		  		  
            valores_a_mandar = valores_a_mandar + "&lblIdAgendaDetalle=" + valorAtributo('lblIdAgendaDetalle');
            valores_a_mandar = valores_a_mandar + "&txtIdBusPaciente=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
            valores_a_mandar = valores_a_mandar + "&cmbTipoCita=" + valorAtributo('cmbTipoCita');
            valores_a_mandar = valores_a_mandar + "&lblIdListaEspera=" + valorAtributo('lblIdListaEspera');
            break;

        case 'buscarPendienteLE_LE':
            valores_a_mandar = "accion=consultarDisponibleDesdeListaEspera";
            valores_a_mandar = valores_a_mandar + "&txtIdBusPaciente=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
            valores_a_mandar = valores_a_mandar + "&cmbTipoCita=" + valorAtributo('cmbTipoCita');
            break;

        case 'nuevoDocumentoHC':
            if (valorAtributo('lblIdTipoAdmision') != '') {
                if (valorAtributo('cmbIdTipoServicio') != 'INT') {
                    valores_a_mandar = valores_a_mandar + '&opcionTipo=MI_AGENDA'
                    valores_a_mandar = valores_a_mandar + "&lblIdAdmision=" + valorAtributo('lblIdAdmisionAgen');
                    valores_a_mandar = valores_a_mandar + "&lblIdTipoAdmision=" + valorAtributo('lblIdTipoAdmision');
                    valores_a_mandar = valores_a_mandar + "&lblIdPaciente=" + valorAtributo('lblIdPaciente');
                    valores_a_mandar = valores_a_mandar + "&TipoFolio=" + valorAtributo('cmbTipoDocumento');
                    valores_a_mandar = valores_a_mandar + "&lblIdProfesion=" + document.getElementById('lblIdProfesion').lastChild.nodeValue;
                    valores_a_mandar = valores_a_mandar + "&lblIdAuxiliar=" + valorAtributo('lblIdAuxiliar');
                } else guardarYtraerDatoAlListado('nuevoDocumentoInterconsulta')
            } else {
                alert('Debe seleccionar un paciente con Admision')
                return;
            }
            break;

        case 'nuevoDocumentoInterconsulta':
            if (valorAtributo('lblIdTipoAdmision') != '') {
                valores_a_mandar = 'accion=nuevoDocumentoHC'
                valores_a_mandar = valores_a_mandar + '&opcionTipo=INTERCONSULTA'
                valores_a_mandar = valores_a_mandar + "&lblIdPaciente=" + valorAtributo('lblIdPaciente');
                valores_a_mandar = valores_a_mandar + "&TipoFolio=" + valorAtributo('cmbTipoDocumento');
                valores_a_mandar = valores_a_mandar + "&lblIdProfesion=" + document.getElementById('lblIdProfesion').lastChild.nodeValue;
                valores_a_mandar = valores_a_mandar + "&lblIdEspecialidad=" + valorAtributo('lblIdEspecialidad');
                valores_a_mandar = valores_a_mandar + "&lblIdAuxiliar=" + valorAtributo('lblIdAuxiliar');
                opcion = 'nuevoDocumentoHC'
            } else {
                alert('Debe seleccionar un paciente con Admision')
                return;
            }
            break;


        case 'nuevaAdmisionCuenta':
            valores_a_mandar = valores_a_mandar + "&txtNoAutorizacion=" + valorAtributo('txtNoAutorizacion');
            break;


        case 'consultarValoresCuenta':
            if (valorAtributo('lblIdFactura') != '') {
                valores_a_mandar = valores_a_mandar + "&idQuery=682";
                valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
            } else {
                limpiarListadosTotales('listProcedimientosDeFactura');
                limpiaAtributo('txtIdProcedimientoCex', 0);
                limpiaAtributo('lblValorUnitarioProc', 0);
                limpiaAtributo('cmbCantidad', 0);
                limpiaAtributo('lblIdFactura', 0);
                limpiaAtributo('lblIdEstadoFactura', 0);
                limpiaAtributo('lblEstadoFactura', 0);
                limpiaAtributo('lblNumeroFactura', 0);
                limpiaAtributo('lblIdRecibo', 0);
                limpiaAtributo('lblTotalFactura', 0);
                limpiaAtributo('lblValorNoCubierto', 0);
                limpiaAtributo('lblValorCubierto', 0);
                limpiaAtributo('lblDescuento', 0);
                return;
            }

            break;


        case 'verificaCantidadInventario':
            if (valorAtributo('txtCantidad') != '') {
                valores_a_mandar = valores_a_mandar + "&txtCantidad=" + valorAtributoIdAutoCompletar('txtIdArticulo');
            } else {
                return;
            }
            break;
        case 'verificaCantidadInventarioMinimo':
            if (valorAtributo('txtCantidad') != '') {
                valores_a_mandar = valores_a_mandar + "&txtCantidad=" + valorAtributoIdAutoCompletar('txtIdArticulo');
            } else {
                return;
            }
            break;
        case 'verificaValorInventario':
            if (valorAtributo('txtValorU') != '') {
                valores_a_mandar = valores_a_mandar + "&txtCantidad=" + valorAtributoIdAutoCompletar('txtIdArticulo');
            } else {
                return;
            }
            break;
        case 'verificaValorInventarioMinimo':
            if (valorAtributo('txtValorU') != '') {
                valores_a_mandar = valores_a_mandar + "&txtCantidad=" + valorAtributoIdAutoCompletar('txtIdArticulo');
            } else {
                return;
            }
            break;

        case 'nuevoDocumentoInventarioBodegaConsumo':
            if (verificarCamposGuardar('nuevoDocumentoInventarioBodegaConsumo')) {
                valores_a_mandar = valores_a_mandar + '&accion=nuevoDocumentoInventarioProgramacion';
                valores_a_mandar = valores_a_mandar + "&cmbIdTipoDocumento=" + valorAtributo('cmbIdTipoDocumento');
                valores_a_mandar = valores_a_mandar + "&idBodega=" + valorAtributo('cmbIdBodega');
                valores_a_mandar = valores_a_mandar + "&idAdmision=" + valorAtributo('lblIdAdmisionAgen');
                valores_a_mandar = valores_a_mandar + "&sw_origen=" + valorAtributo('cmbIdTipoDevolucion');

            } else { alert('DEBE SELECCIONAR BODEGA Y ADMISION'); return false; }
            break;
        case 'nuevoDocumentoInventarioProgramacion':
            if (valorAtributo('cmbIdBodega') != '') {
                valores_a_mandar = valores_a_mandar + '&accion=nuevoDocumentoInventarioProgramacion';
                valores_a_mandar = valores_a_mandar + "&cmbIdTipoDocumento=" + valorAtributo('cmbIdTipoDocumento');
                valores_a_mandar = valores_a_mandar + "&idBodega=" + valorAtributo('cmbIdBodega');
                valores_a_mandar = valores_a_mandar + "&idCita=" + valorAtributo('lblIdAgendaDetalle'); //cita
            } else { alert('SELECCIONE UNA BODEGA '); return false; }
            break;
        case 'crearDocInvBodega':
            if (valorAtributo('cmbIdBodegaDestino') != '') {
                if (valorAtributo('cmbIdBodega') != valorAtributo('cmbIdBodegaDestino')) {
                    valores_a_mandar = ""
                    valores_a_mandar = valores_a_mandar + '&accion=crearDocInvBodega';
                    valores_a_mandar = valores_a_mandar + "&cmbIdTipoDocumento=" + valorAtributo('cmbIdTipoDocumento');
                    valores_a_mandar = valores_a_mandar + "&idBodega=" + valorAtributo('cmbIdBodega');
                } else { alert('NO PUEDEN SER ORIGEN Y DESTINO LA MISMA BODEGA'); return false; }
            } else { alert('SELECCIONE BODEGA DESTINO'); return false; }
            break;
        case 'existeDocumentoInventarioBodega':
            if (valorAtributo('cmbIdBodega') != '') {
                if (valorAtributo('cmbIdTipoDocumento') != '') {
                    if (verificarCamposGuardar('nuevoDocumentoInventarioBodega')) {
                        valores_a_mandar = valores_a_mandar + '&accion=existeDocumentoInventarioBodega';

                        valores_a_mandar = valores_a_mandar + "&txtNumero=" + valorAtributo('txtNumero');
                        valores_a_mandar = valores_a_mandar + "&txtIdTercero=" + valorAtributoIdAutoCompletar('txtIdTercero');

                    } else { return false; }
                } else { alert('SELECCIONE EL TIPO DE DOCUMENTO'); return false; }
            } else { alert('SELECCIONE UNA BODEGA'); return false; }
            break;
        case 'nuevoDocumentoInventarioBodega':
            if (valorAtributo('cmbIdBodega') != '') {
                if (valorAtributo('cmbIdTipoDocumento') != '') {
                    if (verificarCamposGuardar('nuevoDocumentoInventarioBodega')) {
                        valores_a_mandar = valores_a_mandar + '&accion=nuevoDocumentoInventarioBodega';
                        valores_a_mandar = valores_a_mandar + "&cmbIdTipoDocumento=" + valorAtributo('cmbIdTipoDocumento');
                        valores_a_mandar = valores_a_mandar + "&idBodega=" + valorAtributo('cmbIdBodega');

                        valores_a_mandar = valores_a_mandar + "&txtObservacion=" + valorAtributo('txtObservacion');
                        valores_a_mandar = valores_a_mandar + "&txtNumero=" + valorAtributo('txtNumero');
                        valores_a_mandar = valores_a_mandar + "&txtIdTercero=" + valorAtributoIdAutoCompletar('txtIdTercero');
                        valores_a_mandar = valores_a_mandar + "&txtFechaDocumento=" + valorAtributo('txtFechaDocumento');
                        valores_a_mandar = valores_a_mandar + "&txtValorFlete=" + valorAtributo('txtValorFlete');

                    } else { return false; }
                } else { alert('SELECCIONE EL TIPO DE DOCUMENTO'); return false; }
            } else { alert('SELECCIONE UNA BODEGA'); return false; }
            break;

        case 'ocuparCandadoReporte':
            alert('ocuparCandadoReporte')
            break;
        case 'liberarCandadoReporte':
            alert('liberarCandadoReporte')
            break;

        case 'agregarTurno':
            if (verificarCamposGuardar('agregarTurno')) {

                valores_a_mandar = valores_a_mandar + "&txtIdTurno=" + valorAtributo('lblIdPacienteParlante') + '_' + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblPacienteParlante=" + valorAtributo('lblPacienteParlante');
                valores_a_mandar = valores_a_mandar + "&lblConsultorioParlante=" + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblEstadoParlante=" + valorAtributo('lblEstadoParlante');
                valores_a_mandar = valores_a_mandar + "&cmbTurno=" + valorAtributo('cmbTurno');
            } else { return false; }
            break;

        case 'eliminarTurno':
            if (verificarCamposGuardar('eliminarTurno')) {
                valores_a_mandar = valores_a_mandar + "&txtIdTurno=" + valorAtributo('lblIdPacienteParlante') + '_' + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblPacienteParlante=" + valorAtributo('lblPacienteParlante');
                valores_a_mandar = valores_a_mandar + "&lblConsultorioParlante=" + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblEstadoParlante=" + valorAtributo('lblEstadoParlante');
                valores_a_mandar = valores_a_mandar + "&cmbTurno=" + "0";
            } else { return false; }
            break;

        case 'llamarTurno':
            if (verificarCamposGuardar('llamarTurno')) {
                valores_a_mandar = valores_a_mandar + "&txtIdTurno=" + valorAtributo('lblIdPacienteParlante') + '_' + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblPacienteParlante=" + valorAtributo('lblPacienteParlante');
                valores_a_mandar = valores_a_mandar + "&lblConsultorioParlante=" + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblEstadoParlante=" + valorAtributo('lblEstadoParlante');
                valores_a_mandar = valores_a_mandar + "&cmbTurno=" + "0";
            } else { return false; }
            break;

        case 'modificarTurno':
            if (verificarCamposGuardar('modificarTurno')) {
                valores_a_mandar = valores_a_mandar + "&txtIdTurno=" + valorAtributo('lblIdPacienteParlante') + '_' + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblPacienteParlante=" + valorAtributo('lblPacienteParlante');
                valores_a_mandar = valores_a_mandar + "&lblConsultorioParlante=" + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblEstadoParlante=" + valorAtributo('lblEstadoParlante');
                valores_a_mandar = valores_a_mandar + "&cmbTurno=" + "0";

            } else { return false; }
            break;


        /*  case 'nuevoDocumentoInventario':  
       if( valorAtributo('cmbUbicacion')!='' && valorAtributo('cmbConcepto')!='' &&  valorAtributo('txtFechaConsumo')!=''  ){
          if(valorAtributo('cmbConcepto')=='UD'){ 				   
               limpiaAtributo('lblIdDocumento');
               valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo')+' '+valorAtributo('cmbHoraConsumoDesde')+':00';
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo2="+valorAtributo('txtFechaConsumo2')+' '+valorAtributo('cmbHoraConsumoHasta')+':00'; /* no cambiar el 00 en punto
               valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('cmbUbicacion');	
          }
          else{alert('El concepto debe ser UNIDOSIS'); return false;}
       }
       else{ alert('Debe seleccionar Unidad, Concepto y Fecha de Consumo')
           return;
       }			   
  break;	

  case 'nuevoDocumentoInventarioIndividual':  
  
       if( valorAtributo('cmbUbicacion')!=''  &&  valorAtributo('txtFechaConsumo')!='' &&  valorAtributo('txtFechaConsumo2')!='' &&  valorAtributoIdAutoCompletar('txtBusIdPaciente')!=''  ){
          if(valorAtributo('cmbConcepto')=='SP'){ 
               limpiaAtributo('lblIdDocumento');
               valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo')+' '+valorAtributo('cmbHoraConsumoDesde')+':00';
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo2="+valorAtributo('txtFechaConsumo2')+' '+valorAtributo('cmbHoraConsumoHasta')+':59';
               valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('cmbUbicacion');	
               valores_a_mandar=valores_a_mandar+"&txtBusIdPaciente="+valorAtributoIdAutoCompletar('txtBusIdPaciente');	
          }
        else  alert('El concepto debe ser SP-Despacho Individual')

       }
       else{ alert('Debe seleccionar PACIENTE Unidad, Concepto y Fecha de Consumo')
           return;
       }			   
  break;	
  case 'nuevoDocumentoInventarioVacio':  
  
       if( valorAtributo('cmbUbicacion')!=''  &&  valorAtributo('txtFechaConsumo')!='' &&  valorAtributo('txtFechaConsumo2')!='' &&  valorAtributoIdAutoCompletar('txtBusIdPaciente')!=''  ){
          if(valorAtributo('cmbConcepto')=='SP'){ 
               limpiaAtributo('lblIdDocumento');
               valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo')+' '+valorAtributo('cmbHoraConsumoDesde')+':00';
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo2="+valorAtributo('txtFechaConsumo2')+' '+valorAtributo('cmbHoraConsumoHasta')+':59';
               valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('cmbUbicacion');	
               valores_a_mandar=valores_a_mandar+"&txtBusIdPaciente="+valorAtributoIdAutoCompletar('txtBusIdPaciente');	
          }
        else  alert('El concepto debe ser SP-Despacho Individual')

       }
       else{ alert('Debe seleccionar PACIENTE Unidad, Concepto y Fecha de Consumo')
           return;
       }			   
  break;		  
  case 'nuevoDocumentoInventarioIndividualStock':  
  
       if( valorAtributo('cmbUbicacion')!=''  &&  valorAtributo('txtFechaConsumo')!='' &&  valorAtributo('txtFechaConsumo2')!='' &&  valorAtributoIdAutoCompletar('txtBusIdPaciente')!=''  ){
          if(valorAtributo('cmbConcepto')=='CS'){ 
               limpiaAtributo('lblIdDocumento');
               valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
               valores_a_mandar=valores_a_mandar+"&cmbIdBodega="+valorAtributo('cmbIdBodegaStock');					   
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo')+' '+valorAtributo('cmbHoraConsumoDesde')+':00';
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo2="+valorAtributo('txtFechaConsumo2')+' '+valorAtributo('cmbHoraConsumoHasta')+':59';
               valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('cmbUbicacion');	
               valores_a_mandar=valores_a_mandar+"&txtBusIdPaciente="+valorAtributoIdAutoCompletar('txtBusIdPaciente');	
               valores_a_mandar=valores_a_mandar+"&cmbIdInmediato="+valorAtributo('cmbIdInmediato');						   
          }
        else  alert('El concepto debe ser CS-Despacho Stock')

       }
       else{ alert('Debe seleccionar PACIENTE Unidad, Concepto y Fecha de Consumo')
           return;
       }			   
  break;	
                  
          
  case 'dePendienteAAdministrado':  
                 valores_a_mandar=valores_a_mandar+"&lblIdTnsFarmacia="+valorAtributo('lblIdTnsFarmacia');		  
                 valores_a_mandar=valores_a_mandar+"&lblFechaProgramada="+valorAtributo('lblFechaProgramada');		
           valores_a_mandar=valores_a_mandar+"&txtIdHora="+valorAtributo('txtIdHora');	
           valores_a_mandar=valores_a_mandar+"&txtProyectable="+valorAtributo('txtProyectable');					   
  break; 			
  case 'deAdministradoAPendiente':  
                 valores_a_mandar=valores_a_mandar+"&lblIdTnsFarmacia="+valorAtributo('lblIdTnsFarmacia');		  		  
                 valores_a_mandar=valores_a_mandar+"&lblFechaProgramada="+valorAtributo('lblFechaProgramada');		
           valores_a_mandar=valores_a_mandar+"&txtIdHora="+valorAtributo('txtIdHora');	
           valores_a_mandar=valores_a_mandar+"&txtProyectable="+valorAtributo('txtProyectable');					   				   
  break;		  
  case 'dePendienteAAdministradoNovedad':  
           if(verificarCamposGuardar(opcion)){
                   valores_a_mandar=valores_a_mandar+"&lblIdTnsFarmacia="+valorAtributo('lblIdTnsFarmacia');		  					   
             valores_a_mandar=valores_a_mandar+"&lblFechaProgramada="+valorAtributo('lblFechaProgramada');		
             valores_a_mandar=valores_a_mandar+"&txtIdHora="+valorAtributo('txtIdHora');
             valores_a_mandar=valores_a_mandar+"&cmbIdConcepto="+valorAtributo('cmbIdConcepto');					   				   				   
             valores_a_mandar=valores_a_mandar+"&cmbIdCantidadNovedad="+valorAtributo('cmbIdCantidadNovedad');		
             valores_a_mandar=valores_a_mandar+"&txtObservacionNovedad="+valorAtributo('txtObservacionNovedad');
             valores_a_mandar=valores_a_mandar+"&txtProyectable="+valorAtributo('txtProyectable');					   				   					 					 						   
           }else  return false;
  break;	
  case 'deDevolucion':  
           if(verificarCamposGuardar(opcion)){
             valores_a_mandar=valores_a_mandar+"&lblIdOrden="+valorAtributo('lblIdOrden');
             valores_a_mandar=valores_a_mandar+"&lblFechaProgramada="+valorAtributo('lblFechaProgramada');		
             valores_a_mandar=valores_a_mandar+"&txtIdHora="+valorAtributo('txtIdHora');
             valores_a_mandar=valores_a_mandar+"&cmbIdConcepto="+valorAtributo('cmbIdConcepto');					   				   				   
             valores_a_mandar=valores_a_mandar+"&cmbIdCantidadNovedad="+valorAtributo('cmbIdCantidadNovedad');		
             valores_a_mandar=valores_a_mandar+"&txtObservacionNovedad="+valorAtributo('txtObservacionNovedad');						   
           }else  return false;
  break;	
  case 'cambioDeBodega':  
           if(verificarCamposGuardar(opcion)){
             valores_a_mandar=valores_a_mandar+"&lblIdTransaccion="+valorAtributo('lblIdTransaccion');
             valores_a_mandar=valores_a_mandar+"&cmbIdBodega="+valorAtributo('cmbIdBodega');					 
          alert('cambioDeBodega= '+valores_a_mandar);
           }else  return false;
  break;		  
  case 'dejarEnPendiente':  
           if(verificarCamposGuardar(opcion)){
             valores_a_mandar=valores_a_mandar+"&lblIdTransaccion="+valorAtributo('lblIdTransaccion');
//				  alert('dejarEnPendiente= '+valores_a_mandar);
           }else  return false;
  break;
  */

        /***************************************************************************** FIN   HISTORIA CLINICA *************************************************/
        /*	  case 'listTransaccionBodega':  alert('cmbIdPresentacion= '+valorAtributo('cmbIdPresentacion'))
       if( valorAtributo('txtIdArticulo')!='' && valorAtributo('cmbIdConcepto')!='' &&  valorAtributo('txtCantidad')!='' &&  valorAtributo('txtValor')!='' &&  valorAtributo('lblIdDocumento')!='' &&  valorAtributo('cmbIdPresentacion')!=''  ){
//				   limpiaAtributo('lblIdDocumento');
                 valores_a_mandar=valores_a_mandar+"&cmbIdConcepto="+valorAtributo('cmbIdConcepto');		
                 valores_a_mandar=valores_a_mandar+"&lblIdDocumento="+valorAtributo('lblIdDocumento');
                 valores_a_mandar=valores_a_mandar+"&IdBodega="+valorAtributo('lblId');						   
                 valores_a_mandar=valores_a_mandar+"&txtIdArticulo="+valorAtributoIdAutoCompletar('txtIdArticulo');
           valores_a_mandar=valores_a_mandar+"&txtCantidad="+valorAtributo('txtCantidad');	
           valores_a_mandar=valores_a_mandar+"&cmbIdPresentacion="+valorAtributo('cmbIdPresentacion');					   
           valores_a_mandar=valores_a_mandar+"&txtValor="+valorAtributo('txtValor');					   
       }
       else{ alert('Debe seleccionar Documento Art�culo, Concepto, Cantidad, Presentaci�n y Valor')
           return;
       }			   
  break;
/* case 'nuevoDocumentoInventarioDevoluciones':  

               if( valorAtributo('cmbConcepto')!='' && valorAtributo('cmbCentroCostoSolicita')!='' && valorAtributo('txtBusFechaConsumo')!='' ){
                   limpiaAtributo('lblIdDocumento');
                   valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
                   valores_a_mandar=valores_a_mandar+"&txtIdCentroCosto="+valorAtributo('cmbCentroCostoSolicita');	
                   valores_a_mandar=valores_a_mandar+"&txtBusFechaConsumo="+valorAtributo('txtBusFechaConsumo');							   
               }
               else{ alert('Debe seleccionar  Concepto y Centro de Costos')
                   return;
               }					 
       
  break;
  case 'nuevoDocumentoInventarioEntrada':  

               if( valorAtributo('txtIdCentroCosto')!='' && valorAtributo('cmbConcepto')!='' &&  valorAtributo('txtFechaConsumo')!='' &&  valorAtributo('txtIdProveedor')!='' &&  valorAtributo('cmbIdTipoReferencia')!='' &&  valorAtributo('txtNoReferencia')!=''){
                   limpiaAtributo('lblIdDocumento');
                   valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
                   valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo');		
                   valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('txtIdCentroCosto');	

                   valores_a_mandar=valores_a_mandar+"&txtIdProveedor="+valorAtributoIdAutoCompletar('txtIdProveedor');	
                   valores_a_mandar=valores_a_mandar+"&txtFechaReferencia="+valorAtributo('txtFechaReferencia');	
                   valores_a_mandar=valores_a_mandar+"&cmbIdTipoReferencia="+valorAtributo('cmbIdTipoReferencia');							   
                   valores_a_mandar=valores_a_mandar+"&txtNoReferencia="+valorAtributo('txtNoReferencia');	
                   valores_a_mandar=valores_a_mandar+"&txtObservacionReferencia="+valorAtributo('txtObservacionReferencia');							   
               
               }
               else{ alert('Debe seleccionar  Concepto y Datos completos de Referencia')
                   return;
               }	
  break;	
  
  case 'nuevoDocumentoInventarioTraslado':  

               if( valorAtributo('txtIdCentroCosto')!='' &&   valorAtributo('txtCantidad')!='' ){
                   limpiaAtributo('lblIdDocumento');
                   valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
                   valores_a_mandar=valores_a_mandar+"&idBodegaOrigen="+valorAtributo('lblId');						   						   						   					   
                   valores_a_mandar=valores_a_mandar+"&lblIdArticulo="+valorAtributo('lblIdArticulo');
                   valores_a_mandar=valores_a_mandar+"&txtCantidad="+valorAtributo('txtCantidad');	
                   valores_a_mandar=valores_a_mandar+"&cmbBodegaDestino="+valorAtributo('cmbBodegaDestino');							   					   
                   

                   
                   valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo');		
                   valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('txtIdCentroCosto');	

               }
               else{ alert('Debe seleccionar Bodega Origen y Cantidad')
                   return;
               }	
  break;	*/

    }
    //	   alert('guardarAlListado...'+valores_a_mandar);  
    varajaxInit.send(valores_a_mandar);

}

function checkKey2(key) {

    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }
    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar

    if (unicode == 13) {
        guardarYtraerDatoAlListado('buscarIdentificacionPaciente')
    }

}

function checkKeyVentanita(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }
    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar

    if (unicode == 13) {
        buscarHistoria('listGrillaVentana')
    }

}

function checkKeyVentanitaCondicion1(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }
    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar

    if (unicode == 13) {
        buscarHistoria('listGrillaVentanaCondicion1')
    }

}

/*NO POS*/
function buscarInformacionNoPos(tipoFormato) {
    // borrarRegistrosTabla('tablaCitas');	  

    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestabuscarInformacionNoPos(tipoFormato) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    if (tipoFormato == 'noPos') {
        valores_a_mandar = "accion=noPos";
        valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdArticulo');
    } else {
        valores_a_mandar = "accion=noPosProcedimientos";

        if (tabActivo == 'divProcedimiento')
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdProcedimiento');
        else if (tabActivo == 'divAyudasDiagnosticas')
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica');
        else if (tabActivo == 'divTerapias')
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdTerapia');
        else if (tabActivo == 'divLaboratorioClinico')
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdLaboratorio');



    }

    varajaxInit.send(valores_a_mandar);
}


function respuestabuscarInformacionNoPos(tipoFormato) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('infoVAR') != null) {
                totalRegistros = raiz.getElementsByTagName('VAR1').length;

                if (totalRegistros > 0) {
                    if (tipoFormato == 'noPos') {
                        asignaAtributo('txt_ResumenHC', raiz.getElementsByTagName('VAR1')[0].firstChild.data, 0)
                        asignaAtributo('txt_MedicamentoDelPOS1', raiz.getElementsByTagName('VAR2')[0].firstChild.data, 0)
                        asignaAtributo('txt_PresentacionConcentracion1', raiz.getElementsByTagName('VAR3')[0].firstChild.data, 0)
                        asignaAtributo('txt_DosisFrecuencia1', raiz.getElementsByTagName('VAR4')[0].firstChild.data, 0)
                        asignaAtributo('txt_IndicacionTerapeutica', raiz.getElementsByTagName('VAR5')[0].firstChild.data, 0)
                        asignaAtributo('txt_ExpliquePos', raiz.getElementsByTagName('VAR6')[0].firstChild.data, 0)
                    }
                    if (tipoFormato == 'noPosProcedimientos') {
                        asignaAtributo('txt_ResumenHC', raiz.getElementsByTagName('VAR1')[0].firstChild.data, 0)
                        asignaAtributo('txt_AlternativaPos', raiz.getElementsByTagName('VAR2')[0].firstChild.data, 0)
                        asignaAtributo('txt_PosibilidadTerapeutica', raiz.getElementsByTagName('VAR3')[0].firstChild.data, 0)
                        asignaAtributo('txt_MotivoUso', raiz.getElementsByTagName('VAR4')[0].firstChild.data, 0)
                        asignaAtributo('txt_DiagnostProcPos', raiz.getElementsByTagName('VAR5')[0].firstChild.data, 0)
                        asignaAtributo('txt_JustificacionPos', raiz.getElementsByTagName('VAR6')[0].firstChild.data, 0)
                    }

                } else {
                    alert('ELEMENTO NO TIENE PARAMETROS ANTERIORES POR DEFECTO')
                }
            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}
/*FIN NO POS*/

function respuestaguardarYtraerDatoAlListado(opcion) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('raiz') != null) {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) {

                    switch (opcion) {
                        case 'validaNumeracionFactura':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {

                                var noticia = raiz.getElementsByTagName('NOTICIA')[0].firstChild.data;

                                if (noticia != 'PERMITE_NUMERAR') {
                                    alert('1. ATENCION FACTURADOR:\n\n' + noticia);
                                }


                                console.log('Ajuste: ' + raiz.getElementsByTagName('estadoAjuste')[0].firstChild.data)
                                switch (raiz.getElementsByTagName('estadoAjuste')[0].firstChild.data) {

                                    case 'NUMERAR':
                                        //modificarCRUD('actualizarFacturaGenerada');
                                        break;

                                    case 'MAXIMO_CUENTA':
                                        modificarCRUD('ajustarMaximoCuenta')
                                        break;

                                    case 'MAXIMO_ANUAL':
                                        modificarCRUD('ajustarCuentaAnual')
                                        break;

                                }

                                setTimeout("guardarYtraerDatoAlListado('consultarValoresCuenta')", 200);
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }

                            break;


                            case 'enviarSF':
                         if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                        buscarSuministros('listaPanelMedicamentos');
                         }
                        break;

                        case 'auditarRecibo':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                limpiarDivEditarJuan('auditarRecibo');
                                buscarFacturacion('listGrillaRecibos');
                            }
                            break;

                        case 'auditarFactura':

                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                limpiarDivEditarJuan('auditarFactura');
                                buscarFacturacion('listGrillaFacturas');
                            }


                            break;

                        case 'anularFacturaWS':

                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarFacturacion('listGrillaFacturas');
                                alert('FACTURA ANULADA EXISTOSAMENTE');
                            }

                            break;

                        case 'cerrarDocumentoProgramacion':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()																
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosBodegaProgramacion');
                                /* limpiar campos de las transacciones */

                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'cerrarDocumentoSolicitudesInventario':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()																
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosBodegaConsumo');
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'modificarTransaccionDocumento':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarSuministros('listGrillaTransaccionDocumentoHistorico');
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                ocultar('divVentanitaModTransaccion')
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'eliminarTransaccionDocumento':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarSuministros('listGrillaTransaccionDocumentoHistorico');
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                ocultar('divVentanitaModTransaccion')
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'verificaTransaccionesInventario':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()								
                                /*CAMBIAR EL LABEL DEL ESTADO A 1*/
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosBodega');
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;

                        case 'verificaTransaccionesInventarioTrasladoBodega':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()								
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosTrasladoBodega');
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;



                        case 'consultaExamenFisico':
                            asignaAtributo('txtExamenFisicoDescripcion', raiz.getElementsByTagName('examenFisicoDescripcion')[0].firstChild.data, 0);
                            break;


                        case 'elementosDelFolio':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                imprimirHCPdf_paraFinalizar()
                                //cerrarDocumentClinicoSinImp()
                            } else {
                                alert('ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'buscarSiEsNoPos':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                if (verificarCamposGuardar('listMedicacion')) {
                                    mostrar('divVentanitaMedicamentosNoPos')
                                    cargarTableVistaPreviaNoPos('noPos');
                                }
                            } else {
                                modificarCRUD('listMedicacion');
                            }
                            break;

                        case 'buscarSiEsNoPosProcedim':
                            if (verificarCamposGuardar('listProcedimientosDetalle')) {
                                // verificarNotificacionAntesDeGuardar('verificarEmbarazoProcedimiento');
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups')
                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                            	if(verificarCamposGuardar('listProcedimientosDetalle')){ 
                            	  mostrar('divVentanitaMedicamentosNoPos')
                            	  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            	}
                            }   
                            else{ 
                               modificarCRUD('listProcedimientosDetalle');
                            }*/
                            break;

                        case 'buscarSiEsNoPosProcedimientoNotificacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            } else {
                                modificarCRUD('listProcedimientosDetalle');
                            }

                            break

                        case 'buscarSiEsNoPosAyudaDiagnostica':
                            if (verificarCamposGuardar('listAyudasDiagnosticasDetalle')) {
                                //verificarNotificacionAntesDeGuardar('verificarEmbarazoAyudaDiagnostica');
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups2');
                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                            	if(verificarCamposGuardar('listAyudasDiagnosticasDetalle')){ 
                            	  mostrar('divVentanitaMedicamentosNoPos')
                            	  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            	}
                            }   
                            else{ 
                               modificarCRUD('listAyudasDiagnosticasDetalle');
                            }*/

                            break;

                        case 'buscarSiEsNoPosAyudaDiagnosticaNotificacion':

                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {

                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');

                            } else {
                                modificarCRUD('listAyudasDiagnosticasDetalle');
                            }
                            break;


                        case 'buscarSiEsNoPosLaboratorio':

                            if (verificarCamposGuardar('listLaboratoriosDetalle')) {
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups4');

                                //verificarNotificacionAntesDeGuardar('verificarEmbarazoLaboratorio');
                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                            	if(verificarCamposGuardar('listLaboratoriosDetalle')){ 
                            	  mostrar('divVentanitaMedicamentosNoPos')
                            	  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            	}
                            }   
                            else{ 
                               modificarCRUD('listLaboratoriosDetalle');
                            }*/
                            break;


                        case 'buscarSiEsNoPosLaboratorioNotificacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            } else {
                                modificarCRUD('listLaboratoriosDetalle');
                            }
                            break;


                        case 'buscarSiEsNoPosTerapia':


                            if (verificarCamposGuardar('listTerapiaDetalle')) {
                                //verificarNotificacionAntesDeGuardar('verificarEmbarazoTerapia');
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups3')

                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                            	if(verificarCamposGuardar('listTerapiaDetalle')){ 
                            	  mostrar('divVentanitaMedicamentosNoPos')
                            	  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            	}
                            }   
                            else{ 
                               modificarCRUD('listTerapiaDetalle');
                            }*/
                            break;


                        case 'buscarSiEsNoPosTerapiaNotificacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            } else {
                                modificarCRUD('listTerapiaDetalle');
                            }
                            break;


                        case 'buscarPendienteLE':

                            //alert('.111..'+raiz.getElementsByTagName('existe')[0].firstChild.data)

                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {
                                    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK')
                                        modificarCRUD('crearCitaCirugia')
                                    else modificarCRUD('crearCita');
                                } else {
                                    if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'S') {
                                        alert('PACIENTE YA TIENE UNA CITA O LISTA DE ESPERA DE ESTA ESPECIALIDAD Y SUB ESPECIALIDAD !!!')
                                    } else if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'O') { /*CITA OCUPADA*/
                                        alert('1. EL ESPACIO DE LA CITA YA SE ENCUENTRA OCUPADO')
                                    }

                                }
                            }
                            break;
                        case 'consultarDisponibleDesdeAgenda':

                            switch (raiz.getElementsByTagName('existe')[0].firstChild.data) {
                                case 'C':
                                    alert('1-. PACIENTE YA TIENE CITA A FUTURO')
                                    break;
                                case 'S':
                                    alert('2-. PACIENTE TIENE LISTA DE ESPERA A FUTURO')
                                    break;
                                case 'O':
                                    alert('3-. EL ESPACIO DE LA CITA YA SE ENCUENTRA OCUPADO')
                                    break;
                                case 'N':
                                    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
                                        verificarNotificacionAntesDeGuardar('crearCitaCirugia')
                                        //modificarCRUD('crearCitaCirugia')
                                    } else {
                                        verificarNotificacionAntesDeGuardar('crearCita')
                                        //modificarCRUD('crearCita')
                                    }


                                    break;
                            }
                            break;

                        case 'buscarPendienteLE_LE':
                            switch (raiz.getElementsByTagName('existe')[0].firstChild.data) {
                                case 'C':
                                    alert('1-. PACIENTE YA TIENE CITA A FUTURO')
                                    break;
                                case 'S':
                                    alert('2-. PACIENTE TIENE LISTA DE ESPERA A FUTURO')
                                    break;
                                case 'O':
                                    alert('3-. EL ESPACIO DE LA CITA YA SE ENCUENTRA OCUPADO')
                                    break;
                                case 'N':
                                    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
                                        if (valorAtributo('txt_banderaOpcionCirugiaGestion') == 'OKGESTIONPROC') {
                                            modificarCRUD('crearListaEsperaCirugiaProgramacion')
                                        } else {
                                            modificarCRUD('crearListaEsperaCirugia')
                                        }
                                    } else if (valorAtributo('txt_banderaOpcionCirugia') == 'NO') {
                                        modificarCRUD('crearListaEspera');
                                    }
                                    break;
                            }
                            break;

                        case 'buscarIdentificacionPaciente':
                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {
                                    alert('PACIENTE NO EXISTE !!! \n DEBE USTED BUSCAR POR APELLIDOS SEGUIDO DE NOMBRES, O CON DIFERENTE TIPO DE IDENTIFICACION EN LA SIGUIENTE VENTANA ')
                                    traerVentanita('txtIdBusPaciente', '', 'divParaVentanita', 'txtIdBusPaciente', '7')

                                } else {
                                    mostrar('divVentanitaPacienteYaExiste')
                                    asignaAtributo('lblIdPacienteExiste', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    asignaAtributo('txtIdBusPaciente', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    asignaAtributo('lblLetrero', ' PACIENTE  YA EXISTE  !!!', 0)
                                    buscarAGENDA('listHistoricoListaEsperaEnAgenda')
                                    //document.getElementById('divContenidoListaEsperaHist').innerHTML = '<table id="listHistoricoListaEsperaEnAgenda" class="scroll"></table>';
                                }
                            }
                            break;

                        case 'buscarIdPacienteEncuesta':

                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {

                                    asignaAtributo("lblTipoIdCrearPacienteURG", valorAtributo("cmbTipoIdUrgencias"), 1)
                                    asignaAtributo("lblIdCrearPacienteURG", valorAtributo("txtIdentificacionUrgencias"), 1)
                                    if (valorAtributo("cmbTipoIdUrgencias") == "MS") {
                                        calendario("txtFechaNacimientoUrgencias", 1)
                                    } else if (valorAtributo("cmbTipoIdUrgencias") == "AS") {
                                        fecha = new Date();
                                        fecha.setFullYear(fecha.getFullYear() - 18)
                                        a = fecha.toISOString().slice(0, 10)
                                        asignaAtributo("txtFechaNacimientoUrgencias", a, 0)
                                    }
                                    limpiaAtributo("txtNombre1Urgencias", 0)
                                    limpiaAtributo("txtNombre2Urgencias", 0)
                                    limpiaAtributo("txtApellido1Urgencias", 0)
                                    limpiaAtributo("txtApellido2Urgencias", 0)

                                    asignaAtributo("cmbSexoUrgencias", "O", 0)
                                    limpiaAtributo("txtFechaNacimientoUrgencias", 0)
                                    limpiaAtributo("txtTelefonoUrgencias", 0)
                                    limpiaAtributo("txtCelular1Urgencias", 0)
                                    limpiaAtributo("txtCelular2Urgencias", 0)
                                    asignaAtributo("txtAdministradoraUrgencias", "65-1055 PARTICULAR ADSCRITO", 0)

                                    asignaAtributo("txtMunicipioResidenciaUrgencias", "52001-PASTO", 0)
                                    limpiaAtributo("txtDireccionUrgencias", 0)
                                    limpiaAtributo("txtEmailUrgencias", 0)

                                    mostrar("divCrearPacienteUrgencias")
                                } else {
                                    asignaAtributo('txtIdBusPaciente', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    setTimeout(() => {
                                        buscarInformacionBasicaPaciente()
                                        setTimeout(() => {
                                            cargarInformacionContactoPaciente()
                                            $("#sw_nuevo_paciente").removeAttr("checked");
                                            ocultar("elementosNuevoPaciente")
                                            limpiaAtributo("cmbTipoIdUrgencias", 0)
                                            limpiaAtributo("txtIdentificacionUrgencias", 0)
                                        }, 200);
                                    }, 200);
                                }
                            }
                            break;

                        case 'buscarIdentificacionPacienteUrgencias':
                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {
                                    if (confirm("PACIENTE NO ENCONTRADO \n DESEA CREAR NUEVO PACIENTE CON NUMERO DE DOCUMENTO \n " + valorAtributo("cmbTipoIdUrgencias") + valorAtributo("txtIdentificacionUrgencias") + "?")) {
                                        asignaAtributo("lblTipoIdCrearPacienteURG", valorAtributo("cmbTipoIdUrgencias"), 1)
                                        asignaAtributo("lblIdCrearPacienteURG", valorAtributo("txtIdentificacionUrgencias"), 1)
                                        if (valorAtributo("cmbTipoIdUrgencias") == "MS") {
                                            calendario("txtFechaNacimientoUrgencias", 1)
                                        } else if (valorAtributo("cmbTipoIdUrgencias") == "AS") {
                                            fecha = new Date();
                                            fecha.setFullYear(fecha.getFullYear() - 18)
                                            a = fecha.toISOString().slice(0, 10)
                                            asignaAtributo("txtFechaNacimientoUrgencias", a, 0)
                                        }
                                        limpiaAtributo("txtNombre1Urgencias", 0)
                                        limpiaAtributo("txtNombre2Urgencias", 0)
                                        limpiaAtributo("txtApellido1Urgencias", 0)
                                        limpiaAtributo("txtApellido2Urgencias", 0)

                                        asignaAtributo("cmbSexoUrgencias", "O", 0)
                                        limpiaAtributo("txtFechaNacimientoUrgencias", 0)
                                        limpiaAtributo("txtTelefonoUrgencias", 0)
                                        limpiaAtributo("txtCelular1Urgencias", 0)
                                        limpiaAtributo("txtCelular2Urgencias", 0)
                                        asignaAtributo("txtAdministradoraUrgencias", "65-1055 PARTICULAR ADSCRITO", 0)

                                        asignaAtributo("txtMunicipioResidenciaUrgencias", "52001-PASTO", 0)
                                        limpiaAtributo("txtDireccionUrgencias", 0)
                                        limpiaAtributo("txtEmailUrgencias", 0)

                                        mostrar("divCrearPacienteUrgencias")
                                    }
                                } else {
                                    asignaAtributo('txtIdBusPacienteUrgencias', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    asignaAtributo('lblIdPacienteUrgencias', raiz.getElementsByTagName('dato')[0].firstChild.data.split("-")[0], 0)
                                }
                            }
                            break;

                        case 'nuevoPaciente':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N')
                                    alert(raiz.getElementsByTagName('dato')[0].firstChild.data + '\n \n                           CREADO SATISFACTORIAMENTE  !!!')
                                else alert(raiz.getElementsByTagName('dato')[0].firstChild.data + '\n \n                           YA EXISTE  !!!')
                                asignaAtributo('txtIdBusPaciente', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                            }
                            break;

                        case 'nuevoDocumentoHC':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') {
                                asignaAtributo('lblIdDocumento', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdAdmision', raiz.getElementsByTagName('id_admision')[0].firstChild.data, 1);

                                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');

                                setTimeout("llenarTablaAlAcordion(valorAtributo('lblIdDocumento'), valorAtributo('cmbTipoDocumento'), valorAtributoCombo('cmbTipoDocumento') ) ", 500)

                                mostrar('divContenidos')
                                asignaAtributo('lblTipoDocumento', valorAtributo('cmbTipoDocumento'), 0)
                                asignaAtributo('lblDescripcionTipoDocumento', valorAtributoCombo('cmbTipoDocumento'), 0)
                                asignaAtributo('txtIdEsdadoDocumento', '0', 0)
                                asignaAtributo('lblNomEstadoDocumento', 'Abierto', 0)

                                habilitar('btn_cerrarDocumentoHC', 1)
                                habilitar('btnEliminarMedicamentoListado', 1)
                                habilitar('btnEliminarProcedimientos', 1)
                                habilitar('btnProcedimiento', 1)
                            }
                            break;
                        case 'nuevaAdmisionCuenta':
                            if (raiz.getElementsByTagName('no_autorizacion')[0].firstChild.data != '') {
                                alert('NUMERO DE AUTORIZACION YA EXISTE')
                            }
                            break;

                        case 'consultarValoresCuenta':
                            /*document.getElementById("recibosCopago").innerHTML = ""                            
                            document.getElementById("otrosRecibos").innerHTML = ""
                            document.getElementById("reciboAjuste").innerHTML = ""*/

                            asignaAtributo('lblValorNoCubierto', raiz.getElementsByTagName('valor_nocubierto')[0].firstChild.data, 0);
                            asignaAtributo('lblValorCubierto', raiz.getElementsByTagName('valor_cubierto')[0].firstChild.data, 0);
                            asignaAtributo('lblDescuento', raiz.getElementsByTagName('valor_descuento')[0].firstChild.data, 0);
                            asignaAtributo('lblTotalFactura', raiz.getElementsByTagName('total_factura')[0].firstChild.data, 0);
                            asignaAtributo('lblIdEstadoFactura', raiz.getElementsByTagName('id_estado')[0].firstChild.data, 0);
                            asignaAtributo('lblEstadoFactura', raiz.getElementsByTagName('estado')[0].firstChild.data, 0);
                            asignaAtributo('lblIdFactura', raiz.getElementsByTagName('id_factura')[0].firstChild.data, 0);
                            asignaAtributo('lblNumeroFactura', raiz.getElementsByTagName('numero_factura')[0].firstChild.data, 0);

                            if (raiz.getElementsByTagName('medio_pago')[0].firstChild.data == '1') {
                                $("#efectivo").attr('checked', true);
                            } else if (raiz.getElementsByTagName('medio_pago')[0].firstChild.data == '2') {
                                $("#tarjeta").attr('checked', true);
                            } else {
                                alert("EL MEDIO DE PAGO NO SE PUEDE MOSTRAR")
                            }

                            if (raiz.getElementsByTagName('forma_pago')[0].firstChild.data == '1') {
                                $("#contado").attr('checked', true);
                            } else if (raiz.getElementsByTagName('forma_pago')[0].firstChild.data == '2') {
                                $("#credito").attr('checked', true);
                            } else {
                                alert("LA FORMA DE PAGO NO SE PUEDE MOSTRAR")
                            }

                            if (raiz.getElementsByTagName('id_estado')[0].firstChild.data != 'P') {
                                $("input:radio[name=medioPago]").each(function () {
                                    $(this).attr('disabled', 'disabled')
                                });
                                $("input:radio[name=formaPago]").each(function () {
                                    $(this).attr('disabled', 'disabled')
                                });
                            } else {
                                if (raiz.getElementsByTagName('prefijo_factura')[0].firstChild.data == 'FS') {
                                    $("input:radio[name=medioPago]").each(function () {
                                        $(this).removeAttr('disabled')
                                    });
                                    $("input:radio[name=formaPago]").each(function () {
                                        $(this).attr('disabled', 'disabled')
                                    });
                                } else {
                                    $("input:radio[name=medioPago]").each(function () {
                                        $(this).removeAttr('disabled')
                                    });
                                    $("input:radio[name=formaPago]").each(function () {
                                        $(this).removeAttr('disabled')
                                    });
                                }
                            }

                            if (raiz.getElementsByTagName('prefijo_factura')[0].firstChild.data == 'FS') {
                                $("input:radio[name=formaPago]").each(function () {
                                    $(this).attr('disabled', 'disabled')
                                });
                            }

                            //ESTA SECCION DIBUJA LOS BOTONES DE LOS RCIBOS ASOCIADOS A UNA FACTURA

                            /*for (i = 0; i < raiz.getElementsByTagName('recibos_copago').length; i++) {
                                td = document.getElementById("recibosCopago")
                                id_recibo = raiz.getElementsByTagName('recibos_copago')[i].firstChild.data
                                input = document.createElement("input")
                                input.setAttribute('class', 'small button blue')
                                input.setAttribute('value', id_recibo)
                                input.setAttribute('onclick', "formatoPDFRecibo(" + id_recibo + ")")
                                input.setAttribute('type', 'button')
                                input.setAttribute('style', 'width:20%;')
                                input.setAttribute('style', 'margin: 10px;')
                                td.appendChild(input)
                            }

                            for (i = 0; i < raiz.getElementsByTagName('otros_recibos').length; i++) {
                                td = document.getElementById("otrosRecibos")
                                id_recibo = raiz.getElementsByTagName('otros_recibos')[i].firstChild.data
                                input = document.createElement("input")
                                input.setAttribute('class', 'small button blue')
                                input.setAttribute('value', id_recibo)
                                input.setAttribute('onclick', "formatoPDFRecibo(" + id_recibo + ")")
                                input.setAttribute('type', 'button')
                                input.setAttribute('style', 'width:20%;')
                                input.setAttribute('style', 'margin: 10px;')
                                td.appendChild(input)
                            }

                            if (raiz.getElementsByTagName('recibo_ajuste').length > 0) {
                                td = document.getElementById("reciboAjuste")
                                id_recibo = raiz.getElementsByTagName('recibo_ajuste')[0].firstChild.data
                                input = document.createElement("input")
                                input.setAttribute('class', 'small button blue')
                                input.setAttribute('value', id_recibo)
                                input.setAttribute('onclick', "formatoPDFRecibo(" + id_recibo + ")")
                                input.setAttribute('type', 'button')
                                input.setAttribute('style', 'width:40%;')
                                td.appendChild(input)
                            }*/

                            buscarFacturacion('listRecibosFactura')
                            //setTimeout("modificarCRUD('validarSaldoCredito')", 1000)
                            break;

                        case 'verificaCantidadInventario':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var cantidad = parseInt(valorAtributo('txtCantidad'));
                                var cantMax = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);

                                if (cantidad > cantMax) {
                                    if (!confirm('LA MAXIMA CANTIDAD QUE SE HA INGRESADO HISTORICAMENTE ES : ' + cantMax + '\n Desea continuar ?')) {
                                        asignaAtributo('txtCantidad', '', 0);
                                    }
                                } else guardarYtraerDatoAlListado('verificaCantidadInventarioMinimo')
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'verificaCantidadInventarioMinimo':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var cantidad = parseInt(valorAtributo('txtCantidad'));
                                var cantMax = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);
                                if (cantidad < cantMax) {
                                    if (!confirm('LA MINIMA CANTIDAD QUE SE HA INGRESADO HISTORICAMENTE ES : ' + cantMax + '\n Desea continuar ?')) {
                                        asignaAtributo('txtCantidad', '', 0);
                                    }
                                }
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'verificaValorInventario':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var txtValorU = parseInt(valorAtributo('txtValorU'));
                                var cantMax = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);

                                if (txtValorU > cantMax) {
                                    if (!confirm('EL MAXIMO VALOR UNITARIO QUE SE HA INGRESADO HISTORICAMENTE ES = ' + raiz.getElementsByTagName('cantidad')[0].firstChild.data + '\n Desea continuar ?')) {
                                        asignaAtributo('txtValorU', '', 0);
                                    }
                                } else guardarYtraerDatoAlListado('verificaValorInventarioMinimo')
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'verificaValorInventarioMinimo':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var txtValorU = parseInt(valorAtributo('txtValorU'));
                                var cant = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);

                                if (txtValorU < cant) {
                                    if (!confirm('EL MINIMO VALOR UNITARIO QUE SE HA INGRESADO HISTORICAMENTE ES = ' + raiz.getElementsByTagName('cantidad')[0].firstChild.data + '\n Desea continuar ?')) {
                                        asignaAtributo('txtValorU', '', 0);
                                    }
                                }
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'nuevoDocumentoDespachoSolicitud':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                alert('despacho de solicitud');
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodega')
                            }
                            break;
                        case 'nuevoDocumentoInventarioBodegaConsumo':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto							  
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodegaConsumo');
                                /* cargar elementos de bodega de consumo en hoja de gasto */
                                if (valorAtributo('cmbIdTipoDocumento') == '21') {
                                    if (valorAtributo('cmbIdBodega') != '') {
                                        setTimeout("buscarSuministros('listGrillaElementosBodegaConsumo')", 400);
                                    }
                                }
                            }
                            break;
                        case 'buscarConciliacion':
                            if (raiz.getElementsByTagName('dato1')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('cmbExiste', raiz.getElementsByTagName('dato1')[0].firstChild.data, 0);
                                asignaAtributo('txtConciliacionDescripcion', raiz.getElementsByTagName('dato2')[0].firstChild.data, 0);
                                buscarHC('listAntFarmacologicos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
                                setTimeout("buscarHC('listMedicacionOrdenada' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 500);


                            }
                            break;
                        case 'nuevoDocumentoInventarioProgramacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodegaProgramacion')
                            }
                            break;
                        case 'crearDocInvBodega':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosTrasladoBodega')
                            }
                            break;
                        case 'existeDocumentoInventarioBodega':
                            if (raiz.getElementsByTagName('respuesta')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto							  

                                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data != 'true') {
                                    alert("EL NUMERO DE FACTURA CON IGUAL TERCERO YA EXISTE !!! ")
                                } else {
                                    guardarYtraerDatoAlListado('nuevoDocumentoInventarioBodega');
                                }
                            } else {
                                alert("Ya se encuentra registrado un documento con igual numero de factura y proveedor ")
                            }
                            break;
                        case 'nuevoDocumentoInventarioBodega':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodega')
                                /* cargar transacciones del documento */
                                limpiarDivEditarJuan('limpCamposTransaccion')
                                if (valorAtributo('txtNaturaleza') == 'I') {
                                    setTimeout("buscarSuministros('listGrillaTransaccionDocumento')", 500);
                                } else {
                                    setTimeout("buscarSuministros('listGrillaTransaccionSalida')", 500);
                                }
                            }
                            break;
                        case 'ocuparCandadoReporte':
                            vanOcuparCandadoReporte = raiz.getElementsByTagName('dato')[0].firstChild.data;
                            alert('respueta=' + vanOcuparCandadoReporte)
                            return vanOcuparCandadoReporte;
                            break;


                        case 'agregarTurno':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'true') {
                                modificarCRUD('agregarTurno');
                            }
                            break;

                        case 'eliminarTurno':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'true') {
                                modificarCRUD('eliminarTurno');
                            }
                            break;

                        case 'modificarTurno':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'true') {
                                modificarCRUD('modificarTurno');
                            }
                            break;
                    }


                } else {
                    alert("No se pudo ingresar la informacion");
                }

            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}


function eliminarAlListado(opcion) {

    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/eliminarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestaeliminarAlListado(opcion) };

    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    valores_a_mandar = "accion=" + opcion;
    valores_a_mandar = valores_a_mandar + "&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();

    switch (opcion) {

        case 'listProfesionalesFicha':
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + valorAtributo('cmbIdAreaDerecho');
            valores_a_mandar = valores_a_mandar + "&idProfesional=" + valorAtributo('cmbProfesional');

            break;
        case 'listDxIntegralInicial':
            valores_a_mandar = valores_a_mandar + "&id=" + $('#drag' + ventanaActual.num).find('#lblIdDx').html();
            break;
        case 'listSituaActual':
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&id=" + $('#drag' + ventanaActual.num).find('#lblIdSituacionActual_' + tabActivo).html();
            break;
        case 'listObjMeta':
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&id=" + $('#drag' + ventanaActual.num).find('#lblIdObjetivo_' + tabActivo).html();
            break;
        case 'listActividad':
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&id=" + $('#drag' + ventanaActual.num).find('#lblIdActividad_' + tabActivo).html();
            break;


    }

    //alert('eliminarAlListado...'+valores_a_mandar);  
    varajaxInit.send(valores_a_mandar);

}

function respuestaeliminarAlListado(opcion) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('raiz') != null) {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) {
                    listadoTablaHC(opcion);
                    limpiarElementosDeSeleccionParaLista(opcion);
                } else {
                    alert("No se pudo ingresar la informaci�n");
                }

            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}



var tipoDocuHc = '';


function listadoTablaHC(caso) {

    var pag = '/clinica/paginas/accionesXml/buscarHc_xml.jsp';
    //	tabActivo= $('#drag'+ventanaActual.num).find('#IdTabActivo').val();			
    var ancho = ($('#drag' + ventanaActual.num).find("#divEditar").width()) - 60;
    switch (caso) {
        case 'listOrdenesMedicamentos':
            buscarHC(caso, '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

            if (valorAtributo('cmbTipoDocumento') == 'RCOD') {
                habilitar('chkInmediato', 1)
                ocultar('idDivReglaHoras')
                mostrar('idDivInmediato')
            } else {
                habilitar('chkInmediato', 0)
                mostrar('idDivReglaHoras');
                ocultar('idDivInmediato')
            }

            break;
        case 'listOrdenesMedicamentosModificar':
            buscarHC('listOrdenesMedicamentos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            break;
        case 'listOrdenesInsumos':
            buscarHC(caso, '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            break;

        case 'listProfesionalesFicha':
            valores_a_mandar = pag + "?accion=" + caso;
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&sidx=1&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();
            $('#drag' + ventanaActual.num).find('#' + caso).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'idAreaDerecho', 'Area Derecho', 'Servicio', 'idProfesional', 'Profesional', 'Fecha Elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'idAreaDerecho', index: 'idAreaDerecho', hidden: true },
                    { name: 'AreaDerecho', index: 'AreaDerecho', width: anchoP(ancho, 20) },
                    { name: 'Servicio', index: 'Servicio', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'idProfesional', index: 'idProfesional', hidden: true },
                    { name: 'nomProfesional', index: 'nomProfesional', width: anchoP(ancho, 40), align: 'left' },
                    { name: 'fechaElaboro', index: 'fechaElaboro', width: anchoP(ancho, 20), align: 'left' },
                ],
                ondblClickRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + caso).getRowData(rowid);

                    asignaAtributo('cmbProfesional', datosRow.idProfesional, 0);
                    asignaAtributo('cmbIdAreaDerecho', datosRow.idAreaDerecho, 0);
                },
                height: 111,
                width: 700,
            }); //.navGrid("#pagerAntF",{edit:false,add:false,del:false});		
            $('#drag' + ventanaActual.num).find("#" + caso).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listDxIntegralInicial':
            valores_a_mandar = pag + "?accion=" + caso;
            valores_a_mandar = valores_a_mandar + "&sidx=1&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();
            $('#drag' + ventanaActual.num).find('#' + caso).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'Descripci�n del Diagn�stico inicial', 'Servicio', 'idProfesional', 'Profesional', 'Fecha Elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Descripcion', index: 'Descripcion', width: ((ancho * 77) / 100) },
                    { name: 'Servicio', index: 'Servicio', width: ((ancho * 5) / 100), align: 'left' },
                    { name: 'idProfesional', index: 'idProfesional', hidden: true },
                    { name: 'nomProfesional', index: 'nomProfesional', width: ((ancho * 10) / 100), align: 'left' },
                    { name: 'fechaElaboro', index: 'fechaElaboro', width: ((ancho * 7) / 100), align: 'left' },
                ],

                pager: jQuery('#pager' + caso),
                rowNum: 10,
                rowList: [20, 50, 100],
                sortname: 'FechaHora',
                sortorder: "FechaHora",
                viewrecords: true,
                hidegrid: false,

                ondblClickRow: function (rowid) {
                    var datosDelRegistro = jQuery('#drag' + ventanaActual.num).find('#' + caso).getRowData(rowid);
                    if (datosDelRegistro.idProfesional == $('#drag' + ventanaActual.num).find('#IdUsuario').val()) {

                        $('#drag' + ventanaActual.num).find('#txtDxIntegralInicial').val($.trim(datosDelRegistro.Descripcion));
                        $('#drag' + ventanaActual.num).find('#lblIdDx').html($.trim(datosDelRegistro.id));
                    } else alert('Ud no puede editar porque no es el autor')

                },
                height: 90,
                width: ancho,
            }); //.navGrid("#pagerAntF",{edit:false,add:false,del:false});		
            $('#drag' + ventanaActual.num).find("#" + caso).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listSituaActual':
            valores_a_mandar = pag + "?accion=" + caso;
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&sidx=1&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();
            //alert('Area Derechossss '+valores_a_mandar);
            $('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'Descripci�n', 'Servicio', 'idProfesional', 'Profesional', 'Fecha Elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Descripcion', index: 'Descripcion', width: ((ancho * 77) / 100) },
                    { name: 'Servicio', index: 'Servicio', width: ((ancho * 5) / 100), align: 'left' },
                    { name: 'idProfesional', index: 'idProfesional', hidden: true },
                    { name: 'nomProfesional', index: 'nomProfesional', width: ((ancho * 10) / 100), align: 'left' },
                    { name: 'fechaElaboro', index: 'fechaElaboro', width: ((ancho * 7) / 100), align: 'left' },
                ],

                ondblClickRow: function (rowid) {
                    var datosDelRegistro = jQuery('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).getRowData(rowid);

                    if (datosDelRegistro.idProfesional == $('#drag' + ventanaActual.num).find('#IdUsuario').val() || $('#drag' + ventanaActual.num).find('#IdUsuario').val() == 'CC000098392395') {
                        $('#drag' + ventanaActual.num).find('#txtSituacionActual_' + tabActivo).val($.trim(datosDelRegistro.Descripcion));
                        $('#drag' + ventanaActual.num).find('#lblIdSituacionActual_' + tabActivo).html($.trim(datosDelRegistro.id));
                    } else alert('Ud no puede editar porque no es el autor.')
                },
                height: 111,
                width: ancho,
            }); //.navGrid("#pagerAntF",{edit:false,add:false,del:false});		
            $('#drag' + ventanaActual.num).find("#" + caso + '_' + tabActivo).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listObjMeta':
            valores_a_mandar = pag + "?accion=" + caso;
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&sidx=1&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();
            $('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'Objetivo', 'Meta', 'Servicio', 'idProfesional', 'Profesional', 'Fecha Elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Objetivo', index: 'Objetivo', width: ((ancho * 40) / 100) },
                    { name: 'Meta', index: 'Meta', width: ((ancho * 40) / 100) },
                    { name: 'Servicio', index: 'Servicio', width: ((ancho * 5) / 100), align: 'left' },
                    { name: 'idProfesional', index: 'idProfesional', hidden: true },
                    { name: 'nomProfesional', index: 'nomProfesional', width: ((ancho * 10) / 100), align: 'left' },
                    { name: 'fechaElaboro', index: 'fechaElaboro', width: ((ancho * 5) / 100), align: 'left' },
                ],

                ondblClickRow: function (rowid) {
                    var datosDelRegistro = jQuery('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).getRowData(rowid);
                    if (datosDelRegistro.idProfesional == $('#drag' + ventanaActual.num).find('#IdUsuario').val()) {
                        $('#drag' + ventanaActual.num).find('#txtObjetivo_' + tabActivo).val($.trim(datosDelRegistro.Objetivo));
                        $('#drag' + ventanaActual.num).find('#txtMeta_' + tabActivo).val($.trim(datosDelRegistro.Meta));
                        $('#drag' + ventanaActual.num).find('#lblIdObjetivo_' + tabActivo).html($.trim(datosDelRegistro.id));
                    } else alert('Ud no puede editar porque no es el autor')

                },
                height: 111,
                width: ancho,
            }); //.navGrid("#pagerAntF",{edit:false,add:false,del:false});		
            $('#drag' + ventanaActual.num).find("#" + caso + '_' + tabActivo).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listActividad':
            valores_a_mandar = pag + "?accion=" + caso;
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&sidx=1&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();
            //alert('Area Derechossss '+valores_a_mandar);
            $('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'Descripci�n', 'Servicio', 'idProfesional', 'Profesional', 'Fecha Elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Descripcion', index: 'Descripcion', width: ((ancho * 77) / 100) },
                    { name: 'Servicio', index: 'Servicio', width: ((ancho * 5) / 100), align: 'left' },
                    { name: 'idProfesional', index: 'idProfesional', hidden: true },
                    { name: 'nomProfesional', index: 'nomProfesional', width: ((ancho * 10) / 100), align: 'left' },
                    { name: 'fechaElaboro', index: 'fechaElaboro', width: ((ancho * 7) / 100), align: 'left' },
                ],

                ondblClickRow: function (rowid) {
                    var datosDelRegistro = jQuery('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).getRowData(rowid);
                    if (datosDelRegistro.idProfesional == $('#drag' + ventanaActual.num).find('#IdUsuario').val()) {
                        $('#drag' + ventanaActual.num).find('#txtActividad_' + tabActivo).val($.trim(datosDelRegistro.Descripcion));
                        $('#drag' + ventanaActual.num).find('#lblIdActividad_' + tabActivo).html($.trim(datosDelRegistro.id));
                    } else alert('Ud no puede editar porque no es el autor')

                },
                height: 111,
                width: ancho,
            }); //.navGrid("#pagerAntF",{edit:false,add:false,del:false});		
            $('#drag' + ventanaActual.num).find("#" + caso + '_' + tabActivo).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

    }
}


function llamarAutocomIdDescripcion(idCampoInput, idQuery) {
    //limpiarDivEditarJuan('reportarEventoAdverso');

    if ($('#drag' + ventanaActual.num).find('#' + idCampoInput).val() == "") { // para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
        datos_a_mandar = "accion=idQuery";
        datos_a_mandar = datos_a_mandar + "&idQuery=" + idQuery;
        varajaxInit = crearAjax();
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarAutocompletar_xml.jsp', true);
        varajaxInit.onreadystatechange = function () { respuestaLlamarAutocomIdDescripcion(idCampoInput) };
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.send(datos_a_mandar);
    }
}

function llamarAutocomProcedimientoFactura(idCampoInput, idQuery, parametro1) {
    if ($('#drag' + ventanaActual.num).find('#' + idCampoInput).val() == "") { // para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
        datos_a_mandar = "accion=idQuery";
        datos_a_mandar = datos_a_mandar + "&idQuery=" + idQuery + "&parametro1=" + valorAtributo(parametro1);
        varajaxInit = crearAjax();
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarAutocompletar_xml.jsp', true);
        varajaxInit.onreadystatechange = function () { respuestallamarAutocomProcedimientoFactura(idCampoInput) };
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.send(datos_a_mandar);
    }
}

function respuestallamarAutocomProcedimientoFactura(idCampoInput) {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('item') != null) {
                cod = raiz.getElementsByTagName('cod');
                text = raiz.getElementsByTagName('text');
                data = new Array();
                for (a = 0; a < text.length; a++) {
                    data[a] = cod[a].firstChild.data + '-' + text[a].firstChild.data;
                }

                $('#drag' + ventanaActual.num).find('#' + idCampoInput).flushCache();
                $('#drag' + ventanaActual.num).find('#' + idCampoInput).autocomplete(data, {
                    multiple: false,
                    matchContains: true, //sirve para buscar en cualquier parte de la frase
                    cacheLength: 1,
                    width: 600,
                    height: 1500,
                    deferRequestBy: 0, //miliseconds
                    selSeparator: '|', //separador de campos '|' por defecto
                    minChars: 1 //minimo de caracteres para empezar a desplegar la lista
                });
                buscarFacturacion('listProcedimientosDeFactura')
            } else {
                alert("No se puede Autocompletar");
            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) { }

    $('#drag' + ventanaActual.num).find('#' + idCampoInput).focus();
}




var idCampoInputGral = '',
    idQueryGral = '',
    idCampoInput2 = '';

function llamarAutocomIdDescripcionParametro(idCampoInput, idQuery, parametro1) {
    if ($('#drag' + ventanaActual.num).find('#' + idCampoInput).val() == "") { // para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
        datos_a_mandar = "accion=idQuery";
        datos_a_mandar = datos_a_mandar + "&idQuery=" + idQuery + "&parametro1=" + valorAtributo(parametro1);
        varajaxInit = crearAjax();
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarAutocompletar_xml.jsp', true);
        varajaxInit.onreadystatechange = function () { respuestaLlamarAutocomIdDescripcion(idCampoInput) };
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.send(datos_a_mandar);
    }
}

function llamarAutocomIdDescripcionParametroEmpresa(idCampoInput, idQuery, parametro1) {
    if ($('#drag' + ventanaActual.num).find('#' + idCampoInput).val() == "") { // para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
        datos_a_mandar = "accion=idQuery";
        datos_a_mandar = datos_a_mandar + "&idQuery=" + idQuery + "&parametro1=" + parametro1;
        varajaxInit = crearAjax();
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarAutocompletar_xml.jsp', true);
        varajaxInit.onreadystatechange = function () { respuestaLlamarAutocomIdDescripcion(idCampoInput) };
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.send(datos_a_mandar);
    }
}
function llamarAutocompletarEmpresa(idCampoInput, idQuery) {
    idCampoInput2 = idCampoInput;
    if (valorAtributo(idCampoInput).length == 0) {
        //setTimeout( 'llamarAutocomIdDescripcionParametro('+idCampoInput+','+idQuery+','+  parametro1 +')');
        llamarAutocomIdDescripcionParametroEmpresa(idCampoInput, idQuery, IdEmpresa())
    }
}

function llamarAutocompletarParametro1(idCampoInput, idQuery, parametro1) {

    idCampoInput2 = idCampoInput;
    console.log("lblIdAdministradora: "+valorAtributo(parametro1));
    if (valorAtributo(parametro1)==''||valorAtributo(parametro1)<1) {
        alert("Paciente sin asignacion de plan\n     por favor agregar plan")        
    } else {
        if (valorAtributo(idCampoInput).length == 0) {
            //setTimeout( 'llamarAutocomIdDescripcionParametro('+idCampoInput+','+idQuery+','+  parametro1 +')');
            llamarAutocomIdDescripcionParametro(idCampoInput, idQuery, parametro1)
        }
    }
    
}


function llamarAutocomIdDescripcionConDato(idCampoInput, idQuery) {

    idCampoInput2 = idCampoInput;
    if (valorAtributo(idCampoInput).length == 0) {
        setTimeout('llamarAutocomIdDescripcionConDatoRetardo(' + idCampoInput + ',' + idQuery + ')', 20);
    }
}

function llamarAutocomIdDescripcionConDatoRetardo(idCampoInput, idQuery) { // sirbe tanto para onkeypress o para onfocus
    //alert(idCampoInput2+'--'+idQuery)
    datos_a_mandar = "accion=idQuery";
    datos_a_mandar = datos_a_mandar + "&idQuery=" + idQuery;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarAutocompletar_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestaLlamarAutocomIdDescripcion(idCampoInput2) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxInit.send(datos_a_mandar);
}

function respuestaLlamarAutocomIdDescripcion(idCampoInput) {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('item') != null) {
                cod = raiz.getElementsByTagName('cod');
                text = raiz.getElementsByTagName('text');
                data = new Array();
                for (a = 0; a < text.length; a++) {
                    data[a] = cod[a].firstChild.data + '-' + text[a].firstChild.data;
                }

                $('#drag' + ventanaActual.num).find('#' + idCampoInput).flushCache();
                $('#drag' + ventanaActual.num).find('#' + idCampoInput).autocomplete(data, {
                    multiple: false,
                    matchContains: true, //sirve para buscar en cualquier parte de la frase
                    // autofill:false,
                    //selectedFirst:false, // ordena segun los caracteres coinidentes
                    cacheLength: 1,
                    width: 600,
                    height: 1500,
                    deferRequestBy: 0, //miliseconds
                    //formatItem:formatItem,
                    //formatResult:formatResult,											  
                    selSeparator: '|', //separador de campos '|' por defecto
                    // lineSeparator:'\n' //separador de lineas (registros) '\n' por defecto
                    minChars: 1 //minimo de caracteres para empezar a desplegar la lista
                });
            } else {
                alert("No se puede Autocompletar");
            }
        } else {
            alert("ERROR 101: Problemas con la red, posiblemente su equipo se desconecto del servidor. Por favor intente realizar la accion nuevamente o pongase en contacto con soporte:" + varajaxInit.status);
        }
    }
    if (varajaxInit.readyState == 1) { }
    $('#drag' + ventanaActual.num).find('#' + idCampoInput).focus();
}


function anchoP(ancho, anchoPorciento) {
    return ((ancho * anchoPorciento) / 100)
    // return anchoPorciento;
}


function cargarDatosIdPaciente() {
    setTimeout("llamarAutocomIdDescripcion('txtBusIdPacienteListaEspera',15)", 500);
}

function cambiarElTabActivo(idTab) {

    //	$('#drag'+ventanaActual.num).find('#IdTabActivo').val(idTab);
    tabActivo = idTab;
    listadoTablaHC('listSituaActual');

    //copiarDeListAList('listSituaActual');		
    setTimeout("listadoTablaHC('listObjMeta')", 500);
    setTimeout("listadoTablaHC('listActividad')", 1900);

}

function alert_xxxx(texto) { // para el funcionamiento real dejar= alert
    jAlert(texto, "ATENCION ! ! !");
}

function alertMal(texto) {
    jAlertMal(texto, "ATENCION ! ! !");
}

function alertBien(texto) {
    jAlertBien(texto, "ATENCION ! ! !");
}

function confirmar(texto) {
    jConfirm(texto, "");
}



var headImpCC = " <head>";
headImpCC += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /> ";
headImpCC += "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\"> ";
headImpCC += "<META HTTP-EQUIV=\"Cache-Control\" CONTENT=\"no-cache\"> ";
headImpCC += "<title> . </title> ";
headImpCC += "<link href=\"/clinica/paginas/hc/documentosHistoriaClinicaPDF/pdf.css\" rel=\"stylesheet\" type=\"text/css\"> ";
headImpCC += "<link href=\"/clinica/paginas/hc/documentosHistoriaClinicaPDF/pdf.css\" rel=\"stylesheet\" type=\"text/css\" media=\"print\"></head> ";


//headImpCC += "<link href=\"/clinica/paginas/reportes/documentosHistoriaClinicaPDF\pdf.css" rel=\"stylesheet\" type=\"text/css\"> ";
//headImpCC += "<link href=\"/clinica/paginas/reportes/documentosHistoriaClinicaPDF\pdf.css" rel=\"stylesheet\" type=\"text/css\" media=\"print\"></head> ";



function imprimirCC(que) {
    var ventana = window.open("", "", "");
    var contenido = "<html>" + headImpCC + "<body onload='window.print();window.close();'>" + document.getElementById(que).innerHTML + "</body></html>";
    ventana.document.open();
    ventana.document.write(contenido);
    //ventana.document.close();
}
//





function copiarDeListAList(areaDerecho) {

    var registros0 = '',
        registros1 = '',
        registros2 = '',
        registros3 = '';

    var ids = jQuery("#listSituaActual_" + areaDerecho).getDataIDs();
    for (var i = 0; i < ids.length; i++) {
        var c = ids[i];
        var datosDelRegistro = jQuery("#listSituaActual_" + areaDerecho).getRowData(c);

        registros0 = registros0 + '- ' + $.trim(datosDelRegistro.Descripcion) + ' <p>';
    }


    ids2 = '';
    ids2 = jQuery("#listObjMeta_" + areaDerecho).getDataIDs();
    for (var i = 0; i < ids2.length; i++) {
        var c2 = ids[i];
        var datosDelRegistro2 = jQuery("#listObjMeta_" + areaDerecho).getRowData(c2);

        registros1 = registros1 + '- ' + $.trim(datosDelRegistro2.Objetivo) + '<p>';
        registros2 = registros2 + '- ' + $.trim(datosDelRegistro2.Meta) + '<p>';
    }

    ids3 = '';
    ids3 = jQuery("#listActividad_" + areaDerecho).getDataIDs();
    for (var i = 0; i < ids3.length; i++) {
        var c3 = ids3[i];
        var datosDelRegistro3 = jQuery("#listActividad_" + areaDerecho).getRowData(c3);
        registros3 = registros3 + '- ' + $.trim(datosDelRegistro3.Descripcion) + '<p>';
    }
    // -------------------------------ingresar a las tablas
    borrarRegistrosTabla('listSituaActual_' + areaDerecho + '_doc');
    tablabody = document.getElementById('listSituaActual_' + areaDerecho + '_doc').lastChild;
    tr = document.createElement("TR");
    th = document.createElement("Td");
    th.innerHTML = registros0;
    tr.appendChild(th);
    tablabody.appendChild(tr);


    borrarRegistrosTabla('listObjMeta_' + areaDerecho + '_doc');
    tablabody = document.getElementById('listObjMeta_' + areaDerecho + '_doc').lastChild;
    tr = document.createElement("TR");
    th = document.createElement("Td");
    th.width = '33%';
    th.innerHTML = registros1;
    tr.appendChild(th);

    th = document.createElement("Td");
    th.width = '33%';
    th.innerHTML = registros3;
    tr.appendChild(th);

    th = document.createElement("Td");
    th.width = '33%';
    th.innerHTML = registros2;
    tr.appendChild(th);
    tablabody.appendChild(tr);


}



function vertanasMoviblesConPunteroMouse(idDiv, zIndex2) {

    $('#drag' + ventanaActual.num).find('#' + idDiv).draggable({
        zIndex: zIndex2,
        ghosting: true,
        opacity: 0.7,
        //containment : 'parent',  para que se mueva dependiendo solo de su parent padre
        handle: '#tituloForma'
    });

}

function ira(opc) { //funcion que recarga valores si se da click en flechas
    switch (opc) {
        case 'P':
            posicionActual = 0;
            document.getElementById('ri').style.display = 'none';
            document.getElementById('ra').style.display = 'none';
            document.getElementById('rs').style.display = 'block';
            document.getElementById('rf').style.display = 'block';
            break;
        case 'A':
            posicionActual--;
            if (posicionActual <= 0) {
                document.getElementById('ri').style.display = 'none';
                document.getElementById('ra').style.display = 'none';
            }
            document.getElementById('rs').style.display = 'block';
            document.getElementById('rf').style.display = 'block';
            break;
        case 'S':
            posicionActual++;
            if (posicionActual >= totalRegistros - 1) {
                document.getElementById('rs').style.display = 'none';
                document.getElementById('rf').style.display = 'none';
            }
            document.getElementById('ra').style.display = 'block';
            document.getElementById('ri').style.display = 'block';
            break;
        case 'U':
            posicionActual = totalRegistros - 1;
            document.getElementById('rs').style.display = 'none';
            document.getElementById('rf').style.display = 'none';
            document.getElementById('ri').style.display = 'block';
            document.getElementById('ra').style.display = 'block';
            break;
    }
    cargarFormulariosBusqueda();
}


function presionaEnter(oEvento) {

    var ttt = document.getElementById('txtPassword').value;

    if (document.getElementById('txtLogin').value != '' && ttt.length > 8)
        validar2();


    var iAscii;
    if (oEvento.keyCode)
        iAscii = oEvento.keyCode;
    else if (oEvento.which)
        iAscii = oEvento.which;
    else return false;
    if (iAscii == 13)
        return true;
    else return false;

}


function cargarFormulariosBusqueda() {
    switch (paginaActual) {
        case 'opcion1':
            break;
        case 'opcion2':
            break;
    }
    //cargarFormulariosBusquedaAng();	
}

//cargar imagenes intercambiables
function MM_swapImgRestore() { //v3.0
    var i, x, a = document.MM_sr;
    for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
}

function MM_preloadImages() { //v3.0
    var d = document;
    if (d.images) {
        if (!d.MM_p) d.MM_p = new Array();
        var i, j = d.MM_p.length,
            a = MM_preloadImages.arguments;
        for (i = 0; i < a.length; i++)
            if (a[i].indexOf("#") != 0) {
                d.MM_p[j] = new Image;
                d.MM_p[j++].src = a[i];
            }
    }
}

function MM_findObj(n, d) { //v4.01
    var p, i, x;
    if (!d) d = document;
    if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document;
        n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all) x = d.all[n];
    for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById) x = d.getElementById(n);
    return x;
}

function MM_swapImage() { //v3.0
    var i, j = 0,
        x, a = MM_swapImage.arguments;
    document.MM_sr = new Array;
    for (i = 0; i < (a.length - 2); i += 3)
        if ((x = MM_findObj(a[i])) != null) {
            document.MM_sr[j++] = x;
            if (!x.oSrc) x.oSrc = x.src;
            x.src = a[i + 2];
        }
}

function desactivarboton(boton) {
    if ($(boton).checked == true) {
        $(boton).checked = false;
    }
}

function imprimirAA(que) {
    var ventana = window.open("", "", "");
    var contenido = "<html>" + headImpCC + "<body onload='window.print();window.close();'>" + document.getElementById(que).innerHTML + "</body></html>";
    ventana.document.open();
    ventana.document.write(contenido);
    ventana.document.close();
}

//Funciones para manejo de codigos en los combos
function cambiarCombo(idcombo, texto, idfoco) {
    band = false;
    for (var i = 0; i < $(idcombo).length; i++) {
        if (trim($(idcombo).options[i].value.toUpperCase()) == trim($(texto).value.toUpperCase())) {
            $(idcombo).selectedIndex = i;
            i = $(idcombo).length;
            band = true;
            $(texto).style.backgroundColor = "#99CCFF";
            $(idcombo).style.backgroundColor = '#99CCFF';
            $(idfoco).focus();
        }
    }
    if (!band) {
        $(texto).value = "NaN->";
        $(texto).style.backgroundColor = "#9999CC";
        $(idcombo).style.backgroundColor = '#FFFFFF';
        $(idcombo).selectedIndex = 0;
    }
    return band;
}

function mandarCombo(idtexto, combo) {
    document.getElementById(idtexto).value = document.getElementById(combo).options[document.getElementById(combo).selectedIndex].value;
    document.getElementById(idtexto).style.backgroundColor = "#99CCFF";
    document.getElementById(combo).style.backgroundColor = '#99CCFF';
}
////////////////////Fin de funciones para codigos y combos


// ubicaciones
function cargarDepartamentos(codPais, pagina) {
    valores_a_mandar = "codPais=" + codPais;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarDeptos;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);

}



function cargarDepartamentos1(codPais, pagina) {

    valores_a_mandar = "codPais=" + codPais;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarDeptos1;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);

}

function cargarDepartamentos2(codPais, pagina) {

    valores_a_mandar = "codPais=" + codPais;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarDeptos2;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);

}

function respuestaCargarDeptos() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbDepartamento').options.length = 1;
            c = raiz.getElementsByTagName('codDepto');
            n = raiz.getElementsByTagName('nomDepto');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbDepartamento').options[(document.getElementById('cmbDepartamento').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }

        } else {
            alert("Problemas de conexion con servidor: " + varajax.status);
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function respuestaCargarDeptos1() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbDepartamento1').options.length = 1;
            c = raiz.getElementsByTagName('codDepto');
            n = raiz.getElementsByTagName('nomDepto');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbDepartamento1').options[(document.getElementById('cmbDepartamento1').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: " + varajax.status);
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function respuestaCargarDeptos2() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbDepartamento2').options.length = 1;
            c = raiz.getElementsByTagName('codDepto');
            n = raiz.getElementsByTagName('nomDepto');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbDepartamento2').options[(document.getElementById('cmbDepartamento2').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: " + varajax.status);
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarCiudades(codDepto, pagina) {
    valores_a_mandar = "codDepto=" + codDepto;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarCiudades;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}

function cargarCiudades1(codDepto, pagina) {
    valores_a_mandar = "codDepto=" + codDepto;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarCiudades1;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}

function cargarCiudades2(codDepto, pagina) {
    valores_a_mandar = "codDepto=" + codDepto;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarCiudades2;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}

function respuestaCargarCiudades() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbMunicipio').options.length = 1;
            c = raiz.getElementsByTagName('codMunicipio');
            n = raiz.getElementsByTagName('nomMunicipio');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbMunicipio').options[(document.getElementById('cmbMunicipio').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: " + varajax.status);
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function respuestaCargarCiudades1() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbMunicipio1').options.length = 1;
            c = raiz.getElementsByTagName('codMunicipio');
            n = raiz.getElementsByTagName('nomMunicipio');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbMunicipio1').options[(document.getElementById('cmbMunicipio1').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: " + varajax.status);
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function respuestaCargarCiudades2() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbMunicipio2').options.length = 1;
            c = raiz.getElementsByTagName('codMunicipio');
            n = raiz.getElementsByTagName('nomMunicipio');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbMunicipio2').options[(document.getElementById('cmbMunicipio2').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: " + varajax.status);
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}



function cargarBarrios(codMunici, pagina) {
    valores_a_mandar = "codMunici=" + codMunici;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarBarrios;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}


var banderaBarrios = false;

function respuestaCargarBarrios() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbBarrios').options.length = 1;
            c = raiz.getElementsByTagName('codBarrio');
            n = raiz.getElementsByTagName('nomBarrio');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    document.getElementById('cmbBarrios').options[(document.getElementById('cmbBarrios').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
                }
            } else {
                if (banderaBarrios == true) {
                    alert('Tenga en cuenta que este municipio no tiene barrrios registrados.\nRegistre nuevos barrios entrando por la opcion de Ubicaciones \ndel menu de Administracion-Seguridad/Parametros generales. ');
                    banderaBarrios = false;
                }
            }
        } else {
            alert("Problemas de conexion con servidor: " + varajax.status);
        }
    }
    if (varajax.readyState == 1) { }
}

function cargarDepartamentosIns(deptoDefecto, comboDep, codPais, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codPais=" + codPais;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarDeptosIns;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestaCargarDeptosIns() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('codDepto');
            n = raiz.getElementsByTagName('nomDepto');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
                    }
                }
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione un departamento]', '', true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: cargar Departamentos " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarCiudadesIns(ciuDefecto, comboCiu, codDepto, pagina) {
    idcombo = comboCiu;
    porDefecto = ciuDefecto;
    valores_a_mandar = "codDepto=" + codDepto;
    varajax2 = crearAjax();
    varajax2.open("POST", pagina, true);
    varajax2.onreadystatechange = respuestaCargarCiudadesIns;
    varajax2.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax2.send(valores_a_mandar);
}
var bandera = 0;

function respuestaCargarCiudadesIns() {
    if (varajax2.readyState == 4) {
        if (varajax2.status == 200) {
            raiz = varajax2.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('codMunicipio');
            n = raiz.getElementsByTagName('nomMunicipio');
            if (c.length > 0) { //  alert("a "+bandera); 
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
                    }
                }

            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione un municipio]', '', true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: cargar Municipios " + varajax2.status);
        }
    }
    if (varajax2.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarBarriosIns(codBarrio, comboBarr, codMunici, pagina) { // alert("b "+bandera);   // alert(varajax1.readyState+" "+ varajax2.readyState);

    idcombo = comboBarr;
    porDefecto = codBarrio;
    valores_a_mandar = "codMunici=" + codMunici;
    varajax3 = crearAjax();
    varajax3.open("POST", pagina, true);
    varajax3.onreadystatechange = respuestaCargarBarriosIns;
    varajax3.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax3.send(valores_a_mandar);

}

var banderaBarrios = false;

function hola() {
    alert("falta retrazar el ajax");

}

function respuestaCargarBarriosIns() { //setTimeout('hola()',50);
    if (varajax3.readyState == 4) {
        if (varajax3.status == 200) {
            raiz = varajax3.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('codBarrio');
            n = raiz.getElementsByTagName('nomBarrio');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
                    }
                }

            } else {
                if (banderaBarrios == true) {
                    alert('Tenga en cuenta que este municipio no tiene barrrios registrados.\nRegistre nuevos barrios entrando por la opcion de Ubicaciones \ndel menu de Administracion-Seguridad/Parametros generales. ');
                    banderaBarrios = false;
                }
            }
        } else {
            alert("Problemas de conexion con servidor: cargar Barrios" + varajax3.status);
        }
    }
    if (varajax3.readyState == 1) { }
}

function cargarCriterioFactor(deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestacargarCriterioFactor;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargarCriterioFactor() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');
            t = raiz.getElementsByTagName('tratam');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[Seleccione criterio]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione criterio]', '00', true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: cargar Evento " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarNoConformidades(deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarNoConformidades;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestaCargarNoConformidades() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');
            t = raiz.getElementsByTagName('tratam');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: cargar Evento " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarOtroCombo(deptoDefecto, comboHijo, codPadre, pagina) {
    idcombo = comboHijo;
    porDefecto = deptoDefecto;
    valores_a_mandar = "accion=" + comboHijo;
    valores_a_mandar = valores_a_mandar + "&codComboPadre=" + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestacargarOtroCombo;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);

}

function respuestacargarOtroCombo() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');

            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a..', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[Seleccione]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione]', '00', true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: cargar  " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

//******************************************************************************************
function cargarComboGRALCondicionDocumentosBodega(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=1&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarComboGRALOrdenesMedicaVia;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
    return true;
}

/*function cargarSubEspecialidadDesdeEspecialidad(comboOrigen, comboDestino) {
    cargarCombo2Tecnica('cmbTecnicaDos', '1', comboDestino, 111, valorAtributo(comboOrigen));
}*/

function cargarComboGRAL(cmbPadre, deptoDefecto, comboDestino, idQueryCombo) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=0";
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarComboGRAL;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarComboGRALCondicion1(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=1&condicion1=" + condicion1;

    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarComboGRAL;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    //alert(valores_a_mandar)
    varajax1.send(valores_a_mandar);
}

function cargarComboGRALCondicion2(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1, condicion2) {
    if (condicion1 != '' && condicion2 != '') {
        idcombo = comboDestino;
        porDefecto = deptoDefecto;
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=2&condicion1=" + condicion1 + "&condicion2=" + condicion2;
        varajax1 = crearAjax();
        varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
        varajax1.onreadystatechange = respuestacargarComboGRAL;
        varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajax1.send(valores_a_mandar);
    } else {
        if (comboDestino == 'cmbIdRango') {
            asignaAtributoCombo2('cmbIdRango', '', '[ Seleccione ]');
            //console.log('Ingreso a limpiar')
            //limpiaAtributo('cmbIdRango',0);
        } else if (comboDestino == 'cmbMotivoConsultaClase') {
            asignaAtributoCombo2('cmbMotivoConsultaClase', '', '[ Seleccione ]');
        }

        $("#" + comboDestino).empty();
        $("#" + comboDestino).append(new Option("[ SELECCIONE ]", "", true, true));
    }
}

function cargarComboGRALCondicion3(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1, condicion2, condicion3) {
    if (condicion1 != '' && condicion2 != '' && condicion3 != '') {
        idcombo = comboDestino;
        porDefecto = deptoDefecto;
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
        varajax1 = crearAjax();
        varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
        varajax1.onreadystatechange = respuestacargarComboGRAL;
        varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajax1.send(valores_a_mandar);
    } else {

        if (comboDestino == 'cmbIdRango') {
            asignaAtributoCombo2('cmbIdRango', '', '[ Seleccione ]');
            //console.log('Ingreso a limpiar')
            //limpiaAtributo('cmbIdRango',0);
        } else if (comboDestino == 'cmbMotivoConsultaClase') {
            asignaAtributoCombo2('cmbMotivoConsultaClase', '', '[ Seleccione ]');
        }
    }
}

function respuestacargarComboGRAL() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            //document.getElementById(idcombo).options.length = 1;
            if (document.getElementById(idcombo) != null) { document.getElementById(idcombo).options.length = 1; }
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');

            selected = false;
            $("#" + idcombo).empty();
            if (porDefecto == '') {
                $("#" + idcombo).append(new Option("[ SELECCIONE ]", "", true, true));
                selected = true;
            } else {
                $("#" + idcombo).append(new Option("[ SELECCIONE ]", ""));
            }

            if (c.length > 0) {
                if (c.length <= 1) {
                    if (selected) {
                        $("#" + idcombo).append(new Option(n[0].firstChild.data, c[0].firstChild.data));
                        document.getElementById(idcombo).options[1].title = 'DEFINICION : ' + d[0].firstChild.data;
                    } else {
                        $("#" + idcombo).append(new Option(n[0].firstChild.data, c[0].firstChild.data, true, true));
                        document.getElementById(idcombo).options[1].title = 'DEFINICION : ' + d[0].firstChild.data;
                    }
                } else {
                    for (i = 0; i < c.length; i++) {
                        if (c[i].firstChild.data == porDefecto) {
                            $("#" + idcombo).append(new Option(n[i].firstChild.data, c[i].firstChild.data, true, true));
                            document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                        } else {
                            $("#" + idcombo).append(new Option(n[i].firstChild.data, c[i].firstChild.data));
                            document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                        }
                    }
                }

            }

            /*if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, false);
                    document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                }

                if (idcombo == 'cmbRefCampo') {
                    $("#" + idcombo + " option[value='" + porDefecto + "']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
                } else {
                    document.getElementById(idcombo).options[0] = new Option('[ Seleccione ]', '', true, false);
                    $("#" + idcombo + " option[value='" + porDefecto + "']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
                }
            } else {
                document.getElementById(idcombo).options[0] = new Option('[ Seleccione ]', '', true, false);
            }*/
        } else {
            alert("Problemas de conexion con servidor: cargar Conbo General " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarLabelPlanContratacionAdmisiones(idLabelDestino, nomLabelDestino, condicion1, condicion2, condicion3, idQueryCombo) {
    idcombo = idLabelDestino;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargarLabelPlanContratacionAdmisiones(idLabelDestino, nomLabelDestino) };

    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarLabelPlanContratacionEmpresa(idLabelDestino, nomLabelDestino, condicion1, condicion2, condicion3, condicion4, idQueryCombo) {
    idcombo = idLabelDestino;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=cuatro&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3 + "&condicion4=" + condicion4;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargarLabelPlanContratacionEmpresa(idLabelDestino, nomLabelDestino) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarLabelPlanContratacionListaEspera(idLabelDestino, nomLabelDestino, condicion1, condicion2, condicion3, idQueryCombo) {
    idcombo = idLabelDestino;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargarLabelPlanContratacionListaEspera(idLabelDestino, nomLabelDestino) };

    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarLabelPlanContratacionAgenda(idLabelDestino, nomLabelDestino, condicion1, condicion2, condicion3, idQueryCombo) {
    idcombo = idLabelDestino;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargarLabelPlanContratacionAgenda(idLabelDestino, nomLabelDestino) };

    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}



function respuestacargarLabelPlanContratacionAdmisiones(idLabelDestino, nomLabelDestino) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino);
            limpiaAtributo(nomLabelDestino);
            asignaAtributo(idLabelDestino, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(nomLabelDestino, raiz.getElementsByTagName('nom')[0].firstChild.data)
            asignaAtributo('txtDiagnostico', raiz.getElementsByTagName('title')[0].firstChild.data)

            if (valorAtributo('cmbIdTipoServicio') == 'CIR') {
                buscarAGENDA('listADMISIONESCirugiaProcedimientoTraer')
            } else {
                buscarAGENDA('listAdmisionCEXProcedimientoTraer')
            }



        } else {
            alert("Problemas de conexion con servidor: respuestacargarLabelCondicion2 " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function respuestacargarLabelPlanContratacionEmpresa(idLabelDestino, nomLabelDestino) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino);
            limpiaAtributo(nomLabelDestino);
            asignaAtributo(idLabelDestino, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(nomLabelDestino, raiz.getElementsByTagName('nom')[0].firstChild.data)
            asignaAtributo('txtDiagnostico', raiz.getElementsByTagName('title')[0].firstChild.data)
            if (valorAtributo('cmbIdTipoServicio') == 'CIR') {
                buscarAGENDA('listADMISIONESCirugiaProcedimientoTraer')
            } else {
                buscarAGENDA('listAdmisionCEXProcedimientoTraer')
            }
        } else {
            alert("Problemas de conexion con servidor: respuestacargarLabelCondicion2 " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}

function respuestacargarLabelPlanContratacionListaEspera(idLabelDestino, nomLabelDestino) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino);
            limpiaAtributo(nomLabelDestino);
            asignaAtributo(idLabelDestino, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(nomLabelDestino, raiz.getElementsByTagName('nom')[0].firstChild.data)

            buscarAGENDA('listEsperaCEXProcedimientoTraer')

        } else {
            alert("Problemas de conexion con servidor: respuestacargarLabelPlanContratacionListaEspera " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}


function respuestacargarLabelPlanContratacionAgenda(idLabelDestino, nomLabelDestino) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino);
            limpiaAtributo(nomLabelDestino);
            asignaAtributo(idLabelDestino, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(nomLabelDestino, raiz.getElementsByTagName('nom')[0].firstChild.data)


            if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
                setTimeout("buscarAGENDA('listProcedimientosAgendaCirugia')", 800);

            } else {
                buscarAGENDA('listProcedimientosAgenda')
                setTimeout("buscarAGENDA('listCitaCexProcedimientoTraer')", 800)
            }

        } else {
            alert("Problemas de conexion con servidor: respuestacargarLabelPlanContratacionAgenda " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) { }
}




function cargarLabelCondicion2(idLabelDestino, nomLabelDestino, condicion1, condicion2, idQueryCombo) {
    idcombo = idLabelDestino;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=2&condicion1=" + condicion1 + "&condicion2=" + condicion2;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargarLabelCondicion2(idLabelDestino, nomLabelDestino) };

    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}


function respuestacargarLabelCondicion2(idLabelDestino, nomLabelDestino) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino);
            limpiaAtributo(nomLabelDestino);
            asignaAtributo(idLabelDestino, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(nomLabelDestino, raiz.getElementsByTagName('nom')[0].firstChild.data)

        } else {
            alert("Problemas de conexion con servidor: respuestacargarLabelCondicion2 " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}


function cargarElementoCondicionDosCodigoBarras(elementoDestino, idQueryCombo, condicion1, condicion2) {
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=dos&condicion1=" + condicion1 + "&condicion2=" + condicion2;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarElementoGRALCodigoBarras;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargarElementoGRALCodigoBarras() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');


            if (c.length > 0) {
                asignaAtributo(idcombo, c[0].firstChild.data)
                //

                if ($('#txtIdBarCode').length) {
                    verificarNotificacionAntesDeGuardar('existenciaBodegaOptica');
                    // si existe
                } else {
                    // no existe
                    codigoBarras();
                }


            } else {

                alert('CODIGO NO REGISTRADO !!')
                //cargarMenu('<%= response.encodeURL("suministros/articulo/codigoBarras.jsp")%>','9-4-0-22','codigoBarras','codigoBarras','s','s','s','s','s','s');
                /*if ($('#txtCodBarrasBus').length) {		  
                    limpiaAtributo('txtCodBarrasBus',0);

                }*/
                limpiaAtributo('txtIdBarCode', 0);
                limpiaAtributo('txtCodBarrasBus', 0);
                asignaAtributo('txtCodBarrasBus', '', 0)


            }
        } else {
            //alert("Problemas de conexion con servidor: respuestacargarElementoGRALCodigoBarras "+varajax1.status);
            document.getElementById('txtCodBarrasBus').focus()
        }
    }


}



function cargarElementoCondicionCodigoBarrasVentas(elementoDestino, idQueryCombo, condicion1, condicion2, condicion3) {
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarCodigoBarrasVentas;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}






function respuestacargarCodigoBarrasVentas() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');


            if (c.length > 0) {
                asignaAtributo(idcombo, c[0].firstChild.data)
                console.log(c[0].firstChild.data)

                if ($('#txtIdCodigoBarrasVentas').length) {
                    verificarNotificacionAntesDeGuardar('existenciaBodegaVentas');
                    // si existe
                } else {
                    // no existe
                    codigoBarrasVentas();
                }

            } else {
                alert('CODIGO NO REGISTRADO !!')
                limpiaAtributo('txtIdCodigoBarrasVentas', 0);
                limpiaAtributo('lblDatosCodigoBarraVentas', 0);


            }
        } else {
            document.getElementById('txtIdCodigoBarrasVentas').focus()
        }
    }


}






function cargarElementCondicionDosCodigoBarras(elementoDestino, idQueryCombo, condicion1, condicion2) {
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=dos&condicion1=" + condicion1 + "&condicion2=" + condicion2;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarElementGRALCodigoBarras;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargarElementGRALCodigoBarras() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');
            if (c.length > 0) {
                asignaAtributo(idcombo, c[0].firstChild.data)
                respuestaVSerial();
            } else {

                alert('CODIGO NO REGISTRADO !!')

            }
        } else {
            //alert("Problemas de conexion con servidor: respuestacargarElementoGRALCodigoBarras "+varajax1.status);
            document.getElementById('txtCodBarrasBus').focus()
        }
    }
}

function cargarElementCondicionDosCodBarras(elementoDestino, idQueryCombo, condicion1, condicion2) {
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=dos&condicion1=" + condicion1 + "&condicion2=" + condicion2;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarElementGRALCodBarras;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargarElementGRALCodBarras() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');
            if (c.length > 0) {
                asignaAtributo(idcombo, c[0].firstChild.data)
                respuestaVSerialMod(); //
            } else {
                alert('CODIGO NO REGISTRADO !!')
            }
        } else {
            //alert("Problemas de conexion con servidor: respuestacargarElementoGRALCodigoBarras "+varajax1.status);
            document.getElementById('txtCodBarrasBus').focus()
        }
    }


}
//

function cargarElementoCondicionUno(elementoDestino, idQueryCombo, condicion1) {
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarElementoGRAL;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarElementoCondicionDos(elementoDestino, idQueryCombo, condicion1, condicion2) {
    if (condicion1 != '' && condicion2 != '') {
        idcombo = elementoDestino;
        porDefecto = '';
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=dos&condicion1=" + condicion1 + "&condicion2=" + condicion2;
        varajax1 = crearAjax();
        varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
        varajax1.onreadystatechange = respuestacargarElementoGRAL;
        varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajax1.send(valores_a_mandar);
    }
}


function cargarElementoCondicionTres(elementoDestino, idQueryCombo, condicion1, condicion2, condicion3) {
    if (condicion1 != '' && condicion2 != '' && condicion3 != '') {
        idcombo = elementoDestino;
        porDefecto = '';
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
        varajax1 = crearAjax();
        varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
        varajax1.onreadystatechange = respuestacargarElementoGRAL;
        varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajax1.send(valores_a_mandar);
    }
}


function respuestacargarElementoGRAL() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            //document.getElementById(idcombo).options.length=1;	
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');

            // alert(c.length);
            if (c.length > 0) {
                asignaAtributo(idcombo, c[0].firstChild.data, 0)

            } else {
                alert('Problema al cargar respuestacargarElementoGRAL..')
            }
        } else {
            alert("Problemas de conexion con servidor: respuestacargarElementoGRAL. " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}


function cargarComboGRALCondicionOrdenesMedicaVia(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=1&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarComboGRALOrdenesMedicaVia;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
    return true;
}

function respuestacargarComboGRALOrdenesMedicaVia() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');

            // alert(c.length);
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, false);
                }
            }
            if (c.length > 1) {
                document.getElementById(idcombo).options[0] = new Option('', '', true, false);
                $("#" + idcombo + " option[value='']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
            }
            // cargarComboGRALCondicionOrdenesMedicaDosis('txtIdArticulo', 'xxx', 'cmbIdTipoDosis', 51, valorAtributoIdAutoCompletar('txtIdArticulo')) 

        } else alert("Problemas de conexion con servidor: cargar Conbo General " + varajax1.status);
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarComboGRALCondicionOrdenesMedicaDosis(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=1&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarComboGRALOrdenesMedicaDosis;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
    return true;
}

function respuestacargarComboGRALOrdenesMedicaDosis() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');


            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, false);
                }
                $("#" + idcombo + " option[value='" + porDefecto + "']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
            }
            if (c.length > 1) {
                document.getElementById(idcombo).options[0] = new Option('', '', true, false);
                $("#" + idcombo + " option[value='']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
            }

        } else {
            alert("Problemas de conexion con servidor: cargar Conbo General " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}




//******************************************************************************************


function cargarEAHospitalConClasePadre(cmbClaseEA, deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + $('#drag' + ventanaActual.num).find('#' + cmbClaseEA).val() + '&idProceso=' + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarEAHospitalConClase;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarEAHospitalConClase(cmbProceso, deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + codPadre + '&idProceso=' + $('#drag' + ventanaActual.num).find('#' + cmbProceso).val();
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarEAHospitalConClase;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarEAHospitalConClaseEAeIncidentes(cmbProceso, deptoDefecto, comboDep, codPadre, pagina) { //LAMADO DESDE buscarJuan ->administrarEventoAdverso
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=00" + '&idProceso=00';
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarEAHospitalConClase;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestaCargarEAHospitalConClase() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');
            t = raiz.getElementsByTagName('tratam');
            // alert(c.length);
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: cargar Evento " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}


function cargarAspectosAMejorar(deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarAspectosAMejorar;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestaCargarAspectosAMejorar() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');
            t = raiz.getElementsByTagName('tratam');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'INFORMACION : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: cargar Evento " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}
/*en reportar eventoAdverso, partiendo de que se escoja la unidad de servicio donde se encuentre el paciente*/
function cargarPaciente(deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarPaciente;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestaCargarPaciente() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');
            t = raiz.getElementsByTagName('tratam');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda PACIENTE que pueda...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda PACIENTE que pueda...', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'ANOTACI�N : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[SELECCIONE PACIENTE]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[SELECCIONE PACIENTE]', '00', true, false);
            }
        } else {
            alert("Problemas de conexion con servidor: cargar Pacientes " + varajax1.status);
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}






///funiconesd adciones Angelica

function verificarfechaturno(valor, divdatos, id, numid, pagina) {
    // alert(valor+" "+id+" "+numid+" "+pagina);
    if (document.getElementById(valor).value != "") {
        tid = document.getElementById(id).innerHTML;
        nid = document.getElementById(numid).innerHTML;
        dia = document.getElementById(valor).value;
        $('#drag' + ventanaActual.num).find('#divContenido').css('height', '350px');
        mostrar(divdatos);
        valores_a_mandar = "id=" + tid + "&nid=" + nid + "&diat=" + dia;
        varajax = crearAjax();
        varajax.open("POST", pagina, true);
        varajax.onreadystatechange = respuestaVerificarfechaturno;
        varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        varajax.send(valores_a_mandar);
    }
}



function limpiarCampos(divdatos, fechaturno) {
    ocultar(divdatos);
    $('#drag' + ventanaActual.num).find('#divContenido').css('height', '200px');
    document.getElementById(fechaturno).value = "";
}


function calcularDuracionCita(txtCantidadP, txtDuracionC, horafin, horainicio, minfin, mininicio, ampminicio, ampmfin) {
    ban = 0;
    //alert("hola"+document.getElementById(horainicio).value+"  "+document.getElementById(horafin).value);		 
    if (document.getElementById(ampminicio).value == 'A.M' && document.getElementById(ampmfin).value == 'A.M') {
        if (document.getElementById(horainicio).value < document.getElementById(horafin).value) {
            horas = document.getElementById(horafin).value - document.getElementById(horainicio).value;
            ban = 1;
        }
    } else if (document.getElementById(ampminicio).value == 'P.M' && document.getElementById(ampmfin).value == 'P.M') {
        if (document.getElementById(horainicio).value < document.getElementById(horafin).value) {
            horas = document.getElementById(horafin).value - document.getElementById(horainicio).value;
            ban = 1;
        }
    } else if (document.getElementById(ampminicio).value == 'A.M' && document.getElementById(ampmfin).value == 'P.M') {
        horas1 = 12 - parseInt(document.getElementById(horainicio).value);
        horas = parseInt(horas1) + parseInt(document.getElementById(horafin).value);
        ban = 1;
    }
    if (ban == 1) {
        minutos = parseInt(document.getElementById(minfin).value) + parseInt(document.getElementById(mininicio).value);
        totalminutos = (parseInt(horas) * 60) + parseInt(minutos);
        duracioncita = parseInt(totalminutos) / parseInt(document.getElementById(txtCantidadP).value);
        if (duracioncita < 0) {
            duracioncita = duracioncita * (-1);
        }
        document.getElementById(txtDuracionC).value = duracioncita;
        vec = document.getElementById(txtDuracionC).value.split(".");
        document.getElementById(txtDuracionC).value = vec[0];
    }
}

function calcularCantidadPacientes(txtCantidadP, txtDuracionC, horafin, horainicio, minfin, mininicio, ampminicio, ampmfin) {
    ban1 = 0;
    if (document.getElementById(ampminicio).value == 'A.M' && document.getElementById(ampmfin).value == 'A.M') {
        if (document.getElementById(horainicio).value < document.getElementById(horafin).value) {
            horas = document.getElementById(horafin).value - document.getElementById(horainicio).value;
            ban1 = 1;
        }
    } else if (document.getElementById(ampminicio).value == 'P.M' && document.getElementById(ampmfin).value == 'P.M') {
        if (document.getElementById(horainicio).value < document.getElementById(horafin).value) {
            horas = document.getElementById(horafin).value - document.getElementById(horainicio).value;
            ban1 = 1;
        }
    } else if (document.getElementById(ampminicio).value == 'A.M' && document.getElementById(ampmfin).value == 'P.M') {
        horas1 = 12 - parseInt(document.getElementById(horainicio).value);
        horas = parseInt(horas1) + parseInt(document.getElementById(horafin).value);
        ban1 = 1;
    }
    if (ban1 == 1) {
        minutos = parseInt(document.getElementById(minfin).value) + parseInt(document.getElementById(mininicio).value);
        totalminutos = (parseInt(horas) * 60) + parseInt(minutos);
        duracioncita = parseInt(totalminutos) / parseInt(document.getElementById(txtDuracionC).value);
        if (duracioncita < 0) {
            duracioncita = duracioncita * (-1);
        }
        document.getElementById(txtCantidadP).value = duracioncita;
        vec = document.getElementById(txtCantidadP).value.split(".");
        document.getElementById(txtCantidadP).value = vec[0];
    }
}


function LlenarServicios(tipoid, identificacion, pagina) {
    valores_a_mandar = "tipoid=" + tipoid + "&identificacion=" + identificacion;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaLlenarServicios;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}


function respuestaLlenarServicios() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            codigos = raiz.getElementsByTagName('codigos');
            nombres = raiz.getElementsByTagName('nombres');
            if (codigos.length > 0) {
                for (i = 0; i < codigos.length; i++) {
                    document.getElementById('cmbTipoServ1').options[(document.getElementById('cmbTipoServ1').options.length)] = new Option(nombres[i].firstChild.data, codigos[i].firstChild.data, true, false);
                    document.getElementById('cmbTipoServ2').options[(document.getElementById('cmbTipoServ2').options.length)] = new Option(nombres[i].firstChild.data, codigos[i].firstChild.data, true, false);
                    document.getElementById('cmbTipoServ3').options[(document.getElementById('cmbTipoServ3').options.length)] = new Option(nombres[i].firstChild.data, codigos[i].firstChild.data, true, false);
                    document.getElementById('cmbTipoServ4').options[(document.getElementById('cmbTipoServ4').options.length)] = new Option(nombres[i].firstChild.data, codigos[i].firstChild.data, true, false);
                    document.getElementById('cmbTipoServ5').options[(document.getElementById('cmbTipoServ5').options.length)] = new Option(nombres[i].firstChild.data, codigos[i].firstChild.data, true, false);
                }
            } else {
                //document.getElementById(idcombo).options[0]=new Option('[Seleccione un municipio]','',true,false);
            }
        } else {
            alert("Problemas de conexion con servidor: " + varajax.status);
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}


/* Funciones de citologia para las opciones de los botones */
function verificaropcion(boton) {
    if (boton == 'chediu' || boton == 'cheano') {
        document.getElementById('txtCualp').disabled = true;
        document.getElementById('txtCualp').value = "";
    }
    if (boton == 'cheotro') {
        document.getElementById('txtCualp').disabled = false;
    }
}

function verificaropcion1(boton) {
    if (boton == 'checkHiste' || boton == 'checkRadioT' || boton == 'checkTto') {
        document.getElementById('txtCualPro').disabled = true;
    }
    if (boton == 'checkotro') {
        document.getElementById('txtCualPro').disabled = false;
    }
    if (document.getElementById('checkotro').checked == false) {
        document.getElementById('txtCualPro').disabled = true;
        document.getElementById('txtCualPro').value = "";
    }
}
/*
function verificarbotonesr(boton){
	if(boton=='cheSatisfactorio'){
	 document.getElementById('chePresentes').checked=true; 	
     document.getElementById('txtMuestraPro').disabled=true;  
	 document.getElementById('txtMuestraRech').disabled=true;
	 document.getElementById('txtMuestraPro').value="";  
	 document.getElementById('txtMuestraRech').value="";
	}
	if(boton=='checkNegativa'){
	  document.getElementById('chePresentes').checked=false;
	  document.getElementById('chesinErupcionars').checked=false;
	  document.getElementById('txtMuestraPro').disabled=false;  
	  document.getElementById('txtMuestraRech').disabled=false;
	}
	if(boton=='chesinErupcionars' || boton=='chePresentes'){
		if(document.getElementById('cheSatisfactorio').checked==false){
		   document.getElementById('cheSatisfactorio').checked=true;
		}		
	}	
  }*/

function verificarbotonescamb(boton) {
    if (boton == 'checkCambiosCelulares') {
        if (document.getElementById('checkCambiosCelulares').checked == true) {
            document.getElementById('checkInflamacion').checked = true;
            document.getElementById('cheL').checked = true;
        }
        if (document.getElementById('checkCambiosCelulares').checked == false) {
            document.getElementById('checkInflamacion').checked = false;
            document.getElementById('checkRadiacion').checked = false;
            document.getElementById('checkContracepcion').checked = false;
            document.getElementById('cheL').checked = false;
            document.getElementById('cheM').checked = false;
            document.getElementById('cheS').checked = false;
        }
    }
    if (boton == 'cheL' || boton == 'cheM' || boton == 'cheS') {
        if (document.getElementById('checkInflamacion').checked == false) {
            document.getElementById('checkInflamacion').checked = true;
        }
        if (document.getElementById('checkCambiosCelulares').checked == false) {
            document.getElementById('checkCambiosCelulares').checked = true;
        }

    }
    if (boton == 'checkInflamacion') {
        if (document.getElementById('checkInflamacion').checked == false) {
            if (document.getElementById('cheL').checked == true || document.getElementById('cheM').checked == true || document.getElementById('cheS').checked == true) {
                document.getElementById('cheL').checked = false;
                document.getElementById('cheM').checked = false;
                document.getElementById('cheS').checked = false;
            }
        }
        if (document.getElementById('checkInflamacion').checked == true) {
            if (document.getElementById('cheL').checked == false && document.getElementById('cheM').checked == false && document.getElementById('cheS').checked == false) {
                document.getElementById('cheL').checked = true;
            }
        }
    }

    if (boton == 'checkInflamacion' || boton == 'checkRadiacion' || boton == 'checkContracepcion') {
        if (document.getElementById('checkCambiosCelulares').checked == false) {
            document.getElementById('checkCambiosCelulares').checked = true;
        }
    }
    if (document.getElementById('checkInflamacion').checked == false && document.getElementById('checkRadiacion').checked == false && document.getElementById('checkContracepcion').checked == false) {
        document.getElementById('checkCambiosCelulares').checked = false;
    }

}



function getValorRadio(name) {
    if (document.getElementsByName(name) != null) {
        var inputs = document.getElementsByName(name);
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked == true) return inputs[i].value;
        }
    }
    return null;
}

/// --------------------------para cambiar usuarios del sistema con todo y roles juan

function verificarContrasenaAxiliar(param1) {

    varajaxReporte = crearAjax();
    valores_a_mandar = "accion=verificarContrasena";
    valores_a_mandar = valores_a_mandar + "&contrasena=" + param1;
    varajaxReporte.open("POST", '/clinica/paginas/accionesXml/verificarInformacionAuxiliar_xml.jsp', true);
    varajaxReporte.onreadystatechange = llenarverificarContrasenaAxiliar;
    varajaxReporte.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxReporte.send(valores_a_mandar);
}
var _verificacionInformacionAx2;

function llenarverificarContrasenaAxiliar() {
    if (varajaxReporte.readyState == 4) {
        //	VentanaModal.cerrar();
        if (varajaxReporte.status == 200) {
            raiz = varajaxReporte.responseXML.documentElement;
            if (raiz.getElementsByTagName('respuesta')[0].firstChild.data != '') {

                // alert('IDENTIFICACION DEL AUXILIAR = '+raiz.getElementsByTagName('respuesta')[0].firstChild.data)	
                asignaAtributo('lblIdAuxiliar', raiz.getElementsByTagName('respuesta')[0].firstChild.data)
                ocultar('divAuxiliar')


            } else {
                alert("NO EXISTE USUARIO PARA ESTA CONTRASE�A");

                document.getElementById('txtContrasenaAdm').focus()
                asignaAtributo('txtContrasenaAdm', '', 0)
            }
        } else {
            alert("Problemas de conexion con servidor: " + varajaxReporte.status);
        }
    }
    if (varajaxReporte.readyState == 1) {
        //document.getElementById('listado').innerHTML =	crearMensaje();
        //	 abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}
/// --------------------------para cambiar usuarios del sistema con todo y roles juan

function verificarContrasenaAx(param1, param2, pagina) {

    varajaxReporte = crearAjax();
    valores_a_mandar = "accion=verificarContrasena";
    valores_a_mandar = valores_a_mandar + "&login=" + param1;
    valores_a_mandar = valores_a_mandar + "&contrasena=" + param2;
    varajaxReporte.open("POST", '/clinica/paginas/accionesXml/verificarInformacion_xml.jsp', true);
    varajaxReporte.onreadystatechange = llenarinfoVerificaContrasenaAx;
    varajaxReporte.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxReporte.send(valores_a_mandar);
}
var _verificacionInformacionAx;

function llenarinfoVerificaContrasenaAx() {
    //
    _verificacionInformacionAx = false;
    //
    if (varajaxReporte.readyState == 4) {
        //	VentanaModal.cerrar();
        if (varajaxReporte.status == 200) {
            raiz = varajaxReporte.responseXML.documentElement;
            if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
                switch (paginaActual) {
                    case 'usuarios':
                        //alert("verificacion correcta contrase�a ");
                        // se sobreentiende
                        break;
                    default:
                        //alert("verificacion correcta ");
                        break;
                }
                _verificacionInformacionAx = true;

            } else {
                alert(" Su contrase�a es erronea .");
                _verificacionInformacionAx = false;
            }
            //cargarFocos();
            //if(focos.length>0)
            //document.getElementById(focos[1]).focus();

            if (_verificacionInformacionAx) {
                //ocultar('divcontrasena');
                //mostrar('contenido');
                $('#drag' + ventanaActual.num + ' #divBuscarUsua').hide();
                $('#drag' + ventanaActual.num + ' #divEditarUsua').show();
                $('#drag' + ventanaActual.num + ' #divContenidoUsua').css('height', '200px');
                //$('txtId').focus();
            } else {
                //alert('Su contrase�a es incorrecta');
                $('txtContrasenaAdm').value = '';
                $('txtContrasenaAdm').focus();
            }

        } else {
            alert("Problemas de conexion con servidor: " + varajaxReporte.status);
        }
    }
    if (varajaxReporte.readyState == 1) {
        //document.getElementById('listado').innerHTML =	crearMensaje();
        //	 abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}

function consultarNombrePersona(codPersona, tipoid, pagina) {
    codPersona = valorAtributo('txtId');
    if (codPersona != '' && tipoid != '00') {
        varajaxReporte = crearAjax();
        valores_a_mandar = "id=" + codPersona;
        valores_a_mandar = valores_a_mandar + "&tipoId=" + tipoid;
        varajaxReporte.open("POST", pagina, true);
        varajaxReporte.onreadystatechange = llenarinfoNombrePersona;
        varajaxReporte.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxReporte.send(valores_a_mandar);
    }
}


/****** Ajax para leer nombre persona con identificacion *****/
function llenarinfoNombrePersona() {
    //
    var esLaPersona; //
    if (varajaxReporte.readyState == 4) {
        //	VentanaModal.cerrar();
        if (varajaxReporte.status == 200) {
            raiz = varajaxReporte.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nombres');


            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (i == 1) {
                        esLaPersona = " Parece existir mas de una persona ";
                        document.getElementById('txtId').focus(); //por siaca
                    } else {
                        esLaPersona = raiz.getElementsByTagName('nombres')[i].firstChild.data;
                        document.getElementById('txtLogin').focus();
                        $('#drag' + ventanaActual.num).find('#txtLogin').val(raiz.getElementsByTagName('login')[i].firstChild.data);
                        $('#drag' + ventanaActual.num).find('#txtContrasena').val(raiz.getElementsByTagName('contrasena')[i].firstChild.data);
                        $('#drag' + ventanaActual.num).find('#txtContrasena2').val(raiz.getElementsByTagName('contrasena')[i].firstChild.data);
                        $("#cmbRol option[value='" + $.trim(raiz.getElementsByTagName('rol')[i].firstChild.data) + "']").attr('selected', 'selected');
                        $("#cmbEstado option[value='" + $.trim(raiz.getElementsByTagName('estado')[i].firstChild.data) + "']").attr('selected', 'selected');

                    }
                }
            } else {
                esLaPersona = '[No existe ninguna persona con esa identificacion y tipo de id]';
                document.getElementById('txtId').focus();
            }

            $('#drag' + ventanaActual.num).find('#txtLogin').html(esLaPersona);
            $('#drag' + ventanaActual.num).find('#txtnomPersona').html(esLaPersona);

            //  document.getElementById('nomPersona').innerHTML = esLaPersona;
            //cargarFocos();
            //actualizarMensajeReporte();
            //if(focos.length>0)
            //document.getElementById(focos[1]).focus();
        } else {
            alert("Problemas de conexion con servidor: " + varajaxReporte.status);
        }
    }
    if (varajaxReporte.readyState == 1) {
        //document.getElementById('listado').innerHTML =	crearMensaje();
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}



function cargarUsuario(tipoId, id) {
    valores_a_mandar = "accion=consultaUsuario";
    valores_a_mandar = valores_a_mandar + "&tipoId=" + tipoId;
    valores_a_mandar = valores_a_mandar + "&id=" + id;
    varajax = crearAjax();
    varajax.open("POST", '/clinica/paginas/accionesXml/cargarPersonas_xml.jsp', true);
    varajax.onreadystatechange = respuestaCargarUsuario;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}


function respuestaCargarUsuario() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;

            $('#drag' + ventanaActual.num).find('#lblNomPersona').empty();
            $('#drag' + ventanaActual.num).find('#txtLogin').val('');
            $('#drag' + ventanaActual.num).find('#hddId').val('');
            $('#drag' + ventanaActual.num).find('#hddTipoIdUsua').val('');
            $('#drag' + ventanaActual.num).find('#txtContrasena').val('');
            $('#drag' + ventanaActual.num).find('#txtContrasena2').val('');
            $("#cmbRol option[value='']").attr('selected', 'selected');

            tipoId = raiz.getElementsByTagName('tipoId')[0].firstChild.data;
            id = raiz.getElementsByTagName('id')[0].firstChild.data;
            nombres = raiz.getElementsByTagName('nombres')[0].firstChild.data;
            login = raiz.getElementsByTagName('login')[0].firstChild.data;
            contrasena = raiz.getElementsByTagName('contrasena')[0].firstChild.data;
            rol = $.trim(raiz.getElementsByTagName('rol')[0].firstChild.data);
            estadoCuenta = $.trim(raiz.getElementsByTagName('estado')[0].firstChild.data);
            cuantos = raiz.getElementsByTagName('cuantos')[0].firstChild.data;

            if (cuantos == 1) { //si esta en hoja de vida y tiene cuenta de usuario
                $('#drag' + ventanaActual.num).find('#hddId').val(tipoId);
                $('#drag' + ventanaActual.num).find('#hddTipoIdUsua').val(id);
                $('#drag' + ventanaActual.num).find('#lblNomPersona').append(nombres);
                $('#drag' + ventanaActual.num).find('#txtLogin').val(login);
                $('#drag' + ventanaActual.num).find('#txtContrasena').val(contrasena);
                $('#drag' + ventanaActual.num).find('#txtContrasena2').val(contrasena);

                $("#cmbRol option[value='" + rol + "']").attr('selected', 'selected');
                $("#cmbEstado option[value='" + estadoCuenta + "']").attr('selected', 'selected');
            } else if (cuantos == 2) { //si esta en hoja de vida pero NO tiene cuenta de usuario	   
                $('#drag' + ventanaActual.num).find('#hddId').val(tipoId);
                $('#drag' + ventanaActual.num).find('#hddTipoIdUsua').val(id);
                $('#drag' + ventanaActual.num).find('#lblNomPersona').empty();
                $('#drag' + ventanaActual.num).find('#txtLogin').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena2').val('');
                $("#cmbRol option[value='']").attr('selected', 'selected');
                $("#cmbEstado option[value='1']").attr('selected', 'selected');

                $('#drag' + ventanaActual.num).find('#lblNomPersona').append(nombres);
                alert(nombres + " No posee cuenta en este Sistama");
            } else if (cuantos == 0) {
                $('#drag' + ventanaActual.num).find('#lblNomPersona').empty();
                $('#drag' + ventanaActual.num).find('#txtLogin').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena2').val('');
                $("#cmbRol option[value='']").attr('selected', 'selected');
                $("#cmbEstado option[value='1']").attr('selected', 'selected');

                alert("Empleado no se encuentra registrado en las hojas de vida de la Instituci�n, o est� Inactivo");
            }
        } else {
            alert("Problemas de conexion con servidor: " + varajax.status);
        }
    }
    if (varajax.readyState == 1) { }
}




function sacarElMaximo(arg) {
    var ids = jQuery("#" + arg).getDataIDs();
    cant = ids.length;
    var maxim = 0;

    for (var i = 0; i < ids.length; i++) {
        var datosDelRegistro = jQuery("#" + arg).getRowData(ids[i]);


        if (datosDelRegistro.idRegistro > maxim)
            maxim = datosDelRegistro.idRegistro;
        else maxim = maxim;


    }
    maximo = parseInt(maxim);
    maximo = (maximo + 1);
    return maximo;
}


function reglaRutasCirugia() {
    var ids = jQuery("#listCitaCirugiaProcedimiento").getDataIDs();
    var idEps = valorAtributo('lblIdEpsPlan');


    if (ids.length == 1) { /*SI SOLO ES UN PROCEDIMIENTO*/
        var c = ids[0];
        var datosRow = jQuery("#listCitaCirugiaProcedimiento").getRowData(c);

        if (datosRow.Contratado == 'SI') {
            if (datosRow.lateralidad == 'U') {

                alert('A. PARA UN SOLO PROCEDIMIENTO UNILATERAL CONTRATADO \n DEBE FACTURARSE DIRECTAMENTE');
                return false;

            }
            if (datosRow.lateralidad == 'B') {
                if (datosRow.Contratado == 'SI') {

                    if (idEps == '10' || idEps == '26' || idEps == '27' || idEps == '29' || idEps == '39' || idEps == '13' || idEps == '50' || idEps == '57' || idEps == '15' || idEps == '3' || idEps == '5') {
                        alert('B. PARA UN SOLO PROCEDIMIENTO BILATERAL CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }

                }
            }
        } else {
            if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                alert('A1. PARA UN SOLO PROCEDIMIENTO UNILATERAL O BILATERAL NO CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                return false;
            }
        }
    } else { /*DOS O MAS PROCEDIMIENTO*/
        contUnilater = 0;
        contBilater = 0;
        todosContratados = 0;
        validaEpsIdProcedimiento = 0;
        for (var i = 0; i < ids.length; i++) {
            var c = ids[i];
            var datosRow = jQuery("#listCitaCirugiaProcedimiento").getRowData(c);
            if (datosRow.lateralidad == 'U') {
                contUnilater = contUnilater + 1;
            }
            if (datosRow.lateralidad == 'B') {
                contBilater = contBilater + 1;
            }
            if (datosRow.Contratado == 'SI') {
                todosContratados = todosContratados + 1;
            }

        }


        for (var i = 0; i < ids.length; i++) { /*UNA DETERMINADA EPS CON UNOS PROCEDIMIENTOS*/
            var c = ids[i];
            var datosRow = jQuery("#listCitaCirugiaProcedimiento").getRowData(c);

            if (datosRow.idProcedimiento == '147101' || datosRow.idProcedimiento == '147404' || datosRow.idProcedimiento == '147401' || datosRow.idProcedimiento == '147402' || datosRow.idProcedimiento == '147403') {
                if (idEps == '5')
                    validaEpsIdProcedimiento = 1;
                //  alert('algun procedimiento es de estos')
            }
        }

        if (validaEpsIdProcedimiento == 0) {

            alert('todosContratados=' + todosContratados + ' contBilater=' + contBilater + ' contUnilater=' + contUnilater + ' lengt=' + ids.length);

            if (todosContratados == ids.length) { /*TODOS SON CONTRATADOS*/
                if (contUnilater == ids.length) {
                    if (idEps == '10' || idEps == '26' || idEps == '27' || idEps == '29' || idEps == '39' || idEps == '13' || idEps == '50' || idEps == '57' || idEps == '15' || idEps == '3' || idEps == '5') {
                        alert('C. EXISTE MAS DE UN PROCEDIMIENTO CON DIFERENTE VIA CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contBilater == ids.length) {
                    if (idEps == '10' || idEps == '26' || idEps == '27' || idEps == '29' || idEps == '39' || idEps == '13' || idEps == '50' || idEps == '57' || idEps == '15' || idEps == '3' || idEps == '5') {
                        alert('D. EXISTE MAS DE UN PROCEDIMIENTO BILATERALES Y CONTRATADOS PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contUnilater < ids.length && contUnilater > 0) {
                    if (idEps == '10' || idEps == '26' || idEps == '27' || idEps == '29' || idEps == '39' || idEps == '13' || idEps == '50' || idEps == '57' || idEps == '15' || idEps == '3' || idEps == '5') {
                        alert('E. EXISTE MAS DE UN PROCEDIMIENTO UNILATERAL O BILATERALES CONTRATADOS PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\n DEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
            } else if (todosContratados == 0) { /*TODOS SON NO CONTRATADOS*/
                alert('TODOS SON NO CONTRATADOS....')
                if (contUnilater == ids.length) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('C1. EXISTE MAS DE UN PROCEDIMIENTO CON DIFERENTE VIA CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contBilater == ids.length) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('D1. EXISTE MAS DE UN PROCEDIMIENTO BILATERALES Y CONTRATADOS PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contUnilater < ids.length && contUnilater > 0) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('E1. EXISTE MAS DE UN PROCEDIMIENTO UNILATERAL O BILATERALES CONTRATADOS PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\n DEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }

            } else { /*CONTRATADOS Y NO CONTRATADOS*/
                alert('MIX')

                if (contUnilater == ids.length) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('C1.1 EXISTE MAS DE UN PROCEDIMIENTO CON DIFERENTE VIA CONTRATADO Y NO CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contBilater == ids.length) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('D1.1 EXISTE MAS DE UN PROCEDIMIENTO BILATERALES Y CONTRATADOS Y NO CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contUnilater < ids.length && contUnilater > 0) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('E1.1 EXISTE MAS DE UN PROCEDIMIENTO UNILATERAL O BILATERALES CONTRATADOS Y NO CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\n DEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
            }

        }

    }
    return true;
}

function verificarCamposGuardar(arg) {
    switch (arg) {
        case 'guardarCitasProgramadas':
            if (valorAtributo('txtMunicipioOrdenes') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionResOrdenes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtTelefonosOrdenes') == '') { alert('FALTA TELEFONO'); return false; }
            if (valorAtributo('txtCelular1Ordenes') == '') { alert('FALTA CELULAR1'); return false; }
            
            if (valorAtributo('txtAdministradora1Ordenes') == '') { alert('FALTA ADMINISTRADORA AGENDA'); return false; }
            if (valorAtributo('cmbIdTipoRegimenOrdenes') == '') { alert('FALTA TIPO DE REGIMEN'); return false; }
            if (valorAtributo('cmbIdPlanOrdenes') == '') { alert('FALTA PLAN'); return false; }

            //if (valorAtributo('txtFechaPacienteCita') == '') { alert('FALTA FECHA PACIENTE'); return false; }
            //if (Date.parse(valorAtributo('lblFechaCita')) < Date.parse(valorAtributo('txtFechaPacienteCita'))) { alert('FECHA DE PACIENTE NO ES VALIDA. DEBE SER MAYOR O IGUAL A LA FECHA DE LA CITA'); return false; }
            if (valorAtributo('txtIPSRemiteOrdenes') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }
            if (valorAtributo('cmbEstadoCitaTerapias') == '') { alert('FALTA ESTADO DE LA(S) CITA(S)'); return false; }
            if (valorAtributo('cmbMotivoConsultaTerapias') == '') { alert('FALTA VIA DE COMUNICACION'); return false; }
            if (valorAtributo('cmbDuracionMinutosOrdenes') == '') { alert('FALTA LA DURACION DE CITA(S)'); return false; }
            if (valorAtributo('cmbConsultorio') == '') { alert('FALTA SELECCIONAR EL CONSULTORIO'); return false; }
            break;

        case 'crearCitasTerapiasAgenda':
            terapia_seleccionada = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow');
            var count = 0;

            var ids = $("#listDias").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listDias_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    count += 1; 
                }
            }
            
            if (terapia_seleccionada === null) { alert("DEBE SELECCIONAR UNA ORDEN") ; return false; }
            if (valorAtributo('cmbTipoCitaTerapia') == '') { alert("DEBE SELECCIONAR EL TIPO DE CITA") ; return false; }
            //if (valorAtributo('cmbEstadoCitaTerapias') == '') { alert("FALTA SELECCIONAR EL ESTADO DEL CITAS") ; return false; }
            //if (valorAtributo('cmbMotivoConsultaTerapias') == '') { alert("FALTA SELECCIONAR VIA DE COMUNICACION") ; return false; }
            if (valorAtributo('txtHoraCitas') == '') { alert("NO SE HA ESPECIFICADO LA HORA DE LAS CITAS") ; return false; }
            //if (valorAtributo('cmbDuracionMinutosOrdenes') == '') { alert("FALTA SELECCIONAR DURACI&Oacute;N") ; return false; }
            if($('#chkFrecuencia').attr('checked')){
                if (valorAtributo('cmbCantidadCitasTerapia') == '') { alert("FALTA SELECCIONAR LA CANTIDAD DE LAS CITAS") ; return false; }
                if (valorAtributo('cmbFrecuenciaTerapia') == '') { alert("FALTA SELECCIONAR LA FRECUENCIA DE LAS CITAS") ; return false; }
                if(count == 0){ alert("FALTA SELECCIONAR LA FECHA DE INICIO"); return false; }
            } else {
                if(count == 0){ alert("FALTA SELECCIONAR LOS DIAS DE LA AGENDA"); return false; }
            }
            if ($("#listaProgramacionTerapiasAgenda").getGridParam("reccount") + count > $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(terapia_seleccionada).CANTIDAD) { alert("NO SE PUEDE SUPERAR LA CANTIDAD ORDENADA") ; return false; }
            break;

        case 'crearCopago':
            admision = $("#listAdmisionCuenta").jqGrid('getGridParam', 'selrow')
            id_factura = $("#listAdmisionCuenta").getRowData(admision).ID_FACTURA
            id_estado_factura = $("#listAdmisionCuenta").getRowData(admision).ID_ESTADO
            if (id_factura === undefined) { alert("DEBE SELECCIONAR UNA ADMISION CON FACTURA") ; return false; }
            if (id_estado_factura != 'P') { alert("LA FACTURA DEBE ESTAR EN ESTADO PREFINALIZADO.") ; return false; }
            if (isNaN(valorAtributo('txtValorRecibo')) || valorAtributo('txtValorRecibo') <= 0) { alert('EL VALOR DEBE SER UN VALOR NUMERICO MAYOR A 0'); return false; }
            break;
            
        case 'asociarCitasAdmision':
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('EL ESTADO DE LA FACTURA NO PERMITE MODIFICAR'); return false; }
            return confirm("Esta seguro de registrar las citas a esta admision ?")
            break;

        case 'eliminarTipoCita':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE TIPO DE CITA!'); return false; }
            break;

        case 'modificarTipoProcedim':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE TIPO DE CITA!'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('SELECCIONE NOMBRE!'); return false; }
            if (valorAtributo('txtRecomendaciones') == '') { alert('SELECCIONE RECOMENDACIONES!'); return false; }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE ESPECIALIDAD!'); return false; }
            break;

        case 'adicionarTipoCitaFormulario':
            if (valorAtributo('lblIdFormulario') == '') { alert('SELECCIONE UN FORMULARIO'); return false }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE UN TIPO DE CITA'); return false }
            break;
        case 'modificarTipoCitaFormulario':
            if (valorAtributo('lblIdFormulario') == '') { alert('SELECCIONE UN FORMULARIO'); return false }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE UN TIPO DE CITA'); return false }
            break;
        case 'eliminarEspecialidadSede':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA ESPECIALIDAD.'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdSede') == '') { alert('SELECCIONE UNA SEDE.'); return false; }
            break;

        case 'asignarSedeEspecialidad':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA ESPECIALIDAD.'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdSede') == '') { alert('SELECCIONE UNA SEDE.'); return false; }
            break;

        case 'modificarEspecialidad':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA ESPECIALIDAD.'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA ASIGNAR NOMBRE.'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('SELECCIONE EL SERVICIO.'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA ASIGNAR VIGENCIA.'); return false; }
            break;

        case 'crearEspecialidadSede':
            if (valorAtributo('txtNombre') == '') { alert('FALTA ASIGNAR NOMBRE.'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('SELECCIONE EL SERVICIO.'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA ASIGNAR VIGENCIA.'); return false; }
            break;

        case 'eliminarDisponibles':
            if ($("#listDiasC").jqGrid('getGridParam', 'selrow') === null) { alert('SELECCIONE AL MENOS UN DIA DE LA AGENDA'); return false; }
            if (valorAtributo('cmbHoraInicio') == '') { alert('SELECCIONE HORA DE INICIO.'); return false; }
            if (valorAtributo('cmbMinutoInicio') == '') { alert('SELECCIONE MINUTO DE INICIO.'); return false; }
            if (valorAtributo('cmbPeriodoInicio') == '') { alert('FALTA SELECCIONAR PERIORDO DE INICIO AM/PM.'); return false; }
            if (valorAtributo('cmbHoraFinEliminar') == '') { alert('SELECCIONE HORA DE FIN.'); return false; }
            if (valorAtributo('cmbMinutoFinEliminar') == '') { alert('SELECCIONE MINUTO DE FIN.'); return false; }
            if (valorAtributo('cmbPeriodoFinEliminar') == '') { alert('FALTA SELECCIONAR PERIORDO DE FIN AM/PM.'); return false; }
            break;

        case 'inhabilitarElemento':
            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR ELEMENTO'); return false; }
            return confirm("Esta seguro de inhabilitar elemento ?") ? true : false;

        case 'copiarCitasFrecuencia':
            if (valorAtributo('txtCantidadCitas') == '') { alert('FALTA CANDTIDAD'); return false; }
            if (valorAtributo('txtFrecuenciaCitas') == '') { alert('FALTA FRECUENCIA'); return false; }
            break;

        case 'insertarProcedimientoResultadosExternos':
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA SELECCIONAR FOLIO.'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != 0) { alert('EL ESTADO DEL FOLIO NO PERMITE CREAR'); return false; }
            if (valorAtributo('txtIdProcedimientoResultado') == '') { alert('FALTA ESCOGER ESPECIALIDAD'); return false; }
            break;

        case 'eliminarPermisoPerfil':
            if (valorAtributo('lblIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR UNA OPCION DEL SISTEMA'); return false; }
            return confirm("Esta seguro de eliminar elemento ?") ? true : false;

            break;

        case 'finalizarHistoriaClinicaLaboratorio':
            if (valorAtributo('txtIdHCL') == '') { alert("DEBE SELECCIONAR UNA HISTORIA CLINICA"); return false };
            if (valorAtributo('txtIdHCLE') == '1') { alert("LA HISTORIA CLINICA YA FUE FINALIZADA"); return false };
            var ids = $("#listaOrdenesHistoriaClinicaLaboratorio").getDataIDs();
            if (ids.length == 0) { alert('DEBE AGREGAR ALGUN LABORATORIO PARA FINALIZAR.'); return false };

            break;

        case 'crearProcedimientoHistoriaClinicaLaboratorio':
            if (valorAtributo('txtIdHCL') == '') { alert("DEBE SELECCIONAR UNA HISTORIA CLINICA"); return false };
            if (valorAtributo('txtIdHCLE') == '1') { alert("LA HISTORIA CLINICA YA FUE FINALIZADA"); return false };
            if (valorAtributo('txtIdProcedimientoLaboratorio') == '') { alert("DEBE SELECCIONAR UN PROCEDIMIENTO"); return false };
            if (valorAtributo('cmbCantidadLaboratorio') == '') { alert("DEBE SELECCIONAR LA CANTIDAD"); return false };

            break;


        case 'modificarDocumentoTipificacionFormulario':
            if (valorAtributo('lblNombreFormulario') == '') { alert("DEBE SELECCIONAR UN FORMULARIO"); return false };
            break;

        case 'traerTarifarioPlanContratacion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN DE CONTRATACION'); return false };
            if (valorAtributo('cmbIdTarifario') == '') { alert('FALTA SELECCIONAR TARIFARIO BASE'); return false };
            if (valorAtributo('txtPorcentajeIncremento') == '' || isNaN(valorAtributo('txtPorcentajeIncremento'))) { alert('INGRESE UN VALOR NUMERICO EN PORCETANJE DE INCREMENTO'); return false };
            break;

        case 'crearAdmisionSinEncuesta':
            if (valorAtributo('lblIdPaciente') == '') { alert('DEBE SELECCIONAR UN PACIENTE'); return false };
            if (valorAtributo('cmbServicioEntrada') == '') { alert("FALTA SELECCIONAR EL SERVICIO"); return false };
            break;

        case 'crearCitasTerapias':

            console.log($("#listaProgramacionTerapias").getGridParam("reccount"));
            if (valorAtributo('lblIdProcedimientoAutoriza') == '') { alert('DEBE SELECCIONAR UN PROCEDIMIENTO'); return false };
            //if (valorAtributo('lblIdClaseLab') != '9') { alert('EL PROCEDIMIENTO DEBE SER CLASE TERAPEUTICO'); return false };
            if (valorAtributo('cmbSedeTerapia') == '') { alert('DEBE SELECCIONAR UNA SEDE'); return false };
            if (valorAtributo('cmbIdEspecialidadTerapia') == '') { alert('DEBE SELECCIONAR UNA ESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCitaTerapia') == '') { alert('DEBE SELECCIONAR UN TIPO DE CITA'); return false };
            if (valorAtributo('cmbProfesionalesTerapia') == '') { alert('DEBE SELECCIONAR UN PROFESIONAL'); return false };
            if (valorAtributo('txtFechaInicioTerapia') == '') { alert('DEBE SELECCIONAR UNA FECHA INICIO'); return false };
            if (valorAtributo('cmbFrecuenciaTerapia') == '') { alert('DEBE SELECCIONAR UNA FRECUENCIA'); return false };
            if (valorAtributo('cmbCantidadCitasTerapia') == '') { alert('DEBE SELECCIONAR UNA CANTIDAD'); return false };
            if ((parseInt(valorAtributo('cmbCantidadCitasTerapia')) + $("#listaProgramacionTerapias").getGridParam("reccount")
            ) > parseInt(valorAtributo('lblCantidadLab'))) { alert('LA CANTIDAD SUPERA EL NUMERO DE TERAPIAS ORDENADAS'); return false }
            break;

        case 'modificarProgramacionTerapia':
            if (valorAtributo('lblIdProgramacionTerapiaEditar') == '') { alert('DEBE SELECCIONAR UNA TERAPIA'); return false };
            if (valorAtributo('cmbSedeTerapiaEditar') == '') { alert('DEBE SELECCIONAR UNA SEDE'); return false };
            if (valorAtributo('cmbIdEspecialidadTerapiaEditar') == '') { alert('DEBE SELECCIONAR UNA ESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCitaTerapiaEditar') == '') { alert('DEBE SELECCIONAR UN TIPO DE CITA'); return false };
            if (valorAtributo('cmbProfesionalesTerapiaEditar') == '') { alert('DEBE SELECCIONAR UN PROFESIONAL'); return false };
            if (valorAtributo('txtFechaInicioTerapiaEditar') == '') { alert('DEBE SELECCIONAR UNA FECHA INICIO'); return false };

            break;

        case 'eliminarProgramacionTerapia':
            if (!confirm('ESTA SEGURO QUE DESEA ELIMINAR LA TERAPIA?')) { return false; }

            break;



        case 'modificaPlanContratacion':
            if (valorAtributo('txtIdPlan') == '') { alert('DEBE SELECCIONAR UN PLAN'); return false };
            if (valorAtributo('txtDescPlan') == '') { alert('FALTA DESCRIPCION'); return false };
            if (valorAtributoIdAutoCompletar('txtIdAdministradora') == '') { alert('FALTA ADMISTRADORA O EL FOMATO NO ES VALIDO'); return false };
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FATA TIPO DE REGIMEN'); return false };
            if (valorAtributo('cmbIdTipoCliente') == '') { alert('FALTA TIPO DE CLIENTE'); return false };
            if (valorAtributo('txtNumeroContrato') == '') { alert('FALTA NUMERO DE CONTRATO'); return false };
            if (valorAtributo('txtFechaInicio') == '') { alert('FALTA FECHA INICIAL'); return false };
            if (valorAtributo('txtFechaFinal') == '') { alert('FALTA FECHA FINAL'); return false };
            if (valorAtributo('txtMontoContrato') == '' || isNaN(valorAtributo('txtMontoContrato'))) { alert('FALTA MONTO CONTRATO O EL VALOR NO ES NUMERICO'); return false };
            if (valorAtributo('cmbIdTipoPlan') == '') { alert('FALTA TIPO DE PLAN'); return false };
            //if (valorAtributo('cmbIdTarifario') == '') { alert('FALTA TARIFARIO BASE'); return false };
            //if (valorAtributo('txtPorcentajeIncremento') == '') { alert('FALTA PORCENTAJE DE INCREMENTO'); return false };
            break;

        case 'crearPlanContratacion':
            if (valorAtributo('txtDescPlan') == '') { alert('FALTA DESCRIPCION'); return false };
            if (valorAtributoIdAutoCompletar('txtIdAdministradora') == '') { alert('FALTA ADMISTRADORA O EL FOMATO NO ES VALIDO'); return false };
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FATA TIPO DE REGIMEN'); return false };
            if (valorAtributo('cmbIdTipoCliente') == '') { alert('FALTA TIPO DE CLIENTE'); return false };
            if (valorAtributo('txtNumeroContrato') == '') { alert('FALTA NUMERO DE CONTRATO'); return false };
            if (valorAtributo('txtFechaInicio') == '') { alert('FALTA FECHA INICIAL'); return false };
            if (valorAtributo('txtFechaFinal') == '') { alert('FALTA FECHA FINAL'); return false };
            if (valorAtributo('txtMontoContrato') == '' || isNaN(valorAtributo('txtMontoContrato'))) { alert('FALTA MONTO CONTRATO O EL VALOR NO ES NUMERICO'); return false };
            if (valorAtributo('cmbIdTipoPlan') == '') { alert('FALTA TIPO DE PLAN'); return false };
            break;

        case 'modificarPacienteEncuesta':
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA ESCOGER TIPO DE DOCUMENTO'); return false };
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA DIGITAR IDENTIFICACION'); return false };
            if (valorAtributo('txtApellido1') == '') { alert('FALTA DIGITAR APELLIDO 1'); return false };
            if (valorAtributo('txtNombre1') == '') { alert('FALTA DIGITAR NOMBRE 1'); return false };
            if (valorAtributo('txtFechaNac') == '') { alert('FALTA DIGITAR FECHA NACIMIENTO'); return false };
            if (valorAtributo('cmbSexo') == '') { alert('FALTA ESCOGER SEXO'); return false };
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA DIGITAR MUNICIPIO'); return false };
            break;

        case 'nuevoIdPacienteEncuesta':
            if (valorAtributo('cmbTipoIdNuevoPacienteEncuesta') == '') { alert('FALTA ESCOGER TIPO DE DOCUMENTO'); return false };
            if (valorAtributo('txtNumeroDocNuevoPacienEncuesta') == '') { alert('FALTA NUMERO DE DOCUMENTO'); return false };
            break;

        case 'interpretarProcedimiento':
            if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 8 || valorAtributo('txtIdEsdadoDocumento') == 9 || valorAtributo('txtIdEsdadoDocumento') == 10) {
                alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR');
                return false;
            }
            if (valorAtributo('lblIdOrden') == '') { alert('FALTA ESCOGER PROCEDIMIENTO'); return false };
            if (valorAtributo('lblIdEstadoOrden') != '3' && valorAtributo('lblIdEstadoOrden') != '4') { alert('EL PROCEDIMIENTO DEBE ESTAR REALIZADO PARA INTERPRETAR'); return false };
            if (valorAtributo('txtInterpretacion') === '') { alert('LA INTERPRETACION NO PUEDE ESTAR VACIA'); return false };

            break;

        case 'modificarInterpretacionProcedimiento':
            if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 8 || valorAtributo('txtIdEsdadoDocumento') == 9 || valorAtributo('txtIdEsdadoDocumento') == 10) {
                alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR');
                return false;
            }
            if (valorAtributo('lblIdOrden') == '') { alert('FALTA ESCOGER PROCEDIMIENTO'); return false };
            if (valorAtributo('txtInterpretacion') === '') { alert('LA INTERPRETACION NO PUEDE ESTAR VACIA'); return false };

            break;

        case 'listGrillaPaciente':
            if ((valorAtributo('txtCodBus') == '' && valorAtributo('txtIdBus') == '' && valorAtributo('txtNomBus') == '')) {
                alert('INGRESE UN PARAMETRO DE BUSQUEDA');
                return false;
            }

            break;

        case 'listGrillaVentana':
            if ((valorAtributo('txtCodBus') == '' && valorAtributo('txtNomBus') == '')) {
                alert('INGRESE UN PARAMETRO DE BUSQUEDA');
                return false;
            }
            break;

        case 'direccionarLaboratorio':
            if (!(valorAtributo('lblIdClaseLab') === '4' || valorAtributo('lblIdClaseLab') === '5')) {
                alert('SOLO PUEDE DIRECCIONAR DIAGNOSTICOS O LABORATORIOS'); return false
            };
            if (valorAtributo('lblIdEstadoLab') == '2') { alert('EL PROCEDIMIENTO YA SE ENCUENTRA DIRECCIONADO'); return false };
            if (valorAtributo('lblIdEstadoLab') != '1') { alert('SOLO PUEDE DIRECCIONAR EN ESTADO SOLICITADO'); return false };
            if (valorAtributo('txtIdLaboratorioEmpresa') == '') { alert('FALTA LABORATORIO'); return false };
            if (valorAtributo('lblIdProcedimientoAutoriza') == '') { alert('FALTA ESCOGER PROCEDIMIENTO'); return false };

            break;

        case 'crearCitaProgramacion':
            if (valorAtributo('lblIdProcedimientoAutoriza') == '') { alert('DEBE SELECCIONAR UNA ORDEN'); return false };
            if (valorAtributo('cmbSede') == '') { alert('SELECCIONE UNA SEDE'); return false };
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE UNA ESPECIALIDAD'); return false };
            if (valorAtributo('cmbProfesionales') == '') { alert('SELECCIONE UN PROFESIONAL'); return false };
            if (valorAtributo('txtFechaCita') == '') { alert('FALTA SELECCIONAR FECHA'); return false };
            ids = $("#listaEsperaProgramacion").getDataIDs();
            if (ids.length > 0) { alert('LA ORDEN YA TIENE UNA CITA O LISTA DE ESPERA ASOCIADA.'); return false };
            break;

        case 'crearListaEsperaProgramacion':
            if (valorAtributo('lblIdProcedimientoAutoriza') == '') { alert('DEBE SELECCIONAR UNA ORDEN'); return false };
            if (valorAtributo('cmbSede') == '') { alert('SELECCIONE UNA SEDE'); return false };
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE UNA ESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE UN TIPO DE CITA'); return false };
            if (valorAtributo('cmbDiscapaciFisica') == '') { alert('SELECCIONE DISCAPACIDAD FISICA'); return false };
            if (valorAtributo('cmbNecesidad') == '') { alert('SELECCIONE GRADO DE NECESIDAD'); return false };
            ids = $("#listaEsperaProgramacion").getDataIDs();
            if (ids.length > 0) { alert('LA ORDEN YA TIENE UNA CITA O LISTA DE ESPERA ASOCIADA.'); return false };
            break;

        case 'modificarAdmisionCama':
            if (valorAtributo('lblIdAdmisionUbicacionPaciente') == '') { alert('FALTA SELECCIONAR PACIENTE'); return false };
            break;

        case 'enviarOrdenMedicaURG':
            if (valorAtributo('lblIdAdmision') == '') { alert('DEBE SELECCIONAR PACIENTE CON UNA ADMISION'); return false };
            if (valorAtributo('lblIdDocumento') == '') { alert('DEBE SELECCIONAR UN FOLIO'); return false };
            if (valorAtributo('txtIdEsdadoDocumento') != 0) { alert('EL ESTADO DEL FOLIO NO PERMITE ADICIONAR'); return false };
            if (valorAtributo('cmbOrdenenesAdmisionURG') == '') { alert('DEBE SELECCIONAR LA ORDEN DEL SERVICIO'); return false };
            if (valorAtributo('cmbOrdenenesAdmisionURG') == "1") {
                if (valorAtributo('cmbTipoAlta') == "") { alert('SELECCIONE EL TIPO DE ALTA MEDICA'); return false };
                if (valorAtributo('cmbTipoAlta') == "6") {
                    if (valorAtributo('txtIdDxMuerte') == "") { alert('INGRESE LA CAUSA BÁSICA DE MUERTE'); return false };
                    if (!$('#sw_fecha_actual').attr('checked')) {
                        if (valorAtributo('txtFechaMuerte') == "") { alert('INGRESE LA FECHA DE MUERTE'); return false };
                        if (valorAtributo('txtHoraMuerte') == "") { alert('INGRESE LA HORA DE MUERTE'); return false };
                        if (valorAtributo('txtMinutoMuerte') == "") { alert('INGRESE EL MINUTO DE MUERTE'); return false };
                        if (valorAtributo('cmbPeriodoMuerte') == "") { alert('INGRESE EL PERIODO (AM - PM)'); return false };
                        if (valorAtributo('txtHoraMuerte') <= 0 || valorAtributo('txtHoraMuerte') > 12) { alert('FORMATO DE HORA NO ES VALIDO'); return false };
                        if (valorAtributo('txtMinutoMuerte') < 0 || valorAtributo('txtHoraMuerte') > 59) { alert('FORMATO DE HORA NO ES VALIDO'); return false };
                    }
                }
            }
            break;

        case 'modificarServicioPacienteURG':
            if (valorAtributo('cmbIdTipoAdmisionModificarURG') == '') { alert('DEBE SELECCIONAR UN SERVICIO'); return false };
            break;

        case 'ingresarPacienteURG':
            if (valorAtributo('lblIdPacienteUrgencias') == '') { alert('FALTA SELECCIONAR PACIENTE'); return false };
            if (valorAtributo('cmbIdTipoServicioUrgencias') == '') { alert('FALTA SELECCIONAR SERVICIO'); return false };
            tipo_admision_ingreso = $("#cmbIdTipoServicioUrgencias option:selected").attr("title").split(":")[1].trim();
            //if (tipo_admision_ingreso == '') { alert('NO ES POSIBLE INGRESAR PACIENTE AL SERVICIO'); return false };
            //if (valorAtributo('cmbIdTipoServicioUrgencias') != 'ORI') { alert('PACIENTE DEBE INGRESAR A ORIENTACION'); return false };
            break;

        case 'subirExcelFacturacion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false };
            if (!confirm('SE ELIMINARAN TODOS LOS PROCEDIMIENTOS DEL PLAN \n\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'crearPacienteUrgencias':
            if (valorAtributo('txtApellido1Urgencias') == '' && valorAtributo('txtApellido2Urgencias') == '') { alert('DEBE INGRESAR AL MENOS UN APELLIDO'); return false };
            if (valorAtributo('txtNombre1Urgencias') == '' && valorAtributo('txtNombre2Urgencias') == '') { alert('DEBE INGRESAR AL MENOS UN NOMBRE'); return false };
            if (valorAtributo('cmbSexoUrgencias') == '') { alert('FALTA SEXO PACIENTE'); return false };
            if (valorAtributo('txtFechaNacimientoUrgencias') == '' && valorAtributo('txtEdadUrgencias') == '') { alert('INGRESE FECHA DE NACIMIENTO'); return false };
            if (valorAtributoIdAutoCompletar('txtAdministradoraUrgencias') == '') { alert('FALTA ADMINISTRADORA'); return false };
            if (valorAtributoIdAutoCompletar('txtMunicipioResidenciaUrgencias') == '') { alert('FALTA MUNICIPIO'); return false };
            break;

        case 'nuevoIdPacienteUrgencias':
            if (valorAtributo('cmbTipoIdUrgencias') == '') { alert('DEBE SELECCIONAR UN TIPO DE DOCUMENTO'); return false };
            if (valorAtributo('txtIdentificacionUrgencias') == '' && ["AS", "MS"].indexOf(valorAtributo('cmbTipoIdUrgencias')) == -1) { alert('FALTA DIGITAR NUMERO DE IDENTIFICAICON'); return false };
            break;

        case 'modificaRuta':
            if (valorAtributo('txtId') == '') { alert('DIGITE EL ID'); return false };
            if (valorAtributo('txtNombre') == '') { alert('DIGITE NOMBRE'); return false };
            if (valorAtributo('txtDuracion') == '') { alert('DIGITE DURACION'); return false };
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE VIGENCIA'); return false };
            if (valorAtributo('cmbPlan') == '') { alert('SELECCIONE PLAN'); return false };
            if (valorAtributo('cmbCiclica') == '') { alert('SELECCIONE CICLICA'); return false };
            break;

        case 'pacienteConsulta':
            if (valorAtributo('lblIdTipoAdmision') == '') { alert('NO SE PUEDE ATENDER PORQUE DEBE TENER UNA ADMISION'); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert(' DEBE SELECCIONAR UN FOLIO'); return false; }
            break;

        case 'modificarTipoCitaARutaPaciente':
            if (valorAtributo('cmbIdEspecialidadEdit') == '') { alert('SELECCIONE UNA ESPECIALIDAD'); return false };
            //if (valorAtributo('cmbIdSubEspecialidadEdit') == '') { alert('SELECCIONE UNA SUBESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCitaEdit') == '') { alert('SELECCIONE TIPO DE CITA'); return false };
            if (valorAtributo('cmbEsperarEdit') == '') { alert('SELECCIONE UN TIEMPO DE ESPERA'); return false };
            if (valorAtributo('txtOrdenEdit') == '') { alert('DIGITE ORDEN'); return false };
            if (valorAtributo('cmbNecesidadEdit') == '') { alert('SELECCIONE GRADO DE NECESIDAD'); return false };
            if (valorAtributo('txtPrestadorAsignadoEdit') == '') { alert('SELECCIONE PRESTADOR ASIGNADO'); return false };
            break;

        case 'adicionarTipoCitaARutaPaciente':
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONAR ESPECIALIDAD'); return false };
            // if (valorAtributo('cmbIdSubEspecialidad') == '') { alert('SELECCIONAR SUBESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONAR TIPO CITA'); return false };
            if (valorAtributo('cmbEsperar') == '') { alert('SELECCIONE UN PERIODO DE ESPERA'); return false };
            if (valorAtributo('txtOrden') == '') { alert('DEBE DIGITAR ORDEN'); return false };
            if (valorAtributo('cmbNecesidad') == '') { alert('DEBE DIGITAR GRADO DE NECESIDAD'); return false };
            if (valorAtributo('txtPrestadorAsignado') == '') { alert('ESCOJA EL PRESTADOR ASIGNADO'); return false };
            break;

        case 'eliminarRutaTipoCita':
            if (valorAtributo('lblIdParametr') == '') { alert('SELECCIONE UN PARAMETRO PARA ELIMINAR'); return false };
            break;

        case 'crearPacienteRuta':
            if (valorAtributo('lblIdPaciente') == '') { alert('SELECCIONE UN PACIENTE'); return false };
            if (valorAtributo('cmbPlan') == '') { alert('SELECCIONE UN PLAN DE ATENCION'); return false };
            if (valorAtributo('cmbRuta') == '') { alert('SELECCIONE UNA RUTA DEL PLAN'); return false };
            if (valorAtributo('txtPrestadorAsignadoRuta') == '') { alert('SELECCIONE UN PRESTADOR ASIGNADO A LA RUTA'); return false };
            break;

        case 'adicionarRutaTipoCita':

            if (valorAtributo('txtId') == '') { alert('SELECCIONE EL PLAN AL CUAL DESEA BORRARLE EL PARAMETRO'); return false };
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('DEBE SELECCIONAR UNA ESPECIALIDAD'); return false };
            //if (valorAtributo('cmbIdSubEspecialidad') == '') { alert('DEBE SELECCIONAR UNA SUBESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCita') == '') { alert('DEBE SELECCIONAR EL TIPO DE CITA'); return false };
            if (valorAtributo('cmbEsperar') == '') { alert('DEBE SELECCIONAR UN TIEMPO DE ESPERA'); return false };
            if (valorAtributo('txtOrden') == '') { alert('DEBE DIGITAR UNA ORDEN'); return false };
            if (valorAtributo('cmbEstado') == '') { alert('DEBE SELECCIONAR UN ESTADO'); return false };
            break;

        case 'crearRuta':
            if (valorAtributo('txtId') == '') { alert('DEBE DIGITAR ID'); return false };
            if (valorAtributo('txtNombre') == '') { alert('DEBE DIGITAR UN NOMBRE'); return false };
            if (valorAtributo('txtDuracion') == '') { alert('DEBE DIGITAR LA DURACION'); return false };
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE UNA OPCION EN VIGENTE'); return false };
            if (valorAtributo('cmbPlan') == '') { alert('SELECCIONE UNA OPCION EN PLAN'); return false };
            if (valorAtributo('cmbCiclica') == '') { alert('SELECCIONE UNA OPCION EN CICLICA'); return false };
            break;

        case 'modificarReferencia':
            if (valorAtributo('lblIdArea') == '') { alert('DEBE SELECCIONAR UN AREA'); return false; }
            if (valorAtributo('txtOrdenReferencia') == '') { alert('DEBE SELECCIONAR UN ORDEN'); return false; }
            if (valorAtributo('txtLatitudReferencia') == '') { alert('DEBE SELECCIONAR UNA LATITUD'); return false; }
            if (valorAtributo('txtLongitudReferencia') == '') { alert('DEBE SELECCIONAR UNA LONGITUD'); return false; }
            break;
        case 'adicionarReferencia':
            if (valorAtributo('lblIdArea') == '') { alert('DEBE SELECCIONAR UN AREA'); return false; }
            if (valorAtributo('txtOrdenReferencia') == '') { alert('DEBE SELECCIONAR UN ORDEN'); return false; }
            if (valorAtributo('txtLatitudReferencia') == '') { alert('DEBE SELECCIONAR UNA LATITUD'); return false; }
            if (valorAtributo('txtLongitudReferencia') == '') { alert('DEBE SELECCIONAR UNA LONGITUD'); return false; }
            break;
        case 'cambiarMedioPago':
            if (valorAtributo('lblIdFactura') == '') { alert('DEBE SELECCIONAR UNA FACTURA'); return false; }
            break;

        case 'modificarFormaPago':
            if (valorAtributo('lblIdFactura') == '') { alert('DEBE SELECCIONAR UNA FACTURA'); return false; }
            break;

        case 'guardarDatosEncuesta':
            if (valorAtributo('lblIdEncuestaPaciente') == '') { alert('DEBE SELECCIONAR UN paciente'); return false; }
            break;

        case 'guardarDatosPlantilla':
            if (valorAtributo('lblIdDocumento') == '') { alert('NO SE ENCUENTRA ID EVOLUCION'); return false; }

            break;

        case 'crearEncuestaPaciente':
            if (valorAtributo('lblIdPaciente') == '') { alert('DEBE SELECCIONAR UN PACIENTE.'); return false; }
            if (document.getElementById('lblIdEncuestaParaCrear').value == '' || document.getElementById('lblIdEncuestaParaCrear').value == '0') { alert('DEBE SELECCIONAR UNA ENCUESTA.'); return false; }
            //if (valorAtributo('lblDiligenciado') == 'Diligenciada -Cerrada') { alert('DEBE SELECCIONAR UNA ENCUESTA SIN DILIGENCIAR.'); return false; }
            //if (valorAtributo('lblDiligenciado') == 'Diligenciada - Abierta') { alert('DEBE SELECCIONAR UNA ENCUESTA SIN DILIGENCIAR.'); return false; }
            break;

        case 'cerrarEncuestaPaciente':
            if (valorAtributo('lblIdEncuestaPaciente') == '') { alert('DEBE SELECCIONAR UNA ENCUESTA.'); return false; }
            if (valorAtributo('lblDiligenciado') == 'Sin Diligenciar') { alert('DEBE SELECCIONAR UNA ENCUESTA DILIGENCIADA O ABIERTA.'); return false; }
            if (valorAtributo('lblDiligenciado') == 'Diligenciada -Cerrada') { alert('DEBE SELECCIONAR UNA ENCUESTA DILIGENCIADA ABIERTA.'); return false; }

            break;

        case 'cambiarFormaPago':
            if (valorAtributo('lblIdFactura') == '') { alert('DEBE SELECCIONAR UNA FACTURA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'P') { alert('LA FACTURA NO SE PUEDE MODIFICAR'); return false; }
            break;

        case 'accionesAlEstadoFolio':
            if (valorAtributo('cmbAccionFolio') == 7 && valorAtributo('txtIdEsdadoDocumento') != 0) { alert('EL ESTADO DEL FOLIO ACTUAL NO PERMITE MODIFICACIONES.'); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert('NO SE PUEDE ATENDER PORQUE DEBE TENER UNA ADMISION.'); return false; }
            if (valorAtributo('cmbAccionFolio') == 0 && valorAtributo('cmbMotivosAbrir') == '') { alert('DEBE SELECCIONAR UN MOTIVO PARA ABRIR.'); return false; }
            if (valorAtributo('txtMotivoAccion') == '') { alert('ESCRIBA JUSTIFICACION DE LA ACCION.'); return false; }
            if (valorAtributo('lblIdAuxiliar') == '') { alert('FALTA CLAVE AUXILIAR.'); return false; }

            if (!confirm('SOLO MODIFICARA EL ESTADO DEL FOLIO \nSE REGISTRARA ESTA ACCION A SU NOMBRE CON FECHA Y HORA \n\nESTA SEGURO?')) { return false; }

            break;

        case 'crearConsentimiento':
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA SELECCIONAR FOLIO.'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != 0) { alert('EL ESTADO DEL FOLIO NO PERMITE CREAR'); return false; }
            if (valorAtributo('cmbTipoConsentimiento') == '') { alert('FALTA TIPO DE CONSENTIMIENTO.'); return false; }
            break;

        case 'modificarPlanAtencion':
            if (valorAtributo('txtId') == '') { alert('FALTA ID PLAN.'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            if (valorAtributo('txtObservaciones') == '') { alert('FALTA OBSERVACIONES'); return false; }
            if (valorAtributo('txtDuracion') == '') { alert('FALTA DURACION'); return false; }
            break;

        case 'modificarPacienteAdmision':
            if (valorAtributo('txtFechaNac') == '') { alert('FALTA FECHA DE NACIMIENTO'); return false; }
            if (valorAtributo('cmbSexo') == '') { alert('FALTA SEXO'); return false; }
            if (valorAtributoIdAutoCompletar('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            break;

        case 'agregarTurno':
            if (valorAtributo('cmbTurno') == '') { alert('FALTA TURNO'); return false; }

            var array = valorAtributo('lblIds').split(',');

            for (i = 0; i < array.length; i++) {
                var turno = array[i].split('::')[1];
                if (valorAtributo('cmbTurno') === turno) {
                    alert('EL TURNO ' + valorAtributo('cmbTurno') + ' YA SE ENCUENTRA ASIGNADO'); return false;
                }
            }

            break;

        case 'llamarTurno':
            if (valorAtributo('lblTurno') == '') { alert('NO TIENE TURNO'); return false; }
            if (valorAtributo('lblTurno') != '1') { alert('SOLO PUEDE LLAMAR AL TURNO 1'); return false; }
            break;

        case 'eliminarTurno':
            //if (valorAtributo('lblTurno') == '') { alert('NO TIENE TURNO'); return false; }
            break;

        case 'modificarTurno':
            if (valorAtributo('lblTurno') == '') { alert('NO TIENE TURNO'); return false; }
            if (valorAtributo('lblTurno') != '1') { alert('SOLO PUEDE INGRESAR AL TURNO 1'); return false; }
            break;


        case 'procedimientoAdd':
            if (valorAtributo('txtId') == '') { alert('FALTA ID'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            if (valorAtributo('cmbIdTipoRips') == '') { alert('FALTA TIPO RIPS'); return false; }
            if (valorAtributo('cmbIdCentroCosto') == '') { alert('FALTA CENTRO DE COSTO'); return false; }
            if (valorAtributo('cmbIdGrupoCentroCosto') == '') { alert('FALTA GRUPO CENTRO DE COSTO'); return false; }
            if (valorAtributo('cmbNivel') == '') { alert('FALTA NIVEL'); return false; }
            if (valorAtributo('txtCups') == '') { alert('FALTA CUPS'); return false; }
            if (valorAtributo('txtCosto') == '') { alert('FALTA COSTO'); return false; }
            if (valorAtributo('cmbClase') == '') { alert('FALTA CLASE'); return false; }
            if (valorAtributo('cmbTipoServicio') == '') { alert('FALTA TIPO DE SERVICIO'); return false; }
            if (valorAtributo('cmbTipo') == '') { alert('FALTA TIPO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA VIGENTE'); return false; }
            if (valorAtributo('cmbMostrarHc') == '') { alert('FALTA MOSTRAR HISTORIA CLINICA'); return false; }
            if (valorAtributo('cmbMostrarV') == '') { alert('FALTA VIGENTE PARA MAS DE UN PROCEDIMIENTO'); return false; }
            break;

        case 'procedimientosEdit':
            if (valorAtributo('txtId') == '') { alert('FALTA ID'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            if (valorAtributo('cmbIdTipoRips') == '') { alert('FALTA TIPO RIPS'); return false; }
            if (valorAtributo('cmbIdCentroCosto') == '') { alert('FALTA CENTRO DE COSTO'); return false; }
            if (valorAtributo('cmbIdGrupoCentroCosto') == '') { alert('FALTA GRUPO CENTRO DE COSTO'); return false; }
            if (valorAtributo('cmbNivel') == '') { alert('FALTA NIVEL'); return false; }
            if (valorAtributo('txtCups') == '') { alert('FALTA CUPS'); return false; }
            if (valorAtributo('txtCosto') == '') { alert('FALTA COSTO'); return false; }
            if (valorAtributo('cmbClase') == '') { alert('FALTA CLASE'); return false; }
            if (valorAtributo('cmbTipoServicio') == '') { alert('FALTA TIPO DE SERVICIO'); return false; }
            if (valorAtributo('cmbTipo') == '') { alert('FALTA TIPO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA VIGENTE'); return false; }
            if (valorAtributo('cmbMostrarHc') == '') { alert('FALTA MOSTRAR HISTORIA CLINICA'); return false; }
            if (valorAtributo('cmbMostrarV') == '') { alert('FALTA VIGENTE PARA MAS DE UN PROCEDIMIENTO'); return false; }
            break;




        case 'crearArchivosRips':
            if (valorAtributo('cmbIdPlan') == '') { alert('FALTA SELECCIONAR PLAN DE CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipo') == '') { alert('FALTA SELECCIONAR TIPO POS'); return false; }
            break;

        case 'asociarReciboFactura':
            if (valorAtributo('lblIdReciboAsociar') == 'F') { alert('FALTA SELECCIONAR RECIBO.'); return false; }
            if (valorAtributo('lblIdFacturaAsociar') == 'F') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO SE PUEDE ASOCIAR PORQUE LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO SE PUEDE ASOCIAR PORQUE LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'T') { alert('NO SE PUEDE ASOCIAR PORQUE LA FACTURA SE ENCUENTRA ABIERTA.'); return false; }
            if (valorAtributo('lblIdEstadoRecibo') != 'C') { alert('EL RECIBO DEBE ESTAR CERRADO.'); return false; }
            break;

        case 'desAsociarReciboFactura':
            if (valorAtributo('lblIdReciboAsociar') == 'F') { alert('FALTA SELECCIONAR RECIBO.'); return false; }
            if (valorAtributo('lblIdFacturaAsociar') == 'F') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO SE PUEDE DESASOCIAR PORQUE LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO SE PUEDE DESASOCIAR PORQUE LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'T') { alert('NO SE PUEDE DESASOCIAR PORQUE LA FACTURA SE ENCUENTRA ABIERTA.'); return false; }
            if (valorAtributo('lblIdEstadoRecibo') != 'C') { alert('EL RECIBO DEBE ESTAR CERRADO.'); return false; }
            break;


        case 'crearFacturaDesdeAdmision':
            if (valorAtributo('lblIdAdmision') == '') { alert('FALTA SELECCIONAR LA ADMISION'); return false; }
            if (!confirm('DESEA CREAR OTRA FACTURA Y ASOCIARLA A LA ADMISION SELECCIONADA?')) { return false; }
            break;

        case 'auditarRecibo':
            if (valorAtributo('lblIdRecibo') == '') { alert('SELECCIONE UN RECIBO PARA AUDITAR'); return false; }
            if (valorAtributo('lblIdEstadoRecibo') != 'C') { alert('EL RECIBO DEBE ESTAR CERRADO'); return false; }
            if (valorAtributo('txtIdEstadoAuditoria') == '2') { alert('EL RECIBO YA ESTA AUDITADO'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA AUDITAR EL RECIBO?')) { return false; }
            break;

        case 'auditarFactura':
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA PARA AUDITAR'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'F') { alert('LA FACTURA DEBE ESTAR FINALIZADA'); return false; }
            if (valorAtributo('lblIdEstadoAuditoria') == '2') { alert('NO SE PUEDE AUDITAR, YA SE ENCUENTRA EN ESTE ESTADO'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA AUDITAR LA FACTURA?')) { return false; }
            break;

        case 'modificarListaEsperaCirugia':

            if (valorAtributo('cmbGradoNecesidadMod') == '') { alert('FALTA SELECCIONAR GRADO NESECIDAD'); return false; }
            if (valorAtributo('txtAdministradoraLE') == '') { alert('FALTA SELECCIONAR ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimenLE') == '') { alert('FALTA SELECCIONAR REGIMEN - PLAN'); return false; }
            if (valorAtributo('lblIdPlanContratacionLE') == '') { alert('FALTA SELECCIONAR  REGIMEN - PLAN'); return false; }

            break;

        case 'modificarObservacionListaEspera':

            if (valorAtributo('cmbGradoNecesidadMod') == '') { alert('FALTA SELECCIONAR GRADO NESECIDAD'); return false; }
            if (valorAtributo('txtAdministradoraLE') == '') { alert('FALTA SELECCIONAR ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimenLE') == '') { alert('FALTA SELECCIONAR REGIMEN - PLAN'); return false; }
            if (valorAtributo('lblIdPlanContratacionLE') == '') { alert('FALTA SELECCIONAR  REGIMEN - PLAN'); return false; }

            break;

        case 'firmarPaciente':
            if (valorAtributo('lblIdTrabajo') == '') { alert('FALTA SELECCIONAR EL TRABAJO'); return false; }
            break;

        case 'crearRecibo':

            if (valorAtributo('txtIdEditPacienteRecibo') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA PLAN DE CONTRATACION'); return false; }
            if (valorAtributo('txtValorUnidadRecibo') == '') { alert('FALTA VALOR'); return false; }
            if (valorAtributo('cmbIdConceptoRecibo') == '') { alert('FALTA CONCEPTO'); return false; }
            if (valorAtributo('cmbIdMedioPago') == '') { alert('FALTA MEDIO DE PAGO'); return false; }
            break;

        case 'adicionarMunicipio':
            if (valorAtributo('lblPlanM') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA SELECCIONAR EL MUNICIPIO'); return false; }
            break;

        case 'eliminarMunicipio':
            if (valorAtributo('lblPlanM') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('lblMuni') == '') { alert('FALTA SELECCIONAR EL MUNICIPIO'); return false; }
            break;

        case 'modificarMunicipioPlan':
            if (valorAtributo('lblPlanM') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA SELECCIONAR EL MUNICIPIO'); return false; }
            break;

        case 'facturarLiquidacionCirugia':
            if (valorAtributo('lblIdLiquidacion') == '') { alert('FALTA SELECCIONAR LA LIQUIDACION'); return false; }
            break;

        case 'crearFacturaArticulo':

            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR DOCUMENTO'); return false; }
            if (valorAtributo('lblIdEstado') != '1') { alert('EL DOCUMENTO DEBE ESTAR FINALIZADO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR EPS'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }

            break;
        case 'adicionarFuncionarioVenta':

            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION DEL FUNCIONARIO'); return false; }
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA EL TIPO DE IDENTIFICACION!!'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL FUNCIONARIO'); return false; }
            if (valorAtributo('lblIdTercero') == '') { alert('SELECCIONE UN TERCERO!!'); return false; }

            break;

        case 'modificarFuncionarioVenta':


            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION DEL FUNCIONARIO'); return false; }

            break;
        case 'eliminarFuncionarioVenta':


            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION DEL FUNCIONARIO'); return false; }

            break;
        case 'eliminarFuncionarioVenta':


            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION DEL FUNCIONARIO'); return false; }

            break;

        case 'adicionarTercero':

            if (valorAtributo('txtTipoId') == '') { alert('FALTA EL TIPO IDENTIFICACION DEL TERCERO'); return false; }
            if (valorAtributo('txtIdTercero') == '') { alert('FALTA IDENTIFICACION DEL TERCERO'); return false; }
            if (valorAtributo('txtNombreTercero') == '') { alert('FALTA EL NOMBRE DEL TERCERO'); return false; }
            if (valorAtributo('cmbProveedor') == '') { alert('SELECCIONE SI ES PROVEEDOR!!'); return false; }

            break;

        case 'eliminarTercero':

            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR EL TERCERO'); return false; }

            break;
        case 'modificarTercero':

            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR EL TERCERO'); return false; }
            if (valorAtributo('txtTipoId') == '') { alert('FALTA EL TIPO IDENTIFICACION DEL TERCERO'); return false; }
            if (valorAtributo('txtIdTercero') == '') { alert('FALTA IDENTIFICACION DEL TERCERO'); return false; }
            if (valorAtributo('txtNombreTercero') == '') { alert('FALTA EL NOMBRE DEL TERCERO'); return false; }
            if (valorAtributo('cmbProveedor') == '') { alert('SELECCIONE SI ES PROVEEDOR!!'); return false; }

            break;
        case 'adicionarTrabajoControl':
            if (valorAtributo('txtIdFactura') == '') { alert('SELECCIONE LA FACTURA'); return false; }
            if (valorAtributo('txtNumeroOrdenN') == '') { alert('FALTA EL NUMERO DE ORDEN!!'); return false; }
            if (valorAtributo('cmbNombreLabo') == '') { alert('SELECCIONE EL PROVEEDOR'); return false; }
            if (valorAtributo('cmbNombreFuncionario1') == '') { alert('SELECCIONE FUNCIONARIO QUE RECIBE!!'); return false; }
            if (valorAtributo('txtFechaRecibeF') == '') { alert('SELECCIONE FECHA RECIBE!!'); return false; }
            if (valorAtributo('txtFechaCumpli') == '') { alert('SELECCIONE FECHA CUMPLIMIENTO!!'); return false; }
            break;

        case 'eliminarTrabajo':
            if (valorAtributo('lblIdTrabajo') == '') { alert('SELECCIONE EL TRABAJO'); return false; }
            if (valorAtributo('txtNumeroOrdenN') == '') { alert('SELECCIONE EL TRABAJO'); return false; }
            if (valorAtributo('lblFirma1') != '') { alert('NO SE PUEDE ELIMINAR, YA ESTA FIRMADO EL TRABAJO'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ELIMINAR EL TRABAJO?')) { return false; }

            break;


        case 'modificarDatosEnvioTrabajo':
            if (valorAtributo('lblIdTrabajo') == '') { alert('SELECCIONE EL TRABAJO'); return false; }
            if (valorAtributo('lblFirma1') != '') { alert('NO SE PUEDE MODIFICAR, YA ESTA FIRMADO ENVIADO'); return false; }
            if (valorAtributo('txtNumeroOrdenN') == '') { alert('FALTA EL NUMERO DE ORDEN!!'); return false; }
            if (valorAtributo('cmbNombreLabo') == '') { alert('SELECCIONE EL PROVEEDOR'); return false; }
            if (valorAtributo('cmbNombreFuncionario1') == '') { alert('SELECCIONE FUNCIONARIO QUE RECIBE!!'); return false; }
            if (valorAtributo('txtFechaRecibeF') == '') { alert('SELECCIONE FECHA RECIBE!!'); return false; }
            if (valorAtributo('txtFechaCumpli') == '') { alert('SELECCIONE FECHA CUMPLIMIENTO!!'); return false; }



            break;

        case 'recepcionarTrabajo':
            if (valorAtributo('txtIdEstado') != 'E') { alert('EL TRABAJO NO SE HA ENVIADO!!'); return false; }
            if (valorAtributo('lblFirma1') === '') { alert('FALTA FIRMA ENTREGADO'); return false; }
            if (valorAtributo('cmbNombreFuncionario2') == '') { alert('FALTA EL FUNCIONARIO'); return false; }
            break;
        case 'entregarTrabajo':
            if (valorAtributo('txtIdEstado') != 'C') { alert('NO SE TIENEN TRABAJOS POR ENTREGAR!!'); return false; }
            if (valorAtributo('txtNombreQuienRep') == '') { alert('FALTA EL NOMBRE DEL CLIENTE QUE RECIBE'); return false; }
            if (valorAtributo('lblFirma2') === '') { alert('FALTA FIRMA CUMPLIDO'); return false; }
            break;


        case 'agregarDetalleArticuloTrabajo':
            if (valorAtributo('lblIdTrabajoVentanita') != '') { alert('ARTICULO YA TIENE TRABAJO.'); return false; }
            break;

        case 'eliminarDetalleArticuloTrabajo':
            if (valorAtributo('lblIdTrabajoVentanita') === '') { alert('ARTICULO NO TIENE TRABAJO PARA ELIMINAR.'); return false; }
            break;

        case 'adicionarPiePagina':
            if (valorAtributo('txtNombre') == '') { alert('EL PIE DE PAGINA NO DEBE ESTAR VACIO'); return false; }
            if (valorAtributo('cmbTipo') == '') { alert('SELECCIONE UN TIPO'); return false; }
            break;


        case 'modificarPiePagina':
            if (valorAtributo('lblId') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('EL PIE DE PAGINA NO DEBE ESTAR VACIO'); return false; }
            if (valorAtributo('cmbTipo') == '') { alert('SELECCIONE UN TIPO'); return false; }
            break;

        case 'cambioEstadoFolio':

            if (valorAtributo('cmbIdEstadoFolioEdit') == '') { alert('FALTA ESTADO'); return false; }

            if (valorAtributo('cmbIdEstadoFolioEdit') == '7' || valorAtributo('cmbIdEstadoFolioEdit') == '10') {
                if (valorAtributo('cmbIdMotivoEstadoEdit') == '') { alert('FALTA MOTIVO'); return false; }
                if (valorAtributo('cmbMotivoClaseFolioEdit') == '') { alert('FALTA CLASIFICACION'); return false; }
            };

            break;

        case 'modificarTipoProcedim':

            if (valorAtributo('txtId') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }
            if (valorAtributo('txtRecomendaciones') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }

            break;

        case 'crearTipoCitaProcedimiento':

            if (valorAtributo('txtId') == '') { alert('FALTA EL ID TIPO DE CITA DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA DEFINIR EL PROCEDIMIENTO'); return false; }

            break;

        case 'eliminarTipoCitaProcedimiento':

            if (valorAtributo('txtId') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }

            break;

        case 'adicionarCentroCosto':
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            //if (valorAtributo('cmbPadre') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE VIGENCIA'); return false; }

            break;

        case 'eliminarCentroCosto':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            break;

        case 'modificarCentroCosto':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            //if (valorAtributo('cmbPadre') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE VIGENCIA'); return false; }
            break;


        case 'crearTipoCita':

            if (valorAtributo('txtId') == '') { alert('FALTA EL ID TIPO DE CITA DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA DEFINIR EL NOMBRE DEL TIPO DE CITA'); return false; }
            if (valorAtributo('txtRecomendaciones') == '') { alert('FALTA ANADIR RECOMENDACIONES'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA ANADIR EL ESTADO DEL PROCEDIMIENTO'); return false; }

            break;

        case 'crearArticuloTemporal':

            if (valorAtributo('txtNombreArticulo') == '') { alert('FALTA NOMBRE DEL ARTICULO'); return false; }
            if (valorAtributo('txtNombreComercial') == '') { alert('FALTA NOMBRE COMERCIAL DEL ARTICULO'); return false; }
            if (valorAtributo('cmbIdClase') == '') { alert('FALTA SELECCIONAR CLASE'); return false; }
            if (valorAtributo('cmbPresentacion') == '') { alert('FALTA SELECCIONAR PRESENTACION'); return false; }
            if (valorAtributo('txtConcentracion') == '') { alert('FALTA SELECCIONAR CONCENTRACION'); return false; }
            if (valorAtributo('cmbPOS') == '') { alert('FALTA SELECCIONAR POS'); return false; }
            if (jQuery("#listGrillaViaArticulo").getDataIDs().length == 0) { alert('FALTA AGREGAR VIA'); return false; }

            break;

        case 'agregarListaArticuloTemporal':

            if (valorAtributo('cmbViaArticulo') == '') { return false; }

            var ids = jQuery("#listGrillaViaArticulo").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listGrillaViaArticulo").getRowData(i);
                if (c == valorAtributo('cmbViaArticulo')) {
                    alert('YA EXISTE EN LA LISTA');
                    return false;
                }
            }


            break;


        case 'crearPlantillaEvolucion':
            if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 8 || valorAtributo('txtIdEsdadoDocumento') == 9 || valorAtributo('txtIdEsdadoDocumento') == 10) {
                alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR');
                return false;
            }
            break;

        case 'modificarPlantillaEvolucion':
            if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 8 || valorAtributo('txtIdEsdadoDocumento') == 9 || valorAtributo('txtIdEsdadoDocumento') == 10) {
                alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR');
                return false;
            }
            break;

        case 'crearPlantillaElemento':
            if (valorAtributo('lblCodigo') == '') { alert('SELECCIONE UNA PLANTILLA'); return false; }
            if (valorAtributo('txtDescripcion') == '') { alert('FALTA DESCRIPCION'); return false; }
            break;

        case 'modificarPlantillaElemento':
            if (valorAtributo('txtPlantilla') == '') { alert('SELECCIONE UNA PLANTILLA'); return false; }
            if (valorAtributo('txtOrden') == '') { alert('SELECCIONE UN ELEMENTO DE LA PLANTILLA'); return false; }
            break;


        case 'eliminarTratamientoPorDiente':
            if (valorAtributo('txtIdEsdadoDocumento') != '0') { alert('EL ESTADO DEL FOLIO NO PERMITE EDITAR'); return false; }
            break;
        case 'tratamientoPorDiente':
            if (valorAtributo('txtIdEsdadoDocumento') != '0') { alert('EL ESTADO DEL FOLIO NO PERMITE EDITAR'); return false; }
            break;

        case 'eliminarAdmisionFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA ADMISION PARA ELIMINAR'); return false; }
            if (LoginSesion() != valorAtributo('lblUsuarioCrea')) { alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO SE PUEDE ELIMINAR PORQUE LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO SE PUEDE ELIMINAR PORQUE LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'T') { alert('NO SE PUEDE ELIMINAR PORQUE LA FACTURA SE ENCUENTRA ABIERTA.'); return false; }
            if (valorAtributo('lblIdAdmision') == '') { alert('NO HA SELECCIONADO LA ADMISION.'); return false; }
            if (!confirm('SI ELIMINA LA ADMISION, SE ELIMINARA TODA LA INFORMACION DE LA FACTURA\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'eliminarAdmisionSinFactura':
            if (LoginSesion() != valorAtributo('lblUsuarioCrea')) { alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO'); return false; }
            if (valorAtributo('lblIdAdmision') == '') { alert('NO HA SELECCIONADO LA ADMISION.'); return false; }
            if (!confirm('SI LA ADMISION TIENE ASOCIADO FOLIOS NO PERMITIRA ELIMINAR\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'modificarProcedimientoCuenta':
            //if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO SE PUEDE MODIFICAR PORQUE LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            //if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO SE PUEDE MODIFICAR PORQUE LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            break;

        case 'modificarAdmisionFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE ADMISION A MODIFICAR'); return false; }
            //if (LoginSesion() != valorAtributo('lblUsuarioCrea')) { alert('NO SE PUEDE MODIFICAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO SE PUEDE MODIFICAR PORQUE LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO SE PUEDE MODIFICAR PORQUE LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('txtIdDx') == '') { alert('FALTA DIAGNOSTICO'); return false; }
            //if (valorAtributo('txtIPSRemite') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('FALTA SELECCIONAR ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbTipoAdmision') == '') { alert('FALTA TIPO ADMISION'); return false; }
            //if (valorAtributo('cmbIdProfesionales') == '') { alert('FALTA PROFESIONAL'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR EPS'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            if (!confirm('SI MODIFICA LA ADMISION SE ELIMINARAN TODOS LOS DATOS DE LA FACTURA\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'modificarAdmisionSinFactura':

            //if (LoginSesion() != valorAtributo('lblUsuarioCrea')) { alert('NO SE PUEDE MODIFICAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO'); return false; }
            if (valorAtributo('txtIdDx') == '') { alert('FALTA DIAGNOSTICO'); return false; }
            //if (valorAtributo('txtIPSRemite') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('FALTA SELECCIONAR ESPECIALIDAD'); return false; }
            //if (valorAtributo('cmbIdSubEspecialidad') == '') { alert('FALTA SELECCIONAR SUBESPECIALIDAD'); return false; }
            if (valorAtributo('cmbTipoAdmision') == '') { alert('FALTA TIPO ADMISION'); return false; }
            //if (valorAtributo('cmbIdProfesionales') == '') { alert('FALTA PROFESIONAL'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR EPS'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            if (!confirm('SI MODIFICA LA ADMISION SE ACTUALIZARAN LOS DATOS DE FOLIOS ASOCIADOS\nDESEA CONTINUAR?')) { return false; }
            break;


        case 'listMedicamentosEditar':

            if (valorAtributo('lblIdEditar') == '') { alert('FALTA ELEMENTO PARA EDITAR'); return false; }
            if (valorAtributo('cmbIdViaEditar') == '') { alert('FALTA VIA'); return false; }
            if (valorAtributo('cmbUnidadEditar') == '') { alert('FALTA FORMA FARMACEUTICA'); return false; }
            if (valorAtributo('cmbCantEditar') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('cmbFrecuenciaEditar') == '') { alert('FALTA FRECUENCIA'); return false; }
            if (valorAtributo('cmbDiasEditar') == '') { alert('FALTA DIAS'); return false; }
            if (valorAtributo('txtCantidadEditar') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('cmbUnidadFarmaceuticaEditar') == '') { alert('FALTA UNIDAD FARMACEUTICA'); return false; }


            break;
        case 'adicionarProcedimientoE':

            if (valorAtributo('txtIdProcedimientoe') == '') { alert('FALTA EL PROCEDIMIENTO'); return false; }


            break;

        case 'anularFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ANULAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'F') { alert('PARA ANULAR LA FACTURA, ESTA DEBE ESTAR FINALIZADA.'); return false; }
            if (valorAtributo('cmbIdMotivoAnulacion') == '') { alert('SELECCIONE MOTIVO.'); return false; }
            if (valorAtributo('txtNoDevolucion') != '') { alert('LA FACTURA YA TIENE UNA NOTA DE DEVOLUCION'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ANULAR LA FACTURA?,\nESTA OPCION NO SE PODRA REVERTIR.')) { return false; }
            break;


        case 'soloAnularFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ANULAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'F') { alert('PARA ANULAR LA FACTURA, ESTA DEBE ESTAR FINALIZADA.'); return false; }
            if (valorAtributo('cmbIdMotivoAnulacion') == '') { alert('SELECCIONE MOTIVO.'); return false; }
            if (valorAtributo('txtNoDevolucion') != '') { alert('LA FACTURA YA TIENE UNA NOTA DE DEVOLUCION'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ANULAR LA FACTURA?,\nESTA OPCION NO SE PODRA REVERTIR.')) { return false; }
            break;

        case 'anularFacturaVentas':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ANULAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'F') { alert('PARA ANULAR LA FACTURA, ESTA DEBE ESTAR FINALIZADA.'); return false; }
            if (valorAtributo('cmbIdMotivoAnulacion') == '') { alert('SELECCIONE MOTIVO.'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('LA FACTURA DEBE TENER DOCUMENTO.'); return false; }
            if (valorAtributo('txtNoDevolucion') != '') { alert('LA FACTURA YA TIENE UNA NOTA DE DEVOLUCION'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ANULAR LA FACTURA?,\nESTA OPCION NO SE PODRA REVERTIR.')) { return false; }
            break;

        case 'abrirFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ABRIR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'F') { alert('PARA ABRIR LA FACTURA, ESTA DEBE ESTAR FINALIZADA.'); return false; }
            if (valorAtributo('cmbIdMotivoAbrir') == '') { alert('SELECCIONE MOTIVO.'); return false; }
            if (valorAtributo('lblIdEstadoAuditoria') == '2') { alert('NO PUEDE ABRIR FACTURA, SE ENCUENTRA AUDITADA'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ABRIR LA FACTURA?')) { return false; }
            break;

        case 'cerrarFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA CERRAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'T') { alert('PARA CERRAR LA FACTURA, ESTA DEBE ESTAR ABIERTA.'); return false; }
            //if(valorAtributo('cmbIdMotivoAbrir') == ''){ alert('SELECCIONE MOTIVO.');  return false; }
            if (!confirm('ESTA SEGURO QUE DESEA CERRAR LA FACTURA?')) { return false; }
            break;

        case 'modificarFacturaPgp':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA MODIFICAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE EDITAR, LA FACTURA ESTA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE EDITAR, LA FACTURA ESTA ANULADA.'); return false; }
            if (valorAtributo('cmbNumeracionFactura') == '') { alert('FALTA NUMERACION.'); return false; }
            if (valorAtributo('txtValorNoCubierto') == '') { alert('FALTA VALOR NO CUBIERTO.'); return false; }
            if (valorAtributo('txtValorDescuento') == '') { alert('FALTA VALOR DESCUENTO.'); return false; }
            break;

        case 'modificarFechaAdmision':
            if (valorAtributo('txtFechaAdmision') == '') { alert('SELECCIONE UNA FACTURA PARA MODIFICAR.'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA PARA MODIFICAR.'); return false; }
            break;

        case 'mostrarDivVentanaDescuento':

            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE FACTURA PARA REALIZAR DESCUENTO.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('LA FACTURA SE ENCUENTRA ANULADA.'); return false; }

            break;

        case 'validaNumeracionFactura':

            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA ADMISION CON FACTURA.'); return false; }
            //if(valorAtributo('lblIdEstadoCuenta')=='A'){ alert('LA CUENTA DEBE ESTAR ABIERTA.');  return false; }


            break;


        case 'reemplazarCodigoBarras':
            if (valorAtributo('txtCodBarrasNuevo') == '') { alert('SELECCIONE CODIGO A REEMPLAZAR NUEVO'); return false; }
            if (valorAtributo('txtCodBarrasBus') == '') { alert('SELECCIONE CODIGO ANTERIOR'); return false; }
            break;
        case 'listProcedimientosDetalleEditar':
            if (valorAtributo('cmbCantidadProcedimientoEditar') == '') { alert('NO HA SELECCIONADO UNA CANTIDAD'); return false; }
            if (valorAtributo('cmbNecesidadProcedimientoEditar') == '') { alert('NO HA SELECCIONADO UNA NECESIDAD'); return false; }
            if (valorAtributo('cmbProcedimientoDiagnotiscoEditar') == '') { alert('NO HA SELECCIONADO UN DIAGNOSTICO'); return false; }
            if (valorAtributo('lblIdProcedimientoEditar') == '') { alert('NO HA SELECCIONADO UN PROCEDIMIENTO'); return false; }
            break;
        case 'modificarEstadoCita':
            if (valorAtributo('lblIdCitaEdit') == '') { alert('NO HA SELECCIONADO UN PACIENTE'); return false; }
            if (valorAtributo('cmbEstadoCitaEdit') == '') { alert('NO HA SELECCIONADO ESTADO'); return false; }
            if (valorAtributo('cmbMotivoConsultaEdit') == '') { alert('NO HA SELECCIONADO MOTIVO'); return false; }
            if ((valorAtributo('cmbEstadoCitaEdit') == 'L' || valorAtributo('cmbEstadoCitaEdit') == 'R') &&
                valorAtributo('cmbMotivoConsultaClaseEdit') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }

            break;


        case 'modificarEstadoCitaCuenta':
            if (valorAtributo('lblIdCitaEdit') == '') { alert('NO HA SELECCIONADO UN PACIENTE'); return false; }
            if (valorAtributo('cmbEstadoCitaEdit') == '') { alert('NO HA SELECCIONADO ESTADO'); return false; }
            if (valorAtributo('cmbMotivoConsultaEdit') == '') { alert('NO HA SELECCIONADO MOTIVO'); return false; }
            if ((valorAtributo('cmbEstadoCitaEdit') == 'L' || valorAtributo('cmbEstadoCitaEdit') == 'R') &&
                valorAtributo('cmbMotivoConsultaClaseEdit') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }

            break;


        case 'modificarEstadoCitaPrefactura':

            if (valorAtributo('lblIdCitaEdit') == '') { alert('NO HA SELECCIONADO UN PACIENTE'); return false; }
            if (valorAtributo('cmbEstadoCitaEdit') == '') { alert('NO HA SELECCIONADO ESTADO'); return false; }
            if (valorAtributo('cmbMotivoConsultaEdit') == '') { alert('NO HA SELECCIONADO MOTIVO'); return false; }
            if ((valorAtributo('cmbEstadoCitaEdit') == 'L' || valorAtributo('cmbEstadoCitaEdit') == 'R') &&
                valorAtributo('cmbMotivoConsultaClaseEdit') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }
            if (!confirm('SI CAMBIA EL ESTADO DE LA CITA, SE ELIMINARA LA PREFACTURA\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'modificarEstadoCitaPrefactura':
            if (valorAtributo('txtEstadoFacturaVentanitaAgenda') == 'F') { alert('NO SE PUEDE ELIMINAR. LA FACTURA SE ENCUENTRA FINALIZA'); return false; }
            if (valorAtributo('lblIdCitaEdit') == '') { alert('NO HA SELECCIONADO UN PACIENTE'); return false; }
            if (valorAtributo('cmbEstadoCitaEdit') == '') { alert('NO HA SELECCIONADO ESTADO'); return false; }
            if (valorAtributo('cmbMotivoConsultaEdit') == '') { alert('NO HA SELECCIONADO MOTIVO'); return false; }
            if ((valorAtributo('cmbEstadoCitaEdit') == 'L' || valorAtributo('cmbEstadoCitaEdit') == 'R') &&
                valorAtributo('cmbMotivoConsultaClaseEdit') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }
            if (!confirm('SI CAMBIA EL ESTADO DE LA CITA, SE ELIMINARA LA PREFACTURA\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'agregarIncapacidad':
            if (valorAtributo('txtDiasIncapacidad') == '') { alert('NO HA SELECCIONADO LOS DIAS DE INCAPACIDAD'); return false; }
            if (valorAtributo('txtFechaIncaInicio') == '') { alert('NO HA SELECCIONADO LA FECHA DESDE'); return false; }
            //if (valorAtributo('txtFechaIncaFin') == '') { alert('NO HA SELECCIONADO LA FECHA HASTA'); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert('NO HA SELECCIONADO EL DOCUMENTO'); return false; }

            break;

        case 'agregarEducacionPaciente':
            if (valorAtributo('lblIdDocumento') == '') { alert('NO HA SELECCIONADO EL DOCUMENTO'); return false; }
            if (valorAtributo('txtObservacionesEducacion').trim() == '') { alert('SE DEBE DILIGENCIAR EL TEXTO \n DE EDUCACION AL PACIENTE'); return false; }
            break;
        case 'editaAsistenciaPaciente':
            if (valorAtributo('lblIdCitaEdit') == '') { alert('NO HA SELECCIONADO UN PACIENTE'); return false; }
            break;


        case 'guardarSeguimientoTarea':
            if (valorAtributo('lblIdEstadoTareaEdita') == '0') { alert('LA TAREA DEBE ESTAR EJECUTADA'); return false; }
            if (valorAtributo('cmbCumple') == '') { alert('SELECCIONE SI CUMPLE'); return false; }
            if (valorAtributo('txtSeguimiento') == '') { alert('DILIGENCIE EL SEGUIMIENTO'); return false; }
            break;

        case 'guardarSiguienteTarea':

            if (valorAtributo('txtTarea') == '') { alert('DILIGENCIE LA TAREA'); return false; }
            if (valorAtributo('txtFechaTarea') == '') { alert('INGRESE LA FECHA DE ENTREGA'); return false; }
            if (valorAtributo('cmbTareaResponsable') == '') { alert('SELECCIONE AL RESPONSABLE DE LA TAREA'); return false; }

            break;

        case 'eliminarDescuento':
            if (valorAtributo('lblIdArtDescuento') == '') { alert('SELECCIONE UN ARTICULO'); return false; }
            if (valorAtributo('lblSerialDescuento') == '') { alert('NO ESTA EL SERIAL DEL ARTICULO'); return false; }
            break;
        case 'crearnotificacionPersona':
            if (valorAtributo('lblIdNotificacion') == '') { alert('SELECCIONE LA NOTIFICACION'); return false; }
            break;
        case 'crearDescuento':
            if (valorAtributo('txtDescuentoArt') == '') { alert('FALTA INGRESAR EL VALOR DEL DESCUENTO'); return false; }
            if (valorAtributo('txtSerial') == '') { alert('FALTA EL SERIAL DEL ARTICULO'); return false; }

            break;
        case 'traerTrEgresos':
            if (valorAtributo('txtDocEgreso') == '') { alert('FALTA INGRESAR ID DE DOCUMENTO DE SALIDA'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO DE ENTRADA'); return false; }

            break;
        case 'eliminarGrupoUsuario':
            if (valorAtributo('lblIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA SELECCIONAR UN USUARIO'); return false; }

            break;
        case 'crearGrupoUsuario':
            if (valorAtributo('cmbIdGrupo') == '') { alert('FALTA SELECCIONAR OPCION'); return false; }
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA SELECCIONAR UN ROL'); return false; }
            break;

        case 'agregarPermisoPerfil':
            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdOpcion') == '') { alert('FALTA SELECCIONAR UNA OPCION DEL SISTEMA'); return false; }
            break;

        case 'agregarEspProfesional':
            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR UNA ESPECIALIDAD'); return false; }
            if (valorAtributo('txtIdGrupo') == '') { alert('FALTA SELECCIONAR PROFESIONAL'); return false; }
            break;

        case 'eliminarProfesionalEspecialidad':
            if (valorAtributo('lblIdGrupo') == '') { alert('FALTA SELECCIONAR PROFESIONAL'); return false; }
            break;

        case 'agregarSede':
            if (valorAtributo('txtIdentificacion') == '') { alert('SELECCIONE UN PROFESIONAL'); return false; }
            if (valorAtributo('cmbIdSede') == '') { alert('SELECCIONE UNA SEDE'); return false; }
            break;

        case 'agregarSede':
            if (valorAtributo('txtIdentificacion') == '') { alert('SELECCIONE UN PROFESIONAL'); return false; }
            if (valorAtributo('cmbIdSede') == '') { alert('SELECCIONE UNA SEDE DE LA TABLA'); return false; }
            break;

        case 'modificarUsuario':
            if (valorAtributo('txtPNombre') == '') { alert('FALTA PRIMER NOMBRE'); return false; }
            if (valorAtributo('txtPApellido') == '') { alert('FALTA PRIMER APELLIDO'); return false; }
            if (valorAtributo('txtNomPersonal') == '') { alert('FALTA NOMBRE COMPLETO'); return false; }
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION'); return false; }
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA TIPO DE IDENTIFICACION'); return false; }
            if (valorAtributo('txtUSuario') == '') { alert('FALTA NOMBRE DE USUARIO'); return false; }
            if (valorAtributo('cmbIdTipoProfesion') == '') { alert('FALTA PROFESION'); return false; }
            if (valorAtributo('cmbSede') == '') { alert('FALTA LA SEDE'); return false; }

            break;
        case 'crearUsuario':
            if (valorAtributo('txtPNombre') == '') { alert('FALTA PRIMER NOMBRE'); return false; }
            if (valorAtributo('txtPApellido') == '') { alert('FALTA PRIMER APELLIDO'); return false; }
            if (valorAtributo('txtNomPersonal') == '') { alert('FALTA NOMBRE COMPLETO'); return false; }
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION'); return false; }
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA TIPO DE IDENTIFICACION'); return false; }
            if (valorAtributo('txtUSuario') == '') { alert('FALTA NOMBRE DE USUARIO'); return false; }
            if (valorAtributo('cmbIdTipoProfesion') == '') { alert('FALTA PROFESION'); return false; }
            if (valorAtributo('cmbSede') == '') { alert('FALTA SEDE PRINCIPAL'); return false; }

            break;
        case 'modificarFechaDocumentoInventario':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR DOCUMENTO'); return false; }
            if (valorAtributo('lblIdEstado') == '0') { alert('EL DOCUMENTO DEBE ESTAR FINALIZADO'); return false; }
            if (valorAtributo('lblNaturaleza') != 'I') { alert('EL DOCUMENTO DEBE SER DE ENTRADA'); return false; }
            if (valorAtributo('lblIdBodega') != '1') { alert('LA BODEGA DEBE SER FARMACIA'); return false; }
            if (valorAtributo('txtFechaModifica') == '') { alert('FALTA FECHA'); return false; }

            break;
        case 'adicionarMedicamentoPlan':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('txtIdPrecioArticulo') == '') { alert('FALTA SELECCIONAR PRECIO'); return false; }
            break;
        case 'modificarMedicamentoPlan':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('txtIdPrecioArticulo') == '') { alert('FALTA SELECCIONAR PRECIO'); return false; }
            break;
        case 'eliminarMedicamentoPlan':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            break;

        case 'adicionarExcepcion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA SELECCIONAR EL PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtCantidadExcepcion') == '') { alert('FALTA SELECCIONAR LA CANTIDAD'); return false; }
            if (valorAtributo('cmbmes') == '') { alert('FALTA SELECCIONAR MES'); return false; }
            if (valorAtributo('cmbyear') == '') { alert('FALTA SELECCIONAR AÑO'); return false; }

            break;
        case 'modificarExcepcion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA SELECCIONAR EL PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtCantidadExcepcion') == '') { alert('FALTA SELECCIONAR LA CANTIDAD'); return false; }
            if (valorAtributo('cmbmes') == '') { alert('FALTA SELECCIONAR MES'); return false; }
            if (valorAtributo('cmbyear') == '') { alert('FALTA SELECCIONAR AÑO'); return false; }

            break;
        case 'eliminarExcepcion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA SELECCIONAR EL PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtCantidadExcepcion') == '') { alert('FALTA SELECCIONAR LA CANTIDAD'); return false; }
            if (valorAtributo('cmbmes') == '') { alert('FALTA SELECCIONAR MES'); return false; }
            if (valorAtributo('cmbyear') == '') { alert('FALTA SELECCIONAR AÑO'); return false; }

            break;
        case 'adicionarLiquidacion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR UN PLAN'); return false; }
            if (valorAtributo('cmbOrdenProcedimiento') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcentaje') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLateralidad') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'eliminarLiquidacion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR UN PLAN'); return false; }
            if (valorAtributo('cmbOrdenProcedimiento') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcentaje') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLateralidad') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'modificaLiquidacion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR UN PLAN'); return false; }
            if (valorAtributo('cmbOrdenProcedimiento') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcentaje') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLateralidad') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'adicionarProcedimientoLiquidacion':
            if (valorAtributo('txtIdProcedimientoLiquida') == '') { alert('FALTA SELECCIONAR EL ID DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbOrdenProcedimientto') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcen') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLate') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigentte') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'eliminarLiquidacionProcedimiento':
            if (valorAtributo('txtIdProcedimientoLiquida') == '') { alert('FALTA SELECCIONAR EL ID DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbOrdenProcedimientto') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcen') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLate') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigentte') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'modificaLiquidacionProcedimiento':
            if (valorAtributo('txtIdProcedimientoLiquida') == '') { alert('FALTA SELECCIONAR EL ID DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbOrdenProcedimientto') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcen') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLate') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigentte') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'eliminarLiquidacion':
            if (valorAtributo('txtIdProcedimientoLiquida') == '') { alert('FALTA SELECCIONAR EL ID DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbOrdenProcedimientto') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcen') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLate') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigentte') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'modificaLiquidacion':
            if (valorAtributo('txtIdProcedimientoLiquida') == '') { alert('FALTA SELECCIONAR EL ID DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbOrdenProcedimientto') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcen') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLate') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigentte') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;
        case 'adicionarRangoPlan':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA SELECCIONAR EL TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA SELECCIONAR EL ID DEL RANGO'); return false; }
            if (valorAtributo('txtCopago') == '') { alert('FALTA DILIGENCIAR EL COPAGO'); return false; }
            break;

        case 'eliminaRangoPlan':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA SELECCIONAR EL TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA SELECCIONAR EL ID DEL RANGO'); return false; }
            if (valorAtributo('txtCopago') == '') { alert('FALTA DILIGENCIAR EL COPAGO'); return false; }
            break;

        case 'modificaRangoPlan':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA SELECCIONAR EL TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA SELECCIONAR EL ID DEL RANGO'); return false; }
            if (valorAtributo('txtCopago') == '') { alert('FALTA DILIGENCIAR EL COPAGO'); return false; }
            break;


        case 'modificaTransaccionSerial':
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR TRANSACCION'); return false; }
            if (valorAtributo('txtPrecioVenta') == '') { alert('FALTA PRECIO DE VENTA'); return false; }
            break;

        case 'crearTransaccionDevHG':
            if (valorAtributo('txtFV') == '') { alert('SELECCIONE UN LOTE'); return false; }
            break;
        case 'eliminarLiquidacionCirugia':
            if (valorAtributo('lblIdLiquidacion') == '') { alert('SELECCIONE UNA LIQUIDACION PARA ESTA CUENTA'); return false; }
            break;
        case 'crearLiquidacionCirugia':
            //if(valorAtributo('lblIdEstadoFactura') !='A' ){ alert('EL ESTADO DE LA CUENTA DEBE ESTAR EN ABIERTA');  return false; }
            //if(valorAtributo('lblIdLiquidacion') !='' ){ alert('YA EXISTE UNA LIQUIDACION PARA ESTA CUENTA');  return false; }			  
            //if(valorAtributo('cmbIdAnestesiologo') =='' ){ alert('FALTA ANESTESIOLOGO ');  return false; }	
            if (valorAtributo('cmbIdTipoAnestesia') == '') { alert('FALTA TIPO ANESTESIA'); return false; }
            if (valorAtributo('cmbIdFinalidad') == '') { alert('FALTA FINALIDAD'); return false; }
            if (valorAtributo('cmbIdTipoAtencion') == '') { alert('FALTA TIPO ATENCION'); return false; }

            if (valorAtributo('cmbIdTipoAnestesia') == '2' && valorAtributo('chk_Anestesiologo') == 'true') { alert('TIPO ANESTESIA LOCAL NO PUEDE TENER SELECCIONADO ANESTESIOLOGO'); return false; }
            if (valorAtributo('chk_Anestesiologo') == 'true' && valorAtributo('cmbIdAnestesiologo') == '') { alert('FALTA ANESTESIOLOGO '); return false; }


            break;

        case 'crearLiquidacionCirugiaContratadas':

            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA'); return false; }
            if (valorAtributo('lblIdPlan') == '') { alert('FALTA EL PLAN'); return false; }
            if (valorAtributo('lblIdCita') == '') { alert('FALTA SELECCIONAR CITA DE ADMISION'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }

            break;


        case 'eliminarLiquidacionCirugiaContratadas':

            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA'); return false; }
            if (valorAtributo('lblIdCita') == '') { alert('FALTA SELECCIONAR CITA DE ADMISION'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }


            break;

        case 'editarProcedimientoLiquidacionCirugiaContratadas':

            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA'); return false; }
            if (valorAtributo('lblIdCita') == '') { alert('FALTA SELECCIONAR CITA DE ADMISION'); return false; }
            if (valorAtributo('lblIdProcedimientoLiquidacionVentanita') == '') { alert('FALTA PROCEDIMIENTO'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }



            break;


        case 'modificarLiquidacionCirugia':
            //if(valorAtributo('lblIdEstadoCuenta') !='A' ){ alert('EL ESTADO DE LA CUENTA DEBE ESTAR EN ABIERTA');  return false; }
            if (valorAtributo('lblIdLiquidacion') == '') { alert('DEBE EXISTIR Y SELECCIONAR UNA LIQUIDACION'); return false; }
            if (valorAtributo('cmbIdTipoAnestesia') == '') { alert('FALTA TIPO ANESTESIA'); return false; }
            if (valorAtributo('cmbIdFinalidad') == '') { alert('FALTA FINALIDAD'); return false; }
            if (valorAtributo('cmbIdTipoAtencion') == '') { alert('FALTA TIPO ATENCION'); return false; }

            if (valorAtributo('cmbIdTipoAnestesia') == '2' && valorAtributo('chk_Anestesiologo') == 'true') { alert('TIPO ANESTESIA LOCAL NO PUEDE TENER SELECCIONADO ANESTESIOLOGO'); return false; }
            if (valorAtributo('chk_Anestesiologo') == 'true' && valorAtributo('cmbIdAnestesiologo') == '') { alert('FALTA ANESTESIOLOGO '); return false; }

            break;

        case 'priorizarEvento':
            if (valorAtributo('txtFechaInicia') == '') { alert('FALTA FECHA INICIO'); return false; }
            if (valorAtributo('txtFechaEntrega') == '') { alert('FALTA FECHA ENTREGA'); return false; }
            if (valorAtributo('cmbIdIdResponsable') == '') { alert('FALTA RESPONSABLE DE '); return false; }
            if (valorAtributo('txtPriorizacion') == '') { alert('FALTA PRIORIZACION'); return false; }
            break;
        case 'adicionaDescuentoFactura':

            if (valorAtributo('txtValorDescuentoCuenta') == '') { alert('VALOR DEL DESCUENTO NO PUEDE SER VACIO.'); return false; }
            if (valorAtributo('cmbIdQuienAutoriza') == '') { alert('FALTA QUIEN AUTORIZA.'); return false; }
            if (valorAtributo('cmbIdMotivoAutoriza') == '') { alert('FALTA MOTIVO.'); return false; }

            if (parseInt(valorAtributo('txtValorDescuentoCuenta')) <= 0) {
                alert('EL DESCUENTO DEBE SER MAYOR A CERO.');
                return false;
            }

            if (parseInt(valorAtributo('txtValorDescuentoCuenta')) > parseInt(valorAtributo('lblValorBaseDescuento'))) {
                alert('EL DESCUENTO NO PUEDE SER MAYOR AL TOTAL.');
                return false;
            }

            break;

        case 'revertirDescuentoFactura':
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA FACTURA PARA REVERTIR DESCUENTO.'); return false; }
            break;

        case 'crearAdmisionFactura':

            if (valorAtributo('lblIdCita') != 'sin_cita') {
                if (valorAtributo('lblIdCita') == '') { alert('FALTA SELECCIONAR UNA CITA DE AGENDA.'); return false; }
            }

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('txtIdDx') == '') { alert('FALTA DIAGNOSTICO'); return false; }
            if (valorAtributo('txtIPSRemite') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('FALTA SELECCIONAR ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbTipoAdmision') == '') { alert('FALTA TIPO ADMISION'); return false; }
            if (valorAtributo('cmbIdProfesionales') == '') { alert('FALTA PROFESIONAL'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR EPS'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            //if (valorAtributo('txtNoAutorizacion') == '' || isNaN(valorAtributo('txtNoAutorizacion'))) { alert('FALTA NUMERO AUTORIZACION O EL CAMPO DEBE SER NUMERICO'); return false; }
            break;

        case 'crearPlantilla':
            if (valorAtributo('txtAnCol1') == '' || isNaN(valorAtributo('txtAnCol1'))) { alert('NO SE A INGRESADO NINGUN VALOR, EL VALOR DEBE SER NUMERICO'); return false; }
            break

        case 'modificarPlantilla':
            if (valorAtributo('txtAnCol1') == '' || isNaN(valorAtributo('txtAnCol1'))) { alert('NO SE A INGRESADO NINGUN VALOR, EL VALOR DEBE SER NUMERICO'); return false; }

            break

        case 'crearSoloFactura':
            if (valorAtributo('txtIdDx') == '' && valorAtributo('txtDiagnostico') == 'S') { alert('FALTA DIAGNOSTICO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR EPS'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            if (valorAtributo('txtNoAutorizacion') == '') { alert('FALTA NUMERO AUTORIZACION O EL CAMPO DEBE SER NUMERICO'); return false; }

            break;


        case 'crearFacturaDocumentoVentas':

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            if (valorAtributo('cmbBodegaVentas') == '') { alert('FALTA SELECCIONAR BODEGA'); return false; }
            if (valorAtributo('cmbIdProfesionales') == '') { alert('FALTA SELECCIONAR PROFESIONAL'); return false; }

            break;

        case 'modificarFacturaDocumentoVentas':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            if (valorAtributo('cmbBodegaVentas') == '') { alert('FALTA SELECCIONAR BODEGA'); return false; }
            if (valorAtributo('cmbIdProfesionales') == '') { alert('FALTA SELECCIONAR PROFESIONAL'); return false; }
            if (!confirm('SI MODIFICA LA FACTURA SE ELIMINARAN TODOS LOS ELEMENTOS AGREGADOS\nDESEA CONTINUAR?')) { return false; }

            break;


        case 'traerElementosCodigoBarrasVentas':

            if (valorAtributo('txtIdCodigoBarrasVentas') == '') { alert('EL CODIGO NO DEBE SER VACIO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE AGREGAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE AGREGAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE AGREGAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }

            break;


        case 'crearTransaccionVentasOtro':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('cmbIdArticuloOtros') == '') { alert('SELECCIONE UN ARTICULO'); return false; }
            if (valorAtributo('txtObservacionVentasOtros') == '') { alert('FALTA OBSERVACION'); return false; }
            if (valorAtributo('cmbCantidadVentasOtros') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('txtValorUnitarioOtros') == '') { alert('FALTA VALOR'); return false; }

            break;


        case 'eliminarTransaccionVentas':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('lblIdTransaccionVentasVentanita') == '') { alert('FALTA SELECCIONAR TRANSACCION'); return false; }

            break;


        case 'modificarTransaccionVentas':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtValorDescuentoVentanita') == '') { alert('FALTA VALOR DESCUENTO'); return false; }
            if (parseInt(valorAtributo('txtValorDescuentoVentanita')) >= parseInt(valorAtributo('lblValorBaseVentanita'))) { alert('EL DESCUENTO NO PUEDE SER MAYOR AL VALOR DEL ARTICULO.'); return false; }


            break;


        case 'revertirTransaccionVentas':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }

            break;


        case 'modificarTransaccionVentasOtro':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasOtroVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtValorDescuentoOtroVentanita') == '') { alert('FALTA VALOR DESCUENTO'); return false; }
            if (parseInt(valorAtributo('txtValorDescuentoOtroVentanita')) >= parseInt(valorAtributo('lblValorBaseOtroVentanita'))) { alert('EL DESCUENTO NO PUEDE SER MAYOR AL VALOR DEL ARTICULO.'); return false; }
            break;

        case 'revertirTransaccionVentasOtro':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasOtroVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }

            break;

        case 'eliminarArticuloVentasOtro':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasOtroVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }

            break;

        case 'eliminarFacturaDocumentoOptica':

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ELIMINAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE ELIMINAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE ELIMINAR ,LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE ELIMINAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ELIMINAR LA FACTURA?')) { return false; }

            break;

        case 'eliminarSoloFactura':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ELIMINAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE ELIMINAR LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE ELIMINAR LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ELIMINAR LA FACTURA?')) { return false; }
            break;

        case 'modificarSoloFactura':
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA PLAN DE CONTRATACION.'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA PARA MODIFICAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA ANULADA.'); return false; }

            if (!confirm('SI MODIFICA LA FACTURA SE ELIMINARAN TODOS LOS ELEMENTOS AGREGADOS\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'modificarFormaPago':
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'P') { alert('EL ESTADO DE LA FACTURA NO PERMITE MODIFICAR'); return false; }
            break;

        case 'crearReciboInicial':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdReciboInicial') != '') { alert('NO PUEDE CREAR ABONO INICIAL PORQUE YA EXISTE.'); return false; }
            if (valorAtributo('txtValorInicial') == '') { alert('FALTA VALOR.'); return false; }
            if (parseInt(valorAtributo('txtValorInicial')) >= parseInt(valorAtributo('lblValorCubierto'))) { alert('EL VALOR NO PUEDE SER MAYOR A VALOR CUBIERTO.'); return false; }
            if (parseInt(valorAtributo('txtValorInicial')) <= 0) { alert('EL VALOR DEBE SER MAYOR A 0.'); return false; }
            //if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            //if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            break;


        case 'modificarFacturaContado':

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdReciboInicial') == '') { alert('NO PUEDE MODIFICAR, LA FACTURA YA ES DE CONTADO.'); return false; }
            //if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            //if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            break;

        case 'modificarFactura':
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA SELECCIONAR TIPO DE DOCUMENTO.'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('txtNoAutorizacion') == '') { alert('FALTA NUMERO DE AUTORIZACION.'); return false; }
            if (valorAtributo('cmbIdProfesionalesFactura') == '') { alert('FALTA PROFESIONAL FACTURA.'); return false; }
            if (valorAtributo('txtFechaIngreso') == '') { alert('FALTA FECHA DE INGRESO.'); return false; }
            if (valorAtributo('txtNacimiento') == '') { alert('FALTA FECHA DE NACIMIENTO.'); return false; }
            if (valorAtributoIdAutoCompletar('txtMunicipio') == '') { alert('FALTA MUNICIPIO.'); return false; }
            if (valorAtributo('lblIdPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            break;


        case 'crearReciboCartera':

            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            //if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('LA FACTURA NO SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdTipoPago') == '1') { alert('LA FACTURA DEBE SER DE TIPO CREDITO.'); return false; }
            if (valorAtributo('txtValorUnidadRecibo') == '') { alert('FALTA VALOR.'); return false; }
            if (valorAtributo('cmbIdConceptoRecibo') == '') { alert('FALTA CONCEPTO.'); return false; }

            if (parseInt(valorAtributo('txtValorUnidadRecibo')) > parseInt(valorAtributo('lblSaldoFactura'))) { alert('EL VALOR NO PUEDE SER MAYOR AL SALDO.'); return false; }

            break;

        case 'pendienteFactura':
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA'); return false; }
            break;


        case 'crearCanastaTransaccion':
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblCodigoCanasta') == '') { alert('FALTA SELECCIONAR UNA CANASTA'); return false; }
            break;

        case 'crearCanastaTrasladoBodega':
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblCodigoCanasta') == '') { alert('FALTA SELECCIONAR UNA CANASTA'); return false; }
            break;

        case 'crearViaArticulo':
            if (valorAtributo('cmbViaArticulo') == '') { alert('SELECCIONE VIA DE ADMINISTRACION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            break;
        case 'eliminarViaArticulo':
            if (valorAtributo('txtIdViaArticulo') == '') { alert('SELECCIONE VIA DE ADMINISTRACION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            break;
        case 'guardaConciliacion':
            if (valorAtributo('cmbExiste') == '') { alert('SELECCIONE SI EXISTE ITERACCION'); return false; }
            if (valorAtributo('txtConciliacionDescripcion') == '') { alert('FALTA DESCRIPCION DE ITERACCION'); return false; }
            break;


        case 'eliminarRemision':
            if (valorAtributo('lblIdAdmision') != "NO_HC") {

                if (valorAtributo('lblIdDocumento') == '') { alert('NO HA SELECCIONADO UN FOLIO'); return false; }

                if (valorAtributo('txtIdEsdadoDocumento') == '1') { alert('EL ESTADO DEL FOLIO NO PERMITE ELIMINAR'); return false; }
                if (valorAtributo('txtIdEsdadoDocumento') == '9') { alert('EL ESTADO DEL FOLIO NO PERMITE ELIMINAR'); return false; }
                if (valorAtributo('txtIdEsdadoDocumento') == '8') { alert('EL ESTADO DEL FOLIO NO PERMITE ELIMINAR'); return false; }

            }


            break;

        case 'listRemision':


            if (valorAtributo('lblIdAdmision') != "NO_HC") {

                if (valorAtributo('lblIdDocumento') == '') { alert('NO HA SELECCIONADO UN FOLIO'); return false; }

                if (valorAtributo('txtIdEsdadoDocumento') == '1') { alert('EL ESTADO DEL FOLIO NO PERMITE CREAR'); return false; }
                if (valorAtributo('txtIdEsdadoDocumento') == '9') { alert('EL ESTADO DEL FOLIO NO PERMITE CREAR'); return false; }
                if (valorAtributo('txtIdEsdadoDocumento') == '8') { alert('EL ESTADO DEL FOLIO NO PERMITE CREAR'); return false; }

            }

            if (valorAtributo('cmbTipoEvento') == '') { alert('FALTA TIPO DE EVENTO'); return false; }
            if (valorAtributo('cmbServicioSolicita') == '') { alert('FALTA SERVICIO QUE SOLICITA'); return false; }
            if (valorAtributo('cmbServicioParaCual') == '') { alert('FALTA SERVICIO PARA EL CUAL SE SOLICITA'); return false; }
            if (valorAtributo('cmbPrioridad') == '') { alert('FALTA PRIORIDAD'); return false; }
            if (valorAtributo('cmbNivelRemis') == '') { alert('FALTA NIVEL'); return false; }

            if (valorAtributo('cmbTipoDocResponsable') == '') { alert('FALTA TIPO DOCUMENTO RESPONSABLE'); return false; }
            if (valorAtributo('txtDocResponsable') == '') { alert('FALTA DOCUMENTO DEL RESPONSABLE'); return false; }
            if (valorAtributo('txtPrimerApeResponsable') == '') { alert('FALTA PRIMER APELLIDO RESPONSABLE'); return false; }

            if (valorAtributo('txtPrimerNomResponsable') == '') { alert('FALTA PRIMER NOMBRE RESPONSABLE'); return false; }
            if (valorAtributo('txtTelefonoResponsable') == '') { alert('FALTA TELEFONO RESPONSABLE'); return false; }
            if (valorAtributo('txtDireccionResponsable') == '') { alert('FALTA DIRECCION RESPONSABLE'); return false; }
            if (valorAtributo('txtDeparResponsable') == '') { alert('FALTA DEPARTAMENTO RESPONSABLE'); return false; }
            if (valorAtributo('txtMunicResponsable') == '') { alert('FALTA MUNICIPIO RESPONSABLE'); return false; }

            if (valorAtributo('txt_InformacionRemision') == '') { alert('FALTA INFORMACION CLINICA RELEVANTE'); return false; }
            if (valorAtributo('cmbIdProfesionalRemision') == '') { alert('FALTA PROFESIONAL REALIZA'); return false; }


            if (valorAtributo('cmbVerificacionPaciente') == '') { alert('FALTA VERIFICACION PACIENTE'); return false; }
            if (valorAtributo('cmbFormatoDiligenciado') == '') { alert('FALTA FORMATO DILIGENCIADO'); return false; }
            if (valorAtributo('cmbResumenHc') == '') { alert('FALTA RESUMEN HISTORIA CLINICA'); return false; }
            if (valorAtributo('cmbDocumentoIdentidad') == '') { alert('FALTA DOCUMENTO IDENTIDAD'); return false; }
            if (valorAtributo('cmbImagenDiagnostica') == '') { alert('FALTA IMAGEN DIAGNOSTICA'); return false; }
            if (valorAtributo('cmbPacienteManilla') == '') { alert('FALTA PACIENTE MANILLA'); return false; }


            break;

        case 'listAntecedentesFarmacologicos':
            if (valorAtributo('lblIdDocumento') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('txtAntecedenteFarmacologico') == '') { alert('FALTA SELECCIONAR UN ANTECEDENTE'); return false; }
            break;

        case 'eliminaTransaccionDevolucion':
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR UNA TRANSACCION'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            break;
        case 'modificaTransaccionSolicitud':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            //if(valorAtributo('lblIdSolicitud') =='' ){ alert('FALTA SELECCIONAR UNA SOLICITUD');  return false; }								  						  						 
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL  DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            break;
        case 'crearTransaccionDevolucion':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblIdDevolucion') == '') { alert('FALTA SELECCIONAR UNA DEVOLUCION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL  DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            break;
        case 'crearTransaccionSolicitud':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblIdSolicitud') == '') { alert('FALTA SELECCIONAR UNA SOLICITUD'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('lblValorImpuesto') == '') { alert('FALTA CALCULAR EL IMPUESTO.'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL  DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            break;
        case 'eliminaTransaccionSolicitud':
            /* inventario */
            if (valorAtributo('lblIdAdmisionAgen') == '') {
                if (valorAtributo('cmbIdGastos') == 'S') {
                    asignaAtributo('lblIdAdmisionAgen', '0', 0);
                    return true;
                } else {
                    alert('FALTA SELECCIONAR PACIENTE CON ADMISION VALIDA');
                    return false;
                }
            }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL  DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            break;
        case 'eliminaSolicitud':
            /* hc */
            if (valorAtributo('lblIdAdmisionAgen') == '') { alert('FALTA SELECCIONAR PACIENTE CON ADMISION VALIDA'); return false; }
            /*if(valorAtributo('txtIdArticuloSolicitudes') =='' ){ alert('FALTA SELECCIONAR UN ARTICULO');  return false; }*/
            if (valorAtributo('txtIdArticuloSolicitudes') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidadSolicitud') == '') { alert('FALTA CANTIDAD'); return false; }
            break;
        case 'modificaSolicitud':
            if (valorAtributo('lblIdAdmisionAgen') == '') { alert('FALTA SELECCIONAR PACIENTE CON ADMISION VALIDA'); return false; }
            if (valorAtributo('txtIdArticuloSolicitudes') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidadSolicitud') == '') { alert('FALTA CANTIDAD'); return false; }
            break;
        case 'crearSolicitud':
            if (valorAtributo('lblIdAdmisionAgen') == '') { alert('FALTA SELECCIONAR PACIENTE CON ADMISION VALIDA'); return false; }
            if (valorAtributo('txtIdArticuloSolicitudes') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidadSolicitud') == '') { alert('FALTA CANTIDAD'); return false; }

            var ids = jQuery("#listGrillaSolicitudFarmacia").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listGrillaSolicitudFarmacia").getRowData(c);
                if (datosRow.ID_ARTICULO == valorAtributoIdAutoCompletar('txtIdArticuloSolicitudes')) {
                    alert('Atencion!!!! \nYA EXISTE ESTE ARTICULO EN EL LISTADO');
                    return false;
                }
            }
            break;
        case 'nuevoDocumentoInventarioBodegaConsumo':
            if (valorAtributo('cmbIdGastos') == 'N') {
                if (valorAtributo('lblIdAdmisionAgen') == '') { alert('FALTA SELECCIONAR PACIENTE CON ADMISION VALIDA'); return false; }
            } else {
                asignaAtributo('lblIdAdmisionAgen', '0', 0);
            }
            if (valorAtributo('cmbIdBodega') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            break;
        case 'traerArticulo':
            /*CONSUMO DE ARTICULO A HOJA DE GASTOS*/
            //if(valorAtributo('lblIdDocumento') =='' ){ alert('FALTA SELECCIONAR UN DOCUMENTO CLINICO');  return false; }								  						  						 
            if (valorAtributo('lblIdAdmision') == '') { alert('FALTA SELECCIONAR UN PACIENTE CON UNA ADMISI�N V�LIDA'); return false; }
            if (valorAtributo('cmbNovedad') == '') { alert('FALTA SELECCIONAR UNA NOVEDAD'); return false; }
            if (valorAtributo('lblCostoHG') == '' || valorAtributo('lblCostoHG') == '0') { alert('FALTA CALCULAR EL COSTO DEL ARTICULO'); return false; }
            //if(valorAtributo('txtIdEsdadoDocumento') =='1' ){ alert('EL DOCUMENTO YA ESTA FINALIZADO');  return false; }	activar_condicion							  						  						 
            break;
        case 'traerUltimoArticulo':
            /*CONSUMO DE ULTIMO ARTICULO A HOJA DE GASTOS*/
            //if(valorAtributo('lblIdDocumento') =='' ){ alert('FALTA SELECCIONAR UN DOCUMENTO CLINICO');  return false; }								  						  						 
            if (valorAtributo('lblIdAdmision') == '') { alert('FALTA SELECCIONAR UN PACIENTE CON UNA ADMISI�N V�LIDA'); return false; }
            /* if(valorAtributo('cmbNovedad') =='' ){ alert('FALTA SELECCIONAR UNA NOVEDAD');  return false; }								  						  						  */
            if (valorAtributo('lblCostoHG') == '' || valorAtributo('lblCostoHG') == '0') { alert('FALTA CALCULAR EL COSTO DEL ARTICULO'); return false; }
            //if(valorAtributo('txtIdEsdadoDocumento') =='1' ){ alert('EL DOCUMENTO YA ESTA FINALIZADO');  return false; }	activar_condicion							  						  						 
            break;
        case 'eliminarArticuloCanasta':
            if (valorAtributo('lblIdDetalle') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            break;
        case 'adicionarArticuloCanasta':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('cmbCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            /* --- --- -- */
            var ids = jQuery("#listGrillaCanastaDetalle").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listGrillaCanastaDetalle").getRowData(c);
                if (datosRow.ID_ARTICULO == valorAtributoIdAutoCompletar('txtIdArticulo')) {
                    alert('YA EXISTE ESTE ARTICULO EN EL LISTADO');
                    return false;
                }
            }
            break;
        case 'modificarPlantillaCanasta':
            if (valorAtributo('txtNombreCanasta') == '') { alert('FALTA EL NOMBRE DE LA CANASTA'); return false; }
            if (valorAtributo('cmbTipoCanasta') == '') { alert('FALTA SELECCIONAR TIPO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            break;
        case 'crearPlantillaCanasta':
            if (valorAtributo('txtNombreCanasta') == '') { alert('FALTA EL NOMBRE DE LA CANASTA'); return false; }
            if (valorAtributo('cmbTipoCanasta') == '') { alert('FALTA SELECCIONAR TIPO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            break;
        case 'cerrarDocumentoDevolucion':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR DOCUMENTO'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            break;
        case 'cerrarDocumentoBodega':
            if (valorAtributo('cmbIdBodega') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR DOCUMENTO'); return false; }
            //if(valorAtributo('cmbIdTipoDocumento') =='' ){ alert('FALTA SELECCIONAR TIPO DE DOCUMENTO');  return false; }					
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            break;
        case 'eliminaTransaccion':
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR UNA TRANSACCION'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            break;
        case 'crearTransaccion':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('lblValorImpuesto') == '') { alert('FALTA CALCULAR EL VALOR DEL IMPUESTO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('txtValorU') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('txtPrecioVenta') == '') { alert('FALTA PRECIO DE VENTA'); return false; }
                if (valorAtributo('txtFechaVencimiento') == '') { alert('FALTA FECHA DE VENCIMIENTO '); return false; }
                if (valorAtributo('txtLote') == '') { alert('FALTA LOTE '); return false; }

                var lote = valorAtributo('txtLote');
            } else {
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('lblIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('cmbIdLote') == '') { alert('FALTA LOTE '); return false; }
                var lote = valorAtributo('cmbIdLote');
            }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }

            if (valorAtributo('cmbIdBodega') == '6') {
                var ids = jQuery("#listGrillaTransaccionDocumento").getDataIDs();
                for (var i = 0; i < ids.length; i++) {
                    var c = ids[i];
                    var datosRow = jQuery("#listGrillaTransaccionDocumento").getRowData(c);
                    if (datosRow.ID_ARTICULO == valorAtributoIdAutoCompletar('txtIdArticulo') && datosRow.LOTE == lote && datosRow.SERIAL == serial) {
                        alert('YA EXISTE ESTE ARTICULO EN EL LISTADO');
                        return false;
                    }
                }
            }

            break;


        case 'crearTransaccionVentas':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }


            break;



        case 'crearTransaccion_dos':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('lblValorImpuesto') == '') { alert('FALTA CALCULAR EL VALOR DEL IMPUESTO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('txtValorU') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('txtPrecioVenta') == '') { alert('FALTA PRECIO DE VENTA'); return false; }
                if ((valorAtributo('txtFechaVencimiento') == '') && (valorAtributo('txtLote') != '')) { alert('FALTA FECHA DE VENCIMIENTO '); return false; }
                if ((valorAtributo('txtFechaVencimiento') != '') && (valorAtributo('txtLote') == '')) { alert('FALTA LOTE '); return false; }
                var lote = decodeURIComponent(valorAtributo('txtLote'));
                var serial = valorAtributo('txtSerial');
            } else {
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('lblIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }


                var lote = decodeURIComponent(valorAtributo('txtIdLote_dos'));
                var serial = valorAtributo('txtSerial');
            }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }


            var ids = jQuery("#listGrillaTransaccionDocumento").getDataIDs();
            console.log('crearTransaccion_dos: ' + ids.length)
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listGrillaTransaccionDocumento").getRowData(c);

                console.log('tabla: ' + datosRow.ID_ARTICULO + ' - ' + datosRow.LOTE + ' - ' + datosRow.SERIAL);
                console.log('nuevo: ' + valorAtributo('txtIdArticulo_dos') + ' - ' + lote + ' - ' + serial);

                if (datosRow.ID_ARTICULO == valorAtributo('txtIdArticulo_dos') && datosRow.LOTE == lote && datosRow.SERIAL == serial) {



                    alert('YA EXISTE ESTE ARTICULO EN EL LISTADO, NO SE PUEDE AGREGAR');
                    return false;
                }
            }

            break;
        case 'crearTransaccionDevolucionCompra':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('lblValorImpuesto') == '') { alert('FALTA CALCULAR EL VALOR DEL IMPUESTO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('txtValorU') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('txtPrecioVenta') == '') { alert('FALTA PRECIO DE VENTA'); return false; }
                if ((valorAtributo('txtFechaVencimiento') == '') && (valorAtributo('txtLote') != '')) { alert('FALTA FECHA DE VENCIMIENTO '); return false; }
                if ((valorAtributo('txtFechaVencimiento') != '') && (valorAtributo('txtLote') == '')) { alert('FALTA LOTE '); return false; }
                var lote = valorAtributo('txtLote');
            } else {
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('lblIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                var lote = valorAtributo('lblIdLote');
            }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }

            /* --- --- -- */
            var ids = jQuery("#listGrillaTransaccionDocumento").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listGrillaTransaccionDocumento").getRowData(c);
                if (datosRow.ID_ARTICULO == valorAtributoIdAutoCompletar('txtIdArticulo') && datosRow.LOTE == lote) {
                    alert('YA EXISTE ESTE ARTICULO EN EL LISTADO');
                    return false;
                }
            }

            break;
        case 'creaSerialesTransaccion':
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO DEBE ESTAR ABIERTO'); return false; }
            break;

        case 'modificaTransaccion':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR UNA TRANSACCION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('txtFechaVencimiento') == '') { alert('FALTA FECHA DE VENCIMIENTO '); return false; }
                if (valorAtributo('txtLote') == '') { alert('FALTA LOTE '); return false; }
            } else {
                if (valorAtributo('lblIva') == '') { alert('FALTA IVA'); return false; }
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIdLote') == '') { alert('FALTA LOTE '); return false; }
            }
            if (valorAtributo('txtSerial') == '') { alert('ERROR AL MODIFICAR!!!  \nSI EL ARTICULO TIENE SERIAL, DEBE ESCRIBIRLO Y DARLE ENTER'); return false; }
            break;



        case 'modificaTransaccionTraslado':

            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR UNA TRANSACCION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('txtLote') == '') { alert('FALTA LOTE '); return false; }
            } else {
                if (valorAtributo('lblIva') == '') { alert('FALTA IVA'); return false; }
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIdLote') == '') { alert('FALTA LOTE '); return false; }
            }

            break;

        case 'modificaTransaccionOptica':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR UNA TRANSACCION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
            } else {
                if (valorAtributo('lblIva') == '') { alert('FALTA IVA'); return false; }
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIdLote') == '') { alert('FALTA LOTE'); return false; }
            }
            if (valorAtributo('txtSerial') == '') { alert('ERROR AL MODIFICAR!!!  \nSI EL ARTICULO TIENE SERIAL, DEBE ESCRIBIRLO Y DARLE ENTER'); return false; }
            break;
        case 'nuevoDocumentoInventarioBodega':
            if (valorAtributo('cmbIdBodega') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            if (valorAtributo('cmbIdTipoDocumento') == '') { alert('FALTA SELECCIONAR TIPO DE DOCUMENTO'); return false; }
            if (valorAtributo('txtIdTercero') == '') { alert('FALTA SELECCIONAR TERCERO'); return false; }
            if (valorAtributo('txtValorFlete') == '') { alert('FALTA VALOR DEL FLETE (CERO SI NO EXISTE)'); return false; }
            break;
        case 'crearDocumento':
            if (valorAtributo('cmbIdBodega') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            if (valorAtributo('cmbIdTipoDocumento') == '') { alert('FALTA SELECCIONAR TIPO DE DOCUMENTO'); return false; }
            //if(valorAtributo('cmbIdEstado') =='' ){ alert('FALTA SELECCIONAR EL ESTADO');  return false; }								 			  			  
            break;
        case 'modificaDocumento':
            if (valorAtributo('cmbIdBodega') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR DOCUMENTO'); return false; }
            if (valorAtributo('cmbIdTipoDocumento') == '') { alert('FALTA SELECCIONAR TIPO DE DOCUMENTO'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            break;
        case 'crearBodega':
            if (valorAtributo('txtDescripcion') == '') { alert('FALTA DESCRIPCION DE LA BODEGA'); return false; }
            if (valorAtributo('cmbIdEmpresa') == '') { alert('FALTA DESCRIPCION DE LA BODEGA'); return false; }
            if (valorAtributo('cmbIdCentroCosto') == '') { alert('FALTA DESCRIPCION DE LA BODEGA'); return false; }
            if (valorAtributo('cmbIdIdResponsable') == '') { alert('FALTA SELECCIONAR RESPONSABLE'); return false; }
            if (valorAtributo('cmbEstado') == '') { alert('FALTA SELECCIONAR ESTADO'); return false; }
            if (valorAtributo('cmbSWRestitucion') == '') { alert('FALTA SELECCIONAR RESTITUCION'); return false; }
            if (valorAtributo('txtAutorizacion') == '') { alert('FALTA AUTORIZACION RECIBIR COMPRAS'); return false; }
            if (valorAtributo('cmbSWRestriccionStock') == '') { alert('FALTA SELECCIONAR RESTRICCION STOCK'); return false; }
            if (valorAtributo('cmbIdBodegaTipo') == '') { alert('FALTA SELECCIONAR TIPO BODEGA'); return false; }
            if (valorAtributo('cmbSWConsignacion') == '') { alert('FALTA SELECCIONAR CONSIGNACION'); return false; }
            if (valorAtributo('cmbSWAprovechamiento') == '') { alert('FALTA SELECCIONAR APROVECHAMIENTO'); return false; }
            if (valorAtributo('cmbSWAfectaCosto') == '') { alert('FALTA SELECCIONAR AFECTA COSTO'); return false; }

            break;
        case 'modificaBodega':
            if (valorAtributo('cmbIdBodega') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            if (valorAtributo('txtDescripcion') == '') { alert('FALTA DESCRIPCION DE LA BODEGA'); return false; }
            if (valorAtributo('cmbIdEmpresa') == '') { alert('FALTA DESCRIPCION DE LA BODEGA'); return false; }
            if (valorAtributo('cmbIdCentroCosto') == '') { alert('FALTA DESCRIPCION DE LA BODEGA'); return false; }
            if (valorAtributo('cmbIdIdResponsable') == '') { alert('FALTA SELECCIONAR RESPONSABLE'); return false; }
            if (valorAtributo('cmbEstado') == '') { alert('FALTA SELECCIONAR ESTADO'); return false; }
            if (valorAtributo('cmbSWRestitucion') == '') { alert('FALTA SELECCIONAR RESTITUCION'); return false; }
            if (valorAtributo('txtAutorizacion') == '') { alert('FALTA AUTORIZACION RECIBIR COMPRAS'); return false; }
            if (valorAtributo('cmbSWRestriccionStock') == '') { alert('FALTA SELECCIONAR RESTRICCION STOCK'); return false; }
            if (valorAtributo('cmbIdBodegaTipo') == '') { alert('FALTA SELECCIONAR TIPO BODEGA'); return false; }
            if (valorAtributo('cmbSWConsignacion') == '') { alert('FALTA SELECCIONAR CONSIGNACION'); return false; }
            if (valorAtributo('cmbSWAprovechamiento') == '') { alert('FALTA SELECCIONAR APROVECHAMIENTO'); return false; }
            if (valorAtributo('cmbSWAfectaCosto') == '') { alert('FALTA SELECCIONAR AFECTA COSTO'); return false; }
            break;
        case 'modificaMedicamento':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtNombreArticulo') == '') { alert('FALTA NOMBRE DEL ARTICULO'); return false; }
            if (valorAtributo('cmbIdClase') == '') { alert('FALTA SELECCIONAR CLASE'); return false; }
            if (valorAtributo('cmbIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('cmbPresentacion') == '') { alert('FALTA SELECCIONAR PRESENTACION'); return false; }
            if (valorAtributo('txtConcentracion') == '') { alert('FALTA CONCENTRACION DEL MEDICAMENTO'); return false; }
            if (valorAtributo('cmbPOS') == '') { alert('FALTA SELECCIONAR POS'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            if (valorAtributo('txtATC') == '') { alert('FALTA CODIGO ATC'); return false; }
            if (valorAtributo('cmbAnatomofarmacologico') == '') { alert('FALTA ANATOMOFARMACOLOGICO'); return false; }
            if (valorAtributo('cmbPrincipioActivo') == '') { alert('FALTA PRINCIPIO ACTIVO'); return false; }
            if (valorAtributo('cmbFormaFarmacologica') == '') { alert('FALTA FORMA FARMACEUTICA'); return false; }
            if (valorAtributo('cmbIdUnidadAdministracion') == '') { alert('FALTA FACTOR DE CONVERSION'); return false; }
            break;
        case 'modificaArticulo':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtNombreArticulo') == '') { alert('FALTA NOMBRE DEL ARTICULO'); return false; }
            if (valorAtributo('cmbIdClase') == '') { alert('FALTA SELECCIONAR CLASE'); return false; }
            if (valorAtributo('cmbIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('cmbPresentacion') == '') { alert('FALTA SELECCIONAR TIPO'); return false; }
            //if(valorAtributo('cmbIdSubTipo') =='' ){ alert('FALTA SELECCIONAR SUBTIPO');  return false; }			  
            if (valorAtributo('cmbPOS') == '') { alert('FALTA SELECCIONAR POS'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            if (valorAtributo('cmbIdUnidadAdministracion') == '') { alert('FALTA FACTOR DE CONVERSION'); return false; }
            break;
        case 'crearMedicamento':
            if (valorAtributo('txtNombreArticulo') == '') { alert('FALTA NOMBRE DEL ARTICULO'); return false; }
            if (valorAtributo('cmbIdClase') == '') { alert('FALTA SELECCIONAR CLASE'); return false; }
            if (valorAtributo('cmbIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('cmbPresentacion') == '') { alert('FALTA SELECCIONAR PRESENTACION'); return false; }
            if (valorAtributo('txtIVA') == '') { alert('FALTA IVA'); return false; }
            if (valorAtributo('cmbIVAVenta') == '') { alert('FALTA IVA VENTA'); return false; }
            if (valorAtributo('cmbPOS') == '') { alert('FALTA SELECCIONAR POS'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            if (valorAtributo('txtATC') == '') { alert('FALTA CUM'); return false; }
            if (valorAtributo('cmbAnatomofarmacologico') == '') { alert('FALTA ANATOMOFARMACOLOGICO'); return false; }
            if (valorAtributo('cmbPrincipioActivo') == '') { alert('FALTA PRINCIPIO ACTIVO'); return false; }
            if (valorAtributo('cmbFormaFarmacologica') == '') { alert('FALTA FORMA FARMACEUTICA'); return false; }
            if (valorAtributo('cmbIdUnidadAdministracion') == '') { alert('FALTA FACTOR DE CONVERSION'); return false; }
            break;
        case 'crearArticulo':
            if (valorAtributo('txtNombreArticulo') == '') { alert('FALTA NOMBRE DEL ARTICULO'); return false; }
            if (valorAtributo('cmbIdClase') == '') { alert('FALTA SELECCIONAR CLASE'); return false; }
            if (valorAtributo('cmbIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('cmbPresentacion') == '') { alert('FALTA SELECCIONAR TIPO'); return false; }
            if (valorAtributo('txtIVA') == '') { alert('FALTA IVA'); return false; }
            if (valorAtributo('cmbIVAVenta') == '') { alert('FALTA IVA VENTA'); return false; }
            if (valorAtributo('cmbPOS') == '') { alert('FALTA SELECCIONAR POS'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            if (valorAtributo('cmbIdUnidadAdministracion') == '') { alert('FALTA FACTOR DE CONVERSION'); return false; }
            break;
        case 'modificaCitaArchivo':
            if (valorAtributo('cmbTipoCita') == '') { alert('FALTA SELECCIONAR TIPO DE CITA'); return false; }
            if (valorAtributo('cmbMotivoConsulta') == '') { alert('FALTA SELECCIONAR MOTIVO'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR ADMINISTRADORA'); return false; }
            if (valorAtributo('txtFechaPacienteCita') == '') { alert('FALTA SELECCIONAR FECHA PACIENTE'); return false; }
            if (valorAtributo('txtIPSRemite') == '') { alert('FALTA SELECCIONAR INSTITUCION QUE REMITE'); return false; }
            break;
        case 'modificarProcedimientoPlan':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA SELECCIONAR PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPrecioProcedimiento') == '') { alert('FALTA SELECCIONAR PRECIO PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtCodigoExterno') == '') { alert('FALTA INGRESAR CODIGO EXTERNO'); return false; }
            break;
        case 'adicionarProcedimientoPlan':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA SELECCIONAR PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPrecioProcedimiento') == '') { alert('FALTA SELECCIONAR PRECIO PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtCodigoExterno') == '') { alert('FALTA INGRESAR CODIGO EXTERNO'); return false; }
            break;
        case 'cerrarDocumentoSinImp':
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA ESCOJA DOCUMENTO CLINICO'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') == '1') { alert('EL DOCUMENTO YA ESTA FINALIZADO'); return false; }
            break;
        case 'cerrarFolioJasper':
            if (valorAtributo('lblIdFolioJasper') == '') { alert('FALTA ESCOJA DOCUMENTO CLINICO'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') == '1') { alert('EL DOCUMENTO YA ESTA FINALIZADO'); return false; }
            break;

        case 'adicionarProcedimientosDeFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA ADMISION CON FACTURA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'P') { alert('SOLO SE PERMITE EDITAR FACTURAS PRE-FINALIZADAS'); return false; }
            var ids = jQuery("#listProcedimientosDeFactura").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listProcedimientosDeFactura").getRowData(c);
                if (datosRow.CODIGO == valorAtributoIdAutoCompletar('txtIdProcedimientoCex')) {
                    alert('A1. YA EXISTE ESTE PROCEDIMIENTO EN EL LISTADO');
                    return false;
                }
            }
            if (valorAtributo('lblIdAdmision') == '') { alert('SELECCIONE ADMISION'); return false; }
            //if(valorAtributo('lblIdEstadoCuenta') !='A' ){ alert('LA CUENTA DEBE ESTAR ABIERTA');  return false; }			  
            if (valorAtributo('lblIdTipoAdmision') == '') { alert('SELECCIONE TIPO ADMISION'); return false; }
            if (valorAtributo('lblIdRango') == '') { alert('SELECCIONE RANGO'); return false; }
            if (valorAtributo('txtIdProcedimientoCex') == '') { alert('SELECCIONE PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbCantidad') == '') { alert('SELECCIONE CANTIDAD'); return false; }
            break;

        case 'adicionarProcedimientosSoloFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA ADMISION CON FACTURA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA FINALIZADA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA ANULADA'); return false; }

            var ids = jQuery("#listProcedimientosDeFactura").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listProcedimientosDeFactura").getRowData(c);
                if (datosRow.CODIGO == valorAtributoIdAutoCompletar('txtIdProcedimientoCex')) {
                    alert('A1. YA EXISTE ESTE PROCEDIMIENTO EN EL LISTADO');
                    return false;
                }
            }

            if (valorAtributo('txtIdProcedimientoCex') == '') { alert('SELECCIONE PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbCantidad') == '') { alert('SELECCIONE CANTIDAD'); return false; }
            break;



        case 'adicionarArticulosFacturaInventario':

            if (valorAtributo('lblIdDoc') == '') { alert('SELECCIONE UNA DOCUMENTO'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA FINALIZADA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA ANULADA'); return false; }

            var ids = jQuery("#listArticulosDeFactura").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listArticulosDeFactura").getRowData(c);
                if (datosRow.ID == valorAtributoIdAutoCompletar('txtIdArticuloFactura')) {
                    alert('A1. YA EXISTE ESTE ARTICULO EN EL LISTADO');
                    return false;
                }
            }

            if (valorAtributo('txtIdArticuloFactura') == '') { alert('SELECCIONE ARTICULO'); return false; }
            if (valorAtributo('txtValorUnitarioArticuloFactura') == '') { alert('SELECCIONE VALOR UNITARIO'); return false; }
            if (valorAtributo('cmbCantidad') == '') { alert('SELECCIONE CANTIDAD'); return false; }


            break;

        case 'adicionarArticulosFactura':

            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA FINALIZADA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA ANULADA'); return false; }
            if (valorAtributo('txtIdArticuloFactura') == '') { alert('SELECCIONE ARTICULO'); return false; }
            if (valorAtributo('lblValorUnitarioArticuloFactura') == '') { alert('FALTA VALOR UNITARIO'); return false; }
            if (valorAtributo('cmbCantidadArticuloFactura') == '') { alert('SELECCIONE CANTIDAD'); return false; }


            break;


        case 'eliminarProcedimientoCuenta':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA FINALIZADA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA ANULADA'); return false; }

            break;


        case 'eliminaCargoTarifario':
            if (valorAtributo('lblCargo') == '') { alert('SELECCIONE CARGO'); return false; }
            if (valorAtributo('lblIdTarifario') == '') { alert('SELECCIONE TARIFARIO'); return false; }
            break;
        case 'modificaCargoTarifario':
            if (valorAtributo('lblCargo') == '') { alert('SELECCIONE CARGO'); return false; }
            if (valorAtributo('txtDescCargoTarifario') == '') { alert('FALTA DESCRIPCION CARGO'); return false; }
            if (valorAtributo('cmbIdGrupoTarifario') == '') { alert('FALTA GRUPO TARIFARIO'); return false; }
            if (valorAtributo('cmbIdSubGrupoTarifario') == '') { alert('FALTA SUB-GRUPO TARIFARIO'); return false; }
            if (valorAtributo('cmbIdGrupoTipoCargo') == '') { alert('FALTA TIPO CARGO'); return false; }

            if (valorAtributo('cmbIdGrupoTipoCargo') == '') { alert('FALTA GRUPO TIPO CARGO'); return false; }
            if (valorAtributo('cmbIdTipoCargo') == '') { alert('FALTA TIPO CARGO'); return false; }
            if (valorAtributo('cmbIdConceptoRips') == '') { alert('FALTA CONCEPTO RIPS'); return false; }
            if (valorAtributo('cmbIdNivelAtencion') == '') { alert('FALTA NIVEL ATENCION'); return false; }
            if (valorAtributo('cmbUnidadPrecio') == '') { alert('FALTA UNIDAD PRECIO'); return false; }

            if (valorAtributo('cmbIdTipoUnidad') == '') { alert('FALTA TIPO UNIDAD PRECIO'); return false; }
            if (valorAtributo('txtGravamen') == '') { alert('FALTA GRAVAMEN'); return false; }
            if (valorAtributo('cmbHonorarios') == '') { alert('FALTA HONORARIOS'); return false; }
            if (valorAtributo('cmbExigeCantidad') == '') { alert('FALTA EXIGE CANTIDAD'); return false; }
            break;
        case 'crearCargoTarifario':
            if (valorAtributo('lblCargo') != '') { alert('PRIMERO LIMPIE'); return false; }
            if (valorAtributo('lblIdTarifario') == '') { alert('SELECCIONE TARIFARIO'); return false; }
            if (valorAtributo('txtIdCargoNuevo') == '') { alert('FALTA NUEVO CARGO'); return false; }
            if (valorAtributo('txtDescCargoTarifario') == '') { alert('FALTA DESCRIPCION CARGO'); return false; }
            if (valorAtributo('cmbIdGrupoTarifario') == '') { alert('FALTA GRUPO TARIFARIO'); return false; }
            if (valorAtributo('cmbIdSubGrupoTarifario') == '') { alert('FALTA SUB-GRUPO TARIFARIO'); return false; }
            if (valorAtributo('cmbIdGrupoTipoCargo') == '') { alert('FALTA TIPO CARGO'); return false; }

            if (valorAtributo('cmbIdGrupoTipoCargo') == '') { alert('FALTA GRUPO TIPO CARGO'); return false; }
            if (valorAtributo('cmbIdTipoCargo') == '') { alert('FALTA TIPO CARGO'); return false; }
            if (valorAtributo('cmbIdConceptoRips') == '') { alert('FALTA CONCEPTO RIPS'); return false; }
            if (valorAtributo('cmbIdNivelAtencion') == '') { alert('FALTA NIVEL ATENCION'); return false; }
            if (valorAtributo('txtUnidadPrecio') == '') { alert('FALTA UNIDAD PRECIO'); return false; }

            if (valorAtributo('cmbIdTipoUnidad') == '') { alert('FALTA TIPO UNIDAD PRECIO'); return false; }
            if (valorAtributo('txtGravamen') == '') { alert('FALTA GRAVAMEN'); return false; }
            if (valorAtributo('cmbHonorarios') == '') { alert('FALTA HONORARIOS'); return false; }
            if (valorAtributo('cmbExigeCantidad') == '') { alert('FALTA EXIGE CANTIDAD'); return false; }
            break;
        case 'listGrillaTarifarioEquivalencia':
            if (valorAtributo('txtIdEquivalenciaCups') == '') { alert('FALTA CODIGO CUPS'); return false; }
            if (valorAtributo('lblCargo') == '') { alert('SELECCIONE CARGO'); return false; }
            if (valorAtributo('lblIdTarifario') == '') { alert('SELECCIONE TARIFARIO'); return false; }

            break;

        case 'adicionarPersonalProc':
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA ESCOJA DOCUMENTO CLINICO'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != '0' && valorAtributo('txtIdEsdadoDocumento') != '12') { alert('ESTADO DEL DOCUMENTO NO LO PERMITE'); return false; }
            if (valorAtributo('cmbIdProfesiones') == '') { alert('FALTA ESCOJER PROFESION'); return false; }
            if (valorAtributo('cmbIdProfesionalesProc') == '') { alert('FALTA ESCOJER PROFESIONAL'); return false; }
            /* var ids = jQuery("#listPersonalProc").getDataIDs();
             for (var i = 0; i < ids.length; i++) {
                 var c = ids[i];
                 var datosRow = jQuery("#listPersonalProc").getRowData(c);
                 if (datosRow.ID_PERSONAL == valorAtributo('cmbIdProfesionalesProc')) {
                     alert('YA EXISTE ESTE PROFESIONAL EN EL LISTADO');
                     return false;
                 }
             }*/
            break;



        case 'adicionarPersonalCirugia':
            if (valorAtributo('lblIdAgendaDetalle') == '') { alert('FALTA ESCOJER AGENDA'); return false; }
            if (valorAtributo('cmbIdProfesiones') == '') { alert('FALTA ESCOJER PROFESION'); return false; }
            if (valorAtributo('cmbIdProfesionalesProc') == '') { alert('FALTA ESCOJER PROFESIONAL'); return false; }
            break;


        case 'listPersonalProcEliminar':
            if (valorAtributo('txtIdEsdadoDocumento') != '0' && valorAtributo('txtIdEsdadoDocumento') != '12') { alert('ESTADO DEL DOCUMENTO NO LO PERMITE'); return false; }
            break;

        case 'adicionarHcCirugiaProcedimientosPos':
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA ESCOJA DOCUMENTO CLINICO'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != '0') { alert('ESTADO DEL DOCUMENTO DEBE SER ABIERTO'); return false; }
            if (valorAtributo('cmbIdSitioQuirurgicoPosO') == '') { alert('FALTA ESCOJER SITIO'); return false; }
            if (valorAtributo('txtIdProcedimientoPosO') == '') { alert('FALTA ESCOJER PROCEDIMIENTO'); return false; }
            if (valorAtributo('lblTipoDocumento') != 'HQLA' &&
                valorAtributo('lblTipoDocumento') != 'HQUI' && valorAtributo('lblTipoDocumento') != 'HPRO'
            ) { alert('SOLO SE PUEDE AGREGAR A UNA DESCRIPCION QUIRURGICA'); return false; }

            var ids = jQuery("#listPosOperatorio").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listPosOperatorio").getRowData(c);
                if (datosRow.ID_PROCEDIMIENTO == valorAtributoIdAutoCompletar('txtIdProcedimientoPosO') && datosRow.ID_SITIO == valorAtributo('cmbIdSitioQuirurgicoPosO')) {
                    alert('YA EXISTE ESTE PROCEDIMIENTO A ESTE SITIO EN EL LISTADO');
                    return false;
                }
            }
            break;
        case 'traerHcCirugiaProcedimientosPreAPos':
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA ESCOJA DOCUMENTO CLINICO'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != '0') { alert('ESTADO DEL DOCUMENTO DEBE SER ABIERTO'); return false; }
            if (valorAtributo('lblTipoDocumento') != 'HQLA' &&
                valorAtributo('lblTipoDocumento') != 'HQUI'
            ) { alert('SOLO SE PUEDE AGREGAR A UNA DESCRIPCION QUIRURGICA'); return false; }

            var ids = jQuery("#listPosOperatorio").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listPosOperatorio").getRowData(c);
                if (datosRow.ID_PROCEDIMIENTO == valorAtributo('lblIdProcedimiento') && datosRow.ID_SITIO == valorAtributo('lblIdSitioPre')) {
                    alert('YA EXISTE ESTE PROCEDIMIENTO A ESTE SITIO EN EL LISTADO');
                    return false;
                }
            }
            break;

        case 'procedimientosGestionEdit':
            if (valorAtributo('txt_observacionGestion') == '') { alert('FALTA OBSERVACION'); return false; }
            if (valorAtributo('cmbEstadoEditProc') == '') { alert('FALTA ESTADO'); return false; }
            break;
        case 'listAnestesia':
            if (valorAtributo('cmbAnestesiasGrupo') == '') { alert('FALTA GRUPO ANESTESIA'); return false; }
            if (valorAtributo('txtHoraAnestesia') == '' || valorAtributo('txtHoraAnestesia') > 12 || valorAtributo('txtHoraAnestesia') < 0) { alert('FALTA HORA EVENTO O ES MAYOR QUE 12'); return false; }
            if (valorAtributo('txtMinAnestesia') == '' || valorAtributo('txtMinAnestesia') > 59 || valorAtributo('txtMinAnestesia') < 0) { alert('FALTA MINUTOS'); return false; }
            break;
        case 'listMedicacionConNoPOS':
            verificarCamposGuardar('listMedicacion')
            if (valorAtributo('txt_ResumenHC') == '') { alert('FALTA RESUMEN ENFERMEDAD ACTUAL'); return false; }
            if (valorAtributo('txt_MedicamentoDelPOS1') == '') { alert('FALTA MEDICAMENTO DEL POST UTILIZADO'); return false; }
            if (valorAtributo('txt_PresentacionConcentracion1') == '') { alert('FALTA PRESENTACION DEL MEDICAMENTO 1 NO POS'); return false; }
            if (valorAtributo('txt_DosisFrecuencia1') == '') { alert('FALTA FRECUENCIA DEL MEDICAMENTO 1 NO POS'); return false; }
            if (valorAtributo('txt_IndicacionTerapeutica') == '') { alert('FALTA INDICACION TERAPEUTICA DEL MEDICAMENTO 1 NO POS'); return false; }
            if (valorAtributo('txt_ExpliquePos') == '') { alert('FALTA EXPLICACION NO POS'); return false; }
            if (valorAtributo('cmbUnidadFarmaceutica') == '') { alert('FALTA UNIDAD FARMACEUTICA'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA CANTIDAD'); return false; }
            break;
        case 'listMedicacion':
            if (valorAtributo('lblIdDocumento') == '') { alert('SELECCIONE DOCUMENTO CLINICO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdArticulo') == '') { alert('SELECCIONE MEDICAMENTO'); return false; }
            if (valorAtributo('cmbIdVia') == '') { alert('FALTA VIA'); return false; }
            if (valorAtributo('cmbUnidad') == '') { alert('FALTA FORMA FARMACEUTICA'); return false; }
            if (valorAtributo('cmbCant') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('cmbFrecuencia') == '') { alert('FALTA FRECUENCIA'); return false; }
            if (valorAtributo('cmbDias') == '') { alert('FALTA DIAS'); return false; }
            if (valorAtributo('cmbUnidadFarmaceutica') == '') { alert('FALTA UNIDAD FARMACEUTICA'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA CANTIDAD'); return false; }

            var ids = jQuery("#listMedicacion").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listMedicacion").getRowData(c);
                if (datosRow.ID_ARTICULO == valorAtributoIdAutoCompletar('txtIdArticulo')) {
                    alert('YA EXISTE ESTE MEDICAMENTO EN EL LISTADO');
                    return false;
                }
            }
            break;
        case 'listSistemas':
            if (valorAtributo('lblIdDocumento') == '') { alert('SELECCIONE DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtSistema') == '') { alert('SELECCIONE SISTEMA'); return false; }
            if (valorAtributo('txtHallazgo') == '') { alert('FALTA HALLAZGO'); return false; }

            var ids = jQuery("#listSistemas").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listSistemas").getRowData(c);
                if (datosRow.id == valorAtributoIdAutoCompletar('txtSistema')) {
                    alert('YA EXISTE ESTE SISTEMA EN EL LISTADO DE ESTE DOCUMENTO');
                    return false;
                }
            }
            break;

        case 'listExamenFisicoSistemas':

            if (valorAtributo('lblIdDocumento') == '') { alert('SELECCIONE DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtExamenSistema') == '') { alert('SELECCIONE SISTEMA'); return false; }
            if (valorAtributo('txtExamenHallazgo') == '') { alert('FALTA HALLAZGO'); return false; }

            var ids = jQuery("#listExamenFisicoSistemas").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listExamenFisicoSistemas").getRowData(c);
                if (datosRow.id == valorAtributoIdAutoCompletar('txtExamenSistema')) {
                    alert('YA EXISTE ESTE SISTEMA EN EL LISTADO DE ESTE DOCUMENTO');
                    return false;
                }
            }
            break;

        case 'procedimientoAdd':
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            if (valorAtributo('txtCups') == '') { alert('FALTA CUPS'); return false; }
            break;

        case 'modificarDx':
            if (valorAtributoIdAutoCompletar('txtIdDxVentanita') == '') { alert('SELECCIONE DIAGNOSTICO'); return false; }
            if (valorAtributo('cmbDxTipoVentanita') == '') { alert('FALTA TIPO'); return false; }
            if (valorAtributo('cmbDxClaseVentanita') == '') { alert('FALTA CLASE'); return false; }
            if (valorAtributo('cmbIdSitioQuirurgicoDxVentanita') == '') { alert('FALTA SITIO'); return false; }

            var ids = jQuery("#listDiagnosticos").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listDiagnosticos").getRowData(c);
                if (datosRow.id != valorAtributo('txtIdTrans')) {
                    if (datosRow.id_cie == valorAtributoIdAutoCompletar('txtIdDxVentanita') && datosRow.Sitio == valorAtributoCombo('cmbIdSitioQuirurgicoDxVentanita')) {
                        alert('YA EXISTE ESTE DIAGNOSTICO CON SITIO EN EL LISTADO DE ESTE DOCUMENTO');
                        return false;
                    }
                }
            }

            if (datosRow.id != valorAtributo('txtIdTrans')) {
                for (var i = 0; i < ids.length; i++) {
                    var c = ids[i];
                    var datosRow = jQuery("#listDiagnosticos").getRowData(c);
                    if (datosRow.id_cie == valorAtributoIdAutoCompletar('txtIdDxVentanita')) {
                        alert('Atencion!!!! \nYA EXISTE ESTE DIAGNOSTICO EN EL LISTADO');
                        return true;
                    }
                }
            }
            break;

        case 'listDiagnosticos':
            if (valorAtributo('lblIdDocumento') == '') { alert('SELECCIONE DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdDx') == '') { alert('SELECCIONE DIAGNOSTICO'); return false; }
            if (valorAtributo('cmbDxTipo') == '') { alert('FALTA TIPO'); return false; }
            if (valorAtributo('cmbDxClase') == '') { alert('FALTA CLASE'); return false; }
            if (valorAtributo('cmbIdSitioQuirurgicoDx') == '') { alert('FALTA SITIO'); return false; }

            var ids = jQuery("#listDiagnosticos").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listDiagnosticos").getRowData(c);
                if (datosRow.id_cie == valorAtributoIdAutoCompletar('txtIdDx') && datosRow.Sitio == valorAtributoCombo('cmbIdSitioQuirurgicoDx')) {
                    alert('YA EXISTE ESTE DIAGNOSTICO CON SITIO EN EL LISTADO DE ESTE DOCUMENTO');
                    return false;
                }
            }
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listDiagnosticos").getRowData(c);
                if (datosRow.id_cie == valorAtributoIdAutoCompletar('txtIdDx')) {
                    alert('Atencion!!!! \nYA EXISTE ESTE DIAGNOSTICO EN EL LISTADO');
                    return true;
                }
            }
            break;

        case 'crearAdmision':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONOS'); return false; }

            if (valorAtributo('cmbIdEtnia') == '') { alert('FALTA ETNIA'); return false; }
            if (valorAtributo('cmbIdNivelEscolaridad') == '') { alert('FALTA NIVEL ESCOLARIDAD'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA TIPO REGIMEN'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdOcupacion') == '') { alert('FALTA OCUPACION'); return false; }
            if (valorAtributo('cmbIdEstrato') == '') { alert('FALTA ESTRATO'); return false; }
            if (valorAtributoIdAutoCompletar('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('txtNoAutorizacion') == '') { alert('FALTA NUMERO DE AUTORIZACION'); return false; }
            if (valorAtributoIdAutoCompletar('txtIPSRemite') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdDx') == '') { alert('FALTA DIAGNOSTICO'); return false; }
            if (valorAtributo('cmbTipoAdmision') == '') { alert('FALTA TIPO'); return false; }
            if (valorAtributo('cmbCausaExterna') == '') { alert('FALTA CAUSA EXTERNA'); return false; }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('FALTA ESPECIALIDAD'); return false; }
            //if (valorAtributo('cmbIdSubEspecialidad') == '') { alert('FALTA SUB ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbIdProfesionales') == '') { alert('FALTA MEDICO'); return false; }
            if (valorAtributo('txtObservacion') == '') { alert('OJO !!!  LE FALTA EL NUMERO DE FACTURA'); return false; }

            /* if(valorAtributo('lblIdCita')!=''){
				  var ids = jQuery("#listAdmision").getDataIDs();			  
				  for(var i=0;i<ids.length;i++){ 
					  var c = ids[i];
					  var datosRow = jQuery("#listAdmision").getRowData(c); 
					  if(datosRow.id_cita == valorAtributo('lblIdCita')){ 
							alert('NO SE PUEDE CREAR ADMISION PORQUE YA EXISTE UNA CITA EN EL LISTADO DE OTRA ADMISION'); return false; 
					  }
				  }	
			  }*/

            break;
        case 'listCitaCirugiaProcedimiento':
            if (valorAtributo('cmbIdSitioQuirurgico') == '') { alert('FALTA SITIO QUIRURGICO'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA PROCEDIMIENTO'); return false; }
            var ids = jQuery("#listCitaCirugiaProcedimiento").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listCitaCirugiaProcedimiento").getRowData(c);

                if (datosRow.idSitioQuirur == valorAtributo('cmbIdSitioQuirurgico')) {
                    alert('ATENCION !!!! YA EXISTE UN SITIO EN EL LISTADO, \n IGUAL SE ADICIONARA');
                    if (datosRow.idProcedimiento == valorAtributoIdAutoCompletar('txtIdProcedimiento')) {
                        alert('YA EXISTE UN PROCEDIMIENTO PARA ESTE SITIO EN EL LISTADO');
                        return false;

                    }
                    alert('Asignar un procedimiento a sitio diferente?')
                }
            }
            break;
        case 'listCitaCirugiaProcedimientoLE':
            if (valorAtributo('cmbIdSitioQuirurgicoLE') == '') { alert('FALTA SITIO QUIRURGICO'); return false; }
            if (valorAtributo('txtIdProcedimientoLE') == '') { alert('FALTA PROCEDIMIENTO'); return false; }
            var ids = jQuery("#listCitaCirugiaProcedimientoLE").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listCitaCirugiaProcedimientoLE").getRowData(c);

                if (datosRow.idSitioQuirur == valorAtributo('cmbIdSitioQuirurgicoLE')) {
                    alert('ATENCION !!!! YA EXISTE UN SITIO EN EL LISTADO, \n IGUAL SE ADICIONARA');
                    if (datosRow.idProcedimiento == valorAtributoIdAutoCompletar('txtIdProcedimientoLE')) {
                        alert('YA EXISTE UN PROCEDIMIENTO PARA ESTE SITIO EN EL LISTADO');
                        return false;

                    }

                }
            }
            break;
        case 'adicionarListaEsperaProcedimientos':
            if (valorAtributo('cmbIdSitioQuirurgico') == '') { alert('FALTA SITIO QUIRURGICO'); return false; }
            break;
        case 'eliminaListaEspera':

            var ids = jQuery("#listListaEsperaProcedimiento").getDataIDs();
            if (ids.length > 0) {
                alert('NO SE PUEDE ELIMINAR PORQUE TIENE PROCEDIMIENTOS RELACIONADOS')
                return false;
            }
            break;
        case 'crearListaEsperaCirugia':
            var ids = jQuery("#listCitaCirugiaProcedimientoLE").getDataIDs();
            if (ids.length < 1) { alert('AL MENOS DEBE HABER UN PROCEDIMIENTO ! ! !'); return false; }

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONOS'); return false; }
            if (valorAtributo('txtCelular1') == '') { alert('FALTA CELULAR1'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA TIPO REGIMEN - PLAN'); return false; }

            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE Especialidad'); return false; }
            //if (valorAtributo('cmbIdSubEspecialidad') == '') { alert('SELECCIONE SubEspecialidad'); return false; }
            if (valorAtributo('cmbIdProfesionales') == '') { alert('SELECCIONE PROFESIONAL'); return false; }
            if (valorAtributo('cmbDiscapaciFisica') == '') { alert('SELECCIONE Discapacidad Fisica'); return false; }
            if (valorAtributo('cmbEmbarazo') == '') { alert('SELECCIONE Embarazo'); return false; }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE Tipo Cita'); return false; }
            if (valorAtributo('cmbEsperar') == '') { alert('SELECCIONE ESPERAR'); return false; }
            if (valorAtributo('cmbNecesidad') == '') { alert('SELECCIONE Necesidad'); return false; }
            if (valorAtributo('cmbEstado') == '') { alert('SELECCIONE Estado'); return false; }

            break;
        case 'crearListaEsperaCirugiaProgramacion':

            var ids = jQuery("#listCitaCirugiaProcedimientoLEGestion").getDataIDs();
            if (ids.length < 1) { alert('AL MENOS DEBE HABER UN PROCEDIMIENTO ! ! !'); return false; }

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONOS'); return false; }
            if (valorAtributo('txtCelular1') == '') { alert('FALTA CELULAR1'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }

            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE Especialidad'); return false; }
            if (valorAtributo('cmbDiscapaciFisica') == '') { alert('SELECCIONE Discapacidad Fisica'); return false; }
            if (valorAtributo('cmbEmbarazo') == '') { alert('SELECCIONE Embarazo'); return false; }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE Tipo Cita'); return false; }
            if (valorAtributo('cmbEsperar') == '') { alert('SELECCIONE ESPERAR'); return false; }
            if (valorAtributo('cmbNecesidad') == '') { alert('SELECCIONE Necesidad'); return false; }
            if (valorAtributo('cmbEstado') == '') { alert('SELECCIONE Estado'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('SELECCIONE TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('SELECCIONE TIPO REGIMEN'); return false; }
            break;
        case 'modificarListaEspera':
            if (valorAtributo('txtAdministradora1LE') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            //  if(valorAtributo('cmbIdEspecialidadLE') ==''  ){ alert('SELECCIONE Especialidad'); return false; }
            //if (valorAtributo('cmbIdSubEspecialidadLE') == '') { alert('SELECCIONE SubEspecialidad'); return false; }
            break;
        case 'crearListaEspera':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONOS'); return false; }
            if (valorAtributo('txtCelular1') == '') { alert('FALTA CELULAR1'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA REGIMEN - PLAN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA REGIMEN - PLAN'); return false; }

            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE Especialidad'); return false; }
            if (valorAtributo('cmbDiscapaciFisica') == '') { alert('SELECCIONE Discapacidad Fisica'); return false; }
            if (valorAtributo('cmbEmbarazo') == '') { alert('SELECCIONE Embarazo'); return false; }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE Tipo Cita'); return false; }
            if (valorAtributo('cmbEsperar') == '') { alert('SELECCIONE ESPERAR'); return false; }
            if (valorAtributo('cmbNecesidad') == '') { alert('SELECCIONE Necesidad'); return false; }
            if (valorAtributo('cmbEstado') == '') { alert('SELECCIONE Estado'); return false; }
            if (valorAtributo('cmbSede') == '') { alert('SELECCIONE SEDE'); return false; }
            break;
        case 'crearNuevoPaciente':

            //if(validarFecha('txtFechaNac',110)==false){ return false;}
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA TIPO ID'); return false; }
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA IDENTIFICACION'); return false; }
            if (valorAtributo('txtApellido1') == '') { alert('FALTA PRIMER APELLIDO'); return false; }
            if (valorAtributo('txtNombre1') == '') { alert('FALTA PRIMER NOMBRE'); return false; }
            if (valorAtributo('txtFechaNac') == '') { alert('FALTA FECHA NACIMIENTO'); return false; }
            if (validarFecha('txtFechaNac', 110) == false) { return false; }
            if (valorAtributo('cmbSexo') == '') { alert('FALTA SEXO'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            // if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONO 1'); return false; }
            if (valorAtributo('txtCelular1') == '') { alert('FALTA CELULAR 1'); return false; }
            if (valorAtributo('txtAdministradoraPaciente') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            break;

        case 'crearCita':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE TIPO CITA'); return false; }
            if (valorAtributo('cmbEstadoCita') == '') { alert('SELECCIONE ESTADO CITA'); return false; }
            if (valorAtributo('cmbMotivoConsulta') == '') { alert('SELECCIONE MOTIVO CONSULTA'); return false; }
            //if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONOS'); return false; }
            if (valorAtributo('txtCelular1') == '') { alert('FALTA CELULAR1'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }

            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA TIPO DE REGIMEN - PLAN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA TIPO DE REGIMEN - PLAN'); return false; }

            if (valorAtributo('txtIPSRemite') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }

            if (valorAtributo('txtFechaPacienteCita') == '') { alert('FALTA FECHA PACIENTE'); return false; }
            if (Date.parse(valorAtributo('lblFechaCita')) < Date.parse(valorAtributo('txtFechaPacienteCita'))) { alert('FECHA DE PACIENTE NO ES VALIDA. DEBE SER MAYOR O IGUAL A LA FECHA DE LA CITA'); return false; }

            //if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('txtNoAutorizacion') == '') { alert('PARA CONFIRMAR DEBE DILIGENCIAR:   NUMERO DE AUTORIZACION'); return false; }
            //if (valorAtributo('cmbEstadoCita') == 'F' && valorAtributo('txtNoAutorizacion') == '') { alert('PARA FACTURADO DEBE DILIGENCIAR:   NUMERO DE AUTORIZACION'); return false; }

            if ((valorAtributo('cmbEstadoCita') == 'L' || valorAtributo('cmbEstadoCita') == 'R') &&
                valorAtributo('cmbMotivoConsultaClase') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }

            var gr1 = jQuery("#listProcedimientosAgenda").getDataIDs();
            var gr2 = jQuery("#listCitaCexProcedimientoTraer").getDataIDs();

            if ((gr1.length + 0) > 0 && (gr2.length + 0) === 0 && valorAtributo('cmbIdTipoRegimen').split('-')[1] === '2') { alert('DEBE AGREGAR ALGUN PROCEDIMIENTO!'); return false; }
            //var gr1 = jQuery("#listProcedimientosAgenda").getDataIDs();
            break;

        case 'crearCitaCirugia':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE TIPO CITA'); return false; }
            if (valorAtributo('cmbEstadoCita') == '') { alert('SELECCIONE ESTADO CITA'); return false; }
            if (valorAtributo('cmbMotivoConsulta') == '') { alert('SELECCIONE MOTIVO'); return false; }
            if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONOS'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA TIPO DE REGIMEN - PLAN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA TIPO DE REGIMEN - PLAN'); return false; }

            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('txtFechaPacienteCita') == '') { alert('FALTA FECHA PACIENTE'); return false; }
            if (valorAtributo('txtIPSRemite') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }

            if (valorAtributo('cmbEstadoCita') == 'A' && valorAtributo('cmbTipoAnestesia') == '') { alert('FALTA TIPO ANESTESIA'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('cmbTipoAnestesia') == '') { alert('FALTA TIPO ANESTESIA'); return false; }

            if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('txtAnestesiologo') == '') { alert('FALTA ANESTESIOLOGO'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'A' && valorAtributo('txtAnestesiologo') == '') { alert('FALTA ANESTESIOLOGO'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('txtInstrumentador') == '') { alert('FALTA INSTRUMENTADOR'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'A' && valorAtributo('txtInstrumentador') == '') { alert('FALTA INSTRUMENTADOR'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('txtCirculante') == '') { alert('FALTA CIRCULANTE'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'A' && valorAtributo('txtCirculante') == '') { alert('FALTA CIRCULANTE'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('txtNoLente') == '') { alert('FALTA NUMERO LENTE'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'A' && valorAtributo('txtNoLente') == '') { alert('FALTA NUMERO LENTE'); return false; }

            if ((valorAtributo('cmbEstadoCita') == 'L' || valorAtributo('cmbEstadoCita') == 'R') &&
                valorAtributo('cmbMotivoConsultaClase') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }

            if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('txtNoAutorizacion') == '') { alert('PARA CONFIRMAR DEBE DILIGENCIAR:   NUMERO DE AUTORIZACION'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'F' && valorAtributo('txtNoAutorizacion') == '') { alert('PARA FACTURADO DEBE DILIGENCIAR:   NUMERO DE AUTORIZACION'); return false; }

            var ids = jQuery("#listCitaCirugiaProcedimiento").getDataIDs();
            if (ids.length < 1) { alert('AL MENOS DEBE HABER UN PROCEDIMIENTO ! ! !'); return false; }
            break;
        /******* AGENDA */

        /******* AGENDA */
        case 'listAgenda':
            if (valorAtributo('cmbHoraInicio') == '') { alert('SELECCIONE HORA DE INICIO.'); return false; }
            if (valorAtributo('cmbMinutoInicio') == '') { alert('SELECCIONE MINUTO DE INICIO.'); return false; }
            if (valorAtributo('cmbPeriodoInicio') == '') { alert('FALTA SELECCIONAR PERIORDO DE INICIO AM/PM.'); return false; }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbIdProfesionales') == '') { alert('SELECCIONE PROFESIONAL'); return false; }
            if (valorAtributo('cmbDuracionMinutos') == '') { alert('SELECCIONE DURACION EN MINUTOS'); return false; }
            if (valorAtributo('cmbCuantas') == '') { alert('SELECCIONE CUANTAS CITAS'); return false; }
            if (valorAtributo('lblFechaSeleccionada') == '') { alert('SELECCIONE DIA FECHA'); return false; }
            if (valorAtributo('cmbBloqueCita') == '') { alert('SELECCIONE SI DESEA CREAR CITA EN BLOQUE'); return false; }
            if (valorAtributo('cmbConsultorio') == '') { alert('FALTA CONSULTORIO'); return false; }
            break;

        case 'listAgendaCopiarDia':
            if ($("#listDiasC").jqGrid('getGridParam', 'selrow') === null) { alert('SELECCIONE UN DIA DE LA AGENDA'); return false; }
            if (valorAtributo('txtFechaDestino') == '') { alert('SELECCIONE FECHA DESTINO'); return false; }
            break;

        case 'MoverAgenda':
            if ($("#listDiasC").jqGrid('getGridParam', 'selrow') === null) { alert('SELECCIONE UN DIA DE LA AGENDA'); return false; }
            if (valorAtributo('txtFechaDestino') == '') { alert('SELECCIONE FECHA DESTINO'); return false; }
            break;

        case 'MoverAgendaporProfesional':
            if ($("#listDiasC").jqGrid('getGridParam', 'selrow') === null) { alert('SELECCIONE UN DIA DE LA AGENDA'); return false; }
            if (valorAtributo('txtFechaDestino') == '') { alert('SELECCIONE FECHA DESTINO'); return false; }
            if (valorAtributo('cmbIdProfesionalesMov') == '') { alert('SELECCIONE PROFESIONAL PARA MODIFICAR LA AGENDA'); return false; }
            break;
        /*************FIN CLINICA*/
        case 'adicionarSolicitudADocumento':
            if (valorAtributo('lblIdEstadoDocumento') != 0) { alert(' DOCUMENTO DEBE ESTAR ABIERTO ! ! !'); return false; }
            if (valorAtributo('lblIdEstadoDocumento') == '') { alert(' FALTA SELECCIONAR DOCUMENTO ! ! !'); return false; }
            break;

        case 'pdfAnexo3':
            if (valorAtributo('lblMunicipio') == 'null') { alert(' FALTA DILIGENCIAR MUNICIPIO ! ! !'); return false; }
            if (valorAtributo('lblTipoUsuario') == 'null') { alert(' FALTA DILIGENCIAR TIPO PACIENTE ! ! !'); return false; }
            break;

        case 'pdfAnexo2':
            if (valorAtributo('lblOrigenAtencion') == 'null') { alert(' FALTA DILIGENCIAR ORIGEN DE LA ANECION ! ! !'); return false; }
            if (valorAtributo('lblMunicipio') == 'null') { alert(' FALTA DILIGENCIAR MUNICIPIO ! ! !'); return false; }
            if (valorAtributo('lblOrigenAtencion') == 'null') { alert(' FALTA DILIGENCIAR ORIGEN DE LA ANECION ! ! !'); return false; }
            if (valorAtributo('lblClasTriage') == 'null') { alert(' FALTA DILIGENCIAR CLASE TRIAGE ! ! !'); return false; }
            if (valorAtributo('lbl_DestinoPaciente') == 'null') { alert(' FALTA DILIGENCIAR DESTINO PACIENTE ! ! !'); return false; }
            if (valorAtributo('lblTipoUsuario') == 'null') { alert(' FALTA DILIGENCIAR TIPO PACIENTE ! ! !'); return false; }

            break;
        case 'pdfAnexo':


            if (valorAtributo('lblOrigenAtencion') == 'null') { alert(' FALTA DILIGENCIAR ORIGEN DE LA ANECION ! ! !'); return false; }
            if (valorAtributo('lblMunicipio') == 'null') { alert(' FALTA DILIGENCIAR MUNICIPIO ! ! !'); return false; }
            if (valorAtributo('lblOrigenAtencion') == 'null') { alert(' FALTA DILIGENCIAR ORIGEN DE LA ANECION ! ! !'); return false; }
            if (valorAtributo('lblClasTriage') == 'null') { alert(' FALTA DILIGENCIAR CLASE TRIAGE ! ! !'); return false; }
            if (valorAtributo('lbl_DestinoPaciente') == 'null') { alert(' FALTA DILIGENCIAR DESTINO PACIENTE ! ! !'); return false; }
            if (valorAtributo('lblTipoUsuario') == 'null') { alert(' FALTA DILIGENCIAR TIPO PACIENTE ! ! !'); return false; }

            break;


        case 'proyectarDocumentoInventario':
            if (valorAtributo('cmbIdBodega') == '') { alert(' FALTA ESCOGER BODEGA ! ! !'); return false; }
            if (valorAtributo('cmbIdPresentacion') == '') { alert(' FALTA ESCOGER PRESENTACION ! ! !'); return false; }
            break;
        case 'cerrarTransaccionesDocumentoInvUD':

            var ids = jQuery("#listTransaccionesInventarioConsolidado").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listTransaccionesInventarioConsolidado").getRowData(c);
                if (datosRow.id_permitido == 'N') { alert('No se puede Cerrar porque existe una cantidad NO permitida'); return false; }
            }

            var ids = jQuery("#listTransaccionesInventario").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listTransaccionesInventario").getRowData(c);
                if (datosRow.Proyectable == 'S' && datosRow.Proyectable) {
                    if (datosRow.Proyectable != 'S') {
                        alert('No se puede Cerrar porque existen elementos proyectables');
                        return false;
                    }
                }
            }
            return true;
            break;
        case 'cerrarTransaccionesDocumentoInvUDReportesExcel':

            if (valorAtributo('lblIdEstadoDocumento') == 0) {
                alert('NO SE PERMITE IMPRIMIR PORQUE EL DOCUMENTO ESTA ABIERTO');
                return false;
            } else return true;
            break;

        case 'MoverCitaPaciente':
            if (valorAtributo('txtFechaDestinoCita') == '') { alert(' DEBE SELECCIONAR FECHA DESTINO! ! !'); return false; }
            if (valorAtributo('cmbIdProfesionalesAg') == '') { alert(' DEBE SELECCIONAR PROFESIONAL! ! !'); return false; }
            if (valorAtributo('cmbConsultorioAg') == '') { alert('DEBE SELECCIONAR UN CONSULTORIO'); return false; }
            break;

        case 'listProcedimientos':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdTipoServicioSolicitado') == '') { alert(' FALTA ESCOGER SERVICIO SOLICITADO ! ! !'); return false; }
            if (valorAtributo('cmbIdPrioridad') == '') { alert(' FALTA ESCOGER PRIORIDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdGuia') == '') { alert(' FALTA ESCOGER GUIA DE MANEJO INTEGRAL ! ! !'); return false; }
            if (valorAtributo('txtJustificacion') == '') { alert(' FALTA ESCOGER JUSTIFICACION ! ! !'); return false; }
            break;
        case 'listProcedimientosDetalle':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO !!!'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            if (valorAtributo('cmbIdDxRelacionadoP') == '') { alert(' FALTA ESCOGER DIAGNOSTICO ASOCIADO !!!'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdProcedimiento') == '') { alert(' FALTA ESCOGER PROCEDIMIENTO !!!'); return false; }
            if (valorAtributo('txtPrincipalHC') == 'OK') {
                if (valorAtributo('cmbCantidad') == '') { alert(' FALTA ESCOGER CANTIDAD !!!'); return false; }
            } else {
                if (valorAtributo('cmbCantidadFactur') == '') { alert(' FALTA ESCOGER CANTIDAD FACTURADOR !!!'); return false; }
            }
            if (valorAtributo('cmbNecesidad') == '') { alert(' FALTA GRADO NECESIDAD !!!'); return false; }

            break;
        case 'listAyudasDiagnosticas':

            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdTipoServicioSolicitadoAD') == '') { alert(' FALTA ESCOGER SERVICIO SOLICITADO ! ! !'); return false; }
            if (valorAtributo('cmbIdPrioridadAD') == '') { alert(' FALTA ESCOGER PRIORIDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdGuiaAD') == '') { alert(' FALTA ESCOGER GUIA DE MANEJO INTEGRAL ! ! !'); return false; }
            if (valorAtributo('txtJustificacionAD') == '') { alert(' FALTA ESCOGER JUSTIFICACION ! ! !'); return false; }
            break;
        case 'listAyudasDiagnosticasDetalle':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdDxRelacionadoA') == '') { alert(' FALTA ESCOGER DIAGNOSTICO ASOCIADO ! ! !'); return false; }
            if (valorAtributo('txtIdAyudaDiagnostica') == '') { alert('FALTA AYUDA DIAGNOSTICA'); return false; }
            if (valorAtributo('cmbCantidadAD') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('cmbIdSitioQuirurgicoAD') == '') { alert('FALTA SITIO'); return false; }
            break;
        case 'listLaboratorios':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdTipoServicioSolicitadoLaboratorio') == '') { alert(' FALTA ESCOGER SERVICIO SOLICITADO ! ! !'); return false; }
            if (valorAtributo('cmbIdPrioridadLaboratorio') == '') { alert(' FALTA ESCOGER PRIORIDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdGuiaLaboratorio') == '') { alert(' FALTA ESCOGER GUIA DE MANEJO INTEGRAL ! ! !'); return false; }
            if (valorAtributo('txtJustificacionLaboratorio') == '') { alert(' FALTA ESCOGER JUSTIFICACION ! ! !'); return false; }
            break;
        case 'listLaboratoriosDetalle':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdDxRelacionadoL') == '') { alert(' FALTA ESCOGER DIAGNOSTICO ASOCIADO ! ! !'); return false; }
            if (valorAtributo('txtIdLaboratorio') == '') { alert('FALTA LABORATORIO'); return false; }
            if (valorAtributo('cmbCantidadLaboratorio') == '') { alert('FALTA CANTIDAD'); return false; }
            break;

        case 'listTerapia':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdTipoServicioSolicitadoTerapia') == '') { alert(' FALTA ESCOGER SERVICIO SOLICITADO ! ! !'); return false; }
            if (valorAtributo('cmbIdPrioridadTerapia') == '') { alert(' FALTA ESCOGER PRIORIDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdGuiaTerapia') == '') { alert(' FALTA ESCOGER GUIA DE MANEJO INTEGRAL ! ! !'); return false; }
            if (valorAtributo('txtJustificacionTerapia') == '') { alert(' FALTA ESCOGER JUSTIFICACION ! ! !'); return false; }
            break;

        case 'listTerapiaDetalle':
            if (valorAtributo('cmbIdDxRelacionadoT') == '') { alert(' FALTA ESCOGER DIAGNOSTICO ASOCIADO ! ! !'); return false; }
            if (valorAtributo('txtIdTerapia') == '') { alert('FALTA TERAPIA'); return false; }
            if (valorAtributo('cmbCantidadTerapia') == '') { alert('FALTA CANTIDAD'); return false; }
            break;

        case 'listDx':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdDx') == '') { alert(' FALTA ESCOGER DIAGNOSTICO ! ! !'); return false; }
            if (valorAtributo('cmbDxTipo') == '') { alert(' FALTA ESCOGER TIPO ! ! !'); return false; }
            if (valorAtributo('cmbDxClase') == '') { alert(' FALTA ESCOGER CLASE ! ! !'); return false; }
            break;

        case 'listAntecedentes':
            if (valorAtributo('cmbAntecedenteGrupo') == '') { alert(' FALTA ESCOGER GRUPO DE ANTECEDENTES ! ! !'); return false; }
            if (valorAtributo('cmbTiene') == '') { alert(' FALTA ESCOGER TIENE'); return false; }
            if (valorAtributo('cmbControlada') == '') { alert(' FALTA ESCOGER CONTROLADA ! ! !'); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO CLINICO ! ! !'); return false; }
            if (valorAtributo('lblIdPaciente') == '') { alert(' FALTA ESCOGER PACIENTE ! ! !'); return false; }
            /*var ids = jQuery("#listAntecedentes").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listAntecedentes").getRowData(c);
                if (datosRow.id_grupo == valorAtributo('cmbAntecedenteGrupo')) {
                    alert('YA EXISTE UN ANTECEDENTE EN EL LISTADO');
                    return false;
                }
            }*/
            break;

        case 'modificarAntecedente':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO CLINICO ! ! !'); return false; }
            if (valorAtributo('lblIdPaciente') == '') { alert(' FALTA ESCOGER PACIENTE ! ! !'); return false; }
            if (valorAtributo('lblIdDocumento') != valorAtributo('lblIdEvolucionAntecedente')) { alert('NO PUEDES MODIFICAR SOBRE EL FOLIO ACTUAL'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != 0) { alert('ESTADO DE FOLIO NO PERMITE MODIFICAR'); return false; }
            /*var ids = jQuery("#listAntecedentes").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listAntecedentes").getRowData(c);
                if (datosRow.id_grupo == valorAtributo('cmbAntecedenteGrupo')) {
                    alert('YA EXISTE UN ANTECEDENTE EN EL LISTADO');
                    return false;
                }
            }*/
            break;

        case 'eliminarAntecedentes':
            if (valorAtributo('lblIdDocumento') != valorAtributo('lblIdEvolucionAntecedente')) { alert('NO PUEDES ELIMINAR SOBRE EL FOLIO ACTUAL'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != 0) { alert('ESTADO DE FOLIO NO PERMITE ELIMINAR'); return false; }
            /*var ids = jQuery("#listAntecedentes").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listAntecedentes").getRowData(c);
                if (datosRow.id_grupo == valorAtributo('cmbAntecedenteGrupo')) {
                    alert('YA EXISTE UN ANTECEDENTE EN EL LISTADO');
                    return false;
                }
            }*/
            break;

        case 'listSignosVitales':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('txtTemperatura') == '') { alert(' FALTA ESCOGER TEMPERATURA ! ! !'); return false; }
            if (valorAtributo('txtSistolica') == '') { alert(' FALTA ESCOGER SISTOLICA ! ! !'); return false; }
            if (valorAtributo('txtDiastolica') == '') { alert(' FALTA ESCOGER DIASTOLICA ! ! !'); return false; }
            if (valorAtributo('txtPulso') == '') { alert(' FALTA ESCOGER PULSO ! ! !'); return false; }
            if (valorAtributo('txtRespiracion') == '') { alert(' FALTA ESCOGER RESPIRACION ! ! !'); return false; }
            if (valorAtributo('txtPeso') == '') { alert(' FALTA ESCOGER PESO ! ! !'); return false; }
            if (valorAtributo('txtTalla') == '') { alert(' FALTA ESCOGER TALLA ! ! !'); return false; }

            var ids = jQuery("#listSignosVitales").getDataIDs(); /*PARA CONSULTA EXTERNA SOLO UNO REGISTRO*/
            if (ids.length > 0) { alert('YA EXISTEN REGISTRO DE UN EXAMEN FISICO ! ! !'); return false; }


            break;

        case 'cerrarTransaccionesDocumentoInv':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            break;

        case 'deDevolucion':
            if (valorAtributo('cmbIdCantidadNovedad') == '') { alert(' FALTA ESCOGER LA CANTIDAD CON NOVEDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdConcepto') == '') { alert(' FALTA ESCOGER EL TIPO DE NOVEDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdCantidadNovedad') > valorAtributo('lblCantidad')) { alert(' LA CANTIDAD CON NOVEDAD NO PUEDE SER MAYOR QUE LA ORDENADA ! ! !'); return false; }

            //if( $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="" ){  alert('El estado del Evento no permite Adicionar, pruebe con NUEVO');  return false; }		
            break;
        case 'dePendienteAAdministradoNovedad':
            if (valorAtributo('cmbIdCantidadNovedad') == '') { alert(' FALTA ESCOGER LA CANTIDAD CON NOVEDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdConcepto') == '') { alert(' FALTA ESCOGER EL TIPO DE NOVEDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdCantidadNovedad') > valorAtributo('lblCantidad')) { alert(' LA CANTIDAD CON NOVEDAD NO PUEDE SER MAYOR QUE LA ORDENADA ! ! !'); return false; }

            //if( $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="" ){  alert('El estado del Evento no permite Adicionar, pruebe con NUEVO');  return false; }		
            break;
        /*        case 'adicionarTrabajoControl':
                    if (valorAtributo('txtNumeroFactura') == '') { alert(' FALTA EL NUMERO DE FACTURA! ! !'); return false; }
                    if (valorAtributo('txtIdPacientee') == '') { alert(' FALTA LA IDENTIFICACION DEL PACIENTE ! ! !'); return false; }
                    if (valorAtributo('txtNumeroOrden') == '') { alert(' FALTA EL NUMERO DE ORDEN ! ! !'); return false; }
                    if (valorAtributo('txtIdMedico') == '') { alert(' FALTA EL MEDICO ! ! !'); return false; }
    
                    break;*/
        case 'tramitar':
            if (valorAtributo('txtPersonaRecibe') == '') { alert(' FALTA CLIENTE ! ! !'); return false; }
            if (valorAtributo('txtNumeroOrden') == '') { alert(' FALTA EL NUMERO DE ORDEN! ! !'); return false; }
            if (valorAtributo('txtIdEvento') == '') { alert(' FALTA SELECCIONAR EL EVENTO ! ! !'); return false; }

            break;
        case 'recibirPedido':
            if (valorAtributo('txtNumeroOrden') == '') { alert(' FALTA EL NUMERO DE ORDEN! ! !'); return false; }

            break;
        case 'entregarAcliente':
            if (valorAtributo('txtClienteRecibe') == '') { alert(' FALTA EL CLIENTE QUE RECIBE! ! !'); return false; }
            if (valorAtributo('txtIdEvento') == '') { alert(' FALTA EL EVENTO! ! !'); return false; }
            if (valorAtributo('txtNumeroOrden') == '') { alert(' FALTA EL NUMERO DE ORDEN! ! !'); return false; }

            break;
        case 'auditarDocumentoClinico':
            if (valorAtributo('lblIdDocumento') == "") { alert('Falta Escojer Documento'); return false; }
            if (valorAtributo('txtAuditor') != '') { alert('Documento ya se encuentra Auditado '); return false; }
            if (valorAtributo('cmbIdEstadoAuditado') == '2') {
                if (document.getElementById('txtObservacionAuditoria').value == '') {
                    alert('DEBE ESCRIBIR UNA OBSERVACION CUANDO SE AUDITA CON ERROR ');
                    return false;
                }
            }
            break;
        case 'cerrarDocumentoClinico':
            if (valorAtributo('lblIdDocumento') == "") { alert('Falta Escojer Documento'); return false; }
            //  if(jQuery("#listOrdenesMedicamentos").getDataIDs().length==0){ alert('Al menos debe haber un Insumo o Medicamento, en listado'); return false;}	 			  


            //if( $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="" ){  alert('El estado del Evento no permite Adicionar, pruebe con NUEVO');  return false; }		
            break;
        case 'listOrdenesMedicamentos':

            if (valorAtributo('lblIdDocumento') == '') { alert('Le falta seleccionar DOCUMENTO CL�NICO '); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('Le falta seleccionar MEDICAMENTO '); return false; }
            if (valorAtributo('cmbIdVia') == '') { alert('Le falta seleccionar  VIA  '); return false; }

            //  if( valorAtributo('lblDosis') == '' ){ alert('Le falta seleccionar cantidad en DOSIS Y ESPECIFICAR HORAS  ');	return false; }	 			  			  			  			  
            if (valorAtributo('cmbIdTipoDosis') == '') { alert('Le falta seleccionar tipo DOSIS  '); return false; }
            if (valorAtributo('cmbIdRepeticionProgramada') == '') { alert('Le falta seleccionar DURACION  '); return false; }
            if (valorAtributo('cmbIdIntervalo') == '') { alert('Le falta seleccionar Intervalo  '); return false; }
            if (valorAtributo('lblIdPaciente') == '') { alert('Le falta seleccionar Paciente '); return false; }
            if (valorAtributo('txtFechaOrdenMed') == '') { alert('Le falta seleccionar FECHA INICIO '); return false; }
            if (valorAtributo('cmbHoraOrdenMed') == '') { alert('Le falta seleccionar HORA DE INICIO  '); return false; }
            //  
            //  if(  valorAtributo('cmbIdIntervalo') > valorAtributo('cmbIdRepeticionProgramada') ){ alert('El Intervalo NO puede ser mayor que la Duracion ::   ');	return false; }					  
            //  		   
            if (valorAtributo('chkInmediato') == 'true') {
                if (!seleccionadoAlgunoTodosLosCombosDeLaDosisOrdenMedica()) {
                    if (valorAtributo('cmbIdRepeticionProgramada') == '1') {
                        if (confirm('Ha seleccionado Inmediato, no se registrar� horas y repetici�n programada ser� un dia, ESTA SEGURO ?')) {
                            $("#cmbIdRepeticionProgramada option[value='1']").attr('selected', 'selected');
                            return true;
                        } else return false;
                    } else {
                        alert('La Duraci�n para Inmediato debe ser igual a UN DIA');
                        return false;
                    }

                } else { alert('Para Inmediato no se debe escoger horas'); return false; }

            } else {
                if (!seleccionadoAlgunoTodosLosCombosDeLaDosisOrdenMedica()) {
                    alert('Debe seleccionar al menos una Hora con Cantidad');
                    return false;

                }

            }
            break;
        case 'listOrdenesMedicamentosModificar':

            if (valorAtributo('lblIdOrden') == '') { alert('Le falta seleccionar La Solicitud a Modificar '); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert('Le falta seleccionar DOCUMENTO CL�NICO '); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('Le falta seleccionar MEDICAMENTO '); return false; }
            if (valorAtributo('cmbIdVia') == '') { alert('Le falta seleccionar  VIA  '); return false; }

            if (valorAtributo('lblDosis') == '') { alert('Le falta seleccionar cantidad en DOSIS Y ESPECIFICAR HORAS  '); return false; }
            if (valorAtributo('cmbIdTipoDosis') == '') { alert('Le falta seleccionar tipo DOSIS  '); return false; }
            if (valorAtributo('cmbIdRepeticionProgramada') == '') { alert('Le falta seleccionar DURACION  '); return false; }
            if (valorAtributo('cmbIdIntervalo') == '') { alert('Le falta seleccionar Intervalo  '); return false; }
            if (valorAtributo('lblIdPaciente') == '') { alert('Le falta seleccionar Paciente '); return false; }
            if (valorAtributo('txtFechaOrdenMed') == '') { alert('Le falta seleccionar FECHA INICIO '); return false; }
            if (valorAtributo('cmbHoraOrdenMed') == '') { alert('Le falta seleccionar HORA DE INICIO  '); return false; }
            // REGLA PARA VALIDAR EL INTERVALO POR:JUAN if( valorAtributo('cmbIdIntervalo') > valorAtributo('cmbIdRepeticionProgramada') ){ alert('El Intervalo NO puede ser mayor que la Duraci�n  ');	return false; }					  			  			   			  			   
            if (valorAtributo('chkInmediato') == 'true') {
                if (!seleccionadoAlgunoTodosLosCombosDeLaDosisOrdenMedica()) {
                    if (valorAtributo('cmbIdRepeticionProgramada') == '1') {
                        if (confirm('Ha seleccionado Inmediato, no se registrar� horas y repetici�n programada ser� un dia, ESTA SEGURO ?')) {
                            $("#cmbIdRepeticionProgramada option[value='1']").attr('selected', 'selected');
                            return true;
                        } else return false;
                    } else {
                        alert('La repetici�n programada para Inmediato debe ser igual a UN DIA');
                        return false;
                    }

                } else { alert('Para Inmediato no se debe escoger horas'); return false; }

            } else {
                if (!seleccionadoAlgunoTodosLosCombosDeLaDosisOrdenMedica()) {
                    alert('Debe seleccionar al menos una Hora con Cantidad');
                    return false;

                }

            }
            break;

        case 'reportarNoConformidad':
            if ($('#drag' + ventanaActual.num).find('#txtDescripEvento').val() == "") { alert('Falta ingresar Descripcion evento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbproceso').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbNoConformidades').val() == "00") { alert('Falta escoger la no conformidad del proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbUbicacionArea').val() == "00") { alert('Falta escoger el Area donde se presenta el evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaEvento').val() == "") { alert('Falta ingresar fecha del evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbHoraEvent').val() == "--") { alert('Falta ingresar hora del evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtMinutEvent').val() == "") { alert('Falta ingresar minuto del evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtMinutEvent').val() > 59 || $('#drag' + ventanaActual.num).find('#txtMinutEvent').val() < 1) { alert('El valor del minuto debe estar entre 1 y 59 '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtEstadoEvent').val() != "") { alert('Evento ya existe, pruebe con Modificar '); return false; }
            //if( $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="" ){  alert('El estado del Evento no permite Adicionar, pruebe con NUEVO');  return false; }		
            break;
        case 'administrarNoConformidad':
            if ($('#drag' + ventanaActual.num).find('#txtConcepNoConforCalidad').val() == "") { alert('Falta ingresar concepto'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbResponsaDestinoEvento').val() == "00") { alert('Falta escoger el persona destinatario'); return false; }
            if ($('#drag' + ventanaActual.num).find('#lblEstadoEvent').html() != "Reportada" && $('#drag' + ventanaActual.num).find('#lblEstadoEvent').html() != "Asignado") { alert('1e. El estado del Evento no permite Modificarlo'); return false; }
            // if( $('#drag'+ventanaActual.num).find('#cmbTipoPlanAccion').val()=="00" ){  alert('Falta escoger plan de acci�n');  return false; }
            //if( $('#drag'+ventanaActual.num).find('#txtFechaIniPlanAccion').val()==""){  alert('Falta ingresar fecha inicio plan de acci�n');  return false; }
            break;
        case 'listPlanAccion':
            // if( $('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html()!=""){ alert('Para adicionar informaci�n pruebe con modificar'); return false; }									 
            if ($('#drag' + ventanaActual.num).find('#cmbTipoPlanAccion').val() == "00") { alert('Falta escoger el Tipo plan acci�n'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtAnalisisCausas').val() == "") { alert('Falta ingresar An�lisis de causas'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaIniPlanAccion').val() == "") { alert('Falta ingresar fecha inicio plan acci�n'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtMetaPlanAccion').val() == "") { alert('Falta ingresar Meta del plan acci�n'); return false; }
            if (jQuery("#" + arg).getDataIDs().length == 0) { alert('Al menos debe haber una tarea en el plan de acci�n'); return false; }
            if (jQuery("#listPersonasPlanAccion").getDataIDs().length == 0) { alert('Al menos debe haber un individuo,  en Personas Tareas del plan de acci�n'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtConcepNoConforCalidad').val() == "") { alert('Falta escribir concepto a la no conformidad '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbResponsaDestinoEvento').val() == "00") { alert('Falta escoger el Responsable'); return false; }


            break;
        case 'listTareasPlanAccion':
            // if( $('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html()!=""){ alert('Para adicionar informaci�n pruebe con modificar'); return false; }									 
            if ($('#drag' + ventanaActual.num).find('#cmbTipoPlanAccion').val() == "00") { alert('Falta escoger el Tipo plan acci�n'); return false; }
            //if( $('#drag'+ventanaActual.num).find('#txtAnalisisCausas').val()=="" ){  alert('Falta ingresar An�lisis de causas');  return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaIniPlanAccion').val() == "") { alert('Falta ingresar fecha inicio plan acci�n'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtMetaPlanAccion').val() == "") { alert('Falta ingresar Meta del plan acci�n'); return false; }
            if (jQuery("#" + arg).getDataIDs().length == 0) { alert('Al menos debe haber una tarea en el plan de acci�n'); return false; }
            if (jQuery("#listPersonasPlanAccionGral").getDataIDs().length == 0) { alert('Al menos debe haber un individuo,  en Personas Tareas del plan de acci�n'); return false; }
            if (jQuery("#listEspinaPescado").getDataIDs().length == 0) { alert('Al menos debe haber un An�lisis,  en espina de pescado'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtConcepNoConforCalidad').val() == "") { alert('Falta escribir concepto a la no conformidad '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbResponsaDestinoEvento').val() == "00") { alert('Falta escoger el Responsable'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbResponsable').val() == "00") { alert('Escoja responsable del plan acci�n'); return false; }

            break;

        case 'reportarEventoAdverso':


            var datoscadena = $('#drag' + ventanaActual.num).find('#txtBusIdPaciente').val().split('-');
            if (!(datoscadena.length == 2 && datoscadena[0] != '' && datoscadena[1] != '')) { alert('Falta ingresar correctamente: Identificaci�n, Apellidos con nombres separados por el signo - \n \nEjemplo:  CC98400895-Andrade Jesus Alfredo '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbUnidadPertenece').val() == "00") { alert('Falta escoger Unidad a la que Pertenece el paciente'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtDescripEvento').val() == "") { alert('Falta ingresar Descripcion evento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtTratamiento').val() == "") { alert('Falta ingresar tratamiento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbproceso').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbClaseEA').val() == "00") { alert('Falta escoger clasificaci�n del evento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbEAProceso').val() == "00") { alert('Falta escoger Listado Evento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbUbicacionArea').val() == "00") { alert('Falta escoger el Area donde se presenta el evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaEvento').val() == "") { alert('Falta ingresar fecha del evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbHoraEvent').val() == "--") { alert('Falta ingresar Turno del evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtEstadoEvent').val() != "") { alert('Evento ya existe, pruebe con Modificar '); return false; }

            var mes = $('#drag' + ventanaActual.num).find('#txtFechaEvento').val().split('/');
            if (mes[1] != $('#drag' + ventanaActual.num).find('#txt_mes_reporte').val()) { alert('Solo puede reportar eventos del mes en curso '); return false; }



            break;
        case 'administrarEventoAdverso':
            //			  if( $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="Reportada" && $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="Asignado"  ){  alert('1e El estado del Evento no permite Modificarlo porque se encuentra en estado diferente a Reportado o Asignado ');  return false; }		
            if ($('#drag' + ventanaActual.num).find('#cmbproceso').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbNoConformidades').val() == "00") { alert('Falta escoger el evento del proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbEventoProceso').val() == "00") { alert('Falta escoger el evento del proceso'); return false; }

            break;
        case 'administrarPlanMejora':
            if ($('#drag' + ventanaActual.num).find('#cmbproceso').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbTipoPlan').val() == "00") { alert('Falta escoger elTipo de plan'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtAspectoMejorar').val() == "") { alert('Falta ingresar aspecto a mejorar'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbRiesgo').val() == "00") { alert('Falta escoger Riesgo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbCosto').val() == "00") { alert('Falta escoger Costo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbVolumen').val() == "00") { alert('Falta escoger Volumen'); return false; }
            break;
        case 'usuario':
            if ($('#drag' + ventanaActual.num).find('#cmbTipoid').val() == "" || $('#drag' + ventanaActual.num).find('#txtId').val() == "" || $('#drag' + ventanaActual.num).find('#txtLogin').val() == "" || $('#drag' + ventanaActual.num).find('#txtContrasena').val() == "" || $('#drag' + ventanaActual.num).find('#txtContrasena2').val() == "" || $('#drag' + ventanaActual.num).find('#cmbRol').val() == "") {
                alert("Debe de ingresar datos Completos");
                return false;
            } else if ($('#drag' + ventanaActual.num).find('#txtContrasena').val() != $('#drag' + ventanaActual.num).find('#txtContrasena2').val()) {
                alert("La confirmacion de la contraseña no es igual a la contraseña");
                $('#drag' + ventanaActual.num).find('#txtContrasena').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena2').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena2').focus();
                return false;
            } else if ($('#drag' + ventanaActual.num).find('#lblNomPersona').html() == '') {
                alert("Antes de editar informacion, debe de buscar un usuario");
                $('#drag' + ventanaActual.num).find('#txtId').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena2').val('');
                $('#drag' + ventanaActual.num).find('#txtLogin').val('');
                $("#cmbRol option[value='']").attr('selected', 'selected');
                $('#drag' + ventanaActual.num).find('#txtId').focus();
                return false;
            } else if ($('#drag' + ventanaActual.num).find('#hddId').val() != $('#drag' + ventanaActual.num).find('#cmbTipoIdUsua').val() || $('#drag' + ventanaActual.num).find('#hddTipoIdUsua').val() != $('#drag' + ventanaActual.num).find('#txtId').val()) {
                alert("El Tipo o la Identificacion, no pertenecen a " + $('#drag' + ventanaActual.num).find('#lblNomPersona').html());

                $('#drag' + ventanaActual.num).find('#cmbTipoIdUsua').val($('#drag' + ventanaActual.num).find('#hddId').val());
                $('#drag' + ventanaActual.num).find('#txtId').val($('#drag' + ventanaActual.num).find('#hddTipoIdUsua').val())

                return false;
            }
            break;
        case 'listFactContribu':
            if ($('#drag' + ventanaActual.num).find('#cmbBarreraYDefensa').val() == "00") { alert('Falta escoger barrera y defensa'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbAccionInsegura').val() == "00") { alert('Falta escoger accion insegura'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbAmbito').val() == "00") { alert('Falta escoger el ambito '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbPrevenible').val() == "00") { alert('Falta ingresar prevenible '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbOrganizCultura').val() == "00") { alert('Falta ingresar organizacion y cultura '); return false; }



            if (jQuery("#listFactContribu").getDataIDs().length == 0) { alert('Falta ingresar al listado de factores contributivos '); return false; }
            if (jQuery("#listPersonasOportMejora").getDataIDs().length == 0) { alert('Falta ingresar ACCIONES CORRECTIVAS '); return false; }


            break;
        case 'administrarPlanMejoraEA':
            if ($('#drag' + ventanaActual.num).find('#cmbprocesoEA').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbTipoPlan').val() == "00") { alert('Falta escoger elTipo de plan'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtAspectoMejorar').val() == "") { alert('Falta ingresar aspecto a mejorar'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbRiesgo').val() == "00") { alert('Falta escoger Riesgo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbCosto').val() == "00") { alert('Falta escoger Costo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbVolumen').val() == "00") { alert('Falta escoger Volumen'); return false; }
            break;
        case 'relacionEventoOM':
            if ($('#drag' + ventanaActual.num).find('#cmbprocesoEA').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtAspectoMejorar').val() == "") { alert('Falta ingresar aspecto a mejorar'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbRiesgo').val() == "00") { alert('Falta escoger Riesgo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbCosto').val() == "00") { alert('Falta escoger Costo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbVolumen').val() == "00") { alert('Falta escoger Volumen'); return false; }

            verificaAtributo('cmbProcesoOM');
            verificaAtributo('cmbTipoPlanOM');
            verificaAtributo('txtAspectoMejorarOM');
            verificaAtributo('cmbRiesgoOM');
            verificaAtributo('cmbCostoOM');
            verificaAtributo('cmbVolumenOM');


            break;

        case 'adminItem':

            // if( $('#drag'+ventanaActual.num).find('#lblCodItem').html()!='' ){alert('Item ya creado e identificado, pruebe con modificar'); 	 return false; }	
            if ($('#drag' + ventanaActual.num).find('#txtNomItem').val() == "") { alert('Falta ingresar nombre del Item'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbUnidMed').val() == "00") { alert('Falta escoger unidad de medida'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbServicio').val() == "00") { alert('Falta escoger servicio'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbGrupoItem').val() == "00") { alert('Falta escoger Grupo item'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbClase').val() == "00") { alert('Falta escoger Clase'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbEstado').val() == "00") { alert('Falta escoger Estado'); return false; }
            break;
        case 'guardarAsignacionCitaNoPlaneada':

            $('#drag' + ventanaActual.num).find('#hidd_IdPaciente').val('');
            if ($('#drag' + ventanaActual.num).find('#txtNomItem').val() == "") { alert('Falta ingresar nombre del Item'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbTipoUsuario').val() == "00") { alert('Falta el Tipo Usuario'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbTipoCita').val() == "00") { alert('Falta el Tipo Cita'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbEstadoCita').val() == "00") { alert('Falta el Estado Cita'); return false; }

            if (verificarCamposAutocompletar('txtMunicipio') == false) { return false; }
            if (verificarCamposAutocompletar('txtAdministradora1') == false) { return false; }
            if (verificarCamposAutocompletar('txtAdministradora2') == false) { return false; }


            if ($('#drag' + ventanaActual.num).find('#txtBusIdPaciente').val() != '') {
                var datoscadena = $('#drag' + ventanaActual.num).find('#txtBusIdPaciente').val().split('-');
                if (!(datoscadena.length == 2 && datoscadena[0] != '' && datoscadena[1] != '')) {
                    alert('Falta ingresar correctamente: Identificaci�n, Apellidos con nombres separados por el signo - \n \nEjemplo:  CC98400895-Andrade Jesus Alfredo ');
                    return false;
                } else $('#drag' + ventanaActual.num).find('#hidd_IdPaciente').val(datoscadena[0]);

            } else {
                if ($('#drag' + ventanaActual.num).find('#txtNombre1').val() == "") { alert('Falta ingresar Primer Nombre'); return false; }
                if ($('#drag' + ventanaActual.num).find('#txtApellido1').val() == "") { alert('Falta ingresar Primer Apellido'); return false; }
                if ($('#drag' + ventanaActual.num).find('#cmbTipoId').val() == "00") { alert('Falta el Tipo Documento'); return false; }
                if ($('#drag' + ventanaActual.num).find('#txtIdPaciente').val() == "") {
                    alert('Falta ingresar Identificaci�n');
                    return false;
                } else $('#drag' + ventanaActual.num).find('#hidd_IdPaciente').val($('#drag' + ventanaActual.num).find('#cmbTipoId').val() + $('#drag' + ventanaActual.num).find('#txtIdPaciente').val());
            }
            break;
        case 'guardarAsignacionCitaNoPlaneadaListaEspera':

            $('#drag' + ventanaActual.num).find('#hidd_IdPacienteListaEspera').val('');
            if ($('#drag' + ventanaActual.num).find('#cmbTipoUsuarioListaEspera').val() == "00") { alert('Falta el Tipo Usuario'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbTipoCitaListaEspera').val() == "00") { alert('Falta el Tipo Cita'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbEstadoCitaListaEspera').val() == "00") { alert('Falta el Estado Cita'); return false; }

            if ($('#drag' + ventanaActual.num).find('#cmbProfesionalesListaEspera').val() == "00") { alert('Falta el profesional'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbZonaResidenciaListaEspera').val() == "00") { alert('Falta el Zona Residencia'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbNecesidadListaEspera').val() == "00") { alert('Falta el Grado Necesidad'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbDisponibleListaEspera').val() == "00") { alert('Falta el Disponible'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaNacimientoListaEspera').val() == 'null/null/null') { alert('Falta Fecha nacimiento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaNacimientoListaEspera').val() == '') { alert('Falta Fecha nacimiento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbSexoListaEspera').val() == '00') { alert('Falta Sexo'); return false; }


            if (verificarCamposAutocompletar('txtMunicipioListaEspera') == false) { return false; }
            if (verificarCamposAutocompletar('txtAdministradora1ListaEspera') == false) { return false; }
            if (verificarCamposAutocompletar('txtAdministradora2ListaEspera') == false) { return false; }


            if ($('#drag' + ventanaActual.num).find('#txtBusIdPacienteListaEspera').val() != '') {
                var datoscadena = $('#drag' + ventanaActual.num).find('#txtBusIdPacienteListaEspera').val().split('-');
                if (!(datoscadena.length == 2 && datoscadena[0] != '' && datoscadena[1] != '')) {
                    alert('Falta ingresar correctamente: Identificacion, Apellidos con nombres separados por el signo - \n \nEjemplo:  CC98400895-Andrade Jesus Alfredo ');
                    return false;
                } else $('#drag' + ventanaActual.num).find('#hidd_IdPacienteListaEspera').val(datoscadena[0]);
            } else {
                if ($('#drag' + ventanaActual.num).find('#txtNombre1ListaEspera').val() == "") { alert('Falta ingresar Primer Nombre'); return false; }
                if ($('#drag' + ventanaActual.num).find('#txtApellido1ListaEspera').val() == "") { alert('Falta ingresar Primer Apellido'); return false; }
                if ($('#drag' + ventanaActual.num).find('#cmbTipoIdListaEspera').val() == "00") { alert('Falta el Tipo Documento'); return false; }
                if ($('#drag' + ventanaActual.num).find('#txtIdPacienteListaEspera').val() == "") {
                    alert('Falta ingresar Identificaci�n');
                    return false;
                } else $('#drag' + ventanaActual.num).find('#hidd_IdPacienteListaEspera').val($('#drag' + ventanaActual.num).find('#cmbTipoIdListaEspera').val() + $('#drag' + ventanaActual.num).find('#txtIdPacienteListaEspera').val());
            }
            break;
        case 'listSituaActual':
            if ($('#drag' + ventanaActual.num).find('#txtSituacionActual_' + tabActivo).val() == "") { alert('Campo vacio'); return false; }
            break;
        case 'fichaTecnica':
            if (verificaAtributo('txtNombre') == false) return false;
            if (verificaAtributo('cmbTipo') == false) return false;
            if (verificaAtributo('cmbProceso') == false) return false;
            if (verificaAtributo('txtObjetivo') == false) return false;
            if (verificaAtributo('cmbEstado') == false) return false;
            if (verificaAtributo('cmbPeriodicidad') == false) return false;
            if (verificaAtributo('txtFechaVigenciaIni') == false) return false;
            if (verificaAtributo('cmbOrigenInfo') == false) return false;
            if (verificaAtributo('cmbUnidadMedida') == false) return false;
            if (verificaAtributo('txtDescripcionResponsable') == false) return false;
            if (verificaAtributo('txtLineaBase') == false) return false;
            if (verificaAtributo('txtMeta') == false) return false;

            if ($('#drag' + ventanaActual.num).find('#chkDenominadorRequerido').attr('checked') == true)
                if (verificaAtributo('txtDescripcionDenominador') == false) return false;
            if (verificaAtributo('txtDescripcionNumerador') == false) return false;
            if ($('#drag' + ventanaActual.num).find('#lblTotIndicadores').html() > 1) { alert('! ! !  ATENCION \n Tiene asociado m�s de un indicador, ya no se puede modificar'); return false; }
            break;
        case 'indicador':
            if (verificaAtributo('txtValNumerador') == false) return false;
            if (verificaAtributo('txtValDenominador') == false) return false;
            if (verificaAtributo('txtAnalisis') == false) return false;
            if ($('#drag' + ventanaActual.num).find('#txtValDenominador').val() < 1) { alert('!!!  Denominador no puede ser CERO'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtIdEstado').val() == '1') { alert('!!!  Indicador cerrado, no se puede modificar'); return false; }
            break;
        case 'fichaPlatin':
            if (verificaAtributo('cmbPrograma') == false) return false;
            if (verificaAtributo('txtIdPaciente') == false) return false;
            if (verificaAtributo('txtNomAutCompetente') == false) return false;
            if (verificaAtributo('cmbTipoPlan') == false) return false;
            if (verificaAtributo('txtFechaIniPlatin') == false) return false;
            if (verificaAtributo('txtFechaFinPlatin') == false) return false;
            if (verificaAtributo('cmbTipoPlan') == false) return false;
            break;
        case 'paciente':
            if (verificaAtributo('cmbTipoIdEdit') == false) return false;
            if (verificaAtributo('txtIdPaciente') == false) return false;
            if (verificaAtributo('txtNombre1') == false) return false;
            if (verificaAtributo('txtApellido1') == false) return false;
            if (verificaAtributo('cmbSexo') == false) return false;
            if (verificaAtributo('txtFechaNacimiento') == false) return false;

            if (validarFecha('txtFechaNacimiento', 110) == false) { return false; }

            if (verificaAtributo('txtTelefono') == false) return false;
            if (verificaAtributo('txtMunicipioResi') == false) return false;
            if (verificaAtributo('txtAdministradora1') == false) return false;
            if (verificaAtributo('txtDireccion') == false) return false;
            if (verificaAtributo('txtTelefono') == false) return false;
            break;

        case 'crearPacienteNuevo':

            if (valorAtributo('lblId') != '') {
                alert('DEBE CREAR EL USUARIO DESDE CERO');
                return false;
            }
            if (verificaAtributo('cmbTipoIdEdit') == false) return false;
            if (verificaAtributo('txtIdPaciente') == false) return false;
            if (verificaAtributo('txtNombre1') == false) return false;
            if (verificaAtributo('txtApellido1') == false) return false;
            if (verificaAtributo('cmbSexo') == false) return false;
            if (verificaAtributo('txtFechaNacimiento') == false) return false;

            if (validarFecha('txtFechaNacimiento', 110) == false) { return false; }

            if (verificaAtributo('txtTelefono') == false) return false;
            if (verificaAtributo('txtAdministradora1') == false) return false;
            if (verificaAtributo('txtMunicipioResi') == false) return false;
            if (verificaAtributo('txtDireccion') == false) return false;
            if (verificaAtributo('txtTelefono') == false) return false;
            break;

        case 'pacienteModifica':

            if (valorAtributo('txtIdBusPaciente') == '') { alert('FALTA SELECCIONAR PACIENTE'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtAdministradoraPaciente') == '') { alert('FALTA ADMINISTRADORA'); return false; }

            break;

        case 'btn_nueva_Id':
            if (valorAtributo('lblId') == "") {
                alert('No se puede modificar porque aun no ha sido creado');
                return false;
            }
            if (verificaAtributo('cmbTipoIdNueva') == false) return false;
            if (verificaAtributo('txtIdentificacionNueva') == false) return false;
            if (!confirm('Esta seguro ?')) return false;
            break;

        case 'correccion_id_historico':
            if (valorAtributo('lblId') == "") {
                alert('No se puede modificar porque aun no ha sido creado');
                return false;
            }
            if (verificaAtributo('cmbTipoIdNueva') == false) return false;
            if (verificaAtributo('txtIdentificacionNueva') == false) return false;
            break;
        case 'crearSede':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA SEDE'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('txtDireccion') == '') { alert('DEBE INGRESAR LA DIRECCION'); return false; }
            if (valorAtributo('txtTelefono') == '') { alert('INGRESE TELEFONO'); return false; }
            if (valorAtributo('txtCelular') == '') { alert('INGRESE CELULAR'); return false; }
            if (valorAtributo('txtCodHabilitacion') == '') { alert('INGRESE CODIGO DE HABILITACION'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('DEBE SELECCIONAR  TIENE SERVICIO DE HOMECARE'); return false; }
            if (valorAtributo('cmbSeccional') == '') { alert('DEBE SELECCIONAR LA SECCIONAL'); return false; }
            break;

        case 'crearArea':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL AREA'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('DEBE SELECCIONAR EL SERVICIO'); return false; }
            if (valorAtributo('cmbSede') == '') { alert('DEBE SELECCIONAR LA SEDE'); return false; }
            break;
        case 'crearProfesion':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA PROFESION'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('cmbIdEstado') == '') { alert('DEBE SELECCIONAR TIPO DE PROFESION'); return false; }
            if (valorAtributo('cmbFolio') == '') { alert('DEBE SELECCIONAR SI REALIZA IMPRESION DE FOLIOS'); return false; }
            break;
        case 'modificarProfesion':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA PROFESION'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('cmbIdEstado') == '') { alert('DEBE SELECCIONAR TIPO DE PROFESION'); return false; }
            if (valorAtributo('cmbFolio') == '') { alert('DEBE SELECCIONAR SI REALIZA IMPRESION DE FOLIOS'); return false; }
            break;
        case 'modificarSede':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE LA SEDE'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA SEDE'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('txtDireccion') == '') { alert('DEBE INGRESAR LA DIRECCION'); return false; }
            if (valorAtributo('txtTelefono') == '') { alert('INGRESE TELEFONO'); return false; }
            if (valorAtributo('txtCodHabilitacion') == '') { alert('INGRESE CODIGO DE HABILITACION'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('DEBE SELECCIONAR  TIENE SERVICIO DE HOMECARE'); return false; }
            if (valorAtributo('cmbSeccional') == '') { alert('DEBE SELECCIONAR LA SECCIONAL'); return false; }
            break;

        case 'modificarArea':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA AREA'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL AREA'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('DEBE SELECCIONAR EL SERVICIO'); return false; }
            if (valorAtributo('cmbSede') == '') { alert('DEBE SELECCIONAR LA SEDE'); return false; }
            break;


        case 'crearHabitacion':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA HABITACION'); return false; }
            if (valorAtributo('cmbIdArea') == '') { alert('FALTA SELECCIONAR AREA'); return false; }
            if (valorAtributo('cmbIdTipo') == '') { alert('DEBE SELECCIONAR EL TIPO'); return false; }
            if (valorAtributo('cmbCosto') == '') { alert('DEBE SELECCIONAR EL COSTO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            break;

        case 'modificarHabitacion':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA HABITACION'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA HABITACION'); return false; }
            if (valorAtributo('cmbIdArea') == '') { alert('FALTA AREA'); return false; }
            if (valorAtributo('cmbIdTipo') == '') { alert('DEBE SELECCIONAR EL TIPO'); return false; }
            if (valorAtributo('cmbCosto') == '') { alert('DEBE SELECCIONAR EL CENTRO DE COSTO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            //if (valorAtributo('cmbIdAdmision') == '') { alert('DEBE SELECCIONAR TIPO DE ADMISION'); return false; }
            break;

        case 'crearElemento':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL ELEMENTO'); return false; }
            if (valorAtributo('cmbIdEstado') == '') { alert('DEBE SELECCIONAR EL ESTADO'); return false; }
            if (valorAtributo('cmbIdTipo') == '') { alert('DEBE SELECCIONAR EL TIPO'); return false; }
            if (valorAtributo('cmbHabitacion') == '') { alert('DEBE SELECCIONAR LA HABITACION'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            break;

        case 'modificarElemento':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL ELEMENTO'); return false; }
            if (valorAtributo('cmbIdEstado') == '') { alert('DEBE SELECCIONAR EL ESTADO'); return false; }
            if (valorAtributo('cmbIdTipo') == '') { alert('FALTA TIPO DE ELEMENTO'); return false; }
            if (valorAtributo('cmbHabitacion') == '') { alert('DEBE SELECCIONAR LA HABITACION'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            break;

        case 'crearPerfil':
            if (valorAtributo('txtId') == '') { alert('FALTA EL ID DEL ROL'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('INGRESE  NOMBRE DEL ROL'); return false; }
            if (valorAtributo('txtdefinicion') == '') { alert('DEBE SELECCIONAR  DESCRIPCION DEL ROL'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            break;

        case 'modificarPerfil':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL ROL'); return false; }
            if (valorAtributo('txtdefinicion') == '') { alert('DEBE SELECCIONAR EL ESTADODESCRIPCION DEL ROL'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            break;

    }
    return true;
}

function profesionUsuario() {
    return document.getElementById('lblIdProfesion').lastChild.nodeValue;
}


function ponerFoco(idElem) {
    return $('#drag' + ventanaActual.num).find('#' + idElem).focus();
}

function valorAtributoIdAutoCompletar(idElem) {
    
    if (valorAtributo(idElem) != '') { //alert(valorAtributo(idElem)+' idElem  '+idElem)
        var datoscadena =  valorAtributo(idElem).split('-'); //alert('tiene2: '+datoscadena[1].length)
        if (datoscadena[1] == undefined) {
            //	  alert('ERROR EN EL CAMPO:   '+$('#drag'+ventanaActual.num).find('#'+idElem).val().trim() +'\n\nDEL ELEMENTO:   '+idElem+'\n\nEJEMPLO:    codigo-Nombre elemento') 	
            asignaAtributo(idElem, '', 0)
            ponerFoco(idElem)
            return '';
        } else {
            return datoscadena[0];

        }

    } else return '';
}

function valorAtributoNomAutoCompletar(idElem) {
    if (valorAtributo(idElem) != '') { //alert(valorAtributo(idElem)+' idElem  '+idElem)
        //   	   var datoscadena = valorAtributo(idElem).split('-'); //alert('tiene2: '+datoscadena[1].length)
        var datoscadena = $('#drag' + ventanaActual.num).find('#' + idElem).val().trim().split('-');

        if (datoscadena[1] == undefined) {
            alert('ERROR EN EL CAMPO:   ' + $('#drag' + ventanaActual.num).find('#' + idElem).val().trim() + '\n\nDEL ELEMENTO:   ' + idElem + '\n\nEJEMPLO:    codigo-Nombre elemento')
            asignaAtributo(idElem, '', 0)
            ponerFoco(idElem)
            return '';
        } else {
            return datoscadena[1];

        }
    } else return '';
}

function valorAtributoCombo(idElem) { /*el valor que se ve en el combo escogido*/
    return $('#drag' + ventanaActual.num).find('#' + idElem + ' option:selected').text();
}

function asignaAtributoCombo(idElem, value, text) { /*asignar un nuevo valor al combo*/
    $('#drag' + ventanaActual.num).find('#' + idElem).html('<option value="' + value + '">' + text + '</option>');
    $('#drag' + ventanaActual.num).find('#' + idElem).append('<option value=""></option>');
}


function asignaAtributoCombo2(idElem, value, text) { /*asignar un nuevo valor al combo*/
    $('#drag' + ventanaActual.num).find('#' + idElem).html('<option value="' + value + '">' + text + '</option>');
    //$('#drag'+ventanaActual.num).find('#'+idElem).append('<option value=""></option>' );
}



function IdSesion() {
    return document.getElementById('lblIdUsuarioSesion').lastChild.nodeValue;
}

function IdAuxiliar() {
    return document.getElementById('lblIdAuxiliar').lastChild.nodeValue;
}

function LoginSesion() {
    return document.getElementById('lblLoginUsuarioSesion').lastChild.nodeValue;
}

function IdSede() {
    return document.getElementById('lblIdSede').lastChild.nodeValue;
}
function IdEmpresa() {
    return document.getElementById('lblIdEmpresa').lastChild.nodeValue;
}

function NombreEmpresa() {
    return document.getElementById('lblNomEmpresa').lastChild.nodeValue;
}

function valorAtributoValueCheck(idElem) {
    tipoElem = idElem.substring(0, 3);
    switch (tipoElem) {

        case 'chk':
            if ($('#drag' + ventanaActual.num).find('#' + idElem).attr('checked'))
                return '1';
            else return '0';
            break;

    }
    return true;
}

function valorAtributo(idElem) {
    console.log("id item",idElem)


    if ($('#drag' + ventanaActual.num).find('#' + idElem).val() != undefined) {
        tipoElem = idElem.substring(0, 3);
        

        switch (tipoElem) {
            case 'lbl':
                console.log(($('#drag' + ventanaActual.num).find('#' + idElem).html()).trim())
                return ($('#drag' + ventanaActual.num).find('#' + idElem).html()).trim();
                 
                break;
            case 'txt':
                return encodeURIComponent($('#drag' + ventanaActual.num).find('#' + idElem).val().trim());
                break;
            case 'cmb':
                return ($('#drag' + ventanaActual.num).find('#' + idElem).val()).trim();
                break;
            case 'chk':
                if ($('#drag' + ventanaActual.num).find('#' + idElem).attr('checked'))
                    return 'true';
                else return 'false';
                break;

        }


    } else {
        console.log('No existe elemento: ' + idElem)
        return '';
    }



    return true;
}



function valorAtributoSU(idElem) {
    tipoElem = idElem.substring(0, 3);
    switch (tipoElem) {
        case 'lbl':
            return ($('#drag' + ventanaActual.num).find('#' + idElem).html()).trim();
            break;
        case 'txt':
            return $('#drag' + ventanaActual.num).find('#' + idElem).val().trim();
            break;
        case 'cmb':
            return ($('#drag' + ventanaActual.num).find('#' + idElem).val()).trim();
            break;
        case 'chk':
            if ($('#drag' + ventanaActual.num).find('#' + idElem).attr('checked'))
                return 'true';
            else return 'false';
            break;

    }
    return true;
}

function verificaAtributo(idElem) {
    tipoElem = idElem.substring(0, 3);
    switch (tipoElem) {
        case 'lbl':
            if ($('#drag' + ventanaActual.num).find('#' + idElem).html() == '') { alert('Campo ' + idElem.substring(3, 20) + ' esta vacio'); return false; }
            break;
        case 'txt':
            if ($('#drag' + ventanaActual.num).find('#' + idElem).val() == "") { alert('Campo ' + idElem.substring(3, 20) + ' esta vacio'); return false; }
            break;
        case 'cmb':
            if ($('#drag' + ventanaActual.num).find('#' + idElem).val() == '00' || $('#drag' + ventanaActual.num).find('#' + idElem).val() == '') { alert('Campo ' + idElem.substring(3, 20) + ' esta vacio'); return false; }
            break;
    }
    return true;
}


function asignaAtributo(idElem, registro, activo, callback) {

    tipoElem = idElem.substring(0, 3);
    switch (tipoElem) {

        case 'lbl':
            $('#drag' + ventanaActual.num).find('#' + idElem).html($.trim(registro));
            break;
        case 'txt':
            // id element que tenda como en id fecha se le va a cambiar el tipo y valor
            /*if (idElem.indexOf('Fecha') !== -1) {
                var y = document.getElementById(idElem);
                y.setAttribute("type", "date");
                var date = registro.split("/");
                var date_complete = date[2] + "-" + date[1] + "-" + date[0];
                y.setAttribute("value", date_complete);
            } else {*/
            $('#drag' + ventanaActual.num).find('#' + idElem).val($.trim(registro));
            //}
            break;
        case 'for':
            $('#drag' + ventanaActual.num).find('#' + idElem).val($.trim(registro));
            break;
        case 'cmb':
            $('#drag' + ventanaActual.num).find('#' + idElem).val($.trim(registro));
            $("#" + idElem + " option[value='" + $.trim(registro) + "']").attr('selected', 'selected');
            break;
        case 'chk':
            if (registro == 0)
                $('#drag' + ventanaActual.num).find('#' + idElem).attr('checked', '');
            else $('#drag' + ventanaActual.num).find('#' + idElem).attr('checked', 'checked');
            break;
    }

    if (activo == 0) $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', '');
    else $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', 'disabled');

}

function limpiaAtributo(idElem, activo) { // si es 1 desabilitado

    tipoElem = idElem.substring(0, 3);
    switch (tipoElem) {
        case 'lbl':
            $('#drag' + ventanaActual.num).find('#' + idElem).html('');
            break;
        case 'txt':
            $('#drag' + ventanaActual.num).find('#' + idElem).val('');
            break;
        case 'cmb':
            /* alert('primer valor del combo='+$('#drag'+ventanaActual.num).find('#'+idElem+ ' option:selected').text())

                alert('a= '+document.getElementById(idElem).options.length);
				alert('b= '+document.getElementById(idElem).options[0]);*/


            $("#" + idElem + " option[value='00']").attr('selected', 'selected');
            $("#" + idElem + " option[value='']").attr('selected', 'selected');
            break;
        case 'chk':
            $('#drag' + ventanaActual.num).find('#' + idElem).attr('checked', '');
            break;
    }

    if (activo == 0) $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', '');
    else $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', 'disabled');

}

function habilitar(idElem, activo) { // si es 1 desabilitado

    if (activo == 1) $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', '');
    else $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', 'disabled');

}

function desHabilitar(idElem, activo) { // si es 1 desabilitado

    if (activo == 0) $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', '');
    else $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', 'disabled');

}

function concatenarCodigoNombre(codigo, nombre) {
    var cadena = '';
    if (codigo != '') {
        if (nombre != '') {
            cadena = codigo + '-' + nombre;
        } else cadena = codigo;
    }
    return cadena;
}

function concatenarOpcionSistema(codigo, nombre, opcion) {
    var cadena = '';
    if (codigo != '') {
        if (nombre != '') {
            cadena = codigo + '-' + nombre;
        } if (opcion != '') {
            cadena = codigo + '-' + nombre + '-' + opcion;
        } else cadena = codigo;
    }
    return cadena;
}

function splitIdentificacionNombre(nombre) {
    var cadena = '';
    if (nombre != '') {
        cadena = nombre.split("-")[1]
    }
    return cadena
}

function splitTipoIdNombre(nombre) {
    var cadena = '';
    if (nombre != '') {
        cadena = nombre.split("-")[0]
    }
    return cadena
}

/********************************************************** VENTANA MODAL*********************/
function abrirModal() {

    var ancho = 600;
    var alto = 250;
    // fondo transparente
    // creamos un div nuevo, con dos atributos
    var bgdiv = $('<div>').attr({
        className: 'bgtransparent',
        id: 'bgtransparent'
    });
    // agregamos nuevo div a la pagina
    $('body').append(bgdiv);

    // obtenemos ancho y alto de la ventana del explorer
    var wscr = $(window).width();
    var hscr = $(window).height();
    //establecemos las dimensiones del fondo
    $('#bgtransparent').css("width", wscr);
    $('#bgtransparent').css("height", hscr);

    // ventana modal
    // creamos otro div para la ventana modal y dos atributos
    var moddiv = $('<div>').attr({
        className: 'bgmodal',
        id: 'bgmodal'
    });
    // agregamos div a la pagina
    $('body').append(moddiv);

    // agregamos contenido HTML a la ventana modal
    var contenidoHTML = '<p>Tu contenido HTML aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii</p><button onclick=\"cerrarVentanaModal()\">Cerrarrr</button>';
    // var contenidoHTML = $('#drag'+ventanaActual.num).find('#divEditar2');
    // document.getElementById("ventanasActivas").appendChild(contenedor);
    // var contenidoHTML = document.getElementById('divEditar2');		

    titulo = 'dddd';

    var html = "" +
        "<table cellpadding='3' cellspacing='0' border='0' class='ventana-modal-ventana1'><tr><td class='ventana-modal-barra1' align='center' >" + titulo + "</td><td class='ventana-modal-barra1' >" +
        "<img class='ventana-modal-cerrar' src='/pcweb/utilidades/imagenes/ventana-modal/cerrar.gif' title='Cerrar ventana' onclick='cerrarVentanaModal()'>" +
        "</td></tr><tr><td align='center' colspan='2' background='../utilidades/imagenes/acciones/fondo-aplicacion.gif' ><div id='contenidoModal'></div>"
        //+ "<iframe name='" + nombre + "' src='" + pagina + "' width='100%' height='" + (parseInt(alto) - 30) + "' frameborder='0'></iframe>"

        +
        "</td></tr></table>";


    $('#bgmodal').append(html);
    //			      document.getElementById("drag"+ventanaActual.num).innerHTML = varajaxMenu.responseText;
    //  $('#bgmodal').innerHTML ="dddddddd"

    //     $('#bgmodal').setContenido(document.getElementById('divEditar2'));
    // redimensionamos para que se ajuste al centro y mas
    $(window).resize();
    //   });

    $(window).resize(function () {
        // dimensiones de la ventana del explorer 
        var wscr = $(window).width();
        var hscr = $(window).height();

        // estableciendo dimensiones de fondo
        $('#bgtransparent').css("width", wscr);
        $('#bgtransparent').css("height", hscr);

        // estableciendo tama�o de la ventana modal
        $('#bgmodal').css("width", ancho + 'px');
        $('#bgmodal').css("height", alto + 'px');

        // obtiendo tama�o de la ventana modal
        var wcnt = $('#bgmodal').width();
        var hcnt = $('#bgmodal').height();

        // obtener posicion central
        var mleft = (wscr - wcnt) / 2;
        var mtop = (hscr - hcnt) / 2;

        // estableciendo ventana modal en el centro
        $('#bgmodal').css("left", mleft + 'px');
        $('#bgmodal').css("top", mtop + 'px');
    });

};

function cerrarVentanaModal() {
    // removemos divs creados
    $('#bgmodal').remove();
    $('#bgtransparent').remove();
}



///

function validarFecha(campo, limite) {

    fecha = valorAtributo(campo).replace(/%2F/g, "/");

    fecha = new Date(desformatearFecha(fecha));
    hoy = new Date();

    if (fecha >= hoy) {
        alert('LA FECHA NO PUEDE SER MAYOR AL DIA DE HOY');
        return false;
    }

    if (calcularEdad(fecha) > limite) {
        alert('LA EDAD NO PUEDE SER MAYOR A ' + limite);
        return false;
    }

}

function calcularEdad(fecha) {
    var hoy = new Date();
    var cumpleanos = new Date(fecha);
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }

    return edad;
}


function validaEspacio(e, campo) {
    key = e.keyCode ? e.keyCode : e.which;
    if (key == 32) { return false; }
}

function buscarUsuario(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';

    switch (arg) {
        case 'listGrillaGrupoUsuarios':

            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=598&parametros=";

            add_valores_a_mandar(valorAtributo('txtIdentificacion'));

            $('#drag' + ventanaActual.num).find("#listGrillaGrupoUsuarios").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID ROL', 'ROL', 'DEFINICION'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2), hidden: true },
                    { name: 'id_grupo', index: 'id_grupo', width: anchoP(ancho, 3) },
                    { name: 'descripcion', index: 'descripcion', width: anchoP(ancho, 10) },
                    { name: 'definicion', index: 'definicion', width: anchoP(ancho, 10) }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 350,
                width: ancho,
                caption: "ROLES DE USUARIO",
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaGrupoUsuarios').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdGrupo', datosRow.id_grupo, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaPerfiles':

            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=941&parametros=";

            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));


            $('#drag' + ventanaActual.num).find("#listGrillaPerfiles").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['contador', 'ID_GRUPO', 'ROL', 'DEFINICION', 'VIGENTE'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID_GRUPO', index: 'ID_GRUPO', width: anchoP(ancho, 4) },
                    { name: 'ROL', index: 'ROL', width: anchoP(ancho, 10) },
                    { name: 'DEFINICION', index: 'DEFINICION', width: anchoP(ancho, 10) },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPerfiles').getRowData(rowid);

                    asignaAtributo('txtId', datosRow.ID_GRUPO, 1);
                    asignaAtributo('txtNombre', datosRow.ROL, 0);
                    asignaAtributo('txtdefinicion', datosRow.DEFINICION, 0);
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0);
                    buscarParametros('listGrillaGrupopciones');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;



        case 'listGrillaUsuarios':
            // limpiarDivEditarJuan(arg); 
            //ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=595&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusId'));
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            add_valores_a_mandar(valorAtributo('cmbSE'));
            add_valores_a_mandar(valorAtributo('cmbSE'));

            $('#drag' + ventanaActual.num).find("#listGrillaUsuarios").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'TIPO ID', 'IDENTIFICACION', 'pnombre', 'snombre', 'papellido', 'sapellido', 'NOMBRE PERSONAL', 'id_prof', 'PROFESION', 'registro', 'estado', 'usuario', 'Correo', 'Telefono', 'Sede', 'Estado', 'Firma'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'tipo_id', index: 'tipo_id', width: 20 },
                    { name: 'id', index: 'id', width: 38 },
                    { name: 'pnombre', index: 'pnombre', hidden: true },
                    { name: 'snombre', index: 'snombre', hidden: true },
                    { name: 'papellido', index: 'papellido', hidden: true },
                    { name: 'sapellido', index: 'sapellido', hidden: true },
                    { name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 25) },
                    { name: 'id_profesion', index: 'id_profesion', hidden: true },
                    { name: 'Profesiones', index: 'Profesiones', width: anchoP(ancho, 15) },
                    { name: 'noregistro', index: 'noregistro', hidden: true },
                    { name: 'estado', index: 'estado', hidden: true },
                    { name: 'usuario', index: 'usuario', hidden: true },
                    { name: 'Correo', index: 'Correo', hidden: true },
                    { name: 'Telefono', index: 'Telefono', hidden: true },
                    { name: 'Sede', index: 'Sede', hidden: true },
                    { name: 'Estado', index: 'Estado', hidden: true },
                    { name: 'Firma', index: 'Firma', hidden: true }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: 1050,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaUsuarios').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('cmbTipoId', datosRow.tipo_id, 0);
                    asignaAtributo('txtIdentificacion', datosRow.id, 0);
                    asignaAtributo('txtPNombre', datosRow.pnombre, 0);
                    asignaAtributo('txtSNombre', datosRow.snombre, 0);
                    asignaAtributo('txtPApellido', datosRow.papellido, 0);
                    asignaAtributo('txtSApellido', datosRow.sapellido, 0);

                    asignaAtributo('txtNomPersonal', datosRow.nompersonal, 0);
                    asignaAtributo('cmbIdTipoProfesion', datosRow.id_profesion, 0);
                    asignaAtributo('txtRegistro', datosRow.noregistro, 0);
                    asignaAtributo('cmbVigente', datosRow.estado, 0);
                    asignaAtributo('txtUSuario', datosRow.usuario, 0);
                    asignaAtributo('txtCorreo', datosRow.Correo, 0);
                    asignaAtributo('txtDigitacion', datosRow.Fecha_Digitacion, 0);
                    asignaAtributo('txtTipo', datosRow.Tipo, 0);
                    asignaAtributo('txtPhone', datosRow.Telefono, 0);
                    asignaAtributo('cmbSede', datosRow.Sede, 0);
                    asignaAtributo('cmbEstado', datosRow.Estado, 0);

                    recargarFirmaProfesional(datosRow.Firma);

                    asignaAtributo('lblFirmaProfesional', datosRow.Firma, 0);

                    buscarUsuario('sede');
                    setTimeout(() => {
                        buscarUsuario('listGrillaGrupoUsuarios');
                        setTimeout(() => {
                            buscarUsuario('sedes');
                        }, 200);
                    }, 200);                  
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'sede':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=938&parametros=";

            add_valores_a_mandar(valorAtributo('txtIdentificacion'));

            $('#drag' + ventanaActual.num).find("#sede").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id GR', 'NOMBRE SEDE'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2), hidden: true },
                    { name: 'id_sede', index: 'id_sede', width: anchoP(ancho, 3), hidden: true },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 125,
                width: ancho,
                caption: "SEDES SECUNDARIAS",
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#sede').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdSede', datosRow.id_sede, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        
        case 'sedes':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=982&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdentificacion'));            

            $('#drag' + ventanaActual.num).find("#sedes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id GR', 'NOMBRE SEDE'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2), hidden: true },
                    { name: 'id_sede', index: 'id_sede', width: anchoP(ancho, 3), hidden: true },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 125,
                width: ancho,
                caption: "SEDES",
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosSedeRow = jQuery('#drag' + ventanaActual.num).find('#sedes').getRowData(rowid);
                    estad = 0;
                    //asignaAtributo('lblIdSede', datosSedeRow.id_sede, 0);
                },
                multiselect: true,
                multiboxonly: true
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');            
        break;

    }
}


function mostrarOpcionesAgenda() {
    $("#tituloOpciones").show();
    $("#divOpciones").show();
    $("#divBotonOpciones").hide();
}

function OcultarOpcionesAgenda() {
    $("#tituloOpciones").hide();
    $("#divOpciones").hide();
    $("#divBotonOpciones").show();
}


function ponerTitulo(idElemento, mensaje) {
    msj = String(mensaje);
    document.getElementById(idElemento).title = mensaje;
}

function quitarTitulo(idElemento) {
    document.getElementById(idElemento).removeAttribute("title");
}

function mostrarGrafica() {
    var url = "hc/graficas/grafica.jsp";
    var dimension = 'width=1150,height=952,scrollbars=YES,statusbar=NO,left=150,top=90';
    window.open(url, '', dimension);
}

function graficaPlantilla(idEvolucion, idPlantillaGrafica) {


    var url = "hc/graficas/grafica.jsp?idEvolucion=" + idEvolucion + "&idPlantillaGrafica='" + idPlantillaGrafica + "'";
    var dimension = 'width=1150,height=952,scrollbars=YES,statusbar=NO,left=150,top=90';
    window.open(url, '', dimension);
}

function validarNumeros(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function calcularFechas() {
    function calcularFechas() {

        var numero = valorAtributo('txtDiasIncapacidad').toString();
        var fechaInicial = valorAtributo('txtFechaIncaInicio').toString();
        var fechamod = fechaInicial.replace("%2F", "/");
        var fechamod2 = fechamod.replace("%2F", "/");
        var numero2 = parseInt(numero);
        var fecha200 = new Date(formatoFecha(fechamod2));
        fecha200.setDate(fecha200.getDate() + numero2);
        var fecha201 = fecha200.toLocaleDateString();
        asignaAtributo('txtFechaIncaFin', fecha201, 0);
        asignaAtributo('txtFechaIncaFin2', fecha201, 0);

    }

    function formatoFecha(fecha) {

        var fechaT = fecha.split('/');
        return fechaT[1] + '/' + fechaT[0] + '/' + fechaT[2];

    }
    var numero = valorAtributo('txtDiasIncapacidad').toString();
    var fechaInicial = valorAtributo('txtFechaIncaInicio').toString();
    var fechamod = fechaInicial.replace("%2F", "/");
    var fechamod2 = fechamod.replace("%2F", "/");
    var numero2 = parseInt(numero);
    var fecha200 = new Date(formatoFecha(fechamod2));
    fecha200.setDate(fecha200.getDate() + numero2);
    var fecha201 = fecha200.toLocaleDateString();
    asignaAtributo('txtFechaIncaFin', fecha201, 0);
    asignaAtributo('txtFechaIncaFin2', fecha201, 0);

}

function formatoFecha(fecha) {

    var fechaT = fecha.split('/');
    return fechaT[1] + '/' + fechaT[0] + '/' + fechaT[2];

}

function imprimirEncuesta(params) {

    var nuevaURL = "";
    var idReporte = "HCTM";
    /*if( valorAtributo('lblIdEncuestaPaciente') =='' ){
    var evo= document.getElementById('lblIdDocumento').innerHTML;
    }
    else{
        var evo= id_evolucion;
        var idReporte =tipo_evolucion;
    }*/
    if (valorAtributo('lblIdEncuesta') == 19) {
        nuevaURL = "ireports/hc/generaHC.jsp?reporte=" + idReporte + "&evo=" + valorAtributo('lblIdEncuestaPaciente');
        var dimension = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90';
        if (valorAtributo('lblIdEncuestaPaciente') != '') window.open(nuevaURL, '', dimension);
        else alert('Debe Seleccionar una encuesta')
    } else {
        alert("Encuesta no habilitada para impresion");
    }
    //var nuevaURL="ireports/hc/generaHC.jsp?reporte="+idReporte+"&evo="+evo; 
    // asignaAtributo('txtIdDocumentoVistaPrevia','',0);  
}


function recargarFirmaProfesional(nombre) {

    var imagen = '../paginas/ireports/imagenes/firmas/'+ nombre + '?' + Math.floor((Math.random() * 100) + 1);
    $("#imgFirmaProfesional").attr('src', imagen);

}

function validarImagenFirma() {
    if (valorAtributo('txtIdentificacion') === '') {
        alert('Debe seleccionar un profesional');
        return;
    }

    var nombre = document.getElementById("firma").value;
    var ext = nombre.substring(nombre.length - 3, nombre.length).toLowerCase();
    //Se valida la extension del archivo
    if (ext === "jpg" || ext === "jpeg" || ext === "png") {

        var selectedfile = document.getElementById("firma").files;

        if (selectedfile.length > 0) {
            var imageFile = selectedfile[0];

            //Se transforma la imagen a base64
            var fileReader = new FileReader();
            fileReader.onload = function (fileLoadedEvent) {
                var srcData = fileLoadedEvent.target.result;
                valores_a_mandar = 'base64=' + srcData + "&ext=" + ext + "&firma=" + valorAtributo('lblFirmaProfesional');
                ajaxSubirImagen();

            }

            fileReader.readAsDataURL(imageFile);

        }

    } else {
        alert("Archivo no valido");
        document.getElementById("firma").focus();
    }
}

function ajaxSubirImagen() {
    varajax = crearAjax();
    varajax.open("POST", '/clinica/paginas/accionesXml/crear_imagen_xml.jsp', true);
    varajax.onreadystatechange = respuestaSubirImagen;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);

}

function respuestaSubirImagen() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            MsgAlerta = raiz.getElementsByTagName('MsgAlerta')[0].firstChild.data;

            if (raiz.getElementsByTagName('MsgAlerta')[0].firstChild.data == '') {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
                    alert('Imagen guardada');
                    recargarFirmaProfesional(valorAtributo('lblFirmaProfesional'));
                    document.getElementById("firma").value = '';

                } else {
                    alert("ATENCION !!!  Problemas de conexión con servidor, \n RECTIFIQUE QUE EL ELEMENTO NO ESTE ADICIONADO \n Si el problema persiste reinicie el navegador, verifique o llame a soporte...");
                }
            }
            else alert('PROBLEMAS AL REGISTRAR\n' + MsgAlerta)

        } else {
            alert("ATENCION !!!  Problemas de conexion con servidor, Si el problema persiste reinicie el navegador, verifique o llame a soporte. Fallo=" + varajax.status);
        }
    }
    if (varajax.readyState == 1) {
    }
}

var nom1 = "";
var nom2 = "";
var ape1 = "";
var ape2 = "";
function llenarNombre1()
{
    nom1 = document.getElementById("txtPNombre").value;
    nom1 = nom1.toUpperCase() + " ";
    document.getElementById("txtNomPersonal").value = nom1 + nom2 + ape1 +ape2;
}
/*function llenarNombre2()
{
    nom2 = document.getElementById("txtSNombre").value;
    nom2 = nom2.toUpperCase() + " ";
    document.getElementById("txtNomPersonal").value = nom1 + nom2 + ape1 +ape2;
}*/
function llenarApe1()
{
    ape1 = document.getElementById("txtPApellido").value;
    ape1 = ape1.toUpperCase() + " ";
    document.getElementById("txtNomPersonal").value = nom1 + nom2 + ape1 +ape2;
}
/*function llenarApe2()
{
    ape2 = document.getElementById("txtSApellido").value;
    ape2 = ape2.toUpperCase() + " ";
    document.getElementById("txtNomPersonal").value = nom1 + nom2 + ape1 +ape2;
}*/

function teleconsulta()
{
    var btnVideo = document.getElementById("divVideollamada");
    btnVideo.style.display="BLOCK";
    //var frame = $('#frame');
     //var url = 'https://190.60.242.160:8082';
     //frame.attr('src',url).show();
    //var url = "https://190.60.242.160:8082";
    //window.open(url, '_blank');

}

function ventanaEditar(divPaVentanita)
{
    if(valorAtributo('lblIdDocumento')=='')
    {
        alert('SELECCIONE UNA SOLICITUD');
        return false;
    }
    else
    {
        varajaxMenu = crearAjax();
        valores_a_mandar = "";    
        varajaxMenu.open("POST", '/clinica/paginas/autorizaciones/editar.jsp', true);

        varajaxMenu.onreadystatechange = function () { llenarVentanaEditar(divPaVentanita) };
        varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxMenu.send(valores_a_mandar);
    }
}

function llenarVentanaEditar(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var f = new Date();
            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            var doc = valorAtributo('lblIdDocumento');
            asignaAtributo('txtSolicitud',valorAtributo('lblSolicitud'),1);
            asignaAtributo('txtPaciente',valorAtributo('lblPaciente'),1);
            asignaAtributo('txtMedico',valorAtributo('lblMedico'),1);
            asignaAtributo('cmbTipoDiagnostico',valorAtributo('lblTipoDiagnostico'),0);
            asignaAtributo('txtDiagnosticoP',valorAtributo('lblDiagnosticoP'),0);
            asignaAtributo('txtJustificacion',valorAtributo('lblJustificacion'),0);
            asignaAtributo('lblIdDocumentoV', doc, 0);
            asignaAtributo('txtFecha',f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear(),1);
            asignaAtributo('txtHora',f.getHours() + ":" + f.getMinutes(),1);
            setTimeout(buscarHC('listGrillaProcedimientos'),1000);
            setTimeout(buscarHistoria('listArchivosAdjuntosAnexos'),1000);
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function traerVentanitaFuncionesAT(idLupitaVentanita, accion, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaFuncionesSO.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaFuncionesAT(divPaVentanita, accion) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenartraerVentanitaFuncionesAT(divPaVentanita, accion) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuestaS');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            asignaAtributo("txtAccionVentintaHC", accion, 0)
            limpiarListadosTotales('listGrillaVentana');
        } else {
            alert("Problemas de conexion con servidor: Reinicie el aplicativo " + varajaxMenu.status);
        }
    }
    if (varajaxMenu.readyState == 1) {
        alert("Problemas de conexion con Servidor: Reinicie el aplicativo")
    }
}

function clasificarPaciente()
{
    if(valorAtributo("cmbServicio") == "HD")
    {
        modificarCRUD("crearAdmisionUrkunina");
    }
    else{
        modificarCRUD("clasificarPaciente")
    }
}
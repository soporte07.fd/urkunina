//FUNCIONES PARA CRONOMETRAR EL TIEMPO DE SESION DE UN USUARIO EN UNA PAGINA DETERMINADA

var cont = 1;

function buscarNotificacion(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';

    switch (arg) {
        case 'listGrillaNotificaciones':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=619&parametros=";
            add_valores_a_mandar(IdSesion());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'ID', 'ASUNTO', 'CONTENIDO', 'ID_EMISOR', 'NOMBRE EMISOR', 'FECHA_CREA', 'FECHA_INI', 'FECHA_FIN'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 5) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ASUNTO', index: 'ASUNTO', width: anchoP(ancho, 65) },
                    { name: 'CONTENIDO', index: 'CONTENIDO', hidden: true },
                    { name: 'id_emisor', index: 'id_emisor', hidden: true },
                    { name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 20) },
                    { name: 'fecha_crea', index: 'fecha_crea', width: anchoP(ancho, 10) },
                    { name: 'fecha_ini', index: 'fecha_ini', hidden: true },
                    { name: 'fecha_fin', index: 'fecha_fin', hidden: true },
                ],
                //  pager: jQuery('#pagerGrilla'), 
                height: 170,
                width: ancho,
                onSelectRow: function (rowid) {
                    //           limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;

                    asignaAtributo('lblIdNotificacion', datosRow.ID, 1);
                    buscarNotificacion('listGrillaNotificacionDestino');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaNotificacionDestino':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=621&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdNotificacion'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'id_destino', 'id_notificacion', 'id_persona', 'PERSONA DESTINO', 'ID_ESTADO', 'ESTADO', 'FECHA_RECIBIDA'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id_notifi_destino', index: 'id_notifi_destino', hidden: true },
                    { name: 'id_notificacion', index: 'id_notificacion', hidden: true },
                    { name: 'id_personal_destino', index: 'id_personal_destino', width: anchoP(ancho, 12) },
                    { name: 'persona', index: 'persona', width: anchoP(ancho, 25) },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'estado', index: 'estado', width: anchoP(ancho, 8) },
                    { name: 'fecha_recibida', index: 'fecha_recibida', width: anchoP(ancho, 10) },
                ],
                //  pager: jQuery('#pagerGrilla'), 
                height: 170,
                width: ancho,
                onSelectRow: function (rowid) {
                    //           limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;

                    asignaAtributo('lblIdNotificacionEnviada', datosRow.id_notifi_destino, 1);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaLeerNotificacion':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=616&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdProfesionalesBus'));
            add_valores_a_mandar(valorAtributo('txtAsuntoBus'));
            add_valores_a_mandar(valorAtributo('cmbEstadoBus'));
            add_valores_a_mandar(IdSesion());
            $('#drag' + ventanaActual.num).find("#listGrillaLeerNotificacion").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'ID', 'ID_NOT_DESTINO', 'ASUNTO', 'CONTENIDO', 'NOMBRE EMISOR', 'ID_ESTADO', 'ESTADO'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 5) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_NOT_DESTINO', index: 'ID_NOT_DESTINO', hidden: true },
                    { name: 'ASUNTO', index: 'ASUNTO', width: anchoP(ancho, 75) },
                    { name: 'CONTENIDO', index: 'CONTENIDO', hidden: true },
                    { name: 'NOMBRE_EMISOR', index: 'NOMBRE_EMISOR EMISOR', width: anchoP(ancho, 20) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 10) },
                ],
                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //           limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaLeerNotificacion').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdNotDestino', datosRow.ID_NOT_DESTINO, 1);
                    asignaAtributo('lblNomEmisor', datosRow.NOMBRE_EMISOR, 1);
                    asignaAtributo('lblAsunto', datosRow.ASUNTO, 1);
                    asignaAtributo('lblContenido', datosRow.CONTENIDO, 1);
                    asignaAtributo('lblNomEstado', datosRow.ESTADO, 0);

                    if (datosRow.ID_ESTADO == 'S')
                        habilitar('btn_leer_not', 1)
                    else
                        habilitar('btn_leer_not', 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaLeerNotificacion").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
    }
}

function abrirVentanasNotificadas() {
    cargarMenu('calidad/planMejora/administrarPlanMejora.jsp', '2-1-3', 'administrarPlanMejora', 'administrarPlanMejora', 's', 's', 's', 's', 's', 's')
    alert("Esta noticaci�n, queda registrada como RECIBIDO");
    $('#drag' + ventanaActual.num).find('#txtBusIdEvento').val('00000272');
    buscarJuan('administrarPlanMejora', '/clinica/paginas/accionesXml/buscar_xml.jsp')
    mostrarBuscar()
    //   setTimeout("$('#drag'+ventanaActual.num).find('#txtBusIdEvento').val('E00000099')", 1000);   
    //   setTimeout("buscarJuan('administrarPlanMejora','/clinica/paginas/accionesXml/buscar_xml.jsp')", 5000);  

}








var CronoID = null
var CronoEjecutandose = false
var decimas, segundos, minutos, limite

//.....LLAMADO A SERVIDOR MEDIANTE AJAX
var varajax;


var timerHora = '';

function actualizaTiempo(lahora, elminuto, elsegundo) { /*actualiza homa minuto segundo del servidor y refresca en el lbl del menu*/

    hora = parseInt(lahora);
    minuto = parseInt(elminuto);
    segundo = parseInt(elsegundo);
    segundo++;
    if (segundo == 60) {
        segundo = 0;
        minuto++;
    } else if (segundo == 30) { }
    if (minuto == 60) {
        minuto = 0;
        hora++;
    }
    if (hora == 24) {
        hora = 0;
    }
    lahora = (hora < 10) ? ("0" + hora) : hora;
    elminuto = (minuto < 10) ? ("0" + minuto) : minuto;
    elsegundo = (segundo < 10) ? ("0" + segundo) : segundo;
    document.getElementById("lblHoraServidor").innerHTML = lahora + ":" + elminuto + ":" + elsegundo;
    timerHora = setTimeout("actualizaTiempo(hora,minuto,segundo)", 1000);
}

function iniciarDiccionarioConNotificaciones(limite) { //alert(8888)
    ejecutarAjaxDiccionario()
    setTimeout("consultarNotificacionAutomatica(" + limite + ")", 800);
}

function consultarNotificacionAutomatica(limite) { //alert(9999)
    ejecutarAjaxDiccionario()
    // traerNotificacion('escrita');
    //traerNotificacion(arg, 'escrita2', 9, 'P1');
    // alert('sssss')
    // setTimeout("consultarNotificacionAutomatica(" + limite + ")", 50000); // cada 5 mtos,  aqui el tiempo en dispararce      60000=1 minuto  
    return true;
}

function prueba_notificacion() {

    if (Notification) {

        if (Notification.permission !== "granted") {

            Notification.requestPermission()

        }
        var title = "Fundonar"
        var extra = {

            icon: "",
            body: "pruebaaa"


        }
        var nuevo = "hola"
        var noti = new Notification(title, extra, nuevo)
        noti.onclick = {



        }
        noti.onclose = {


        }
        setTimeout(function () { noti.close() }, 10000)

    }
}

function verificarNotificacionAntesDeGuardar(arg) {
    switch (arg) {
        case 'finalizarFolio':
            traerNotificacion(arg, 'notificacionAlert', 27, valorAtributo('lblIdDocumento'));
            break;

        case 'accionesAlEstadoFolio':
            traerNotificacion(arg, 'notificacionAlert', 26, valorAtributo('lblIdAdmision'));
            break;

        case 'asociarCitasAdmision':
            if(verificarCamposGuardar(arg)){
                var lista_citas = [];

                var ids = $("#citasDisponiblesAsociar").getDataIDs();
    
                for (var i = 0; i < ids.length; i++) {
                    var c = "jqg_citasDisponiblesAsociar_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#citasDisponiblesAsociar").getRowData(ids[i]);
                        lista_citas.push(datosRow.ID_CITA);
                    }
                }
                traerNotificacion(arg, 'notificacionAlert', 25, lista_citas);
            }
            break;

        case 'dejarDisponibleAgendaDetalle':
            traerNotificacion(arg, 'notificacionAlert', 23, valorAtributo('lblIdAgendaDetalle'));
            break;

        case 'eliminarAgendaDetalle':
            traerNotificacion(arg, 'notificacionAlert', 22, valorAtributo('lblIdAgendaDetalle'));
            break;

        case 'crearAdmisionSinEncuesta':
            if (verificarCamposGuardar(arg)) {
                traerNotificacion(arg, 'notificacionAlert', 21, valorAtributo('lblIdPaciente'));
            }
            break;

        case 'crearAdmisionEncuesta':
            if (verificarCamposGuardar(arg)) {
                traerNotificacion(arg, 'notificacionAlert', 21, valorAtributo('lblIdPaciente'));
            }
            break;

        case 'ingresarPacienteURG':
            if (verificarCamposGuardar(arg)) {
                traerNotificacion(arg, 'notificacionAlert', 21, valorAtributo('lblIdPacienteUrgencias'));
            }
            break;

        case 'verificarSoloAnularFactura':
            traerNotificacion(arg, 'notificacionAlert', 19, valorAtributo('lblIdFactura'));
            break;

        case 'verificarAnularFactura':
            traerNotificacion(arg, 'notificacionAlert', 19, valorAtributo('lblIdFactura'));
            break;

        case 'verificarAnularFacturaVentas':
            traerNotificacion(arg, 'notificacionAlert', 19, valorAtributo('lblIdFactura'));
            break;

        case 'verificarFechaPaciente':
            setTimeout(() => {
                traerNotificacion(arg, 'notificacionAlert', 18, valorAtributo('txtFechaPacienteCita'), valorAtributo('lblIdListaEspera'))
            }, 500);
            break;

        case 'verificarMontoContrato':
            traerNotificacion(arg, 'notificacionAlert', 17, valorAtributo('lblIdFactura'));
            break;

        case 'crearSoloFactura':
            traerNotificacion(arg, 'notificacionAlert', 16, valorAtributo('cmbIdTipoRegimen').split('-')[0], valorAtributo('lblIdPlanContratacion'), valorAtributo('lblIdPlanContratacion'));
            break;
 
        case 'validarPlanAdmision':
            traerNotificacion(arg, 'notificacionAlert', 16, valorAtributo('cmbIdTipoRegimen').split('-')[0], valorAtributo('lblIdPlanContratacion'), valorAtributo('lblIdPlanContratacion'));
            break;

        case 'validarCantidadMaximaAgendaDetalle':
            traerNotificacion(arg, 'escrita2', 9, valorAtributoIdAutoCompletar('txtMunicipio'));
            break;

        case 'validarTipoCitaFactura':
            traerNotificacion(arg, 'notificacionAlert', 5, valorAtributoIdAutoCompletar('txtIdBusPaciente'), valorAtributo('cmbTipoAdmision'))
            break;

        case 'validarIdentificacionPaciente':
            traerNotificacion(arg, 'notificacionAlert', 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            break;

        case 'crearAdmision':
            traerNotificacion(arg, 'notificacionAlert', 1, valorAtributo('lblIdCita'));
            break;

        case 'crearAdmisionCirugia':
            traerNotificacion(arg, 'notificacionAlert', 1, valorAtributo('lblIdCita'));
            break;

        case 'verificarDxRelacionados':
            traerNotificacion(arg, 'notificacionAlert', 3, valorAtributo('lblId'), valorAtributo('lblIdElemento'));
            break;

        case 'verificarEmbarazoProcedimiento':
            traerNotificacion(arg, 'notificacionAlert', 4, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdProcedimiento'));
            break;

        case 'verificarEmbarazoAyudaDiagnostica':
            traerNotificacion(arg, 'notificacionAlert', 4, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica'));
            break;

        case 'verificarEmbarazoTerapia':
            traerNotificacion(arg, 'notificacionAlert', 4, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdTerapia'));
            break;

        case 'verificarEmbarazoLaboratorio':
            traerNotificacion(arg, 'notificacionAlert', 4, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdLaboratorio'));
            break;

        case 'existenciaBodegaOptica':
            traerNotificacion(arg, 'notificacionAlert', 6, valorAtributo('cmbIdBodega'), valorAtributo('txtIdBarCode'));
            break;

        case 'existenciaBodegaVentas':
            traerNotificacion(arg, 'notificacionAlert', 6, valorAtributo('lblIdBodega'), valorAtributo('txtIdCodigoBarrasVentas'));
            break;

        case 'crearAdmisionFactura':
            traerNotificacion(arg, 'notificacionAlert', 7, valorAtributo('cmbTipoAdmision'), valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            break;

        case 'crearAdmisionSinFactura':
            traerNotificacion(arg, 'notificacionAlert', 7, valorAtributo('cmbTipoAdmision'), valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            break;

        case 'crearCita':
            sacarValoresColumnasDeGrilla('listCitaCexProcedimientoTraer');
            traerNotificacion(arg, 'notificacionAlert', 11,
                valorAtributo('lblIdAgendaDetalle'),
                valorAtributo('txtColumnaIdProced'),
                valorAtributo('txtColumnaIdSitioQuirur'),
                valorAtributo('lblIdAgendaDetalle'),
                valorAtributo('lblIdPlanContratacion'));
            break;

        case 'validarFechasCrearCita':
            traerNotificacion(arg, 'notificacionAlert', 24,
                valorAtributo('lblIdAgendaDetalle'));
            break;

        case 'crearCitaCirugia':
            sacarValoresColumnasDeGrilla('listCitaCirugiaProcedimiento');
            traerNotificacion(arg, 'notificacionAlert', 11,
                valorAtributo('lblIdAgendaDetalle'),
                valorAtributo('txtColumnaIdProced'),
                valorAtributo('txtColumnaIdSitioQuirur'),
                valorAtributo('lblIdAgendaDetalle'),
                valorAtributo('lblIdPlanContratacion'));
            break;
        case 'verificarCantidadCups':
            traerNotificacion(arg, 'notificacionAlert', 12, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdProcedimiento'));
            break;
        case 'verificarCantidadCups2':
            traerNotificacion(arg, 'notificacionAlert', 12, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica'));
            break;
        case 'verificarCantidadCups3':

            traerNotificacion(arg, 'notificacionAlert', 12, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdTerapia'));

            break;
        case 'verificarCantidadCups4':

            traerNotificacion(arg, 'notificacionAlert', 12, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdLaboratorio'));

            break;


        case 'crearFolio':
            traerNotificacion(arg, 'notificacionAlert', 13,
                valorAtributo('cmbTipoDocumento'),
                valorAtributo('lblIdPaciente'),
                valorAtributo('lblIdPaciente'),
                valorAtributo('lblIdTipoAdmision')
            );
            break;

        case 'verificarLaboratorios':
            traerNotificacion(arg, 'notificacionAlert', 14, valorAtributo('lblIdDocumento'));
            break;

        case 'verificarProcedimientosRepetidos':
            traerNotificacion(arg, 'notificacionAlert', 15,
                valorAtributo('lblIdPaciente'),
                valorAtributo('cmbIdSitioQuirurgico'),
                valorAtributoIdAutoCompletar('txtIdProcedimiento')
            );

            break;

        case 'verificarPersonalCirugia':
            sacarValoresColumnasDeGrillaPersonal('listPersonalCirugia');

            var idProfesion = valorAtributo('txtColumnaIdProfesion');
            if (idProfesion === '') idProfesion = 1;

            traerNotificacion(arg, 'notificacionAlert', 20,
                valorAtributo('cmbTipoAnestesia'),
                idProfesion,
                valorAtributo('lblIdAgendaDetalle')
            );
            break;
    }
}



function traerNotificacion(arg, tipoNotificacion, idQuery, parametro1, parametro2, parametro3, parametro4, parametro5, parametro6) {

    if (parametro2 == undefined) parametro2 = '';
    if (parametro3 == undefined) parametro3 = '';
    if (parametro4 == undefined) parametro4 = '';
    if (parametro5 == undefined) parametro5 = '';
    if (parametro6 == undefined) parametro6 = '';

    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/notificaciones_xml.jsp', true);
    varajaxInit.onreadystatechange = function () {
        respuestatraerNotificacion(arg, tipoNotificacion)
    };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "tipoNotificacion=" + tipoNotificacion;
    valores_a_mandar = valores_a_mandar + "&parametro1=" + parametro1;
    valores_a_mandar = valores_a_mandar + "&parametro2=" + parametro2;
    valores_a_mandar = valores_a_mandar + "&parametro3=" + parametro3;
    valores_a_mandar = valores_a_mandar + "&parametro4=" + parametro4;
    valores_a_mandar = valores_a_mandar + "&parametro5=" + parametro5;
    valores_a_mandar = valores_a_mandar + "&parametro6=" + parametro6;
    valores_a_mandar = valores_a_mandar + "&idQuery=" + idQuery;
    varajaxInit.send(valores_a_mandar);
}

function respuestatraerNotificacion(arg, tipoNotificacion) {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('totalNot') != null) {

                totalNotSinLeer = raiz.getElementsByTagName('totalNot')[0].firstChild.data;
                descripcionMensaje = raiz.getElementsByTagName('descripcionNot')[0].firstChild.data;
                //                 alert('*totalNotSinLeer='+ totalNotSinLeer  +'\n*arg='+arg   +'\n*length='+descripcionMensaje.length   )     

                if (totalNotSinLeer > 0) {
                    switch (tipoNotificacion) {
                        case 'escrita':
                            document.getElementById('lblTotMensajes').lastChild.nodeValue = totalNotSinLeer;
                            document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = 'TIENE ' + totalNotSinLeer + 'SIN LEER';
                            document.getElementById('lblVentanillaPie').lastChild.nodeValue = 'Clic aqui para leer';
                            break;
                        case 'escrita2':
                            mostrar('divNotificacionSuperior')
                            document.getElementById('lblTextoNoti').innerHTML = '' + descripcionMensaje;
                            break;
                        case 'alert':
                            //document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = raiz.getElementsByTagName('descripcion')[0].firstChild.data;
                            document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = descripcionMensaje;
                            break;
                        case 'soloMensaje':
                            document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = descripcionMensaje;
                            break;

                        case 'notificacionAlert':
                            // document.getElementById('lblTotNotifVentanillaAlert').lastChild.nodeValue = descripcionMensaje;
                            document.getElementById('lblTotNotifVentanillaAlert').innerHTML = descripcionMensaje;

                            break;
                    }
                }
                enviarAProcesar(arg, totalNotSinLeer, descripcionMensaje, tipoNotificacion)

            }
        } else {
            alert("ALERTA... hay  problemas en la red, nos desconectamos del servidor: " + varajaxInit.status);
        }
    }
}


function verificarFacturaAnular(functionAnulacion) {
    switch (functionAnulacion) {
        case 'soloAnularFactura':
            verificarNotificacionAntesDeGuardar('verificarSoloAnularFactura')
            break;

        case 'anularFactura':
            verificarNotificacionAntesDeGuardar('verificarAnularFactura')
            break;

        case 'anularFacturaVentas':
            verificarNotificacionAntesDeGuardar('verificarAnularFacturaVentas')
            break;
    }
}

function enviarAProcesar(arg, totalNotSinLeer, descripcionMensaje, tipoNotificacion) {
    ban = 0
    switch (arg) {
        case 'finalizarFolio':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlertaPopupHC(descripcionMensaje);
                    ban = 1;
                }
            }
            if (ban == 0) {
                popupFirmar.inicio(popupFirmar.document.forms['frmFinalizar']);
            }
            break;

        case 'accionesAlEstadoFolio':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('accionesAlEstadoFolio');
            }
            break;

        case 'asociarCitasAdmision':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    modificarCRUD('asociarCitasAdmision');
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('asociarCitasAdmision');
            }
            break;

        case 'dejarDisponibleAgendaDetalle':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('dejarDisponibleAgendaDetalle', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
            }
            break;

        case 'eliminarAgendaDetalle':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('eliminarAgendaDetalle', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
            }
            break;

        case 'crearAdmisionSinEncuesta':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearAdmisionSinEncuesta');
            }
            break;

        case 'crearAdmisionEncuesta':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearAdmisionEncuesta');
            }
            break;

        case 'ingresarPacienteURG':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('ingresarPacienteURG');
            }
            break;

        case 'verificarSoloAnularFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('soloAnularFactura')
            }
            break;

        case 'verificarAnularFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('anularFactura')
            }
            break;

        case 'verificarAnularFacturaVentas':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('anularFacturaVentas')
            }
            break;

        case 'verificarFechaPaciente':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                    limpiaAtributo('txtFechaPacienteCita', 0)
                }
            }
            break;

        case 'validarTipoCitaFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('validarPlanAdmision');
            }
            break;

        case 'validarCantidadMaximaAgendaDetalle':

            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }

            break;

        case 'validarIdentificacionPaciente':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            break;

        case 'crearAdmisionCirugia':
            if (totalNotSinLeer > 0) {
                alert('PACIENTE SE ENCUENTRA EN ESTADO NO ASISTIO O REPROGRAMADO CIRUGIA!!!!')
            } else {

                traerNotificacion('validarTipoCitaCirugia', tipoNotificacion, 5, valorAtributoIdAutoCompletar('txtIdBusPaciente'), valorAtributo('cmbTipoAdmision'))

                //traerNotificacion('tienePdfIdentificacionCirugia', tipoNotificacion, 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            }
            break;

        case 'crearAdmision':
            if (totalNotSinLeer > 0) {
                alert('PACIENTE SE ENCUENTRA EN ESTADO NO ASISTIO O REPROGRAMADO!!!!')
            } else {
                traerNotificacion('validarTipoCita', tipoNotificacion, 5, valorAtributoIdAutoCompletar('txtIdBusPaciente'), valorAtributo('cmbTipoAdmision'))

                //traerNotificacion('tienePdfIdentificacion', tipoNotificacion, 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            }
            break;

        case 'validarTipoCitaCirugia':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta('', tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0)
                traerNotificacion('tienePdfIdentificacionCirugia', tipoNotificacion, 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            break;

        case 'validarTipoCita':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta('', tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0)
                traerNotificacion('tienePdfIdentificacion', tipoNotificacion, 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            //traerNotificacion('tienePdfIdentificacion', tipoNotificacion, 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            break;


        case 'tienePdfIdentificacionCirugia':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta('crearAdmisionCirugia', tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0)
                modificarCRUD('crearAdmisionCirugia');
            break;

        case 'tienePdfIdentificacion':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta('crearAdmision', tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0)
                modificarCRUD('crearAdmision');
            break;

        case 'verificarDxRelacionados':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta('verificarDxRelacionados', tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                //alert('se lo puede eliminar');
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=350&parametros=";
                add_valores_a_mandar(valorAtributo('lblId'));
                add_valores_a_mandar(valorAtributo('txtIdTrans'));
                ajaxModificar();
            }
            break;

        case 'verificarEmbarazoProcedimiento':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosProcedimientoNotificacion');
            }
            break;

        case 'verificarCantidadCups':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                //verificarNotificacionAntesDeGuardar('verificarProcedimientosRepetidos');
                guardarYtraerDatoAlListado('buscarSiEsNoPosProcedimientoNotificacion');
                // verificarNotificacionAntesDeGuardar('verificarEmbarazoProcedimiento');

            }
            break;

        case 'verificarCantidadCups2':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosAyudaDiagnosticaNotificacion');
                verificarNotificacionAntesDeGuardar('verificarEmbarazoAyudaDiagnostica');
            }
            break;

        case 'verificarEmbarazoAyudaDiagnostica':

            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosAyudaDiagnosticaNotificacion');
            }
            break;

        case 'verificarCantidadCups3':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosTerapiaNotificacion');
                verificarNotificacionAntesDeGuardar('verificarEmbarazoTerapia');

            }
            break;

        case 'verificarEmbarazoTerapia':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosTerapiaNotificacion');
            }
            break;

        case 'verificarCantidadCups4':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosLaboratorioNotificacion');
                verificarNotificacionAntesDeGuardar('verificarEmbarazoLaboratorio');
            }
            break;

        case 'verificarEmbarazoLaboratorio':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosLaboratorioNotificacion');
            }
            break;

        case 'existenciaBodegaOptica':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                codigoBarras();
            }
            break;

        case 'existenciaBodegaVentas':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                codigoBarrasVentas();
            }
            break;

        case 'crearSoloFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearSoloFactura')
            }
            break;

        case 'verificarMontoContrato':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlertaPopup(descripcionMensaje);
                    ban = 1;
                }
            }
            if (ban == 0) {
                /*
                SE LLAMA A LA FUNCION 'numeraFactura(frm)' CONTENIDA EN LA VENTANA EMERGENTE 
                :popupFactura: elemento instanciado de window.open
                */
                popupFactura.numeraFactura(popupFactura.document.forms['frmFinalizar'])
            }
            break;

        case 'validarPlanAdmision':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                mostrar('divVentanitaVerificacion')
            }
            break;

        case 'crearAdmisionFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearAdmisionFactura');
            }
            break;

        case 'crearAdmisionSinFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearAdmisionSinFactura');
            }
            break;

        case 'validarFechasCrearCita':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearCita')
            }
            break;

        case 'crearCita':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('validarFechasCrearCita')
            }
            break;

        case 'crearCitaCirugia':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarPersonalCirugia');
                //modificarCRUD('crearCitaCirugia')
            }
            break;

        case 'crearFolio':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                //guardarYtraerDatoAlListado('nuevoDocumentoHC')
                modificarCRUD('crearFolio');
            }
            break;

        case 'verificarLaboratorios':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('elementosDelFolio');
            }
            break;

        case 'verificarProcedimientosRepetidos':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosProcedimientoNotificacion');
                verificarNotificacionAntesDeGuardar('verificarEmbarazoProcedimiento');
            }
            break;

        case 'verificarPersonalCirugia':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearCitaCirugia')
            }
            break;
    }
}

function dibujarVentanitaAlertaPopup(descripcionMensaje) {
    popupFactura.document.getElementById('divNotificacionAlert').style.display = 'block';
    popupFactura.document.getElementById('lblTotNotifVentanillaAlert').innerHTML = descripcionMensaje;
}

function dibujarVentanitaAlertaPopupHC(descripcionMensaje) {
    popupFirmar.document.getElementById('divNotificacionAlert').style.display = 'block';
    popupFirmar.document.getElementById('lblTotNotifVentanillaAlert').innerHTML = descripcionMensaje;
}

function dibujarVentanitaAlerta(funcionCRUD, tipoNotificacion) {

    switch (tipoNotificacion) {

        case ('notificacionAlert'):
            $('#divNotificacionAlert').addClass('cssjas');
            $('#divNotificacionAlert').show("explode", { number: 9 }, 400);
            break;

        case ('escrita2'):
            $('#divNotificacionSuperior').show("", { number: 9 }, 400);
            break;
        default:

            $('#divNotificacion').addClass('cssjas');
            $('#divNotificacion').show("explode", { number: 9 }, 400);
            break;

    }

    // velocidad en que aparece         
    //   setTimeout("cierraNotifica()",5000); // esperar para cerrar la notificacion    
    //$('#divNotificacion').dialog( 'open' );   


    switch (funcionCRUD) {

        case 'verificarDxRelacionados':
            limpTablas('idTableBotonNotificaAlert')
            ocultar('divVentanitaDx');
            break;

        case 'verificarEmbarazoProcedimiento':
            fabricaBotonesEmbarazo(funcionCRUD);
            break;

        case 'verificarEmbarazoAyudaDiagnostica':
            fabricaBotonesEmbarazo(funcionCRUD);
            break;

        case 'verificarEmbarazoTerapia':
            fabricaBotonesEmbarazo(funcionCRUD);
            break;

        case 'verificarEmbarazoLaboratorio':
            fabricaBotonesEmbarazo(funcionCRUD);
            break;

        case 'crearAdmisionCirugia':
            fabricaBotonNotificacion(funcionCRUD);
            break;

        case 'crearAdmision':
            fabricaBotonNotificacion(funcionCRUD);
            break;

        case 'crearAdmisionFactura':
            var id_factura = descripcionMensaje;
            id_factura = id_factura.split('FACTURA:')[1].trim();
            estado_factura = descripcionMensaje.split('<b>')[1].trim().split('</b>')[0].trim();

            asignaAtributo('txtIdFacturaAsociar', id_factura, 0);
            asignaAtributo('txtEstadoFacturaAsociar', estado_factura, 0);
            fabricaBotonesAdmisiones(funcionCRUD);
            break;

        case 'crearFolio':
            fabricaBotonesFolio(funcionCRUD);
            break;

        case 'verificarLaboratorios':
            fabricaBotonesLaboratorios(funcionCRUD);
            break;

        case 'verificarProcedimientosRepetidos':
            fabricaBotonesProcedimientosRepetidos(funcionCRUD);
            break;

        default:
            limpTablas('idTableBotonNotificaAlert');
            break;
    }


}

function cierraNotifica() {
    $('#divNotificacion').hide("explode", { number: 9 }, 1000); // velocidad en que desaparece      
}


function fabricaBotonNotificacion(funcionCRUD) {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");
    TD = document.createElement("TD");
    TD.setAttribute('align', 'center');

    /*Ncampo1 = document.createElement("IMG");
    Ncampo1.setAttribute('src', '/clinica/utilidades/imagenes/acciones/aceptar.png');
    Ncampo1.setAttribute('width', '80');
    Ncampo1.setAttribute('height', '35');
    Ncampo1.setAttribute('align', 'center');
    Ncampo1.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "');$('#divNotificacionAlert').hide()");*/


    boton1 = document.createElement("input");
    boton1.setAttribute('type', 'button');
    boton1.setAttribute('class', 'small button blue');
    boton1.setAttribute('value', 'ACEPTAR');
    boton1.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "');$('#divNotificacionAlert').hide()");

    TD.appendChild(boton1);
    TR.appendChild(TD);
    tabla.appendChild(TR);
}


function fabricaBotonesEmbarazo(funcionCRUD) {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");

    TD1 = document.createElement("TD");
    TD1.setAttribute('align', 'center');
    TD1.setAttribute('width', '33%');

    boton1 = document.createElement("input");
    boton1.setAttribute('type', 'button');
    boton1.setAttribute('class', 'small button blue');
    boton1.setAttribute('value', 'SI');
    boton1.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "Si');$('#divNotificacionAlert').hide()");

    TD2 = document.createElement("TD");
    TD2.setAttribute('align', 'center');
    TD2.setAttribute('width', '33%');

    boton2 = document.createElement("input");
    boton2.setAttribute('type', 'button');
    boton2.setAttribute('class', 'small button blue');
    boton2.setAttribute('value', 'NO');


    switch (funcionCRUD) {

        case 'verificarEmbarazoProcedimiento':
            boton2.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "No');" +
                "guardarYtraerDatoAlListado('buscarSiEsNoPosProcedimientoNotificacion');" +
                "$('#divNotificacionAlert').hide()");
            break;

        case 'verificarEmbarazoAyudaDiagnostica':
            boton2.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "No');" +
                "guardarYtraerDatoAlListado('buscarSiEsNoPosAyudaDiagnosticaNotificacion');" +
                "$('#divNotificacionAlert').hide()");
            break;

        case 'verificarEmbarazoTerapia':
            boton2.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "No');" +
                "guardarYtraerDatoAlListado('buscarSiEsNoPosTerapiaNotificacion');" +
                "$('#divNotificacionAlert').hide()");
            break;

        case 'verificarEmbarazoLaboratorio':
            boton2.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "No');" +
                "guardarYtraerDatoAlListado('buscarSiEsNoPosLaboratorioNotificacion');" +
                "$('#divNotificacionAlert').hide()");
            break;


    }


    TD3 = document.createElement("TD");
    TD3.setAttribute('align', 'center');
    TD3.setAttribute('width', '33%');

    boton3 = document.createElement("input");
    boton3.setAttribute('type', 'button');
    boton3.setAttribute('class', 'small button blue');
    boton3.setAttribute('value', 'NO SABE');
    boton3.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "NoSabe');$('#divNotificacionAlert').hide()");

    TD1.appendChild(boton1);
    TD2.appendChild(boton2);
    TD3.appendChild(boton3);
    TR.appendChild(TD1);
    TR.appendChild(TD2);
    TR.appendChild(TD3);
    tabla.appendChild(TR);
}


function fabricaBotonesAdmisiones(funcionCRUD) {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");

    TD1 = document.createElement("TD");
    TD1.setAttribute('align', 'center');
    TD1.setAttribute('width', '33%');

    if (valorAtributo('txtEstadoFacturaAsociar') != 'ANULADA') {
        boton1 = document.createElement("input");
        boton1.setAttribute('type', 'button');
        boton1.setAttribute('class', 'small button blue');
        boton1.setAttribute('value', 'ASOCIAR A FACTURA');
        boton1.setAttribute('onclick', "modificarCRUD('crearAdmisionAsociada');$('#divNotificacionAlert').hide()");
    }

    TD2 = document.createElement("TD");
    TD2.setAttribute('align', 'center');
    TD2.setAttribute('width', '33%');

    boton2 = document.createElement("input");
    boton2.setAttribute('type', 'button');
    boton2.setAttribute('class', 'small button blue');
    boton2.setAttribute('value', 'CREAR ADMISION CON NUEVA FACTURA');
    boton2.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "');$('#divNotificacionAlert').hide()");

    TD3 = document.createElement("TD");
    TD3.setAttribute('align', 'center');
    TD3.setAttribute('width', '33%');

    boton3 = document.createElement("input");
    boton3.setAttribute('type', 'button');
    boton3.setAttribute('class', 'small button blue');
    boton3.setAttribute('value', 'CANCELAR');
    boton3.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

    TD1.appendChild(boton1);
    TD2.appendChild(boton2);
    TD3.appendChild(boton3);
    TR.appendChild(TD1);
    TR.appendChild(TD2);
    TR.appendChild(TD3);
    tabla.appendChild(TR);
}

function fabricaBotonesProcedimientosRepetidos(funcionCRUD) {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");

    TD1 = document.createElement("TD");
    TD1.setAttribute('align', 'center');
    TD1.setAttribute('width', '33%');

    boton1 = document.createElement("input");
    boton1.setAttribute('type', 'button');
    boton1.setAttribute('class', 'small button blue');
    boton1.setAttribute('value', 'CONTINUAR');
    boton1.setAttribute('onclick', "guardarYtraerDatoAlListado('buscarSiEsNoPosProcedimientoNotificacion');verificarNotificacionAntesDeGuardar('verificarEmbarazoProcedimiento');;$('#divNotificacionAlert').hide()");


    TD3 = document.createElement("TD");
    TD3.setAttribute('align', 'center');
    TD3.setAttribute('width', '33%');

    boton3 = document.createElement("input");
    boton3.setAttribute('type', 'button');
    boton3.setAttribute('class', 'small button blue');
    boton3.setAttribute('value', 'CANCELAR');
    boton3.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

    TD1.appendChild(boton1);
    TD3.appendChild(boton3);
    TR.appendChild(TD1);
    TR.appendChild(TD3);
    tabla.appendChild(TR);
}

function fabricaBotonesFolio(funcionCRUD) {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");

    TD1 = document.createElement("TD");
    TD1.setAttribute('align', 'center');
    TD1.setAttribute('width', '33%');

    boton1 = document.createElement("input");
    boton1.setAttribute('type', 'button');
    boton1.setAttribute('class', 'small button blue');
    boton1.setAttribute('value', 'CREAR FOLIO');
    boton1.setAttribute('onclick', " modificarCRUD('crearFolio');$('#divNotificacionAlert').hide()");



    TD3 = document.createElement("TD");
    TD3.setAttribute('align', 'center');
    TD3.setAttribute('width', '33%');

    boton3 = document.createElement("input");
    boton3.setAttribute('type', 'button');
    boton3.setAttribute('class', 'small button blue');
    boton3.setAttribute('value', 'CANCELAR');
    boton3.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

    TD1.appendChild(boton1);
    TD3.appendChild(boton3);
    TR.appendChild(TD1);
    TR.appendChild(TD3);
    tabla.appendChild(TR);
}



function fabricaBotonesLaboratorios(funcionCRUD) {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");

    TD1 = document.createElement("TD");
    TD1.setAttribute('align', 'center');
    TD1.setAttribute('width', '33%');

    boton1 = document.createElement("input");
    boton1.setAttribute('type', 'button');
    boton1.setAttribute('class', 'small button blue');
    boton1.setAttribute('value', 'FINALIZAR FOLIO');
    boton1.setAttribute('onclick', "guardarYtraerDatoAlListado('elementosDelFolio');$('#divNotificacionAlert').hide()");



    TD2 = document.createElement("TD");
    TD2.setAttribute('align', 'center');
    TD2.setAttribute('width', '33%');

    boton2 = document.createElement("input");
    boton2.setAttribute('type', 'button');
    boton2.setAttribute('class', 'small button blue');
    boton2.setAttribute('value', 'CANCELAR');
    boton2.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

    TD1.appendChild(boton1);
    TD2.appendChild(boton2);
    TR.appendChild(TD2);
    TR.appendChild(TD1);
    tabla.appendChild(TR);
}


//var diccionario = new Array;
var diccionario = '';

function ejecutarAjaxDiccionario() {
    valores_a_mandar = "idQueryCombo=550&cantCondiciones=0";
    varajax2 = crearAjax();
    varajax2.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax2.onreadystatechange = respuestaejecutarAjaxDiccionario;
    varajax2.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax2.send(valores_a_mandar);
}

function respuestaejecutarAjaxDiccionario() {
    if (varajax2.readyState == 4) {
        if (varajax2.status == 200) {
            raiz = varajax2.responseXML.documentElement;
            co = raiz.getElementsByTagName('id');
            no = raiz.getElementsByTagName('nom');
            de = raiz.getElementsByTagName('title');
            if (co.length > 0) {
                for (i = 0; i < co.length; i++) {
                    //diccionario.push(co[i].firstChild.data+'_-_'+no[i].firstChild.data);
                    diccionario = diccionario + co[i].firstChild.data + '_-_' + no[i].firstChild.data + '_-_';
                    //alert(diccionario[i]);
                }
                //alert(diccionario.length)
            } else {
                alert('No hay datos en el diccionario');
            }
        } else {
            alert("ALERTA... hay  problemas en la red, nos desconectamos del servidor: " + varajax2.status);
        }
    }
    if (varajax2.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}



function DetenerCrono() {
    if (CronoEjecutandose)
        clearTimeout(CronoID)
    CronoEjecutandose = false
}

function InicializarCrono() { //inicializa contadores globales
    decimas = 0;
    segundos = 0;
    minutos = 0;
}

function MostrarCrono2(limite) { //incrementa el crono   
    decimas++;

    if (decimas > 9) {
        decimas = 0;
        segundos++;
    }

    if (segundos > 59) {
        segundos = 0;
        minutos++;
    }

    if (minutos > 99) {
        DetenerCrono();
        return true;
    }
    if (segundos == limite) {
        ejecutarAjaxCrono();
        iniciarCronometroNotificaciones(limite);
    }
    CronoID = setTimeout("MostrarCrono(" + limite + ")", 10000); // aqui el tiempo en dispararce 100
    CronoEjecutandose = true;
    return true;
}





// FIN FUNCIONES DE CRONOMETRO DE SESION




/*
 function DetenerCronoCitas(){   
 if(CronoEjecutandose)
 clearTimeout(CronoID)
 CronoEjecutandose = false
 }
 
 function InicializarCronoCitas (){ //inicializa contadores globales
 decimas = 0;   segundos = 0;   minutos = 0;     
 }*/





function dispararCronometro(limite, opcion) {
    switch (opcion) {
        case 'citas':
            if (ventanaActual.opc == 'citas') { // si la ventana actual es la de citas (principal.jsp) entonces dispararce
                if ($('#drag' + ventanaActual.num + ' #divListaEspera').css('display') == 'block' || $('#drag' + ventanaActual.num + ' #divTraerListaEspera').css('display') == 'block') {
                    setTimeout("dispararCronometro(" + limite + ",'citas')", 4000); // pa mantener vivo el asunto si no esta en la ventana de citas  
                } else {
                    llenarCitas();
                    setTimeout("dispararCronometro(" + limite + ",'citas')", 40000); // (40 segundos) aqui el tiempo en dispararce   5000=5 segundos     50000=5 minutos    
                }
            } else {
                setTimeout("dispararCronometro(" + limite + ",'citas')", 20000); // pa mantener vivo el asunto si no esta en la ventana de citas     
            }
            break;
        case 'misCitasHoy':
            if (ventanaActual.opc == 'misCitasHoy') { // si la ventana actual es la de citas (principal.jsp) entonces dispararce
                buscarMisPacienteHoy();
                setTimeout("dispararCronometro(" + limite + ",'misCitasHoy')", 50000);
            }
            break;
    }
}
SELECT
  TODO.id 	C1,
  TODO.nombre_generico C2,
  TODO.nombre c3,
  bod.descripcion c4,
  TODO.existencia c5,
  TODO.valor_unitario c6,
  (TODO.existencia * TODO.valor_unitario ) c7,
  TODO.lote c8,
  CAST (TODO.fecha_vencimiento AS VARCHAR (10) ) c9,
  current_date c10, 
  SPLIT_PART(CAST(TODO.fecha_vencimiento - current_date as VARCHAR(10)),' ',1) c11,
  CASE
    WHEN SPLIT_PART(CAST(TODO.fecha_vencimiento - current_date as VARCHAR(10)),' ',1)::INTEGER < (SELECT dias FROM inventario.semaforo_vencimiento ORDER BY id ASC LIMIT 1) THEN
    	(SELECT semaforo FROM inventario.semaforo_vencimiento ORDER BY id ASC  LIMIT 1)
  	WHEN SPLIT_PART(CAST(TODO.fecha_vencimiento - current_date as VARCHAR(10)),' ',1)::INTEGER >= (SELECT dias FROM inventario.semaforo_vencimiento ORDER BY id ASC LIMIT 1) and SPLIT_PART(CAST(TODO.fecha_vencimiento - current_date as VARCHAR(10)),' ',1)::INTEGER <= (SELECT dias FROM inventario.semaforo_vencimiento ORDER BY id ASC LIMIT 1 OFFSET 1) THEN
		(SELECT semaforo FROM inventario.semaforo_vencimiento ORDER BY id ASC  LIMIT 1)
    WHEN SPLIT_PART(CAST(TODO.fecha_vencimiento - current_date as VARCHAR(10)),' ',1)::INTEGER > (SELECT dias FROM inventario.semaforo_vencimiento ORDER BY id ASC LIMIT 1 OFFSET 1) and SPLIT_PART(CAST(TODO.fecha_vencimiento - current_date as VARCHAR(10)),' ',1)::INTEGER <= (SELECT dias FROM inventario.semaforo_vencimiento ORDER BY id ASC  LIMIT 1 OFFSET 2) THEN
		(SELECT semaforo FROM inventario.semaforo_vencimiento ORDER BY id ASC  LIMIT 1 OFFSET 1)
    ELSE 
    	(SELECT semaforo FROM inventario.semaforo_vencimiento ORDER BY id ASC  LIMIT 1 OFFSET 2)   
    END c12    		
FROM (
     SELECT
        art.id,
        art.nombre_generico,
        clas.nombre,
        be.existencia,
        inv.valor_unitario,
        inv.valor_total,
        be.id_bodega,
        bel.lote,
       COALESCE(bel.fecha_vencimiento,now())::DATE fecha_vencimiento       
     from inventario.vi_articulo_inventario art
     inner join inventario.inventarios inv on art.id = inv.id_articulo
     INNER JOIN inventario.clase clas on art.clase = clas.id
     INNER JOIN inventario.bodegas_existencias be on art.id = be.id_articulo
     LEFT JOIN inventario.bodegas_existencias_lote bel on art.id = bel.id_articulo
     WHERE   inv.existencia > 0
     and  bel.id_bodega <> 6::integer
     AND bel.id_articulo is null

     union all

     SELECT
        art.id,
        art.nombre_generico,
        clas.nombre ,
        be.existencia,
        inv.valor_unitario,
        inv.valor_total,
        be.id_bodega,
        be.lote,
        be.fecha_vencimiento
     from inventario.vi_articulo_inventario art
     inner join inventario.inventarios inv on art.id = inv.id_articulo
     INNER JOIN inventario.clase clas on art.clase = clas.id
     INNER JOIN inventario.bodegas_existencias_lote be on art.id = be.id_articulo
     WHERE  inv.existencia > 0
    and  be.id_bodega <> 6::integer

) TODO
INNER JOIN inventario.bodegas bod on todo.id_bodega = bod.id
where todo.existencia > 0
ORDER BY   bod.descripcion , TODO.id
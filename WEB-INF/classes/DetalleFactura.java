package WebServices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para DetalleFactura complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="DetalleFactura">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoGrupo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Subtotal" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="Descuento" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="TarifaIVA" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="IVA" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="Total" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetalleFactura", propOrder = {
    "codigoGrupo",
    "descripcionConcepto",
    "centroCosto",
    "subtotal",
    "descuento",
    "tarifaIVA",
    "iva",
    "total"
})
public class DetalleFactura {

    @XmlElement(name = "CodigoGrupo", required = true)
    protected String codigoGrupo;
    @XmlElement(name = "Descripcion_concepto", required = true)
    protected String descripcionConcepto;
    @XmlElement(name = "CentroCosto", required = true)
    protected String centroCosto;
    @XmlElement(name = "Subtotal", required = true)
    protected Float subtotal;
    @XmlElement(name = "Descuento")
    protected Float descuento;
    @XmlElement(name = "TarifaIVA")
    protected Float tarifaIVA;
    @XmlElement(name = "IVA")
    protected Float iva;
    @XmlElement(name = "Total", required = true)
    protected Float total;

    public DetalleFactura() {
    }

    public String getCodigoGrupo() {
        return codigoGrupo;
    }

    public void setCodigoGrupo(String codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
    }

    public Float getDescuento() {
        return descuento;
    }

    public void setDescuento(Float descuento) {
        this.descuento = descuento;
    }

    public Float getTarifaIVA() {
        return tarifaIVA;
    }

    public void setTarifaIVA(Float tarifaIVA) {
        this.tarifaIVA = tarifaIVA;
    }

    public Float getIva() {
        return iva;
    }

    public void setIva(Float iva) {
        this.iva = iva;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

     public float getCentroCosto() {
        return total;
    }

    public void setCentroCosto(String centroCosto) {
        this.centroCosto = centroCosto;
    }

    public void setDescripcionConcepto(String descripcionConcepto) {
        this.descripcionConcepto = descripcionConcepto;
    }

    public String getDescripcionConcepto() {
        return descripcionConcepto;
    }

    @Override
    public String toString() {
        return "\nDetalleFactura{" + "codigoGrupo=" + codigoGrupo + ", centroCosto=" + centroCosto + ", subtotal=" + subtotal + ", descuento=" + descuento + ", tarifaIVA=" + tarifaIVA + ", iva=" + iva + ", total=" + total + '}';
    }

}
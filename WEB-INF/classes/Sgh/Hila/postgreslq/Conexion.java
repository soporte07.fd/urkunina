/**
*@(#)Clase Conexion.java version 1.02 2015/12/09
*Copyright(c) 2015 Firmas Digital.
*/
package Sgh.Utilidades;
import Sgh.AdminSeguridad.*;
import Clinica.Utilidades.Constantes;
import java.io.*;
import java.text.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import Clinica.Presentacion.*;


public class Conexion{
    public Usuario usuario;
    public Constantes constantes;
    private StringBuffer sql = new StringBuffer();
    private Connection cn;
  public PreparedStatement ps1;
    public PreparedStatement ps2;
    public PreparedStatement ps3;
    public PreparedStatement ps4;
    public PreparedStatement ps5;
    public PreparedStatement psQ;
    public ResultSet rs1 = null;
  public ResultSet rs2 = null;
  public ResultSet rs3 = null;
  public ResultSet rs4 = null;
  public ResultSet rs5 = null;
  public ResultSet rsQ = null;
    boolean estado=false;
    public boolean errorTrans;
    public boolean exito=true;
    private Mensajes msg; // no usar new() jamas.    s
    public String msgAlerta; // no usar new() jamas.    s
    public boolean queryTabla;

    ArrayList<ConexionVO> resultadoQuery=new ArrayList<ConexionVO>();
    ArrayList<ConexionVO> resultadoQueryCombo=new ArrayList<ConexionVO>();
    public ConexionVO parametroQ, parametroQCombo;

    String url="";
  String user="postgres";
  String pwd="postgres";
  String drv="";

    public boolean imprimirConsola = true;



//    String servidor = "juan\\SQLEXPRESS";

/* FUNDONAR*/
//  String servidor = "localhost";
  String servidor = "192.168.1.44";   /*intranet hila*/
//  String servidor = "SERVIDOR";
//    String servidor = "190.147.111.78";   /*ip publica fundonar*/



/* NOFRODIAL*/
//    String servidor = "ALTERNO\\SQLEXPRESS";
//    String servidor = "192.168.1.170\\SQLEXPRESS";    /*intranet*/
//    String servidor = "179.32.198.128\\SQLEXPRESS";   /* ip publica*/



    String motor = "postgres";
//      String motor = "sqlserver";


    public Conexion(){
        initdb();
        usuario= new Usuario();
        queryTabla = true;
        System.out.println(" ");
        System.out.println("............................................................................................ ");
        System.out.println(" MOTOR       : "+ motor +"\n SERVIDOR    : "+servidor +"\n IMP.CONSOLA : "+imprimirConsola+"\n" );

        traerTodosLosQuerys();
    }

    private void initdb(){

    if(motor.equals("postgres")){
       url="jdbc:postgresql://"+servidor+"/Financiera_Ant";
  //       url="jdbc:postgresql://"+servidor+"/rtecnica";
//          url="jdbc:postgresql://"+servidor+"/clinica";
 //     user="postgres";
//  pwd="271hi321Gate";
     // pwd="postgrespostgres";

      drv="org.postgresql.Driver";
    }
    else if(motor.equals("sqlserver")){
        url="jdbc:sqlserver://"+servidor+";databaseName=CLINICA";  // para SQL SERVER  // ATENCION: se debe colocar el sqljdbc_auth.dll en carpeta de SO windows   181.52.246.33
      user="";
      pwd="integratedSecurity=true";
      drv="com.microsoft.sqlserver.jdbc.SQLServerDriver"; //en Win "sun.jdbc.odbc.JdbcOdbcDriver"
    }

    try{ Class.forName(drv);estado=true;}
    catch(ClassNotFoundException e){System.out.println(e.getMessage());estado=false;}

    try{
           if(motor.equals("postgres"))
              cn = DriverManager.getConnection(url,user,pwd);     //POSTGRES
           else if(motor.equals("sqlserver"))
              cn = DriverManager.getConnection(url+";"+pwd);    //SQL SERVER

           estado=true;
        }
    catch(SQLException ex){ System.out.println(" "+ex.getMessage());estado=false;}
    this.rs1 = null;    this.rs2 = null;    this.rs3 = null;    this.rs4 = null;
    this.rs5 = null;        this.ps1 = null;    this.ps2 = null;    this.ps3 = null;
    this.ps4 = null;    this.ps5 = null;        this.rsQ = null;
  }

   public StringBuffer traerTodosLosQuerys(){

       StringBuffer valor = new StringBuffer();
       valor.delete(0,valor.length());

       if( queryTabla ){
           try{
             StringBuffer sqlQ = new StringBuffer();
             if(this.isEstado()){
               sqlQ.delete(0,sqlQ.length());
               sqlQ.append("select id, query from sw.query");
               this.prepareStatementSEL(Constantes.PS5, sqlQ);
               if(this.selectSQL(Constantes.PS5)){

                  while(this.rs5.next()){
                     parametroQ = new ConexionVO();
                     parametroQ.setId(this.rs5.getInt("id"));
                     parametroQ.setVar2(this.rs5.getString("query"));
                     resultadoQuery.add(parametroQ);
                  }
               }

               sqlQ.delete(0,sqlQ.length());
               sqlQ.append("select id, query from sw.query_combo");
               this.prepareStatementSEL(Constantes.PS5, sqlQ);
               if(this.selectSQL(Constantes.PS5)){

                  while(this.rs5.next()){
                     parametroQCombo = new ConexionVO();
                     parametroQCombo.setId(this.rs5.getInt("id"));
                     parametroQCombo.setVar2(this.rs5.getString("query"));
                     resultadoQueryCombo.add(parametroQCombo);
                  }
               }

               this.cerrarPS(Constantes.PS5);

               int i=0;
             while(i<resultadoQueryCombo.size()){
              parametroQCombo=(ConexionVO)resultadoQueryCombo.get(i);
            i++;
                   // System.out.println(" id= "+parametroQCombo.getId());
                    //if(parametroQCombo.getId().equals(105))
                      //  System.out.println("combooooorrrrrrrrrrrrrrr= "+parametroQCombo.getVar2());
               }



             }
          }catch(SQLException e){
              System.out.println("Errorrr --> clase  -->conexion-- function traerTodosLosQuerys --> SQLException --> " + e.getMessage());
          }
      }
      return  valor;
   }

   public StringBuffer traerElQuery(int idQuery){
      if(this.imprimirConsola){
        System.out.println(" \n \n ID QUERY== " + idQuery );
      }

      StringBuffer valor = new StringBuffer();
      valor.delete(0,valor.length());

      if( queryTabla ){
           /*try{
                 StringBuffer sqlQ = new StringBuffer();
                 if(this.isEstado()){
                   sqlQ.delete(0,sqlQ.length());
                   sqlQ.append("select query from sw.query where id = ? ");
                   this.prepareStatementSEL(Constantes.PS5, sqlQ);
                   this.ps5.setInt(1,idQuery);
                   if(this.selectSQL(Constantes.PS5)){
                      while(this.rs5.next()){
                           valor.append(this.rs5.getString("query"));
                           this.cerrarPS(Constantes.PS5);
                           return  valor;
                      }
                   }
                 }
              }catch(SQLException e){
                  System.out.println("Errorrr --> clase  -->conexion-- function traerElQuery --> SQLException --> " + e.getMessage());
           }  */

           int i=0;
         while(i<resultadoQuery.size()){
            parametroQ=(ConexionVO)resultadoQuery.get(i);
          i++;
                if(parametroQ.getId().equals(idQuery)){
                    valor.append(parametroQ.getVar2());
                     return  valor;
                //    System.out.println("qoqoqoqoqoqoqo= "+parametroQ.getVar2());
                }
           }
      }
      else{
          try{
            FileReader fr = null;
            BufferedReader br = null;
            String contenido = "";
            try{
                String  ruta = "C:\\tomcat\\webapps\\clinica\\WEB-INF\\classes\\Clinica\\Sw\\"+motor+"\\query\\"+Integer.toString(idQuery)+".txt";
                fr = new FileReader( ruta );
                br = new BufferedReader( fr );
                String linea;
                while( ( linea = br.readLine() ) != null ){
                    contenido += linea + "\n";
                }
                valor.append(contenido);
                return  valor;
            }catch( Exception e ){
            }
            finally{
                try{
                    br.close();
                }catch( Exception e ){
                   System.out.println("Error --> clase  -->conexion-- function traerElQuery --> Exception --> " + e.getMessage());
                }
            }
          }catch(Exception e){
               System.out.println("Error --> clase  -->conexion-- function traerElQuery --> Exception --> " + e.getMessage());
          }
      }
      return  valor;
 }
 public StringBuffer traerElQueryCombo(int idQuery){
      if(this.imprimirConsola){
       // System.out.println("__________________________ ID QUERY_COMBO== " + idQuery );
      }

      StringBuffer valor = new StringBuffer();
      valor.delete(0,valor.length());

      if( queryTabla ){
           int i=0;
         while(i<resultadoQueryCombo.size()){
            parametroQCombo=(ConexionVO)resultadoQueryCombo.get(i);
          i++;
                if(parametroQCombo.getId().equals(idQuery)){
                    valor.append(parametroQCombo.getVar2());
                  //   System.out.println("------------------------------------ "+parametroQCombo.getVar2());
                     return  valor;

                }
           }
              //      System.out.println("______________________________________");
      }
      else{
          try{
            FileReader fr = null;
            BufferedReader br = null;
            String contenido = "";
            try{
                String  ruta = "C:\\tomcat\\webapps\\clinica\\WEB-INF\\classes\\Clinica\\Sw\\"+motor+"\\query_combo\\"+Integer.toString(idQuery)+".txt";
                fr = new FileReader( ruta );
                br = new BufferedReader( fr );
                String linea;
                while( ( linea = br.readLine() ) != null ){
                    contenido += linea + "\n";
                }
                valor.append(contenido);
                return  valor;
            }catch( Exception e ){
            }
            finally{
                try{
                    br.close();
                }catch( Exception e ){
                   System.out.println("Error --> clase  -->conexion-- function traerElQuery --> Exception --> " + e.getMessage());
                }
            }
          }catch(Exception e){
               System.out.println("Error --> clase  -->conexion-- function traerElQuery --> Exception --> " + e.getMessage());
          }
      }
      return  valor;
 }

 public StringBuffer traerElQueryVentana(int idQuery){
    if(this.imprimirConsola){
       System.out.println(" ID QUERY= " + idQuery );
    }

       StringBuffer valor = new StringBuffer();
       valor.delete(0,valor.length());

       if( queryTabla){
           try{
             StringBuffer sqlQ = new StringBuffer();
             if(this.isEstado()){
               sqlQ.delete(0,sqlQ.length());
               sqlQ.append("select query from sw.query_ventana where id = ? ");
               this.prepareStatementSEL(Constantes.PS5, sqlQ);
               this.ps5.setInt(1,idQuery);
               if(this.selectSQL(Constantes.PS5)){
                  while(this.rs5.next()){
                       valor.append(this.rs5.getString("query"));
                       this.cerrarPS(Constantes.PS5);
                       return  valor;
                  }
               }
             }
          }catch(SQLException e){
              // System.out.println("Error --> clase  -->conexion-- function traerElQuery --> SQLException --> " + e.getMessage());
          }
      }
      else{
          try{
            FileReader fr = null;
            BufferedReader br = null;
            String contenido = "";
            try{
                String  ruta = "C:\\tomcat\\webapps\\clinica\\WEB-INF\\classes\\Clinica\\Sw\\"+motor+"\\query_ventana"+"\\"+Integer.toString(idQuery)+".txt";
                fr = new FileReader( ruta );
                br = new BufferedReader( fr );
                String linea;
                while( ( linea = br.readLine() ) != null ){
                    contenido += linea + "\n";
                }
                valor.append(contenido);
                return  valor;
            }catch( Exception e ){
            }
            finally{
                try{
                    br.close();
                }catch( Exception e ){
                   System.out.println("Error --> clase  -->conexion-- function traerElQueryVentana --> Exception --> " + e.getMessage());
                }
            }
          }catch(Exception e){
               System.out.println("Error --> clase  -->conexion-- function traerElQueryVentana --> Exception --> " + e.getMessage());
          }
      }
      return  valor;
   }










  public void cerrarPS(int numPS){
        try{
            cerrarRS(numPS);
            switch(numPS){
                case 1: if(this.ps1!=null){
                            this.ps1.close();
                            this.ps1=null;
                        }
                        break;
                case 2: if(this.ps2!=null){
                            this.ps2.close();
                            this.ps2=null;
                        }
                        break;
                case 3: if(this.ps3!=null){
                            this.ps3.close();
                            this.ps3=null;
                        }
                        break;
                case 4: if(this.ps4!=null){
                            this.ps4.close();
                            this.ps4=null;
                        }
                        break;
                case 5: if(this.ps5!=null){
                            this.ps5.close();
                            this.ps5=null;
                        }
                        break;
                case 6: if(this.psQ!=null){
                            this.psQ.close();
                            this.psQ=null;
                        }
                        break;

            }
        }catch(SQLException e){
            System.out.println(e.getMessage()+"Error cerrando  PS"+numPS);
       }
    }
    /* self */
  public void cerrarPS( StatementResultSet srs){
        try{/*
            cerrarRS(numPS);
            switch(numPS){
                case 1: if(this.ps1!=null){
                            this.ps1.close();
                            this.ps1=null;
                        }
                        break;
                case 2: if(this.ps2!=null){
                            this.ps2.close();
                            this.ps2=null;
                        }
                        break;
            } */
            //
            cerrarRS( srs.getRs());
            if(srs.ps!=null){
                srs.ps.close();
                srs.ps=null;
            }
        }catch(SQLException e){
            System.out.println(e.getMessage()+"Error cerrando  PS self"+srs);
       }
    }

/**
*M\E9todo para cerrar los resultSet
*@param numRS N\FAmero del resultSet que se quiere cerrar
*/
    private void cerrarRS(int numRS){
        try{
            switch(numRS){
                case 1: if(this.rs1!=null){
                            this.rs1.close();
                            this.rs1=null;
                        }
                        break;
                case 2: if(this.rs2!=null){
                            this.rs2.close();
                            this.rs2=null;
                        }
                        break;
                case 3: if(this.rs3!=null){
                            this.rs3.close();
                            this.rs3=null;
                        }
                        break;
                case 4: if(this.rs4!=null){
                            this.rs4.close();
                            this.rs4=null;
                        }
                        break;
                case 5: if(this.rs5!=null){
                            this.rs5.close();
                            this.rs5=null;
                        }
                        break;
                case 6: if(this.rsQ!=null){
                            this.rsQ.close();
                            this.rsQ=null;
                        }
                        break;
            }

        }catch(SQLException e){
            System.out.println(e.getMessage()+"Error en clase conexion cerrando  RS"+numRS);
        }

    }
/* self */
    private void cerrarRS( ResultSet rs){
        try{/*
            switch(numRS){
                case 1: if(this.rs1!=null){
                            this.rs1.close();
                            this.rs1=null;
                        }
                        break;
                case 5: if(this.rs5!=null){
                            this.rs5.close();
                            this.rs5=null;
                        }
                        break;
            }*/
            //
            if(rs!=null){
                rs.close();
                rs=null;
            }

        }catch(SQLException e){
            System.out.println(e.getMessage()+"Error en clase conexion cerrando  RS self"+ rs);
        }

    }
/**
 * M\E9todo para realizar una sentencia preparada de: consultas (Query) a la BD
 * @param numPS N\FAmero de preparedStament a utilizar en la consulta
 * @param sql Sentencia sql a ejecutarse
 * */
  public void prepareStatementSEL(int numPS, StringBuffer sql) throws SQLException {

          this.cerrarPS(numPS);
         switch(numPS){
               case 1: this.ps1=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);

                       break;
               case 2: this.ps2=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);

                       break;
               case 3: this.ps3=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);

                       break;
               case 4: this.ps4=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);

                       break;
               case 5: this.ps5=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);

                       break;
               case 6: this.psQ=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);

                       break;

          }
    }
    /* self */
  public void prepareStatementSEL(StatementResultSet srs, StringBuffer sql ) throws SQLException {
          this.cerrarPS(srs);
          /*
         switch(numPS){
               case 1: this.ps1=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
                       break;
               case 2: this.ps2=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
                       break;
               case 3: this.ps3=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
                       break;
               case 4: this.ps4=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
                       break;
               case 5: this.ps5=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
                       break;
          }*/
          srs.ps=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);

    }
/**
 * Metodo para crear una sentencia preparada de: Insert, Delete y Updates en la BD
 * @param numPS N\FAmero de PreparedStatement a utilizar en la operaci\F3n
 * @param sql Sentencia sql a ejecutarse
 */
    public void prepareStatementIDU(int numPS, StringBuffer sql) throws SQLException {
           this.cerrarPS(numPS);
           switch(numPS){
               case 1: this.ps1=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 2: this.ps2=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 3: this.ps3=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 4: this.ps4=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 5: this.ps5=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 6: this.psQ=new LoggableStatement(this.cn,sql.toString());
                       break;
           }

    }
/**/
    public void prepareStatementIDU(StatementResultSet srs, StringBuffer sql) throws SQLException {
           this.cerrarPS(srs);
           /*
           switch(numPS){
               case 1: this.ps1=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 2: this.ps2=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 3: this.ps3=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 4: this.ps4=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 5: this.ps5=new LoggableStatement(this.cn,sql.toString());
                       break;
           } */
           srs.ps=new LoggableStatement(this.cn,sql.toString());
    }

/**
 * Metodo que ejecuta una consulta (Query) en la BD
 * @param numPS N\FAmero del PreparedStatement utilizado en la consulta
*/
 public boolean selectSQL(int numPS){
    boolean rta=false;
    try {
        cerrarRS(numPS);
            switch(numPS){
                case 1:
                      //  System.out.println("XXX=  " +  ((LoggableStatement)this.ps1).getQueryString());
                        this.rs1=this.ps1.executeQuery();
                        rta=true;
                        break;
                case 2:
                      //  System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs2=this.ps2.executeQuery();
                        rta=true;
                        break;
                case 3:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs3=this.ps3.executeQuery();
                        rta=true;
                        break;
                case 4:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs4=this.ps4.executeQuery();
                        rta=true;
                        break;
                case 5:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs5=this.ps5.executeQuery();
                        rta=true;
                        break;
                case 6:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rsQ=this.psQ.executeQuery();
                        rta=true;
                        break;

            }
    }
    catch (SQLException e) {
            System.out.println("--------------------------------------------------------------------------------------------");
      switch(numPS){
                case 1:   System.out.println("Error al ejecutar select 1:\n  " +  ((LoggableStatement)this.ps1).getQueryString() + "\n::exception " +e.getMessage());
                          break;
                case 2:   System.out.println("Error al ejecutar select 2:\n  " +  ((LoggableStatement)this.ps2).getQueryString() + "\n::exception " +e.getMessage());
                          break;
                case 3:   System.out.println("Error al ejecutar select 3:\n  " +  ((LoggableStatement)this.ps3).getQueryString() + "\n::exception " +e.getMessage());
                          break;
                case 4:   System.out.println("Error al ejecutar select 4:\n  " +  ((LoggableStatement)this.ps4).getQueryString() + "\n::exception " +e.getMessage());
                          break;
                case 5:   System.out.println("Error al ejecutar select 5:\n  " +  ((LoggableStatement)this.ps5).getQueryString() + "\n::exception " +e.getMessage());
                          break;
                case 6:   System.out.println("Error al ejecutar select 6:\n  " +  ((LoggableStatement)this.psQ).getQueryString() + "\n::exception " +e.getMessage());
                          break;

            }
            System.out.println("--------------------------------------------------------------------------------------------");
            errorTrans=true;
            exito=false;
           // this.rollback();
             //msg//
             msg.SIS_MENSAJE = msg.obtenerMensajeParaUsuario(e.getMessage());
             msg.SIS_ERROR = e.getErrorCode();
             System.out.println("SQLEX msg:"+msg.SIS_MENSAJE+" "+msg.SIS_ERROR+".");
    }
    return rta;
  }


 /* self */
 public boolean selectSQL( StatementResultSet srs){
    boolean rta=false;
    try {
        //cerrarRS(numPS);
        cerrarRS( srs.getRs());
        /*
            switch(numPS){
                case 1:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps1).getQueryString());
                        this.rs1=this.ps1.executeQuery();
                        rta=true;
                        break;
                case 2:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs2=this.ps2.executeQuery();
                        rta=true;
                        break;
                case 3:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs3=this.ps3.executeQuery();
                        rta=true;
                        break;
                case 4:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs4=this.ps4.executeQuery();
                        rta=true;
                        break;
                case 5:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs5=this.ps5.executeQuery();
                        rta=true;
                        break;

            } */
               //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
               srs.rs=srs.ps.executeQuery();
               rta=true;

    }
    catch (SQLException e) {
         /*
      switch(numPS){
                case 1:   System.out.println("Error al ejecutar select:  " +  ((LoggableStatement)this.ps1).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 2:   System.out.println("Error al ejecutar select:  " +  ((LoggableStatement)this.ps2).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 3:   System.out.println("Error al ejecutar select:  " +  ((LoggableStatement)this.ps3).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 4:   System.out.println("Error al ejecutar select:  " +  ((LoggableStatement)this.ps4).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 5:   System.out.println("Error al ejecutar select:  " +  ((LoggableStatement)this.ps5).getQueryString() + "::exception " +e.getMessage());
                          break;
            } */
            System.out.println("Error al ejecutar SELF select:  " +  ((LoggableStatement)srs.ps).getQueryString() + "::exception " +e.getMessage());
            errorTrans=true;
            exito=false;
            this.rollback();
             //msg//
             msg.SIS_MENSAJE = msg.obtenerMensajeParaUsuario(e.getMessage());
             msg.SIS_ERROR = e.getErrorCode();
             System.out.println("SQLEX msg:"+msg.SIS_MENSAJE+" "+msg.SIS_ERROR+".");
    }
    return rta;
  }

/**
 * Metodo que ejecuta un Insert, Delete o Update en la tabal sw_bitacora
 * @param numPS N\FAmero del PreparedStatement utilizado en la operaci\F3n
*/

public boolean escribirBitacoraIDU(String cadena){
     boolean rta=true;
/*
      try{
             sql.delete(0,sql.length());
             sql.append("INSERT INTO sw_bitacora( id_persona,  query) ");
             sql.append("VALUES (?,?) ");
             this.prepareStatementIDU(6,sql);

             this.psQ.setString(1,usuario.getIdentificacion());
             this.psQ.setString(2,cadena);
             this.iduSQL(6);


       }catch(SQLException e){
           System.out.println("Error --> clase Conexion --> function escribirBitacoraIDU --> SQLException --> "+e.getMessage());
       }catch(Exception e){
          System.out.println("Error --> clase Conexion --> function escribirBitacoraIDU --> Exception --> " + e.getMessage());
       }
       */
   return rta;
}


/**
 * Metodo que ejecuta un Insert, Delete o Update en la BD
 * @param numPS N\FAmero del PreparedStatement utilizado en la operaci\F3n
*/

   public boolean iduSQL(int numPS) {
    this.msgAlerta = "";
        boolean rta = true;
        try {
            switch (numPS) {
                case 1:
                    if (this.ps1.executeUpdate() == 0) {
                        exito = false;
                    }
                    /* else{
                       escribirBitacoraIDU(((LoggableStatement)this.ps1).getQueryString());
                     */
                    if (imprimirConsola) {
                        System.out.println("Query Consola= " + ((LoggableStatement) this.ps1).getQueryString());
                    }

                    /* }*/
                    break;
                case 2:
                    if (this.ps2.executeUpdate() == 0) {
                        exito = false;
                    } else {
                        escribirBitacoraIDU(((LoggableStatement) this.ps1).getQueryString());
                    }
                    break;
                case 3:
                    if (this.ps3.executeUpdate() == 0) {
                        exito = false;
                    } else {
                        escribirBitacoraIDU(((LoggableStatement) this.ps1).getQueryString());
                    }
                    break;
                case 4:
                    if (this.ps4.executeUpdate() == 0) {
                        exito = false;
                    } else {
                        escribirBitacoraIDU(((LoggableStatement) this.ps1).getQueryString());
                    }
                    break;
                case 5:
                    if (this.ps5.executeUpdate() == 0) {
                        exito = false;
                    } else {
                        escribirBitacoraIDU(((LoggableStatement) this.ps1).getQueryString());
                    }
                    break;
                case 6:              // esta opcion no escribe bitacora porque se volveria un ciclo infinito
                    if (this.psQ.executeUpdate() == 0) {
                        exito = false;
                    }
                    break;
            }
        } catch (SQLException e) {
            switch (numPS) {
                case 1:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.ps1).getQueryString() + "::exception " + e.getMessage());
                    break;
                case 2:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.ps2).getQueryString() + "::exception " + e.getMessage());
                    break;
                case 3:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.ps3).getQueryString() + "::exception " + e.getMessage());
                    break;
                case 4:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.ps4).getQueryString() + "::exception " + e.getMessage());
                    break;
                case 5:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.ps5).getQueryString() + "::exception " + e.getMessage());
                    break;
                case 6:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.psQ).getQueryString() + "::exception " + e.getMessage());
                    break;
            }
         //   System.out.println("1.1 CODIGO_POSTGRES = " +e.getSQLState());

            if(e.getSQLState().equals("23505") ){
               this.msgAlerta = "EL REGISTRO YA EXISTE !!!!";
            }
            else if(e.getSQLState().equals("22P02") ){
               this.msgAlerta = "UN VALOR ESTA INCOMPLETO O FALTA DILIGENCIAR !!!!";
            }
            else if(e.getSQLState().equals("22023") ){
               this.msgAlerta = "EL INDICE DE LA COLUMNA ESTA FUERA DEL RANGO !!!!";
            }
            else if(e.getSQLState().equals("23503") ){
               this.msgAlerta = "PROBLEMAS AL RELACIONAR CON ALGUN REGISTRO DEPENDIENTE (LLAVE FORANEA)!!!!";
            }


            e.printStackTrace();
            this.rollback();
            errorTrans = true;
            exito = false;
            rta = false;
            //msg//
//            msg.SIS_MENSAJE = msg.obtenerMensajeParaUsuario(e.getMessage());
///            msg.SIS_ERROR = e.getErrorCode();
            System.out.println("SQLEX msg:" + msg.SIS_MENSAJE + " " + msg.SIS_ERROR + ".");
        }

        return rta;
    }
/* self */
 public boolean iduSQL( StatementResultSet srs){
    boolean rta=true;
    try{/*
      switch(numPS){
                 case 1:
                    if(this.ps1.executeUpdate()==0)
                        exito=false;
                    break;
                 case 2:
                    if(this.ps2.executeUpdate()==0)
                        exito=false;
                    break;
                 case 3:
                    if(this.ps3.executeUpdate()==0)
                        exito=false;
                    break;
                 case 4:
                    if(this.ps4.executeUpdate()==0)
                        exito=false;
                    break;
                 case 5:
                    if(this.ps5.executeUpdate()==0)
                        exito=false;
                    break;
            }*/
            if(srs.ps.executeUpdate()==0)
            exito=false;
            // ESTE MENSAJE INDICA QUE SE EJECUTO CORRECTAMENTE LA INSTRUCCION, PERO NO SE ACTUALIZO NINGUN REGISTRO.
            //msg.SIS_MENSAJE=" INFO(1.1): No se actualizo ningun registro";}
    }
     catch(SQLException e){
          /*
      switch(numPS){
                case 1:   System.out.println("Error al ejecutar:  " +  ((LoggableStatement)this.ps1).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 2:   System.out.println("Error al ejecutar:  " +  ((LoggableStatement)this.ps2).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 3:   System.out.println("Error al ejecutar:  " +  ((LoggableStatement)this.ps3).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 4:   System.out.println("Error al ejecutar:  " +  ((LoggableStatement)this.ps4).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 5:   System.out.println("Error al ejecutar:  " +  ((LoggableStatement)this.ps5).getQueryString() + "::exception " +e.getMessage());
                          break;
             }*/
             //
             System.out.println("Error al ejecutar SELF IDU:  " +  ((LoggableStatement)srs.ps).getQueryString() + "::exception " +e.getMessage());
            e.printStackTrace();
            this.rollback();
      errorTrans=true;
      exito=false;
            rta=false;
             //msg//
             msg.SIS_MENSAJE = msg.obtenerMensajeParaUsuario(e.getMessage());
             msg.SIS_ERROR = e.getErrorCode();
             System.out.println("SQLEX msg:"+msg.SIS_MENSAJE+" "+msg.SIS_ERROR+".");
    }
    return rta;
  }






 /**
  * Metodo que iniciar una transaccion de BD
  */
 public void iniciatransaccion(){
         errorTrans=false;
         exito=true;
         try{
           cn.setAutoCommit(false);
           if( this.imprimirConsola )
              System.out.println("********************INI TRANSACCION");
         }
         catch(SQLException e){
           System.out.println("Error en iniciatransaccion() " + e.getMessage());
           e.printStackTrace();
           exito=false;
           errorTrans=true;
         }
      }

/**
 * M\E9todo que permite finalizar una transaccion de BD
 */
      public void fintransaccion(){
        try{
           if(errorTrans==false){
               cn.commit();
                 if( this.imprimirConsola )
                  System.out.println("********************FIN TRANSACCION");
           errorTrans=false;
           }
           else {cn.rollback();
                errorTrans=true;}
               cn.setAutoCommit(true);
        }
        catch(SQLException e){
           System.out.println("Error en fintransaccion() " + e.getMessage());
           e.printStackTrace();
           exito=false;
           errorTrans=true;
        }

      }

/**
 * M\E9todo que realiza un Rollback(deshacer lo hecho en una transaccion no satisfactoria)
 */

   public void rollback(){
        try{
           cn.rollback();
        }
        catch(SQLException e){
          System.out.println("error en bd.rollback() clase conexion " + e.getMessage());
        }
      }

/**
 * M\E9todo destroy de la clase conexion llama al m\E9todo encargado de cerrar
 * los elementos utilizados para las ejecuciones con BD
 */
    public void destroy(){destroydb();}

/**
 * M\E9todo que cierra los PreparedStatement, ResultSet y la conexion con la BD
 */
    private void destroydb(){
      try{
             this.cerrarRS(1);
             this.cerrarRS(2);
             this.cerrarRS(3);
             this.cerrarRS(4);
             this.cerrarRS(5);
             this.cerrarPS(1);
             this.cerrarPS(2);
             this.cerrarPS(3);
             this.cerrarPS(4);
             this.cerrarPS(5);
             cn.close();
          }
    catch(SQLException ex){ }
  }

/**
 * M\E9todo que retorna el estado de la conexion con la BD
 */
    public boolean isEstado() {
        return estado;
    }

/**
 * m\E9todo mutador que permite cambiar la variable de estado de la conexion de la BD
 */
    public void setEstado(boolean value) {
        estado = value;
    }

/** Adrian --- Para IReports
 * m\E9todo que retorna la variable de conexion
 */
    public Connection getConexion(){
        return this.cn;
    }

    /** establecer o retornar msg, con el mensaje ultimo. **/
  /*  public SinergiaBasic.Utilidades.Mensajes getMsg() {
        return msg;
    }

    public void setMsg(SinergiaBasic.Utilidades.Mensajes value) {
        msg = value;
    }   */

    public java.lang.String getMotor() {
        return motor;
    }

    public void setMotor(java.lang.String value) {
        motor = value;
    }
    public java.lang.String getServidor() {
        return servidor;
    }

    public void setServidor(java.lang.String value) {
        servidor = value;
    }

    public java.lang.String getMsgAlerta() {
        return msgAlerta;
    }

    public void setMsgAlerta(java.lang.String value) {
        msgAlerta = value;
    }

}
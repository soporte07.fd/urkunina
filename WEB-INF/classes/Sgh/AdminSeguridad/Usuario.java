package Sgh.AdminSeguridad;

import Clinica.Utilidades.Constantes;
import Clinica.Utilidades.Fecha;
import Sgh.Presentacion.UsuarioVO;
import Sgh.Utilidades.Conexion;
import Sgh.Utilidades.Des;
import Sgh.Utilidades.LoggableStatement;
import Sgh.Utilidades.PersonaId;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @(#)Clase Conexion.java version 2.0 2019/11/01 Copyright(c) 2019 Firmas
 * Digitales.
 */
public class Usuario {

    public static final String _ESTADO_ACTIVO = "1";
    public static final String _ROL_ADMIN = "1";

    private String rol;
    private String estado;
    private String login;
    private String contrasena;
    private String tipoId;
    private String identificacion;
    private String nombre1;
    private String nombre2;
    private String apellido1;
    private String apellido2;
    private String codigo;
    private String rolUsuarioSession;
    private String idProfesion;
    private String codTiposervicio;

    private String idEmpresa;
    private String tipoIdIdEmpresa;
    private String nomEmpresa;
    private String ipIntranet;
    private String ipPublica;
    private String logo1;
    private String logo2;
    private String logo3;
    private String sistemaOperativo;
    private String rutaRaiz;
    private String idSede;
    private String nomSede;
    private String pid;
    private String profesion;
    private String superuser;
    private int total_sedes;

    private Conexion cn;
    //public PersonaId persoId = new PersonaId();
    //private Des des;

    private StringBuffer sql = new StringBuffer();
    public ArrayList servicios = new ArrayList();
    public ArrayList menu = new ArrayList();

    private String sinertrans_user;

    public Usuario() {

        this.rol = "";
        this.estado = "";
        this.login = "";
        this.contrasena = "";
        this.tipoId = "";
        this.identificacion = "";
        this.nombre1 = "";
        this.nombre2 = "";
        this.apellido1 = "";
        this.apellido2 = "";
        this.codigo = "";
        this.rolUsuarioSession = "";
        this.sinertrans_user = "";
        this.idProfesion = "";
        this.codTiposervicio = "";
        this.servicios.clear();
        this.menu.clear();

        this.idEmpresa = "";
        this.tipoIdIdEmpresa = "";
        this.nomEmpresa = "";
        this.ipIntranet = "";
        this.ipPublica = "";
        this.logo1 = "";
        this.logo2 = "";
        this.logo3 = "";
        this.sistemaOperativo = "";
        this.rutaRaiz = "";
        this.idSede = "";
        this.profesion = "";
        this.superuser = "";
        this.total_sedes = 0;
        pid = "";

    }

    /**
     * *
     * Metodo que consulta si el login y el password ingresados desde la
     * interfaz se encuentran registrados en el sistema para permitir el acceso
     * al sistema
     *
     * @param login
     * @param password
     * @return
     */
    public boolean ingresar(String login, String password) {
        boolean ingreso = false;
        Des des = new Des();
        PersonaId persoId = new PersonaId();

        System.out.println("Estado Conexion: " + cn.isEstado());

        try {
            if (cn.isEstado() && persoId.compararInstit()) {

                sql.delete(0, sql.length());

                sql.append("SELECT p.id,\n"
                        + "       p.pnombre,\n"
                        + "       p.snombre,\n"
                        + "       p.papellido,\n"
                        + "       p.sapellido,\n"
                        + "       p.nompersonal,\n"
                        + "       pro.nombre profesion,\n"
                        + "       p.id_profesion,\n"
                        + "       p.idsede,\n"
                        + "       u.superuser,\n"
                        + "       s.nombre nomSede,\n"
                        + "       e.id_empresa,\n"
                        + "       e.id_tipo_id_tercero,\n"
                        + "       e.razon_social,\n"
                        + "       e.ip_intranet,\n"
                        + "       e.ip_publica,\n"
                        + "       e.logo1,\n"
                        + "       e.logo2,\n"
                        + "       e.logo3,\n"
                        + "       pg_backend_pid() pid\n"
                        + "FROM adm.personal p\n"
                        + "     INNER JOIN adm.profesiones pro ON pro.id = p.id_profesion\n"
                        + "     INNER JOIN adm.usuario u ON u.identificacion = p.id\n"
                        + "     INNER JOIN adm.empresas e ON e.id_empresa = u.id_empresa\n"
                        + "     INNER JOIN adm.sede s ON p.idsede = s.id\n"
                        + "WHERE u.usuario = ? AND\n"
                        + "      u.contrasena = ? AND\n"
                        + "      u.estado = 1 ");

                cn.prepareStatementSEL(Constantes.PS1, sql);

                cn.ps1.setString(1, login);
                cn.ps1.setString(2, des.encriptar(password));

                System.out.println(((LoggableStatement) cn.ps1).getQueryString());

                if (cn.selectSQL(Constantes.PS1)) {
                    if (cn.rs1 != null) {
                        while (cn.rs1.next()) {

                            this.login = login.trim();
                            contrasena = password.trim();
                            rol = "1";
                            rolUsuarioSession = "1";
                            identificacion = cn.rs1.getString("id").trim();
                            nombre1 = cn.rs1.getString("pnombre");
                            nombre2 = cn.rs1.getString("snombre");
                            apellido1 = cn.rs1.getString("papellido");
                            apellido2 = cn.rs1.getString("sapellido");
                            idProfesion = cn.rs1.getString("id_profesion").trim();
                            profesion = cn.rs1.getString("profesion");
                            idSede = cn.rs1.getString("idsede").trim();
                            nomSede = cn.rs1.getString("nomSede").trim();

                            idEmpresa = cn.rs1.getString("id_empresa");
                            tipoIdIdEmpresa = cn.rs1.getString("id_tipo_id_tercero");
                            nomEmpresa = cn.rs1.getString("razon_social");
                            ipIntranet = cn.rs1.getString("ip_intranet");
                            ipPublica = cn.rs1.getString("ip_publica");
                            logo1 = cn.rs1.getString("logo1");
                            logo2 = cn.rs1.getString("logo2");
                            logo3 = cn.rs1.getString("logo3");
                            pid = cn.rs1.getString("pid");
                            superuser = cn.rs1.getString("superuser");

                            rutaRaiz = "/opt/tomcat8/webapps/clinica/";

                            System.out.println("Fecha Ingreso: " + Fecha.getHoyHora());
                            System.out.println("Usuario: " + identificacion + " - " + apellido1 + " " + nombre1);
                            System.out.println("Pid: " + pid);

                            //llenarEmpresa();
                            ingreso = llenarRolesMenu();

                        }
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método ingresar() de Usuario.java");
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método ingresar() de Usuario.java");
        }
        return ingreso;
    }

    /**
     * Metodo para verificar sedes secundarias
     */

    public boolean verificarSedes()
    {
     try
     {
      if (cn.isEstado()) {
                sql.delete(0, sql.length());
                sql.append("SELECT count(*) as c "
                        + "FROM adm.usuario_otras_sedes u \n"
                        + "WHERE u.identificacion = ?");

                cn.prepareStatementSEL(Constantes.PS1, sql);

                cn.ps1.setString(1, identificacion);
                System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    if (cn.rs1 != null) {
                        while (cn.rs1.next()) {
                            total_sedes = cn.rs1.getInt("c");
                            System.out.println(total_sedes);
                            }
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }
     }
     catch (SQLException e) {
            System.out.println(e.getMessage() + " en metodo verificarSedes(), de Usuario.java");
            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en metodo verificarSedes(), de Usuario.java");
            return false;
        }
     if(total_sedes > 1)
     {
     return true;
     }
     else
     {
     return false;
     }
    }

    /**
     * Metodo que trae los menus que tiene acceso el usuario
     *
     * @return
     */
    public boolean llenarRolesMenu() {
        try {
            if (cn.isEstado()) {
                cn.prepareStatementSEL(Constantes.PS2, cn.traerElQuery(321));
                cn.ps2.setString(1, identificacion.trim());
                cn.ps2.setString(2, identificacion.trim());
                if (cn.selectSQL(Constantes.PS2)) {
                    if (cn.rs2 != null) {
                        while (cn.rs2.next()) {
                            menu.add(cn.rs2.getString("id_menu"));
                        }
                    }
                    cn.cerrarPS(Constantes.PS2);
                    System.err.println("11112");
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método llenarRolesMenu(), de Usuario.java");
            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método llenarRolesMenu(), de Usuario.java");
            return false;
        }
        return true;
    }

    /*  public boolean llenarProfesionTipoServicio(){

       try{
         if(cn.isEstado()){
            this.cn.prepareStatementSEL(Constantes.PS2,cn.traerElQuery(321));
            cn.ps2.setString(1,this.identificacion);
            if(cn.selectSQL(Constantes.PS2)){
               if (cn.rs2!=null) {
                   while (cn.rs2.next()){
                       //this.codTiposervicio = cn.rs2.getString("cod_tipo_servicio");
                        this.idProfesion = cn.rs2.getString("id_profesion");
                        System.out.println("    Tiposervicio...... " + this.codTiposervicio+ "  idProfesion= " +this.idProfesion );
                   }
                }
               cn.cerrarPS(Constantes.PS2);
             }
      }
      }catch(SQLException e){
            System.out.println(e.getMessage()+" en método llenarProfesionTipoServicio() de Usuario.java");
            return  false;
      }catch(Exception e){
            System.out.println(e.getMessage()+" en método llenarProfesionTipoServicio() de Usuario.java");
            return  false;
      }
      return  true;
  }*/
    public boolean preguntarMenu(String menu) {
        boolean ban = false;
        try {
            for (int i = 0; i < this.menu.size(); i++) {
                if (this.menu.get(i).equals(menu)) {
                    ban = true;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método preguntarMenu() de Usuario.java");
        }
        return ban;
    }

    public boolean llenarRolesServicios() {

        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                sql.append("SELECT ");
                sql.append("  adm.tipo_servicios.desc_tiposerv,");
                sql.append("  adm.tipo_servicios.cod_tiposerv");
                sql.append(" FROM");
                sql.append("  adm.roles");
                sql.append("  INNER JOIN adm.usuario ON (adm.roles.cod_rol = adm.usuario.rol)");
                sql.append("  INNER JOIN adm.rol_tipos_serv ON (adm.roles.cod_rol = adm.rol_tipos_serv.cod_rol)");
                sql.append("  INNER JOIN adm.tipo_servicios ON (adm.rol_tipos_serv.cod_tipo_serv = adm.tipo_servicios.cod_tiposerv)");
                sql.append(" WHERE");
                sql.append("  adm.usuario.tipoid = ? ");
                sql.append("  and adm.usuario.identificacion = ? ");

                cn.prepareStatementSEL(Constantes.PS2, sql);
                cn.ps2.setString(1, this.tipoId);
                cn.ps2.setString(2, this.identificacion);

                //System.out.println(((LoggableStatement)cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    if (cn.rs2 != null) {
                        //   System.out.println("-------------------------------------------------------------");
                        while (cn.rs2.next()) {
//                      this.login=cn.rs1.getString("login");
                            //  System.out.println("descripcion del Tipo= "+cn.rs2.getString("desc_tiposerv")+" codTipo= "+cn.rs2.getString("cod_tiposerv") );
                            this.servicios.add(cn.rs2.getString("cod_tiposerv"));
                        }
                        //   System.out.println("-------------------------------------------------------------");
                    }
                    cn.cerrarPS(Constantes.PS2);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método llenarRoles() de Usuario.java");

            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método llenarRoles() de Usuario.java");
        }
        return true;
    }

    /*
    public boolean llenarEmpresa() {

        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                sql.append("SELECT id_empresa, id_tipo_id_tercero, razon_social, ip_intranet, ip_publica, logo1, logo2, logo3, so_aplicacion FROM  adm.empresas WHERE vigente =1 ");
                cn.prepareStatementSEL(Constantes.PS2, sql);

                System.out.println("xxxxx " + ((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    if (cn.rs2 != null) {
                        while (cn.rs2.next()) {
                            this.idEmpresa = cn.rs2.getString("id_empresa");
                            this.tipoIdIdEmpresa = cn.rs2.getString("id_tipo_id_tercero");
                            this.nomEmpresa = cn.rs2.getString("razon_social");
                            this.ipIntranet = cn.rs2.getString("ip_intranet");
                            this.ipPublica = cn.rs2.getString("ip_publica");
                            this.logo1 = cn.rs2.getString("logo1");
                            this.logo2 = cn.rs2.getString("logo2");
                            this.logo3 = cn.rs2.getString("logo3");
                            this.sistemaOperativo = cn.rs2.getString("so_aplicacion");

                            this.sistemaOperativo = "LINUX";

                            this.rutaRaiz = "/opt/tomcat8/webapps/clinica/";

                        }
                        System.out.println(this.sistemaOperativo + "-------------------------------------------------------------" + this.getRutaRaiz());
                    }
                    cn.cerrarPS(Constantes.PS2);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método llenarRoles() de Usuario.java");

            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método llenarRoles() de Usuario.java");
        }
        return true;
    }
     */
    public boolean preguntarServicios(String servicio) {
        boolean ban = false;
        try {
            for (int i = 0; i < this.servicios.size(); i++) {
                if (this.servicios.get(i).equals(servicio)) {
                    ban = true;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método preguntarServicios() de Usuario.java");
        }
        return ban;
    }

    public int cargarcontra() {
        Des des = new Des();
        int contador = 0;
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                sql.append("select 'CC'as tipo_id, identificacion, nombre_usr AS usuario, contrasena  from adm_usuario_xls where activo ='1' ");
                sql.append("union  ");
                sql.append("select 'CC'as tipo_id, identificacion, identificacion AS usuario, identificacion AS contrasena ");
                sql.append("from adm_usuario_talento ");
                sql.append("where identificacion not in ( select  identificacion from adm_usuario_xls where activo ='1' )");
                cn.prepareStatementSEL(Constantes.PS1, sql);
                //  System.out.println(((LoggableStatement)cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    if (cn.rs1 != null) {
                        while (cn.rs1.next()) {
                            //this.nombre1= cn.rs1.getString("nombreComplet");
                            //System.out.println(cn.rs1.getString("tipo_id")+"---"+cn.rs1.getString("identificacion")+"---"+cn.rs1.getString("usuario")+"---"+cn.rs1.getString("contrasena") +"---"+des.encriptar(cn.rs1.getString("contrasena")) );

                            /**
                             * ******************************************************************
                             */
                            cn.iniciatransaccion();

                            sql.delete(0, sql.length());
                            sql.append("INSERT INTO adm_usuario(  tipo_id,  identificacion,  usuario,  contrasena)values( ?,?,?,?) ");
                            cn.prepareStatementIDU(Constantes.PS2, sql);;
                            cn.ps2.setString(1, cn.rs1.getString("tipo_id"));
                            cn.ps2.setString(2, cn.rs1.getString("identificacion"));
                            cn.ps2.setString(3, cn.rs1.getString("usuario"));
                            cn.ps2.setString(4, des.encriptar(cn.rs1.getString("contrasena")));

                            System.out.println(((LoggableStatement) cn.ps2).getQueryString());
                            cn.iduSQL(Constantes.PS2);
                            cn.fintransaccion();
                            /**
                             * ******************************************************************
                             */
                        }
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método cargarDatosDelUsuario() de Usuario.java");
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método cargarDatosDelUsuario() de Usuario.java");
            cn.exito = false;
        }
        //return cn.exito;
        return contador;
    }

    /*
   usuario que este en la vista vi_empleado y que aun no tenga usuario y contraseéa

     */
    public boolean crearLoginContrasenaInicial(String login) {  // System.out.println("0 +++++++++++*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*");
        Des des = new Des();
        boolean ingreso = false;
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                sql.append("select tipo_id, identificacion, nombre1,nombre2,apellido1,apellido2, NoId_Individual from adm_empleado  ");
                sql.append("where identificacion=? and  identificacion not in ( select identificacion from adm_usuario )  ");
                cn.prepareStatementSEL(Constantes.PS1, sql);
                cn.ps1.setString(1, login);

                // System.out.println("******** "+((LoggableStatement)cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    if (cn.rs1 != null) {

                        if (!cn.rs1.next()) {
                            System.out.println("no se encuentra ni en personal- 3.1 usuario");

                            ingreso = false;
                        } else {
                            System.out.println("si se encuentra en personal- 3.2 usuario");
                            cn.rs1.beforeFirst();

                            while (cn.rs1.next()) { // System.out.println("1+++++++++++*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*");
                                //this.nombre1= cn.rs1.getString("nombreComplet");
                                System.out.println(cn.rs1.getString("tipo_id") + "---" + cn.rs1.getString("identificacion") + "---" + des.encriptar(cn.rs1.getString("identificacion")));
                                cn.iniciatransaccion();
                                sql.delete(0, sql.length());
                                sql.append("INSERT INTO adm_usuario( tipo_idid,  tipo_id,  identificacion,  usuario,  contrasena)values(?,?,?,?,?) ");
                                cn.prepareStatementIDU(Constantes.PS2, sql);;
                                cn.ps2.setString(1, cn.rs1.getString("tipo_id") + cn.rs1.getString("identificacion"));
                                cn.ps2.setString(2, cn.rs1.getString("tipo_id"));
                                cn.ps2.setString(3, cn.rs1.getString("identificacion"));
                                cn.ps2.setString(4, login);
                                cn.ps2.setString(5, des.encriptar(login));

                                // System.out.println("-+-+-+-+-+-+-+"+((LoggableStatement)cn.ps2).getQueryString());
                                cn.iduSQL(Constantes.PS2);

                                this.login = login; //cn.rs1.getString("login");
                                this.contrasena = cn.rs1.getString("identificacion");//cn.rs1.getString("contrasena");
                                this.rol = "1";//cn.rs1.getString("rol");
                                this.rolUsuarioSession = "1";//cn.rs1.getString("rol");
                                this.nombre1 = cn.rs1.getString("nombre1");
                                this.nombre2 = cn.rs1.getString("nombre2");
                                this.apellido1 = cn.rs1.getString("apellido1");
                                this.apellido2 = cn.rs1.getString("apellido2");
                                this.tipoId = cn.rs1.getString("tipo_id");
                                this.identificacion = cn.rs1.getString("NoId_Individual");
                                cn.fintransaccion();
                                ingreso = true;
                                /**
                                 * ******************************************************************
                                 */
                            }
                        }
                    }

                    cn.cerrarPS(Constantes.PS1);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método crearLoginContrasenaInicial() de Usuario.java");
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método crearLoginContrasenaInicial() de Usuario.java");
            cn.exito = false;
        }

        return ingreso;

    }

    public boolean demonio() {
        int contador;
        String rolactual;
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                sql.delete(0, sql.length());
                sql.append("INSERT INTO sw.bitacora_historico( id_persona, query, fecha_elaboro) ( SELECT  id_persona, query,  fecha_elaboro FROM sw.bitacora ); delete from sw.bitacora ");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método demonio() de Usuario.java");
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método demonio() de Usuario.java");
            cn.exito = false;
        }
        return cn.exito;
    }

    public boolean guardarUsuario() {
        //Constantes.MAX_MENSAJE = "...";
        Des des = new Des();
        boolean resultado = true;
        int contador;
        try {

            if (cn.isEstado()) {
                //cn.setError(-1);
                //cn.iniciatransaccion();
                //System.out.println("aqui 3:");
                sql.delete(0, sql.length());
                sql.append("INSERT INTO adm.usuario (login,contrasena,rol, estado, tipoid, identificacion ");
                sql.append(", _usr_digita, _fec_digita ) ");
                sql.append("VALUES( ?,?,?,?,?,? ");
                sql.append(", '" + this.sinertrans_user + "', now() ) ");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.ps1.setString(1, this.login);
                cn.ps1.setString(2, des.encriptar(this.contrasena));
                cn.ps1.setString(3, this.rol);
                cn.ps1.setString(4, this.estado);
                cn.ps1.setString(5, this.tipoId);
                cn.ps1.setString(6, this.identificacion);
                //cn.ps1.setString(5, this.codigo);
                //System.out.println(((LoggableStatement)cn.ps1).getQueryString());
                resultado = cn.iduSQL(Constantes.PS1);
                //cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método guardarUsuario() de clase Usuario");
            resultado = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método guardarUsuario() de clase Usuario");
            resultado = false;
        }
        return resultado;
    }//Fin metodo guardarUsuario()

    //Metodo para realizar busquedas de programas
    public Object buscarUsuario() {
        ArrayList<UsuarioVO> resultado = new ArrayList<UsuarioVO>();
        String var = "";
        UsuarioVO usu;
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                // Ojo el estado de usuario
                sql.append("SELECT u.tipoid, u.identificacion,login,contrasena,rol, u.estado, nombre1, nombre2, apellido1, apellido2 ");
                sql.append(" FROM adm_usuario u, adm_persona p ");
                sql.append("WHERE u.identificacion = p.identificacion AND u.tipoid = p.tipoid ");
                sql.append("AND u.identificacion LIKE ? AND u.tipoid LIKE ? ");
                sql.append("AND u.login LIKE ? ");
                //sql.append("AND p.tipoid LIKE ? "); //AND tipo = ? ");
                sql.append("ORDER BY tipoid, identificacion, login ASC");
                this.cn.prepareStatementSEL(Constantes.PS1, sql);
                cn.ps1.setString(1, "%" + this.identificacion + "%");
                cn.ps1.setString(2, "%" + this.tipoId + "%");
                cn.ps1.setString(3, "%" + this.login + "%");
                //cn.ps1.setString(4, "%"+ this.tipoId +"%");
                //cn.ps1.setInt(6, this.profesional);
                //System.out.println(((LoggableStatement)cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    while (cn.rs1.next()) {
                        usu = new UsuarioVO();
                        //usu.setCodigo(cn.rs1.getString("codigo"));
                        usu.setTipoId(cn.rs1.getString("tipoid"));
                        usu.setIdentificacion(cn.rs1.getString("identificacion"));
                        usu.setLogin(cn.rs1.getString("login"));
                        usu.setContrasena(cn.rs1.getString("contrasena"));
                        usu.setRol(cn.rs1.getString("rol"));
                        usu.setEstado(cn.rs1.getString("estado"));

                        usu.setApellido1(cn.rs1.getString("apellido1"));
                        usu.setApellido2(cn.rs1.getString("apellido2"));
                        usu.setNombre1(cn.rs1.getString("nombre1"));
                        usu.setNombre2(cn.rs1.getString("nombre2"));
                        resultado.add(usu);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }
        } catch (SQLException e) {
            //System.out.println("Error --> clase Usuario --> function buscarUsuario --> SQLException --> "+e.getMessage());
        } catch (Exception e) {
            //System.out.println("Error --> clase Usuario --> function buscarUsuario --> Exception --> " + e.getMessage());
        }
        return true;       // cambiar a true por resultado para validar usuario
    }//Fin funcion buscarUsuario()

    /**
     * eliminar un usuario especifico*
     */
    public boolean eliminarUsuario() {
        //boolean resultado=true;
        //Constantes.MAX_MENSAJE = "...";
        int contador;
        try {
            // COMMENT: verifica que exista un usario con ese login para eliminarlo
            if (cn.isEstado()) {
                //cn.setError(-1);
                // cn.iniciatransaccion();
                sql.delete(0, sql.length());
                sql.append("SELECT count(login) as n_login ");
                sql.append("FROM adm_usuario ");
                sql.append("WHERE adm_usuario.login= (SELECT login FROM adm_usuario WHERE login = ? ) ");
                cn.prepareStatementSEL(Constantes.PS1, sql);
                cn.ps1.setString(1, login);
                //System.out.println(((LoggableStatement)cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    if (cn.rs1 != null) {
                        while (cn.rs1.next()) {
                            contador = cn.rs1.getInt("n_login");
                            //System.out.println("contador de usuarios con login "+contador);
                            // COMMENT: verifica que exista un usario con ese login para eliminarlo
                            if (contador <= 0) {
                                //Constantes.MAX_MENSAJE= " No se ha encontrado un usuario con el Login que especifica. ";
                                //System.out.println("sga_mensaje "+Constantes.SINERTRANS_MENSAJE+" obj " );
                                return false;
                            }

                        }
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
                // cn.fintransaccion();
            }

            // COMMENT: si es el ultimo usuario con este rol, no deberia eliminarlo
            if (cn.isEstado()) {
                //cn.setError(-1);
                // cn.iniciatransaccion();
                sql.delete(0, sql.length());
                sql.append("SELECT count(rol) as role ");
                sql.append("FROM adm_usuario ");
                sql.append("WHERE adm_usuario.rol= (SELECT rol FROM adm_usuario WHERE login = ? ) ");
                cn.prepareStatementSEL(Constantes.PS1, sql);
                cn.ps1.setString(1, login);
                //System.out.println(((LoggableStatement)cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    if (cn.rs1 != null) {
                        while (cn.rs1.next()) {
                            contador = cn.rs1.getInt("role");
                            //System.out.println("contador roles "+contador);
                            // COMMENT: si es el ultimo usuario con este rol, no deberia eliminarlo
                            if (contador <= 1) {
                                //Constantes.MAX_MENSAJE= " Debe existir al menos un usuario de cada tipo de Rol ";
                                //System.out.println("sga_mensaje "+Constantes.SINERTRANS_MENSAJE+" obj " );
                                return false;
                            }

                        }
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
                // cn.fintransaccion();
            }
            if (cn.isEstado()) {
                //cn.setError(-1);
                cn.iniciatransaccion();
                //System.out.println("aqui 3:");
                sql.delete(0, sql.length());
                sql.append("DELETE FROM adm_usuario WHERE login=?");
                cn.prepareStatementIDU(Constantes.PS1, sql);;
                cn.ps1.setString(1, this.login);
                //System.out.println(((LoggableStatement)cn.ps1).getQueryString());
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método eliminarUsuario() de Usuario.java");
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método eliminarUsuario() de Usuario.java");
            cn.exito = false;
        }
        return cn.exito;
    }

    /**
     * *
     */
    public boolean cambiarContrasena(String valor) {
        //boolean resultado=true;
        try {
            if (cn.isEstado()) {
                //cn.setError(-1);
                cn.iniciatransaccion();
                //System.out.println("aqui 3:");
                sql.delete(0, sql.length());
                sql.append("UPDATE adm.usuario SET contrasena=? ");
                sql.append("WHERE login =? AND contrasena=? ");
                cn.prepareStatementIDU(Constantes.PS1, sql);;
                cn.ps1.setString(1, valor);
                cn.ps1.setString(2, this.login);
                cn.ps1.setString(3, this.contrasena);
                //System.out.println(((LoggableStatement)cn.ps1).getQueryString());
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método cambiarContrasena() de Programa.java");
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método cambiarContrasena() de Programa.java");
            cn.exito = false;
        }
        return cn.exito;
    }

    /**
     * *
     */
    public boolean verificarContrasena(String contras) {
        Des des = new Des();
        int contador;
        try {
            if (cn.isEstado()) {
                //cn.setError(-1);
                // cn.iniciatransaccion();

                sql.delete(0, sql.length());
                sql.append("SELECT count(*) as usuariook ");
                sql.append("FROM adm.usuario ");
                sql.append("WHERE adm.usuario.login= ? AND adm.usuario.contrasena= ? ");
                cn.prepareStatementSEL(Constantes.PS1, sql);
                cn.ps1.setString(1, login);
                // cn.ps1.setString(2,contras);
                cn.ps1.setString(2, des.encriptar(contras));
                //System.out.println(((LoggableStatement)cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    if (cn.rs1 != null) {
                        while (cn.rs1.next()) {
                            contador = cn.rs1.getInt("usuariook");
                            //System.out.println("contador usuarios "+contador);
                            // COMMENT: si no hay usuarios coinciden en login actual y contraseéa enviada
                            if (contador <= 0) {
                                return false;
                            }

                        }
                    }
                    cn.cerrarPS(Constantes.PS1);
                }

                // cn.fintransaccion();
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método cambiarContrasena() de Programa.java");
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método cambiarContrasena() de Programa.java");
            cn.exito = false;
        }
        //return cn.exito;
        return true;
    }

    public boolean verificarContrasenaAux(String contras) {
        System.out.println("coDDDDDDDD");
        Des des = new Des();
        int contador;
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                sql.append("SELECT tipo_id,  identificacion FROM  adm.usuario where contraseéa = ? ");
                cn.prepareStatementSEL(Constantes.PS1, sql);
                //  cn.ps1.setString(1,login );
                // cn.ps1.setString(2,contras);
                cn.ps1.setString(1, des.encriptar(contras));
                System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    if (cn.rs1 != null) {
                        while (cn.rs1.next()) {
                            contador = cn.rs1.getInt("usuariook");
                            //System.out.println("contador usuarios "+contador);
                            // COMMENT: si no hay usuarios coinciden en login actual y contraseéa enviada
                            if (contador <= 0) {
                                return false;
                            }

                        }
                    }
                    cn.cerrarPS(Constantes.PS1);
                }

                // cn.fintransaccion();
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método cambiarContrasena() de Programa.java");
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método cambiarContrasena() de Programa.java");
            cn.exito = false;
        }
        //return cn.exito;
        return true;
    }

    public int cargarDatosDelUsuario() {
        Des des = new Des();
        int contador = 0;
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQuery(23));
                cn.ps1.setString(1, this.tipoId);
                cn.ps1.setString(2, this.identificacion);
                if (cn.selectSQL(Constantes.PS1)) {
                    if (cn.rs1 != null) {
                        while (cn.rs1.next()) {

                            if (!cn.rs1.getString("login").equals("")) {
                                this.nombre1 = cn.rs1.getString("nombreComplet");
                                this.login = cn.rs1.getString("login");
                                this.contrasena = des.desencriptar(cn.rs1.getString("contrasena"));
                                this.estado = cn.rs1.getString("estadoCuenta");
                                this.rol = cn.rs1.getString("rol");
                                contador = 1;   //encontro un empleado con cuenta de usuario
                            } else {
                                this.nombre1 = cn.rs1.getString("nombreComplet");
                                contador = 2;  //encontro un empleado SIN cuenta de usuario
                            }

                        }
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en método cargarDatosDelUsuario() de Programa.java");
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en método cargarDatosDelUsuario() de Programa.java");
            cn.exito = false;
        }
        //return cn.exito;
        return contador;
    }

    public void setSinertrans_user(java.lang.String value) {
        sinertrans_user = value;
    }

    public java.lang.String getRolUsuarioSession() {
        return rolUsuarioSession;
    }

    public void setRolUsuarioSession(java.lang.String value) {
        rolUsuarioSession = value;
    }

    public java.lang.String getIdProfesion() {
        return idProfesion;
    }

    public void setIdProfesion(java.lang.String value) {
        idProfesion = value;
    }

    public java.lang.String getProfesion() {
        return profesion;
    }

    public java.lang.String getSuperuser() {
        return superuser;
    }

    public java.lang.String getCodTiposervicio() {
        return codTiposervicio;
    }

    public void setCodTiposervicio(java.lang.String value) {
        codTiposervicio = value;
    }

    public java.lang.String getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(java.lang.String value) {
        idEmpresa = value;
    }

    public java.lang.String getNomEmpresa() {
        return nomEmpresa;
    }

    public void setNomEmpresa(java.lang.String value) {
        nomEmpresa = value;
    }

    public java.lang.String getTipoIdIdEmpresa() {
        return tipoIdIdEmpresa;
    }

    public void setTipoIdIdEmpresa(java.lang.String value) {
        tipoIdIdEmpresa = value;
    }

    public java.lang.String getIpIntranet() {
        return ipIntranet;
    }

    public void setIpIntranet(java.lang.String value) {
        ipIntranet = value;
    }

    public java.lang.String getIpPublica() {
        return ipPublica;
    }

    public void setIpPublica(java.lang.String value) {
        ipPublica = value;
    }

    public java.lang.String getLogo1() {
        return logo1;
    }

    public void setLogo1(java.lang.String value) {
        logo1 = value;
    }

    public java.lang.String getLogo2() {
        return logo2;
    }

    public void setLogo2(java.lang.String value) {
        logo2 = value;
    }

    public java.lang.String getLogo3() {
        return logo3;
    }

    public void setLogo3(java.lang.String value) {
        logo3 = value;
    }

    public java.lang.String getSistemaOperativo() {
        return sistemaOperativo;
    }

    public void setSistemaOperativo(java.lang.String value) {
        sistemaOperativo = value;
    }

    public java.lang.String getRutaRaiz() {
        return rutaRaiz;
    }

    public void setRutaRaiz(java.lang.String value) {
        rutaRaiz = value;
    }

    public java.lang.String getRol() {
        return rol;
    }

    public void setRol(java.lang.String value) {
        rol = value;
    }

    public java.lang.String getEstado() {
        return estado;
    }

    public void setEstado(java.lang.String value) {
        estado = value;
    }

    public void setCodigo(java.lang.String value) {
        codigo = value;
    }

    public java.lang.String getCodigo() {
        return codigo;
    }

    public java.lang.String getLogin() {
        return login;
    }

    public void setLogin(java.lang.String value) {
        login = value;
    }

    public java.lang.String getContrasena() {
        return contrasena;
    }

    public void setContrasena(java.lang.String value) {
        contrasena = value;
    }

    public Conexion getCn() {
        return cn;
    }

    public void setCn(Conexion value) {
        cn = value;
    }

    public java.lang.StringBuffer getSql() {
        return sql;
    }

    public void setSql(java.lang.StringBuffer value) {
        sql = value;
    }

    public java.lang.String getTipoId() {
        return tipoId;
    }

    public void setTipoId(java.lang.String value) {
        tipoId = value;
    }

    public java.lang.String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(java.lang.String value) {
        identificacion = value;
    }

    public java.lang.String getNombre1() {
        return nombre1;
    }

    public void setNombre1(java.lang.String value) {
        nombre1 = value;
    }

    public java.lang.String getNombre2() {
        return nombre2;
    }

    public void setNombre2(java.lang.String value) {
        nombre2 = value;
    }

    public java.lang.String getApellido1() {
        return apellido1;
    }

    public void setApellido1(java.lang.String value) {
        apellido1 = value;
    }

    public java.lang.String getApellido2() {
        return apellido2;
    }

    public void setApellido2(java.lang.String value) {
        apellido2 = value;
    }

    public java.lang.String getIdSede() {
        return idSede;
    }

    public void setIdSede(java.lang.String value) {
        idSede = value;
    }

    public String getNomSede() {
        return nomSede;
    }

    public void setNomSede(String nomSede) {
        this.nomSede = nomSede;
    }

}
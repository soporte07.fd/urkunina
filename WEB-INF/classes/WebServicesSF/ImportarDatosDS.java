
package WebServicesSF;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idDocumento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="strNombreDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCompania" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="strCompania" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="strUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="strClave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dsFuenteDatos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;any/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Path" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idDocumento",
    "strNombreDocumento",
    "idCompania",
    "strCompania",
    "strUsuario",
    "strClave",
    "dsFuenteDatos",
    "path"
})
@XmlRootElement(name = "ImportarDatosDS")
public class ImportarDatosDS {

    protected int idDocumento;
    protected String strNombreDocumento;
    protected int idCompania;
    protected String strCompania;
    protected String strUsuario;
    protected String strClave;
    protected ImportarDatosDS.DsFuenteDatos dsFuenteDatos;
    @XmlElement(name = "Path")
    protected String path;

    /**
     * Obtiene el valor de la propiedad idDocumento.
     * 
     */
    public int getIdDocumento() {
        return idDocumento;
    }

    /**
     * Define el valor de la propiedad idDocumento.
     * 
     */
    public void setIdDocumento(int value) {
        this.idDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad strNombreDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrNombreDocumento() {
        return strNombreDocumento;
    }

    /**
     * Define el valor de la propiedad strNombreDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrNombreDocumento(String value) {
        this.strNombreDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad idCompania.
     * 
     */
    public int getIdCompania() {
        return idCompania;
    }

    /**
     * Define el valor de la propiedad idCompania.
     * 
     */
    public void setIdCompania(int value) {
        this.idCompania = value;
    }

    /**
     * Obtiene el valor de la propiedad strCompania.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrCompania() {
        return strCompania;
    }

    /**
     * Define el valor de la propiedad strCompania.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrCompania(String value) {
        this.strCompania = value;
    }

    /**
     * Obtiene el valor de la propiedad strUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrUsuario() {
        return strUsuario;
    }

    /**
     * Define el valor de la propiedad strUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrUsuario(String value) {
        this.strUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad strClave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrClave() {
        return strClave;
    }

    /**
     * Define el valor de la propiedad strClave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrClave(String value) {
        this.strClave = value;
    }

    /**
     * Obtiene el valor de la propiedad dsFuenteDatos.
     * 
     * @return
     *     possible object is
     *     {@link ImportarDatosDS.DsFuenteDatos }
     *     
     */
    public ImportarDatosDS.DsFuenteDatos getDsFuenteDatos() {
        return dsFuenteDatos;
    }

    /**
     * Define el valor de la propiedad dsFuenteDatos.
     * 
     * @param value
     *     allowed object is
     *     {@link ImportarDatosDS.DsFuenteDatos }
     *     
     */
    public void setDsFuenteDatos(ImportarDatosDS.DsFuenteDatos value) {
        this.dsFuenteDatos = value;
    }

    /**
     * Obtiene el valor de la propiedad path.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPath() {
        return path;
    }

    /**
     * Define el valor de la propiedad path.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPath(String value) {
        this.path = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;any/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "any"
    })
    public static class DsFuenteDatos {

        @XmlAnyElement(lax = true)
        protected Object any;

        /**
         * Obtiene el valor de la propiedad any.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getAny() {
            return any;
        }

        /**
         * Define el valor de la propiedad any.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setAny(Object value) {
            this.any = value;
        }

    }

}

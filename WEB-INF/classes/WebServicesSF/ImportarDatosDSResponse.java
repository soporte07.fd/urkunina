
package WebServicesSF;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ImportarDatosDSResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Path" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "importarDatosDSResult",
    "path"
})
@XmlRootElement(name = "ImportarDatosDSResponse")
public class ImportarDatosDSResponse {

    @XmlElement(name = "ImportarDatosDSResult")
    protected String importarDatosDSResult;
    @XmlElement(name = "Path")
    protected String path;

    /**
     * Obtiene el valor de la propiedad importarDatosDSResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImportarDatosDSResult() {
        return importarDatosDSResult;
    }

    /**
     * Define el valor de la propiedad importarDatosDSResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImportarDatosDSResult(String value) {
        this.importarDatosDSResult = value;
    }

    /**
     * Obtiene el valor de la propiedad path.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPath() {
        return path;
    }

    /**
     * Define el valor de la propiedad path.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPath(String value) {
        this.path = value;
    }

}

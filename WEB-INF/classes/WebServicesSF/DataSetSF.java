package WebServicesSF;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "MyDataSet")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataSetSF {

    @XmlElement(name = "Remision_agil")
    private OrdenSF orden;

    @XmlElement(name = "Movto_ventas_agil")
    private List<ArticuloSF> listaArticulos;

    public DataSetSF() {
    }
    

    public DataSetSF(OrdenSF orden, List<ArticuloSF> listaArticulos) {
        this.orden = orden;
        this.listaArticulos = listaArticulos;
    }

    public List<ArticuloSF> getListaArticulos() {
        return listaArticulos;
    }

    public void setListaArticulos(List<ArticuloSF> listaArticulos) {
        this.listaArticulos = listaArticulos;
    }

    public OrdenSF getOrden() {
        return orden;
    }

    public void setOrden(OrdenSF orden) {
        this.orden = orden;
    }

}

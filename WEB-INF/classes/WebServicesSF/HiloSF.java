
package WebServicesSF;


public class HiloSF extends Thread {

    private final String idEvolucion;

    public HiloSF(String idEvolucion) {
        this.idEvolucion = idEvolucion;
    }

    @Override
    public void run() {
        WebServicesSF WS = new WebServicesSF(idEvolucion);
        WS.enviarMedicamentos();
    }

}

package WebServicesSF;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Remision_agil")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrdenSF {

    @XmlElement(name = "f350_consec_docto")
    private String idOrden;

    @XmlElement(name = "f350_fecha")
    private String fecha;

    @XmlElement(name = "f460_id_cond_pago")
    private String condicionPago;

    @XmlElement(name = "f4613_nit_paciente")
    private String paciente;

    @XmlElement(name = "f4613_id_tipo_venta")
    private String tipoVenta;

    @XmlElement(name = "f4613_perfil")
    private String perfil;

    @XmlElement(name = "f4613_numero_aprobacion")
    private String aprobacion;

    @XmlElement(name = "f4613_numero_formula")
    private String formula;

    @XmlElement(name = "f4613_ips")
    private String ips;

    @XmlElement(name = "f4613_medico")
    private String medico;

    public OrdenSF() {
    }

    public String getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(String idOrden) {
        this.idOrden = idOrden;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCondicionPago() {
        return condicionPago;
    }

    public void setCondicionPago(String condicionPago) {
        this.condicionPago = condicionPago;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getTipoVenta() {
        return tipoVenta;
    }

    public void setTipoVenta(String tipoVenta) {
        this.tipoVenta = tipoVenta;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getAprobacion() {
        return aprobacion;
    }

    public void setAprobacion(String aprobacion) {
        this.aprobacion = aprobacion;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getIps() {
        return ips;
    }

    public void setIps(String ips) {
        this.ips = ips;
    }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

}

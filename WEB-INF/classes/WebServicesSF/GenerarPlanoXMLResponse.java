
package WebServicesSF;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GenerarPlanoXMLResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Path" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="strResultado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "generarPlanoXMLResult",
    "path",
    "strResultado"
})
@XmlRootElement(name = "GenerarPlanoXMLResponse")
public class GenerarPlanoXMLResponse {

    @XmlElement(name = "GenerarPlanoXMLResult")
    protected String generarPlanoXMLResult;
    @XmlElement(name = "Path")
    protected String path;
    protected String strResultado;

    /**
     * Obtiene el valor de la propiedad generarPlanoXMLResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGenerarPlanoXMLResult() {
        return generarPlanoXMLResult;
    }

    /**
     * Define el valor de la propiedad generarPlanoXMLResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGenerarPlanoXMLResult(String value) {
        this.generarPlanoXMLResult = value;
    }

    /**
     * Obtiene el valor de la propiedad path.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPath() {
        return path;
    }

    /**
     * Define el valor de la propiedad path.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPath(String value) {
        this.path = value;
    }

    /**
     * Obtiene el valor de la propiedad strResultado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrResultado() {
        return strResultado;
    }

    /**
     * Define el valor de la propiedad strResultado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrResultado(String value) {
        this.strResultado = value;
    }

}

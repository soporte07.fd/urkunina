package WebServicesSF;

import WebServicesLaboratorio.ConexionLaboratorio;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXB;
import javax.xml.ws.Holder;

public class WebServicesSF {
    
     private final String idEvolucion;

    public WebServicesSF(String idEvolucion) {
        this.idEvolucion = idEvolucion;
    }

    public void enviarMedicamentos() {

        WsGenerarPlano webServices;
        WsGenerarPlanoSoap port;

        webServices = new WsGenerarPlano();
        port = webServices.getWsGenerarPlanoSoap();

        Holder<String> path = new Holder<>();
        Holder<String> result = new Holder<>();

        Connection con = null;
        ConexionLaboratorio conexion = null;
        PreparedStatement ps = null, ps2 = null,ps3= null;
        ResultSet rs = null, rs2 = null,rs3 = null;
        OrdenSF orden = new OrdenSF();
         DataSetSF dataSet =null;
         
        try {
            //Se crea una nueva conexión a la BD
            conexion = new ConexionLaboratorio();
            con = conexion.getConnection();

            //Query cabecera del xml (Remision Agil)
            String query = conexion.consultarQuery(776, ps, rs);

            ps = con.prepareStatement(query);
            ps.setString(1, idEvolucion);

            System.out.println(ps.toString());
            rs = ps.executeQuery();

            while (rs.next()) {

                orden.setIdOrden(rs.getString("id"));
                orden.setFecha(rs.getString("fecha"));
                orden.setCondicionPago(rs.getString("condicion_pago"));
                orden.setPaciente(rs.getString("identificacion"));
                orden.setTipoVenta(rs.getString("tipo_venta"));
                orden.setPerfil(rs.getString("perfil"));
                orden.setAprobacion(rs.getString("aprobacion"));
                orden.setFormula(rs.getString("formula"));
                orden.setIps(rs.getString("ips"));
                orden.setMedico(rs.getString("medico"));

                //Query articulos de la orden
                String query2 = conexion.consultarQuery(777, ps, rs);

                ps2 = con.prepareStatement(query2);
                ps2.setString(1, idEvolucion);

                System.out.println(ps2.toString());
                rs2 = ps2.executeQuery();
                List<ArticuloSF> listaArticulos = new ArrayList();
                while (rs2.next()) {

                    ArticuloSF a = new ArticuloSF();
                    a.setIdOrden(rs2.getString("id_evolucion"));
                    a.setOrden(rs2.getString("orden"));
                    a.setItem(rs2.getString("item"));
                    a.setMotivo(rs2.getString("motivo"));
                    a.setListaPrecio(rs2.getString("lista_precio"));
                    a.setCantidad(rs2.getString("cantidad"));
                    a.setUnidadMedida(rs2.getString("id_unidad_medida_sf"));
                    a.setLote(rs2.getString("lote"));

                    listaArticulos.add(a);
                }

                dataSet = new DataSetSF(orden, listaArticulos);

            }
              path.value = "C:\\inetpub\\wwwroot\\GTIntegration\\Planos\\";
              
             StringWriter sw = new StringWriter();
                JAXB.marshal(dataSet, sw);
                String xmlString = sw.toString();
                xmlString = xmlString.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n", "");
                xmlString = xmlString.replace(" xmlns=\"http://generictransfer.com/\"", "");

                System.out.println(xmlString);

                port.importarDatosXML(81106,
                        "Remisiones",
                        2,
                        "12",
                        "gt",
                        "gt",
                        xmlString,
                        path,
                        result);
                
                System.out.println(result.value);
                
                
                String query3 = conexion.consultarQuery(778, ps, rs);

                ps3 = con.prepareStatement(query3);
                ps3.setString(1, (result.value.equals("Importacion exitosa") ? "E":"R"));                
                ps3.setString(2, result.value);
                ps3.setString(3, idEvolucion);


                System.out.println(ps3.toString());
                
                int row = ps3.executeUpdate();
                    if (row > 0) {
                        System.out.println("Se actulizaron: "+row);
                        con.commit();
                    }
            

        } catch (SQLException e) {
            System.out.println("Error al ejecutar querys: " + e.getMessage());
        } finally {
            //Se cierra la conexión a la BD
            try {
                if (rs != null) {
                    rs.close();
                }

                if (rs2 != null) {
                    rs2.close();
                }
                
                if (rs3 != null) {
                    rs3.close();
                }

                if (ps != null) {
                    ps.close();
                }

                if (ps2 != null) {
                    ps2.close();
                }
                
                if (ps3 != null) {
                    ps3.close();
                }

                if (con != null) {
                    con.close();
                }

                if (conexion != null) {
                    conexion.cerrarConexion();
                }
            } catch (SQLException e) {
                System.out.println("Error al ejecutar querys: " + e.getMessage());
            }

        }

    }

}

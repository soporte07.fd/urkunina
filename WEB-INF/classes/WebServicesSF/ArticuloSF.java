package WebServicesSF;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "laboratorio")
@XmlAccessorType(XmlAccessType.FIELD)
public class ArticuloSF {

    @XmlElement(name = "f470_consec_docto")
    private String idOrden;

    @XmlElement(name = "f470_nro_registro")
    private String orden;

    @XmlElement(name = "f470_id_item")
    private String item;

    @XmlElement(name = "f470_id_motivo")
    private String motivo;

    @XmlElement(name = "f470_id_lista_precio")
    private String listaPrecio;

    @XmlElement(name = "f4701_cant1_formula_orig")
    private String cantidad;

    @XmlElement(name = "f470_id_unidad_medida")
    private String unidadMedida;

    @XmlElement(name = "f470_id_lote")
    private String lote;

    public ArticuloSF() {
    }

    public String getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(String idOrden) {
        this.idOrden = idOrden;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getListaPrecio() {
        return listaPrecio;
    }

    public void setListaPrecio(String listaPrecio) {
        this.listaPrecio = listaPrecio;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

}

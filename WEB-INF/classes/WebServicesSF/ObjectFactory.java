
package WebServicesSF;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.generictransfer package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.generictransfer
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ImportarDatosDS }
     * 
     */
    public ImportarDatosDS createImportarDatosDS() {
        return new ImportarDatosDS();
    }

    /**
     * Create an instance of {@link GenerarPlanoXML }
     * 
     */
    public GenerarPlanoXML createGenerarPlanoXML() {
        return new GenerarPlanoXML();
    }

    /**
     * Create an instance of {@link ImportarDatosTablasDS }
     * 
     */
    public ImportarDatosTablasDS createImportarDatosTablasDS() {
        return new ImportarDatosTablasDS();
    }

    /**
     * Create an instance of {@link ImportarDatosTablasDSResponse }
     * 
     */
    public ImportarDatosTablasDSResponse createImportarDatosTablasDSResponse() {
        return new ImportarDatosTablasDSResponse();
    }

    /**
     * Create an instance of {@link ImportarDatosXML }
     * 
     */
    public ImportarDatosXML createImportarDatosXML() {
        return new ImportarDatosXML();
    }

    /**
     * Create an instance of {@link ImportarDatosDS.DsFuenteDatos }
     * 
     */
    public ImportarDatosDS.DsFuenteDatos createImportarDatosDSDsFuenteDatos() {
        return new ImportarDatosDS.DsFuenteDatos();
    }

    /**
     * Create an instance of {@link GenerarPlanoXML.DsFuenteDatos }
     * 
     */
    public GenerarPlanoXML.DsFuenteDatos createGenerarPlanoXMLDsFuenteDatos() {
        return new GenerarPlanoXML.DsFuenteDatos();
    }

    /**
     * Create an instance of {@link ImportarDatosXMLResponse }
     * 
     */
    public ImportarDatosXMLResponse createImportarDatosXMLResponse() {
        return new ImportarDatosXMLResponse();
    }

    /**
     * Create an instance of {@link ImportarDatosTablasXML }
     * 
     */
    public ImportarDatosTablasXML createImportarDatosTablasXML() {
        return new ImportarDatosTablasXML();
    }

    /**
     * Create an instance of {@link ImportarDatosTablasDS.DsFuenteDatos }
     * 
     */
    public ImportarDatosTablasDS.DsFuenteDatos createImportarDatosTablasDSDsFuenteDatos() {
        return new ImportarDatosTablasDS.DsFuenteDatos();
    }

    /**
     * Create an instance of {@link ImportarDatosDSResponse }
     * 
     */
    public ImportarDatosDSResponse createImportarDatosDSResponse() {
        return new ImportarDatosDSResponse();
    }

    /**
     * Create an instance of {@link GenerarPlanoXMLResponse }
     * 
     */
    public GenerarPlanoXMLResponse createGenerarPlanoXMLResponse() {
        return new GenerarPlanoXMLResponse();
    }

    /**
     * Create an instance of {@link ImportarDatosTablasXMLResponse }
     * 
     */
    public ImportarDatosTablasXMLResponse createImportarDatosTablasXMLResponse() {
        return new ImportarDatosTablasXMLResponse();
    }

}

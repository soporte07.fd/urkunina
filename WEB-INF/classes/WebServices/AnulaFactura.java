package WebServices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para AnulaFactura complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="AnulaFactura">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NoFactura" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FechaAnulacion" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Motivo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnulaFactura", propOrder = { 
    "prefijo",
    "noFactura",
    "fechaAnulacion",
    "motivo"
})
public class AnulaFactura {
    @XmlElement(name = "Prefijo", required = true)
    protected String prefijo;
    @XmlElement(name = "NoFactura", required = true)
    protected String noFactura;
    @XmlElement(name = "FechaAnulacion", required = true)
    protected String fechaAnulacion;
    @XmlElement(name = "Motivo", required = true)
    protected String motivo;

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getNoFactura() {
        return noFactura;
    }

    public void setNoFactura(String noFactura) {
        this.noFactura = noFactura;
    }

    public String getFechaAnulacion() {
        return fechaAnulacion;
    }

    public void setFechaAnulacion(String fechaAnulacion) {
        this.fechaAnulacion = fechaAnulacion;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Override
    public String toString() {
        return "\nAnulaFactura{" + "noFactura=" + noFactura + ", fechaAnulacion=" + fechaAnulacion + ", motivo=" + motivo + '}';
    }

}

package WebServices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para FacturaVenta complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="FacturaVenta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NoFactura" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FechaFactura" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="TipoCliente" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="NITCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Cedula" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Subtotal" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="Copago" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="Descuento" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="IVA" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="Total" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="Recibos_Asociados" type="{InsercionFacturacion}Recibos"/>
 *         &lt;element name="Detalles" type="{InsercionFacturacion}Detalles"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FacturaVenta", propOrder = {
    "prefijo",
    "noFactura",
    "fechaFactura",
    "tipoCliente",
    "nitCliente",
    "cedula",
    "subtotal",
    "descuento",
    "iva",
    "total",
    "recibosAsociados",
    "detalles"
})
public class FacturaVenta {

    @XmlElement(name = "Prefijo", required = true)
    protected String prefijo;
    @XmlElement(name = "NoFactura", required = true)
    protected String noFactura;
    @XmlElement(name = "FechaFactura", required = true)
    protected String fechaFactura;
    @XmlElement(name = "TipoCliente", required = true)
    protected String tipoCliente;
    @XmlElement(name = "NITCliente", required = true)
    protected String nitCliente;
    @XmlElement(name = "Cedula", required = true)
    protected String cedula;
    @XmlElement(name = "Subtotal", required = true)
    protected float subtotal;
    @XmlElement(name = "Descuento")
    protected Float descuento;
    @XmlElement(name = "IVA")
    protected Float iva;
    @XmlElement(name = "Total", required = true)
    protected float total;
    @XmlElement(name = "Recibos_Asociados")
    protected Recibos recibosAsociados;
    @XmlElement(name = "Detalles", required = true)
    protected Detalles detalles;

    public FacturaVenta() {
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getNoFactura() {
        return noFactura;
    }

    public void setNoFactura(String noFactura) {
        this.noFactura = noFactura;
    }

    public String getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(String fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public String getNitCliente() {
        return nitCliente;
    }

    public void setNitCliente(String nitCliente) {
        this.nitCliente = nitCliente;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
    }

    public Float getDescuento() {
        return descuento;
    }

    public void setDescuento(Float descuento) {
        if(descuento > 0){
            this.descuento = descuento;
        }
    }

    public Float getIva() {
        return iva;
    }

    public void setIva(Float iva) {
        if(iva > 0){
            this.iva = iva;
        }
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

     public Recibos getRecibosAsociados() {
        return recibosAsociados;
    }

    public void setRecibosAsociados(Recibos recibosAsociados) {
        this.recibosAsociados = recibosAsociados;
    }

    public Detalles getDetalles() {
        return detalles;
    }

    public void setDetalles(Detalles detalles) {
        this.detalles = detalles;
    }

    @Override
    public String toString() {
        return "\nFacturaVenta{" + "noFactura=" + noFactura + ", fechaFactura=" + fechaFactura + ", tipoCliente=" + tipoCliente + ", nitCliente=" + nitCliente + ", cedula=" + cedula + ", subtotal=" + subtotal +", descuento=" + descuento + ", iva=" + iva + ", total=" + total + ", recibosAsociados=" + recibosAsociados + ", detalles=" + detalles + '}';
    }

}
package WebServices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para InfoFacturacion complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="InfoFacturacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Datos_Terceros" type="{InsercionFacturacion}Datos_Terceros"/>
 *         &lt;element name="Datos_facturas" type="{InsercionFacturacion}Datos_facturas"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoFacturacion", propOrder = {
    "datosTerceros",
    "datosFacturas"
})
public class InfoFacturacion {

    @XmlElement(name = "Datos_Terceros", required = true)
    protected DatosTerceros datosTerceros;
    @XmlElement(name = "Datos_facturas", required = true)
    protected DatosFacturas datosFacturas;

    public InfoFacturacion() {
    }

    public DatosTerceros getDatosTerceros() {
        return datosTerceros;
    }

    public void setDatosTerceros(DatosTerceros datosTerceros) {
        this.datosTerceros = datosTerceros;
    }

    public DatosFacturas getDatosFacturas() {
        return datosFacturas;
    }

    public void setDatosFacturas(DatosFacturas datosFacturas) {
        this.datosFacturas = datosFacturas;
    }

    @Override
    public String toString() {
        return "\nInfoFacturacion{" + "datosTerceros=" + datosTerceros + ", datosFacturas=" + datosFacturas + '}';
    }

}

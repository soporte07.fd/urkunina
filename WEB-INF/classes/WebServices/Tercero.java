package WebServices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para Tercero complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="Tercero">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Identificacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrimApe" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SegApe" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrimNom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SegNom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Razon_Social" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Direccion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Pais" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Municipio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Regimen" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tercero", propOrder = {
    "tipoDocumento",
    "identificacion",
    "primApe",
    "segApe",
    "primNom",
    "segNom",
    "razonSocial",
    "direccion",
    "telefono",
    "email",
    "pais",
    "departamento",
    "municipio",
    "regimen"
})
public class Tercero {

    @XmlElement(name = "TipoDocumento", required = true)
    protected String tipoDocumento;
    @XmlElement(name = "Identificacion", required = true)
    protected String identificacion;
    @XmlElement(name = "PrimApe", required = true)
    protected String primApe;
    @XmlElement(name = "SegApe")
    protected String segApe;
    @XmlElement(name = "PrimNom", required = true)
    protected String primNom;
    @XmlElement(name = "SegNom")
    protected String segNom;
    @XmlElement(name = "Razon_Social")
    protected String razonSocial;
    @XmlElement(name = "Direccion")
    protected String direccion;
    @XmlElement(name = "Telefono")
    protected String telefono;
    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "Pais")
    protected String pais;
    @XmlElement(name = "Departamento")
    protected String departamento;
    @XmlElement(name = "Municipio")
    protected String municipio;
    @XmlElement(name = "Regimen", required = true)
    protected String regimen;

    public Tercero() {
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPrimApe() {
        return primApe;
    }

    public void setPrimApe(String primApe) {
        this.primApe = primApe;
    }

    public String getSegApe() {
        return segApe;
    }

    public void setSegApe(String segApe) {
        this.segApe = segApe;
    }

    public String getPrimNom() {
        return primNom;
    }

    public void setPrimNom(String primNom) {
        this.primNom = primNom;
    }

    public String getSegNom() {
        return segNom;
    }

    public void setSegNom(String segNom) {
        this.segNom = segNom;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    @Override
    public String toString() {
        return "\nTercero{" + "tipoDocumento=" + tipoDocumento + ", identificacion=" + identificacion + ", primApe=" + primApe + ", segApe=" + segApe + ", primNom=" + primNom + ", segNom=" + segNom + ", razonSocial=" + razonSocial + ", direccion=" + direccion + ", telefono=" + telefono + ", email=" + email + ", pais=" + pais + ", departamento=" + departamento + ", municipio=" + municipio + ", regimen=" + regimen + '}';
    }

}

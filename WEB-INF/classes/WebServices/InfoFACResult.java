package WebServices;

import java.util.Arrays;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para InfoFACResult complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="InfoFACResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Procesados" type="{InsercionFacturacion}Resultado"/>
 *         &lt;element name="Errores" type="{InsercionFacturacion}Error"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoFACResult", propOrder = {
    "procesados",
    "errores"
})
public class InfoFACResult {

    @XmlElementWrapper(name = "Procesados")
    @XmlElement(name = "Resultado")
    protected Resultado[] procesados;
    @XmlElementWrapper(name = "Errores")
    @XmlElement(name = "Error")
    protected Error[] errores;

    public InfoFACResult() {
    }

    public Resultado[] getProcesados() {
        return procesados;
    }

    public void setProcesados(Resultado[] procesados) {
        this.procesados = procesados;
    }

    public Error[] getErrores() {
        return errores;
    }

    public void setErrores(Error[] errores) {
        this.errores = errores;
    }

     public String resultado() {

        String resultado = "";

        for (Error error : errores) {
            resultado += error.campo + ": " + error.error + " - ";  
        }

        for (Resultado res : procesados) {
            resultado += res.comprobante + ": " + res.numero + " - ";
        }

        return resultado;
    }

    public boolean auditado() {
        return errores.length == 0 && procesados.length > 0;
    }

    @Override
    public String toString() {
        return "\nInfoFACResult{" + "procesados=" + Arrays.toString(procesados) + ", errores=" + Arrays.toString(errores) + '}';
    }

}
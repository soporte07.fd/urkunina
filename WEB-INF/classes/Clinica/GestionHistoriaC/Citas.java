package Clinica.GestionHistoriaC;

import Clinica.Presentacion.HistoriaClinica.PacienteVO;
import Sgh.AdminSeguridad.Usuario;
import Sgh.Utilidades.Conexion;
import Sgh.Utilidades.Constantes;
import Sgh.Utilidades.LoggableStatement;
import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Citas {

    private Integer id;
    private String tipoId;
    private String pacienteTodo;
    private String existe;

    private Integer var1;
    private String var2;
    private String var3;
    private Integer var7;
  private String var11;
    private String noticia;
    private Integer idDocumento;
    public Constantes constantes;
    private Conexion cn;
    private StringBuffer sql = new StringBuffer();

    public Citas() {
        id = Integer.valueOf(0);
        var1 = Integer.valueOf(0);
        var2 = "";
        var3 = "";
        existe = "";
        var11 = "";
    }



    public String TienePendienteLE() {
        pacienteTodo = "NO_PENDIENTE";
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(" SELECT PENDI.PENDIENTE_LE FROM( ");
                localStringBuffer.append("   SELECT CASE WHEN ad.id_lista_espera IS NULL THEN 'PENDIENTE' ELSE 'NO_PENDIENTE' END AS PENDIENTE_LE ");
                localStringBuffer.append("   FROM   citas.lista_espera  le ");
                localStringBuffer.append("   LEFT JOIN  citas.agenda_detalle ad ON le.id = ad.id_lista_espera");
                localStringBuffer.append("   WHERE le.id_paciente = ?::integer AND le.tipo_cita = ? AND le.id_estado != 5::integer AND le.con_cita = 'NO' ");
                localStringBuffer.append(" )PENDI ORDER BY PENDI.PENDIENTE_LE");

                cn.prepareStatementSEL(2, localStringBuffer);

                cn.ps2.setString(1, var2);
                cn.ps2.setString(2, var3);

           //     System.out.println("--lespera =" + ((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        pacienteTodo = cn.rs2.getString("PENDIENTE_LE");
                    }
                }
                cn.cerrarPS(2);
            }

        } catch (SQLException localSQLException) {
            System.out.println("Error --> Paciente.java-- function siExisteLe--> SQLException --> " + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> Paciente.java-- function siExisteLe --> Exception --> " + localException.getMessage());
        }

        System.out.println("TienePendienteLE = " + pacienteTodo);
        return pacienteTodo;
    }

    public String TieneCitaFutura() {
        pacienteTodo = "SIN_CITA_FUTURA";
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append("SELECT CASE WHEN ad.id IS NOT NULL THEN 'PENDIENTE' ELSE 'NO_PENDIENTE' END AS PENDIENTE_LE  ");
                localStringBuffer.append("FROM   citas.agenda_detalle ad  ");
                localStringBuffer.append("INNER JOIN citas.agenda a ON ad.id_agenda = a.id  ");
                localStringBuffer.append("INNER JOIN citas.agenda_estado ae ON (ad.id_estado = ae.id and ae.exito = 'S')  ");
                localStringBuffer.append("WHERE ad.id_paciente = ?::integer ");
                localStringBuffer.append("AND ad.id_tipo = ?  ");
                localStringBuffer.append("AND ad.fecha_cita >  now()  ");
                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setString(1, var2);
                cn.ps2.setString(2, var3);

             //   System.out.println("--citafutura =" + ((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        pacienteTodo = cn.rs2.getString("PENDIENTE_LE");
                    }
                }
                cn.cerrarPS(2);
            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> Paciente.java-- function siExisteLe--> SQLException --> " + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> Paciente.java-- function siExisteLe --> Exception --> " + localException.getMessage());
        }
        System.out.println("TieneCitaFutura = " + pacienteTodo);
        return pacienteTodo;
    }

    public boolean espacioDeCitaDisponible() {
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(" SELECT id_estado FROM citas.agenda_detalle where id = ? ");
                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setInt(1, var7.intValue());

                if ((cn.selectSQL(2))
                        && (cn.rs2.next())) {

                    if (cn.rs2.getString("id_estado").equals("D")) {
                        cn.cerrarPS(2);

                        return true;
                    }

                    cn.cerrarPS(2);
                    return false;
                }

            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> Paciente.java-- function espacioDeCitaDisponible --> SQLException --> " + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> Paciente.java-- function espacioDeCitaDisponible --> Exception --> " + localException.getMessage());
        }

        return true;
    }

    public String consultarDisponibleDesdeListaEspera() {
        String str = "";
        try {
            if (cn.isEstado()) {
                if (TienePendienteLE().equals("NO_PENDIENTE")) {
                    if (TieneCitaFutura().equals("SIN_CITA_FUTURA")) {
                        str = "N";
                    } else {
                        str = "C";
                    }
                } else {
                    str = "S";
                }
            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
            str = "";
        }
        return str;
    }

    public String consultarDisponibleDesdeAgenda(){      //   System.out.println("............j............");
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                if (espacioDeCitaDisponible()) {
                    if (var11.equals("")) {
                        System.out.println("__SIN LISTA ESPERA");

                        if (TienePendienteLE().equals("NO_PENDIENTE")) {
                            if (TieneCitaFutura().equals("SIN_CITA_FUTURA")) {
                                existe = "N";
                            } else {
                                existe = "C";
                            }
                        } else {
                            existe = "S";
                        }
                    } else {
                        System.out.println("__CON LISTA ESPERA");

                        if (TieneCitaFutura().equals("SIN_CITA_FUTURA")) {
                            existe = "N";
                        } else {
                            existe = "C";
                        }
                    }
                } else {
                    existe = "O";
                }

            }
        } catch (Exception localException) {
            cn.exito = false;
        }
        return existe;
    }

    public boolean consultarDisponibleAgendaSinLE() {
   //     System.out.println("setVar2" + var2);
   //     System.out.println("setVar3" + var3);
   //     System.out.println("setVar4" + var4);

        cn.exito = true;
        try {
            if (cn.isEstado()) {
                if (TienePendienteLE().equals("NO_PENDIENTE")) {
                    existe = "N";
                } else {
                    existe = "S";
                }

            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en mEtodo crearPaciente() de Paciente.java");
            cn.exito = false;
        }
        return cn.exito;
    }


    public java.lang.Integer getId() {
        return id;
    }

    public void setId(java.lang.Integer value) {
        id = value;
    }

    public java.lang.String getTipoId() {
        return tipoId;
    }

    public void setTipoId(java.lang.String value) {
        tipoId = value;
    }

    public java.lang.String getPacienteTodo() {
        return pacienteTodo;
    }

    public void setPacienteTodo(java.lang.String value) {
        pacienteTodo = value;
    }

    public java.lang.String getExiste() {
        return existe;
    }

    public void setExiste(java.lang.String value) {
        existe = value;
    }

    public java.lang.Integer getVar1() {
        return var1;
    }

    public void setVar1(java.lang.Integer value) {
        var1 = value;
    }

    public java.lang.String getVar2() {
        return var2;
    }

    public void setVar2(java.lang.String value) {
        var2 = value;
    }

    public java.lang.String getVar3() {
        return var3;
    }

    public void setVar3(java.lang.String value) {
        var3 = value;
    }

    public java.lang.Integer getVar7() {
        return var7;
    }

    public void setVar7(java.lang.Integer value) {
        var7 = value;
    }

    public java.lang.String getVar11() {
        return var11;
    }

    public void setVar11(java.lang.String value) {
        var11 = value;
    }

    public java.lang.String getNoticia() {
        return noticia;
    }

    public void setNoticia(java.lang.String value) {
        noticia = value;
    }

    public java.lang.Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(java.lang.Integer value) {
        idDocumento = value;
    }

    public Sgh.Utilidades.Constantes getConstantes() {
        return constantes;
    }

    public void setConstantes(Sgh.Utilidades.Constantes value) {
        constantes = value;
    }

    public Sgh.Utilidades.Conexion getCn() {
        return cn;
    }

    public void setCn(Sgh.Utilidades.Conexion value) {
        cn = value;
    }

    public java.lang.StringBuffer getSql() {
        return sql;
    }

    public void setSql(java.lang.StringBuffer value) {
        sql = value;
    }
}

package Clinica.GestionHistoriaC;

import Sgh.Utilidades.*;
import Clinica.Presentacion.*;
import Clinica.Presentacion.HistoriaClinica.*;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.Date;
import java.lang.*;
import java.text.SimpleDateFormat;
import java.sql.*;
import java.io.File;
import java.lang.String;
import java.sql.*;

/**
 * @(#)Clase Conexion.java version 1.02 2015/12/09 Copyright(c) 2015 Firmas
 * Digital. JAS
 */
public class DocumentoHC {

    private String idPaciente;
    private String idDocumento;
    private String idAdmision;
    private String idTipoAdmision;
    private String idEvolucion;


    private String nomDocumento;
    private String tipoDocumento;
    private String totalDocumento;
    private String nomProfesional;
    private String fechaDocumento;
    private String tipoServicio;
    private String idProfesion;
    private String idEspecialidad;
    private String idIdAuxiliar;
    private String examenFisico;

    private String parametros;
    private String idQuery;

    public Constantes constantes;
    int numColumnas = 0;

    private int var1, var2, var7;
    private String var3, var4, var5, var6, var8, var9;

    //public Fecha fecha;
    private Conexion cn;

    private StringBuffer sql = new StringBuffer();

    public DocumentoHC() {
        numColumnas = 0;

        idPaciente = "";
        idDocumento = "";
        idAdmision = "";
        idTipoAdmision = "";
        nomDocumento = "";
        tipoDocumento = "";
        totalDocumento = "";
        nomProfesional = "";
        fechaDocumento = "";
        tipoServicio = "";
        idProfesion = "";
        idIdAuxiliar = "";

        var1 = 0;
        var2 = 0;
        var7 = 0;
        var3 = "";
        var4 = "";
        var5 = "";
        var6 = "";
        var8 = "";
        var9 = "";
        examenFisico = "";

    }

    //contar
    public int contar() {
        String var = "";
        int cuantos = 0;
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                sql.append("SELECT   count(cod_tipodoc) as total FROM adm.tipos_documentos ");
                sql.append("WHERE cod_tipodoc like ? AND desc_tipodoc like ? ");

                //sql.append("ORDER BY identificacion, ApellidosYNombres ASC");
                this.cn.prepareStatementSEL(Constantes.PS1, sql);
                //System.out.println(((LoggableStatement)cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    while (cn.rs1.next()) {
                        cuantos = (cn.rs1.getInt("total"));
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            //System.out.println("Error --> clase CentroSalud --> function contar --> SQLException --> "+e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            //System.out.println("Error --> clase Color --> function contar --> Exception --> " + e.getMessage());
            e.printStackTrace();
        }
        return cuantos;
    }//Fin funcion contar()

    public Object asignaBuscarDocumentosPaciente() {
        ArrayList<DocumentoHCVO> resultado = new ArrayList<DocumentoHCVO>();
        String var = "";
        DocumentoHCVO parametro;

        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQuery(7));
                cn.ps1.setString(1, this.idPaciente);
                // hasta aqui llego falta hacer funcionar el select
                //    System.out.println(" DocumentoHC -> asignaBuscarDocumentosPaciente "+((LoggableStatement)cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    while (cn.rs1.next()) {
                        parametro = new DocumentoHCVO();
                        parametro.setTipoDocumento(cn.rs1.getString("tipoDoc"));
                        parametro.setNomDocumento(cn.rs1.getString("nomDoc"));
                        parametro.setTotalDocumento(cn.rs1.getString("cuantosDoc"));
                        parametro.setNomProfesional(cn.rs1.getString("nomProfesional"));
                        parametro.setFechaDocumento(cn.rs1.getString("ultimaFecha"));
                        resultado.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase DocumentoHC --> function asignaBuscarDocumentosPaciente --> SQLException --> " + e.getMessage());
            //    e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error --> clase DocumentoHC --> function asignaBuscarDocumentosPaciente --> Exception --> " + e.getMessage());
            //    e.printStackTrace();
        }
        return resultado;
    }//Fin funcion buscarPag()

    public Object buscarConciliacion() {
        ArrayList<DocumentoHCVO> resultado = new ArrayList<DocumentoHCVO>();
        String var = "";
        //   DocumentoHCVO parametro;

        try {
            if (cn.isEstado()) {

                sql.delete(0, sql.length());
                if (cn.getMotor().equals("sqlserver")) {
                    sql.append("SELECT conciliacion_existe, conciliacion_descripcion FROM hc.evolucion WHERE id = ? ");
                } else {
                    System.out.println("--POSTGRES A ");
                    sql.append("select conciliacion_existe, conciliacion_descripcion from hc.evolucion where id = ?::integer ");
                }
                cn.prepareStatementSEL(Constantes.PS1, sql);
                cn.ps1.setString(1, this.var3);

                if (cn.selectSQL(Constantes.PS1)) {
                    while (cn.rs1.next()) {
                        /*
                  parametro = new DocumentoHCVO();
                  parametro.setC1(cn.rs1.getString("conciliacion_existe"));
                  parametro.setC2(cn.rs1.getString("conciliacion_descripcion"));
                  resultado.add(parametro); */
                        this.var4 = cn.rs1.getString("conciliacion_existe");
                        this.var5 = cn.rs1.getString("conciliacion_descripcion");
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase DocumentoHC --> function buscarConciliacion --> SQLException --> " + e.getMessage());

        } catch (Exception e) {
            System.out.println("Error --> clase DocumentoHC --> function buscarConciliacion --> Exception --> " + e.getMessage());
        }
        return resultado;
    }

    public int maximoValorTabla() {
        int valor = 0;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append(" SELECT  id, id2  FROM hc.candado");
                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        valor = cn.rs2.getInt("id");
                        this.idAdmision = cn.rs2.getString("id2");
                        cn.cerrarPS(Constantes.PS2);
                    }
                }
            }
        } catch (SQLException e) {
            //  System.out.println("Error --> clase  -->DocumentoHc.java-- function maximoValorTabla --> SQLException --> "+e.getMessage());
        } catch (Exception e) {
            //  System.out.println("Error --> clase  -->DocumentoHc.java-- function maximoValorTabla --> Exception --> " + e.getMessage());
        }
        if (valor == 0) {
            valor = 1;
        }
        return valor;
    }

    public int maximoValorTablaDocInventario() {
        int valor = 0;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append(" SELECT id_doc FROM inventario.candado_documentos;");
                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        this.idDocumento = cn.rs2.getString("id_doc");
                        cn.cerrarPS(Constantes.PS2);
                    }
                }
            }
        } catch (SQLException e) {
            //  System.out.println("Error --> clase  -->DocumentoHc.java-- function maximoValorTabla --> SQLException --> "+e.getMessage());
        } catch (Exception e) {
            //  System.out.println("Error --> clase  -->DocumentoHc.java-- function maximoValorTabla --> Exception --> " + e.getMessage());
        }
        if (valor == 0) {
            valor = 1;
        }
        return valor;
    }

    public boolean crearFolioConAdmisionEnviada() {
        System.out.println("--crearFolioConAdmisionEnviada = " +this.idTipoAdmision);
        cn.exito = true;

        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                this.idDocumento = "";
                sql.delete(0, sql.length());
                sql.append(" INSERT INTO hc.candado(id, id2) values( ( SELECT COALESCE(MAX(id)+1, 1) FROM  hc.evolucion) ,  (SELECT  MAX(id) ID FROM  facturacion.admision where id_paciente = "+this.idPaciente+"  and  id_tipo_admision =  '"+this.idTipoAdmision +"')::INTEGER ); ");
                sql.append(" INSERT INTO hc.evolucion ( id, tipo, id_admision, id_paciente, id_medico, id_especialidad, id_profesion, id_auxiliar) ");
                sql.append(" VALUES ((SELECT id FROM hc.candado), ?, (SELECT id2 FROM hc.candado),");
                sql.append(" ?::INTEGER,?, (SELECT id_especialidad FROM facturacion.admision where id = (SELECT id2 FROM hc.candado) )::INTEGER, ?::INTEGER, ?); ");
                sql.append(" INSERT INTO formulario." + this.tipoDocumento + " (id_evolucion) ");
                sql.append(" VALUES((SELECT id FROM hc.candado));");
                sql.append(" UPDATE facturacion.admision_agenda_detalle ");
                sql.append(" SET  id_estado = 'ATENDIDO', usuario_atendio =?, ");
                sql.append(" fecha_atendido = now() WHERE id_admision = (SELECT id2 FROM hc.candado) ");

                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.ps1.setString(1, this.tipoDocumento);
                cn.ps1.setString(2, this.idPaciente);
                cn.ps1.setString(3, cn.usuario.getIdentificacion());
                cn.ps1.setString(4, cn.usuario.getIdProfesion());
                cn.ps1.setString(5, this.getIdIdAuxiliar());
                cn.ps1.setString(6, cn.usuario.getLogin());

                if (cn.imprimirConsola) {
                    System.out.println("--CREACION FOLIO:n " + ((LoggableStatement) cn.ps1).getQueryString());
                }

                cn.iduSQL(Constantes.PS1);
                this.idDocumento = String.valueOf(maximoValorTabla());

                sql.delete(0, sql.length());
                sql.append("delete from hc.candado;");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en mEtodo guardarDocumentoHc() de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en mEtodo guardarDocumentoHc() de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        }

        return cn.exito;
    }

    public boolean consultarCuentaConAutorizacion() {
        System.out.println("consultarCuentaConAutorizacion() "+var3);
        cn.exito = true;
         var6 = "";

        if (cn.isEstado()) {
            try {


                    cn.prepareStatementSEL(Constantes.PS1, cn.traerElQuery(98));


                    cn.ps1.setString(1, var3);
                     System.out.println(((LoggableStatement) cn.ps1).getQueryString());

                    if (cn.selectSQL(Constantes.PS1)) {
                        while (cn.rs1.next()) {

                            System.out.println(cn.rs1.getString("no_autorizacion"));
                            var6 = cn.rs1.getString("no_autorizacion");
                        }
                        cn.cerrarPS(Constantes.PS1);

                } else {
                    var6 = "";
                }
            } catch (SQLException e) {
                System.out.println("Error --> clase DocumentoHC --> function consultarCuentaConNoAutorizacion --> SQLException --> " + e.getMessage());
            } catch (Exception e) {
                System.out.println("Error --> clase DocumentoHC --> function consultarCuentaConNoAutorizacion --> Exception --> " + e.getMessage());
            }
        }
        return cn.exito;
    }

    ////////
    public boolean consultaExamenFisico() {

        boolean fisico = false;
        System.out.println("--------------ExamenFisico-------------");

        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("SELECT CASE WHEN examen_fisico IS NULL THEN 'APARIENCIA Y GENERAL:\nCABEZA Y CUELLO:\nGENITO URINARIO:\nEXTREMIDADES:\nSISTEMA NERVIOSO CENTRAL:\nPIEL Y ANEXOS:\nT�RAX - RESPIRATORIO:' ELSE examen_fisico END examen_fisico\n"
                        + "  FROM hc.evolucion\n"
                        + "  WHERE id = ?::INTEGER");

                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setString(1, idEvolucion);

                System.out.println("SQL ExamenFisico = " + ((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        examenFisico  = cn.rs2.getString("examen_fisico");
                        fisico = true;
                        cn.cerrarPS(Constantes.PS2);
                        break;
                    }
                }

            }
        } catch (SQLException e) {
            System.out.println("Error --> Fcturacion.java-- function consultaExamenFisico --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> Fcturacion.java-- function consultaExamenFisico --> Exception --> " + e.getMessage());
        }
        return fisico;
    }


    /////////
    public boolean crearFolioUltimaAdmisionDelPaciente() {
        System.out.println("--crearFolioUltimaAdmisionDelPaciente ");
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                this.idDocumento = "";
                sql.delete(0, sql.length());

                    System.out.println("--POSTGRES B ");
                    sql.append("INSERT INTO hc.candado(id) ( SELECT MAX(id)+1 FROM  hc.evolucion ); ");
                    sql.append("UPDATE hc.candado SET id2 = ( SELECT  MAX(id) FROM  facturacion.admision WHERE id_paciente = ?::INTEGER); ");
                    sql.append("INSERT INTO hc.evolucion (id,tipo,id_admision,identificacion_paciente,id_medico,id_especialidad,id_profesion,id_auxiliar) ");
                    sql.append("VALUES ((SELECT id FROM hc.candado),?,(SELECT id2 FROM hc.candado),?,?,?::INTEGER,?::INTEGER,?); ");
                    sql.append("INSERT INTO formulario." + this.tipoDocumento + " (id_evolucion) ");
                    sql.append("VALUES((SELECT id FROM hc.candado));");
                    sql.append("UPDATE facturacion.admision_agenda_detalle ");
                    sql.append("SET  id_estado = 'ATENDIDO', usuario_atendio =?, ");
                    sql.append("fecha_atendido = now() WHERE id_admision = (SELECT id2 FROM hc.candado) ");

                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.ps1.setString(1, this.idPaciente);
                cn.ps1.setString(2, this.tipoDocumento);
                cn.ps1.setString(3, this.idPaciente);
                cn.ps1.setString(4, cn.usuario.getIdentificacion());
                cn.ps1.setString(5, this.getIdEspecialidad());
                cn.ps1.setString(6, cn.usuario.getIdProfesion());
                cn.ps1.setString(7, this.getIdIdAuxiliar());
                cn.ps1.setString(8, cn.usuario.getLogin());

                if (cn.imprimirConsola) {
                    System.out.println("--CREACION FOLIO:n " + ((LoggableStatement) cn.ps1).getQueryString());
                }

                cn.iduSQL(Constantes.PS1);

                this.idDocumento = String.valueOf(maximoValorTabla());

                sql.delete(0, sql.length());
                sql.append("delete from hc.candado;");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en m�todo guardarDocumentoHc() de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en m�todo guardarDocumentoHc() de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        }

        return cn.exito;
    }

    public boolean crearDocumentoInventarioBodegaConsumo() {//      System.out.println("--crearDocumentoInventarioProgramacion ");
        cn.exito = true;      //preguntar
        String ideMed = cn.usuario.getIdentificacion();
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                this.idDocumento = "";
                sql.delete(0, sql.length());

                if (cn.getMotor().equals("sqlserver")) {
                    sql.append(" INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(id)+1 FROM  inventario.documentos ); ");
                    sql.append(" INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_usuario_crea)  ");
                    sql.append(" VALUES ((SELECT id_doc FROM INVENTARIO.candado_documentos),?,?,?); ");
                    sql.append(" INSERT INTO inventario.documentos_programacion( id_admision, id_documento) VALUES (?,(SELECT id_doc FROM INVENTARIO.candado_documentos)); ");
                } else {
                    System.out.println("--POSTGRES A -");
                    sql.append(" INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(id)+1 FROM  inventario.documentos ); ");
                    sql.append(" INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_usuario_crea)  ");
                    sql.append(" VALUES ((SELECT id_doc FROM INVENTARIO.candado_documentos)::integer,?::integer,?::integer,?); ");
                    if (this.var3.equals("SIN_ADMISION") || this.var3.equals("OTROS")) {
                        /* Solo se usa para devoluciones sin admision */
                        sql.append(" INSERT INTO inventario.documentos_programacion_devolucion( id_cita, id_documento, sw_origen) VALUES (?::integer,(SELECT id_doc FROM INVENTARIO.candado_documentos)::integer, ?); ");
                    } else {
                        sql.append(" INSERT INTO inventario.documentos_programacion( id_admision, id_documento, sw_origen) VALUES (?::integer,(SELECT id_doc FROM INVENTARIO.candado_documentos)::integer, ?); ");
                    }
                }
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.ps1.setString(1, this.tipoDocumento);
                cn.ps1.setInt(2, this.var1); //id bodega
                while (ideMed.equals("")) {
                    ideMed = cn.usuario.getIdentificacion();
                }
                cn.ps1.setString(3, ideMed);
                cn.ps1.setInt(4, this.var2); //id cita
                cn.ps1.setString(5, this.var3); //id_origen

                if (cn.imprimirConsola) {
                    System.out.println("--CREACION doc:n " + ((LoggableStatement) cn.ps1).getQueryString());
                }

                cn.iduSQL(Constantes.PS1);

//             this.idDocumento =  String.valueOf(maximoValorTabla()) ;
                maximoValorTablaDocInventario();

                sql.delete(0, sql.length());
                sql.append("delete from inventario.candado_documentos;");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en m�todo guardarDocumentoHc-Inv() de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en m�todo guardarDocumentoHc-Inv() de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        }

        return cn.exito;
    }

    public boolean crearDocInvBodega() {
        System.out.println("--crearDocInvBodega ");
        cn.exito = true;      //preguntar

        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                this.idDocumento = "";
                sql.delete(0, sql.length());

                if (cn.getMotor().equals("sqlserver")) {
                    sql.append(" INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(id)+1 FROM  inventario.documentos ); ");
                    sql.append(" INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_usuario_crea)  ");
                    sql.append(" VALUES ((SELECT id_doc FROM INVENTARIO.candado_documentos),?,?,?); ");
                } else {
                    System.out.println("--POSTGRES A -");
                    sql.append(" INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(id)+1 FROM  inventario.documentos ); ");
                    sql.append(" INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_usuario_crea)  ");
                    sql.append(" VALUES ((SELECT id_doc FROM INVENTARIO.candado_documentos)::integer,?::integer,?::integer,?); ");
                }
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.ps1.setString(1, this.tipoDocumento);
                cn.ps1.setInt(2, this.var1); //id bodega
                cn.ps1.setString(3, cn.usuario.getIdentificacion());

                if (cn.imprimirConsola) {
                    System.out.println("--CREACION doc:n " + ((LoggableStatement) cn.ps1).getQueryString());
                }

                cn.iduSQL(Constantes.PS1);

//             this.idDocumento =  String.valueOf(maximoValorTabla()) ;
                maximoValorTablaDocInventario();

                sql.delete(0, sql.length());
                sql.append("delete from inventario.candado_documentos;");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en m�todo guardarDocumentoHc-inv() de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en m�todo guardarDocumentoHc-inv() de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        }

        return cn.exito;
    }

    public boolean crearDocumentoInventarioProgramacion() {//      System.out.println("--crearDocumentoInventarioProgramacion ");
        cn.exito = true;      //preguntar

        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                this.idDocumento = "";
                sql.delete(0, sql.length());

                if (cn.getMotor().equals("sqlserver")) {
                    sql.append(" INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(id)+1 FROM  inventario.documentos ); ");
                    sql.append(" INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_usuario_crea)  ");
                    sql.append(" VALUES ((SELECT id_doc FROM INVENTARIO.candado_documentos),?,?,?); ");
                    sql.append(" INSERT INTO inventario.documentos_programacion( id_cita, id_documento) VALUES (?,(SELECT id_doc FROM INVENTARIO.candado_documentos)); ");
                } else {
                    System.out.println("--POSTGRES A -");
                    sql.append(" INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(id)+1 FROM  inventario.documentos ); ");
                    sql.append(" INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_usuario_crea)  ");
                    sql.append(" VALUES ((SELECT id_doc FROM INVENTARIO.candado_documentos)::integer,?::integer,?::integer,?); ");
                    sql.append(" INSERT INTO inventario.documentos_programacion( id_cita, id_documento,sw_origen) VALUES (?::integer,(SELECT id_doc FROM INVENTARIO.candado_documentos)::integer,'DESPACHO'); ");
                }
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.ps1.setString(1, this.tipoDocumento);
                cn.ps1.setInt(2, this.var1); //id bodega
                cn.ps1.setString(3, cn.usuario.getIdentificacion());
                cn.ps1.setInt(4, this.var2); //id cita

                if (cn.imprimirConsola) {
                    System.out.println("--CREACION FOLIO:n " + ((LoggableStatement) cn.ps1).getQueryString());
                }

                cn.iduSQL(Constantes.PS1);

//             this.idDocumento =  String.valueOf(maximoValorTabla()) ;
                maximoValorTablaDocInventario();

                sql.delete(0, sql.length());
                sql.append("delete from inventario.candado_documentos;");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en m�todo guardarDocumentoHc-inv() de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en m�todo guardarDocumentoHc-inv() de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        }

        return cn.exito;
    }

    public boolean verificaCantidadInventario() {
        boolean bool = false;
        int valor = 0;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("\n SELECT   ");
                sql2.append("\n max( cantidad ) cantidad ");
                sql2.append("\n FROM inventario.transacciones t ");
                sql2.append("\n inner join inventario.documentos d on (t.id_documento = d.id  and d.id_documento_tipo in (5)   )  ");		// solo  documentos facturas de entrada (5)
                sql2.append("\n where t.id_articulo =  ?::integer ");

                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setInt(1, this.var1);

                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        valor = cn.rs2.getInt("cantidad");
                        this.var2 = cn.rs2.getInt("cantidad");
                        System.out.println("--Result- =" + cn.rs2.getString("cantidad"));
                    }
                    cn.cerrarPS(Constantes.PS2);
                }

            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en m�todo 1 verificaCantidadInventario(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en m�todo 2 verificaCantidadInventario(), de Paciente.java");
            return false;
        }
        return true;
    }

    public boolean verificaCantidadInventarioMinimo() {
        boolean bool = false;
        int valor = 0;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("\n SELECT   ");
                sql2.append("\n min( cantidad ) cantidad ");
                sql2.append("\n FROM inventario.transacciones t ");
                sql2.append("\n inner join inventario.documentos d on (t.id_documento = d.id  and d.id_documento_tipo in (5)   )  ");		// solo  documentos facturas de entrada (5)
                sql2.append("\n where t.id_articulo =  ?::integer ");

                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setInt(1, this.var1);

                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        valor = cn.rs2.getInt("cantidad");
                        this.var2 = cn.rs2.getInt("cantidad");
                        System.out.println("--Result- =" + cn.rs2.getString("cantidad"));
                    }
                    cn.cerrarPS(Constantes.PS2);
                }

            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en m�todo 1 verificaCantidadInventario(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en m�todo 2 verificaCantidadInventario(), de Paciente.java");
            return false;
        }
        return true;
    }

    public boolean verificaValorInventario() {
        boolean bool = false;
        int valor = 0;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("\n SELECT   ");
                sql2.append("\n max( t.valor_unitario ) cantidad ");
                sql2.append("\n FROM inventario.transacciones t ");
                sql2.append("\n inner join inventario.documentos d on (t.id_documento = d.id  and d.id_documento_tipo in (5)   )  ");		// solo  documentos facturas de entrada (5)
                sql2.append("\n where t.id_articulo =  ?::integer ");

                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setInt(1, this.var1);

                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        valor = cn.rs2.getInt("cantidad");
                        this.var2 = cn.rs2.getInt("cantidad");
                        System.out.println("--Result- =" + cn.rs2.getString("cantidad"));
                    }
                    cn.cerrarPS(Constantes.PS2);
                }

            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en m�todo 1 verificaValorInventario(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en m�todo 2 verificaValorInventario(), de Paciente.java");
            return false;
        }
        return true;
    }

    public boolean verificaValorInventarioMinimo() {
        boolean bool = false;
        int valor = 0;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("\n SELECT   ");
                sql2.append("\n min( t.valor_unitario ) cantidad ");
                sql2.append("\n FROM inventario.transacciones t ");
                sql2.append("\n inner join inventario.documentos d on (t.id_documento = d.id  and d.id_documento_tipo in (5)   )  ");		// solo  documentos facturas de entrada (5)
                sql2.append("\n where t.id_articulo =  ?::integer ");

                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setInt(1, this.var1);

                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        valor = cn.rs2.getInt("cantidad");
                        this.var2 = cn.rs2.getInt("cantidad");
                        System.out.println("--Result- =" + cn.rs2.getString("cantidad"));
                    }
                    cn.cerrarPS(Constantes.PS2);
                }

            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en m�todo 1 verificaValorInventario(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en m�todo 2 verificaValorInventario(), de Paciente.java");
            return false;
        }
        return true;
    }

    public boolean existeFacturaDocumento() {
        boolean bool = false;
        int valor = 0;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("\n select count(doc.numero ) cantidad  ");
                sql2.append("\n from inventario.documentos doc ");
                sql2.append("\n where trim (upper(doc.numero)) = trim( upper(?)) ");
                sql2.append("\n and doc.id_estado = '1'  ");
                sql2.append("\n and doc.id_tercero = ?::integer ");

                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setString(1, this.var4);
                cn.ps2.setInt(2, this.var7); //id cedula

                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        valor = cn.rs2.getInt("cantidad");
                        System.out.println("--Result- =" + cn.rs2.getString("cantidad"));
                    }
                    cn.cerrarPS(Constantes.PS2);
                }
                if (valor > 0) {
                    /* hacer el cambio de lote */
                    System.out.println("--No se puede crear el documento. Ya existe el numero de factura ");
                    bool = false;
                } else {
                    System.out.println("--Si se puede crear el documento. no existe el numero de factura ");
                    bool = true;
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en m�todo 1 existeFacturaDocumento(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en m�todo 2 existeFacturaDocumento(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean crearDocumentoInventarioBodega() {
        cn.exito = true;
        String ideMed = cn.usuario.getIdentificacion();
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                this.idDocumento = "";
                sql.delete(0, sql.length());

                if (cn.getMotor().equals("sqlserver")) {

                    sql.append(" INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(id)+1 FROM  inventario.documentos ); ");
                    sql.append(" INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_usuario_crea,   observacion, numero, id_tercero, fecha_documento )  ");
                    sql.append(" VALUES ((SELECT id_doc FROM INVENTARIO.candado_documentos),?,?,?,?,?,?,? ); ");

                } else {
                    System.out.println("--POSTGRES A -");
                    sql.append(" INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(id)+1 FROM  inventario.documentos ); ");
                    sql.append(" INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_usuario_crea,   observacion, numero, id_tercero, fecha_documento, valor_flete )  ");
                    sql.append(" VALUES ((SELECT id_doc FROM INVENTARIO.candado_documentos)::integer,?::integer,?::integer,?,?,?,?::integer,coalesce(cast(nullif(?,'') as TIMESTAMP),null) ,?::integer ); ");
                }
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.ps1.setString(1, this.tipoDocumento);
                cn.ps1.setInt(2, this.var1); //id bodega
                while (ideMed.equals("")) {
                    ideMed = cn.usuario.getIdentificacion();
                }
                cn.ps1.setString(3, ideMed);

                cn.ps1.setString(4, this.var3); //id observacion
                cn.ps1.setString(5, this.var4); //id numero
                cn.ps1.setInt(6, this.var7); //id cedula
                cn.ps1.setString(7, this.var5); //fecha documento factura
                cn.ps1.setInt(8, this.var2); //valor flete

                if (cn.imprimirConsola) {
                    System.out.println("--CREACION doc_inv:n " + ((LoggableStatement) cn.ps1).getQueryString());
                }

                cn.iduSQL(Constantes.PS1);

//             this.idDocumento =  String.valueOf(maximoValorTabla()) ;
                maximoValorTablaDocInventario();

                sql.delete(0, sql.length());
                sql.append("delete from inventario.candado_documentos;");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en m�todo guardarDocumentoHc-inv() de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en m�todo guardarDocumentoHc inv () de DocumentoHc.java");
            this.idDocumento = "";
            cn.exito = false;
        }

        return cn.exito;
    }

    public boolean ocuparCandadoReporte() {
        boolean van = true;
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                sql.delete(0, sql.length());
                sql.append("INSERT INTO REPORTE.CANDADO (ID) VALUES (1)");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en m�todo ocuparCandadoReporte() de DocumentoHc.java");
            van = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en m�todo ocuparCandadoReporte() de DocumentoHc.java");
            van = false;
        }
        System.out.println("SE REALIZO?= " + van);
        return van;
    }

    public boolean liberarCandadoReporte() {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                sql.delete(0, sql.length());
                sql.append("DELETE FROM REPORTE.CANDADO ");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en m�todo liberarCandadoReporte() de DocumentoHc.java");
            cn.exito = false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en m�todo liberarCandadoReporte() de DocumentoHc.java");
            cn.exito = false;
        }
        return cn.exito;
    }

    public int traerQueryDelTipoDocumento() {
        int valor = 0;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append(" SELECT id_query FROM FORMULARIO.TIPO_FORMULARIO WHERE ID = ? ");
                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setString(1, this.tipoDocumento);
                //   System.out.println("SQL idqueryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy = "+((LoggableStatement)cn.ps2).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        valor = cn.rs2.getInt("id_query");
                        cn.cerrarPS(Constantes.PS2);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("Error --> clase  -->DocumentoHc.java-- function traerQueryDelTipoDocumento --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> clase  -->DocumentoHc.java-- function traerQueryDelTipoDocumento --> Exception --> " + e.getMessage());
        }
        return valor;
    }

    public StringBuffer traerElQueryEvoluciones(String campo) {
        StringBuffer valor = new StringBuffer();
        valor.delete(0, valor.length());
        StringBuffer sqlQ = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sqlQ.delete(0, sqlQ.length());
                sqlQ.append("SELECT " + campo + " query FROM  FORMULARIO.TIPO_FORMULARIO WHERE ID = ? ");
                this.cn.prepareStatementSEL(Constantes.PS5, sqlQ);
                this.cn.ps5.setString(1, this.tipoDocumento);
                if (this.cn.selectSQL(Constantes.PS5)) {
                    while (this.cn.rs5.next()) {
                        valor.append(this.cn.rs5.getString("query"));
                        this.cn.cerrarPS(Constantes.PS5);
                    }
                }
            }
        } catch (SQLException e) {
        } catch (Exception e) {
        }
        return valor;
    }

    public boolean modificarCRUD() {
        cn.exito = true;
        String[] temp = null;
        temp = parametros.split("_-");

        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                cn.prepareStatementIDU(Constantes.PS1, traerElQueryEvoluciones("QUPDATE"));

                for (int i = 1; i < temp.length; i++) {
                    if (temp[i].equals("x.X.x")) {
                        cn.ps1.setString(i, "");
                    } else {
                        cn.ps1.setString(i, temp[i]);
                    }
                }
                if (cn.imprimirConsola) {
                    System.out.println((new StringBuilder()).append("--------1 = ").append(((LoggableStatement) cn.ps1).getQueryString()).toString());
                }

                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en m�todo modificarCRUD() de grilla.java");
            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en m�todo modificarCRUD() de grilla.java");
            return false;
        }
        return true;
    }

    public Object traerContenidoDocumentoSeleccionado() {
        ArrayList<DocumentoHCVO> resultado = new ArrayList<DocumentoHCVO>();
        ArrayList nomColumnas = new ArrayList();
        nomColumnas.clear();
        String[] temp = null;
        String parametros = "";
        numColumnas = 0;

        int totalParametrosQuery = 0;
        temp = parametros.split("_-");
        DocumentoHCVO parametro;
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());

                cn.prepareStatementSEL(Constantes.PS1, traerElQueryEvoluciones("QSELECT"));
                cn.ps1.setString(1, this.idDocumento);

                if (cn.imprimirConsola) {
                    System.out.println("---------------- SQL EVOLUCION " + this.tipoDocumento + " :");
                    System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                }

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData();
                    /**
                     * PARA SACAR EL NUMERO DE COLUMNAS*
                     */
                    numColumnas = rsmd.getColumnCount();
                    for (int i = 1; i < numColumnas + 1; i++) {
                        /**
                         * PARA SACAR EL NOMBRE DE LAS COLUMNAS*
                         */
                        nomColumnas.add(rsmd.getColumnName(i));
                        // System.out.println("nombre columna="+rsmd.getColumnName(i));

                    }
                    /**/

                    while (cn.rs1.next()) {

                        parametro = new DocumentoHCVO();
                        if (numColumnas >= 1) {
                            parametro.setC1(cn.rs1.getString(String.valueOf(nomColumnas.get(0))));
                            /**
                             * CADA NOMBRE DE COLUMNAS, SE INGRESARA EN CADA C1,
                             * C2.. *
                             */
                        }
                        if (numColumnas >= 2) {
                            parametro.setC2(cn.rs1.getString(String.valueOf(nomColumnas.get(1))));
                        }
                        if (numColumnas >= 3) {
                            parametro.setC3(cn.rs1.getString(String.valueOf(nomColumnas.get(2))));
                        }
                        if (numColumnas >= 4) {
                            parametro.setC4(cn.rs1.getString(String.valueOf(nomColumnas.get(3))));
                        }
                        if (numColumnas >= 5) {
                            parametro.setC5(cn.rs1.getString(String.valueOf(nomColumnas.get(4))));
                        }
                        if (numColumnas >= 6) {
                            parametro.setC6(cn.rs1.getString(String.valueOf(nomColumnas.get(5))));
                        }
                        if (numColumnas >= 7) {
                            parametro.setC7(cn.rs1.getString(String.valueOf(nomColumnas.get(6))));
                        }
                        if (numColumnas >= 8) {
                            parametro.setC8(cn.rs1.getString(String.valueOf(nomColumnas.get(7))));
                        }
                        if (numColumnas >= 9) {
                            parametro.setC9(cn.rs1.getString(String.valueOf(nomColumnas.get(8))));
                        }
                        if (numColumnas >= 10) {
                            parametro.setC10(cn.rs1.getString(String.valueOf(nomColumnas.get(9))));
                        }
                        if (numColumnas >= 11) {
                            parametro.setC11(cn.rs1.getString(String.valueOf(nomColumnas.get(10))));
                        }
                        if (numColumnas >= 12) {
                            parametro.setC12(cn.rs1.getString(String.valueOf(nomColumnas.get(11))));
                        }
                        if (numColumnas >= 13) {
                            parametro.setC13(cn.rs1.getString(String.valueOf(nomColumnas.get(12))));
                        }
                        if (numColumnas >= 14) {
                            parametro.setC14(cn.rs1.getString(String.valueOf(nomColumnas.get(13))));
                        }
                        if (numColumnas >= 15) {
                            parametro.setC15(cn.rs1.getString(String.valueOf(nomColumnas.get(14))));
                        }
                        if (numColumnas >= 16) {
                            parametro.setC16(cn.rs1.getString(String.valueOf(nomColumnas.get(15))));
                        }
                        if (numColumnas >= 17) {
                            parametro.setC17(cn.rs1.getString(String.valueOf(nomColumnas.get(16))));
                        }
                        if (numColumnas >= 18) {
                            parametro.setC18(cn.rs1.getString(String.valueOf(nomColumnas.get(17))));
                        }
                        if (numColumnas >= 19) {
                            parametro.setC19(cn.rs1.getString(String.valueOf(nomColumnas.get(18))));
                        }
                        if (numColumnas >= 20) {
                            parametro.setC20(cn.rs1.getString(String.valueOf(nomColumnas.get(19))));
                        }
                        if (numColumnas >= 21) {
                            parametro.setC21(cn.rs1.getString(String.valueOf(nomColumnas.get(20))));
                        }
                        if (numColumnas >= 22) {
                            parametro.setC22(cn.rs1.getString(String.valueOf(nomColumnas.get(21))));
                        }
                        if (numColumnas >= 23) {
                            parametro.setC23(cn.rs1.getString(String.valueOf(nomColumnas.get(22))));
                        }
                        if (numColumnas >= 24) {
                            parametro.setC24(cn.rs1.getString(String.valueOf(nomColumnas.get(23))));
                        }
                        if (numColumnas >= 25) {
                            parametro.setC25(cn.rs1.getString(String.valueOf(nomColumnas.get(24))));
                        }
                        if (numColumnas >= 26) {
                            parametro.setC26(cn.rs1.getString(String.valueOf(nomColumnas.get(25))));
                        }
                        if (numColumnas >= 27) {
                            parametro.setC27(cn.rs1.getString(String.valueOf(nomColumnas.get(26))));
                        }
                        if (numColumnas >= 28) {
                            parametro.setC28(cn.rs1.getString(String.valueOf(nomColumnas.get(27))));
                        }
                        if (numColumnas >= 29) {
                            parametro.setC29(cn.rs1.getString(String.valueOf(nomColumnas.get(28))));
                        }
                        if (numColumnas >= 30) {
                            parametro.setC30(cn.rs1.getString(String.valueOf(nomColumnas.get(29))));
                        }
                        if (numColumnas >= 31) {
                            parametro.setC31(cn.rs1.getString(String.valueOf(nomColumnas.get(30))));
                        }
                        if (numColumnas >= 32) {
                            parametro.setC32(cn.rs1.getString(String.valueOf(nomColumnas.get(31))));
                        }
                        if (numColumnas >= 33) {
                            parametro.setC33(cn.rs1.getString(String.valueOf(nomColumnas.get(32))));
                        }
                        if (numColumnas >= 34) {
                            parametro.setC34(cn.rs1.getString(String.valueOf(nomColumnas.get(33))));
                        }
                        if (numColumnas >= 35) {
                            parametro.setC35(cn.rs1.getString(String.valueOf(nomColumnas.get(34))));
                        }
                        if (numColumnas >= 36) {
                            parametro.setC36(cn.rs1.getString(String.valueOf(nomColumnas.get(35))));
                        }
                        if (numColumnas >= 37) {
                            parametro.setC37(cn.rs1.getString(String.valueOf(nomColumnas.get(36))));
                        }
                        if (numColumnas >= 38) {
                            parametro.setC38(cn.rs1.getString(String.valueOf(nomColumnas.get(37))));
                        }
                        if (numColumnas >= 39) {
                            parametro.setC39(cn.rs1.getString(String.valueOf(nomColumnas.get(38))));
                        }
                        if (numColumnas >= 40) {
                            parametro.setC40(cn.rs1.getString(String.valueOf(nomColumnas.get(39))));
                        }
                        if (numColumnas >= 41) {
                            parametro.setC41(cn.rs1.getString(String.valueOf(nomColumnas.get(40))));
                        }
                        if (numColumnas >= 42) {
                            parametro.setC42(cn.rs1.getString(String.valueOf(nomColumnas.get(41))));
                        }
                        if (numColumnas >= 43) {
                            parametro.setC43(cn.rs1.getString(String.valueOf(nomColumnas.get(42))));
                        }
                        if (numColumnas >= 44) {
                            parametro.setC44(cn.rs1.getString(String.valueOf(nomColumnas.get(43))));
                        }
                        if (numColumnas >= 45) {
                            parametro.setC45(cn.rs1.getString(String.valueOf(nomColumnas.get(44))));
                        }
                        if (numColumnas >= 46) {
                            parametro.setC46(cn.rs1.getString(String.valueOf(nomColumnas.get(45))));
                        }
                        if (numColumnas >= 47) {
                            parametro.setC47(cn.rs1.getString(String.valueOf(nomColumnas.get(46))));
                        }
                        if (numColumnas >= 48) {
                            parametro.setC48(cn.rs1.getString(String.valueOf(nomColumnas.get(47))));
                        }
                        if (numColumnas >= 49) {
                            parametro.setC49(cn.rs1.getString(String.valueOf(nomColumnas.get(48))));
                        }
                        if (numColumnas >= 50) {
                            parametro.setC50(cn.rs1.getString(String.valueOf(nomColumnas.get(49))));
                        }
                        if (numColumnas >= 51) {
                            parametro.setC51(cn.rs1.getString(String.valueOf(nomColumnas.get(50))));
                        }
                        if (numColumnas >= 52) {
                            parametro.setC52(cn.rs1.getString(String.valueOf(nomColumnas.get(51))));
                        }
                        if (numColumnas >= 53) {
                            parametro.setC53(cn.rs1.getString(String.valueOf(nomColumnas.get(52))));
                        }
                        if (numColumnas >= 54) {
                            parametro.setC54(cn.rs1.getString(String.valueOf(nomColumnas.get(53))));
                        }
                        if (numColumnas >= 55) {
                            parametro.setC55(cn.rs1.getString(String.valueOf(nomColumnas.get(54))));
                        }
                        if (numColumnas >= 56) {
                            parametro.setC56(cn.rs1.getString(String.valueOf(nomColumnas.get(55))));
                        }
                        if (numColumnas >= 57) {
                            parametro.setC57(cn.rs1.getString(String.valueOf(nomColumnas.get(56))));
                        }
                        if (numColumnas >= 58) {
                            parametro.setC58(cn.rs1.getString(String.valueOf(nomColumnas.get(57))));
                        }
                        if (numColumnas >= 59) {
                            parametro.setC59(cn.rs1.getString(String.valueOf(nomColumnas.get(58))));
                        }
                        if (numColumnas >= 60) {
                            parametro.setC60(cn.rs1.getString(String.valueOf(nomColumnas.get(59))));
                        }
                        if (numColumnas >= 61) {
                            parametro.setC61(cn.rs1.getString(String.valueOf(nomColumnas.get(60))));
                        }
                        if (numColumnas >= 62) {
                            parametro.setC62(cn.rs1.getString(String.valueOf(nomColumnas.get(61))));
                        }
                        if (numColumnas >= 63) {
                            parametro.setC63(cn.rs1.getString(String.valueOf(nomColumnas.get(62))));
                        }
                        if (numColumnas >= 64) {
                            parametro.setC64(cn.rs1.getString(String.valueOf(nomColumnas.get(63))));
                        }
                        if (numColumnas >= 65) {
                            parametro.setC65(cn.rs1.getString(String.valueOf(nomColumnas.get(64))));
                        }
                        if (numColumnas >= 66) {
                            parametro.setC66(cn.rs1.getString(String.valueOf(nomColumnas.get(65))));
                        }
                        if (numColumnas >= 67) {
                            parametro.setC67(cn.rs1.getString(String.valueOf(nomColumnas.get(66))));
                        }
                        if (numColumnas >= 68) {
                            parametro.setC68(cn.rs1.getString(String.valueOf(nomColumnas.get(67))));
                        }
                        if (numColumnas >= 69) {
                            parametro.setC69(cn.rs1.getString(String.valueOf(nomColumnas.get(68))));
                        }
                        if (numColumnas >= 70) {
                            parametro.setC70(cn.rs1.getString(String.valueOf(nomColumnas.get(69))));
                        }
                        if (numColumnas >= 71) {
                            parametro.setC71(cn.rs1.getString(String.valueOf(nomColumnas.get(70))));
                        }
                        if (numColumnas >= 72) {
                            parametro.setC72(cn.rs1.getString(String.valueOf(nomColumnas.get(71))));
                        }
                        if (numColumnas >= 73) {
                            parametro.setC73(cn.rs1.getString(String.valueOf(nomColumnas.get(72))));
                        }
                        if (numColumnas >= 74) {
                            parametro.setC74(cn.rs1.getString(String.valueOf(nomColumnas.get(73))));
                        }
                        if (numColumnas >= 75) {
                            parametro.setC75(cn.rs1.getString(String.valueOf(nomColumnas.get(74))));
                        }
                        if (numColumnas >= 76) {
                            parametro.setC76(cn.rs1.getString(String.valueOf(nomColumnas.get(75))));
                        }
                        if (numColumnas >= 77) {
                            parametro.setC77(cn.rs1.getString(String.valueOf(nomColumnas.get(76))));
                        }
                        if (numColumnas >= 78) {
                            parametro.setC78(cn.rs1.getString(String.valueOf(nomColumnas.get(77))));
                        }
                        if (numColumnas >= 79) {
                            parametro.setC79(cn.rs1.getString(String.valueOf(nomColumnas.get(78))));
                        }
                        if (numColumnas >= 80) {
                            parametro.setC80(cn.rs1.getString(String.valueOf(nomColumnas.get(79))));
                        }
                        if (numColumnas >= 81) {
                            parametro.setC81(cn.rs1.getString(String.valueOf(nomColumnas.get(80))));
                        }
                        if (numColumnas >= 82) {
                            parametro.setC82(cn.rs1.getString(String.valueOf(nomColumnas.get(81))));
                        }
                        if (numColumnas >= 83) {
                            parametro.setC83(cn.rs1.getString(String.valueOf(nomColumnas.get(82))));
                        }
                        if (numColumnas >= 84) {
                            parametro.setC84(cn.rs1.getString(String.valueOf(nomColumnas.get(83))));
                        }
                        if (numColumnas >= 85) {
                            parametro.setC85(cn.rs1.getString(String.valueOf(nomColumnas.get(84))));
                        }
                        if (numColumnas >= 86) {
                            parametro.setC86(cn.rs1.getString(String.valueOf(nomColumnas.get(85))));
                        }
                        if (numColumnas >= 87) {
                            parametro.setC87(cn.rs1.getString(String.valueOf(nomColumnas.get(86))));
                        }
                        if (numColumnas >= 88) {
                            parametro.setC88(cn.rs1.getString(String.valueOf(nomColumnas.get(87))));
                        }
                        if (numColumnas >= 89) {
                            parametro.setC89(cn.rs1.getString(String.valueOf(nomColumnas.get(88))));
                        }
                        if (numColumnas >= 90) {
                            parametro.setC90(cn.rs1.getString(String.valueOf(nomColumnas.get(89))));
                        }
                        if (numColumnas >= 91) {
                            parametro.setC91(cn.rs1.getString(String.valueOf(nomColumnas.get(90))));
                        }
                        if (numColumnas >= 92) {
                            parametro.setC92(cn.rs1.getString(String.valueOf(nomColumnas.get(91))));
                        }
                        if (numColumnas >= 93) {
                            parametro.setC93(cn.rs1.getString(String.valueOf(nomColumnas.get(92))));
                        }
                        if (numColumnas >= 94) {
                            parametro.setC94(cn.rs1.getString(String.valueOf(nomColumnas.get(93))));
                        }
                        if (numColumnas >= 95) {
                            parametro.setC95(cn.rs1.getString(String.valueOf(nomColumnas.get(94))));
                        }
                        if (numColumnas >= 96) {
                            parametro.setC96(cn.rs1.getString(String.valueOf(nomColumnas.get(95))));
                        }
                        if (numColumnas >= 97) {
                            parametro.setC97(cn.rs1.getString(String.valueOf(nomColumnas.get(96))));
                        }
                        if (numColumnas >= 98) {
                            parametro.setC98(cn.rs1.getString(String.valueOf(nomColumnas.get(97))));
                        }
                        if (numColumnas >= 99) {
                            parametro.setC99(cn.rs1.getString(String.valueOf(nomColumnas.get(98))));
                        }
                        if (numColumnas >= 100) {
                            parametro.setC100(cn.rs1.getString(String.valueOf(nomColumnas.get(99))));
                        }
                        if (numColumnas >= 101) {
                            parametro.setC101(cn.rs1.getString(String.valueOf(nomColumnas.get(100))));
                        }
                        if (numColumnas >= 102) {
                            parametro.setC102(cn.rs1.getString(String.valueOf(nomColumnas.get(101))));
                        }
                        if (numColumnas >= 103) {
                            parametro.setC103(cn.rs1.getString(String.valueOf(nomColumnas.get(102))));
                        }
                        if (numColumnas >= 104) {
                            parametro.setC104(cn.rs1.getString(String.valueOf(nomColumnas.get(103))));
                        }
                        if (numColumnas >= 105) {
                            parametro.setC105(cn.rs1.getString(String.valueOf(nomColumnas.get(104))));
                        }
                        if (numColumnas >= 106) {
                            parametro.setC106(cn.rs1.getString(String.valueOf(nomColumnas.get(105))));
                        }
                        if (numColumnas >= 107) {
                            parametro.setC107(cn.rs1.getString(String.valueOf(nomColumnas.get(106))));
                        }
                        if (numColumnas >= 108) {
                            parametro.setC108(cn.rs1.getString(String.valueOf(nomColumnas.get(107))));
                        }
                        if (numColumnas >= 109) {
                            parametro.setC109(cn.rs1.getString(String.valueOf(nomColumnas.get(108))));
                        }
                        if (numColumnas >= 110) {
                            parametro.setC110(cn.rs1.getString(String.valueOf(nomColumnas.get(109))));
                        }
                        if (numColumnas >= 111) {
                            parametro.setC111(cn.rs1.getString(String.valueOf(nomColumnas.get(110))));
                        }
                        if (numColumnas >= 112) {
                            parametro.setC112(cn.rs1.getString(String.valueOf(nomColumnas.get(111))));
                        }
                        if (numColumnas >= 113) {
                            parametro.setC113(cn.rs1.getString(String.valueOf(nomColumnas.get(112))));
                        }
                        if (numColumnas >= 114) {
                            parametro.setC114(cn.rs1.getString(String.valueOf(nomColumnas.get(113))));
                        }
                        if (numColumnas >= 115) {
                            parametro.setC115(cn.rs1.getString(String.valueOf(nomColumnas.get(114))));
                        }
                        if (numColumnas >= 116) {
                            parametro.setC116(cn.rs1.getString(String.valueOf(nomColumnas.get(115))));
                        }
                        if (numColumnas >= 117) {
                            parametro.setC117(cn.rs1.getString(String.valueOf(nomColumnas.get(116))));
                        }
                        if (numColumnas >= 118) {
                            parametro.setC118(cn.rs1.getString(String.valueOf(nomColumnas.get(117))));
                        }
                        if (numColumnas >= 119) {
                            parametro.setC119(cn.rs1.getString(String.valueOf(nomColumnas.get(118))));
                        }
                        if (numColumnas >= 120) {
                            parametro.setC120(cn.rs1.getString(String.valueOf(nomColumnas.get(119))));
                        }
                        if (numColumnas >= 121) {
                            parametro.setC121(cn.rs1.getString(String.valueOf(nomColumnas.get(120))));
                        }
                        if (numColumnas >= 122) {
                            parametro.setC122(cn.rs1.getString(String.valueOf(nomColumnas.get(121))));
                        }
                        if (numColumnas >= 123) {
                            parametro.setC123(cn.rs1.getString(String.valueOf(nomColumnas.get(122))));
                        }
                        if (numColumnas >= 124) {
                            parametro.setC124(cn.rs1.getString(String.valueOf(nomColumnas.get(123))));
                        }
                        if (numColumnas >= 125) {
                            parametro.setC125(cn.rs1.getString(String.valueOf(nomColumnas.get(124))));
                        }
                        if (numColumnas >= 126) {
                            parametro.setC126(cn.rs1.getString(String.valueOf(nomColumnas.get(125))));
                        }
                        if (numColumnas >= 127) {
                            parametro.setC127(cn.rs1.getString(String.valueOf(nomColumnas.get(126))));
                        }
                        if (numColumnas >= 128) {
                            parametro.setC128(cn.rs1.getString(String.valueOf(nomColumnas.get(127))));
                        }
                        if (numColumnas >= 129) {
                            parametro.setC129(cn.rs1.getString(String.valueOf(nomColumnas.get(128))));
                        }
                        if (numColumnas >= 130) {
                            parametro.setC130(cn.rs1.getString(String.valueOf(nomColumnas.get(129))));
                        }
                        if (numColumnas >= 131) {
                            parametro.setC131(cn.rs1.getString(String.valueOf(nomColumnas.get(130))));
                        }
                        if (numColumnas >= 132) {
                            parametro.setC132(cn.rs1.getString(String.valueOf(nomColumnas.get(131))));
                        }
                        if (numColumnas >= 133) {
                            parametro.setC133(cn.rs1.getString(String.valueOf(nomColumnas.get(132))));
                        }
                        if (numColumnas >= 134) {
                            parametro.setC134(cn.rs1.getString(String.valueOf(nomColumnas.get(133))));
                        }
                        if (numColumnas >= 135) {
                            parametro.setC135(cn.rs1.getString(String.valueOf(nomColumnas.get(134))));
                        }
                        if (numColumnas >= 136) {
                            parametro.setC136(cn.rs1.getString(String.valueOf(nomColumnas.get(135))));
                        }
                        if (numColumnas >= 137) {
                            parametro.setC137(cn.rs1.getString(String.valueOf(nomColumnas.get(136))));
                        }
                        if (numColumnas >= 138) {
                            parametro.setC138(cn.rs1.getString(String.valueOf(nomColumnas.get(137))));
                        }
                        if (numColumnas >= 139) {
                            parametro.setC139(cn.rs1.getString(String.valueOf(nomColumnas.get(138))));
                        }
                        if (numColumnas >= 140) {
                            parametro.setC140(cn.rs1.getString(String.valueOf(nomColumnas.get(139))));
                        }
                        if (numColumnas >= 141) {
                            parametro.setC141(cn.rs1.getString(String.valueOf(nomColumnas.get(140))));
                        }
                        if (numColumnas >= 142) {
                            parametro.setC142(cn.rs1.getString(String.valueOf(nomColumnas.get(141))));
                        }
                        if (numColumnas >= 143) {
                            parametro.setC143(cn.rs1.getString(String.valueOf(nomColumnas.get(142))));
                        }
                        if (numColumnas >= 144) {
                            parametro.setC144(cn.rs1.getString(String.valueOf(nomColumnas.get(143))));
                        }
                        if (numColumnas >= 145) {
                            parametro.setC145(cn.rs1.getString(String.valueOf(nomColumnas.get(144))));
                        }
                        if (numColumnas >= 146) {
                            parametro.setC146(cn.rs1.getString(String.valueOf(nomColumnas.get(145))));
                        }
                        if (numColumnas >= 147) {
                            parametro.setC147(cn.rs1.getString(String.valueOf(nomColumnas.get(146))));
                        }
                        if (numColumnas >= 148) {
                            parametro.setC148(cn.rs1.getString(String.valueOf(nomColumnas.get(147))));
                        }
                        if (numColumnas >= 149) {
                            parametro.setC149(cn.rs1.getString(String.valueOf(nomColumnas.get(148))));
                        }
                        if (numColumnas >= 150) {
                            parametro.setC150(cn.rs1.getString(String.valueOf(nomColumnas.get(149))));
                        }
                        if (numColumnas >= 151) {
                            parametro.setC151(cn.rs1.getString(String.valueOf(nomColumnas.get(150))));
                        }
                        if (numColumnas >= 152) {
                            parametro.setC152(cn.rs1.getString(String.valueOf(nomColumnas.get(151))));
                        }
                        if (numColumnas >= 153) {
                            parametro.setC153(cn.rs1.getString(String.valueOf(nomColumnas.get(152))));
                        }
                        if (numColumnas >= 154) {
                            parametro.setC154(cn.rs1.getString(String.valueOf(nomColumnas.get(153))));
                        }
                        if (numColumnas >= 155) {
                            parametro.setC155(cn.rs1.getString(String.valueOf(nomColumnas.get(154))));
                        }
                        if (numColumnas >= 156) {
                            parametro.setC156(cn.rs1.getString(String.valueOf(nomColumnas.get(155))));
                        }
                        if (numColumnas >= 157) {
                            parametro.setC157(cn.rs1.getString(String.valueOf(nomColumnas.get(156))));
                        }
                        if (numColumnas >= 158) {
                            parametro.setC158(cn.rs1.getString(String.valueOf(nomColumnas.get(157))));
                        }
                        if (numColumnas >= 159) {
                            parametro.setC159(cn.rs1.getString(String.valueOf(nomColumnas.get(158))));
                        }
                        if (numColumnas >= 160) {
                            parametro.setC160(cn.rs1.getString(String.valueOf(nomColumnas.get(159))));
                        }
                        if (numColumnas >= 161) {
                            parametro.setC161(cn.rs1.getString(String.valueOf(nomColumnas.get(160))));
                        }
                        if (numColumnas >= 162) {
                            parametro.setC162(cn.rs1.getString(String.valueOf(nomColumnas.get(161))));
                        }
                        if (numColumnas >= 163) {
                            parametro.setC163(cn.rs1.getString(String.valueOf(nomColumnas.get(162))));
                        }
                        if (numColumnas >= 164) {
                            parametro.setC164(cn.rs1.getString(String.valueOf(nomColumnas.get(163))));
                        }
                        if (numColumnas >= 165) {
                            parametro.setC165(cn.rs1.getString(String.valueOf(nomColumnas.get(164))));
                        }
                        if (numColumnas >= 166) {
                            parametro.setC166(cn.rs1.getString(String.valueOf(nomColumnas.get(165))));
                        }
                        if (numColumnas >= 167) {
                            parametro.setC167(cn.rs1.getString(String.valueOf(nomColumnas.get(166))));
                        }
                        if (numColumnas >= 168) {
                            parametro.setC168(cn.rs1.getString(String.valueOf(nomColumnas.get(167))));
                        }
                        if (numColumnas >= 169) {
                            parametro.setC169(cn.rs1.getString(String.valueOf(nomColumnas.get(168))));
                        }
                        if (numColumnas >= 170) {
                            parametro.setC170(cn.rs1.getString(String.valueOf(nomColumnas.get(169))));
                        }
                        if (numColumnas >= 171) {
                            parametro.setC171(cn.rs1.getString(String.valueOf(nomColumnas.get(170))));
                        }
                        if (numColumnas >= 172) {
                            parametro.setC172(cn.rs1.getString(String.valueOf(nomColumnas.get(171))));
                        }
                        if (numColumnas >= 173) {
                            parametro.setC173(cn.rs1.getString(String.valueOf(nomColumnas.get(172))));
                        }
                        if (numColumnas >= 174) {
                            parametro.setC174(cn.rs1.getString(String.valueOf(nomColumnas.get(173))));
                        }
                        if (numColumnas >= 175) {
                            parametro.setC175(cn.rs1.getString(String.valueOf(nomColumnas.get(174))));
                        }
                        if (numColumnas >= 176) {
                            parametro.setC176(cn.rs1.getString(String.valueOf(nomColumnas.get(175))));
                        }
                        if (numColumnas >= 177) {
                            parametro.setC177(cn.rs1.getString(String.valueOf(nomColumnas.get(176))));
                        }
                        if (numColumnas >= 178) {
                            parametro.setC178(cn.rs1.getString(String.valueOf(nomColumnas.get(177))));
                        }
                        if (numColumnas >= 179) {
                            parametro.setC179(cn.rs1.getString(String.valueOf(nomColumnas.get(178))));
                        }
                        if (numColumnas >= 180) {
                            parametro.setC180(cn.rs1.getString(String.valueOf(nomColumnas.get(179))));
                        }
                        if (numColumnas >= 181) {
                            parametro.setC181(cn.rs1.getString(String.valueOf(nomColumnas.get(180))));
                        }
                        if (numColumnas >= 182) {
                            parametro.setC182(cn.rs1.getString(String.valueOf(nomColumnas.get(181))));
                        }
                        if (numColumnas >= 183) {
                            parametro.setC183(cn.rs1.getString(String.valueOf(nomColumnas.get(182))));
                        }
                        if (numColumnas >= 184) {
                            parametro.setC184(cn.rs1.getString(String.valueOf(nomColumnas.get(183))));
                        }
                        if (numColumnas >= 185) {
                            parametro.setC185(cn.rs1.getString(String.valueOf(nomColumnas.get(184))));
                        }
                        if (numColumnas >= 186) {
                            parametro.setC186(cn.rs1.getString(String.valueOf(nomColumnas.get(185))));
                        }
                        if (numColumnas >= 187) {
                            parametro.setC187(cn.rs1.getString(String.valueOf(nomColumnas.get(186))));
                        }
                        if (numColumnas >= 188) {
                            parametro.setC188(cn.rs1.getString(String.valueOf(nomColumnas.get(187))));
                        }
                        if (numColumnas >= 189) {
                            parametro.setC189(cn.rs1.getString(String.valueOf(nomColumnas.get(188))));
                        }
                        if (numColumnas >= 190) {
                            parametro.setC190(cn.rs1.getString(String.valueOf(nomColumnas.get(189))));
                        }
                        if (numColumnas >= 191) {
                            parametro.setC191(cn.rs1.getString(String.valueOf(nomColumnas.get(190))));
                        }
                        if (numColumnas >= 192) {
                            parametro.setC192(cn.rs1.getString(String.valueOf(nomColumnas.get(191))));
                        }
                        if (numColumnas >= 193) {
                            parametro.setC193(cn.rs1.getString(String.valueOf(nomColumnas.get(192))));
                        }
                        if (numColumnas >= 194) {
                            parametro.setC194(cn.rs1.getString(String.valueOf(nomColumnas.get(193))));
                        }
                        if (numColumnas >= 195) {
                            parametro.setC195(cn.rs1.getString(String.valueOf(nomColumnas.get(194))));
                        }
                        if (numColumnas >= 196) {
                            parametro.setC196(cn.rs1.getString(String.valueOf(nomColumnas.get(195))));
                        }
                        if (numColumnas >= 197) {
                            parametro.setC197(cn.rs1.getString(String.valueOf(nomColumnas.get(196))));
                        }
                        if (numColumnas >= 198) {
                            parametro.setC198(cn.rs1.getString(String.valueOf(nomColumnas.get(197))));
                        }
                        if (numColumnas >= 199) {
                            parametro.setC199(cn.rs1.getString(String.valueOf(nomColumnas.get(198))));
                        }
                        if (numColumnas >= 200) {
                            parametro.setC200(cn.rs1.getString(String.valueOf(nomColumnas.get(199))));
                        }
                        if (numColumnas >= 201) {
                            parametro.setC201(cn.rs1.getString(String.valueOf(nomColumnas.get(200))));
                        }
                        if (numColumnas >= 202) {
                            parametro.setC202(cn.rs1.getString(String.valueOf(nomColumnas.get(201))));
                        }
                        if (numColumnas >= 203) {
                            parametro.setC203(cn.rs1.getString(String.valueOf(nomColumnas.get(202))));
                        }
                        if (numColumnas >= 204) {
                            parametro.setC204(cn.rs1.getString(String.valueOf(nomColumnas.get(203))));
                        }
                        if (numColumnas >= 205) {
                            parametro.setC205(cn.rs1.getString(String.valueOf(nomColumnas.get(204))));
                        }
                        if (numColumnas >= 206) {
                            parametro.setC206(cn.rs1.getString(String.valueOf(nomColumnas.get(205))));
                        }
                        if (numColumnas >= 207) {
                            parametro.setC207(cn.rs1.getString(String.valueOf(nomColumnas.get(206))));
                        }
                        if (numColumnas >= 208) {
                            parametro.setC208(cn.rs1.getString(String.valueOf(nomColumnas.get(207))));
                        }
                        if (numColumnas >= 209) {
                            parametro.setC209(cn.rs1.getString(String.valueOf(nomColumnas.get(208))));
                        }
                        if (numColumnas >= 210) {
                            parametro.setC210(cn.rs1.getString(String.valueOf(nomColumnas.get(209))));
                        }
                        if (numColumnas >= 211) {
                            parametro.setC211(cn.rs1.getString(String.valueOf(nomColumnas.get(210))));
                        }
                        if (numColumnas >= 212) {
                            parametro.setC212(cn.rs1.getString(String.valueOf(nomColumnas.get(211))));
                        }
                        if (numColumnas >= 213) {
                            parametro.setC213(cn.rs1.getString(String.valueOf(nomColumnas.get(212))));
                        }
                        if (numColumnas >= 214) {
                            parametro.setC214(cn.rs1.getString(String.valueOf(nomColumnas.get(213))));
                        }
                        if (numColumnas >= 215) {
                            parametro.setC215(cn.rs1.getString(String.valueOf(nomColumnas.get(214))));
                        }
                        if (numColumnas >= 216) {
                            parametro.setC216(cn.rs1.getString(String.valueOf(nomColumnas.get(215))));
                        }
                        if (numColumnas >= 217) {
                            parametro.setC217(cn.rs1.getString(String.valueOf(nomColumnas.get(216))));
                        }
                        if (numColumnas >= 218) {
                            parametro.setC218(cn.rs1.getString(String.valueOf(nomColumnas.get(217))));
                        }
                        if (numColumnas >= 219) {
                            parametro.setC219(cn.rs1.getString(String.valueOf(nomColumnas.get(218))));
                        }
                        if (numColumnas >= 220) {
                            parametro.setC220(cn.rs1.getString(String.valueOf(nomColumnas.get(219))));
                        }
                        if (numColumnas >= 221) {
                            parametro.setC221(cn.rs1.getString(String.valueOf(nomColumnas.get(220))));
                        }
                        if (numColumnas >= 222) {
                            parametro.setC222(cn.rs1.getString(String.valueOf(nomColumnas.get(221))));
                        }
                        if (numColumnas >= 223) {
                            parametro.setC223(cn.rs1.getString(String.valueOf(nomColumnas.get(222))));
                        }
                        if (numColumnas >= 224) {
                            parametro.setC224(cn.rs1.getString(String.valueOf(nomColumnas.get(223))));
                        }
                        if (numColumnas >= 225) {
                            parametro.setC225(cn.rs1.getString(String.valueOf(nomColumnas.get(224))));
                        }
                        if (numColumnas >= 226) {
                            parametro.setC226(cn.rs1.getString(String.valueOf(nomColumnas.get(225))));
                        }
                        if (numColumnas >= 227) {
                            parametro.setC227(cn.rs1.getString(String.valueOf(nomColumnas.get(226))));
                        }
                        if (numColumnas >= 228) {
                            parametro.setC228(cn.rs1.getString(String.valueOf(nomColumnas.get(227))));
                        }
                        if (numColumnas >= 229) {
                            parametro.setC229(cn.rs1.getString(String.valueOf(nomColumnas.get(228))));
                        }
                        if (numColumnas >= 230) {
                            parametro.setC230(cn.rs1.getString(String.valueOf(nomColumnas.get(229))));
                        }
                        if (numColumnas >= 231) {
                            parametro.setC231(cn.rs1.getString(String.valueOf(nomColumnas.get(230))));
                        }
                        if (numColumnas >= 232) {
                            parametro.setC232(cn.rs1.getString(String.valueOf(nomColumnas.get(231))));
                        }
                        if (numColumnas >= 233) {
                            parametro.setC233(cn.rs1.getString(String.valueOf(nomColumnas.get(232))));
                        }
                        if (numColumnas >= 234) {
                            parametro.setC234(cn.rs1.getString(String.valueOf(nomColumnas.get(233))));
                        }
                        if (numColumnas >= 235) {
                            parametro.setC235(cn.rs1.getString(String.valueOf(nomColumnas.get(234))));
                        }
                        if (numColumnas >= 236) {
                            parametro.setC236(cn.rs1.getString(String.valueOf(nomColumnas.get(235))));
                        }
                        if (numColumnas >= 237) {
                            parametro.setC237(cn.rs1.getString(String.valueOf(nomColumnas.get(236))));
                        }
                        if (numColumnas >= 238) {
                            parametro.setC238(cn.rs1.getString(String.valueOf(nomColumnas.get(237))));
                        }
                        if (numColumnas >= 239) {
                            parametro.setC239(cn.rs1.getString(String.valueOf(nomColumnas.get(238))));
                        }
                        if (numColumnas >= 240) {
                            parametro.setC240(cn.rs1.getString(String.valueOf(nomColumnas.get(239))));
                        }
                        if (numColumnas >= 241) {
                            parametro.setC241(cn.rs1.getString(String.valueOf(nomColumnas.get(240))));
                        }
                        if (numColumnas >= 242) {
                            parametro.setC242(cn.rs1.getString(String.valueOf(nomColumnas.get(241))));
                        }
                        if (numColumnas >= 243) {
                            parametro.setC243(cn.rs1.getString(String.valueOf(nomColumnas.get(242))));
                        }
                        if (numColumnas >= 244) {
                            parametro.setC244(cn.rs1.getString(String.valueOf(nomColumnas.get(243))));
                        }
                        if (numColumnas >= 245) {
                            parametro.setC245(cn.rs1.getString(String.valueOf(nomColumnas.get(244))));
                        }
                        if (numColumnas >= 246) {
                            parametro.setC246(cn.rs1.getString(String.valueOf(nomColumnas.get(245))));
                        }
                        if (numColumnas >= 247) {
                            parametro.setC247(cn.rs1.getString(String.valueOf(nomColumnas.get(246))));
                        }
                        if (numColumnas >= 248) {
                            parametro.setC248(cn.rs1.getString(String.valueOf(nomColumnas.get(247))));
                        }
                        if (numColumnas >= 249) {
                            parametro.setC249(cn.rs1.getString(String.valueOf(nomColumnas.get(248))));
                        }
                        if (numColumnas >= 250) {
                            parametro.setC250(cn.rs1.getString(String.valueOf(nomColumnas.get(249))));
                        }
                        if (numColumnas >= 251) {
                            parametro.setC251(cn.rs1.getString(String.valueOf(nomColumnas.get(250))));
                        }
                        if (numColumnas >= 252) {
                            parametro.setC252(cn.rs1.getString(String.valueOf(nomColumnas.get(251))));
                        }
                        if (numColumnas >= 253) {
                            parametro.setC253(cn.rs1.getString(String.valueOf(nomColumnas.get(252))));
                        }
                        if (numColumnas >= 254) {
                            parametro.setC254(cn.rs1.getString(String.valueOf(nomColumnas.get(253))));
                        }
                        if (numColumnas >= 255) {
                            parametro.setC255(cn.rs1.getString(String.valueOf(nomColumnas.get(254))));
                        }
                        if (numColumnas >= 256) {
                            parametro.setC256(cn.rs1.getString(String.valueOf(nomColumnas.get(255))));
                        }
                        if (numColumnas >= 257) {
                            parametro.setC257(cn.rs1.getString(String.valueOf(nomColumnas.get(256))));
                        }
                        if (numColumnas >= 258) {
                            parametro.setC258(cn.rs1.getString(String.valueOf(nomColumnas.get(257))));
                        }
                        if (numColumnas >= 259) {
                            parametro.setC259(cn.rs1.getString(String.valueOf(nomColumnas.get(258))));
                        }
                        if (numColumnas >= 260) {
                            parametro.setC260(cn.rs1.getString(String.valueOf(nomColumnas.get(259))));
                        }
                        if (numColumnas >= 261) {
                            parametro.setC261(cn.rs1.getString(String.valueOf(nomColumnas.get(260))));
                        }
                        if (numColumnas >= 262) {
                            parametro.setC262(cn.rs1.getString(String.valueOf(nomColumnas.get(261))));
                        }
                        if (numColumnas >= 263) {
                            parametro.setC263(cn.rs1.getString(String.valueOf(nomColumnas.get(262))));
                        }
                        if (numColumnas >= 264) {
                            parametro.setC264(cn.rs1.getString(String.valueOf(nomColumnas.get(263))));
                        }
                        if (numColumnas >= 265) {
                            parametro.setC265(cn.rs1.getString(String.valueOf(nomColumnas.get(264))));
                        }
                        if (numColumnas >= 266) {
                            parametro.setC266(cn.rs1.getString(String.valueOf(nomColumnas.get(265))));
                        }
                        if (numColumnas >= 267) {
                            parametro.setC267(cn.rs1.getString(String.valueOf(nomColumnas.get(266))));
                        }
                        if (numColumnas >= 268) {
                            parametro.setC268(cn.rs1.getString(String.valueOf(nomColumnas.get(267))));
                        }
                        if (numColumnas >= 269) {
                            parametro.setC269(cn.rs1.getString(String.valueOf(nomColumnas.get(268))));
                        }
                        if (numColumnas >= 270) {
                            parametro.setC270(cn.rs1.getString(String.valueOf(nomColumnas.get(269))));
                        }
                        if (numColumnas >= 271) {
                            parametro.setC271(cn.rs1.getString(String.valueOf(nomColumnas.get(270))));
                        }
                        if (numColumnas >= 272) {
                            parametro.setC272(cn.rs1.getString(String.valueOf(nomColumnas.get(271))));
                        }
                        if (numColumnas >= 273) {
                            parametro.setC273(cn.rs1.getString(String.valueOf(nomColumnas.get(272))));
                        }
                        if (numColumnas >= 274) {
                            parametro.setC274(cn.rs1.getString(String.valueOf(nomColumnas.get(273))));
                        }
                        if (numColumnas >= 275) {
                            parametro.setC275(cn.rs1.getString(String.valueOf(nomColumnas.get(274))));
                        }
                        if (numColumnas >= 276) {
                            parametro.setC276(cn.rs1.getString(String.valueOf(nomColumnas.get(275))));
                        }
                        if (numColumnas >= 277) {
                            parametro.setC277(cn.rs1.getString(String.valueOf(nomColumnas.get(276))));
                        }
                        if (numColumnas >= 278) {
                            parametro.setC278(cn.rs1.getString(String.valueOf(nomColumnas.get(277))));
                        }
                        if (numColumnas >= 279) {
                            parametro.setC279(cn.rs1.getString(String.valueOf(nomColumnas.get(278))));
                        }
                        if (numColumnas >= 280) {
                            parametro.setC280(cn.rs1.getString(String.valueOf(nomColumnas.get(279))));
                        }
                        if (numColumnas >= 281) {
                            parametro.setC281(cn.rs1.getString(String.valueOf(nomColumnas.get(280))));
                        }
                        if (numColumnas >= 282) {
                            parametro.setC282(cn.rs1.getString(String.valueOf(nomColumnas.get(281))));
                        }
                        if (numColumnas >= 283) {
                            parametro.setC283(cn.rs1.getString(String.valueOf(nomColumnas.get(282))));
                        }
                        if (numColumnas >= 284) {
                            parametro.setC284(cn.rs1.getString(String.valueOf(nomColumnas.get(283))));
                        }
                        if (numColumnas >= 285) {
                            parametro.setC285(cn.rs1.getString(String.valueOf(nomColumnas.get(284))));
                        }
                        if (numColumnas >= 286) {
                            parametro.setC286(cn.rs1.getString(String.valueOf(nomColumnas.get(285))));
                        }
                        if (numColumnas >= 287) {
                            parametro.setC287(cn.rs1.getString(String.valueOf(nomColumnas.get(286))));
                        }
                        if (numColumnas >= 288) {
                            parametro.setC288(cn.rs1.getString(String.valueOf(nomColumnas.get(287))));
                        }
                        if (numColumnas >= 289) {
                            parametro.setC289(cn.rs1.getString(String.valueOf(nomColumnas.get(288))));
                        }
                        if (numColumnas >= 290) {
                            parametro.setC290(cn.rs1.getString(String.valueOf(nomColumnas.get(289))));
                        }
                        if (numColumnas >= 291) {
                            parametro.setC291(cn.rs1.getString(String.valueOf(nomColumnas.get(290))));
                        }
                        if (numColumnas >= 292) {
                            parametro.setC292(cn.rs1.getString(String.valueOf(nomColumnas.get(291))));
                        }
                        if (numColumnas >= 293) {
                            parametro.setC293(cn.rs1.getString(String.valueOf(nomColumnas.get(292))));
                        }
                        if (numColumnas >= 294) {
                            parametro.setC294(cn.rs1.getString(String.valueOf(nomColumnas.get(293))));
                        }
                        if (numColumnas >= 295) {
                            parametro.setC295(cn.rs1.getString(String.valueOf(nomColumnas.get(294))));
                        }
                        if (numColumnas >= 296) {
                            parametro.setC296(cn.rs1.getString(String.valueOf(nomColumnas.get(295))));
                        }
                        if (numColumnas >= 297) {
                            parametro.setC297(cn.rs1.getString(String.valueOf(nomColumnas.get(296))));
                        }
                        if (numColumnas >= 298) {
                            parametro.setC298(cn.rs1.getString(String.valueOf(nomColumnas.get(297))));
                        }
                        if (numColumnas >= 299) {
                            parametro.setC299(cn.rs1.getString(String.valueOf(nomColumnas.get(298))));
                        }
                        if (numColumnas >= 300) {
                            parametro.setC300(cn.rs1.getString(String.valueOf(nomColumnas.get(299))));
                        }
                        if (numColumnas >= 301) {
                            parametro.setC301(cn.rs1.getString(String.valueOf(nomColumnas.get(300))));
                        }
                        if (numColumnas >= 302) {
                            parametro.setC302(cn.rs1.getString(String.valueOf(nomColumnas.get(301))));
                        }
                        if (numColumnas >= 303) {
                            parametro.setC303(cn.rs1.getString(String.valueOf(nomColumnas.get(302))));
                        }
                        if (numColumnas >= 304) {
                            parametro.setC304(cn.rs1.getString(String.valueOf(nomColumnas.get(303))));
                        }
                        if (numColumnas >= 305) {
                            parametro.setC305(cn.rs1.getString(String.valueOf(nomColumnas.get(304))));
                        }
                        if (numColumnas >= 306) {
                            parametro.setC306(cn.rs1.getString(String.valueOf(nomColumnas.get(305))));
                        }
                        if (numColumnas >= 307) {
                            parametro.setC307(cn.rs1.getString(String.valueOf(nomColumnas.get(306))));
                        }
                        if (numColumnas >= 308) {
                            parametro.setC308(cn.rs1.getString(String.valueOf(nomColumnas.get(307))));
                        }
                        if (numColumnas >= 309) {
                            parametro.setC309(cn.rs1.getString(String.valueOf(nomColumnas.get(308))));
                        }
                        if (numColumnas >= 310) {
                            parametro.setC310(cn.rs1.getString(String.valueOf(nomColumnas.get(309))));
                        }
                        if (numColumnas >= 311) {
                            parametro.setC311(cn.rs1.getString(String.valueOf(nomColumnas.get(310))));
                        }
                        if (numColumnas >= 312) {
                            parametro.setC312(cn.rs1.getString(String.valueOf(nomColumnas.get(311))));
                        }
                        if (numColumnas >= 313) {
                            parametro.setC313(cn.rs1.getString(String.valueOf(nomColumnas.get(312))));
                        }
                        if (numColumnas >= 314) {
                            parametro.setC314(cn.rs1.getString(String.valueOf(nomColumnas.get(313))));
                        }
                        if (numColumnas >= 315) {
                            parametro.setC315(cn.rs1.getString(String.valueOf(nomColumnas.get(314))));
                        }
                        if (numColumnas >= 316) {
                            parametro.setC316(cn.rs1.getString(String.valueOf(nomColumnas.get(315))));
                        }
                        if (numColumnas >= 317) {
                            parametro.setC317(cn.rs1.getString(String.valueOf(nomColumnas.get(316))));
                        }
                        if (numColumnas >= 318) {
                            parametro.setC318(cn.rs1.getString(String.valueOf(nomColumnas.get(317))));
                        }
                        if (numColumnas >= 319) {
                            parametro.setC319(cn.rs1.getString(String.valueOf(nomColumnas.get(318))));
                        }
                        if (numColumnas >= 320) {
                            parametro.setC320(cn.rs1.getString(String.valueOf(nomColumnas.get(319))));
                        }
                        if (numColumnas >= 321) {
                            parametro.setC321(cn.rs1.getString(String.valueOf(nomColumnas.get(320))));
                        }
                        if (numColumnas >= 322) {
                            parametro.setC322(cn.rs1.getString(String.valueOf(nomColumnas.get(321))));
                        }
                        if (numColumnas >= 323) {
                            parametro.setC323(cn.rs1.getString(String.valueOf(nomColumnas.get(322))));
                        }
                        if (numColumnas >= 324) {
                            parametro.setC324(cn.rs1.getString(String.valueOf(nomColumnas.get(323))));
                        }
                        if (numColumnas >= 325) {
                            parametro.setC325(cn.rs1.getString(String.valueOf(nomColumnas.get(324))));
                        }
                        if (numColumnas >= 326) {
                            parametro.setC326(cn.rs1.getString(String.valueOf(nomColumnas.get(325))));
                        }
                        if (numColumnas >= 327) {
                            parametro.setC327(cn.rs1.getString(String.valueOf(nomColumnas.get(326))));
                        }
                        if (numColumnas >= 328) {
                            parametro.setC328(cn.rs1.getString(String.valueOf(nomColumnas.get(327))));
                        }
                        if (numColumnas >= 329) {
                            parametro.setC329(cn.rs1.getString(String.valueOf(nomColumnas.get(328))));
                        }
                        if (numColumnas >= 330) {
                            parametro.setC330(cn.rs1.getString(String.valueOf(nomColumnas.get(329))));
                        }
                        if (numColumnas >= 331) {
                            parametro.setC331(cn.rs1.getString(String.valueOf(nomColumnas.get(330))));
                        }
                        if (numColumnas >= 332) {
                            parametro.setC332(cn.rs1.getString(String.valueOf(nomColumnas.get(331))));
                        }
                        if (numColumnas >= 333) {
                            parametro.setC333(cn.rs1.getString(String.valueOf(nomColumnas.get(332))));
                        }
                        if (numColumnas >= 334) {
                            parametro.setC334(cn.rs1.getString(String.valueOf(nomColumnas.get(333))));
                        }
                        if (numColumnas >= 335) {
                            parametro.setC335(cn.rs1.getString(String.valueOf(nomColumnas.get(334))));
                        }
                        if (numColumnas >= 336) {
                            parametro.setC336(cn.rs1.getString(String.valueOf(nomColumnas.get(335))));
                        }
                        if (numColumnas >= 337) {
                            parametro.setC337(cn.rs1.getString(String.valueOf(nomColumnas.get(336))));
                        }
                        if (numColumnas >= 338) {
                            parametro.setC338(cn.rs1.getString(String.valueOf(nomColumnas.get(337))));
                        }
                        if (numColumnas >= 339) {
                            parametro.setC339(cn.rs1.getString(String.valueOf(nomColumnas.get(338))));
                        }
                        if (numColumnas >= 340) {
                            parametro.setC340(cn.rs1.getString(String.valueOf(nomColumnas.get(339))));
                        }
                        if (numColumnas >= 341) {
                            parametro.setC341(cn.rs1.getString(String.valueOf(nomColumnas.get(340))));
                        }
                        if (numColumnas >= 342) {
                            parametro.setC342(cn.rs1.getString(String.valueOf(nomColumnas.get(341))));
                        }
                        if (numColumnas >= 343) {
                            parametro.setC343(cn.rs1.getString(String.valueOf(nomColumnas.get(342))));
                        }
                        if (numColumnas >= 344) {
                            parametro.setC344(cn.rs1.getString(String.valueOf(nomColumnas.get(343))));
                        }
                        if (numColumnas >= 345) {
                            parametro.setC345(cn.rs1.getString(String.valueOf(nomColumnas.get(344))));
                        }
                        if (numColumnas >= 346) {
                            parametro.setC346(cn.rs1.getString(String.valueOf(nomColumnas.get(345))));
                        }
                        if (numColumnas >= 347) {
                            parametro.setC347(cn.rs1.getString(String.valueOf(nomColumnas.get(346))));
                        }
                        if (numColumnas >= 348) {
                            parametro.setC348(cn.rs1.getString(String.valueOf(nomColumnas.get(347))));
                        }
                        if (numColumnas >= 349) {
                            parametro.setC349(cn.rs1.getString(String.valueOf(nomColumnas.get(348))));
                        }
                        if (numColumnas >= 350) {
                            parametro.setC350(cn.rs1.getString(String.valueOf(nomColumnas.get(349))));
                        }

                        resultado.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }
        } catch (SQLException e) {
            System.out.println("Error --> clase DocumentoHC --> function traerContenidoDocumentoSeleccionado --> SQLException --> " + e.getMessage());
            //  e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error --> clase DocumentoHC --> function traerContenidoDocumentoSeleccionado --> Exception --> " + e.getMessage());
            //  e.printStackTrace();
        }
        return resultado;
    }

    public java.lang.String getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(java.lang.String value) {
        idPaciente = value;
    }

    public java.lang.String getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(java.lang.String value) {
        idDocumento = value;
    }

    public java.lang.String getNomDocumento() {
        return nomDocumento;
    }

    public void setNomDocumento(java.lang.String value) {
        nomDocumento = value;
    }

    public java.lang.String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(java.lang.String value) {
        tipoDocumento = value;
    }

    public java.lang.String getTotalDocumento() {
        return totalDocumento;
    }

    public void setTotalDocumento(java.lang.String value) {
        totalDocumento = value;
    }

    public java.lang.String getNomProfesional() {
        return nomProfesional;
    }

    public void setNomProfesional(java.lang.String value) {
        nomProfesional = value;
    }

    public java.lang.String getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(java.lang.String value) {
        fechaDocumento = value;
    }

    public Sgh.Utilidades.Conexion getCn() {
        return cn;
    }

    public void setCn(Sgh.Utilidades.Conexion value) {
        cn = value;
    }

    public java.lang.String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(java.lang.String value) {
        tipoServicio = value;
    }

    public java.lang.String getIdAdmision() {
        return idAdmision;
    }

    public void setIdAdmision(java.lang.String value) {
        idAdmision = value;
    }

    public int getNumColumnas() {
        return numColumnas;
    }

    public void setNumColumnas(int value) {
        numColumnas = value;
    }

    public java.lang.String getIdProfesion() {
        return idProfesion;
    }

    public void setIdProfesion(java.lang.String value) {
        idProfesion = value;
    }

    public java.lang.String getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(java.lang.String value) {
        idEspecialidad = value;
    }

    public java.lang.String getParametros() {
        return parametros;
    }

    public void setParametros(java.lang.String value) {
        parametros = value;
    }

    public java.lang.String getIdQuery() {
        return idQuery;
    }

    public void setIdQuery(java.lang.String value) {
        idQuery = value;
    }

    public java.lang.String getIdIdAuxiliar() {
        return idIdAuxiliar;
    }

    public void setIdIdAuxiliar(java.lang.String value) {
        idIdAuxiliar = value;
    }

    public java.lang.String getVar3() {
        return var3;
    }

    public void setVar3(java.lang.String value) {
        var3 = value;
    }

    public java.lang.String getVar4() {
        return var4;
    }

    public void setVar4(java.lang.String value) {
        var4 = value;
    }

    public java.lang.String getVar5() {
        return var5;
    }

    public void setVar5(java.lang.String value) {
        var5 = value;
    }

    public java.lang.String getVar6() {
        return var6;
    }

    public void setVar6(java.lang.String value) {
        var6 = value;
    }

    public int getVar1() {
        return var1;
    }

    public void setVar1(int value) {
        var1 = value;
    }

    public int getVar2() {
        return var2;
    }

    public void setVar2(int value) {
        var2 = value;
    }

    public int getVar7() {
        return var7;
    }

    public void setVar7(int value) {
        var7 = value;
    }

    public java.lang.String getVar8() {
        return var8;
    }

    public void setVar8(java.lang.String value) {
        var8 = value;
    }

    public java.lang.String getVar9() {
        return var9;
    }

    public void setVar9(java.lang.String value) {
        var9 = value;
    }

    public String getIdEvolucion() {
        return idEvolucion;
    }

    public void setIdEvolucion(String idEvolucion) {
        this.idEvolucion = idEvolucion;
    }

    public String getExamenFisico() {
        return examenFisico;
    }

    public void setExamenFisico(String examenFisico) {
        this.examenFisico = examenFisico;
    }

    public java.lang.String getIdTipoAdmision() {
        return idTipoAdmision;
    }

    public void setIdTipoAdmision(java.lang.String value) {
        idTipoAdmision = value;
    }
}
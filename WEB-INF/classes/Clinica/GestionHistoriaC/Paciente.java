package Clinica.GestionHistoriaC;

import Clinica.Presentacion.HistoriaClinica.PacienteVO;
import Sgh.AdminSeguridad.Usuario;
import Sgh.Utilidades.Conexion;
import Sgh.Utilidades.Constantes;
import Sgh.Utilidades.LoggableStatement;
import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.net.URI;
import javax.json.JsonObject;
import javax.json.spi.JsonProvider;
import WebSocket.SocketCliente;

public class Paciente {

    private Integer id;
    private String tipoId;
    private String Identificacion;
    private String Nombre1;
    private String Nombre2;
    private String Apellido1;
    private String Apellido2;
    private String idMunicipio;
    private String fechaNacimiento;
    private String direccion;
    private String telefono;
    private String celular1;
    private String celular2;
    private String email;
    private String idOcupacion;
    private String acompanante;
    private String idEstadoCivil;
    private String sexo;
    private String esTraslado;
    private String pacienteTodo;
    private String existe;
    private Float var8;
    private Float var9;
    private Float var10;
    private Integer var1;
    private Integer var6;
    private Integer var7;
    private String var2;
    private String var3;
    private String var4;
    private String var5;
    private String var11;
    private String var12;
    private String noticia;
    private Integer idDocumento;
    public Constantes constantes;
    private Conexion cn;
    private StringBuffer sql = new StringBuffer();

    public Paciente() {
        id = Integer.valueOf(0);
        tipoId = "";
        Identificacion = "";
        Nombre1 = "";
        Nombre2 = "";
        Apellido1 = "";
        Apellido2 = "";
        idMunicipio = "";
        fechaNacimiento = "";
        direccion = "";
        telefono = "";
        celular1 = "";
        celular2 = "";
        sexo = "";
        email = "";
        idOcupacion = "";
        acompanante = "";
        idEstadoCivil = "";
        pacienteTodo = "";
        existe = "";
        var1 = Integer.valueOf(0);
        var6 = Integer.valueOf(0);
        var7 = Integer.valueOf(0);
        var2 = "";
        var3 = "";
        var4 = "";
        var5 = "";
        var11 = "";
        var12 = "";
        noticia = "";
        idDocumento = Integer.valueOf(0);
        esTraslado = "";
    }

    public String siExistePaciente() {
        pacienteTodo = "";
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(" SELECT ID, TIPO_ID, IDENTIFICACION, NOMBRE_COMPLETO");
                localStringBuffer.append(" FROM HC.VI_PACIENTE WHERE TIPO_ID =? AND IDENTIFICACION = ? ");
                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setString(1, tipoId);
                cn.ps2.setString(2, Identificacion);

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        pacienteTodo = (cn.rs2.getString("ID") + "-" + cn.rs2.getString("TIPO_ID")
                                + cn.rs2.getString("IDENTIFICACION") + " " + cn.rs2.getString("NOMBRE_COMPLETO"));
                        cn.cerrarPS(2);
                    }
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> DocumentoHc.java-- function siExistePaciente --> SQLException --> "
                    + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> DocumentoHc.java-- function siExistePaciente --> Exception --> "
                    + localException.getMessage());
        }

        return pacienteTodo;
    }

    public boolean crearPaciente() {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                if (siExistePaciente().equals("")) {
                    cn.iniciatransaccion();
                    cn.prepareStatementIDU(1, cn.traerElQuery(311));
                    cn.ps1.setString(1, tipoId);
                    cn.ps1.setString(2, Identificacion);
                    cn.ps1.setString(3, Apellido1);
                    cn.ps1.setString(4, Apellido2);
                    cn.ps1.setString(5, Nombre1);
                    cn.ps1.setString(6, Nombre2);
                    cn.ps1.setString(7, fechaNacimiento);
                    cn.ps1.setString(8, sexo);
                    cn.ps1.setString(9, idMunicipio);
                    cn.ps1.setString(10, direccion);
                    cn.ps1.setString(11, acompanante);
                    cn.ps1.setString(12, telefono);
                    cn.ps1.setString(13, celular1);
                    cn.ps1.setString(14, celular2);
                    cn.ps1.setString(15, email);
                    cn.ps1.setString(16, cn.usuario.getLogin());
                    cn.iduSQL(1);
                    existe = "N";
                    cn.fintransaccion();
                } else {
                    existe = "S";
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método crearPaciente() de Paciente.java");
            cn.exito = false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
            cn.exito = false;
        }
        siExistePaciente();
        return cn.exito;
    }

    public boolean buscarIdentificacionPaciente() {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                if (siExistePaciente().equals("")) {
                    existe = "N";
                } else {
                    existe = "S";
                }
            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
            cn.exito = false;
        }
        return cn.exito;
    }

    public String TienePendienteLE() {
        pacienteTodo = "NO_PENDIENTE";
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(" SELECT PENDI.PENDIENTE_LE FROM( ");
                localStringBuffer.append(
                        "   SELECT CASE WHEN ad.id_lista_espera IS NULL THEN 'PENDIENTE' ELSE 'NO_PENDIENTE' END AS PENDIENTE_LE ");
                localStringBuffer.append("   FROM   citas.lista_espera  le ");
                localStringBuffer.append("   LEFT JOIN  citas.agenda_detalle ad ON le.id = ad.id_lista_espera");
                localStringBuffer.append(
                        "   WHERE le.id_paciente = ?::integer AND le.id_especialidad = ?::integer AND le.id_sub_especialidad = ?::integer AND le.id_estado != 5::integer AND le.con_cita = 'NO' ");
                localStringBuffer.append(" )PENDI ORDER BY PENDI.PENDIENTE_LE");

                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setString(1, var2);
                cn.ps2.setString(2, var3);
                cn.ps2.setString(3, var4);

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        pacienteTodo = cn.rs2.getString("PENDIENTE_LE");
                    }
                }
                cn.cerrarPS(2);
            }

        } catch (SQLException localSQLException) {
            System.out.println("Error --> Paciente.java-- function siExisteLe--> SQLException --> "
                    + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println(
                    "Error --> Paciente.java-- function siExisteLe --> Exception --> " + localException.getMessage());
        }

        System.out.println("TienePendienteLE = " + pacienteTodo);
        return pacienteTodo;
    }

    public String TieneCitaFutura() {
        pacienteTodo = "SIN_CITA_FUTURA";
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(
                        "SELECT CASE WHEN ad.id IS NOT NULL THEN 'PENDIENTE' ELSE 'NO_PENDIENTE' END AS PENDIENTE_LE  ");
                localStringBuffer.append("FROM   citas.agenda_detalle ad  ");
                localStringBuffer.append("INNER JOIN citas.agenda a ON ad.id_agenda = a.id  ");
                localStringBuffer
                        .append("INNER JOIN citas.agenda_estado ae ON (ad.id_estado = ae.id and ae.exito = 'S')  ");
                localStringBuffer.append("WHERE ad.id_paciente = ?::integer ");
                localStringBuffer.append("AND a.id_especialidad = ?::integer  ");
                localStringBuffer.append("AND a.id_sub_especialidad = ?::integer ");
                localStringBuffer.append("AND ad.fecha_cita >  now()  ");
                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setString(1, var2);
                cn.ps2.setString(2, var3);
                cn.ps2.setString(3, var4);

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        pacienteTodo = cn.rs2.getString("PENDIENTE_LE");
                    }
                }
                cn.cerrarPS(2);
            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> Paciente.java-- function siExisteLe--> SQLException --> "
                    + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println(
                    "Error --> Paciente.java-- function siExisteLe --> Exception --> " + localException.getMessage());
        }
        System.out.println("TieneCitaFutura = " + pacienteTodo);
        return pacienteTodo;
    }

    public boolean espacioDeCitaDisponible() {
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(" SELECT id_estado FROM citas.agenda_detalle where id = ? ");
                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setInt(1, var7.intValue());

                if ((cn.selectSQL(2)) && (cn.rs2.next())) {

                    if (cn.rs2.getString("id_estado").equals("D")) {
                        cn.cerrarPS(2);

                        return true;
                    }

                    cn.cerrarPS(2);
                    return false;
                }

            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> Paciente.java-- function espacioDeCitaDisponible --> SQLException --> "
                    + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> Paciente.java-- function espacioDeCitaDisponible --> Exception --> "
                    + localException.getMessage());
        }

        return true;
    }

    public String consultarDisponibleDesdeListaEspera() {
        String str = "";
        try {
            if (cn.isEstado()) {
                if (TienePendienteLE().equals("NO_PENDIENTE")) {
                    if (TieneCitaFutura().equals("SIN_CITA_FUTURA")) {
                        str = "N";
                    } else {
                        str = "C";
                    }
                } else {
                    str = "S";
                }
            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
            str = "";
        }
        return str;
    }

    public String consultarDisponibleDesdeAgenda() {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                if (espacioDeCitaDisponible()) {
                    if (var11.equals("")) {
                        System.out.println("__SIN LISTA ESPERA");

                        if (TienePendienteLE().equals("NO_PENDIENTE")) {
                            if (TieneCitaFutura().equals("SIN_CITA_FUTURA")) {
                                existe = "N";
                            } else {
                                existe = "C";
                            }
                        } else {
                            existe = "S";
                        }
                    } else {
                        System.out.println("__CON LISTA ESPERA");

                        if (TieneCitaFutura().equals("SIN_CITA_FUTURA")) {
                            existe = "N";
                        } else {
                            existe = "C";
                        }
                    }
                } else {
                    existe = "O";
                }

            }
        } catch (Exception localException) {
            cn.exito = false;
        }
        return existe;
    }

    public boolean consultarDisponibleAgendaSinLE() {
        System.out.println("setVar2" + var2);
        System.out.println("setVar3" + var3);
        System.out.println("setVar4" + var4);

        cn.exito = true;
        try {
            if (cn.isEstado()) {
                if (TienePendienteLE().equals("NO_PENDIENTE")) {
                    existe = "N";
                } else {
                    existe = "S";
                }

            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
            cn.exito = false;
        }
        return cn.exito;
    }

    public boolean buscarSiFolioTieneDiligenciadosElementos(int paramInt) {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                cn.prepareStatementSEL(3, cn.traerElQuery(paramInt));
                cn.ps3.setInt(1, var1.intValue());
                System.out.println(((LoggableStatement) cn.ps3).getQueryString());
                if (cn.selectSQL(3)) {
                    if ((cn.rs3 != null) && (cn.rs3.next())) {
                        if (cn.rs3.getString("NOTICIA").equals("DILIGENCIADO")) {
                            System.out.println("DILIGENCIADO --> " + paramInt);
                            return true;
                        }

                        noticia = cn.rs3.getString("NOTICIA");
                        System.out.println(noticia);
                        return false;
                    }

                    cn.cerrarPS(3);
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage()
                    + " en método buscarSiFolioTieneDiligenciadosElementos(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage()
                    + " en método buscarSiFolioTieneDiligenciadosElementos(), de Paciente.java");
            return false;
        }
        return false;
    }

    public void actualizarEstadoFacturacionCita() {
        System.out.println("*********************  FFFFFF");
        try {
            if (cn.isEstado()) {
                if(var3.equals("F")){
                    System.out.println("*********************  FFFFFF");
                    cn.prepareStatementIDU(2, cn.traerElQuery(209));
                    cn.ps2.setString(1, var2.trim());
                    cn.ps2.setString(2, var2.trim());
                    cn.ps2.setInt(3, var1);
                }else{
                    cn.prepareStatementIDU(2, cn.traerElQuery(210));
                    cn.ps2.setString(1, var2.trim());
                    cn.ps2.setInt(2, var1);
                }

                System.out.println("-- ACTUALIZA ESTADO DE CITA --");
                cn.iduSQL(2);
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage()
                    + " en método actualizarEstadoFacturacionCita(),, de Paciente.java");
        } catch (Exception localException) {
            System.out.println(
                    localException.getMessage() + " en método actualizarEstadoFacturacionCita(), de Paciente.java");
        }
    }

    public boolean buscarElementosObligatoriosFolio() {
        System.out.println("var1= " + var1);
        System.out.println("var2= " + var2);
        boolean bool = true;
        noticia = "";
        try {
            if (cn.isEstado()) {
                cn.prepareStatementSEL(2, cn.traerElQuery(443));
                cn.ps2.setString(1, var2.trim());

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        if (cn.rs2.getString("SW_MOTIVO_ENFERMEDAD").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(450);
                            if (!bool) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_ANTECEDENTES").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(451);
                            if (!bool) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_POS_OPERATORIO").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(454);
                            if (!bool) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_EXAMEN_FISICO").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(452);
                            if (!bool) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_DIAGNOSTICOS").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(453);
                            if (!bool) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_SIGNOS_VITALES").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(452);
                            if (!bool) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_CONSENTIMIENTO").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(455);
                            if (!bool) {
                                return false;
                            }
                        }
                    }

                    cn.cerrarPS(2);
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage()
                    + " en método buscarElementosObligatoriosFolio(),, de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(
                    localException.getMessage() + " en método buscarElementosObligatoriosFolio(), de Paciente.java");
            return false;
        }
        return bool;
    }

    /* para bodegas */
    public int bodega_doc() {
        boolean bool = true;
        int i = 0;
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());

                sql.append("\n select id_bodega cantidad ");
                sql.append("\n from inventario.documentos  ");
                sql.append("\n where id = (select id_doc from inventario.candado)::integer ");

                cn.prepareStatementSEL(2, sql);
                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        System.out.println("--Result bodega origen- =" + cn.rs2.getString("cantidad"));
                        i = cn.rs2.getInt("cantidad");
                    }
                    cn.cerrarPS(2);
                }
                return i;
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 cambiarLote(), de Paciente.java");
            return 0;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 cambiarLote(), de Paciente.java");
            return 0;
        }
        return i;
    }

    public boolean hacerSalidasInventario() {
        boolean bool = true;
        noticia = "xxx";
        String ide_us = "";
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                if (cn.getMotor().equals("sqlserver")) {
                    sql.append("INSERT INTO INVENTARIO.CANDADO (ID_DOC, ID_USUARIO) VALUES(?,?) ");
                } else {
                    System.out.println("--POSTGRES B ");
                    sql.append("\n INSERT INTO inventario.candado (id_doc, id_usuario) VALUES(?::integer,?) ");
                }
                ide_us = cn.usuario.getIdentificacion();
                while (ide_us.equals("")) {
                    ide_us = cn.usuario.getIdentificacion();
                }
                cn.prepareStatementSEL(1, sql);
                cn.ps1.setInt(1, var1.intValue());
                cn.ps1.setString(2, ide_us);
                System.out.println("--0=" + ((LoggableStatement) cn.ps1).getQueryString());
                cn.iduSQL(1);

                sql.delete(0, sql.length());
                if (cn.getMotor().equals("sqlserver")) {
                    sql.append("UPDATE INVENTARIO.TRANSACCIONES  SET EXITO ='S' WHERE ID_DOCUMENTO =? ");
                } else {
                    System.out.println("--POSTGRES B ");
                    sql.append("\n UPDATE inventario.transacciones  SET exito ='S' WHERE id_documento =?::integer ");
                }
                cn.prepareStatementSEL(1, sql);
                cn.ps1.setInt(1, var1.intValue());
                System.out.println("--0.1=" + ((LoggableStatement) cn.ps1).getQueryString());
                cn.iduSQL(1);

                sql.delete(0, sql.length());
                if (cn.getMotor().equals("sqlserver")) {
                    sql.append(
                            "\n INSERT INTO INVENTARIO.TRANSACCIONES_TMP( ID, ID_DOCUMENTO, ID_ARTICULO, CANTIDAD,VALOR_UNITARIO,IVA,VALOR_IMPUESTO, LOTE, SERIAL, PRECIO_VENTA ,VALOR_TOTAL, FECHA_VENCIMIENTO )  ");
                    sql.append(
                            " ( SELECT ID, ID_DOCUMENTO, ID_ARTICULO, CANTIDAD,VALOR_UNITARIO,IVA,VALOR_IMPUESTO, LOTE, SERIAL,PRECIO_VENTA ,(CANTIDAD*VALOR_UNITARIO), FECHA_VENCIMIENTO   FROM INVENTARIO.TRANSACCIONES  WHERE  ID_DOCUMENTO = ?)");
                } else {
                    System.out.println("--POSTGRES B ");
                    sql.append(
                            "\n INSERT INTO inventario.transacciones_tmp( id, id_documento, id_articulo, cantidad,valor_unitario,iva,valor_impuesto, lote, serial, precio_venta ,valor_total, fecha_vencimiento )  ");
                    sql.append(
                            "\n ( SELECT id, id_documento, id_articulo, cantidad,valor_unitario,iva,valor_impuesto, lote, serial,precio_venta ,(cantidad*valor_unitario), fecha_vencimiento   FROM inventario.transacciones  WHERE  id_documento = ?::integer)");
                }
                cn.prepareStatementIDU(2, sql);
                cn.ps2.setInt(1, var1.intValue());
                System.out.println("--1 " + ((LoggableStatement) cn.ps2).getQueryString());
                cn.iduSQL(2);

                sql.delete(0, sql.length());

                System.out.println("--POSTGRES B ");

                sql.append("\n UPDATE ");
                sql.append("\n  inventario.transacciones_tmp ");
                sql.append("\n SET ");
                sql.append("\n  exito = 'N' ");
                sql.append("\n WHERE id IN ( ");
                sql.append("\n SELECT  ");
                sql.append("\n      cantidad.id_transaccion  ");
                sql.append("\n    FROM ");
                sql.append("\n    ( ");
                sql.append("\n      SELECT  ");
                sql.append("\n        tr.id id_transaccion, ");
                sql.append("\n        case when  bel.existencia is null then -tr.cantidad ");
                sql.append("\n          when tr.cantidad > bel.existencia then bel.existencia - tr.cantidad ");
                sql.append("\n       else bel.existencia - tr.cantidad  ");
                sql.append("\n       end existencias ");
                sql.append("\n     FROM  ");
                sql.append("\n       inventario.transacciones_tmp tr ");
                sql.append("\n     inner join inventario.documentos doc on tr.id_documento = doc.id ");
                sql.append(
                        "\n     left join inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo and doc.id_bodega = bel.id_bodega and tr.lote = bel.lote) ");
                sql.append("\n     where doc.id = ( select id_doc from inventario.candado)::integer ");
                sql.append("\n     and tr.lote <> ''  ");
                sql.append("\n    ) as cantidad ");
                sql.append("\n   WHERE cantidad.existencias < 0 ");
                sql.append("\n ); ");

                sql.append("\n update  ");
                sql.append("\n  inventario.transacciones_tmp   ");
                sql.append("\n set  ");
                sql.append("\n  exito = 'N' ");
                sql.append("\n WHERE id in (     ");
                sql.append("\n select  ");
                sql.append("\n     cantidad.id        ");
                sql.append("\n   from ");
                sql.append("\n   (     ");
                sql.append("\n       select ");
                sql.append("\n         tr.id,    ");
                sql.append("\n         tr.id_articulo, ");
                sql.append("\n         doc.id_bodega, ");
                sql.append("\n         case when  be.existencia is null then tr.cantidad ");
                sql.append("\n           when tr.cantidad > be.existencia then be.existencia - tr.cantidad ");
                sql.append("\n         else be.existencia - tr.cantidad  end existencias ");
                sql.append("\n       FROM ");
                sql.append("\n         inventario.transacciones_tmp tr ");
                sql.append("\n       inner join inventario.documentos doc on tr.id_documento = doc.id ");
                sql.append(
                        "\n       left join inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo and doc.id_bodega = be.id_bodega) ");
                sql.append("\n       where tr.id_documento = ( select id_doc from inventario.candado)::integer ");
                sql.append("\n       and tr.lote = '' ");
                sql.append("\n   ) as cantidad   ");
                sql.append("\n   where cantidad.existencias < 0 ");
                sql.append("\n ); ");

                cn.prepareStatementIDU(1, sql);
                System.out.println("--TIGERR " + ((LoggableStatement) cn.ps1).getQueryString());
                cn.iduSQL(1);

                sql.delete(0, sql.length());
                if (cn.getMotor().equals("sqlserver")) {
                    sql.append(" UPDATE INVENTARIO.TRANSACCIONES SET  EXITO = 'N' ");
                    sql.append(" FROM INVENTARIO.TRANSACCIONES a, INVENTARIO.TRANSACCIONES_TMP b ");
                    sql.append(" WHERE a.ID = b.ID AND a.ID_DOCUMENTO = ? AND b.EXITO = 'N' ");
                } else {
                    System.out.println("--POSTGRES B ");
                    sql.append("\n UPDATE inventario.transacciones set  exito = 'N' ");
                    sql.append("\n where id in ( ");
                    sql.append(
                            "\n SELECT id FROM inventario.transacciones_tmp where id_documento = ?::integer and exito = 'N'); ");
                }
                cn.prepareStatementIDU(2, sql);
                cn.ps2.setInt(1, var1.intValue());
                System.out.println("--1.1.1 " + ((LoggableStatement) cn.ps2).getQueryString());
                cn.iduSQL(2);

                sql.delete(0, sql.length());
                sql.append("\n SELECT exito FROM inventario.transacciones_tmp WHERE exito = 'N'");
                cn.prepareStatementSEL(2, sql);
                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        System.out.println("--3 =" + cn.rs2.getString("exito"));
                        if (cn.rs2.getString("exito").equals("N")) {
                            bool = false;
                        }
                    }

                    cn.cerrarPS(1);
                }

                if (bool) {
                    sql.delete(0, sql.length());
                    if (esTraslado == "SI") {
                        if (cn.getMotor().equals("sqlserver")) {
                            sql.append(" EXEC INVENTARIO.SP_CIERRA_DOCUMENTO_SALIDA_TRASLADO;  ");

                            cn.prepareStatementIDU(1, sql);
                            cn.iduSQL(1);
                        } else {
                            System.out.println("--POSTGRES A -");
                            if (bodega_doc() == 1) {
                                if (cierraDocumentoSalidaTraslado()) {
                                    System.out.println(
                                            " -******************************** Se Ejecuto sp cerrar el documento TRASLADO salida farmaci ***************************************************************-");
                                }
                            } else if (cierraDocumentoSalidaTraslado2()) {
                                System.out.println(
                                        " -******************************** Se Ejecuto sp cerrar el documento TRASLADO ***************************************************************-");
                            }
                        }
                    } else if (cn.getMotor().equals("sqlserver")) {
                        sql.append(" EXEC INVENTARIO.SP_CIERRA_DOCUMENTO_SALIDA;  ");

                        cn.prepareStatementIDU(1, sql);
                        cn.iduSQL(1);
                    } else {
                        System.out.println("--POSTGRES A -");
                        if (cierraDocumentoSalida()) {
                            System.out.println(" -************* Se Ejecuto sp cerrar el documento  **-");
                        } else {
                            bool = false;
                        }
                    }

                    System.out.println(" -** Se Ejecuto los procedimientos para cerrar el documentos  **-");
                    System.out.println("CEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEERRANDO "
                            + ((LoggableStatement) cn.ps1).getQueryString());
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(
                    localSQLException.getMessage() + " en método 1 hacerSalidasInventario(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 hacerSalidasInventario(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean cierraDocumentoSalida() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());
            if (cn.getMotor().equals("postgres")) {
                sql.append("\tINSERT INTO  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\t(  id_articulo,id_bodega,lote,existencia,fecha_vencimiento  )\t");
                sql.append("\t(\t");

                sql.append("\t\tSELECT tr.id_articulo, tr.id_bodega,tr.lote,\t");
                sql.append(" \t\t\tCASE WHEN  bel.existencia IS NULL THEN -tr.cantidad\t");
                sql.append("\t\t\tWHEN tr.cantidad > bel.existencia THEN bel.existencia - tr.cantidad\t");
                sql.append("\t\t\t  ELSE bel.existencia - tr.cantidad END existencias,\t");
                sql.append("\t\t\t  tr.fecha_vencimiento\t");
                sql.append(
                        "\t\tFROM (  \tSELECT t.id_articulo, doc.id_bodega , t.lote, sum(t.cantidad) cantidad, t.fecha_vencimiento, doc.id");
                sql.append(" \t\t\t\t   from inventario.transacciones_tmp t ");
                sql.append("\t\t\t    INNER JOIN inventario.documentos doc on t.id_documento = doc.id\t");
                sql.append("\t\t\t    WHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t\t    GROUP by  t.id_articulo, doc.id_bodega , t.lote, t.fecha_vencimiento, doc.id");
                sql.append("\t\t) tr            \t\t\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND tr.id_bodega = bel.id_bodega AND tr.lote = bel.lote)");
                sql.append("\t\tWHERE tr.lote <> ''");

                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias_lote\t");
                sql.append("\tWHERE  id_articulo IN (     SELECT  tr.id_articulo\t");
                sql.append("\t\t\tFROM  inventario.transacciones tr\t\t");
                sql.append("\t\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\tLEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\t\tWHERE doc.id =  ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("    )\t");
                sql.append("\t  AND id_bodega IN (SELECT   doc.id_bodega\t");
                sql.append("\t\tFROM   inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\tWHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t)\t");
                sql.append("\t AND lote IN (\t");
                sql.append("\t\tSELECT    tr.lote\t");
                sql.append("\t\tFROM   inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\tWHERE doc.id =( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t);\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias_lote\t");
                sql.append("\t(  id_articulo, id_bodega,lote,existencia,fecha_vencimiento )\t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT id_articulo, id_bodega,lote, existencia, fecha_vencimiento\t");
                sql.append("\t\tFROM  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\t);\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias_tmp\t");
                sql.append("\t(  id_articulo,id_bodega, existencia )\t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT tr.id_articulo, doc.id_bodega,\t");
                sql.append("\t\t  CASE WHEN  be.existencia IS NULL THEN - sum (tr.cantidad )\t");
                sql.append("\t\t\tWHEN sum (tr.cantidad ) > be.existencia THEN be.existencia - sum (tr.cantidad )\t");
                sql.append("\t\t  ELSE be.existencia - sum (tr.cantidad ) END existencias\t");
                sql.append("\t\tFROM  inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\tWHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)    ::integer\t");
                sql.append("\t\tgroup by tr.id_articulo,doc.id_bodega, be.existencia\t");
                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias\t");
                sql.append("\tWHERE id_articulo IN (SELECT tr.id_articulo FROM inventario.transacciones tr\t");
                sql.append("\t\t\t\t\t\t  INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\t\t\t\t  LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\t\t\t\t\t  WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t\t\t\t  )\t");
                sql.append("\tAND id_bodega IN (SELECT doc.id_bodega FROM  inventario.transacciones tr\t");
                sql.append("\t\t\t\t\t  INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\t\t\t  LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\t\t\t\t  WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t\t\t\t  );\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias\t");
                sql.append("\t(  id_articulo, id_bodega, existencia )\t");
                sql.append("\t(\t");
                sql.append("\t  SELECT id_articulo,id_bodega,existencia\t");
                sql.append("\t  FROM inventario.bodegas_existencias_tmp\t");
                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias_tmp ;\t");
                sql.append("\tDELETE FROM inventario.bodegas_existencias_lote_tmp ;\t");

                sql.append("\t   INSERT INTO inventario.inventarios_tmp\t");
                sql.append("\t(\t");
                sql.append("\t  id_articulo,\t");
                sql.append("\t  valor_unitario,\t");
                sql.append("\t  precio_venta,\t\t");
                sql.append("\t  existencia,\t");
                sql.append("\t  valor_total\t");
                sql.append("\t) \t");
                sql.append("\t (\t");
                sql.append("\t\t SELECT  \t");
                sql.append("\t\t  tr.id_articulo,  \t");
                sql.append("\t\t  CASE WHEN (invt.existencia-tr.cantidad) = 0 THEN '0'\t");
                sql.append("\t\t\t   ELSE ((invt.valor_total-tr.valor_total)/(invt.existencia-tr.cantidad))\t\t");
                sql.append("\t\t  END valor_unitario,\t");
                sql.append("\t\t  tr.precio_venta,\t");
                sql.append("\t\t  invt.existencia -tr.cantidad ,\t");
                sql.append("\t\t  invt.valor_total-tr.valor_total\t");
                sql.append("\t\tFROM (    \t");
                sql.append("\t\tSELECT trp.id_documento,\t");
                sql.append("\t\t\ttrp.id_articulo, \t");
                sql.append("\t\t\tsum (trp.valor_unitario) valor_unitario,\t");
                sql.append("\t\t\t0 precio_venta,\t");
                sql.append("\t\t\tsum(trp.cantidad) cantidad, \t");
                sql.append("\t\t\tsum (trp.valor_total) valor_total\t");
                sql.append("\t\t\tFROM inventario.transacciones_tmp  trp \t");
                sql.append("\t\tGROUP BY trp.id_articulo,  trp.id_documento ");
                sql.append("\t\t) tr  ");
                sql.append("\t\tINNER JOIN inventario.inventarios  invt ON tr.id_articulo = invt.id_articulo  ");
                sql.append("\t\tWHERE id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t);   ");

                sql.append("\tUPDATE inventario.transacciones_tmp ");
                sql.append("\tSET valor_unitario_promedio = inventario.inventarios_tmp.valor_unitario ");
                sql.append(
                        "\tFROM inventario.inventarios_tmp WHERE inventario.inventarios_tmp.id_articulo = inventario.transacciones_tmp.id_articulo; ");

                sql.append("\tUPDATE inventario.transacciones  ");
                sql.append("\tSET valor_unitario_promedio = inventario.transacciones_tmp.valor_unitario_promedio ");
                sql.append(
                        "\tFROM inventario.transacciones_tmp WHERE inventario.transacciones_tmp.id = inventario.transacciones.id; ");

                sql.append("\tDELETE FROM inventario.inventarios \t");
                sql.append("\tWHERE id_articulo IN (SELECT  tr.id_articulo  \t");
                sql.append("\t\t\t\t\t\t  FROM inventario.transacciones_tmp tr\t");
                sql.append("\t\t\t\t\t\t  LEFT JOIN inventario.inventarios inv on tr.id_articulo = inv.id_articulo \t");
                sql.append("\t\t\t\t\t\t  WHERE id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t);\t");

                sql.append("\tINSERT INTO inventario.inventarios\t");
                sql.append("\t(\t");
                sql.append("\t  id_articulo,\t");
                sql.append("\t  valor_unitario,\t");
                sql.append("\t  precio_venta,\t");
                sql.append("\t  existencia,\t");
                sql.append("\t  valor_total\t");
                sql.append("\t) \t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT \t");
                sql.append("\t\tid_articulo,\t");
                sql.append("\t\tvalor_unitario,\t");
                sql.append("\t\tprecio_venta,\t");
                sql.append("\t\texistencia,\t");
                sql.append("\t\tvalor_total\t");
                sql.append("\t\tFROM inventario.inventarios_tmp\t");
                sql.append("\t);\t");

                sql.append("DELETE FROM inventario.inventarios_tmp ;\t");

                sql.append("\tupdate  inventario.documentos\t");
                sql.append("\tset  fecha_cierre = now(),\t");
                sql.append("\t  id_usuario_cierra = ( SELECT id_usuario FROM inventario.candado),\t");
                sql.append("\t  id_estado = 1\t");
                sql.append("\tWHERE  id = ( SELECT id_doc FROM inventario.candado)::integer;\t");
            }
            cn.prepareStatementIDU(1, sql);
            System.out.println("-- cierra documento --");
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out
                    .println(localSQLException.getMessage() + " en método 1 cierraDocumentoSalida(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 cierraDocumentoSalida(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean cierraDocumentoSalidaTraslado() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());
            if (cn.getMotor().equals("postgres")) {

                sql.append("\n UPDATE inventario.transacciones  ");
                sql.append("\n set estado = 'N'  ");
                sql.append("\n where id in ( ");
                sql.append("\n   select id_transaccion  ");
                sql.append("\n from inventario.vi_traslado_devolucion       ");
                sql.append("\n   where id_documento = (select id_doc from inventario.candado) ");
                sql.append("\n ); ");

                sql.append("\n INSERT INTO  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\n (  id_articulo,id_bodega,lote,existencia,fecha_vencimiento  )\t");
                sql.append("\n (\t");
                sql.append("\n SELECT tr.id_articulo, tr.id_bodega,tr.lote,\t");
                sql.append("\n CASE WHEN  bel.existencia IS NULL THEN -tr.cantidad\t");
                sql.append("\n WHEN tr.cantidad > bel.existencia THEN bel.existencia - tr.cantidad\t");
                sql.append("\n  ELSE bel.existencia - tr.cantidad END existencias,\t");
                sql.append("\n  tr.fecha_vencimiento\t");
                sql.append(
                        "\n FROM (  \tSELECT t.id_articulo, doc.id_bodega , t.lote, sum(t.cantidad) cantidad, t.fecha_vencimiento, doc.id");
                sql.append("\n   from inventario.transacciones_tmp t ");
                sql.append("\n    INNER JOIN inventario.documentos doc on t.id_documento = doc.id\t");
                sql.append("\n    WHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n    GROUP by  t.id_articulo, doc.id_bodega , t.lote, t.fecha_vencimiento, doc.id");
                sql.append("\n ) tr            \t\t\t");
                sql.append(
                        "\n LEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND tr.id_bodega = bel.id_bodega AND tr.lote = bel.lote)");
                sql.append("\n WHERE tr.lote <> ''");
                sql.append("\n );\t");

                sql.append("\n DELETE FROM inventario.bodegas_existencias_lote\t");
                sql.append("\n WHERE  id_articulo IN (     SELECT  tr.id_articulo\t");
                sql.append("\n FROM  inventario.transacciones tr\t\t");
                sql.append("\n INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n LEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\n WHERE doc.id =  ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n )");
                sql.append("\n  AND id_bodega IN (SELECT   doc.id_bodega\t");
                sql.append("\n FROM   inventario.transacciones tr\t");
                sql.append("\n INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n LEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\n WHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n )\t");
                sql.append("\n  AND lote IN (\t");
                sql.append("\n SELECT    tr.lote\t");
                sql.append("\n FROM   inventario.transacciones tr\t");
                sql.append("\n INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n LEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\n WHERE doc.id =( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n );");

                sql.append("\n INSERT INTO inventario.bodegas_existencias_lote\t");
                sql.append("\n (  id_articulo, id_bodega,lote,existencia,fecha_vencimiento )\t");
                sql.append("\n (\t");
                sql.append("\n SELECT id_articulo, id_bodega,lote, existencia, fecha_vencimiento\t");
                sql.append("\n FROM  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\n );\t");

                sql.append("\n INSERT INTO inventario.bodegas_existencias_tmp\t");
                sql.append("\n (  id_articulo,id_bodega, existencia )\t");
                sql.append("\n (\t");
                sql.append("\n SELECT tr.id_articulo, doc.id_bodega,\t");
                sql.append("\n   CASE WHEN  be.existencia IS NULL THEN - sum (tr.cantidad )\t");
                sql.append("\n WHEN sum (tr.cantidad ) > be.existencia THEN be.existencia - sum (tr.cantidad )\t");
                sql.append("\n   ELSE be.existencia - sum (tr.cantidad ) END existencias\t");
                sql.append("\n FROM  inventario.transacciones tr\t");
                sql.append("\n INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\n WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)    ::integer\t");
                sql.append("\n group by tr.id_articulo,doc.id_bodega, be.existencia\t");
                sql.append("\n );\t");

                sql.append("\n DELETE FROM inventario.bodegas_existencias\t");
                sql.append("\n WHERE id_articulo IN (SELECT tr.id_articulo FROM inventario.transacciones tr\t");
                sql.append("\n   INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n   LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\n   WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n   )\t");
                sql.append("\n AND id_bodega IN (SELECT doc.id_bodega FROM  inventario.transacciones tr\t");
                sql.append("\n  INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n  LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\n   WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n  );\t");

                sql.append("\n INSERT INTO inventario.bodegas_existencias\t");
                sql.append("\n (  id_articulo, id_bodega, existencia )\t");
                sql.append("\n (\t");
                sql.append("\n   SELECT id_articulo,id_bodega,existencia\t");
                sql.append("\n   FROM inventario.bodegas_existencias_tmp\t");
                sql.append("\n );\t");

                sql.append("\n DELETE FROM inventario.bodegas_existencias_tmp ;\t");
                sql.append("\n DELETE FROM inventario.bodegas_existencias_lote_tmp ;\t");

                sql.append("\n    INSERT INTO inventario.inventarios_tmp\t");
                sql.append("\n (\t");
                sql.append("\n   id_articulo,\t");
                sql.append("\n   valor_unitario,\t");
                sql.append("\n   precio_venta,\t\t");
                sql.append("\n   existencia,\t");
                sql.append("\n   valor_total\t");
                sql.append("\n ) \t");
                sql.append("\n  (\t");
                sql.append("\n  SELECT  \t");
                sql.append("\n   tr.id_articulo,  \t");
                sql.append("\n   CASE WHEN (invt.existencia-tr.cantidad) = 0 THEN '0'\t");
                sql.append("\n    ELSE ((invt.valor_total-tr.valor_total)/(invt.existencia-tr.cantidad))\t\t");
                sql.append("\n   END valor_unitario,\t");
                sql.append("\n   tr.precio_venta,\t");
                sql.append("\n   invt.existencia -tr.cantidad ,\t");
                sql.append("\n   invt.valor_total-tr.valor_total\t");
                sql.append("\n FROM (    \t");
                sql.append("\n SELECT trp.id_documento,\t");
                sql.append("\n trp.id_articulo, \t");
                sql.append("\n sum (trp.valor_unitario) valor_unitario,\t");
                sql.append("\n 0 precio_venta,\t");
                sql.append("\n sum(trp.cantidad) cantidad, \t");
                sql.append("\n sum (trp.valor_total) valor_total\t");
                sql.append("\n FROM inventario.transacciones_tmp  trp \t");
                sql.append("\n GROUP BY trp.id_articulo,  trp.id_documento ");
                sql.append("\n ) tr  ");
                sql.append("\n INNER JOIN inventario.inventarios  invt ON tr.id_articulo = invt.id_articulo  ");
                sql.append("\n WHERE id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n );   ");

                sql.append("\n UPDATE inventario.transacciones_tmp ");
                sql.append("\n SET valor_unitario_promedio = inventario.inventarios_tmp.valor_unitario ");
                sql.append(
                        "\n FROM inventario.inventarios_tmp WHERE inventario.inventarios_tmp.id_articulo = inventario.transacciones_tmp.id_articulo; ");

                sql.append("\n UPDATE inventario.transacciones  ");
                sql.append("\n SET valor_unitario_promedio = inventario.transacciones_tmp.valor_unitario_promedio ");
                sql.append(
                        "\n FROM inventario.transacciones_tmp WHERE inventario.transacciones_tmp.id = inventario.transacciones.id; ");

                sql.append("\n DELETE FROM inventario.inventarios \t");
                sql.append("\n WHERE id_articulo IN (SELECT  tr.id_articulo  \t");
                sql.append("\n  FROM inventario.transacciones_tmp tr\t");
                sql.append("\n  LEFT JOIN inventario.inventarios inv on tr.id_articulo = inv.id_articulo \t");
                sql.append("\n  WHERE id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n);\t");

                sql.append("\n INSERT INTO inventario.inventarios\t");
                sql.append("\n (\t");
                sql.append("\n   id_articulo,\t");
                sql.append("\n   valor_unitario,\t");
                sql.append("\n   precio_venta,\t");
                sql.append("\n   existencia,\t");
                sql.append("\n   valor_total\t");
                sql.append("\n ) \t");
                sql.append("\n (\t");
                sql.append("\n SELECT \t");
                sql.append("\n id_articulo,\t");
                sql.append("\n valor_unitario,\t");
                sql.append("\n precio_venta,\t");
                sql.append("\n existencia,\t");
                sql.append("\n valor_total\t");
                sql.append("\n FROM inventario.inventarios_tmp\t");
                sql.append("\n );\t");

                sql.append("DELETE FROM inventario.inventarios_tmp ;\t");

                sql.append("\n update  inventario.documentos\t");
                sql.append("\n set  fecha_cierre = now(),\t");
                sql.append("\n   id_usuario_cierra = ( SELECT id_usuario FROM inventario.candado),\t");
                sql.append("\n   id_estado = 1\t");
                sql.append("\n WHERE  id = ( SELECT id_doc FROM inventario.candado)::integer;\t");
            }
            cn.prepareStatementIDU(1, sql);
            System.out.println("-- cierra documento --");
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out.println(
                    localSQLException.getMessage() + " en método 1 cierraDocumentoSalidaTraslado(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(
                    localException.getMessage() + " en método 2 cierraDocumentoSalidaTraslado(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean cierraDocumentoSalidaTraslado2() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());
            if (cn.getMotor().equals("postgres")) {

                sql.append(" UPDATE inventario.transacciones  ");
                sql.append(" set estado = 'N'  ");
                sql.append(" where id in ( ");
                sql.append("   select id_transaccion  ");
                sql.append("\tfrom inventario.vi_traslado_devolucion       ");
                sql.append("    where id_documento = (select id_doc from inventario.candado) ");
                sql.append(" ); ");

                sql.append("\tINSERT INTO  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\t(  id_articulo,id_bodega,lote,existencia,fecha_vencimiento  )\t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT tr.id_articulo, tr.id_bodega,tr.lote,\t ");
                sql.append("\t\t  CASE WHEN  bel.existencia IS NULL THEN -tr.cantidad\t ");
                sql.append("\t\t\t\tWHEN tr.cantidad > bel.existencia THEN bel.existencia - tr.cantidad\t ");
                sql.append("\t\t\t ELSE bel.existencia - tr.cantidad  ");
                sql.append("             END existencias, ");
                sql.append("          tr.fecha_vencimiento ");
                sql.append("        from ( ");
                sql.append(
                        "       \tSELECT t.id_articulo, doc.id_bodega , t.lote, sum(t.cantidad) cantidad, t.fecha_vencimiento, doc.id ");
                sql.append("           from inventario.transacciones t  ");
                sql.append("            INNER JOIN inventario.documentos doc on t.id_documento = doc.id\t");
                sql.append("            WHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t ");
                sql.append("            GROUP by  t.id_articulo, doc.id_bodega , t.lote, t.fecha_vencimiento, doc.id ");
                sql.append("        ) tr ");
                sql.append(
                        "        LEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND tr.id_bodega = bel.id_bodega AND tr.lote = bel.lote) ");
                sql.append("        WHERE  tr.lote <> '' ");
                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias_lote\t");
                sql.append("\tWHERE  id_articulo IN (     SELECT  tr.id_articulo\t");
                sql.append("\t\t\tFROM  inventario.transacciones tr\t\t");
                sql.append("\t\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\tLEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\t\tWHERE doc.id =  ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("    )\t");
                sql.append("\t  AND id_bodega IN (SELECT   doc.id_bodega\t");
                sql.append("\t\tFROM   inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\tWHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t)\t");
                sql.append("\t AND lote IN (\t");
                sql.append("\t\tSELECT    tr.lote\t");
                sql.append("\t\tFROM   inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\tWHERE doc.id =( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t);\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias_lote\t");
                sql.append("\t(  id_articulo, id_bodega,lote,existencia,fecha_vencimiento )\t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT id_articulo, id_bodega,lote, existencia, fecha_vencimiento\t");
                sql.append("\t\tFROM  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\t);\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias_tmp\t");
                sql.append("\t(  id_articulo,id_bodega, existencia )\t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT tr.id_articulo, doc.id_bodega,\t");
                sql.append("\t\t  CASE WHEN  be.existencia IS NULL THEN - sum (tr.cantidad )\t");
                sql.append("\t\t\tWHEN sum (tr.cantidad ) > be.existencia THEN be.existencia - sum (tr.cantidad )\t");
                sql.append("\t\t  ELSE be.existencia - sum (tr.cantidad ) END existencias\t");
                sql.append("\t\tFROM  inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\tWHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)    ::integer\t");
                sql.append("\t\tgroup by tr.id_articulo,doc.id_bodega, be.existencia\t");
                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias\t");
                sql.append("\tWHERE id_articulo IN (SELECT tr.id_articulo FROM inventario.transacciones tr\t");
                sql.append("\t\t\t\t\t\t  INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\t\t\t\t  LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\t\t\t\t\t  WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t\t\t\t  )\t");
                sql.append("\tAND id_bodega IN (SELECT doc.id_bodega FROM  inventario.transacciones tr\t");
                sql.append("\t\t\t\t\t  INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\t\t\t  LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\t\t\t\t  WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t\t\t\t  );\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias\t");
                sql.append("\t(  id_articulo, id_bodega, existencia )\t");
                sql.append("\t(\t");
                sql.append("\t  SELECT id_articulo,id_bodega,existencia\t");
                sql.append("\t  FROM inventario.bodegas_existencias_tmp\t");
                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias_tmp ;\t");
                sql.append("\tDELETE FROM inventario.bodegas_existencias_lote_tmp ;\t");

                sql.append("\tupdate  inventario.documentos\t");
                sql.append("\tset  fecha_cierre = now(),\t");
                sql.append("\t  id_usuario_cierra = ( SELECT id_usuario FROM inventario.candado),\t");
                sql.append("\t  id_estado = 1\t");
                sql.append("\tWHERE  id = ( SELECT id_doc FROM inventario.candado)::integer;\t");
            }
            cn.prepareStatementIDU(1, sql);
            System.out.println(
                    "-- cierra documento TRASLADO ********************************************************* --");
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out
                    .println(localSQLException.getMessage() + " en método 1 cierraDocumentoSalida(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 cierraDocumentoSalida(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean limpiarCandados() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());
            sql.append("\n delete from inventario.transaccion_modifica ;");
            sql.append("\n DELETE FROM INVENTARIO.BODEGAS_EXISTENCIAS_TMP; ");
            sql.append("\n DELETE FROM INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP; ");

            sql.append("\n delete from inventario.candado; DELETE FROM  INVENTARIO.CANDADO_TMP;  ");
            sql.append("\n delete from inventario.candado_documentos;  ");
            sql.append("\n DELETE FROM INVENTARIO.TRANSACCIONES_TMP;  ");

            sql.append("\n DELETE FROM INVENTARIO.inventarios_tmp; ");
            sql.append("\n DELETE FROM INVENTARIO.promedio_articulo;; ");

            cn.prepareStatementIDU(1, sql);
            System.out.println("LLLLLLLLLLLLLIMPIANDO " + ((LoggableStatement) cn.ps1).getQueryString());
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 limpiarCandados(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 limpiarCandados(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean hacerTrasladoEntreBodegas() {
        System.out.println(
                "-----------------------------------------------------------------------------------------\n hacerTrasladoEntreBodegas::ID_DOC origen= "
                        + var1 + " id_BODEGA_destino=" + var6);
        esTraslado = "SI";
        boolean bool = true;
        noticia = "xxx";
        try {
            cn.iniciatransaccion();
            if (hacerSalidasInventario()) {
                // verificar la bodega de entradas

                if (var6 == 1) {
                    if (hacerEntradasInventarioFarmacia()) {
                        noticia = "DOCUMENTOS FINALIZADOS CON EXITO.";

                        sql.delete(0, sql.length());
                        sql.append("\n UPDATE inventario.documentos ");
                        sql.append(
                                "\n SET fecha_crea = (select fecha_crea from inventario.documentos where id = (select id_doc from inventario.candado)), ");
                        sql.append(
                                "\n   fecha_cierre = (select fecha_cierre from inventario.documentos where id = (select id_doc from inventario.candado)),"
                                        + " id_documento_movimiento = (select id_doc from inventario.candado) ");
                        sql.append("\n WHERE id =( SELECT  id_doc FROM inventario.candado_documentos ); ");

                        sql.append("\n UPDATE inventario.documentos "
                                + "set id_documento_movimiento = ( SELECT  id_doc FROM inventario.candado_documentos )"
                                + " WHERE id = (select id_doc from inventario.candado);");

                        cn.prepareStatementIDU(2, sql);
                        System.out.println(
                                "-- UPDATE FECHAS DOC DE TRASLADO -- " + ((LoggableStatement) cn.ps2).getQueryString());
                        cn.iduSQL(2);

                        limpiarCandados();
                    } else {
                        noticia = "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS 2";
                        return false;
                    }
                } else if (hacerEntradasInventario()) {
                    noticia = "DOCUMENTOS DE TRASLADOS FINALIZADOS CON EXITO";

                    sql.delete(0, sql.length());
                    sql.append("\n UPDATE inventario.documentos ");
                    sql.append(
                            "\n SET fecha_crea = (select fecha_crea from inventario.documentos where id = (select id_doc from inventario.candado)), ");
                    sql.append(
                            "\n   fecha_cierre = (select fecha_cierre from inventario.documentos where id = (select id_doc from inventario.candado)) ,"
                                    + " id_documento_movimiento = (select id_doc from inventario.candado) ");
                    sql.append("\n WHERE id =( SELECT  id_doc FROM inventario.candado_documentos ) ; ");

                    sql.append("\n UPDATE inventario.documentos "
                            + "set id_documento_movimiento = ( SELECT  id_doc FROM inventario.candado_documentos )"
                            + " WHERE id = (select id_doc from inventario.candado);");

                    cn.prepareStatementIDU(2, sql);
                    System.out.println(
                            "-- UPDATE FECHAS DOC DE TRASLADO -- " + ((LoggableStatement) cn.ps2).getQueryString());
                    cn.iduSQL(2);

                    limpiarCandados();
                } else {
                    noticia = "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS 2";
                    return false;
                }
            } else {
                noticia = "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS";
                limpiarCandados();
            }
            cn.fintransaccion();
        } catch (Exception localException) {
            System.out.println(
                    localException.getMessage() + " en método 2 verificaTransaccionesInventario(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean hacerEntradasInventarioFarmacia() {
        System.out.println(
                "-----------------------------------------------------------------------------------------\n HacerEntradasInventario::ID_DOC origen= "
                        + var1 + " id_BODEGA_destino=" + var6);

        boolean bool = true;
        String ide_us = "";
        try {
            sql.delete(0, sql.length());

            if (cn.getMotor().equals("sqlserver")) {
                sql.append(
                        "\n INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(ID)+1 FROM inventario.documentos ); ");
                sql.append(
                        "\n INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_estado, id_usuario_crea)  ");
                sql.append("\n VALUES ((SELECT id_doc FROM inventario.candado_documentos),?,?,?,?); ");
            } else {
                System.out.println("--POSTGRES B ");
                sql.append(
                        "\n INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(ID)+1 FROM inventario.documentos ); ");
                sql.append(
                        "\n INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_estado, id_usuario_crea)  ");
                sql.append(
                        "\n VALUES ((SELECT id_doc FROM inventario.candado_documentos)::integer,?::integer,?::integer,?,?); ");
            }
            ide_us = cn.usuario.getIdentificacion();
            while (ide_us.equals("")) {
                ide_us = cn.usuario.getIdentificacion();
            }
            cn.prepareStatementIDU(1, sql);
            cn.ps1.setString(1, "20");
            cn.ps1.setInt(2, var6.intValue());
            cn.ps1.setInt(3, 1);
            cn.ps1.setString(4, ide_us);

            if (cn.imprimirConsola) {
                System.out.println("--CREACION doc:n " + ((LoggableStatement) cn.ps1).getQueryString());
            }
            cn.iduSQL(1);

            sql.delete(0, sql.length());

            System.out.println("--POSTGRES B *********************************** ");

            sql.append("\n INSERT INTO INVENTARIO.TRANSACCIONES_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID, ");
            sql.append("\n   ID_DOCUMENTO, ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   CANTIDAD, ");
            sql.append("\n   VALOR_UNITARIO, ");
            sql.append("\n   IVA, ");
            sql.append("\n   VALOR_IMPUESTO, ");
            sql.append("\n   ID_NATURALEZA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   SERIAL, ");
            sql.append("\n   PRECIO_VENTA, ");
            sql.append("\n   FECHA_VENCIMIENTO, ");
            sql.append("\n   EXITO, ");
            sql.append("\n   valor_unitario_promedio, ");
            sql.append("\n   VALOR_TOTAL ");
            sql.append("\n ) ( ");
            sql.append("\n    SELECT ");
            sql.append("\n    tr.ID, ");
            sql.append("\n     ( SELECT ID_DOC FROM INVENTARIO.candado_documentos), ");
            sql.append("\n     tr.ID_ARTICULO, ");
            sql.append("\n     tr.CANTIDAD, ");
            sql.append("\n     tr.VALOR_UNITARIO, ");
            sql.append("\n     tr.IVA, ");
            sql.append("\n     tr.VALOR_IMPUESTO, ");
            sql.append("\n     tr.ID_NATURALEZA, ");
            sql.append("\n     tr.LOTE, ");
            sql.append("\n     tr.SERIAL, ");
            sql.append("\n     tr.PRECIO_VENTA, ");
            sql.append("\n     tr.FECHA_VENCIMIENTO, ");
            sql.append("\n     tr.EXITO, ");
            sql.append("\n     tr.valor_unitario_promedio, ");
            sql.append("\n     (tr.cantidad*(tr.valor_unitario+tr.valor_impuesto+documento.vlr_flete)) ");
            sql.append("\n   FROM INVENTARIO.TRANSACCIONES tr ");
            sql.append("\n   inner join ( ");
            sql.append("\n\t select (doc.valor_flete/sum (tr.cantidad)) vlr_flete, doc.id ");
            sql.append("\n\t from inventario.transacciones tr  ");
            sql.append("\n\t inner join inventario.documentos doc on tr.id_documento = doc.id ");
            sql.append("\n\t where doc.id =( SELECT id_doc FROM inventario.candado)::integer ");
            sql.append("\n\t group by doc.valor_flete, doc.id ");
            sql.append("\n   ) documento on tr.id_documento = documento.id ");
            sql.append("\n   WHERE tr.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO) ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.TRANSACCIONES ");
            sql.append("\n ( ");
            sql.append("\n   ID_DOCUMENTO, ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   CANTIDAD, ");
            sql.append("\n   VALOR_UNITARIO, ");
            sql.append("\n   IVA, ");
            sql.append("\n   VALOR_IMPUESTO, ");
            sql.append("\n   ID_NATURALEZA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   SERIAL, ");
            sql.append("\n   PRECIO_VENTA, ");
            sql.append("\n   FECHA_VENCIMIENTO, ");
            sql.append("\n   EXITO, ");
            sql.append("\n   valor_unitario_promedio ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n   SELECT  ");
            sql.append("\n     ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS), /* DOC CANDADO DESTINO */ ");
            sql.append("\n     ID_ARTICULO, ");
            sql.append("\n     CANTIDAD, ");
            sql.append("\n     VALOR_UNITARIO, ");
            sql.append("\n     IVA, ");
            sql.append("\n     VALOR_IMPUESTO, ");
            sql.append("\n     'I' ID_NATURALEZA, ");
            sql.append("\n     LOTE, ");
            sql.append("\n     SERIAL, ");
            sql.append("\n     PRECIO_VENTA, ");
            sql.append("\n     FECHA_VENCIMIENTO, ");
            sql.append("\n     EXITO, ");
            sql.append("\n     valor_unitario_promedio ");
            sql.append("\n   FROM  INVENTARIO.TRANSACCIONES_TMP  ");
            sql.append("\n   WHERE ID_DOCUMENTO = (SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   EXISTENCIA, ");
            sql.append("\n   FECHA_VENCIMIENTO ");
            sql.append("\n )  ");
            sql.append("\n  ( ");
            sql.append("\n      SELECT  ");
            sql.append("\n           TR.ID_ARTICULO, ");
            sql.append("\n           DOC.ID_BODEGA, ");
            sql.append("\n           TR.LOTE, ");
            sql.append(
                    "\n           CASE WHEN  BEL.EXISTENCIA IS NULL THEN TR.CANTIDAD ELSE   TR.CANTIDAD + BEL.EXISTENCIA END EXISTENCIAS, TR.FECHA_VENCIMIENTO ");
            sql.append("\n\t\tFROM ( ");
            sql.append("\n\t\t\t\tSELECT      ");
            sql.append("\n\t\t\t\t  id_documento, ");
            sql.append("\n\t\t\t\t  id_articulo, ");
            sql.append("\n\t\t\t\t  sum(cantidad) cantidad, ");
            sql.append("\n\t\t\t\t  lote  , fecha_vencimiento      ");
            sql.append("\n\t\t\t\tFROM inventario.transacciones_tmp ");
            sql.append("\n\t\t\t\tGROUP BY id_documento, id_articulo,lote, fecha_vencimiento ");
            sql.append("\n\t\t  ) TR ");

            sql.append("\n         INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n         LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n         WHERE DOC.ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n         AND TR.LOTE <> '' ");
            sql.append("\n ); ");

            sql.append("\n DELETE FROM  ");
            sql.append("\n   INVENTARIO.BODEGAS_EXISTENCIAS_LOTE  ");
            sql.append("\n WHERE  ID_ARTICULO IN ( SELECT TR.ID_ARTICULO ");
            sql.append("\n         FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n         INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n         LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n         WHERE DOC.ID =  ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n   ) ");
            sql.append("\n   AND ID_BODEGA IN (SELECT DOC.ID_BODEGA ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n     WHERE DOC.ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     ) ");
            sql.append("\n  AND LOTE IN (SELECT  TR.LOTE ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n     WHERE DOC.ID =  ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_LOTE ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   EXISTENCIA, ");
            sql.append("\n   FECHA_VENCIMIENTO ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n     SELECT        ");
            sql.append("\n       ID_ARTICULO, ");
            sql.append("\n       ID_BODEGA, ");
            sql.append("\n       LOTE, ");
            sql.append("\n       EXISTENCIA, ");
            sql.append("\n       FECHA_VENCIMIENTO ");
            sql.append("\n     FROM INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP   ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   EXISTENCIA ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n \tSELECT  ");
            sql.append("\n       TR.ID_ARTICULO, ");
            sql.append("\n       DOC.ID_BODEGA, ");
            sql.append(
                    "\n       CASE WHEN  BE.EXISTENCIA IS NULL THEN SUM(TR.CANTIDAD) ELSE SUM(TR.CANTIDAD) + BE.EXISTENCIA END EXISTENCIAS ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append("\n     WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     GROUP BY tr.id_articulo,doc.id_bodega,be.existencia ");
            sql.append("\n ); ");

            sql.append("\n DELETE FROM INVENTARIO.BODEGAS_EXISTENCIAS  ");
            sql.append("\n WHERE ID_ARTICULO IN (SELECT TR.ID_ARTICULO  ");
            sql.append("\n                       FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n                       INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n                       LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append(
                    "\n                       WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n                   ) ");
            sql.append("\n AND ID_BODEGA IN (SELECT DOC.ID_BODEGA ");
            sql.append("\n                   FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n                   INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n                   LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append(
                    "\n                   WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n                   ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   EXISTENCIA ");
            sql.append("\n )  ");
            sql.append("\n  ( ");
            sql.append("\n   SELECT  ");
            sql.append("\n     ID_ARTICULO, ");
            sql.append("\n     ID_BODEGA, ");
            sql.append("\n     EXISTENCIA ");
            sql.append("\n   FROM INVENTARIO.BODEGAS_EXISTENCIAS_TMP ");
            sql.append(" ); ");

            /* para traslados entradas */
            /* MOVIMIENTO DE inventarios */
            sql.append("\n INSERT INTO inventario.inventarios_tmp(  ");
            sql.append("\n  id_articulo, ");
            sql.append("\n  valor_unitario, ");
            sql.append("\n  precio_venta, ");
            sql.append("\n  existencia, ");
            sql.append("\n  valor_total ");
            sql.append("\n )(	  ");
            sql.append("\n	SELECT   ");
            sql.append("\n	  tr.id_articulo,   ");
            sql.append("\n	  ((tr.valor_total+invt.valor_total)/(tr.cantidad  + invt.existencia)), ");
            sql.append("\n	  tr.precio_venta, ");
            sql.append("\n	  tr.cantidad  + invt.existencia, ");
            sql.append("\n	  tr.valor_total+invt.valor_total  ");
            sql.append("\n	FROM ( ");
            sql.append("\n		SELECT  ");
            sql.append("\n		  trp.id_documento,	 ");
            sql.append("\n		  trp.id_articulo, ");
            sql.append("\n		  sum (trp.valor_unitario) valor_unitario, ");
            sql.append("\n		  0 precio_venta, ");
            sql.append("\n		  sum(trp.cantidad) cantidad,      ");
            sql.append("\n		  sum (trp.valor_total) valor_total ");
            sql.append("\n		FROM inventario.transacciones_tmp  trp   ");
            sql.append("\n		GROUP BY trp.id_articulo,  trp.id_documento ");
            sql.append("\n	) tr     ");
            sql.append("\n	INNER JOIN inventario.inventarios  invt ON tr.id_articulo = invt.id_articulo ");
            sql.append("\n	WHERE id_documento = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n );   ");

            /**/
            sql.append("\n	UPDATE inventario.transacciones_tmp  ");
            sql.append("\n	SET valor_unitario_promedio = inventario.inventarios_tmp.valor_unitario ");
            sql.append(
                    "\n	FROM inventario.inventarios_tmp WHERE inventario.inventarios_tmp.id_articulo = inventario.transacciones_tmp.id_articulo; ");

            /* actualizar transacciones */
            sql.append("\n	UPDATE inventario.transacciones  ");
            sql.append("\n	SET valor_unitario_promedio = inventario.transacciones_tmp.valor_unitario_promedio ");
            sql.append(
                    "\n	FROM inventario.transacciones_tmp WHERE inventario.transacciones_tmp.id = inventario.transacciones.id; ");

            /**/

            /* ELIMINAR DE LA TABLA LOS REGISTROS PARA ACTUALIZAR */
            sql.append("\n DELETE FROM inventario.inventarios  ");
            sql.append("\n WHERE id_articulo IN (SELECT  tr.id_articulo   ");
            sql.append("\n					  FROM inventario.transacciones_tmp tr ");
            sql.append(
                    "\n					  LEFT JOIN inventario.inventarios inv ON tr.id_articulo = inv.id_articulo  ");
            sql.append(
                    "\n					  WHERE id_documento = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n );  ");

            /* INSERTAR EN inventario */
            sql.append("\n INSERT INTO inventario.inventarios( ");
            sql.append("\n   id_articulo, ");
            sql.append("\n  valor_unitario, ");
            sql.append("\n  precio_venta, ");
            sql.append("\n  existencia, ");
            sql.append("\n  valor_total ");
            sql.append("\n ) ( ");
            sql.append("\n	SELECT  ");
            sql.append("\n	id_articulo, ");
            sql.append("\n	valor_unitario, ");
            sql.append("\n	precio_venta, ");
            sql.append("\n	existencia, ");
            sql.append("\n	valor_total ");
            sql.append("\n	FROM inventario.inventarios_tmp ");
            sql.append("\n );	 ");

            /* fin */
            ide_us = "";
            ide_us = cn.usuario.getIdentificacion();
            while (ide_us.equals("")) {
                ide_us = cn.usuario.getIdentificacion();
            }
            sql.append("\n UPDATE  INVENTARIO.DOCUMENTOS   ");
            sql.append("\n SET  ");
            sql.append("\n   FECHA_CIERRE = now(), ");
            sql.append("\n   ID_USUARIO_CIERRA = " + cn.usuario.getIdentificacion() + ", ");
            sql.append("\n   ID_ESTADO = 1 ");
            sql.append("\n WHERE ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer; ");

            cn.prepareStatementIDU(1, sql);
            if (cn.imprimirConsola) {
                System.out.println("--CREACION doc:n " + ((LoggableStatement) cn.ps1).getQueryString());
            }
            cn.iduSQL(1);

        } catch (SQLException localSQLException) {
            System.out.println(
                    localSQLException.getMessage() + " en método hacerEntradasInventario() de DocumentoHc.java");
            cn.exito = false;
        } catch (Exception localException) {
            System.out
                    .println(localException.getMessage() + " en método hacerEntradasInventario() de DocumentoHc.java");
            cn.exito = false;
        }

        return bool;
    }

    public boolean hacerEntradasInventario() {
        System.out.println(
                "-----------------------------------------------------------------------------------------\n HacerEntradasInventario::ID_DOC origen= "
                        + var1 + " id_BODEGA_destino=" + var6);

        boolean bool = true;
        String ide_us = "";
        try {
            sql.delete(0, sql.length());

            if (cn.getMotor().equals("sqlserver")) {
                sql.append(
                        "\n INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(ID)+1 FROM inventario.documentos ); ");
                sql.append(
                        "\n INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_estado, id_usuario_crea)  ");
                sql.append("\n VALUES ((SELECT id_doc FROM inventario.candado_documentos),?,?,?,?); ");
            } else {
                System.out.println("--POSTGRES B ");
                sql.append(
                        "\n INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(ID)+1 FROM inventario.documentos ); ");
                sql.append(
                        "\n INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_estado, id_usuario_crea)  ");
                sql.append(
                        "\n VALUES ((SELECT id_doc FROM inventario.candado_documentos)::integer,?::integer,?::integer,?,?); ");
            }
            ide_us = cn.usuario.getIdentificacion();
            while (ide_us.equals("")) {
                ide_us = cn.usuario.getIdentificacion();
            }
            cn.prepareStatementIDU(1, sql);
            cn.ps1.setString(1, "20");
            cn.ps1.setInt(2, var6.intValue());
            /* bodega destino */
            cn.ps1.setInt(3, 1);
            cn.ps1.setString(4, ide_us);

            if (cn.imprimirConsola) {
                System.out.println("--CREACION doc:n " + ((LoggableStatement) cn.ps1).getQueryString());
            }
            cn.iduSQL(1);

            sql.delete(0, sql.length());

            System.out.println("--POSTGRES B *********************************** ");

            sql.append("\n INSERT INTO INVENTARIO.TRANSACCIONES_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID, ");
            sql.append("\n   ID_DOCUMENTO, ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   CANTIDAD, ");
            sql.append("\n   VALOR_UNITARIO, ");
            sql.append("\n   IVA, ");
            sql.append("\n   VALOR_IMPUESTO, ");
            sql.append("\n   ID_NATURALEZA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   SERIAL, ");
            sql.append("\n   PRECIO_VENTA, ");
            sql.append("\n   FECHA_VENCIMIENTO, ");
            sql.append("\n   EXITO, ");
            sql.append("\n   valor_unitario_promedio, ");
            sql.append("\n   VALOR_TOTAL ");
            sql.append("\n ) ( ");
            sql.append("\n    SELECT ");
            sql.append("\n    tr.ID, ");
            sql.append("\n     ( SELECT ID_DOC FROM INVENTARIO.candado_documentos), ");
            sql.append("\n     tr.ID_ARTICULO, ");
            sql.append("\n     tr.CANTIDAD, ");
            sql.append("\n     tr.VALOR_UNITARIO, ");
            sql.append("\n     tr.IVA, ");
            sql.append("\n     tr.VALOR_IMPUESTO, ");
            sql.append("\n     tr.ID_NATURALEZA, ");
            sql.append("\n     tr.LOTE, ");
            sql.append("\n     tr.SERIAL, ");
            sql.append("\n     tr.PRECIO_VENTA, ");
            sql.append("\n     tr.FECHA_VENCIMIENTO, ");
            sql.append("\n     tr.EXITO, ");
            sql.append("\n     tr.valor_unitario_promedio, ");
            sql.append("\n     (tr.cantidad*(tr.valor_unitario+tr.valor_impuesto+documento.vlr_flete)) ");
            sql.append("\n   FROM INVENTARIO.TRANSACCIONES tr ");
            sql.append("\n   inner join ( ");
            sql.append("\n\t select (doc.valor_flete/sum (tr.cantidad)) vlr_flete, doc.id ");
            sql.append("\n\t from inventario.transacciones tr  ");
            sql.append("\n\t inner join inventario.documentos doc on tr.id_documento = doc.id ");
            sql.append("\n\t where doc.id =( SELECT id_doc FROM inventario.candado)::integer ");
            sql.append("\n\t group by doc.valor_flete, doc.id ");
            sql.append("\n   ) documento on tr.id_documento = documento.id ");
            sql.append("\n   WHERE tr.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO) ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.TRANSACCIONES ");
            sql.append("\n ( ");
            sql.append("\n   ID_DOCUMENTO, ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   CANTIDAD, ");
            sql.append("\n   VALOR_UNITARIO, ");
            sql.append("\n   IVA, ");
            sql.append("\n   VALOR_IMPUESTO, ");
            sql.append("\n   ID_NATURALEZA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   SERIAL, ");
            sql.append("\n   PRECIO_VENTA, ");
            sql.append("\n   FECHA_VENCIMIENTO, ");
            sql.append("\n   EXITO, ");
            sql.append("\n   valor_unitario_promedio ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n   SELECT  ");
            sql.append("\n     ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS), /* DOC CANDADO DESTINO */ ");
            sql.append("\n     ID_ARTICULO, ");
            sql.append("\n     CANTIDAD, ");
            sql.append("\n     VALOR_UNITARIO, ");
            sql.append("\n     IVA, ");
            sql.append("\n     VALOR_IMPUESTO, ");
            sql.append("\n     'I' ID_NATURALEZA, ");
            sql.append("\n     LOTE, ");
            sql.append("\n     SERIAL, ");
            sql.append("\n     PRECIO_VENTA, ");
            sql.append("\n     FECHA_VENCIMIENTO, ");
            sql.append("\n     EXITO, ");
            sql.append("\n     valor_unitario_promedio ");
            sql.append("\n   FROM  INVENTARIO.TRANSACCIONES_TMP  ");
            sql.append("\n   WHERE ID_DOCUMENTO = (SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   EXISTENCIA, ");
            sql.append("\n   FECHA_VENCIMIENTO ");
            sql.append("\n )  ");
            sql.append("\n  ( ");
            sql.append("\n      SELECT  ");
            sql.append("\n           TR.ID_ARTICULO, ");
            sql.append("\n           DOC.ID_BODEGA, ");
            sql.append("\n           TR.LOTE, ");
            sql.append(
                    "\n           CASE WHEN  BEL.EXISTENCIA IS NULL THEN TR.CANTIDAD ELSE   TR.CANTIDAD + BEL.EXISTENCIA END EXISTENCIAS, TR.FECHA_VENCIMIENTO ");
            sql.append("\n\t\tFROM ( ");
            sql.append("\n\t\t\t\tSELECT      ");
            sql.append("\n\t\t\t\t  id_documento, ");
            sql.append("\n\t\t\t\t  id_articulo, ");
            sql.append("\n\t\t\t\t  sum(cantidad) cantidad, ");
            sql.append("\n\t\t\t\t  lote  , fecha_vencimiento      ");
            sql.append("\n\t\t\t\tFROM inventario.transacciones_tmp ");
            sql.append("\n\t\t\t\tGROUP BY id_documento, id_articulo,lote, fecha_vencimiento ");
            sql.append("\n\t\t  ) TR ");

            sql.append("\n         INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n         LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n         WHERE DOC.ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n         AND TR.LOTE <> '' ");
            sql.append("\n ); ");

            sql.append("\n DELETE FROM  ");
            sql.append("\n   INVENTARIO.BODEGAS_EXISTENCIAS_LOTE  ");
            sql.append("\n WHERE  ID_ARTICULO IN ( SELECT TR.ID_ARTICULO ");
            sql.append("\n         FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n         INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n         LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n         WHERE DOC.ID =  ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n   ) ");
            sql.append("\n   AND ID_BODEGA IN (SELECT DOC.ID_BODEGA ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n     WHERE DOC.ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     ) ");
            sql.append("\n  AND LOTE IN (SELECT  TR.LOTE ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n     WHERE DOC.ID =  ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_LOTE ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   EXISTENCIA, ");
            sql.append("\n   FECHA_VENCIMIENTO ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n     SELECT        ");
            sql.append("\n       ID_ARTICULO, ");
            sql.append("\n       ID_BODEGA, ");
            sql.append("\n       LOTE, ");
            sql.append("\n       EXISTENCIA, ");
            sql.append("\n       FECHA_VENCIMIENTO ");
            sql.append("\n     FROM INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP   ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   EXISTENCIA ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n \tSELECT  ");
            sql.append("\n       TR.ID_ARTICULO, ");
            sql.append("\n       DOC.ID_BODEGA, ");
            sql.append(
                    "\n       CASE WHEN  BE.EXISTENCIA IS NULL THEN SUM(TR.CANTIDAD) ELSE SUM(TR.CANTIDAD) + BE.EXISTENCIA END EXISTENCIAS ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append("\n     WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     GROUP BY tr.id_articulo,doc.id_bodega,be.existencia ");
            sql.append("\n ); ");

            sql.append("\n DELETE FROM INVENTARIO.BODEGAS_EXISTENCIAS  ");
            sql.append("\n WHERE ID_ARTICULO IN (SELECT TR.ID_ARTICULO  ");
            sql.append("\n                       FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n                       INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n                       LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append(
                    "\n                       WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n                   ) ");
            sql.append("\n AND ID_BODEGA IN (SELECT DOC.ID_BODEGA ");
            sql.append("\n                   FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n                   INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n                   LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append(
                    "\n                   WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n                   ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   EXISTENCIA ");
            sql.append("\n )  ");
            sql.append("\n  ( ");
            sql.append("\n   SELECT  ");
            sql.append("\n     ID_ARTICULO, ");
            sql.append("\n     ID_BODEGA, ");
            sql.append("\n     EXISTENCIA ");
            sql.append("\n   FROM INVENTARIO.BODEGAS_EXISTENCIAS_TMP ");
            sql.append(" ); ");

            sql.append("\n UPDATE  INVENTARIO.DOCUMENTOS   ");
            sql.append("\n SET  ");
            sql.append("\n   FECHA_CIERRE = now(), ");
            sql.append("\n   ID_USUARIO_CIERRA = " + cn.usuario.getIdentificacion() + ", ");
            sql.append("\n   ID_ESTADO = 1 ");
            sql.append("\n WHERE ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer; ");

            cn.prepareStatementIDU(1, sql);
            if (cn.imprimirConsola) {
                System.out.println("--CREACION doc:n " + ((LoggableStatement) cn.ps1).getQueryString());
            }
            cn.iduSQL(1);

        } catch (SQLException localSQLException) {
            System.out.println(
                    localSQLException.getMessage() + " en método hacerEntradasInventario() de DocumentoHc.java");
            cn.exito = false;
        } catch (Exception localException) {
            System.out
                    .println(localException.getMessage() + " en método hacerEntradasInventario() de DocumentoHc.java");
            cn.exito = false;
        }

        return bool;
    }

    public boolean movimientoInventario() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());

            /* para traslados entradas */
            /* MOVIMIENTO DE inventarios */
            sql.append("\n INSERT INTO inventario.inventarios_tmp(  ");
            sql.append("\n  id_articulo, ");
            sql.append("\n  valor_unitario, ");
            sql.append("\n  precio_venta, ");
            sql.append("\n  existencia, ");
            sql.append("\n  valor_total ");
            sql.append("\n )(	  ");
            sql.append("\n	SELECT   ");
            sql.append("\n	  tr.id_articulo,   ");
            sql.append("\n	  ((tr.valor_total+invt.valor_total)/(tr.cantidad  + invt.existencia)), ");
            sql.append("\n	  tr.precio_venta, ");
            sql.append("\n	  tr.cantidad  + invt.existencia, ");
            sql.append("\n	  tr.valor_total+invt.valor_total  ");
            sql.append("\n	FROM ( ");
            sql.append("\n		SELECT  ");
            sql.append("\n		  trp.id_documento,	 ");
            sql.append("\n		  trp.id_articulo, ");
            sql.append("\n		  sum (trp.valor_unitario) valor_unitario, ");
            sql.append("\n		  0 precio_venta, ");
            sql.append("\n		  sum(trp.cantidad) cantidad,      ");
            sql.append("\n		  sum (trp.valor_total) valor_total ");
            sql.append("\n		FROM inventario.transacciones_tmp  trp   ");
            sql.append("\n		GROUP BY trp.id_articulo,  trp.id_documento ");
            sql.append("\n	) tr     ");
            sql.append("\n	INNER JOIN inventario.inventarios  invt ON tr.id_articulo = invt.id_articulo ");
            sql.append("\n	WHERE id_documento = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n );   ");

            /**/
            sql.append("\n	UPDATE inventario.transacciones_tmp  ");
            sql.append("\n	SET valor_unitario_promedio = inventario.inventarios_tmp.valor_unitario ");
            sql.append(
                    "\n	FROM inventario.inventarios_tmp WHERE inventario.inventarios_tmp.id_articulo = inventario.transacciones_tmp.id_articulo; ");

            /* actualizar transacciones */
            sql.append("\n	UPDATE inventario.transacciones  ");
            sql.append("\n	SET valor_unitario_promedio = inventario.transacciones_tmp.valor_unitario_promedio ");
            sql.append(
                    "\n	FROM inventario.transacciones_tmp WHERE inventario.transacciones_tmp.id = inventario.transacciones.id; ");

            /**/

            /* ELIMINAR DE LA TABLA LOS REGISTROS PARA ACTUALIZAR */
            sql.append("\n DELETE FROM inventario.inventarios  ");
            sql.append("\n WHERE id_articulo IN (SELECT  tr.id_articulo   ");
            sql.append("\n					  FROM inventario.transacciones_tmp tr ");
            sql.append(
                    "\n					  LEFT JOIN inventario.inventarios inv ON tr.id_articulo = inv.id_articulo  ");
            sql.append(
                    "\n					  WHERE id_documento = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n );  ");

            /* INSERTAR EN inventario */
            sql.append("\n INSERT INTO inventario.inventarios( ");
            sql.append("\n   id_articulo, ");
            sql.append("\n  valor_unitario, ");
            sql.append("\n  precio_venta, ");
            sql.append("\n  existencia, ");
            sql.append("\n  valor_total ");
            sql.append("\n ) ( ");
            sql.append("\n	SELECT  ");
            sql.append("\n	id_articulo, ");
            sql.append("\n	valor_unitario, ");
            sql.append("\n	precio_venta, ");
            sql.append("\n	existencia, ");
            sql.append("\n	valor_total ");
            sql.append("\n	FROM inventario.inventarios_tmp ");
            sql.append("\n );	 ");

            /* fin */
            cn.prepareStatementIDU(1, sql);
            System.out.println("\n -- ejecutar  movimientoInventario **************************** --");
            cn.iduSQL(1);

        } catch (SQLException localSQLException) {
            System.out
                    .println(localSQLException.getMessage() + " en método 1 movimientoInventario(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 movimientoInventario(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean cerrarDocumentoSolicitudesInventario() {
        System.out.println("ID_DOC= " + var2);
        boolean bool = true;
        esTraslado = "NO";
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();

                if (hacerSalidasInventario()) {

                    noticia = "DOCUMENTO FINALIZADO CON EXITO..";
                } else {
                    noticia = "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS";
                }
                limpiarCandados();
                cn.fintransaccion();
            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage()
                    + " en método 2 cerrarDocumentoSolicitudesInventario(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean finalizarDocumentoFactura() {

        if (verificaTransaccionesInventario()) {
            finalizarFactura();
        }

        return true;
    }

    public boolean verificarDetalleFactura() {
        System.out.println("-------VERIFICANDO ELEMENTOS DE FACTURA-------");
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS3, cn.traerElQuery(166));
                cn.ps3.setInt(1, var6);

                System.out.println(((LoggableStatement) cn.ps3).getQueryString());

                if (cn.selectSQL(Constantes.PS3)) {
                    while (cn.rs3.next()) {
                        if (cn.rs3.getBoolean("revision")) {
                            cn.cerrarPS(Constantes.PS3);
                            return true;
                        }
                    }
                }
            }
            return false;
        } catch (SQLException e) {
            System.out.println(
                    "Error --> Paciente.java-- function comprobarRecibo --> SQLException --> " + e.getMessage());
            return false;
        } catch (Exception e) {
            System.out
                    .println("Error --> Paciente.java-- function comprobarRecibo --> Exception --> " + e.getMessage());
            return false;
        }
    }

    public boolean verificarRecibo() {
        System.out.println("-------VERIFICANDO RECIBO DE FACTURA-------");
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS3, cn.traerElQuery(163));
                cn.ps3.setInt(1, var6);

                System.out.println(((LoggableStatement) cn.ps3).getQueryString());

                if (cn.selectSQL(Constantes.PS3)) {
                    while (cn.rs3.next()) {
                        if (cn.rs3.getBoolean("revision")) {
                            cn.cerrarPS(Constantes.PS3);
                            return true;
                        }
                        cn.cerrarPS(Constantes.PS3);
                        cn.iniciatransaccion();
                        System.out.println("--------RESTAURANDO RECIBO NO CREADO.--------");
                        cn.prepareStatementIDU(Constantes.PS1, cn.traerElQuery(164));
                        cn.ps1.setString(1, var3);
                        cn.ps1.setString(2, cn.usuario.getIdEmpresa());
                        cn.ps1.setInt(3, var6);


                        System.out.println(((LoggableStatement) cn.ps1).getQueryString());

                        cn.iduSQL(Constantes.PS1);
                        System.out.println("--------RECIBO CREADO.--------");
                        cn.fintransaccion();
                        return true;
                    }
                }
            }
            return false;
        } catch (SQLException e) {
            System.out.println(
                    "Error --> Paciente.java-- function comprobarRecibo --> SQLException --> " + e.getMessage());
            return false;
        } catch (Exception e) {
            System.out
                    .println("Error --> Paciente.java-- function comprobarRecibo --> Exception --> " + e.getMessage());
            return false;
        }
    }

    public boolean validarNumeracionFactura() {
        /*if (!verificarRecibo()) {
            return false;
        }*/

        if (!verificarDetalleFactura()) {
            return false;
        }
        return true;
    }

    public boolean finalizarFactura() {
        try {
            if (cn.isEstado()) {
                if (!validarNumeracionFactura()) {
                    System.out.println(
                            "---------------------ERROR AL VALIDAR FACTURA. NO SE PUEDE NUMERAR---------------------");
                    return false;
                }
                cn.iniciatransaccion();
                cn.prepareStatementIDU(Constantes.PS1, cn.traerElQuery(119));
                cn.ps1.setString(1, var3);
                cn.ps1.setInt(2, var6);
                cn.ps1.setInt(3, var6);
                cn.ps1.setString(4, var3);
                cn.ps1.setInt(5, var6);

                if (cn.imprimirConsola) {
                    System.out.println("finalizarFactura=\n " + ((LoggableStatement) cn.ps1).getQueryString());
                }

                cn.iduSQL(Constantes.PS1);

                cn.fintransaccion();

                //auditarFactura();
            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> clase Paciente --> function finalizarFactura --> SQLException --> "
                    + localSQLException.getMessage());
            return false;
        } catch (Exception localException) {
            System.out.println("Error --> clase Paciente --> function finalizarFactura --> Exception --> "
                    + localException.getMessage());
            return false;
        }
        return true;
    }

    public boolean cerrarRecibo() {
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                cn.prepareStatementIDU(Constantes.PS1, cn.traerElQuery(160));
                cn.ps1.setString(1, var3);
                cn.ps1.setInt(2, var6);
                cn.ps1.setInt(3, var6);

                if (cn.imprimirConsola) {
                    System.out.println("cerrarRecibo=\n " + ((LoggableStatement) cn.ps1).getQueryString());
                }

                cn.iduSQL(Constantes.PS1);

                auditarRecibo();

                cn.fintransaccion();
                return true;

            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> clase Paciente --> function cerrarRecibo --> SQLException --> "
                    + localSQLException.getMessage());
            return false;
        } catch (Exception localException) {
            System.out.println("Error --> clase Paciente --> function cerrarRecibo --> Exception --> "
                    + localException.getMessage());
            return false;
        }
        return true;
    }

    public void auditarRecibo() {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                Facturacion facturacion = new Facturacion();
                facturacion.setCn(cn);

                this.cn.prepareStatementSEL(Constantes.PS3, cn.traerElQuery(161));
                cn.ps3.setString(1, Integer.toString(var6));
                System.out.println(((LoggableStatement) cn.ps3).getQueryString());

                if (cn.selectSQL(Constantes.PS3)) {
                    if (cn.rs3 != null) {
                        if (cn.rs3.next()) {
                            if (cn.rs3.getBoolean("sw_auditar_automatico")) {
                                facturacion.auditarRecibo(Integer.toString(var6));
                            } else {
                                System.out.println("NO SE AUDITA EL RECIBO: " + var6);
                            }
                        }
                    }
                }
                cn.cerrarPS(Constantes.PS3);
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Paciente --> function auditarRecibo --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out
                    .println("Error --> clase Paciente --> function auditarRecibo --> Exception --> " + e.getMessage());
        }
    }

    public void auditarFactura() {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                Facturacion facturacion = new Facturacion();
                facturacion.setCn(cn);

                this.cn.prepareStatementSEL(Constantes.PS3, cn.traerElQuery(151));
                cn.ps3.setString(1, var6 + "");
                System.out.println(((LoggableStatement) cn.ps3).getQueryString());

                if (cn.selectSQL(Constantes.PS3)) {
                    if (cn.rs3 != null) {
                        while (cn.rs3.next()) {
                            if (cn.rs3.getString("valor").equals("AUDITAR")) {
                                facturacion.setIdFactura(var6 + "");
                                facturacion.auditarFactura();
                            }
                        }
                    }
                }

                cn.cerrarPS(Constantes.PS3);

                // AUDITAR COPAGO
                this.cn.prepareStatementSEL(Constantes.PS3, cn.traerElQuery(157));
                cn.ps3.setString(1, Integer.toString(var6));
                if (cn.selectSQL(Constantes.PS3)) {
                    if (cn.rs3 != null) {
                        if (cn.rs3.next()) {
                            facturacion.auditarRecibo(cn.rs3.getString("id_recibo"));
                        }
                    }
                }

                cn.cerrarPS(Constantes.PS3);
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Paciente --> function auditarFactura --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> clase Paciente --> function auditarFactura --> Exception --> " + e.getMessage());
        }
    }

    public boolean verificaTransaccionesInventario() {
        System.out.println("ID_DOC= " + var2);
        boolean bool = true;
        esTraslado = "NO";
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();

                if (hacerSalidasInventario()) {
                    System.out.println("tttttttttttttttttttt");
                    noticia = "DOCUMENTO FINALIZADO CON EXITO..";
                } else {
                    System.out.println("ffffffffffffffffffffff");
                    noticia = "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS";
                }
                limpiarCandados();
                cn.fintransaccion();
            }
        } catch (Exception localException) {
            System.out.println(
                    localException.getMessage() + " en método 2 verificaTransaccionesInventario(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean salidaTransaccionesInventario() {
        System.out.println("ID_DOC= " + var2);
        boolean bool = true;
        esTraslado = "SI";
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();

                if (hacerSalidasInventario()) {
                    noticia = "DOCUMENTO FINALIZADO CON EXITO..";
                } else {
                    noticia = "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS";
                }
                limpiarCandados();
                cn.fintransaccion();
            }
        } catch (Exception localException) {
            System.out.println(
                    localException.getMessage() + " en método 2 verificaTransaccionesInventario(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public int maximoValorTablaDocInventario() {
        int i = 0;
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(" SELECT id_doc FROM inventario.candado_documentos;");
                cn.prepareStatementSEL(2, localStringBuffer);
                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        idDocumento = Integer.valueOf(cn.rs2.getInt("id_doc"));
                        cn.cerrarPS(2);
                    }
                }
            }
        } catch (SQLException localSQLException) {
        } catch (Exception localException) {
        }

        if (i == 0) {
            i = 1;
        }
        return i;
    }

    public boolean moverBodegaLote() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());
            sql.append("\n  INSERT INTO inventario.bodegas_existencias_lote_tmp( ");
            sql.append("\n  id_articulo,");
            sql.append("\n  id_bodega,");
            sql.append("\n  lote,");
            sql.append("\n  existencia,");
            sql.append("\n  fecha_vencimiento");
            sql.append("\n)(");
            sql.append("\n   SELECT ");
            sql.append("\n\ttodo_b.id_articulo,");
            sql.append("\n\ttodo_b.id_bodega,");
            sql.append("\n\ttodo_b.lote,");
            sql.append("\n\tsum (todo_b.existencias),");
            sql.append("\n\ttodo_b.fecha_vencimiento");
            sql.append("\n\tfrom (");
            sql.append("\n\t\tSELECT ");
            sql.append("\n\t\t  tr.id_articulo,");
            sql.append("\n\t\t  doc.id_bodega,");
            sql.append("\n\t\t  tm.lote,");
            sql.append(
                    "\n\t\t  CASE WHEN  bel.existencia IS NULL THEN tm.cant_nue ELSE   tm.cant_nue + bel.existencia END existencias,");
            sql.append("\n\t\t  tm.fecha_vencimiento");
            sql.append("\n\t\tFROM inventario.transaccion_modifica tm ");
            sql.append("\n\t\tinner join inventario.transacciones tr on tm.id = tr.id");
            sql.append("\n\t\tinner join inventario.documentos doc on tr.id_documento = doc.id");
            sql.append(
                    "\n\t\tleft join inventario.bodegas_existencias_lote bel on tr.id_articulo = bel.id_articulo and tm.lote = bel.lote and doc.id_bodega = bel.id_bodega ");

            sql.append("\n\t) todo_b");
            sql.append(
                    "\n\tgroup by todo_b.id_articulo,    todo_b.id_bodega,    todo_b.lote,    todo_b.fecha_vencimiento");
            sql.append("\n );");

            sql.append("\nDELETE FROM inventario.bodegas_existencias_lote ");
            sql.append("\nWHERE  id_articulo IN ( SELECT tr.id_articulo  ");
            sql.append("\n\t\tFROM inventario.transacciones tr");
            sql.append("\n\t\tinner join inventario.transaccion_modifica tm on tr.id = tm.id");
            sql.append("\n\t\tINNER JOIN inventario.documentos doc ON tr.id_documento = doc.ID");
            sql.append(
                    "\n\t\tLEFT JOIN inventario.bodegas_existencias_lote bel ON ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)");
            sql.append("\n  )");
            sql.append("\n  AND id_bodega IN (SELECT doc.id_bodega");
            sql.append("\n\tFROM inventario.transacciones tr");
            sql.append("\n\tinner join inventario.transaccion_modifica tm on tr.id = tm.id");
            sql.append("\n\tINNER JOIN inventario.documentos doc ON tr.id_documento = doc.ID");
            sql.append(
                    "\n\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel ON ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)");
            sql.append("\n\t)");
            sql.append("\n AND lote IN (");
            sql.append("\n\tSELECT  tm.lote");
            sql.append("\n\tFROM inventario.transacciones tr");
            sql.append("\n\tinner join inventario.transaccion_modifica tm on tr.id = tm.id");
            sql.append("\n\tINNER JOIN inventario.documentos doc ON tr.id_documento = doc.ID");
            sql.append(
                    "\n\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel ON ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tm.lote = bel.lote)");
            sql.append("\n\t); ");

            sql.append("\nINSERT INTO inventario.bodegas_existencias_lote ( ");
            sql.append("\n  id_articulo,");
            sql.append("\n  id_bodega,");
            sql.append("\n  lote,");
            sql.append("\n  existencia,");
            sql.append("\n  FECHA_VENCIMIENTO");
            sql.append("\n)(");
            sql.append("\n\tSELECT     ");
            sql.append("\n\t  id_articulo,");
            sql.append("\n\t  id_bodega,");
            sql.append("\n\t  lote,");
            sql.append("\n\t  existencia,");
            sql.append("\n\t  FECHA_VENCIMIENTO");
            sql.append("\n\tFROM inventario.bodegas_existencias_lote_tmp  ");
            sql.append("\n );");

            sql.append("\nUPDATE inventario.bodegas_existencias_lote ");
            sql.append("\nSET existencia = trans.existencia");

            sql.append("\nFROM (");
            sql.append("\n select ");
            sql.append("\n\t  case when bel.existencia - tm.cant_nue < 0 then 0");
            sql.append("\n\t  else bel.existencia - tm.cant_nue end existencia,");
            sql.append("\n\t  tm.lote_ant, tr.id_articulo , doc.id_bodega");
            sql.append("\n  from inventario.transaccion_modifica tm ");
            sql.append("\n inner join inventario.transacciones tr on tm.id = tr.id");
            sql.append("\n  inner join inventario.documentos doc on tr.id_documento = doc.id");
            sql.append(
                    "\n  inner join inventario.bodegas_existencias_lote bel on tr.id_articulo = bel.id_articulo and doc.id_bodega = bel.id_bodega and tm.lote_ant = bel.lote");
            sql.append("\n) trans");
            sql.append("\nwhere inventario.bodegas_existencias_lote.lote = trans.lote_ant");
            sql.append("\nAND inventario.bodegas_existencias_lote.id_articulo = trans.id_articulo ");
            sql.append("\nAND inventario.bodegas_existencias_lote.id_bodega = trans.id_bodega ;");

            sql.append("\n delete from inventario.bodegas_existencias_lote_tmp; ");

            cn.prepareStatementIDU(1, sql);
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 moverBodegaLote(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 moverBodegaLote(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean cambiarLote() {
        boolean bool = true;
        int i = 0;
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());

                sql.append("\n select (bel.existencia - tm.cant_nue ) cantidad ");
                sql.append("\n from inventario.transaccion_modifica tm ");
                sql.append("\n inner join inventario.transacciones tr on tm.id = tr.id ");
                sql.append("\n inner join inventario.documentos doc on tr.id_documento = doc.id ");
                sql.append(
                        "\n inner join inventario.bodegas_existencias_lote bel on tr.id_articulo = bel.id_articulo and tm.lote_ant = bel.lote and doc.id_bodega = bel.id_bodega ; ");

                cn.prepareStatementSEL(2, sql);
                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        System.out.println("--Result- =" + cn.rs2.getString("cantidad"));
                        i = cn.rs2.getInt("cantidad");
                    }
                    cn.cerrarPS(2);
                }
                if (i >= 0) {
                    if (moverBodegaLote()) {
                        System.out.println("--Se realizo el cambio de lote  ");
                    }
                } else {
                    System.out.println("--No se puede realizar por control de existencias  ");
                    return false;
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 cambiarLote(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 cambiarLote(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean loteTransaccion() {
        boolean bool = true;
        String str = "";
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                sql.append(
                        "\n select case when  tm.lote = tm.lote_ant then 'IGUAL' else 'DIF' END mod_lote from inventario.transaccion_modifica tm ; ");
                cn.prepareStatementSEL(2, sql);
                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        System.out.println("--Result- =" + cn.rs2.getString("mod_lote"));
                        if (cn.rs2.getString("mod_lote").equals("IGUAL")) {
                            bool = true;
                            str = "IGUAL";
                        }
                        if (cn.rs2.getString("mod_lote").equals("DIF")) {
                            bool = true;
                            str = "DIF";
                        }
                    }
                    cn.cerrarPS(2);
                }

                if (str.equals("DIF")) {
                    if (cambiarLote()) {
                        System.out.println(" - *** Realizo el cambio de lotes en bodegas **  -");
                    } else {
                        System.out.println(" - *** no hay cambio en lotes **  -");
                        bool = false;
                    }
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 loteTransaccion(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 loteTransaccion(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean exitoTransaccion() {
        boolean bool = false;
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                sql.append("\n SELECT exito FROM inventario.transacciones_tmp WHERE exito = 'N'");
                cn.prepareStatementSEL(3, sql);
                System.out.println("--3 =" + ((LoggableStatement) cn.ps3).getQueryString());

                if (cn.selectSQL(3)) {
                    while (cn.rs3.next()) {

                        if (cn.rs3.getString("exito").equals("N")) {
                            bool = true;
                        }
                    }
                    cn.cerrarPS(3);
                    System.out.println("--Result- =" + cn.rs3.getString("exito"));
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 exitoTransaccion(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 exitoTransaccion(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean salidaCantidad() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());

            sql.append("\n UPDATE inventario.bodegas_existencias_lote ");
            sql.append("\n SET  fecha_vencimiento = inventario.transacciones_tmp.fecha_vencimiento");
            sql.append("\n FROM inventario.transacciones_tmp");
            sql.append(
                    "\n WHERE inventario.bodegas_existencias_lote.id_articulo = inventario.transacciones_tmp.id_articulo");
            sql.append("\n AND inventario.bodegas_existencias_lote.lote = inventario.transacciones_tmp.lote ;");

            sql.append("\n INSERT INTO  inventario.bodegas_existencias_lote_tmp\t ");
            sql.append("\n  (  id_articulo,id_bodega,lote,existencia,fecha_vencimiento  )(\t ");
            sql.append("\n\t  SELECT tr.id_articulo, tr.id_bodega,tr.lote,\t");
            sql.append("\n\t\t  CASE WHEN  bel.existencia IS NULL THEN - tr.resta\t");
            sql.append("\n\t\t  WHEN tr.resta > bel.existencia THEN bel.existencia - tr.resta\t");
            sql.append("\n\t\t\tELSE bel.existencia - tr.resta END existencias,\t");
            sql.append("\n\t\t\ttr.fecha_vencimiento\t");
            sql.append(
                    "\n\t  FROM (  \tSELECT t.id_articulo, doc.id_bodega , t.lote, sum(t.cantidad) cantidad, t.fecha_vencimiento, doc.id, (tm.cant_ant-tm.cant_nue) resta ");
            sql.append("\n\t\t\t\t from inventario.transacciones_tmp t ");
            sql.append("\n\t\t\t  INNER JOIN inventario.documentos doc on t.id_documento = doc.id");
            sql.append("\n\t\t\t  INNER JOIN inventario.transaccion_modifica tm on t.id = tm.id\t");
            sql.append(
                    "\n\t\t\t  GROUP by  t.id_articulo, doc.id_bodega , t.lote, t.fecha_vencimiento, doc.id, tm.cant_nue, tm.cant_ant");
            sql.append("\n\t  ) tr         ");
            sql.append(
                    "\n\t  LEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND tr.id_bodega = bel.id_bodega AND tr.lote = bel.lote)");
            sql.append("\n\t  WHERE tr.lote <> ''");
            sql.append("\n\t);\t");

            sql.append("\n\tDELETE FROM inventario.bodegas_existencias_lote ");
            sql.append("\n\tWHERE  id_articulo IN (     SELECT  tr.id_articulo\t");
            sql.append("\n\t\t\tFROM  inventario.transacciones tr\t\t");
            sql.append("\n\t\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
            sql.append(
                    "\n\t\t\tLEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
            sql.append("\n\t\t\tWHERE tr.id =  ( SELECT id FROM inventario.transaccion_modifica)::integer\t");
            sql.append("\n\t)\t");
            sql.append("\n\t  AND id_bodega IN (SELECT   doc.id_bodega\t");
            sql.append("\n\t\tFROM   inventario.transacciones tr\t");
            sql.append("\n\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
            sql.append(
                    "\n\t\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
            sql.append("\n\t\tWHERE tr.id =  ( SELECT id FROM inventario.transaccion_modifica)::integer\t");
            sql.append("\n\t\t)\t");
            sql.append("\n\t AND lote IN (\t");
            sql.append("\n\t\tSELECT    tr.lote\t");
            sql.append("\n\t\tFROM   inventario.transacciones tr\t");
            sql.append("\n\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
            sql.append(
                    "\n\t\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
            sql.append("\n\t   WHERE tr.id =  ( SELECT id FROM inventario.transaccion_modifica)::integer");
            sql.append("\n\t\t);\t");

            sql.append("\n INSERT INTO inventario.bodegas_existencias_lote\t");
            sql.append("\n (  id_articulo, id_bodega,lote,existencia,fecha_vencimiento )(\t");
            sql.append("\n    SELECT id_articulo, id_bodega,lote, existencia, fecha_vencimiento\t");
            sql.append("\n   FROM  inventario.bodegas_existencias_lote_tmp\t");
            sql.append("\n );\t");

            sql.append("\n INSERT INTO inventario.bodegas_existencias_tmp\t");
            sql.append("\n (  id_articulo,id_bodega, existencia )\t(\t");
            sql.append("\n    SELECT tr.id_articulo, doc.id_bodega,\t");
            sql.append("\n      CASE WHEN  be.existencia IS NULL THEN - sum ((tm.cant_ant-tm.cant_nue))");
            sql.append(
                    "\n        WHEN sum ((tm.cant_ant-tm.cant_nue) ) > be.existencia THEN be.existencia - sum ((tm.cant_ant-tm.cant_nue))\t");
            sql.append("\n      ELSE be.existencia - sum ((tm.cant_ant-tm.cant_nue) ) END existencias\t");
            sql.append("\n    FROM  inventario.transacciones_tmp tr\t");
            sql.append("\n    INNER JOIN inventario.documentos doc on tr.id_documento = doc.id");
            sql.append(
                    "\n    LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
            sql.append("\n    inner join inventario.transaccion_modifica tm on tr.id = tm.id");
            sql.append("\n    group by tr.id_articulo,doc.id_bodega, be.existencia, tm.cant_nue,tm.cant_ant\t");
            sql.append("\n );\t");

            sql.append("\n DELETE FROM inventario.bodegas_existencias\t");
            sql.append("\n WHERE id_articulo IN (SELECT tr.id_articulo FROM inventario.transacciones_tmp tr\t");
            sql.append("\n                      INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
            sql.append(
                    "\n                      LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");

            sql.append("\n                  )\t");
            sql.append("\n AND id_bodega IN (SELECT doc.id_bodega FROM  inventario.transacciones_tmp tr");
            sql.append("\n                  INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
            sql.append(
                    "\n                  LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");

            sql.append("\n                  );\t");

            sql.append("\n INSERT INTO inventario.bodegas_existencias\t");
            sql.append("\n (  id_articulo, id_bodega, existencia )\t");
            sql.append("\n (\t");
            sql.append("\n  SELECT id_articulo,id_bodega,existencia\t");
            sql.append("\n  FROM inventario.bodegas_existencias_tmp\t");
            sql.append("\n );\t");

            sql.append("\n DELETE FROM inventario.bodegas_existencias_tmp ;\t");
            sql.append("\n DELETE FROM inventario.bodegas_existencias_lote_tmp ;");

            cn.prepareStatementIDU(1, sql);
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 salidaCantidad(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 salidaCantidad(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean restarTransaccion() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());

            sql.append("\n INSERT INTO inventario.transacciones_tmp ( ");
            sql.append("\n  id, ");
            sql.append("\n   id_documento, ");
            sql.append("\n   id_articulo, ");
            sql.append("\n   cantidad, ");
            sql.append("\n   valor_unitario, ");
            sql.append("\n   iva, ");
            sql.append("\n   valor_impuesto, ");
            sql.append("\n   id_naturaleza, ");
            sql.append("\n   lote, ");
            sql.append("\n   precio_venta, ");
            sql.append("\n   fecha_vencimiento, ");
            sql.append("\n   exito,  ");
            sql.append("\n   valor_total ");
            sql.append("\n  )( ");
            sql.append("\n  SELECT ");
            sql.append("\n\t\ttr.id, ");
            sql.append("\n\t\ttr.id_documento, ");
            sql.append("\n\t\ttr.id_articulo, ");
            sql.append("\n\t\ttm.cant_nue, ");
            sql.append("\n\t\ttm.valor_unitario, ");
            sql.append("\n\t\ttm.iva, ");
            sql.append("\n\t\t(tm.valor_unitario * tm.iva /100 ), ");
            sql.append("\n\t\ttr.id_naturaleza, ");
            sql.append("\n\t\ttr.lote, ");
            sql.append("\n\t\ttr.precio_venta, ");
            sql.append("\n\t\ttr.fecha_vencimiento, ");
            sql.append("\n\t\t'S' exito, ");
            sql.append(
                    "\n\t\t(tm.cant_nue*(documento.vlr_flete + tm.valor_unitario+(tm.valor_unitario * tm.iva /100 ))) ");
            sql.append("\n\t\t FROM inventario.transacciones tr ");
            sql.append("\n\t\tinner join ( ");
            sql.append(
                    "\n\t\t  select \t(transaccion.valor_flete / (sum(tr.cantidad)-dif)) vlr_flete, tr.id_documento id ");
            sql.append("\n\t\tfrom inventario.transacciones tr ");
            sql.append("\n\t\t, ( ");
            sql.append("\n\t\t\tselect t.id, t.id_documento, (tmo.cant_ant - tmo.cant_nue) dif , doc.valor_flete ");
            sql.append(
                    "\n\t\t\tfrom inventario.transacciones t inner join inventario.transaccion_modifica tmo on t.id = tmo.id ");
            sql.append("\n\t\t\tinner join inventario.documentos doc on t.id_documento = doc.id ");
            sql.append("\n\t\t) transaccion  ");
            sql.append("\n\t\twhere tr.id_documento = transaccion.id_documento ");
            sql.append("\n\t\tgroup by tr.id_documento, transaccion.dif, transaccion.valor_flete ");
            sql.append("\n  ) documento on tr.id_documento = documento.id ");
            sql.append("\n  INNER JOIN inventario.transaccion_modifica tm on tr.id = tm.id ");
            sql.append("\n  WHERE tr.id = tm.id ");
            sql.append("\n ); ");

            sql.append("\n UPDATE  inventario.transacciones_tmp ");
            sql.append("\n SET exito = 'N'  ");
            sql.append("\n WHERE id IN (  ");
            sql.append("\n  SELECT  cantidad.id_transaccion   ");
            sql.append("\n  FROM (  ");
            sql.append("\n\tSELECT  ");
            sql.append("\n\t   tr.id id_transaccion, ");
            sql.append("\n\t   case when  bel.existencia is null then -tr.cantidad ");
            sql.append("\n\t\t when tr.cantidad > bel.existencia then bel.existencia - tr.cantidad ");
            sql.append("\n\t\t else bel.existencia - tr.cantidad  ");
            sql.append("\n\t   end existencias ");
            sql.append("\n\t FROM inventario.transacciones_tmp tr ");
            sql.append("\n\t inner join inventario.documentos doc on tr.id_documento = doc.id ");
            sql.append(
                    "\n\t left join inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo and doc.id_bodega = bel.id_bodega and tr.lote = bel.lote) ");
            sql.append("\n\t where tr.lote <> ''  ");
            sql.append("\n   ) as cantidad ");
            sql.append("\n   WHERE cantidad.existencias < 0 ");
            sql.append("\n); ");

            sql.append("\n update inventario.transacciones_tmp ");
            sql.append("\n set exito = 'N' ");
            sql.append("\n WHERE id in (     ");
            sql.append("\n select  ");
            sql.append("\n\t cantidad.id    ");
            sql.append("\n   from( ");
            sql.append("\n\t   select ");
            sql.append("\n\t\t tr.id,    ");
            sql.append("\n\t\t tr.id_articulo, ");
            sql.append("\n\t\t doc.id_bodega, ");
            sql.append("\n\t\t case when  be.existencia is null then tr.cantidad ");
            sql.append("\n\t\t   when tr.cantidad > be.existencia then be.existencia - tr.cantidad ");
            sql.append("\n\t\t else be.existencia - tr.cantidad  end existencias ");
            sql.append("\n\t   FROM  inventario.transacciones_tmp tr ");
            sql.append("\n\t   inner join inventario.documentos doc on tr.id_documento = doc.id ");
            sql.append(
                    "\n\t   left join inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo and doc.id_bodega = be.id_bodega) ");
            sql.append("\n\t   where tr.id = ( select id from inventario.transaccion_modifica)::integer ");
            sql.append("\n\t   and tr.lote = '' ");
            sql.append("\n   ) as cantidad   ");
            sql.append("\n   where cantidad.existencias < 0 ");
            sql.append("\n );");

            sql.append("\n UPDATE inventario.transacciones set  exito = 'N'  ");
            sql.append(
                    "\n where id in ( SELECT id FROM inventario.transacciones_tmp where id = ( select id from inventario.transaccion_modifica) and exito = 'N'); ");

            cn.prepareStatementIDU(1, sql);
            cn.iduSQL(1);

            if (!exitoTransaccion()) {
                if (salidaCantidad()) {
                    System.out.println("\n -- resto transaccion ****************************** --");
                } else {
                    System.out.println("\n -- no se realizo la salida de cantidades -- EN restarTransaccion");
                    bool = false;
                }
            } else {
                System.out.println("\n -- No se puede realizar por control de existencias -- EN restarTransaccion");
                bool = false;
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 restarTransaccion(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 restarTransaccion(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean sumarTransaccion() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());

            sql.append("\n  INSERT INTO inventario.transacciones_tmp ");
            sql.append("  ( ");
            sql.append("\n\tid,");
            sql.append("\n\tid_documento,");
            sql.append("\n\tid_articulo,");
            sql.append("\n\tcantidad,");
            sql.append("\n\tvalor_unitario,");
            sql.append("\n\tiva,");
            sql.append("\n\tvalor_impuesto,");
            sql.append("\n\tid_naturaleza,");
            sql.append("\n\tlote,");
            sql.append("\n\tprecio_venta,");
            sql.append("\n\tfecha_vencimiento,");
            sql.append("\n\texito, ");
            sql.append("\n\tvalor_total");
            sql.append(" \n )(");
            sql.append("\n\tSELECT ");
            sql.append("\n\t  tr.id, ");
            sql.append("\n\t  tr.id_documento, ");
            sql.append("\n\t  tr.id_articulo, ");
            sql.append("\n\t  tm.cant_nue, ");
            sql.append("\n\t  tm.valor_unitario, ");
            sql.append("\n\t  tm.iva, ");
            sql.append("\n\t  (tm.valor_unitario * tm.iva /100 ), ");
            sql.append("\n\t  tr.id_naturaleza, ");
            sql.append("\n\t  tr.lote, ");
            sql.append("\n\t  tr.precio_venta, ");
            sql.append("\n\t  tr.fecha_vencimiento, ");
            sql.append("\n\t  tr.exito, ");
            sql.append(
                    "\n\t  ((tm.cant_nue)*(documento.vlr_flete + tm.valor_unitario+(tm.valor_unitario * tm.iva /100 ))) ");
            sql.append("\n\tFROM inventario.transacciones tr ");
            sql.append("\n\tinner join ( ");
            sql.append(
                    "\n\t  select \t(transaccion.valor_flete / (sum(tr.cantidad)-dif)) vlr_flete, tr.id_documento  id ");
            sql.append("\n\t  from inventario.transacciones tr ");
            sql.append("\n\t  , ( ");
            sql.append("\n\t\t  select t.id, t.id_documento, (tmo.cant_ant - tmo.cant_nue) dif , doc.valor_flete ");
            sql.append(
                    "\n\t\t  from inventario.transacciones t inner join inventario.transaccion_modifica tmo on t.id = tmo.id ");
            sql.append("\n\t\t  inner join inventario.documentos doc on t.id_documento = doc.id ");
            sql.append("\n\t  ) transaccion  ");
            sql.append("\n\t  where tr.id_documento = transaccion.id_documento ");
            sql.append("\n\t  group by tr.id_documento, transaccion.dif, transaccion.valor_flete ");
            sql.append("\n\t) documento on tr.id_documento = documento.id  ");
            sql.append("\n\tINNER JOIN inventario.transaccion_modifica tm on tr.id = tm.id ");
            sql.append("\n\tWHERE tr.id = tm.id ");
            sql.append("\n ); ");

            sql.append("\n UPDATE inventario.bodegas_existencias_lote ");
            sql.append("\n SET  fecha_vencimiento = inventario.transacciones_tmp.fecha_vencimiento");
            sql.append("\n FROM inventario.transacciones_tmp");
            sql.append(
                    "\n WHERE inventario.bodegas_existencias_lote.id_articulo = inventario.transacciones_tmp.id_articulo");
            sql.append("\n AND inventario.bodegas_existencias_lote.lote = inventario.transacciones_tmp.lote ; ");

            sql.append("\n INSERT INTO inventario.bodegas_existencias_lote_tmp ");
            sql.append("\n  (");
            sql.append("\n\tid_articulo,");
            sql.append("\n\tid_bodega,");
            sql.append("\n\tlote,");
            sql.append("\n\texistencia,");
            sql.append("\n\tfecha_vencimiento");
            sql.append("\n  )(");
            sql.append("\n\t SELECT ");
            sql.append("\n\t  todo_b.id_articulo,");
            sql.append("\n\t  todo_b.id_bodega,");
            sql.append("\n\t  todo_b.lote,");
            sql.append("\n\t  sum (todo_b.existencias),");
            sql.append("\n\t  todo_b.fecha_vencimiento");
            sql.append("\n\t  from (");
            sql.append("\n\t\t  SELECT ");
            sql.append("\n\t\t\t  tr.id_articulo,");
            sql.append("\n\t\t\t  doc.id_bodega,");
            sql.append("\n\t\t\t  tr.lote,   ");
            sql.append(
                    "\n\t\t\t  CASE WHEN  bel.existencia IS NULL THEN tr.cantidad ELSE   (tm.cant_nue-tm.cant_ant) + bel.existencia END existencias, ");
            sql.append("\n\t\t\t  tr.fecha_vencimiento");
            sql.append("\n\t\t  FROM inventario.transacciones_tmp tr");
            sql.append("\n\t\t  INNER JOIN inventario.transaccion_modifica tm on tr.id = tm.id");
            sql.append("\n\t\t  INNER JOIN inventario.documentos doc ON tr.id_documento = doc.ID");
            sql.append(
                    "\n\t\t  LEFT JOIN inventario.bodegas_existencias_lote bel ON ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)");
            sql.append("\n\t\t  WHERE  tr.lote <> ''   ");
            sql.append("\n\t  ) todo_b");
            sql.append(
                    "\n\t  group by todo_b.id_articulo,    todo_b.id_bodega,    todo_b.lote,    todo_b.fecha_vencimiento");
            sql.append("\n );");

            sql.append("\n  DELETE FROM inventario.bodegas_existencias_lote ");
            sql.append("\n  WHERE  id_articulo IN ( SELECT tr.id_articulo     ");
            sql.append("\n\t\t  FROM inventario.transacciones_tmp tr");
            sql.append("\n\t\t  INNER JOIN inventario.documentos doc ON tr.id_documento = doc.ID");
            sql.append(
                    "\n\t\t  LEFT JOIN inventario.bodegas_existencias_lote bel ON ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)");
            sql.append("\n\t\t  WHERE tr.ID = ( SELECT id FROM inventario.transaccion_modifica)::integer");
            sql.append("\n\t)");
            sql.append("\n\tAND id_bodega IN (SELECT doc.id_bodega");
            sql.append("\n\t  FROM inventario.transacciones_tmp tr");
            sql.append("\n\t  INNER JOIN inventario.documentos doc ON tr.id_documento = doc.ID");
            sql.append(
                    "\n\t  LEFT JOIN inventario.bodegas_existencias_lote_tmp bel ON ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)");
            sql.append("\n\t  WHERE tr.ID = ( SELECT id FROM inventario.transaccion_modifica)::integer");
            sql.append("\n\t  )");
            sql.append("\n   AND lote IN (");
            sql.append("\n\t  SELECT  tr.lote");
            sql.append("\n\t  FROM inventario.transacciones_tmp tr");
            sql.append("\n\t  INNER JOIN inventario.documentos doc ON tr.id_documento = doc.ID");
            sql.append(
                    "\n\t  LEFT JOIN inventario.bodegas_existencias_lote_tmp bel ON ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)");
            sql.append("\n\t  WHERE tr.ID = ( SELECT id FROM inventario.transaccion_modifica)::integer");
            sql.append("\n\t  );");

            sql.append("\n  INSERT INTO inventario.bodegas_existencias_lote");
            sql.append("\n (");
            sql.append("\n\tid_articulo,");
            sql.append("\n\tid_bodega,");
            sql.append("\n\tlote,");
            sql.append("\n\texistencia,");
            sql.append("\n\tFECHA_VENCIMIENTO");
            sql.append("\n  )(");
            sql.append("\n\t  SELECT       ");
            sql.append("\n\t\tid_articulo,");
            sql.append("\n\t\tid_bodega,");
            sql.append("\n\t\tlote,");
            sql.append("\n\t\texistencia,");
            sql.append("\n\t\tFECHA_VENCIMIENTO");
            sql.append("\n\t  FROM inventario.bodegas_existencias_lote_tmp  ");
            sql.append("\n  );");

            sql.append("\n INSERT INTO inventario.bodegas_existencias_tmp (");
            sql.append("\n\tid_articulo,");
            sql.append("\n\tid_bodega,");
            sql.append("\n\texistencia");
            sql.append("\n  )(");
            sql.append("\n\t  SELECT ");
            sql.append("\n\t\ttr.id_articulo,");
            sql.append("\n\t\tdoc.id_bodega,");
            sql.append(
                    "\n\t\tCASE WHEN  be.existencia IS NULL THEN SUM( tr.cantidad ) ELSE SUM( tm.cant_nue-tm.cant_ant ) + be.existencia END existencias");
            sql.append("\n\t  FROM inventario.transacciones_tmp tr");
            sql.append("\n\t  INNER JOIN inventario.transaccion_modifica tm on tr.id = tm.id ");
            sql.append("\n\t  INNER JOIN inventario.documentos doc ON tr.id_documento = doc.ID");
            sql.append(
                    "\n\t  LEFT JOIN inventario.bodegas_existencias be ON (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)");

            sql.append("\n\t  GROUP BY tr.id_articulo,doc.id_bodega,be.existencia");
            sql.append("\n );");

            sql.append("\n  DELETE FROM inventario.bodegas_existencias  ");
            sql.append("\n  WHERE id_articulo IN (SELECT tr.id_articulo   ");
            sql.append("\n\t\t\t\t\t\tFROM inventario.transacciones_tmp tr");
            sql.append("\n\t\t\t\t\t\tINNER JOIN inventario.documentos doc ON tr.id_documento = doc.ID");
            sql.append(
                    "\n\t\t\t\t\t\tLEFT JOIN inventario.bodegas_existencias be ON (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)");

            sql.append("\n\t\t\t\t\t)");
            sql.append("\n  AND id_bodega IN (SELECT doc.id_bodega");
            sql.append("\n\t\t\t\t\tFROM inventario.transacciones_tmp tr");
            sql.append("\n\t\t\t\t\tINNER JOIN inventario.documentos doc ON tr.id_documento = doc.ID");
            sql.append(
                    "\n\t\t\t\t\tLEFT JOIN inventario.bodegas_existencias be ON (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)");

            sql.append("\n\t\t\t\t\t);");

            sql.append("\n  INSERT INTO inventario.bodegas_existencias");
            sql.append("\n  (");
            sql.append("\n\tid_articulo,");
            sql.append("\n\tid_bodega,");
            sql.append("\n\texistencia");
            sql.append("\n  )(");
            sql.append("\n\tSELECT ");
            sql.append("\n\t  id_articulo,");
            sql.append("\n\t  id_bodega,");
            sql.append("\n\t  existencia");
            sql.append("\n\tFROM inventario.bodegas_existencias_tmp");
            sql.append("\n );");

            sql.append("\n  DELETE FROM inventario.bodegas_existencias_tmp ;");
            sql.append("\n  DELETE FROM inventario.bodegas_existencias_lote_tmp ; ");

            sql.append("\n  DELETE FROM inventario.inventarios_tmp ; \t");

            cn.prepareStatementIDU(1, sql);
            System.out.println("\n -- SUMA transaccion ***************************************** --");
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 sumarTransaccion(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en metodo 2 sumarTransaccion(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean actualizarTransaccion() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());

            sql.append("\n UPDATE inventario.transacciones  ");
            sql.append("\n SET ");
            sql.append("\n  cantidad = inventario.transaccion_modifica.cant_nue, ");
            sql.append("\n  valor_unitario = inventario.transaccion_modifica.valor_unitario, ");
            sql.append("\n  iva = inventario.transaccion_modifica.iva, ");
            sql.append(
                    "\n  valor_impuesto = (inventario.transaccion_modifica.valor_unitario * transaccion_modifica.iva / 100 ),  ");

            sql.append("\n\tlote = inventario.transaccion_modifica.lote,");
            sql.append("\n\tfecha_vencimiento = inventario.transaccion_modifica.fecha_vencimiento");

            sql.append("\n FROM inventario.transaccion_modifica  ");
            sql.append("\n WHERE inventario.transacciones.id = inventario.transaccion_modifica.id; ");

            cn.prepareStatementIDU(1, sql);
            System.out.println("\n -- actualizat la transaccion ***************************************** --");
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out
                    .println(localSQLException.getMessage() + " en método 1 actualizarTransaccion(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 actualizarTransaccion(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean eliminarTransaccion() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());

            sql.append("\n DELETE from inventario.transacciones  ");

            sql.append("\n WHERE inventario.transacciones.id = (SELECT id FROM inventario.transaccion_modifica); ");

            cn.prepareStatementIDU(1, sql);
            System.out.println("\n -- elimina la transaccion ************** --");
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 eliminarTransaccion(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 eliminarTransaccion(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean cambiarValoresTransaccion() {
        boolean bool = true;
        try {
            if (loteTransaccion()) {
                sql.delete(0, sql.length());

                sql.append("\n  INSERT INTO inventario.transacciones_tmp ");
                sql.append("  ( ");
                sql.append("\n\tid,");
                sql.append("\n\tid_documento,");
                sql.append("\n\tid_articulo,");
                sql.append("\n\tcantidad,");
                sql.append("\n\tvalor_unitario,");
                sql.append("\n\tiva,");
                sql.append("\n\tvalor_impuesto,");
                sql.append("\n\tid_naturaleza,");
                sql.append("\n\tlote,");
                sql.append("\n\tprecio_venta,");
                sql.append("\n\tfecha_vencimiento,");
                sql.append("\n\texito, ");
                sql.append("\n\tvalor_total");
                sql.append(" \n )(");
                sql.append("\n\t SELECT");
                sql.append("\n\t  tr.id,");
                sql.append("\n\t  tr.id_documento,");
                sql.append("\n\t  tr.id_articulo, ");
                sql.append("\n\t  tm.cant_nue, ");
                sql.append("\n\t  tm.valor_unitario,");
                sql.append("\n\t  tm.iva, ");
                sql.append("\n\t  (tm.valor_unitario * tm.iva /100 ),");
                sql.append("\n\t  tr.id_naturaleza,");
                sql.append("\n\t  tr.lote,");
                sql.append("\n\t  tr.precio_venta,");
                sql.append("\n\t  tr.fecha_vencimiento,");
                sql.append("\n\t  tr.exito,");
                sql.append(
                        "\n\t  (tm.cant_nue*(documento.vlr_flete + tm.valor_unitario+(tm.valor_unitario * tm.iva /100 ))) ");
                sql.append("\n\tFROM inventario.transacciones tr ");
                sql.append("\n  inner join ( ");
                sql.append("\n\t  select (doc.valor_flete/sum (tr.cantidad)) vlr_flete, doc.id ");
                sql.append("\n\t  from inventario.transacciones tr  ");
                sql.append("\n\t  inner join inventario.documentos doc on tr.id_documento = doc.id ");
                sql.append(
                        "\n\t  where doc.id =( SELECT t.id_documento FROM inventario.transacciones t inner join inventario.transaccion_modifica tmo on t.id = tmo.id )::integer ");
                sql.append("\n    group by doc.valor_flete, doc.id ");
                sql.append("\n  ) documento on tr.id_documento = documento.id ");
                sql.append("\n\tINNER JOIN inventario.transaccion_modifica tm on tr.id = tm.id ");
                sql.append("\n\tWHERE tr.id = tm.id ");
                sql.append("\n ); ");

                sql.append("\n UPDATE inventario.bodegas_existencias_lote ");
                sql.append("\n SET  fecha_vencimiento = inventario.transacciones_tmp.fecha_vencimiento");
                sql.append("\n FROM inventario.transacciones_tmp");
                sql.append(
                        "\n WHERE inventario.bodegas_existencias_lote.id_articulo = inventario.transacciones_tmp.id_articulo");
                sql.append("\n AND inventario.bodegas_existencias_lote.lote = inventario.transacciones_tmp.lote ; ");

                sql.append("\n  INSERT INTO inventario.inventarios_tmp ");
                sql.append("  ( ");
                sql.append("\n\tid_articulo,");
                sql.append("\n\tvalor_unitario,");
                sql.append("\n\tprecio_venta,");
                sql.append("\texistencia,");
                sql.append("\n\tvalor_total");
                sql.append("  )(\t ");
                sql.append("\n\t  SELECT   ");
                sql.append("\n\t\ttr.id_articulo,   ");
                sql.append("\n\t\tcase when invt.existencia = 0 then 0   ");
                sql.append("\n\t\telse ((tr.valor_total)/(invt.existencia)) end, ");
                sql.append("\n\t\ttr.precio_venta, ");
                sql.append("\n\t\tinvt.existencia, ");
                sql.append("\n\t\ttr.valor_total ");
                sql.append("\n\t  FROM ( ");
                sql.append("\n\t\t  SELECT ");
                sql.append("\n                trp.id_documento, ");
                sql.append("\n               trp.id_articulo, ");
                sql.append("\n                sum (trp.valor_unitario) valor_unitario, ");
                sql.append("\n                0 precio_venta, ");
                sql.append("\n                sum(inve.existencia) cantidad, ");
                sql.append(
                        "\n                sum ((trp.valor_impuesto+ trp.valor_unitario+documento.vlr_flete)*inve.existencia) valor_total ");
                sql.append("\n          FROM inventario.transacciones_tmp  trp ");
                sql.append("\n          inner join inventario.inventarios inve on trp.id_articulo = inve.id_articulo ");
                sql.append("\n          inner join (  ");
                sql.append("\n\t\t \t select (doc.valor_flete/sum (tr.cantidad)) vlr_flete, doc.id  ");
                sql.append("\n\t\t \t from inventario.transacciones tr   ");
                sql.append("\n\t\t \t inner join inventario.documentos doc on tr.id_documento = doc.id  ");

                sql.append("\n\t\t \t group by doc.valor_flete, doc.id  ");
                sql.append("\n\t\t    ) documento on trp.id_documento = documento.id     ");
                sql.append("\n          INNER JOIN inventario.transaccion_modifica tm ON  trp.id = tm.id ");
                sql.append("\n          GROUP BY trp.id_articulo,  trp.id_documento ");
                sql.append("\n\t  ) tr    ");
                sql.append("\n\t  INNER JOIN inventario.inventarios  invt ON tr.id_articulo = invt.id_articulo");
                sql.append("  ); ");

                sql.append("\n UPDATE inventario.transacciones_tmp ");
                sql.append("\n  SET valor_unitario_promedio = inventario.inventarios_tmp.valor_unitario");
                sql.append(
                        "\n  FROM inventario.inventarios_tmp WHERE inventario.inventarios_tmp.id_articulo = inventario.transacciones_tmp.id_articulo;");

                sql.append("\n  UPDATE inventario.transacciones ");
                sql.append("\n  SET valor_unitario_promedio = inventario.transacciones_tmp.valor_unitario_promedio");
                sql.append(
                        "\n  FROM inventario.transacciones_tmp WHERE inventario.transacciones_tmp.id = inventario.transacciones.id;");

                sql.append("\n  DELETE FROM inventario.inventarios ");
                sql.append("\n  WHERE id_articulo IN (SELECT  tr.id_articulo  ");
                sql.append("\n\t\t\t\t\t\tFROM inventario.transacciones_tmp tr");
                sql.append("\n\t\t\t\t\t\tLEFT JOIN inventario.inventarios inv ON tr.id_articulo = inv.id_articulo ");

                sql.append("\n  );");

                sql.append("\n  INSERT INTO inventario.inventarios");
                sql.append("  (");
                sql.append("\n\tid_articulo,");
                sql.append("\n\tvalor_unitario,");
                sql.append("\n\tprecio_venta,");
                sql.append("\texistencia,");
                sql.append("\n\tvalor_total");
                sql.append("  )(");
                sql.append("\n\t  SELECT ");
                sql.append("\n\t  id_articulo,");
                sql.append("\n\t  valor_unitario,");
                sql.append("\n\t  precio_venta,");
                sql.append("\t  existencia,");
                sql.append("\n\t  valor_total");
                sql.append("\n\t  FROM inventario.inventarios_tmp");
                sql.append("  );");

                sql.append("\n  DELETE FROM inventario.inventarios_tmp ; \t");

                cn.prepareStatementIDU(1, sql);
                System.out.println("\n -- actualizar  cambiarValoresTransaccion **************************** --");
                cn.iduSQL(1);
            } else {
                System.out.println("\n -- No se actualizo el lote por control de existencias  --");
                return false;
            }
        } catch (SQLException localSQLException) {
            System.out.println(
                    localSQLException.getMessage() + " en método 1 cambiarValoresTransaccion(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(
                    localException.getMessage() + " en método 2 cambiarValoresTransaccion(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean actualizarInventarioDocumento() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());

            sql.append("\n  INSERT INTO  inventario.candado (  pk, id_doc  )  ");
            sql.append("\n  VALUES (  1,(select id_documento from inventario.transacciones_tmp)::integer ); ");

            sql.append("\n  DELETE FROM inventario.transacciones_tmp ; \t");

            sql.append("\n  INSERT INTO inventario.transacciones_tmp ( ");
            sql.append("\n    id, ");
            sql.append("\n    id_documento, ");
            sql.append("\n    id_articulo, ");
            sql.append("\n    cantidad, ");
            sql.append("\n    valor_unitario, ");
            sql.append("\n    iva, ");
            sql.append("\n    valor_impuesto, ");
            sql.append("\n    id_naturaleza,  ");
            sql.append("\n    lote,  ");
            sql.append("\n    precio_venta,  ");
            sql.append("\n    fecha_vencimiento,  ");
            sql.append("\n    exito,   ");
            sql.append("\n    valor_total  ");
            sql.append("\n   )(  ");
            sql.append("\n    SELECT  ");
            sql.append("\n  \ttr.id,  ");
            sql.append("\n  \ttr.id_documento,  ");
            sql.append("\n  \ttr.id_articulo,  ");
            sql.append("\n  \ttr.cantidad,  ");
            sql.append("\n  \ttr.valor_unitario,  ");
            sql.append("\n  \ttr.iva,  ");
            sql.append("\n  \ttr.valor_impuesto, ");
            sql.append("\n  \ttr.id_naturaleza, ");
            sql.append("\n  \ttr.lote, ");
            sql.append("\n  \ttr.precio_venta, ");
            sql.append("\n  \ttr.fecha_vencimiento, ");
            sql.append("\n  \ttr.exito, ");
            sql.append("\n  \t(tr.cantidad*(tr.valor_unitario+tr.valor_impuesto+documento.vlr_flete)) ");
            sql.append("\n    FROM inventario.transacciones tr  ");
            sql.append("\n    inner join ( ");
            sql.append("\n  \tselect (doc.valor_flete/sum (tr.cantidad)) vlr_flete, doc.id ");
            sql.append("\n  \tfrom inventario.transacciones tr  ");
            sql.append("\n  \tinner join inventario.documentos doc on tr.id_documento = doc.id ");
            sql.append("\n  \twhere doc.id =( SELECT id_doc FROM inventario.candado)::integer ");
            sql.append("\n  \tgroup by doc.valor_flete, doc.id ");
            sql.append("\n    ) documento on tr.id_documento = documento.id ");
            sql.append("\n    INNER JOIN inventario.documentos doc on tr.id_documento = doc.id ");
            sql.append("\n    WHERE id_documento = ( SELECT id_doc FROM inventario.candado)::integer ");
            sql.append(
                    "\n    and tr.id_articulo = (SELECT id_articulo from inventario.transaccion_modifica tmo inner join inventario.transacciones tm on tmo.id = tm.id) ");
            sql.append("\n   ); ");

            sql.append("\n  INSERT INTO inventario.promedio( ");
            sql.append("\n    id, ");
            sql.append("\n    id_documento, ");
            sql.append("\n    id_articulo, ");
            sql.append("\n    cantidad, ");
            sql.append("\n    valor_unitario, ");
            sql.append("\n    iva, ");
            sql.append("\n    valor_impuesto, ");
            sql.append("\n    id_naturaleza, ");
            sql.append("\n    lote, ");
            sql.append("\n    precio_venta, ");
            sql.append("\n    fecha_vencimiento, ");
            sql.append("\n    exito, ");
            sql.append("\n    fecha_cierre, ");
            sql.append("\n    vlr_total, ");
            sql.append("\n    numero_fila ");
            sql.append("\n   )( ");
            sql.append("\n  SELECT  ");
            sql.append("\n  \t  tr.id, ");
            sql.append("\n  \t  tr.id_documento, ");
            sql.append("\n  \t  tr.id_articulo, ");
            sql.append(
                    "\n  \t  case when  tr.id_naturaleza = 'S' THEN tr.cantidad * (-1)  ELSE tr.cantidad  end cantidad, ");
            sql.append("\n  \t  tr.valor_unitario, ");
            sql.append("\n  \t  tr.iva, ");
            sql.append("\n  \t  tr.valor_impuesto, ");
            sql.append("\n  \t  tr.id_naturaleza, ");
            sql.append("\n  \t  tr.lote, ");
            sql.append("\n  \t  tr.precio_venta, ");
            sql.append("\n  \t  tr.fecha_vencimiento, ");
            sql.append("\n  \t  tr.exito,  ");
            sql.append("\n  \t  doc.fecha_cierre, ");
            sql.append("\n  \t  case when documento.vlr_flete is null then  ");
            sql.append(
                    "\n  \t\tcase when tr.id_naturaleza = 'S' THEN ((tr.cantidad*-1)*(tr.valor_unitario+tr.valor_impuesto + 0)) ");
            sql.append("\n  \t\telse (tr.cantidad*(tr.valor_unitario+tr.valor_impuesto + 0))  end ");
            sql.append(
                    "\n  \t  else  case when tr.id_naturaleza = 'S' THEN ((tr.cantidad*-1)*(tr.valor_unitario+tr.valor_impuesto + documento.vlr_flete)) ");
            sql.append(
                    "\n  \t\t\telse (tr.cantidad*(tr.valor_unitario+tr.valor_impuesto + documento.vlr_flete))  end ");
            sql.append("\n  \t  end vlr_total ");
            sql.append(
                    "\n  \t  ,  row_number() over (PARTITION BY tr.id_articulo order by tr.id_articulo, doc.fecha_cierre ) numero_fila       ");
            sql.append("\n  \tFROM inventario.transacciones tr ");
            sql.append("\n  \tleft join ( ");
            sql.append("\n  \t\tselect (doc.valor_flete/sum (tr.cantidad)) vlr_flete, doc.id ");
            sql.append("\n  \t  from inventario.transacciones tr ");
            sql.append("\n  \t  inner join inventario.documentos doc on tr.id_documento = doc.id ");

            sql.append("\n    group by doc.valor_flete, doc.id ");
            sql.append("\n  ) documento on tr.id_documento = documento.id ");
            sql.append("\n  inner join inventario.documentos doc on tr.id_documento = doc.id ");
            sql.append("\n  WHERE id_articulo in ( ");
            sql.append("\n    select id_articulo from inventario.transacciones_tmp group by id_articulo ");
            sql.append("\n  )  ");
            sql.append("\n  AND doc.id_estado = '1' ");
            sql.append("\n  AND doc.id_bodega = 1 ");
            sql.append(
                    "\n  AND doc.fecha_cierre >= (select fecha_cierre from inventario.documentos where id = (select id_doc from inventario.candado))::timestamp ");
            sql.append("\n  order by tr.id_articulo ,doc.fecha_cierre  ");
            sql.append("\n   ) ; ");

            sql.append("\n    update inventario.transacciones  ");
            sql.append("\n    set valor_unitario_promedio = pr.promedio , ");
            sql.append("\n    valor_unitario = pr.valor_unitario ");
            sql.append("\n    from inventario.promedio pr  ");
            sql.append(
                    "\n    where  inventario.transacciones.id = pr.id and  inventario.transacciones.id_articulo = pr.id_articulo; ");

            sql.append("\n update inventario.inventarios ");
            sql.append("\n set valor_unitario = promed.vlr_promedio, ");
            sql.append("\n valor_total = promed.vlr_total, ");
            sql.append("\n existencia = promed.existencia ");
            sql.append("\n from ( ");
            sql.append("\n\tselect  promedio vlr_promedio, p.id_articulo, p.id, p.numero_fila ");
            sql.append("\n\t\t, bex.existencia , (bex.existencia*promedio) vlr_total ");
            sql.append("\n\t from inventario.promedio p ");
            sql.append("\n\t inner join inventario.transacciones_tmp trp on p.id_articulo = trp.id_articulo ");
            sql.append("\n\t inner join ( ");
            sql.append(
                    "\n\t\tselect  max(numero_fila) max, id_articulo from inventario.promedio where numero_fila >0 group by id_articulo ");
            sql.append("\n\t )  pr      on trp.id_articulo = pr.id_articulo and p.numero_fila = pr.max ");
            sql.append("\n\t inner join ( ");
            sql.append("\n\t\tSELECT sum (existencia) existencia , id_articulo ");
            sql.append("\n\t\t FROM inventario.bodegas_existencias be  ");
            sql.append("\n\t\t GROUP BY id_articulo ");
            sql.append("\n\t ) bex on trp.id_articulo = bex.id_articulo      ");
            sql.append("\n\t group by promedio, p.id_articulo, p.id, p.numero_fila , p.vlr_total ");
            sql.append("\n\t ,bex.existencia ");
            sql.append("\n ) promed ");
            sql.append("\n where inventario.inventarios.id_articulo = promed.id_articulo; ");

            sql.append("\n  DELETE FROM inventario.candado; ");
            sql.append("\n  delete from inventario.promedio WHERE id <> 0; ");

            cn.prepareStatementIDU(1, sql);
            System.out.println("\n -- actualizar  con qury 586  --");
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out.println(
                    localSQLException.getMessage() + " en método 1 actualizarInventarioDocumento(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(
                    localException.getMessage() + " en método 2 actualizarInventarioDocumento(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean modTransaccion() {
        boolean bool = true;
        String str = "";
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                sql.append(
                        "\n insert into inventario.transaccion_modifica  values (?::integer,?::integer,?::integer, ?::numeric, ?::numeric, ?::numeric, ? , ?::timestamp, ?);");
                cn.prepareStatementSEL(1, sql);
                cn.ps1.setInt(1, var1.intValue());
                cn.ps1.setInt(2, var6.intValue());
                cn.ps1.setInt(3, var7.intValue());
                cn.ps1.setFloat(4, var8.floatValue());
                cn.ps1.setFloat(5, var9.floatValue());
                cn.ps1.setFloat(6, var10.floatValue());
                cn.ps1.setString(7, var2);
                cn.ps1.setString(8, var3);
                cn.ps1.setString(9, var4);
                System.out.println("--0=" + ((LoggableStatement) cn.ps1).getQueryString());
                cn.iduSQL(1);

                sql.delete(0, sql.length());
                sql.append(
                        "\n select  case when cant_ant < cant_nue then 'SUMA' when cant_ant > cant_nue then 'RESTA' else 'IGUAL' end resp from inventario.transaccion_modifica; ");
                cn.prepareStatementSEL(2, sql);
                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        System.out.println("--Result- =" + cn.rs2.getString("resp"));
                        if (cn.rs2.getString("resp").equals("RESTA")) {
                            bool = true;
                            str = "RESTA";
                        }
                        if (cn.rs2.getString("resp").equals("SUMA")) {
                            bool = true;
                            str = "SUMA";
                        }
                        if (cn.rs2.getString("resp").equals("IGUAL")) {
                            bool = true;
                            str = "IGUAL";
                        }
                    }
                    cn.cerrarPS(1);
                }

                if (str.equals("RESTA")) {
                    if (restarTransaccion()) {
                        System.out.println(" - *** Realizo la resta de cantidades de inventario **  -");
                        if (actualizarTransaccion()) {
                            System.out.println(" -*Realizo la actualizacion de tr inventario -");

                            if (actualizarInventarioDocumento()) {
                                System.out.println(" -_: Realizo la actualizacion deinventario __");
                            } else {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                } else if (str.equals("SUMA")) {
                    if (sumarTransaccion()) {
                        System.out.println(" -*Realizo la suma de cantidades de inventario -");
                        if (actualizarTransaccion()) {
                            System.out.println(" -*Realizo la actualizacion de tr inventario -");

                            if (actualizarInventarioDocumento()) {
                                System.out.println(" -_: Realizo la actualizacion deinventario __");
                            } else {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                } else if (str.equals("IGUAL")) {
                    if (cambiarValoresTransaccion()) {
                        System.out.println(" -*Realizo la actualizacion de valores  de inventario -");
                        if (actualizarTransaccion()) {
                            System.out.println(" -*Realizo la actualizacion de tr inventario -");

                            if (actualizarInventarioDocumento()) {
                                System.out.println(" -_: Realizo la actualizacion deinventario __");
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        System.out.println(" -_- *No se Realizo la actualizacion de lote  inventario -");
                        return false;
                    }
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 modtransaccion(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 modtransaccion(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean delTransaccion() {
        boolean bool = true;
        String str = "";
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                sql.append(
                        "\n insert into inventario.transaccion_modifica  values (?::integer,?::integer,0::integer, ?::numeric, ?::numeric, ?::numeric, ? , ?::timestamp, ?);");
                cn.prepareStatementSEL(1, sql);
                cn.ps1.setInt(1, var1.intValue());
                cn.ps1.setInt(2, var6.intValue());

                cn.ps1.setFloat(3, var8.floatValue());
                cn.ps1.setFloat(4, var9.floatValue());
                cn.ps1.setFloat(5, var10.floatValue());
                cn.ps1.setString(6, var2);
                cn.ps1.setString(7, var3);
                cn.ps1.setString(8, var4);
                System.out.println("--0=" + ((LoggableStatement) cn.ps1).getQueryString());
                cn.iduSQL(1);

                sql.delete(0, sql.length());
                sql.append(
                        "\n select  case when cant_ant < cant_nue then 'SUMA' when cant_ant > cant_nue then 'RESTA' else 'IGUAL' end resp from inventario.transaccion_modifica; ");
                cn.prepareStatementSEL(2, sql);
                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        System.out.println("--Result- =" + cn.rs2.getString("resp"));
                        if (cn.rs2.getString("resp").equals("RESTA")) {
                            bool = true;
                            str = "RESTA";
                        }
                        if (cn.rs2.getString("resp").equals("SUMA")) {
                            bool = true;
                            str = "SUMA";
                        }
                        if (cn.rs2.getString("resp").equals("IGUAL")) {
                            bool = true;
                            str = "IGUAL";
                        }
                    }
                    cn.cerrarPS(1);
                }

                if (str.equals("RESTA")) {
                    if (restarTransaccion()) {
                        System.out.println(" - *** Realizo la resta de cantidades de inventario **  -");
                        if (actualizarTransaccion()) {
                            System.out.println(" -*Realizo la actualizacion de tr inventario -");
                        }
                    } else {
                        return false;
                    }
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 delTransaccion(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 delTransaccion(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean modificarTransaccionDocumento() {
        System.out.println("ID_TR= " + var1);
        boolean bool = true;
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                if (modTransaccion()) {
                    noticia = "Transaccion modificada con exito";
                } else {
                    noticia = "No se puede realizar por el control de existencias -- modificarTransaccionDocumento";
                }
                limpiarCandados();
                cn.fintransaccion();
            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + "en metodo 2 modificarTransaccionDocumento pac. jav");
            return false;
        }
        return bool;
    }

    public boolean eliminarTransaccionDocumento() {
        System.out.println("ID_TR= " + var1);
        boolean bool = true;
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                if (delTransaccion()) {
                    noticia = "Transaccion eliminada con exito";
                } else {
                    noticia = "No se puede realizar por el control de existencias -- modificarTransaccionDocumento";
                }
                limpiarCandados();
                cn.fintransaccion();
            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + "en metodo 2 modificarTransaccionDocumento pac. jav");
            return false;
        }
        return bool;
    }

    public boolean preguntarMedicamentoNoPost() {
        pacienteTodo = "";
        cn.exito = true;
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(" SELECT POS FROM INVENTARIO.ARTICULO WHERE ID = ?");
                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setInt(1, var1.intValue());

                System.out.println("OKPOSsss = " + ((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(2)) {
                    for (; cn.rs2.next(); cn.cerrarPS(2)) {
                        pacienteTodo = cn.rs2.getString("POS");
                    }
                }
            }
        } catch (SQLException localSQLException) {
            System.out
                    .println("Error --> Paciente.java-- esNoPos--> SQLException --> " + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> Paciente.java-- esNoPos --> Exception --> " + localException.getMessage());
        }
        return cn.exito;
    }

    public boolean preguntarProcedimientoNoPost() {
        pacienteTodo = "";
        cn.exito = true;
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append("SELECT POS FROM FACTURACION.PROCEDIMIENTOS WHERE ID = ?");
                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setString(1, var2);

                System.out.println("OK POS = " + ((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(2)) {
                    for (; cn.rs2.next(); cn.cerrarPS(2)) {
                        pacienteTodo = cn.rs2.getString("POS");
                    }
                }
            }
        } catch (SQLException localSQLException) {
            System.out
                    .println("Error --> Paciente.java-- esNoPos--> SQLException --> " + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> Paciente.java-- esNoPos --> Exception --> " + localException.getMessage());
        }
        return cn.exito;
    }

    public Object buscarInformacionPaciente() {
        ArrayList<PacienteVO> localArrayList = new ArrayList<PacienteVO>();
        String str = "";

        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());

                cn.prepareStatementSEL(1, cn.traerElQuery(314));
                cn.ps1.setInt(1, id.intValue());
                System.out.println("xxx44---------" + ((LoggableStatement) cn.ps1).getQueryString());
                if (cn.selectSQL(1)) {
                    while (cn.rs1.next()) {
                        PacienteVO localPacienteVO = new PacienteVO();
                        localPacienteVO.setId(Integer.valueOf(cn.rs1.getInt("id")));
                        localPacienteVO.setTipoId(cn.rs1.getString("TIPO_ID"));
                        localPacienteVO.setIdentificacion(cn.rs1.getString("IDENTIFICACION"));
                        localPacienteVO.setNombre1(cn.rs1.getString("NOMBRE1"));
                        localPacienteVO.setNombre2(cn.rs1.getString("NOMBRE2"));
                        localPacienteVO.setApellido1(cn.rs1.getString("APELLIDO1"));
                        localPacienteVO.setApellido2(cn.rs1.getString("APELLIDO2"));
                        localPacienteVO.setIdMunicipio(cn.rs1.getString("ID_MUNICIPIO"));
                        localPacienteVO.setNomMunicipio(cn.rs1.getString("NOM_MUNICIPIO"));
                        localPacienteVO.setDireccion(cn.rs1.getString("DIRECCION"));
                        localPacienteVO.setTelefono(cn.rs1.getString("TELEFONO"));
                        localPacienteVO.setCelular1(cn.rs1.getString("CELULAR1"));
                        localPacienteVO.setCelular2(cn.rs1.getString("CELULAR2"));

                        localPacienteVO.setIdEtnia(cn.rs1.getString("ID_ETNIA"));
                        localPacienteVO.setIdNivelEscolar(cn.rs1.getString("ID_NIVEL_ESCOLAR"));
                        localPacienteVO.setIdOcupacion(cn.rs1.getString("ID_OCUPACION"));
                        localPacienteVO.setNomOcupacion(cn.rs1.getString("NOM_OCUPACION"));
                        localPacienteVO.setIdEstrato(cn.rs1.getString("ID_ESTRATO"));

                        localPacienteVO.setFechaNacimiento(cn.rs1.getString("FECHA_NACIMIENTO"));
                        localPacienteVO.setEdad(cn.rs1.getString("EDAD"));
                        localPacienteVO.setSexo(cn.rs1.getString("SEXO"));
                        localPacienteVO.setAcompanante(cn.rs1.getString("ACOMPANANTE"));
                        localPacienteVO.setVar1(cn.rs1.getString("ADMINISTRADORA"));
                        localPacienteVO.setEmail(cn.rs1.getString("EMAIL"));

                        localArrayList.add(localPacienteVO);
                    }
                    cn.cerrarPS(1);
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> SQLException --> "
                    + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> Exception --> "
                    + localException.getMessage());
        }
        return localArrayList;
    }

    public Object buscarInformacionPacienteDesdeIdEvolucion() {
        ArrayList<PacienteVO> localArrayList = new ArrayList<PacienteVO>();
        String str = "";

        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());

                cn.prepareStatementSEL(1, cn.traerElQuery(13));
                cn.ps1.setInt(1, var1.intValue());

                if (cn.selectSQL(1)) {
                    while (cn.rs1.next()) {
                        PacienteVO localPacienteVO = new PacienteVO();
                        localPacienteVO.setVar1(cn.rs1.getString("FECHA_ELABORO"));
                        localPacienteVO.setVar2(cn.rs1.getString("FECHA_INGRESO"));
                        localPacienteVO.setVar3(cn.rs1.getString("ID_FOLIO"));
                        localPacienteVO.setVar4(cn.rs1.getString("ID_HC"));
                        localPacienteVO.setVar5(cn.rs1.getString("NOM_ADMINISTRADORA"));
                        localPacienteVO.setVar6(cn.rs1.getString("IDENTIFICACION_PACIENTE"));
                        localPacienteVO.setVar7(cn.rs1.getString("ID_PROFESIONAL"));

                        localPacienteVO.setNombre1(cn.rs1.getString("NOMBRE1"));
                        localPacienteVO.setNombre2(cn.rs1.getString("NOMBRE2"));
                        localPacienteVO.setApellido1(cn.rs1.getString("APELLIDO1"));
                        localPacienteVO.setApellido2(cn.rs1.getString("APELLIDO2"));
                        localPacienteVO.setFechaNacimiento(cn.rs1.getString("FECHA_NACIMIENTO"));
                        localPacienteVO.setEdad(cn.rs1.getString("EDAD"));
                        localPacienteVO.setSexo(cn.rs1.getString("SEXO"));
                        localPacienteVO.setIdOcupacion(cn.rs1.getString("OCUPACION"));
                        localPacienteVO.setIdNivelEscolar(cn.rs1.getString("NIVEL_ESCOLAR"));
                        localPacienteVO.setIdEtnia(cn.rs1.getString("ETNIA"));
                        localPacienteVO.setNomMunicipio(cn.rs1.getString("MUNICIPIO"));
                        localPacienteVO.setDireccion(cn.rs1.getString("DIRECCION"));
                        localPacienteVO.setAcompanante(cn.rs1.getString("ACOMPANANTE"));
                        localPacienteVO.setTelefono(cn.rs1.getString("TELEFONO"));

                        localArrayList.add(localPacienteVO);
                    }
                    cn.cerrarPS(1);
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> SQLException --> "
                    + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> Exception --> "
                    + localException.getMessage());
        }
        return localArrayList;
    }

    public Object buscarInformacionParaMedicamentoNoPos() {
        ArrayList<PacienteVO> localArrayList = new ArrayList<PacienteVO>();
        String str = "";

        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                cn.prepareStatementSEL(1, cn.traerElQuery(17));
                cn.ps1.setInt(1, var1.intValue());

                if (cn.selectSQL(1)) {
                    while (cn.rs1.next()) {
                        PacienteVO localPacienteVO = new PacienteVO();
                        localPacienteVO.setVar1(cn.rs1.getString("VAR1"));
                        localPacienteVO.setVar2(cn.rs1.getString("VAR2"));
                        localPacienteVO.setVar3(cn.rs1.getString("VAR3"));
                        localPacienteVO.setVar4(cn.rs1.getString("VAR4"));
                        localPacienteVO.setVar5(cn.rs1.getString("VAR5"));
                        localPacienteVO.setVar6(cn.rs1.getString("VAR6"));

                        localArrayList.add(localPacienteVO);
                    }
                    cn.cerrarPS(1);
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> SQLException --> "
                    + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> Exception --> "
                    + localException.getMessage());
        }
        return localArrayList;
    }

    public Object buscarInformacionParaProcedimientoNoPos() {
        ArrayList<PacienteVO> localArrayList = new ArrayList<PacienteVO>();
        String str = "";

        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                cn.prepareStatementSEL(1, cn.traerElQuery(27));
                cn.ps1.setString(1, var2);

                if (cn.imprimirConsola) {
                    System.out.println("buscarInformacionParaProcedimientoNoPos=\n "
                            + ((LoggableStatement) cn.ps1).getQueryString());
                }
                if (cn.selectSQL(1)) {
                    while (cn.rs1.next()) {
                        PacienteVO localPacienteVO = new PacienteVO();
                        localPacienteVO.setVar1(cn.rs1.getString("VAR1"));
                        localPacienteVO.setVar2(cn.rs1.getString("VAR2"));
                        localPacienteVO.setVar3(cn.rs1.getString("VAR3"));
                        localPacienteVO.setVar4(cn.rs1.getString("VAR4"));
                        localPacienteVO.setVar5(cn.rs1.getString("VAR5"));
                        localPacienteVO.setVar6(cn.rs1.getString("VAR6"));
                        localArrayList.add(localPacienteVO);
                    }
                    cn.cerrarPS(1);
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(
                    "Error --> clase Paciente --> function buscarInformacionParaProcedimientoNoPos --> SQLException --> "
                            + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println(
                    "Error --> clase Paciente --> function buscarInformacionParaProcedimientoNoPos --> Exception --> "
                            + localException.getMessage());
        }
        return localArrayList;
    }

    public Object buscarInformacionArticuloTemporal(Integer articulo) {
        ArrayList<PacienteVO> localArrayList = new ArrayList<PacienteVO>();
        String str = "";

        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());

                cn.prepareStatementSEL(1, cn.traerElQuery(712));
                cn.ps1.setInt(1, articulo);

                if (cn.selectSQL(1)) {
                    while (cn.rs1.next()) {

                        PacienteVO localPacienteVO = new PacienteVO();
                        localPacienteVO.setVar1(cn.rs1.getString("PRESENTACION"));
                        localPacienteVO.setVar2(cn.rs1.getString("ATC"));
                        localPacienteVO.setVar3(cn.rs1.getString("POS"));
                        localPacienteVO.setVar4(cn.rs1.getString("CUM"));
                        localPacienteVO.setVar5(cn.rs1.getString("ADMINISTRACION"));
                        localPacienteVO.setVar6(cn.rs1.getString("MEDICAMENTO"));

                        localArrayList.add(localPacienteVO);
                    }
                    cn.cerrarPS(1);
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(
                    "Error --> clase Paciente --> function buscarInformacionArticuloTemporal --> SQLException --> "
                            + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println(
                    "Error --> clase Paciente --> function buscarInformacionArticuloTemporal --> Exception --> "
                            + localException.getMessage());
        }
        return localArrayList;
    }

    public boolean agregarTurno() {
        return llamarSocket("agregarTurno");
    }

    public boolean eliminarTurno() {
        return llamarSocket("eliminarTurno");
    }

    public boolean llamarTurno() {
        return llamarSocket("llamarTurno");
    }

    public boolean ModificarTurno() {
        return llamarSocket("modificarTurno");
    }

    private boolean llamarSocket(String accion) {
        try {
            final SocketCliente cliente = new SocketCliente(new URI("ws://181.48.155.149:8080/clinicaSocket/turnos"));

            cliente.addMessageHandler(new SocketCliente.MessageHandler() {
                @Override
                public void handleMessage(String message) {
                    System.out.println(message);
                }
            });

            JsonProvider provider = JsonProvider.provider();
            JsonObject addMessage = provider.createObjectBuilder().add("accion", accion).add("id", var2)
                    .add("nombre", var3).add("consultorio", var4).add("estado", var5).add("turno", var1).build();

            cliente.sendMessage(addMessage.toString());

            return true;

        } catch (Exception e) {
            System.out.println("Error en Socket: " + accion + " :: " + e.getMessage());
            return false;
        }
    }

    public Conexion getCn() {
        return cn;
    }

    public void setCn(Conexion paramConexion) {
        cn = paramConexion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer paramInteger) {
        id = paramInteger;
    }

    public String getTipoId() {
        return tipoId;
    }

    public void setTipoId(String paramString) {
        tipoId = paramString;
    }

    public String getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(String paramString) {
        Identificacion = paramString;
    }

    public String getNombre1() {
        return Nombre1;
    }

    public void setNombre1(String paramString) {
        Nombre1 = paramString;
    }

    public String getNombre2() {
        return Nombre2;
    }

    public void setNombre2(String paramString) {
        Nombre2 = paramString;
    }

    public String getApellido1() {
        return Apellido1;
    }

    public void setApellido1(String paramString) {
        Apellido1 = paramString;
    }

    public String getApellido2() {
        return Apellido2;
    }

    public void setApellido2(String paramString) {
        Apellido2 = paramString;
    }

    public String getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(String paramString) {
        idMunicipio = paramString;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String paramString) {
        fechaNacimiento = paramString;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String paramString) {
        direccion = paramString;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String paramString) {
        telefono = paramString;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String paramString) {
        email = paramString;
    }

    public String getIdOcupacion() {
        return idOcupacion;
    }

    public void setIdOcupacion(String paramString) {
        idOcupacion = paramString;
    }

    public String getAcompanante() {
        return acompanante;
    }

    public void setAcompanante(String paramString) {
        acompanante = paramString;
    }

    public String getIdEstadoCivil() {
        return idEstadoCivil;
    }

    public void setIdEstadoCivil(String paramString) {
        idEstadoCivil = paramString;
    }

    public Constantes getConstantes() {
        return constantes;
    }

    public void setConstantes(Constantes paramConstantes) {
        constantes = paramConstantes;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String paramString) {
        sexo = paramString;
    }

    public String getCelular1() {
        return celular1;
    }

    public void setCelular1(String paramString) {
        celular1 = paramString;
    }

    public String getCelular2() {
        return celular2;
    }

    public void setCelular2(String paramString) {
        celular2 = paramString;
    }

    public String getPacienteTodo() {
        return pacienteTodo;
    }

    public void setPacienteTodo(String paramString) {
        pacienteTodo = paramString;
    }

    public String getExiste() {
        return existe;
    }

    public void setExiste(String paramString) {
        existe = paramString;
    }

    public Integer getVar1() {
        return var1;
    }

    public void setVar1(Integer paramInteger) {
        var1 = paramInteger;
    }

    public String getVar2() {
        return var2;
    }

    public void setVar2(String paramString) {
        var2 = paramString;
    }

    public String getVar3() {
        return var3;
    }

    public void setVar3(String paramString) {
        var3 = paramString;
    }

    public String getVar4() {
        return var4;
    }

    public void setVar4(String paramString) {
        var4 = paramString;
    }

    public String getVar5() {
        return var5;
    }

    public void setVar5(String paramString) {
        var5 = paramString;
    }

    public String getNoticia() {
        return noticia;
    }

    public void setNoticia(String paramString) {
        noticia = paramString;
    }

    public Integer getVar6() {
        return var6;
    }

    public void setVar6(Integer paramInteger) {
        var6 = paramInteger;
    }

    public Integer getVar7() {
        return var7;
    }

    public void setVar7(Integer paramInteger) {
        var7 = paramInteger;
    }

    public void setVar8(Float paramFloat) {
        var8 = paramFloat;
    }

    public Float getVar9() {
        return var9;
    }

    public void setVar9(Float paramFloat) {
        var9 = paramFloat;
    }

    public Float getVar10() {
        return var10;
    }

    public void setVar10(Float paramFloat) {
        var10 = paramFloat;
    }

    public Float getVar8() {
        return var8;
    }

    public String getVar11() {
        return var11;
    }

    public void setVar11(String paramString) {
        var11 = paramString;
    }

    public String getVar12() {
        return var12;
    }

    public void setVar12(String paramString) {
        var12 = paramString;
    }
}
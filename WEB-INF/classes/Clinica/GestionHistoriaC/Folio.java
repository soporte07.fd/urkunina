package Clinica.GestionHistoriaC;

import Clinica.Presentacion.HistoriaClinica.PacienteVO;
import Sgh.AdminSeguridad.Usuario;
import Sgh.Utilidades.Conexion;
import Sgh.Utilidades.Constantes;
import Sgh.Utilidades.LoggableStatement;
import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;




public class Folio
{
  private Integer id;
  private String tipoId;
  private String Identificacion;
  private String Nombre1;
  private String Nombre2;
  private String Apellido1;
  private String Apellido2;
  private String idMunicipio;
  private String fechaNacimiento;
  private String direccion;
  private String telefono;
  private String celular1;
  private String celular2;
  private String email;
  private String idOcupacion;
  private String acompanante;
  private String idEstadoCivil;
  private String sexo;
  private String esTraslado;
  private String pacienteTodo;
  private String existe;
  private Float var8;
  private Float var9;
  private Float var10;
  private Integer var1;
  private Integer var6;
  private Integer var7;
  private String var2;
  private String var3;
  private String var4;
  private String var5;
  private String var11;
  private String var12;
  private String noticia;
  private Integer idDocumento;
  public Constantes constantes;
  private Conexion cn;
  private StringBuffer sql = new StringBuffer();

  public Folio()
  {
    id = Integer.valueOf(0);
    tipoId = "";
    Identificacion = "";
    Nombre1 = "";
    Nombre2 = "";
    Apellido1 = "";
    Apellido2 = "";
    idMunicipio = "";
    fechaNacimiento = "";
    direccion = "";
    telefono = "";
    celular1 = "";
    celular2 = "";
    sexo = "";
    email = "";
    idOcupacion = "";
    acompanante = "";
    idEstadoCivil = "";
    pacienteTodo = "";
    existe = "";
    var1 = Integer.valueOf(0);
    var6 = Integer.valueOf(0);
    var7 = Integer.valueOf(0);
    var2 = "";
    var3 = "";
    var4 = "";
    var5 = "";
    var11 = "";
    var12 = "";
    noticia = "";
    idDocumento = Integer.valueOf(0);
    esTraslado = "";
  }

  public String siExistePaciente() {
    pacienteTodo = "";
    StringBuffer localStringBuffer = new StringBuffer();
    try {
      if (cn.isEstado()) {
        localStringBuffer.delete(0, localStringBuffer.length());
        localStringBuffer.append(" SELECT ID, TIPO_ID, IDENTIFICACION, NOMBRE_COMPLETO");
        localStringBuffer.append(" FROM HC.VI_PACIENTE WHERE TIPO_ID =? AND IDENTIFICACION = ? ");
        cn.prepareStatementSEL(2, localStringBuffer);
        cn.ps2.setString(1, tipoId);
        cn.ps2.setString(2, Identificacion);


        if (cn.selectSQL(2)) {
          while (cn.rs2.next()) {
            pacienteTodo = (cn.rs2.getString("ID") + "-" + cn.rs2.getString("TIPO_ID") + cn.rs2.getString("IDENTIFICACION") + " " + cn.rs2.getString("NOMBRE_COMPLETO"));
            cn.cerrarPS(2);
          }
        }
      }
    } catch (SQLException localSQLException) {
      System.out.println("Error --> DocumentoHc.java-- function siExistePaciente --> SQLException --> " + localSQLException.getMessage());
    } catch (Exception localException) {
      System.out.println("Error --> DocumentoHc.java-- function siExistePaciente --> Exception --> " + localException.getMessage());
    }

    return pacienteTodo;
  }

  public boolean crearPaciente() {
    cn.exito = true;
    try
    {
      if (cn.isEstado())
      {
        if (siExistePaciente().equals(""))
        {
          cn.iniciatransaccion();
          cn.prepareStatementIDU(1, cn.traerElQuery(311));
          cn.ps1.setString(1, tipoId);
          cn.ps1.setString(2, Identificacion);
          cn.ps1.setString(3, Apellido1);
          cn.ps1.setString(4, Apellido2);
          cn.ps1.setString(5, Nombre1);
          cn.ps1.setString(6, Nombre2);
          cn.ps1.setString(7, fechaNacimiento);
          cn.ps1.setString(8, sexo);
          cn.ps1.setString(9, idMunicipio);
          cn.ps1.setString(10, direccion);
          cn.ps1.setString(11, acompanante);
          cn.ps1.setString(12, telefono);
          cn.ps1.setString(13, celular1);
          cn.ps1.setString(14, celular2);
          cn.ps1.setString(15, email);
          cn.ps1.setString(16, cn.usuario.getLogin());
          cn.iduSQL(1);
          existe = "N";
          cn.fintransaccion();
        } else {
          existe = "S";
        } }
    } catch (SQLException localSQLException) {
      System.out.println(localSQLException.getMessage() + " en método crearPaciente() de Paciente.java");
      cn.exito = false;
    }
    catch (Exception localException) {
      System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
      cn.exito = false;
    }
    siExistePaciente();
    return cn.exito;
  }

  public boolean buscarIdentificacionPaciente()
  {
    cn.exito = true;
    try {
      if (cn.isEstado())
      {
        if (siExistePaciente().equals("")) {
          existe = "N";
        }
        else {
          existe = "S";
        }
      }
    } catch (Exception localException) {
      System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
      cn.exito = false;
    }
    return cn.exito;
  }

  public String TienePendienteLE() {
    pacienteTodo = "NO_PENDIENTE";
    StringBuffer localStringBuffer = new StringBuffer();
    try {
      if (cn.isEstado()) {
        localStringBuffer.delete(0, localStringBuffer.length());
        localStringBuffer.append(" SELECT PENDI.PENDIENTE_LE FROM( ");
        localStringBuffer.append("   SELECT CASE WHEN ad.id_lista_espera IS NULL THEN 'PENDIENTE' ELSE 'NO_PENDIENTE' END AS PENDIENTE_LE ");
        localStringBuffer.append("   FROM   citas.lista_espera  le ");
        localStringBuffer.append("   LEFT JOIN  citas.agenda_detalle ad ON le.id = ad.id_lista_espera");
        localStringBuffer.append("   WHERE le.id_paciente = ?::integer AND le.id_especialidad = ?::integer AND le.id_sub_especialidad = ?::integer AND le.id_estado != 5::integer AND le.con_cita = 'NO' ");
        localStringBuffer.append(" )PENDI ORDER BY PENDI.PENDIENTE_LE");

        cn.prepareStatementSEL(2, localStringBuffer);
        cn.ps2.setString(1, var2);
        cn.ps2.setString(2, var3);
        cn.ps2.setString(3, var4);

        if (cn.selectSQL(2)) {
          while (cn.rs2.next())
            pacienteTodo = cn.rs2.getString("PENDIENTE_LE");
        }
        cn.cerrarPS(2);
      }

    }
    catch (SQLException localSQLException)
    {
      System.out.println("Error --> Paciente.java-- function siExisteLe--> SQLException --> " + localSQLException.getMessage());
    }
    catch (Exception localException) {
      System.out.println("Error --> Paciente.java-- function siExisteLe --> Exception --> " + localException.getMessage());
    }

    System.out.println("TienePendienteLE = " + pacienteTodo);
    return pacienteTodo;
  }

  public String TieneCitaFutura()
  {
    pacienteTodo = "SIN_CITA_FUTURA";
    StringBuffer localStringBuffer = new StringBuffer();
    try {
      if (cn.isEstado())
      {
        localStringBuffer.delete(0, localStringBuffer.length());
        localStringBuffer.append("SELECT CASE WHEN ad.id IS NOT NULL THEN 'PENDIENTE' ELSE 'NO_PENDIENTE' END AS PENDIENTE_LE  ");
        localStringBuffer.append("FROM   citas.agenda_detalle ad  ");
        localStringBuffer.append("INNER JOIN citas.agenda a ON ad.id_agenda = a.id  ");
        localStringBuffer.append("INNER JOIN citas.agenda_estado ae ON (ad.id_estado = ae.id and ae.exito = 'S')  ");
        localStringBuffer.append("WHERE ad.id_paciente = ?::integer ");
        localStringBuffer.append("AND a.id_especialidad = ?::integer  ");
        localStringBuffer.append("AND a.id_sub_especialidad = ?::integer ");
        localStringBuffer.append("AND ad.fecha_cita >  now()  ");
        cn.prepareStatementSEL(2, localStringBuffer);
        cn.ps2.setString(1, var2);
        cn.ps2.setString(2, var3);
        cn.ps2.setString(3, var4);

        if (cn.selectSQL(2)) {
          while (cn.rs2.next())
            pacienteTodo = cn.rs2.getString("PENDIENTE_LE");
        }
        cn.cerrarPS(2);
      }
    }
    catch (SQLException localSQLException) {
      System.out.println("Error --> Paciente.java-- function siExisteLe--> SQLException --> " + localSQLException.getMessage());
    }
    catch (Exception localException) {
      System.out.println("Error --> Paciente.java-- function siExisteLe --> Exception --> " + localException.getMessage());
    }
    System.out.println("TieneCitaFutura = " + pacienteTodo);
    return pacienteTodo;
  }



  public boolean espacioDeCitaDisponible()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    try {
      if (cn.isEstado()) {
        localStringBuffer.delete(0, localStringBuffer.length());
        localStringBuffer.append(" SELECT id_estado FROM citas.agenda_detalle where id = ? ");
        cn.prepareStatementSEL(2, localStringBuffer);
        cn.ps2.setInt(1, var7.intValue());



        if ((cn.selectSQL(2)) &&
          (cn.rs2.next()))
        {

          if (cn.rs2.getString("id_estado").equals("D")) {
            cn.cerrarPS(2);

            return true;
          }


          cn.cerrarPS(2);
          return false;
        }

      }
    }
    catch (SQLException localSQLException)
    {
      System.out.println("Error --> Paciente.java-- function espacioDeCitaDisponible --> SQLException --> " + localSQLException.getMessage());
    } catch (Exception localException) {
      System.out.println("Error --> Paciente.java-- function espacioDeCitaDisponible --> Exception --> " + localException.getMessage());
    }

    return true;
  }

  public String consultarDisponibleDesdeListaEspera()
  {
    String str = "";
    try {
      if (cn.isEstado())
      {
        if (TienePendienteLE().equals("NO_PENDIENTE")) {
          if (TieneCitaFutura().equals("SIN_CITA_FUTURA"))
            str = "N"; else {
            str = "C";
          }
        }
        else {
          str = "S";
        }
      }
    }
    catch (Exception localException) {
      System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
      str = "";
    }
    return str;
  }

  public String consultarDisponibleDesdeAgenda() {
    cn.exito = true;
    try
    {
      if (cn.isEstado())
      {
        if (espacioDeCitaDisponible())
        {
          if (var11.equals(""))
          {
            System.out.println("__SIN LISTA ESPERA");

            if (TienePendienteLE().equals("NO_PENDIENTE")) {
              if (TieneCitaFutura().equals("SIN_CITA_FUTURA"))
                existe = "N"; else {
                existe = "C";
              }
            }
            else {
              existe = "S";
            }
          }
          else
          {
            System.out.println("__CON LISTA ESPERA");

            if (TieneCitaFutura().equals("SIN_CITA_FUTURA")) {
              existe = "N";
            }
            else {
              existe = "C";
            }
          }
        }
        else {
          existe = "O";
        }

      }
    }
    catch (Exception localException)
    {
      cn.exito = false;
    }
    return existe;
  }


  public boolean consultarDisponibleAgendaSinLE()
  {
    System.out.println("setVar2" + var2);
    System.out.println("setVar3" + var3);
    System.out.println("setVar4" + var4);


    cn.exito = true;
    try {
      if (cn.isEstado())
      {
        if (TienePendienteLE().equals("NO_PENDIENTE")) {
          existe = "N";
        }
        else {
          existe = "S";
        }

      }
    }
    catch (Exception localException)
    {
      System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
      cn.exito = false;
    }
    return cn.exito;
  }

  public boolean buscarSiFolioTieneDiligenciadosElementos(int paramInt)
  {
    cn.exito = true;
    try {
      if (cn.isEstado()) {
        cn.prepareStatementSEL(3, cn.traerElQuery(paramInt));
        cn.ps3.setInt(1, var1.intValue());
        System.out.println(((LoggableStatement)cn.ps3).getQueryString());
        if (cn.selectSQL(3)) {
          if ((cn.rs3 != null) &&
            (cn.rs3.next())) {
            if (cn.rs3.getString("NOTICIA").equals("DILIGENCIADO")) {
              System.out.println("SI SE ENCUENTRA DILIGENCIADO EL  MOTIVO_ENFERMEDAD");
              return true;
            }

            noticia = cn.rs3.getString("NOTICIA");
            System.out.println("NO SE ENCUENTRA DILIGENCIADO EL  MOTIVO_ENFERMEDAD");
            return false;
          }

          cn.cerrarPS(3);
        }
      }
    } catch (SQLException localSQLException) {
      System.out.println(localSQLException.getMessage() + " en método buscarSiFolioTieneDiligenciadosElementos(), de Paciente.java");return false;
    } catch (Exception localException) {
      System.out.println(localException.getMessage() + " en método buscarSiFolioTieneDiligenciadosElementos(), de Paciente.java");return false;
    }
    return false;
  }

  public boolean buscarElementosObligatoriosFolio() {
    System.out.println("var1= " + var1);System.out.println("var2= " + var2);
    boolean bool = true;
    noticia = "";
    try {
      if (cn.isEstado()) {
        cn.prepareStatementSEL(2, cn.traerElQuery(443));
        cn.ps2.setString(1, var2.trim());


        if (cn.selectSQL(2)) {
          while (cn.rs2.next())
          {
            if (cn.rs2.getString("SW_MOTIVO_ENFERMEDAD").equals("S")) {
              bool = buscarSiFolioTieneDiligenciadosElementos(450);
              if (!bool) return false;
            }
            if (cn.rs2.getString("SW_ANTECEDENTES").equals("S")) {
              bool = buscarSiFolioTieneDiligenciadosElementos(451);
              if (!bool) return false;
            }
            if (cn.rs2.getString("SW_POS_OPERATORIO").equals("S")) {
              bool = buscarSiFolioTieneDiligenciadosElementos(454);
              if (!bool) return false;
            }
            if (cn.rs2.getString("SW_EXAMEN_FISICO").equals("S")) {
              bool = buscarSiFolioTieneDiligenciadosElementos(452);
              if (!bool) return false;
            }
            if (cn.rs2.getString("SW_DIAGNOSTICOS").equals("S")) {
              bool = buscarSiFolioTieneDiligenciadosElementos(453);
              if (!bool) return false;
            }
            if (cn.rs2.getString("SW_SIGNOS_VITALES").equals("S")) {
              bool = buscarSiFolioTieneDiligenciadosElementos(452);
              if (!bool) { return false;
              }
            }
          }


          cn.cerrarPS(2);
        }
      }
    } catch (SQLException localSQLException) {
      System.out.println(localSQLException.getMessage() + " en método buscarElementosObligatoriosFolio(),, de Paciente.java");
      return false;
    } catch (Exception localException) {
      System.out.println(localException.getMessage() + " en método buscarElementosObligatoriosFolio(), de Paciente.java");
      return false;
    }
    return bool;
  }






  public boolean preguntarProcedimientoNoPost() { pacienteTodo = "";
    cn.exito = true;
    StringBuffer localStringBuffer = new StringBuffer();
    try {
      if (cn.isEstado()) {
        localStringBuffer.delete(0, localStringBuffer.length());
        localStringBuffer.append("SELECT POS FROM FACTURACION.PROCEDIMIENTOS WHERE ID = ?");
        cn.prepareStatementSEL(2, localStringBuffer);
        cn.ps2.setString(1, var2);

        System.out.println("OK POS = " + ((LoggableStatement)cn.ps2).getQueryString());
        if (cn.selectSQL(2)) {
          for (; cn.rs2.next(); cn.cerrarPS(2))
            pacienteTodo = cn.rs2.getString("POS");
        }
      }
    } catch (SQLException localSQLException) {
      System.out.println("Error --> Paciente.java-- esNoPos--> SQLException --> " + localSQLException.getMessage());
    }
    catch (Exception localException) {
      System.out.println("Error --> Paciente.java-- esNoPos --> Exception --> " + localException.getMessage());
    }
    return cn.exito;
  }

  public Object buscarInformacionPaciente()
  {
    ArrayList localArrayList = new ArrayList();
    String str = "";

    try
    {
      if (cn.isEstado()) {
        sql.delete(0, sql.length());

        cn.prepareStatementSEL(1, cn.traerElQuery(314));
        cn.ps1.setInt(1, id.intValue());
        System.out.println("xxx44---------" + ((LoggableStatement)cn.ps1).getQueryString());
        if (cn.selectSQL(1)) {
          while (cn.rs1.next()) {
            PacienteVO localPacienteVO = new PacienteVO();
            localPacienteVO.setId(Integer.valueOf(cn.rs1.getInt("id")));
            localPacienteVO.setTipoId(cn.rs1.getString("TIPO_ID"));
            localPacienteVO.setIdentificacion(cn.rs1.getString("IDENTIFICACION"));
            localPacienteVO.setNombre1(cn.rs1.getString("NOMBRE1"));
            localPacienteVO.setNombre2(cn.rs1.getString("NOMBRE2"));
            localPacienteVO.setApellido1(cn.rs1.getString("APELLIDO1"));
            localPacienteVO.setApellido2(cn.rs1.getString("APELLIDO2"));
            localPacienteVO.setIdMunicipio(cn.rs1.getString("ID_MUNICIPIO"));
            localPacienteVO.setNomMunicipio(cn.rs1.getString("NOM_MUNICIPIO"));
            localPacienteVO.setDireccion(cn.rs1.getString("DIRECCION"));
            localPacienteVO.setTelefono(cn.rs1.getString("TELEFONO"));
            localPacienteVO.setCelular1(cn.rs1.getString("CELULAR1"));
            localPacienteVO.setCelular2(cn.rs1.getString("CELULAR2"));

            localPacienteVO.setIdEtnia(cn.rs1.getString("ID_ETNIA"));
            localPacienteVO.setIdNivelEscolar(cn.rs1.getString("ID_NIVEL_ESCOLAR"));
            localPacienteVO.setIdOcupacion(cn.rs1.getString("ID_OCUPACION"));
            localPacienteVO.setNomOcupacion(cn.rs1.getString("NOM_OCUPACION"));
            localPacienteVO.setIdEstrato(cn.rs1.getString("ID_ESTRATO"));

            localPacienteVO.setFechaNacimiento(cn.rs1.getString("FECHA_NACIMIENTO"));
            localPacienteVO.setEdad(cn.rs1.getString("EDAD"));
            localPacienteVO.setSexo(cn.rs1.getString("SEXO"));
            localPacienteVO.setAcompanante(cn.rs1.getString("ACOMPANANTE"));
            localPacienteVO.setVar1(cn.rs1.getString("ADMINISTRADORA"));
            localPacienteVO.setEmail(cn.rs1.getString("EMAIL"));

            localArrayList.add(localPacienteVO);
          }
          cn.cerrarPS(1);
        }
      }
    }
    catch (SQLException localSQLException) {
      System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> SQLException --> " + localSQLException.getMessage());
    } catch (Exception localException) {
      System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> Exception --> " + localException.getMessage());
    }
    return localArrayList;
  }




  public Object buscarInformacionPacienteDesdeIdEvolucion()
  {
    ArrayList localArrayList = new ArrayList();
    String str = "";

    try
    {
      if (cn.isEstado()) {
        sql.delete(0, sql.length());

        cn.prepareStatementSEL(1, cn.traerElQuery(13));
        cn.ps1.setInt(1, var1.intValue());

        if (cn.selectSQL(1)) {
          while (cn.rs1.next()) {
            PacienteVO localPacienteVO = new PacienteVO();
            localPacienteVO.setVar1(cn.rs1.getString("FECHA_ELABORO"));
            localPacienteVO.setVar2(cn.rs1.getString("FECHA_INGRESO"));
            localPacienteVO.setVar3(cn.rs1.getString("ID_FOLIO"));
            localPacienteVO.setVar4(cn.rs1.getString("ID_HC"));
            localPacienteVO.setVar5(cn.rs1.getString("NOM_ADMINISTRADORA"));
            localPacienteVO.setVar6(cn.rs1.getString("IDENTIFICACION_PACIENTE"));
            localPacienteVO.setVar7(cn.rs1.getString("ID_PROFESIONAL"));


            localPacienteVO.setNombre1(cn.rs1.getString("NOMBRE1"));
            localPacienteVO.setNombre2(cn.rs1.getString("NOMBRE2"));
            localPacienteVO.setApellido1(cn.rs1.getString("APELLIDO1"));
            localPacienteVO.setApellido2(cn.rs1.getString("APELLIDO2"));
            localPacienteVO.setFechaNacimiento(cn.rs1.getString("FECHA_NACIMIENTO"));
            localPacienteVO.setEdad(cn.rs1.getString("EDAD"));
            localPacienteVO.setSexo(cn.rs1.getString("SEXO"));
            localPacienteVO.setIdOcupacion(cn.rs1.getString("OCUPACION"));
            localPacienteVO.setIdNivelEscolar(cn.rs1.getString("NIVEL_ESCOLAR"));
            localPacienteVO.setIdEtnia(cn.rs1.getString("ETNIA"));
            localPacienteVO.setNomMunicipio(cn.rs1.getString("MUNICIPIO"));
            localPacienteVO.setDireccion(cn.rs1.getString("DIRECCION"));
            localPacienteVO.setAcompanante(cn.rs1.getString("ACOMPANANTE"));
            localPacienteVO.setTelefono(cn.rs1.getString("TELEFONO"));


            localArrayList.add(localPacienteVO);
          }
          cn.cerrarPS(1);
        }
      }
    }
    catch (SQLException localSQLException) {
      System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> SQLException --> " + localSQLException.getMessage());
    } catch (Exception localException) {
      System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> Exception --> " + localException.getMessage());
    }
    return localArrayList;
  }


  public Object buscarInformacionParaMedicamentoNoPos()
  {
    ArrayList localArrayList = new ArrayList();
    String str = "";

    try
    {
      if (cn.isEstado()) {
        sql.delete(0, sql.length());
        cn.prepareStatementSEL(1, cn.traerElQuery(17));
        cn.ps1.setInt(1, var1.intValue());

        if (cn.selectSQL(1)) {
          while (cn.rs1.next()) {
            PacienteVO localPacienteVO = new PacienteVO();
            localPacienteVO.setVar1(cn.rs1.getString("VAR1"));
            localPacienteVO.setVar2(cn.rs1.getString("VAR2"));
            localPacienteVO.setVar3(cn.rs1.getString("VAR3"));
            localPacienteVO.setVar4(cn.rs1.getString("VAR4"));
            localPacienteVO.setVar5(cn.rs1.getString("VAR5"));
            localPacienteVO.setVar6(cn.rs1.getString("VAR6"));



            localArrayList.add(localPacienteVO);
          }
          cn.cerrarPS(1);
        }
      }
    }
    catch (SQLException localSQLException) {
      System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> SQLException --> " + localSQLException.getMessage());
    } catch (Exception localException) {
      System.out.println("Error --> clase Paciente --> function buscarInformacionPaciente --> Exception --> " + localException.getMessage());
    }
    return localArrayList;
  }

  public Object buscarInformacionParaProcedimientoNoPos()
  {
    ArrayList localArrayList = new ArrayList();
    String str = "";

    try
    {
      if (cn.isEstado()) {
        sql.delete(0, sql.length());
        cn.prepareStatementSEL(1, cn.traerElQuery(27));
        cn.ps1.setString(1, var2);

        if (cn.imprimirConsola) {
          System.out.println("buscarInformacionParaProcedimientoNoPos=\n " + ((LoggableStatement)cn.ps1).getQueryString());
        }
        if (cn.selectSQL(1)) {
          while (cn.rs1.next()) {
            PacienteVO localPacienteVO = new PacienteVO();
            localPacienteVO.setVar1(cn.rs1.getString("VAR1"));
            localPacienteVO.setVar2(cn.rs1.getString("VAR2"));
            localPacienteVO.setVar3(cn.rs1.getString("VAR3"));
            localPacienteVO.setVar4(cn.rs1.getString("VAR4"));
            localPacienteVO.setVar5(cn.rs1.getString("VAR5"));
            localPacienteVO.setVar6(cn.rs1.getString("VAR6"));
            localArrayList.add(localPacienteVO);
          }
          cn.cerrarPS(1);
        }
      }
    }
    catch (SQLException localSQLException) {
      System.out.println("Error --> clase Paciente --> function buscarInformacionParaProcedimientoNoPos --> SQLException --> " + localSQLException.getMessage());
    } catch (Exception localException) {
      System.out.println("Error --> clase Paciente --> function buscarInformacionParaProcedimientoNoPos --> Exception --> " + localException.getMessage());
    }
    return localArrayList;
  }


  public Conexion getCn()
  {
    return cn;
  }

  public void setCn(Conexion paramConexion) {
    cn = paramConexion;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer paramInteger) {
    id = paramInteger;
  }

  public String getTipoId() {
    return tipoId;
  }

  public void setTipoId(String paramString) {
    tipoId = paramString;
  }

  public String getIdentificacion() {
    return Identificacion;
  }

  public void setIdentificacion(String paramString) {
    Identificacion = paramString;
  }

  public String getNombre1() {
    return Nombre1;
  }

  public void setNombre1(String paramString) {
    Nombre1 = paramString;
  }

  public String getNombre2() {
    return Nombre2;
  }

  public void setNombre2(String paramString) {
    Nombre2 = paramString;
  }

  public String getApellido1() {
    return Apellido1;
  }

  public void setApellido1(String paramString) {
    Apellido1 = paramString;
  }

  public String getApellido2() {
    return Apellido2;
  }

  public void setApellido2(String paramString) {
    Apellido2 = paramString;
  }

  public String getIdMunicipio() {
    return idMunicipio;
  }

  public void setIdMunicipio(String paramString) {
    idMunicipio = paramString;
  }

  public String getFechaNacimiento() {
    return fechaNacimiento;
  }

  public void setFechaNacimiento(String paramString) {
    fechaNacimiento = paramString;
  }

  public String getDireccion() {
    return direccion;
  }

  public void setDireccion(String paramString) {
    direccion = paramString;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String paramString) {
    telefono = paramString;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String paramString) {
    email = paramString;
  }

  public String getIdOcupacion() {
    return idOcupacion;
  }

  public void setIdOcupacion(String paramString) {
    idOcupacion = paramString;
  }

  public String getAcompanante() {
    return acompanante;
  }

  public void setAcompanante(String paramString) {
    acompanante = paramString;
  }

  public String getIdEstadoCivil() {
    return idEstadoCivil;
  }

  public void setIdEstadoCivil(String paramString) {
    idEstadoCivil = paramString;
  }

  public Constantes getConstantes() {
    return constantes;
  }

  public void setConstantes(Constantes paramConstantes) {
    constantes = paramConstantes;
  }

  public String getSexo() {
    return sexo;
  }

  public void setSexo(String paramString) {
    sexo = paramString;
  }

  public String getCelular1() {
    return celular1;
  }

  public void setCelular1(String paramString) {
    celular1 = paramString;
  }

  public String getCelular2() {
    return celular2;
  }

  public void setCelular2(String paramString) {
    celular2 = paramString;
  }

  public String getPacienteTodo() {
    return pacienteTodo;
  }

  public void setPacienteTodo(String paramString) {
    pacienteTodo = paramString;
  }

  public String getExiste() {
    return existe;
  }

  public void setExiste(String paramString) {
    existe = paramString;
  }

  public Integer getVar1()
  {
    return var1;
  }

  public void setVar1(Integer paramInteger) {
    var1 = paramInteger;
  }

  public String getVar2() {
    return var2;
  }

  public void setVar2(String paramString) {
    var2 = paramString;
  }

  public String getVar3() {
    return var3;
  }

  public void setVar3(String paramString) {
    var3 = paramString;
  }

  public String getVar4() {
    return var4;
  }

  public void setVar4(String paramString) {
    var4 = paramString;
  }

  public String getVar5() {
    return var5;
  }

  public void setVar5(String paramString) {
    var5 = paramString;
  }

  public String getNoticia()
  {
    return noticia;
  }

  public void setNoticia(String paramString) {
    noticia = paramString;
  }

  public Integer getVar6() { return var6; }

  public void setVar6(Integer paramInteger)
  {
    var6 = paramInteger;
  }

  public Integer getVar7() {
    return var7;
  }

  public void setVar7(Integer paramInteger) {
    var7 = paramInteger;
  }

  public void setVar8(Float paramFloat) {
    var8 = paramFloat;
  }

  public Float getVar9() {
    return var9;
  }

  public void setVar9(Float paramFloat) {
    var9 = paramFloat;
  }

  public Float getVar10() {
    return var10;
  }

  public void setVar10(Float paramFloat) {
    var10 = paramFloat;
  }

  public Float getVar8() { return var8; }

  public String getVar11()
  {
    return var11;
  }

  public void setVar11(String paramString) {
    var11 = paramString;
  }

  public String getVar12() {
    return var12;
  }

  public void setVar12(String paramString) {
    var12 = paramString;
  }
}
package Clinica.GestionHistoriaC;

import Clinica.Presentacion.HistoriaClinica.PacienteVO;
import Sgh.AdminSeguridad.Usuario;
import Sgh.Utilidades.Conexion;
import Sgh.Utilidades.Constantes;
import Sgh.Utilidades.LoggableStatement;
import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Paciente {

    private Integer id;
    private String tipoId;
    private String Identificacion;
    private String Nombre1;
    private String Nombre2;
    private String Apellido1;
    private String Apellido2;
    private String idMunicipio;
    private String fechaNacimiento;
    private String direccion;
    private String telefono;
    private String celular1;
    private String celular2;
    private String email;
    private String idOcupacion;
    private String acompanante;
    private String idEstadoCivil;
    private String sexo;
    private String esTraslado;
    private String pacienteTodo;
    private String existe;
    private Float var8;
    private Float var9;
    private Float var10;
    private Integer var1;
    private Integer var6;
    private Integer var7;
    private String var2;
    private String var3;
    private String var4;
    private String var5;
    private String var11;
    private String var12;
    private String noticia;
    private Integer idDocumento;
    public Constantes constantes;
    private Conexion cn;
    private StringBuffer sql = new StringBuffer();

    public Paciente() {
        id = Integer.valueOf(0);
        tipoId = "";
        Identificacion = "";
        Nombre1 = "";
        Nombre2 = "";
        Apellido1 = "";
        Apellido2 = "";
        idMunicipio = "";
        fechaNacimiento = "";
        direccion = "";
        telefono = "";
        celular1 = "";
        celular2 = "";
        sexo = "";
        email = "";
        idOcupacion = "";
        acompanante = "";
        idEstadoCivil = "";
        pacienteTodo = "";
        existe = "";
        var1 = Integer.valueOf(0);
        var6 = Integer.valueOf(0);
        var7 = Integer.valueOf(0);
        var2 = "";
        var3 = "";
        var4 = "";
        var5 = "";
        var11 = "";
        var12 = "";
        noticia = "";
        idDocumento = Integer.valueOf(0);
        esTraslado = "";
    }

    public String siExistePaciente() {
        pacienteTodo = "";
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(" SELECT ID, TIPO_ID, IDENTIFICACION, NOMBRE_COMPLETO");
                localStringBuffer.append(" FROM HC.VI_PACIENTE WHERE TIPO_ID =? AND IDENTIFICACION = ? ");
                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setString(1, tipoId);
                cn.ps2.setString(2, Identificacion);

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        pacienteTodo = (cn.rs2.getString("ID") + "-" + cn.rs2.getString("TIPO_ID")
                                + cn.rs2.getString("IDENTIFICACION") + " " + cn.rs2.getString("NOMBRE_COMPLETO"));
                        cn.cerrarPS(2);
                    }
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> DocumentoHc.java-- function siExistePaciente --> SQLException --> "
                    + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> DocumentoHc.java-- function siExistePaciente --> Exception --> "
                    + localException.getMessage());
        }

        return pacienteTodo;
    }

    public boolean crearPaciente() {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                if (siExistePaciente().equals("")) {
                    cn.iniciatransaccion();
                    cn.prepareStatementIDU(1, cn.traerElQuery(311));
                    cn.ps1.setString(1, tipoId);
                    cn.ps1.setString(2, Identificacion);
                    cn.ps1.setString(3, Apellido1);
                    cn.ps1.setString(4, Apellido2);
                    cn.ps1.setString(5, Nombre1);
                    cn.ps1.setString(6, Nombre2);
                    cn.ps1.setString(7, fechaNacimiento);
                    cn.ps1.setString(8, sexo);
                    cn.ps1.setString(9, idMunicipio);
                    cn.ps1.setString(10, direccion);
                    cn.ps1.setString(11, acompanante);
                    cn.ps1.setString(12, telefono);
                    cn.ps1.setString(13, celular1);
                    cn.ps1.setString(14, celular2);
                    cn.ps1.setString(15, email);
                    cn.ps1.setString(16, cn.usuario.getLogin());
                    cn.iduSQL(1);
                    existe = "N";
                    cn.fintransaccion();
                } else {
                    existe = "S";
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método crearPaciente() de Paciente.java");
            cn.exito = false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
            cn.exito = false;
        }
        siExistePaciente();
        return cn.exito;
    }

    public boolean buscarIdentificacionPaciente() {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                if (siExistePaciente().equals("")) {
                    existe = "N";
                } else {
                    existe = "S";
                }
            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
            cn.exito = false;
        }
        return cn.exito;
    }

    public String TienePendienteLE() {
        pacienteTodo = "NO_PENDIENTE";
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(" SELECT PENDI.PENDIENTE_LE FROM( ");
                localStringBuffer.append(
                        "   SELECT CASE WHEN ad.id_lista_espera IS NULL THEN 'PENDIENTE' ELSE 'NO_PENDIENTE' END AS PENDIENTE_LE ");
                localStringBuffer.append("   FROM   citas.lista_espera  le ");
                localStringBuffer.append("   LEFT JOIN  citas.agenda_detalle ad ON le.id = ad.id_lista_espera");
                localStringBuffer.append(
                        "   WHERE le.id_paciente = ?::integer AND le.id_especialidad = ?::integer AND le.id_sub_especialidad = ?::integer AND le.id_estado != 5::integer AND le.con_cita = 'NO' ");
                localStringBuffer.append(" )PENDI ORDER BY PENDI.PENDIENTE_LE");

                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setString(1, var2);
                cn.ps2.setString(2, var3);
                cn.ps2.setString(3, var4);

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        pacienteTodo = cn.rs2.getString("PENDIENTE_LE");
                    }
                }
                cn.cerrarPS(2);
            }

        } catch (SQLException localSQLException) {
            System.out.println("Error --> Paciente.java-- function siExisteLe--> SQLException --> "
                    + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println(
                    "Error --> Paciente.java-- function siExisteLe --> Exception --> " + localException.getMessage());
        }

        System.out.println("TienePendienteLE = " + pacienteTodo);
        return pacienteTodo;
    }

    public String TieneCitaFutura() {
        pacienteTodo = "SIN_CITA_FUTURA";
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(
                        "SELECT CASE WHEN ad.id IS NOT NULL THEN 'PENDIENTE' ELSE 'NO_PENDIENTE' END AS PENDIENTE_LE  ");
                localStringBuffer.append("FROM   citas.agenda_detalle ad  ");
                localStringBuffer.append("INNER JOIN citas.agenda a ON ad.id_agenda = a.id  ");
                localStringBuffer
                        .append("INNER JOIN citas.agenda_estado ae ON (ad.id_estado = ae.id and ae.exito = 'S')  ");
                localStringBuffer.append("WHERE ad.id_paciente = ?::integer ");
                localStringBuffer.append("AND a.id_especialidad = ?::integer  ");
                localStringBuffer.append("AND a.id_sub_especialidad = ?::integer ");
                localStringBuffer.append("AND ad.fecha_cita >  now()  ");
                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setString(1, var2);
                cn.ps2.setString(2, var3);
                cn.ps2.setString(3, var4);

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        pacienteTodo = cn.rs2.getString("PENDIENTE_LE");
                    }
                }
                cn.cerrarPS(2);
            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> Paciente.java-- function siExisteLe--> SQLException --> "
                    + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println(
                    "Error --> Paciente.java-- function siExisteLe --> Exception --> " + localException.getMessage());
        }
        System.out.println("TieneCitaFutura = " + pacienteTodo);
        return pacienteTodo;
    }

    public boolean espacioDeCitaDisponible() {
        StringBuffer localStringBuffer = new StringBuffer();
        try {
            if (cn.isEstado()) {
                localStringBuffer.delete(0, localStringBuffer.length());
                localStringBuffer.append(" SELECT id_estado FROM citas.agenda_detalle where id = ? ");
                cn.prepareStatementSEL(2, localStringBuffer);
                cn.ps2.setInt(1, var7.intValue());

                if ((cn.selectSQL(2)) && (cn.rs2.next())) {

                    if (cn.rs2.getString("id_estado").equals("D")) {
                        cn.cerrarPS(2);

                        return true;
                    }

                    cn.cerrarPS(2);
                    return false;
                }

            }
        } catch (SQLException localSQLException) {
            System.out.println("Error --> Paciente.java-- function espacioDeCitaDisponible --> SQLException --> "
                    + localSQLException.getMessage());
        } catch (Exception localException) {
            System.out.println("Error --> Paciente.java-- function espacioDeCitaDisponible --> Exception --> "
                    + localException.getMessage());
        }

        return true;
    }

    public String consultarDisponibleDesdeListaEspera() {
        String str = "";
        try {
            if (cn.isEstado()) {
                if (TienePendienteLE().equals("NO_PENDIENTE")) {
                    if (TieneCitaFutura().equals("SIN_CITA_FUTURA")) {
                        str = "N";
                    } else {
                        str = "C";
                    }
                } else {
                    str = "S";
                }
            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
            str = "";
        }
        return str;
    }

    public String consultarDisponibleDesdeAgenda() {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                if (espacioDeCitaDisponible()) {
                    if (var11.equals("")) {
                        System.out.println("__SIN LISTA ESPERA");

                        if (TienePendienteLE().equals("NO_PENDIENTE")) {
                            if (TieneCitaFutura().equals("SIN_CITA_FUTURA")) {
                                existe = "N";
                            } else {
                                existe = "C";
                            }
                        } else {
                            existe = "S";
                        }
                    } else {
                        System.out.println("__CON LISTA ESPERA");

                        if (TieneCitaFutura().equals("SIN_CITA_FUTURA")) {
                            existe = "N";
                        } else {
                            existe = "C";
                        }
                    }
                } else {
                    existe = "O";
                }

            }
        } catch (Exception localException) {
            cn.exito = false;
        }
        return existe;
    }

    public boolean consultarDisponibleAgendaSinLE() {
        System.out.println("setVar2" + var2);
        System.out.println("setVar3" + var3);
        System.out.println("setVar4" + var4);

        cn.exito = true;
        try {
            if (cn.isEstado()) {
                if (TienePendienteLE().equals("NO_PENDIENTE")) {
                    existe = "N";
                } else {
                    existe = "S";
                }

            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método crearPaciente() de Paciente.java");
            cn.exito = false;
        }
        return cn.exito;
    }

    public boolean buscarSiFolioTieneDiligenciadosElementos(int paramInt) {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                cn.prepareStatementSEL(3, cn.traerElQuery(paramInt));
                cn.ps3.setInt(1, var1.intValue());
                System.out.println(((LoggableStatement) cn.ps3).getQueryString());
                if (cn.selectSQL(3)) {
                    if ((cn.rs3 != null) && (cn.rs3.next())) {
                        if (cn.rs3.getString("NOTICIA").equals("DILIGENCIADO")) {
                            System.out.println("SI SE ENCUENTRA DILIGENCIADO EL  MOTIVO_ENFERMEDAD");
                            return true;
                        }

                        noticia = cn.rs3.getString("NOTICIA");
                        System.out.println("NO SE ENCUENTRA DILIGENCIADO EL  MOTIVO_ENFERMEDAD");
                        return false;
                    }

                    cn.cerrarPS(3);
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage()
                    + " en método buscarSiFolioTieneDiligenciadosElementos(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage()
                    + " en método buscarSiFolioTieneDiligenciadosElementos(), de Paciente.java");
            return false;
        }
        return false;
    }

    public boolean buscarElementosObligatoriosFolio() {
        System.out.println("var1= " + var1);
        System.out.println("var2= " + var2);
        boolean bool = true;
        noticia = "";
        try {
            if (cn.isEstado()) {
                cn.prepareStatementSEL(2, cn.traerElQuery(443));
                cn.ps2.setString(1, var2.trim());

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        if (cn.rs2.getString("SW_MOTIVO_ENFERMEDAD").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(450);
                            if (!bool) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_ANTECEDENTES").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(451);
                            if (!bool) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_POS_OPERATORIO").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(454);
                            if (!bool) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_EXAMEN_FISICO").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(452);
                            if (!bool) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_DIAGNOSTICOS").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(453);
                            if (!bool) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_SIGNOS_VITALES").equals("S")) {
                            bool = buscarSiFolioTieneDiligenciadosElementos(452);
                            if (!bool) {
                                return false;
                            }
                        }
                    }

                    cn.cerrarPS(2);
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage()
                    + " en método buscarElementosObligatoriosFolio(),, de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(
                    localException.getMessage() + " en método buscarElementosObligatoriosFolio(), de Paciente.java");
            return false;
        }
        return bool;
    }

    /* para bodegas */
    public int bodega_doc() {
        boolean bool = true;
        int i = 0;
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());

                sql.append("\n select id_bodega cantidad ");
                sql.append("\n from inventario.documentos  ");
                sql.append("\n where id = (select id_doc from inventario.candado)::integer ");

                cn.prepareStatementSEL(2, sql);
                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        System.out.println("--Result bodega origen- =" + cn.rs2.getString("cantidad"));
                        i = cn.rs2.getInt("cantidad");
                    }
                    cn.cerrarPS(2);
                }
                return i;
            }
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 cambiarLote(), de Paciente.java");
            return 0;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 cambiarLote(), de Paciente.java");
            return 0;
        }
        return i;
    }

    public boolean hacerSalidasInventario() {
        boolean bool = true;
        noticia = "xxx";
        String ide_us = "";
        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                if (cn.getMotor().equals("sqlserver")) {
                    sql.append("INSERT INTO INVENTARIO.CANDADO (ID_DOC, ID_USUARIO) VALUES(?,?) ");
                } else {
                    System.out.println("--POSTGRES B ");
                    sql.append("\n INSERT INTO inventario.candado (id_doc, id_usuario) VALUES(?::integer,?) ");
                }
                ide_us = cn.usuario.getIdentificacion();
                while (ide_us.equals("")) {
                    ide_us = cn.usuario.getIdentificacion();
                }
                cn.prepareStatementSEL(1, sql);
                cn.ps1.setInt(1, var1.intValue());
                cn.ps1.setString(2, ide_us);
                System.out.println("--0=" + ((LoggableStatement) cn.ps1).getQueryString());
                cn.iduSQL(1);

                sql.delete(0, sql.length());
                if (cn.getMotor().equals("sqlserver")) {
                    sql.append("UPDATE INVENTARIO.TRANSACCIONES  SET EXITO ='S' WHERE ID_DOCUMENTO =? ");
                } else {
                    System.out.println("--POSTGRES B ");
                    sql.append("\n UPDATE inventario.transacciones  SET exito ='S' WHERE id_documento =?::integer ");
                }
                cn.prepareStatementSEL(1, sql);
                cn.ps1.setInt(1, var1.intValue());
                System.out.println("--0.1=" + ((LoggableStatement) cn.ps1).getQueryString());
                cn.iduSQL(1);

                sql.delete(0, sql.length());
                if (cn.getMotor().equals("sqlserver")) {
                    sql.append(
                            "\n INSERT INTO INVENTARIO.TRANSACCIONES_TMP( ID, ID_DOCUMENTO, ID_ARTICULO, CANTIDAD,VALOR_UNITARIO,IVA,VALOR_IMPUESTO, LOTE, SERIAL, PRECIO_VENTA ,VALOR_TOTAL, FECHA_VENCIMIENTO )  ");
                    sql.append(
                            " ( SELECT ID, ID_DOCUMENTO, ID_ARTICULO, CANTIDAD,VALOR_UNITARIO,IVA,VALOR_IMPUESTO, LOTE, SERIAL,PRECIO_VENTA ,(CANTIDAD*VALOR_UNITARIO), FECHA_VENCIMIENTO   FROM INVENTARIO.TRANSACCIONES  WHERE  ID_DOCUMENTO = ?)");
                } else {
                    System.out.println("--POSTGRES B ");
                    sql.append(
                            "\n INSERT INTO inventario.transacciones_tmp( id, id_documento, id_articulo, cantidad,valor_unitario,iva,valor_impuesto, lote, serial, precio_venta ,valor_total, fecha_vencimiento )  ");
                    sql.append(
                            "\n ( SELECT id, id_documento, id_articulo, cantidad,valor_unitario,iva,valor_impuesto, lote, serial,precio_venta ,(cantidad*valor_unitario), fecha_vencimiento   FROM inventario.transacciones  WHERE  id_documento = ?::integer)");
                }
                cn.prepareStatementIDU(2, sql);
                cn.ps2.setInt(1, var1.intValue());
                System.out.println("--1 " + ((LoggableStatement) cn.ps2).getQueryString());
                cn.iduSQL(2);

                sql.delete(0, sql.length());

                System.out.println("--POSTGRES B ");

                sql.append("\n UPDATE ");
                sql.append("\n  inventario.transacciones_tmp ");
                sql.append("\n SET ");
                sql.append("\n  exito = 'N' ");
                sql.append("\n WHERE id IN ( ");
                sql.append("\n SELECT  ");
                sql.append("\n      cantidad.id_transaccion  ");
                sql.append("\n    FROM ");
                sql.append("\n    ( ");
                sql.append("\n      SELECT  ");
                sql.append("\n        tr.id id_transaccion, ");
                sql.append("\n        case when  bel.existencia is null then -tr.cantidad ");
                sql.append("\n          when tr.cantidad > bel.existencia then bel.existencia - tr.cantidad ");
                sql.append("\n       else bel.existencia - tr.cantidad  ");
                sql.append("\n       end existencias ");
                sql.append("\n     FROM  ");
                sql.append("\n       inventario.transacciones_tmp tr ");
                sql.append("\n     inner join inventario.documentos doc on tr.id_documento = doc.id ");
                sql.append(
                        "\n     left join inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo and doc.id_bodega = bel.id_bodega and tr.lote = bel.lote) ");
                sql.append("\n     where doc.id = ( select id_doc from inventario.candado)::integer ");
                sql.append("\n     and tr.lote <> ''  ");
                sql.append("\n    ) as cantidad ");
                sql.append("\n   WHERE cantidad.existencias < 0 ");
                sql.append("\n ); ");

                sql.append("\n update  ");
                sql.append("\n  inventario.transacciones_tmp   ");
                sql.append("\n set  ");
                sql.append("\n  exito = 'N' ");
                sql.append("\n WHERE id in (     ");
                sql.append("\n select  ");
                sql.append("\n     cantidad.id        ");
                sql.append("\n   from ");
                sql.append("\n   (     ");
                sql.append("\n       select ");
                sql.append("\n         tr.id,    ");
                sql.append("\n         tr.id_articulo, ");
                sql.append("\n         doc.id_bodega, ");
                sql.append("\n         case when  be.existencia is null then tr.cantidad ");
                sql.append("\n           when tr.cantidad > be.existencia then be.existencia - tr.cantidad ");
                sql.append("\n         else be.existencia - tr.cantidad  end existencias ");
                sql.append("\n       FROM ");
                sql.append("\n         inventario.transacciones_tmp tr ");
                sql.append("\n       inner join inventario.documentos doc on tr.id_documento = doc.id ");
                sql.append(
                        "\n       left join inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo and doc.id_bodega = be.id_bodega) ");
                sql.append("\n       where tr.id_documento = ( select id_doc from inventario.candado)::integer ");
                sql.append("\n       and tr.lote = '' ");
                sql.append("\n   ) as cantidad   ");
                sql.append("\n   where cantidad.existencias < 0 ");
                sql.append("\n ); ");

                cn.prepareStatementIDU(1, sql);
                System.out.println("--TIGERR " + ((LoggableStatement) cn.ps1).getQueryString());
                cn.iduSQL(1);

                sql.delete(0, sql.length());
                if (cn.getMotor().equals("sqlserver")) {
                    sql.append(" UPDATE INVENTARIO.TRANSACCIONES SET  EXITO = 'N' ");
                    sql.append(" FROM INVENTARIO.TRANSACCIONES a, INVENTARIO.TRANSACCIONES_TMP b ");
                    sql.append(" WHERE a.ID = b.ID AND a.ID_DOCUMENTO = ? AND b.EXITO = 'N' ");
                } else {
                    System.out.println("--POSTGRES B ");
                    sql.append("\n UPDATE inventario.transacciones set  exito = 'N' ");
                    sql.append("\n where id in ( ");
                    sql.append(
                            "\n SELECT id FROM inventario.transacciones_tmp where id_documento = ?::integer and exito = 'N'); ");
                }
                cn.prepareStatementIDU(2, sql);
                cn.ps2.setInt(1, var1.intValue());
                System.out.println("--1.1.1 " + ((LoggableStatement) cn.ps2).getQueryString());
                cn.iduSQL(2);

                sql.delete(0, sql.length());
                sql.append("\n SELECT exito FROM inventario.transacciones_tmp WHERE exito = 'N'");
                cn.prepareStatementSEL(2, sql);
                System.out.println("--2 =" + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(2)) {
                    while (cn.rs2.next()) {
                        System.out.println("--3 =" + cn.rs2.getString("exito"));
                        if (cn.rs2.getString("exito").equals("N")) {
                            bool = false;
                        }
                    }

                    cn.cerrarPS(1);
                }

                if (bool) {
                    sql.delete(0, sql.length());
                    if (esTraslado == "SI") {
                        if (cn.getMotor().equals("sqlserver")) {
                            sql.append(" EXEC INVENTARIO.SP_CIERRA_DOCUMENTO_SALIDA_TRASLADO;  ");

                            cn.prepareStatementIDU(1, sql);
                            cn.iduSQL(1);
                        } else {
                            System.out.println("--POSTGRES A -");
                            if (bodega_doc() == 1) {
                                if (cierraDocumentoSalidaTraslado()) {
                                    System.out.println(
                                            " -******************************** Se Ejecuto sp cerrar el documento TRASLADO salida farmaci ***************************************************************-");
                                }
                            } else if (cierraDocumentoSalidaTraslado2()) {
                                System.out.println(
                                        " -******************************** Se Ejecuto sp cerrar el documento TRASLADO ***************************************************************-");
                            }
                        }
                    } else if (cn.getMotor().equals("sqlserver")) {
                        sql.append(" EXEC INVENTARIO.SP_CIERRA_DOCUMENTO_SALIDA;  ");

                        cn.prepareStatementIDU(1, sql);
                        cn.iduSQL(1);
                    } else {
                        System.out.println("--POSTGRES A -");
                        if (cierraDocumentoSalida()) {
                            System.out.println(" -************* Se Ejecuto sp cerrar el documento  **-");
                        } else {
                            bool = false;
                        }
                    }

                    System.out.println(" -** Se Ejecuto los procedimientos para cerrar el documentos  **-");
                    System.out.println("CEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEERRANDO "
                            + ((LoggableStatement) cn.ps1).getQueryString());
                }
            }
        } catch (SQLException localSQLException) {
            System.out.println(
                    localSQLException.getMessage() + " en método 1 hacerSalidasInventario(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 hacerSalidasInventario(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean cierraDocumentoSalida() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());
            if (cn.getMotor().equals("postgres")) {
                sql.append("\tINSERT INTO  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\t(  id_articulo,id_bodega,lote,existencia,fecha_vencimiento  )\t");
                sql.append("\t(\t");

                sql.append("\t\tSELECT tr.id_articulo, tr.id_bodega,tr.lote,\t");
                sql.append(" \t\t\tCASE WHEN  bel.existencia IS NULL THEN -tr.cantidad\t");
                sql.append("\t\t\tWHEN tr.cantidad > bel.existencia THEN bel.existencia - tr.cantidad\t");
                sql.append("\t\t\t  ELSE bel.existencia - tr.cantidad END existencias,\t");
                sql.append("\t\t\t  tr.fecha_vencimiento\t");
                sql.append(
                        "\t\tFROM (  \tSELECT t.id_articulo, doc.id_bodega , t.lote, sum(t.cantidad) cantidad, t.fecha_vencimiento, doc.id");
                sql.append(" \t\t\t\t   from inventario.transacciones_tmp t ");
                sql.append("\t\t\t    INNER JOIN inventario.documentos doc on t.id_documento = doc.id\t");
                sql.append("\t\t\t    WHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t\t    GROUP by  t.id_articulo, doc.id_bodega , t.lote, t.fecha_vencimiento, doc.id");
                sql.append("\t\t) tr            \t\t\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND tr.id_bodega = bel.id_bodega AND tr.lote = bel.lote)");
                sql.append("\t\tWHERE tr.lote <> ''");

                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias_lote\t");
                sql.append("\tWHERE  id_articulo IN (     SELECT  tr.id_articulo\t");
                sql.append("\t\t\tFROM  inventario.transacciones tr\t\t");
                sql.append("\t\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\tLEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\t\tWHERE doc.id =  ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("    )\t");
                sql.append("\t  AND id_bodega IN (SELECT   doc.id_bodega\t");
                sql.append("\t\tFROM   inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\tWHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t)\t");
                sql.append("\t AND lote IN (\t");
                sql.append("\t\tSELECT    tr.lote\t");
                sql.append("\t\tFROM   inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\tWHERE doc.id =( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t);\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias_lote\t");
                sql.append("\t(  id_articulo, id_bodega,lote,existencia,fecha_vencimiento )\t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT id_articulo, id_bodega,lote, existencia, fecha_vencimiento\t");
                sql.append("\t\tFROM  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\t);\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias_tmp\t");
                sql.append("\t(  id_articulo,id_bodega, existencia )\t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT tr.id_articulo, doc.id_bodega,\t");
                sql.append("\t\t  CASE WHEN  be.existencia IS NULL THEN - sum (tr.cantidad )\t");
                sql.append("\t\t\tWHEN sum (tr.cantidad ) > be.existencia THEN be.existencia - sum (tr.cantidad )\t");
                sql.append("\t\t  ELSE be.existencia - sum (tr.cantidad ) END existencias\t");
                sql.append("\t\tFROM  inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\tWHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)    ::integer\t");
                sql.append("\t\tgroup by tr.id_articulo,doc.id_bodega, be.existencia\t");
                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias\t");
                sql.append("\tWHERE id_articulo IN (SELECT tr.id_articulo FROM inventario.transacciones tr\t");
                sql.append("\t\t\t\t\t\t  INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\t\t\t\t  LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\t\t\t\t\t  WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t\t\t\t  )\t");
                sql.append("\tAND id_bodega IN (SELECT doc.id_bodega FROM  inventario.transacciones tr\t");
                sql.append("\t\t\t\t\t  INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\t\t\t  LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\t\t\t\t  WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t\t\t\t  );\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias\t");
                sql.append("\t(  id_articulo, id_bodega, existencia )\t");
                sql.append("\t(\t");
                sql.append("\t  SELECT id_articulo,id_bodega,existencia\t");
                sql.append("\t  FROM inventario.bodegas_existencias_tmp\t");
                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias_tmp ;\t");
                sql.append("\tDELETE FROM inventario.bodegas_existencias_lote_tmp ;\t");

                sql.append("\t   INSERT INTO inventario.inventarios_tmp\t");
                sql.append("\t(\t");
                sql.append("\t  id_articulo,\t");
                sql.append("\t  valor_unitario,\t");
                sql.append("\t  precio_venta,\t\t");
                sql.append("\t  existencia,\t");
                sql.append("\t  valor_total\t");
                sql.append("\t) \t");
                sql.append("\t (\t");
                sql.append("\t\t SELECT  \t");
                sql.append("\t\t  tr.id_articulo,  \t");
                sql.append("\t\t  CASE WHEN (invt.existencia-tr.cantidad) = 0 THEN '0'\t");
                sql.append("\t\t\t   ELSE ((invt.valor_total-tr.valor_total)/(invt.existencia-tr.cantidad))\t\t");
                sql.append("\t\t  END valor_unitario,\t");
                sql.append("\t\t  tr.precio_venta,\t");
                sql.append("\t\t  invt.existencia -tr.cantidad ,\t");
                sql.append("\t\t  invt.valor_total-tr.valor_total\t");
                sql.append("\t\tFROM (    \t");
                sql.append("\t\tSELECT trp.id_documento,\t");
                sql.append("\t\t\ttrp.id_articulo, \t");
                sql.append("\t\t\tsum (trp.valor_unitario) valor_unitario,\t");
                sql.append("\t\t\t0 precio_venta,\t");
                sql.append("\t\t\tsum(trp.cantidad) cantidad, \t");
                sql.append("\t\t\tsum (trp.valor_total) valor_total\t");
                sql.append("\t\t\tFROM inventario.transacciones_tmp  trp \t");
                sql.append("\t\tGROUP BY trp.id_articulo,  trp.id_documento ");
                sql.append("\t\t) tr  ");
                sql.append("\t\tINNER JOIN inventario.inventarios  invt ON tr.id_articulo = invt.id_articulo  ");
                sql.append("\t\tWHERE id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t);   ");

                sql.append("\tUPDATE inventario.transacciones_tmp ");
                sql.append("\tSET valor_unitario_promedio = inventario.inventarios_tmp.valor_unitario ");
                sql.append(
                        "\tFROM inventario.inventarios_tmp WHERE inventario.inventarios_tmp.id_articulo = inventario.transacciones_tmp.id_articulo; ");

                sql.append("\tUPDATE inventario.transacciones  ");
                sql.append("\tSET valor_unitario_promedio = inventario.transacciones_tmp.valor_unitario_promedio ");
                sql.append(
                        "\tFROM inventario.transacciones_tmp WHERE inventario.transacciones_tmp.id = inventario.transacciones.id; ");

                sql.append("\tDELETE FROM inventario.inventarios \t");
                sql.append("\tWHERE id_articulo IN (SELECT  tr.id_articulo  \t");
                sql.append("\t\t\t\t\t\t  FROM inventario.transacciones_tmp tr\t");
                sql.append("\t\t\t\t\t\t  LEFT JOIN inventario.inventarios inv on tr.id_articulo = inv.id_articulo \t");
                sql.append("\t\t\t\t\t\t  WHERE id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t);\t");

                sql.append("\tINSERT INTO inventario.inventarios\t");
                sql.append("\t(\t");
                sql.append("\t  id_articulo,\t");
                sql.append("\t  valor_unitario,\t");
                sql.append("\t  precio_venta,\t");
                sql.append("\t  existencia,\t");
                sql.append("\t  valor_total\t");
                sql.append("\t) \t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT \t");
                sql.append("\t\tid_articulo,\t");
                sql.append("\t\tvalor_unitario,\t");
                sql.append("\t\tprecio_venta,\t");
                sql.append("\t\texistencia,\t");
                sql.append("\t\tvalor_total\t");
                sql.append("\t\tFROM inventario.inventarios_tmp\t");
                sql.append("\t);\t");

                sql.append("DELETE FROM inventario.inventarios_tmp ;\t");

                sql.append("\tupdate  inventario.documentos\t");
                sql.append("\tset  fecha_cierre = now(),\t");
                sql.append("\t  id_usuario_cierra = ( SELECT id_usuario FROM inventario.candado),\t");
                sql.append("\t  id_estado = 1\t");
                sql.append("\tWHERE  id = ( SELECT id_doc FROM inventario.candado)::integer;\t");
            }
            cn.prepareStatementIDU(1, sql);
            System.out.println("-- cierra documento --");
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out
                    .println(localSQLException.getMessage() + " en método 1 cierraDocumentoSalida(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 cierraDocumentoSalida(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean cierraDocumentoSalidaTraslado() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());
            if (cn.getMotor().equals("postgres")) {

                sql.append("\n UPDATE inventario.transacciones  ");
                sql.append("\n set estado = 'N'  ");
                sql.append("\n where id in ( ");
                sql.append("\n   select id_transaccion  ");
                sql.append("\n from inventario.vi_traslado_devolucion       ");
                sql.append("\n   where id_documento = (select id_doc from inventario.candado) ");
                sql.append("\n ); ");

                sql.append("\n INSERT INTO  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\n (  id_articulo,id_bodega,lote,existencia,fecha_vencimiento  )\t");
                sql.append("\n (\t");
                sql.append("\n SELECT tr.id_articulo, tr.id_bodega,tr.lote,\t");
                sql.append("\n CASE WHEN  bel.existencia IS NULL THEN -tr.cantidad\t");
                sql.append("\n WHEN tr.cantidad > bel.existencia THEN bel.existencia - tr.cantidad\t");
                sql.append("\n  ELSE bel.existencia - tr.cantidad END existencias,\t");
                sql.append("\n  tr.fecha_vencimiento\t");
                sql.append(
                        "\n FROM (  \tSELECT t.id_articulo, doc.id_bodega , t.lote, sum(t.cantidad) cantidad, t.fecha_vencimiento, doc.id");
                sql.append("\n   from inventario.transacciones_tmp t ");
                sql.append("\n    INNER JOIN inventario.documentos doc on t.id_documento = doc.id\t");
                sql.append("\n    WHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n    GROUP by  t.id_articulo, doc.id_bodega , t.lote, t.fecha_vencimiento, doc.id");
                sql.append("\n ) tr            \t\t\t");
                sql.append(
                        "\n LEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND tr.id_bodega = bel.id_bodega AND tr.lote = bel.lote)");
                sql.append("\n WHERE tr.lote <> ''");
                sql.append("\n );\t");

                sql.append("\n DELETE FROM inventario.bodegas_existencias_lote\t");
                sql.append("\n WHERE  id_articulo IN (     SELECT  tr.id_articulo\t");
                sql.append("\n FROM  inventario.transacciones tr\t\t");
                sql.append("\n INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n LEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\n WHERE doc.id =  ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n )");
                sql.append("\n  AND id_bodega IN (SELECT   doc.id_bodega\t");
                sql.append("\n FROM   inventario.transacciones tr\t");
                sql.append("\n INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n LEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\n WHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n )\t");
                sql.append("\n  AND lote IN (\t");
                sql.append("\n SELECT    tr.lote\t");
                sql.append("\n FROM   inventario.transacciones tr\t");
                sql.append("\n INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n LEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\n WHERE doc.id =( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n );");

                sql.append("\n INSERT INTO inventario.bodegas_existencias_lote\t");
                sql.append("\n (  id_articulo, id_bodega,lote,existencia,fecha_vencimiento )\t");
                sql.append("\n (\t");
                sql.append("\n SELECT id_articulo, id_bodega,lote, existencia, fecha_vencimiento\t");
                sql.append("\n FROM  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\n );\t");

                sql.append("\n INSERT INTO inventario.bodegas_existencias_tmp\t");
                sql.append("\n (  id_articulo,id_bodega, existencia )\t");
                sql.append("\n (\t");
                sql.append("\n SELECT tr.id_articulo, doc.id_bodega,\t");
                sql.append("\n   CASE WHEN  be.existencia IS NULL THEN - sum (tr.cantidad )\t");
                sql.append("\n WHEN sum (tr.cantidad ) > be.existencia THEN be.existencia - sum (tr.cantidad )\t");
                sql.append("\n   ELSE be.existencia - sum (tr.cantidad ) END existencias\t");
                sql.append("\n FROM  inventario.transacciones tr\t");
                sql.append("\n INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\n WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)    ::integer\t");
                sql.append("\n group by tr.id_articulo,doc.id_bodega, be.existencia\t");
                sql.append("\n );\t");

                sql.append("\n DELETE FROM inventario.bodegas_existencias\t");
                sql.append("\n WHERE id_articulo IN (SELECT tr.id_articulo FROM inventario.transacciones tr\t");
                sql.append("\n   INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n   LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\n   WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n   )\t");
                sql.append("\n AND id_bodega IN (SELECT doc.id_bodega FROM  inventario.transacciones tr\t");
                sql.append("\n  INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\n  LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\n   WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n  );\t");

                sql.append("\n INSERT INTO inventario.bodegas_existencias\t");
                sql.append("\n (  id_articulo, id_bodega, existencia )\t");
                sql.append("\n (\t");
                sql.append("\n   SELECT id_articulo,id_bodega,existencia\t");
                sql.append("\n   FROM inventario.bodegas_existencias_tmp\t");
                sql.append("\n );\t");

                sql.append("\n DELETE FROM inventario.bodegas_existencias_tmp ;\t");
                sql.append("\n DELETE FROM inventario.bodegas_existencias_lote_tmp ;\t");

                sql.append("\n    INSERT INTO inventario.inventarios_tmp\t");
                sql.append("\n (\t");
                sql.append("\n   id_articulo,\t");
                sql.append("\n   valor_unitario,\t");
                sql.append("\n   precio_venta,\t\t");
                sql.append("\n   existencia,\t");
                sql.append("\n   valor_total\t");
                sql.append("\n ) \t");
                sql.append("\n  (\t");
                sql.append("\n  SELECT  \t");
                sql.append("\n   tr.id_articulo,  \t");
                sql.append("\n   CASE WHEN (invt.existencia-tr.cantidad) = 0 THEN '0'\t");
                sql.append("\n    ELSE ((invt.valor_total-tr.valor_total)/(invt.existencia-tr.cantidad))\t\t");
                sql.append("\n   END valor_unitario,\t");
                sql.append("\n   tr.precio_venta,\t");
                sql.append("\n   invt.existencia -tr.cantidad ,\t");
                sql.append("\n   invt.valor_total-tr.valor_total\t");
                sql.append("\n FROM (    \t");
                sql.append("\n SELECT trp.id_documento,\t");
                sql.append("\n trp.id_articulo, \t");
                sql.append("\n sum (trp.valor_unitario) valor_unitario,\t");
                sql.append("\n 0 precio_venta,\t");
                sql.append("\n sum(trp.cantidad) cantidad, \t");
                sql.append("\n sum (trp.valor_total) valor_total\t");
                sql.append("\n FROM inventario.transacciones_tmp  trp \t");
                sql.append("\n GROUP BY trp.id_articulo,  trp.id_documento ");
                sql.append("\n ) tr  ");
                sql.append("\n INNER JOIN inventario.inventarios  invt ON tr.id_articulo = invt.id_articulo  ");
                sql.append("\n WHERE id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n );   ");

                sql.append("\n UPDATE inventario.transacciones_tmp ");
                sql.append("\n SET valor_unitario_promedio = inventario.inventarios_tmp.valor_unitario ");
                sql.append(
                        "\n FROM inventario.inventarios_tmp WHERE inventario.inventarios_tmp.id_articulo = inventario.transacciones_tmp.id_articulo; ");

                sql.append("\n UPDATE inventario.transacciones  ");
                sql.append("\n SET valor_unitario_promedio = inventario.transacciones_tmp.valor_unitario_promedio ");
                sql.append(
                        "\n FROM inventario.transacciones_tmp WHERE inventario.transacciones_tmp.id = inventario.transacciones.id; ");

                sql.append("\n DELETE FROM inventario.inventarios \t");
                sql.append("\n WHERE id_articulo IN (SELECT  tr.id_articulo  \t");
                sql.append("\n  FROM inventario.transacciones_tmp tr\t");
                sql.append("\n  LEFT JOIN inventario.inventarios inv on tr.id_articulo = inv.id_articulo \t");
                sql.append("\n  WHERE id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\n);\t");

                sql.append("\n INSERT INTO inventario.inventarios\t");
                sql.append("\n (\t");
                sql.append("\n   id_articulo,\t");
                sql.append("\n   valor_unitario,\t");
                sql.append("\n   precio_venta,\t");
                sql.append("\n   existencia,\t");
                sql.append("\n   valor_total\t");
                sql.append("\n ) \t");
                sql.append("\n (\t");
                sql.append("\n SELECT \t");
                sql.append("\n id_articulo,\t");
                sql.append("\n valor_unitario,\t");
                sql.append("\n precio_venta,\t");
                sql.append("\n existencia,\t");
                sql.append("\n valor_total\t");
                sql.append("\n FROM inventario.inventarios_tmp\t");
                sql.append("\n );\t");

                sql.append("DELETE FROM inventario.inventarios_tmp ;\t");

                sql.append("\n update  inventario.documentos\t");
                sql.append("\n set  fecha_cierre = now(),\t");
                sql.append("\n   id_usuario_cierra = ( SELECT id_usuario FROM inventario.candado),\t");
                sql.append("\n   id_estado = 1\t");
                sql.append("\n WHERE  id = ( SELECT id_doc FROM inventario.candado)::integer;\t");
            }
            cn.prepareStatementIDU(1, sql);
            System.out.println("-- cierra documento --");
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out.println(
                    localSQLException.getMessage() + " en método 1 cierraDocumentoSalidaTraslado(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(
                    localException.getMessage() + " en método 2 cierraDocumentoSalidaTraslado(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean cierraDocumentoSalidaTraslado2() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());
            if (cn.getMotor().equals("postgres")) {

                sql.append(" UPDATE inventario.transacciones  ");
                sql.append(" set estado = 'N'  ");
                sql.append(" where id in ( ");
                sql.append("   select id_transaccion  ");
                sql.append("\tfrom inventario.vi_traslado_devolucion       ");
                sql.append("    where id_documento = (select id_doc from inventario.candado) ");
                sql.append(" ); ");

                sql.append("\tINSERT INTO  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\t(  id_articulo,id_bodega,lote,existencia,fecha_vencimiento  )\t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT tr.id_articulo, tr.id_bodega,tr.lote,\t ");
                sql.append("\t\t  CASE WHEN  bel.existencia IS NULL THEN -tr.cantidad\t ");
                sql.append("\t\t\t\tWHEN tr.cantidad > bel.existencia THEN bel.existencia - tr.cantidad\t ");
                sql.append("\t\t\t ELSE bel.existencia - tr.cantidad  ");
                sql.append("             END existencias, ");
                sql.append("          tr.fecha_vencimiento ");
                sql.append("        from ( ");
                sql.append(
                        "       \tSELECT t.id_articulo, doc.id_bodega , t.lote, sum(t.cantidad) cantidad, t.fecha_vencimiento, doc.id ");
                sql.append("           from inventario.transacciones t  ");
                sql.append("            INNER JOIN inventario.documentos doc on t.id_documento = doc.id\t");
                sql.append("            WHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t ");
                sql.append("            GROUP by  t.id_articulo, doc.id_bodega , t.lote, t.fecha_vencimiento, doc.id ");
                sql.append("        ) tr ");
                sql.append(
                        "        LEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND tr.id_bodega = bel.id_bodega AND tr.lote = bel.lote) ");
                sql.append("        WHERE  tr.lote <> '' ");
                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias_lote\t");
                sql.append("\tWHERE  id_articulo IN (     SELECT  tr.id_articulo\t");
                sql.append("\t\t\tFROM  inventario.transacciones tr\t\t");
                sql.append("\t\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\tLEFT JOIN inventario.bodegas_existencias_lote bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\t\tWHERE doc.id =  ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("    )\t");
                sql.append("\t  AND id_bodega IN (SELECT   doc.id_bodega\t");
                sql.append("\t\tFROM   inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\tWHERE doc.id = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t)\t");
                sql.append("\t AND lote IN (\t");
                sql.append("\t\tSELECT    tr.lote\t");
                sql.append("\t\tFROM   inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias_lote_tmp bel on ( tr.id_articulo = bel.id_articulo AND doc.id_bodega = bel.id_bodega AND tr.lote = bel.lote)\t");
                sql.append("\t\tWHERE doc.id =( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t);\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias_lote\t");
                sql.append("\t(  id_articulo, id_bodega,lote,existencia,fecha_vencimiento )\t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT id_articulo, id_bodega,lote, existencia, fecha_vencimiento\t");
                sql.append("\t\tFROM  inventario.bodegas_existencias_lote_tmp\t");
                sql.append("\t);\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias_tmp\t");
                sql.append("\t(  id_articulo,id_bodega, existencia )\t");
                sql.append("\t(\t");
                sql.append("\t\tSELECT tr.id_articulo, doc.id_bodega,\t");
                sql.append("\t\t  CASE WHEN  be.existencia IS NULL THEN - sum (tr.cantidad )\t");
                sql.append("\t\t\tWHEN sum (tr.cantidad ) > be.existencia THEN be.existencia - sum (tr.cantidad )\t");
                sql.append("\t\t  ELSE be.existencia - sum (tr.cantidad ) END existencias\t");
                sql.append("\t\tFROM  inventario.transacciones tr\t");
                sql.append("\t\tINNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\tLEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\tWHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)    ::integer\t");
                sql.append("\t\tgroup by tr.id_articulo,doc.id_bodega, be.existencia\t");
                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias\t");
                sql.append("\tWHERE id_articulo IN (SELECT tr.id_articulo FROM inventario.transacciones tr\t");
                sql.append("\t\t\t\t\t\t  INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\t\t\t\t  LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\t\t\t\t\t  WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t\t\t\t  )\t");
                sql.append("\tAND id_bodega IN (SELECT doc.id_bodega FROM  inventario.transacciones tr\t");
                sql.append("\t\t\t\t\t  INNER JOIN inventario.documentos doc on tr.id_documento = doc.id\t");
                sql.append(
                        "\t\t\t\t\t  LEFT JOIN inventario.bodegas_existencias be on (tr.id_articulo = be.id_articulo AND doc.id_bodega = be.id_bodega)\t");
                sql.append("\t\t\t\t\t  WHERE tr.id_documento = ( SELECT id_doc FROM inventario.candado)::integer\t");
                sql.append("\t\t\t\t\t  );\t");

                sql.append("\tINSERT INTO inventario.bodegas_existencias\t");
                sql.append("\t(  id_articulo, id_bodega, existencia )\t");
                sql.append("\t(\t");
                sql.append("\t  SELECT id_articulo,id_bodega,existencia\t");
                sql.append("\t  FROM inventario.bodegas_existencias_tmp\t");
                sql.append("\t);\t");

                sql.append("\tDELETE FROM inventario.bodegas_existencias_tmp ;\t");
                sql.append("\tDELETE FROM inventario.bodegas_existencias_lote_tmp ;\t");

                sql.append("\tupdate  inventario.documentos\t");
                sql.append("\tset  fecha_cierre = now(),\t");
                sql.append("\t  id_usuario_cierra = ( SELECT id_usuario FROM inventario.candado),\t");
                sql.append("\t  id_estado = 1\t");
                sql.append("\tWHERE  id = ( SELECT id_doc FROM inventario.candado)::integer;\t");
            }
            cn.prepareStatementIDU(1, sql);
            System.out.println(
                    "-- cierra documento TRASLADO ********************************************************* --");
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out
                    .println(localSQLException.getMessage() + " en método 1 cierraDocumentoSalida(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 cierraDocumentoSalida(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean limpiarCandados() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());
            sql.append("\n delete from inventario.transaccion_modifica ;");
            sql.append("\n DELETE FROM INVENTARIO.BODEGAS_EXISTENCIAS_TMP; ");
            sql.append("\n DELETE FROM INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP; ");

            sql.append("\n delete from inventario.candado; DELETE FROM  INVENTARIO.CANDADO_TMP;  ");
            sql.append("\n delete from inventario.candado_documentos;  ");
            sql.append("\n DELETE FROM INVENTARIO.TRANSACCIONES_TMP;  ");

            sql.append("\n DELETE FROM INVENTARIO.inventarios_tmp; ");
            sql.append("\n DELETE FROM INVENTARIO.promedio_articulo;; ");

            cn.prepareStatementIDU(1, sql);
            System.out.println("LLLLLLLLLLLLLIMPIANDO " + ((LoggableStatement) cn.ps1).getQueryString());
            cn.iduSQL(1);
        } catch (SQLException localSQLException) {
            System.out.println(localSQLException.getMessage() + " en método 1 limpiarCandados(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 limpiarCandados(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean hacerTrasladoEntreBodegas() {
        System.out.println(
                "-----------------------------------------------------------------------------------------\n hacerTrasladoEntreBodegas::ID_DOC origen= "
                        + var1 + " id_BODEGA_destino=" + var6);
        esTraslado = "SI";
        boolean bool = true;
        noticia = "xxx";
        try {
            cn.iniciatransaccion();
            if (hacerSalidasInventario()) {
                // verificar la bodega de entradas

                if (var6 == 1) {
                    if (hacerEntradasInventarioFarmacia()) {
                        noticia = "DOCUMENTOS FINALIZADOS CON EXITO.";

                        sql.delete(0, sql.length());
                        sql.append("\n UPDATE inventario.documentos ");
                        sql.append(
                                "\n SET fecha_crea = (select fecha_crea from inventario.documentos where id = (select id_doc from inventario.candado)), ");
                        sql.append(
                                "\n   fecha_cierre = (select fecha_cierre from inventario.documentos where id = (select id_doc from inventario.candado)),"
                                        + " id_documento_movimiento = (select id_doc from inventario.candado) ");
                        sql.append("\n WHERE id =( SELECT  id_doc FROM inventario.candado_documentos ); ");

                        sql.append("\n UPDATE inventario.documentos "
                                + "set id_documento_movimiento = ( SELECT  id_doc FROM inventario.candado_documentos )"
                                + " WHERE id = (select id_doc from inventario.candado);");

                        cn.prepareStatementIDU(2, sql);
                        System.out.println(
                                "-- UPDATE FECHAS DOC DE TRASLADO -- " + ((LoggableStatement) cn.ps2).getQueryString());
                        cn.iduSQL(2);

                        limpiarCandados();
                    } else {
                        noticia = "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS 2";
                        return false;
                    }
                } else if (hacerEntradasInventario()) {
                    noticia = "DOCUMENTOS DE TRASLADOS FINALIZADOS CON EXITO";

                    sql.delete(0, sql.length());
                    sql.append("\n UPDATE inventario.documentos ");
                    sql.append(
                            "\n SET fecha_crea = (select fecha_crea from inventario.documentos where id = (select id_doc from inventario.candado)), ");
                    sql.append(
                            "\n   fecha_cierre = (select fecha_cierre from inventario.documentos where id = (select id_doc from inventario.candado)) ,"
                                    + " id_documento_movimiento = (select id_doc from inventario.candado) ");
                    sql.append("\n WHERE id =( SELECT  id_doc FROM inventario.candado_documentos ) ; ");

                    sql.append("\n UPDATE inventario.documentos "
                            + "set id_documento_movimiento = ( SELECT  id_doc FROM inventario.candado_documentos )"
                            + " WHERE id = (select id_doc from inventario.candado);");

                    cn.prepareStatementIDU(2, sql);
                    System.out.println(
                            "-- UPDATE FECHAS DOC DE TRASLADO -- " + ((LoggableStatement) cn.ps2).getQueryString());
                    cn.iduSQL(2);

                    limpiarCandados();
                } else {
                    noticia = "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS 2";
                    return false;
                }
            } else {
                noticia = "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS";
                limpiarCandados();
            }
            cn.fintransaccion();
        } catch (Exception localException) {
            System.out.println(
                    localException.getMessage() + " en método 2 verificaTransaccionesInventario(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean hacerEntradasInventarioFarmacia() {
        System.out.println(
                "-----------------------------------------------------------------------------------------\n HacerEntradasInventario::ID_DOC origen= "
                        + var1 + " id_BODEGA_destino=" + var6);

        boolean bool = true;
        String ide_us = "";
        try {
            sql.delete(0, sql.length());

            if (cn.getMotor().equals("sqlserver")) {
                sql.append(
                        "\n INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(ID)+1 FROM inventario.documentos ); ");
                sql.append(
                        "\n INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_estado, id_usuario_crea)  ");
                sql.append("\n VALUES ((SELECT id_doc FROM inventario.candado_documentos),?,?,?,?); ");
            } else {
                System.out.println("--POSTGRES B ");
                sql.append(
                        "\n INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(ID)+1 FROM inventario.documentos ); ");
                sql.append(
                        "\n INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_estado, id_usuario_crea)  ");
                sql.append(
                        "\n VALUES ((SELECT id_doc FROM inventario.candado_documentos)::integer,?::integer,?::integer,?,?); ");
            }
            ide_us = cn.usuario.getIdentificacion();
            while (ide_us.equals("")) {
                ide_us = cn.usuario.getIdentificacion();
            }
            cn.prepareStatementIDU(1, sql);
            cn.ps1.setString(1, "20");
            cn.ps1.setInt(2, var6.intValue());
            cn.ps1.setInt(3, 1);
            cn.ps1.setString(4, ide_us);

            if (cn.imprimirConsola) {
                System.out.println("--CREACION doc:n " + ((LoggableStatement) cn.ps1).getQueryString());
            }
            cn.iduSQL(1);

            sql.delete(0, sql.length());

            System.out.println("--POSTGRES B *********************************** ");

            sql.append("\n INSERT INTO INVENTARIO.TRANSACCIONES_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID, ");
            sql.append("\n   ID_DOCUMENTO, ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   CANTIDAD, ");
            sql.append("\n   VALOR_UNITARIO, ");
            sql.append("\n   IVA, ");
            sql.append("\n   VALOR_IMPUESTO, ");
            sql.append("\n   ID_NATURALEZA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   SERIAL, ");
            sql.append("\n   PRECIO_VENTA, ");
            sql.append("\n   FECHA_VENCIMIENTO, ");
            sql.append("\n   EXITO, ");
            sql.append("\n   valor_unitario_promedio, ");
            sql.append("\n   VALOR_TOTAL ");
            sql.append("\n ) ( ");
            sql.append("\n    SELECT ");
            sql.append("\n    tr.ID, ");
            sql.append("\n     ( SELECT ID_DOC FROM INVENTARIO.candado_documentos), ");
            sql.append("\n     tr.ID_ARTICULO, ");
            sql.append("\n     tr.CANTIDAD, ");
            sql.append("\n     tr.VALOR_UNITARIO, ");
            sql.append("\n     tr.IVA, ");
            sql.append("\n     tr.VALOR_IMPUESTO, ");
            sql.append("\n     tr.ID_NATURALEZA, ");
            sql.append("\n     tr.LOTE, ");
            sql.append("\n     tr.SERIAL, ");
            sql.append("\n     tr.PRECIO_VENTA, ");
            sql.append("\n     tr.FECHA_VENCIMIENTO, ");
            sql.append("\n     tr.EXITO, ");
            sql.append("\n     tr.valor_unitario_promedio, ");
            sql.append("\n     (tr.cantidad*(tr.valor_unitario+tr.valor_impuesto+documento.vlr_flete)) ");
            sql.append("\n   FROM INVENTARIO.TRANSACCIONES tr ");
            sql.append("\n   inner join ( ");
            sql.append("\n\t select (doc.valor_flete/sum (tr.cantidad)) vlr_flete, doc.id ");
            sql.append("\n\t from inventario.transacciones tr  ");
            sql.append("\n\t inner join inventario.documentos doc on tr.id_documento = doc.id ");
            sql.append("\n\t where doc.id =( SELECT id_doc FROM inventario.candado)::integer ");
            sql.append("\n\t group by doc.valor_flete, doc.id ");
            sql.append("\n   ) documento on tr.id_documento = documento.id ");
            sql.append("\n   WHERE tr.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO) ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.TRANSACCIONES ");
            sql.append("\n ( ");
            sql.append("\n   ID_DOCUMENTO, ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   CANTIDAD, ");
            sql.append("\n   VALOR_UNITARIO, ");
            sql.append("\n   IVA, ");
            sql.append("\n   VALOR_IMPUESTO, ");
            sql.append("\n   ID_NATURALEZA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   SERIAL, ");
            sql.append("\n   PRECIO_VENTA, ");
            sql.append("\n   FECHA_VENCIMIENTO, ");
            sql.append("\n   EXITO, ");
            sql.append("\n   valor_unitario_promedio ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n   SELECT  ");
            sql.append("\n     ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS), /* DOC CANDADO DESTINO */ ");
            sql.append("\n     ID_ARTICULO, ");
            sql.append("\n     CANTIDAD, ");
            sql.append("\n     VALOR_UNITARIO, ");
            sql.append("\n     IVA, ");
            sql.append("\n     VALOR_IMPUESTO, ");
            sql.append("\n     'I' ID_NATURALEZA, ");
            sql.append("\n     LOTE, ");
            sql.append("\n     SERIAL, ");
            sql.append("\n     PRECIO_VENTA, ");
            sql.append("\n     FECHA_VENCIMIENTO, ");
            sql.append("\n     EXITO, ");
            sql.append("\n     valor_unitario_promedio ");
            sql.append("\n   FROM  INVENTARIO.TRANSACCIONES_TMP  ");
            sql.append("\n   WHERE ID_DOCUMENTO = (SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   EXISTENCIA, ");
            sql.append("\n   FECHA_VENCIMIENTO ");
            sql.append("\n )  ");
            sql.append("\n  ( ");
            sql.append("\n      SELECT  ");
            sql.append("\n           TR.ID_ARTICULO, ");
            sql.append("\n           DOC.ID_BODEGA, ");
            sql.append("\n           TR.LOTE, ");
            sql.append(
                    "\n           CASE WHEN  BEL.EXISTENCIA IS NULL THEN TR.CANTIDAD ELSE   TR.CANTIDAD + BEL.EXISTENCIA END EXISTENCIAS, TR.FECHA_VENCIMIENTO ");
            sql.append("\n\t\tFROM ( ");
            sql.append("\n\t\t\t\tSELECT      ");
            sql.append("\n\t\t\t\t  id_documento, ");
            sql.append("\n\t\t\t\t  id_articulo, ");
            sql.append("\n\t\t\t\t  sum(cantidad) cantidad, ");
            sql.append("\n\t\t\t\t  lote  , fecha_vencimiento      ");
            sql.append("\n\t\t\t\tFROM inventario.transacciones_tmp ");
            sql.append("\n\t\t\t\tGROUP BY id_documento, id_articulo,lote, fecha_vencimiento ");
            sql.append("\n\t\t  ) TR ");

            sql.append("\n         INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n         LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n         WHERE DOC.ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n         AND TR.LOTE <> '' ");
            sql.append("\n ); ");

            sql.append("\n DELETE FROM  ");
            sql.append("\n   INVENTARIO.BODEGAS_EXISTENCIAS_LOTE  ");
            sql.append("\n WHERE  ID_ARTICULO IN ( SELECT TR.ID_ARTICULO ");
            sql.append("\n         FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n         INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n         LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n         WHERE DOC.ID =  ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n   ) ");
            sql.append("\n   AND ID_BODEGA IN (SELECT DOC.ID_BODEGA ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n     WHERE DOC.ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     ) ");
            sql.append("\n  AND LOTE IN (SELECT  TR.LOTE ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n     WHERE DOC.ID =  ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_LOTE ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   EXISTENCIA, ");
            sql.append("\n   FECHA_VENCIMIENTO ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n     SELECT        ");
            sql.append("\n       ID_ARTICULO, ");
            sql.append("\n       ID_BODEGA, ");
            sql.append("\n       LOTE, ");
            sql.append("\n       EXISTENCIA, ");
            sql.append("\n       FECHA_VENCIMIENTO ");
            sql.append("\n     FROM INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP   ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   EXISTENCIA ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n \tSELECT  ");
            sql.append("\n       TR.ID_ARTICULO, ");
            sql.append("\n       DOC.ID_BODEGA, ");
            sql.append(
                    "\n       CASE WHEN  BE.EXISTENCIA IS NULL THEN SUM(TR.CANTIDAD) ELSE SUM(TR.CANTIDAD) + BE.EXISTENCIA END EXISTENCIAS ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append("\n     WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     GROUP BY tr.id_articulo,doc.id_bodega,be.existencia ");
            sql.append("\n ); ");

            sql.append("\n DELETE FROM INVENTARIO.BODEGAS_EXISTENCIAS  ");
            sql.append("\n WHERE ID_ARTICULO IN (SELECT TR.ID_ARTICULO  ");
            sql.append("\n                       FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n                       INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n                       LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append(
                    "\n                       WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n                   ) ");
            sql.append("\n AND ID_BODEGA IN (SELECT DOC.ID_BODEGA ");
            sql.append("\n                   FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n                   INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n                   LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append(
                    "\n                   WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n                   ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   EXISTENCIA ");
            sql.append("\n )  ");
            sql.append("\n  ( ");
            sql.append("\n   SELECT  ");
            sql.append("\n     ID_ARTICULO, ");
            sql.append("\n     ID_BODEGA, ");
            sql.append("\n     EXISTENCIA ");
            sql.append("\n   FROM INVENTARIO.BODEGAS_EXISTENCIAS_TMP ");
            sql.append(" ); ");

            /* para traslados entradas */
            /* MOVIMIENTO DE inventarios */
            sql.append("\n INSERT INTO inventario.inventarios_tmp(  ");
            sql.append("\n  id_articulo, ");
            sql.append("\n  valor_unitario, ");
            sql.append("\n  precio_venta, ");
            sql.append("\n  existencia, ");
            sql.append("\n  valor_total ");
            sql.append("\n )(	  ");
            sql.append("\n	SELECT   ");
            sql.append("\n	  tr.id_articulo,   ");
            sql.append("\n	  ((tr.valor_total+invt.valor_total)/(tr.cantidad  + invt.existencia)), ");
            sql.append("\n	  tr.precio_venta, ");
            sql.append("\n	  tr.cantidad  + invt.existencia, ");
            sql.append("\n	  tr.valor_total+invt.valor_total  ");
            sql.append("\n	FROM ( ");
            sql.append("\n		SELECT  ");
            sql.append("\n		  trp.id_documento,	 ");
            sql.append("\n		  trp.id_articulo, ");
            sql.append("\n		  sum (trp.valor_unitario) valor_unitario, ");
            sql.append("\n		  0 precio_venta, ");
            sql.append("\n		  sum(trp.cantidad) cantidad,      ");
            sql.append("\n		  sum (trp.valor_total) valor_total ");
            sql.append("\n		FROM inventario.transacciones_tmp  trp   ");
            sql.append("\n		GROUP BY trp.id_articulo,  trp.id_documento ");
            sql.append("\n	) tr     ");
            sql.append("\n	INNER JOIN inventario.inventarios  invt ON tr.id_articulo = invt.id_articulo ");
            sql.append("\n	WHERE id_documento = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n );   ");

            /**/
            sql.append("\n	UPDATE inventario.transacciones_tmp  ");
            sql.append("\n	SET valor_unitario_promedio = inventario.inventarios_tmp.valor_unitario ");
            sql.append(
                    "\n	FROM inventario.inventarios_tmp WHERE inventario.inventarios_tmp.id_articulo = inventario.transacciones_tmp.id_articulo; ");

            /* actualizar transacciones */
            sql.append("\n	UPDATE inventario.transacciones  ");
            sql.append("\n	SET valor_unitario_promedio = inventario.transacciones_tmp.valor_unitario_promedio ");
            sql.append(
                    "\n	FROM inventario.transacciones_tmp WHERE inventario.transacciones_tmp.id = inventario.transacciones.id; ");

            /**/

            /* ELIMINAR DE LA TABLA LOS REGISTROS PARA ACTUALIZAR */
            sql.append("\n DELETE FROM inventario.inventarios  ");
            sql.append("\n WHERE id_articulo IN (SELECT  tr.id_articulo   ");
            sql.append("\n					  FROM inventario.transacciones_tmp tr ");
            sql.append(
                    "\n					  LEFT JOIN inventario.inventarios inv ON tr.id_articulo = inv.id_articulo  ");
            sql.append(
                    "\n					  WHERE id_documento = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n );  ");

            /* INSERTAR EN inventario */
            sql.append("\n INSERT INTO inventario.inventarios( ");
            sql.append("\n   id_articulo, ");
            sql.append("\n  valor_unitario, ");
            sql.append("\n  precio_venta, ");
            sql.append("\n  existencia, ");
            sql.append("\n  valor_total ");
            sql.append("\n ) ( ");
            sql.append("\n	SELECT  ");
            sql.append("\n	id_articulo, ");
            sql.append("\n	valor_unitario, ");
            sql.append("\n	precio_venta, ");
            sql.append("\n	existencia, ");
            sql.append("\n	valor_total ");
            sql.append("\n	FROM inventario.inventarios_tmp ");
            sql.append("\n );	 ");

            /* fin */
            ide_us = "";
            ide_us = cn.usuario.getIdentificacion();
            while (ide_us.equals("")) {
                ide_us = cn.usuario.getIdentificacion();
            }
            sql.append("\n UPDATE  INVENTARIO.DOCUMENTOS   ");
            sql.append("\n SET  ");
            sql.append("\n   FECHA_CIERRE = now(), ");
            sql.append("\n   ID_USUARIO_CIERRA = " + cn.usuario.getIdentificacion() + ", ");
            sql.append("\n   ID_ESTADO = 1 ");
            sql.append("\n WHERE ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer; ");

            cn.prepareStatementIDU(1, sql);
            if (cn.imprimirConsola) {
                System.out.println("--CREACION doc:n " + ((LoggableStatement) cn.ps1).getQueryString());
            }
            cn.iduSQL(1);

        } catch (SQLException localSQLException) {
            System.out.println(
                    localSQLException.getMessage() + " en método hacerEntradasInventario() de DocumentoHc.java");
            cn.exito = false;
        } catch (Exception localException) {
            System.out
                    .println(localException.getMessage() + " en método hacerEntradasInventario() de DocumentoHc.java");
            cn.exito = false;
        }

        return bool;
    }

    public boolean hacerEntradasInventario() {
        System.out.println(
                "-----------------------------------------------------------------------------------------\n HacerEntradasInventario::ID_DOC origen= "
                        + var1 + " id_BODEGA_destino=" + var6);

        boolean bool = true;
        String ide_us = "";
        try {
            sql.delete(0, sql.length());

            if (cn.getMotor().equals("sqlserver")) {
                sql.append(
                        "\n INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(ID)+1 FROM inventario.documentos ); ");
                sql.append(
                        "\n INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_estado, id_usuario_crea)  ");
                sql.append("\n VALUES ((SELECT id_doc FROM inventario.candado_documentos),?,?,?,?); ");
            } else {
                System.out.println("--POSTGRES B ");
                sql.append(
                        "\n INSERT INTO inventario.candado_documentos(id_doc) ( SELECT MAX(ID)+1 FROM inventario.documentos ); ");
                sql.append(
                        "\n INSERT INTO inventario.documentos (id, id_documento_tipo, id_bodega, id_estado, id_usuario_crea)  ");
                sql.append(
                        "\n VALUES ((SELECT id_doc FROM inventario.candado_documentos)::integer,?::integer,?::integer,?,?); ");
            }
            ide_us = cn.usuario.getIdentificacion();
            while (ide_us.equals("")) {
                ide_us = cn.usuario.getIdentificacion();
            }
            cn.prepareStatementIDU(1, sql);
            cn.ps1.setString(1, "20");
            cn.ps1.setInt(2, var6.intValue());
            /* bodega destino */
            cn.ps1.setInt(3, 1);
            cn.ps1.setString(4, ide_us);

            if (cn.imprimirConsola) {
                System.out.println("--CREACION doc:n " + ((LoggableStatement) cn.ps1).getQueryString());
            }
            cn.iduSQL(1);

            sql.delete(0, sql.length());

            System.out.println("--POSTGRES B *********************************** ");

            sql.append("\n INSERT INTO INVENTARIO.TRANSACCIONES_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID, ");
            sql.append("\n   ID_DOCUMENTO, ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   CANTIDAD, ");
            sql.append("\n   VALOR_UNITARIO, ");
            sql.append("\n   IVA, ");
            sql.append("\n   VALOR_IMPUESTO, ");
            sql.append("\n   ID_NATURALEZA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   SERIAL, ");
            sql.append("\n   PRECIO_VENTA, ");
            sql.append("\n   FECHA_VENCIMIENTO, ");
            sql.append("\n   EXITO, ");
            sql.append("\n   valor_unitario_promedio, ");
            sql.append("\n   VALOR_TOTAL ");
            sql.append("\n ) ( ");
            sql.append("\n    SELECT ");
            sql.append("\n    tr.ID, ");
            sql.append("\n     ( SELECT ID_DOC FROM INVENTARIO.candado_documentos), ");
            sql.append("\n     tr.ID_ARTICULO, ");
            sql.append("\n     tr.CANTIDAD, ");
            sql.append("\n     tr.VALOR_UNITARIO, ");
            sql.append("\n     tr.IVA, ");
            sql.append("\n     tr.VALOR_IMPUESTO, ");
            sql.append("\n     tr.ID_NATURALEZA, ");
            sql.append("\n     tr.LOTE, ");
            sql.append("\n     tr.SERIAL, ");
            sql.append("\n     tr.PRECIO_VENTA, ");
            sql.append("\n     tr.FECHA_VENCIMIENTO, ");
            sql.append("\n     tr.EXITO, ");
            sql.append("\n     tr.valor_unitario_promedio, ");
            sql.append("\n     (tr.cantidad*(tr.valor_unitario+tr.valor_impuesto+documento.vlr_flete)) ");
            sql.append("\n   FROM INVENTARIO.TRANSACCIONES tr ");
            sql.append("\n   inner join ( ");
            sql.append("\n\t select (doc.valor_flete/sum (tr.cantidad)) vlr_flete, doc.id ");
            sql.append("\n\t from inventario.transacciones tr  ");
            sql.append("\n\t inner join inventario.documentos doc on tr.id_documento = doc.id ");
            sql.append("\n\t where doc.id =( SELECT id_doc FROM inventario.candado)::integer ");
            sql.append("\n\t group by doc.valor_flete, doc.id ");
            sql.append("\n   ) documento on tr.id_documento = documento.id ");
            sql.append("\n   WHERE tr.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO) ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.TRANSACCIONES ");
            sql.append("\n ( ");
            sql.append("\n   ID_DOCUMENTO, ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   CANTIDAD, ");
            sql.append("\n   VALOR_UNITARIO, ");
            sql.append("\n   IVA, ");
            sql.append("\n   VALOR_IMPUESTO, ");
            sql.append("\n   ID_NATURALEZA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   SERIAL, ");
            sql.append("\n   PRECIO_VENTA, ");
            sql.append("\n   FECHA_VENCIMIENTO, ");
            sql.append("\n   EXITO, ");
            sql.append("\n   valor_unitario_promedio ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n   SELECT  ");
            sql.append("\n     ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS), /* DOC CANDADO DESTINO */ ");
            sql.append("\n     ID_ARTICULO, ");
            sql.append("\n     CANTIDAD, ");
            sql.append("\n     VALOR_UNITARIO, ");
            sql.append("\n     IVA, ");
            sql.append("\n     VALOR_IMPUESTO, ");
            sql.append("\n     'I' ID_NATURALEZA, ");
            sql.append("\n     LOTE, ");
            sql.append("\n     SERIAL, ");
            sql.append("\n     PRECIO_VENTA, ");
            sql.append("\n     FECHA_VENCIMIENTO, ");
            sql.append("\n     EXITO, ");
            sql.append("\n     valor_unitario_promedio ");
            sql.append("\n   FROM  INVENTARIO.TRANSACCIONES_TMP  ");
            sql.append("\n   WHERE ID_DOCUMENTO = (SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   EXISTENCIA, ");
            sql.append("\n   FECHA_VENCIMIENTO ");
            sql.append("\n )  ");
            sql.append("\n  ( ");
            sql.append("\n      SELECT  ");
            sql.append("\n           TR.ID_ARTICULO, ");
            sql.append("\n           DOC.ID_BODEGA, ");
            sql.append("\n           TR.LOTE, ");
            sql.append(
                    "\n           CASE WHEN  BEL.EXISTENCIA IS NULL THEN TR.CANTIDAD ELSE   TR.CANTIDAD + BEL.EXISTENCIA END EXISTENCIAS, TR.FECHA_VENCIMIENTO ");
            sql.append("\n\t\tFROM ( ");
            sql.append("\n\t\t\t\tSELECT      ");
            sql.append("\n\t\t\t\t  id_documento, ");
            sql.append("\n\t\t\t\t  id_articulo, ");
            sql.append("\n\t\t\t\t  sum(cantidad) cantidad, ");
            sql.append("\n\t\t\t\t  lote  , fecha_vencimiento      ");
            sql.append("\n\t\t\t\tFROM inventario.transacciones_tmp ");
            sql.append("\n\t\t\t\tGROUP BY id_documento, id_articulo,lote, fecha_vencimiento ");
            sql.append("\n\t\t  ) TR ");

            sql.append("\n         INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n         LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n         WHERE DOC.ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n         AND TR.LOTE <> '' ");
            sql.append("\n ); ");

            sql.append("\n DELETE FROM  ");
            sql.append("\n   INVENTARIO.BODEGAS_EXISTENCIAS_LOTE  ");
            sql.append("\n WHERE  ID_ARTICULO IN ( SELECT TR.ID_ARTICULO ");
            sql.append("\n         FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n         INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n         LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n         WHERE DOC.ID =  ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n   ) ");
            sql.append("\n   AND ID_BODEGA IN (SELECT DOC.ID_BODEGA ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n     WHERE DOC.ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     ) ");
            sql.append("\n  AND LOTE IN (SELECT  TR.LOTE ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP BEL ON ( TR.ID_ARTICULO = BEL.ID_ARTICULO AND DOC.ID_BODEGA = BEL.ID_BODEGA AND TR.LOTE = BEL.LOTE) ");
            sql.append("\n     WHERE DOC.ID =  ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_LOTE ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   LOTE, ");
            sql.append("\n   EXISTENCIA, ");
            sql.append("\n   FECHA_VENCIMIENTO ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n     SELECT        ");
            sql.append("\n       ID_ARTICULO, ");
            sql.append("\n       ID_BODEGA, ");
            sql.append("\n       LOTE, ");
            sql.append("\n       EXISTENCIA, ");
            sql.append("\n       FECHA_VENCIMIENTO ");
            sql.append("\n     FROM INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP   ");
            sql.append("\n ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS_TMP ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   EXISTENCIA ");
            sql.append("\n )  ");
            sql.append("\n ( ");
            sql.append("\n \tSELECT  ");
            sql.append("\n       TR.ID_ARTICULO, ");
            sql.append("\n       DOC.ID_BODEGA, ");
            sql.append(
                    "\n       CASE WHEN  BE.EXISTENCIA IS NULL THEN SUM(TR.CANTIDAD) ELSE SUM(TR.CANTIDAD) + BE.EXISTENCIA END EXISTENCIAS ");
            sql.append("\n     FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n     INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n     LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append("\n     WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n     GROUP BY tr.id_articulo,doc.id_bodega,be.existencia ");
            sql.append("\n ); ");

            sql.append("\n DELETE FROM INVENTARIO.BODEGAS_EXISTENCIAS  ");
            sql.append("\n WHERE ID_ARTICULO IN (SELECT TR.ID_ARTICULO  ");
            sql.append("\n                       FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n                       INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n                       LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append(
                    "\n                       WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n                   ) ");
            sql.append("\n AND ID_BODEGA IN (SELECT DOC.ID_BODEGA ");
            sql.append("\n                   FROM INVENTARIO.TRANSACCIONES_TMP TR ");
            sql.append("\n                   INNER JOIN INVENTARIO.DOCUMENTOS DOC ON TR.ID_DOCUMENTO = DOC.ID ");
            sql.append(
                    "\n                   LEFT JOIN INVENTARIO.BODEGAS_EXISTENCIAS BE ON (TR.ID_ARTICULO = BE.ID_ARTICULO AND DOC.ID_BODEGA = BE.ID_BODEGA) ");
            sql.append(
                    "\n                   WHERE TR.ID_DOCUMENTO = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n                   ); ");

            sql.append("\n INSERT INTO INVENTARIO.BODEGAS_EXISTENCIAS ");
            sql.append("\n ( ");
            sql.append("\n   ID_ARTICULO, ");
            sql.append("\n   ID_BODEGA, ");
            sql.append("\n   EXISTENCIA ");
            sql.append("\n )  ");
            sql.append("\n  ( ");
            sql.append("\n   SELECT  ");
            sql.append("\n     ID_ARTICULO, ");
            sql.append("\n     ID_BODEGA, ");
            sql.append("\n     EXISTENCIA ");
            sql.append("\n   FROM INVENTARIO.BODEGAS_EXISTENCIAS_TMP ");
            sql.append(" ); ");

            sql.append("\n UPDATE  INVENTARIO.DOCUMENTOS   ");
            sql.append("\n SET  ");
            sql.append("\n   FECHA_CIERRE = now(), ");
            sql.append("\n   ID_USUARIO_CIERRA = " + cn.usuario.getIdentificacion() + ", ");
            sql.append("\n   ID_ESTADO = 1 ");
            sql.append("\n WHERE ID = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer; ");

            cn.prepareStatementIDU(1, sql);
            if (cn.imprimirConsola) {
                System.out.println("--CREACION doc:n " + ((LoggableStatement) cn.ps1).getQueryString());
            }
            cn.iduSQL(1);

        } catch (SQLException localSQLException) {
            System.out.println(
                    localSQLException.getMessage() + " en método hacerEntradasInventario() de DocumentoHc.java");
            cn.exito = false;
        } catch (Exception localException) {
            System.out
                    .println(localException.getMessage() + " en método hacerEntradasInventario() de DocumentoHc.java");
            cn.exito = false;
        }

        return bool;
    }

    public boolean movimientoInventario() {
        boolean bool = true;
        try {
            sql.delete(0, sql.length());

            /* para traslados entradas */
            /* MOVIMIENTO DE inventarios */
            sql.append("\n INSERT INTO inventario.inventarios_tmp(  ");
            sql.append("\n  id_articulo, ");
            sql.append("\n  valor_unitario, ");
            sql.append("\n  precio_venta, ");
            sql.append("\n  existencia, ");
            sql.append("\n  valor_total ");
            sql.append("\n )(	  ");
            sql.append("\n	SELECT   ");
            sql.append("\n	  tr.id_articulo,   ");
            sql.append("\n	  ((tr.valor_total+invt.valor_total)/(tr.cantidad  + invt.existencia)), ");
            sql.append("\n	  tr.precio_venta, ");
            sql.append("\n	  tr.cantidad  + invt.existencia, ");
            sql.append("\n	  tr.valor_total+invt.valor_total  ");
            sql.append("\n	FROM ( ");
            sql.append("\n		SELECT  ");
            sql.append("\n		  trp.id_documento,	 ");
            sql.append("\n		  trp.id_articulo, ");
            sql.append("\n		  sum (trp.valor_unitario) valor_unitario, ");
            sql.append("\n		  0 precio_venta, ");
            sql.append("\n		  sum(trp.cantidad) cantidad,      ");
            sql.append("\n		  sum (trp.valor_total) valor_total ");
            sql.append("\n		FROM inventario.transacciones_tmp  trp   ");
            sql.append("\n		GROUP BY trp.id_articulo,  trp.id_documento ");
            sql.append("\n	) tr     ");
            sql.append("\n	INNER JOIN inventario.inventarios  invt ON tr.id_articulo = invt.id_articulo ");
            sql.append("\n	WHERE id_documento = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n );   ");

            /**/
            sql.append("\n	UPDATE inventario.transacciones_tmp  ");
            sql.append("\n	SET valor_unitario_promedio = inventario.inventarios_tmp.valor_unitario ");
            sql.append(
                    "\n	FROM inventario.inventarios_tmp WHERE inventario.inventarios_tmp.id_articulo = inventario.transacciones_tmp.id_articulo; ");

            /* actualizar transacciones */
            sql.append("\n	UPDATE inventario.transacciones  ");
            sql.append("\n	SET valor_unitario_promedio = inventario.transacciones_tmp.valor_unitario_promedio ");
            sql.append(
                    "\n	FROM inventario.transacciones_tmp WHERE inventario.transacciones_tmp.id = inventario.transacciones.id; ");

            /**/

            /* ELIMINAR DE LA TABLA LOS REGISTROS PARA ACTUALIZAR */
            sql.append("\n DELETE FROM inventario.inventarios  ");
            sql.append("\n WHERE id_articulo IN (SELECT  tr.id_articulo   ");
            sql.append("\n					  FROM inventario.transacciones_tmp tr ");
            sql.append(
                    "\n					  LEFT JOIN inventario.inventarios inv ON tr.id_articulo = inv.id_articulo  ");
            sql.append(
                    "\n					  WHERE id_documento = ( SELECT ID_DOC FROM INVENTARIO.CANDADO_DOCUMENTOS)::integer ");
            sql.append("\n );  ");

            /* INSERTAR EN inventario */
            sql.append("\n INSERT INTO inventario.inventarios( ");
            sql.append("\n   id_articulo, ");
            sql.append("\n  valor_unitario, ");
            sql.append("\n  precio_venta, ");
            sql.append("\n  existencia, ");
            sql.append("\n  valor_total ");
            sql.append("\n ) ( ");
            sql.append("\n	SELECT  ");
            sql.append("\n	id_articulo, ");
            sql.append("\n	valor_unitario, ");
            sql.append("\n	precio_venta, ");
            sql.append("\n	existencia, ");
            sql.append("\n	valor_total ");
            sql.append("\n	FROM inventario.inventarios_tmp ");
            sql.append("\n );	 ");

            /* fin */
            cn.prepareStatementIDU(1, sql);
            System.out.println("\n -- ejecutar  movimientoInventario **************************** --");
            cn.iduSQL(1);

        } catch (SQLException localSQLException) {
            System.out
                    .println(localSQLException.getMessage() + " en método 1 movimientoInventario(), de Paciente.java");
            return false;
        } catch (Exception localException) {
            System.out.println(localException.getMessage() + " en método 2 movimientoInventario(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean cerrarDocumentoSolicitudesInventario() {
        System.out.println("ID_DOC= " + var2);
        boolean bool = true;
        esTraslado = "NO";
        noticia = "xxx";
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();

                if (hacerSalidasInventario()) {

                    noticia = "DOCUMENTO FINALIZADO CON EXITO..";
                } else {
                    noticia = "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS";
                }
                limpiarCandados();
                cn.fintransaccion();
            }
        } catch (Exception localException) {
            System.out.println(localException.getMessage()
                    + " en método 2 cerrarDocumentoSolicitudesInventario(), de Paciente.java");
            return false;
        }
        return bool;
    }

    public boolean finalizarDocumentoFactura() {

        if (verificaTransaccionesInventario()) {
            finalizarFactura();
        }

        return true;
    }
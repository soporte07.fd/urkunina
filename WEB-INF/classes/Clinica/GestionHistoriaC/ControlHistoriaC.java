/*
last: jas
 */

package Clinica.GestionHistoriaC;
import Sgh.Utilidades.*;
import java.util.*;
import Clinica.AdminSeguridad.*;


public class ControlHistoriaC{
   private Conexion cn;


   private String page;
   private String rows;
   private String sidx;
   private String sord;
   public DocumentoHC documentoHC;
   public Facturacion facturacion; 
   public Paciente paciente;
   public Citas citas;
   public Odontograma odontograma;

   private String sinertrans_user;

   public ControlHistoriaC(){
          documentoHC = new DocumentoHC();
          facturacion = new Facturacion();
          paciente = new Paciente();
          citas = new Citas();
          odontograma = new Odontograma();
   }


   //Metodos traducidos por el controlador a las clases especificas




   public boolean asignaGuardar(String opcion){
       int op=0;
       boolean respuesta=false;
       if(opcion.indexOf("listOrdenesMedicamentos")!=-1  )op=1;


       switch (op){
          case 1:
               // respuesta=documentoHC.guardarDocumentoHc();
          break;


       }
       return respuesta;
   }

   public boolean asignaModificar(String opcion){
       int op=0;
       boolean respuesta=false;
       if(opcion.indexOf("ubicaciones")!=-1  )op=1;


       switch (op){
          case 201:
//                respuesta=noConformidad.modificarNoConformidadReportada();
          break;
       }
       return respuesta;
   }



      /*  Metodos Paginacion  */

   // OJO construir try catch para la conversion a int para esta seccion completa..
   private int totalPages;
   private int count;
   private int start; //para int offset
   private int pagina;//para int page
   private int limit;//para int rows


   /* devuelve el numero total de paginas calculadas para el numero de registros contados...
   y para la pagina de peticion, calcula el valor 'start' donde comenzara el set.
   ahora, le paso el totalcount, pero podria tomarlo de this.count, y guardar el tp en this.totalPages. */
   public int totalPages(int totalcount){
       //System.out.println(" ControlHistoriaC.totalPages ");
       int tp=0;
       //System.out.println("aqui 1:"+opcion);
       this.pagina= Integer.parseInt(this.page);// para int
       this.limit= Integer.parseInt(this.rows);// para int
       this.start= 0;

       if(totalcount > 0) tp= (int)Math.ceil((1.0*totalcount)/limit);//asegurar el decimal.
       else tp=0;
       //
       if( this.pagina > tp) this.pagina=tp;
       if( this.pagina <= 0 ) this.pagina=1;// adicionado por mi, pa q avance. revisarlo.
       //
       this.start= this.limit*this.pagina-this.limit;

       return tp;
   }

     public int asignaContar(String opcion){
       int op=0;
       ArrayList respuesta=new ArrayList();
       if(opcion.equals("cargos"))op=7;

       switch (op){
          case 8:
//                op=(int)tipoid.contar();        //cuantos registros con los parametros del select
          break;

       }
       return op;
   }
   public Object asignaBuscarPag(String opcion){
       System.out.println(" sidx ++++++++++++++++++"+sidx);
       int page,limit,sidx,  start;
       // validar el indice de ordenamiento, la direcc de orden,
       if(this.sidx==null) sidx=1;
       else if(this.sidx.equals("")) sidx=1;
       else {
            try{sidx= Integer.parseInt(this.sidx);}
            catch(Exception e){
                sidx= 1;
                System.out.println(" Se paso como Indice de Ordenacion algo que NO es Int !!! ");
            }
            sidx= Integer.parseInt(this.sidx);
       }
       page = this.pagina;
       limit = this.limit;
       start = this.start;
       sord = this.sord;// sort order. String sord
       //
       int op=0;
       ArrayList respuesta=new ArrayList();
       //System.out.println("aqui 1:"+opcion);
       if (opcion.equals("tiposIdentificaciones"))op=8;


       switch (op){
          case 8:
  //              respuesta=(ArrayList)tipoid.buscarPag(sidx, sord, start, limit);
          break;
          case 201:
//                respuesta=(ArrayList)noConformidad.buscarPag(sidx, sord, start, limit);
          break;
       }
       return respuesta;
   }

   /*
    public Object asignaBuscar(String opcion){
       //System.out.println(" ControlHistoriaC.asigna buscar ");
       int op=0;
       ArrayList respuesta=new ArrayList();
       if(opcion.equals("ubicaciones"))op=1;    //ubicaciones

       switch (op){
          case 1:
                //System.out.println("aqui 2:"+opcion);
               // respuesta=(ArrayList)agencia.buscarAgencia();
          break;
       }
       return respuesta;

   } */



   public boolean asignaEliminar(String opcion){
       int op=0;
       boolean respuesta=false;
       if(opcion.indexOf("ubicaciones")!=-1  )op=1;

       switch (op){
           case 1:
               int op2=0;
               if( opcion.indexOf("Pais")!=-1  )op2=1;
               if( opcion.indexOf("Depto")!=-1  )op2=2;
               if( opcion.indexOf("Municipio")!=-1  )op2=3;
               if( opcion.indexOf("Barrio")!=-1  )op2=4;
               if( opcion.indexOf("tiposIdentificaciones")!=-1  )op2=8;
               switch (op2){
                   case 1:
                   //     respuesta=pais.eliminarPais();
                   break;
                   case 8:
//                        respuesta=tipoid.eliminarTipoId();
                   break;
                 }
          break;


       }
       return respuesta;
   }

   public Sgh.Utilidades.Conexion getCn() {
        return cn;
    }

    public void setCn(Sgh.Utilidades.Conexion value) {
        cn = value;
        documentoHC.setCn(this.cn);
        paciente.setCn(this.cn);
        citas.setCn(this.cn);
        odontograma.setCn(this.cn);
        facturacion.setCn(this.cn);
    }

    public java.lang.String getPage() {
        return page;
    }

    public void setPage(java.lang.String value) {
        page = value;
    }

    public java.lang.String getRows() {
        return rows;
    }

    public void setRows(java.lang.String value) {
        rows = value;
    }

    public java.lang.String getSidx() {
        return sidx;
    }

    public void setSidx(java.lang.String value) {
        sidx = value;
    }

    public java.lang.String getSord() {
        return sord;
    }

    public void setSord(java.lang.String value) {
        sord = value;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int value) {
        totalPages = value;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int value) {
        count = value;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int value) {
        start = value;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int value) {
        pagina = value;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int value) {
        limit = value;
    }


}
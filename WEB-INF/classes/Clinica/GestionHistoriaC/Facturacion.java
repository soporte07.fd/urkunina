package Clinica.GestionHistoriaC;

import Sgh.Utilidades.*;
import Clinica.Presentacion.HistoriaClinica.*;
import WebServices.*;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.Date;
import java.lang.*;
import java.text.SimpleDateFormat;
import java.sql.*;
import java.io.File;
import java.sql.*;
import javax.xml.bind.*;

/**
 * @(#)Clase Conexion.java version 1.02 2015/12/09 Copyright(c) 2015 Firmas
 *           Digital. JAS
 */
public class Facturacion {

    private final String NOTICIA_FACTURA = "PERMITE_NUMERAR";
    private final String AJUSTE_NUMERAR = "NUMERAR";
    private final String AJUSTE_VALOR_MAXIMO_CUENTA = "MAXIMO_CUENTA";
    private final String AJUSTE_VALOR_MAXIMO_ANUAL = "MAXIMO_ANUAL";

    private Integer id;
    private String esTraslado;
    private String FacturacionTodo;
    private String existe;
    private Integer idCuenta, varInt1, idPlan, idTarifario, idCita, cantProcedimientos, idLiquidacion,
            id_grupo_quirurgico;
    private String idProcedimiento, tipoTarifarioPlan, lateralidad, id_tipo_unidad, varStr4, varStr5, noticia;
    private String idQuery;
    private Integer porcentage;
    private String totalCuenta, valorNoCubierto, valorCubierto, valorDescuento, totalFactura, idEstado, medioPago, prefijoFactura, formaPago;
    private String estado, idFactura, numeroFactura, idEstadoCuenta, estadoCuenta, idRecibo, idReciboAjuste;
    private List<String> recibosCopagoFactura, otrosConceptosFactura;
    private String estadoAjuste;
    private boolean seAjustoCuenta, seAjustoAnual;
    private String noticiaCuenta, noticiaAnual;

    public Constantes constantes;

    private Conexion cn;

    private StringBuffer sql = new StringBuffer();

    private InsercionFacturacion insercionFacturacion;

    private InsercionFacturacionPortType port;

    public Facturacion() {
        id = 0;
        FacturacionTodo = "";
        existe = "";
        idCuenta = 0;
        idPlan = 0;
        idTarifario = 0;
        idCita = 0;
        cantProcedimientos = 0;
        idLiquidacion = 0;

        idProcedimiento = "";
        tipoTarifarioPlan = "";

        id_tipo_unidad = "";
        lateralidad = "";
        varStr4 = "";
        varStr5 = "";

        noticia = "";
        esTraslado = "";
        porcentage = 0;

        totalCuenta = "";
        valorNoCubierto = "";
        valorCubierto = "";
        valorDescuento = "";
        totalFactura = "";
        idEstado = "";
        estado = "";
        idFactura = "";
        numeroFactura = "";
        estadoAjuste = "";
        seAjustoCuenta = false;
        seAjustoAnual = false;
        noticiaAnual = "";
        noticiaCuenta = "";
        idEstadoCuenta = "";
        estadoCuenta = "";
        idRecibo = "";
        idReciboAjuste = "";
        prefijoFactura = "";
        formaPago = "";
    }

    public boolean liquidarCirugiaSoat() {
        /* UNILATERAL Y SOLO UN PROCEDIMIENTO */
        cn.exito = true;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("SELECT ");
                sql2.append("  t.id,  t.descripcion,  p.id_tarifario, ");
                sql2.append("  p.porcen_variabilidad , ");
                sql2.append("  td.id_cargo, ");
                sql2.append("  td.precio, ");
                sql2.append(
                        "  ( soat.honorarios*sa.honorarios + soat.anestesiologo*sa.anestesiologo + soat.ayudantia*sa.ayudantia + soat.derechos_sala*sa.derechos_sala +  soat.materiales*sa.materiales )  -  ( ( soat.honorarios*sa.honorarios + soat.anestesiologo*sa.anestesiologo + soat.ayudantia*sa.ayudantia + soat.derechos_sala*sa.derechos_sala +  soat.materiales*sa.materiales ) * p.porcen_variabilidad  /100  )    tot_liquidado     ");
                sql2.append("FROM  ");
                sql2.append("  facturacion.planes p ");
                sql2.append("inner join facturacion.tarifarios t on p.id_tarifario = t.id ");
                sql2.append("inner join facturacion.tarifarios_detalle td on td.id_tarifario = t.id ");
                sql2.append("inner join facturacion.qx_soat_grupos_precios  soat on td.precio = soat.id   ");
                sql2.append("inner join facturacion.qx_soat_grupos_precios_admision sa on sa.id_cuenta = ? ");
                sql2.append("WHERE p.id = ? ");
                sql2.append("and td.id_cargo = ? "); // '132300'

                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setInt(1, this.idCuenta);
                cn.ps2.setInt(2, this.idPlan);
                cn.ps2.setString(3, this.idProcedimiento);

                System.out.println("SQL lllllllllllll = " + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {

                        System.out.println("============== " + cn.rs2.getString("tot_liquidado"));

                        cn.cerrarPS(Constantes.PS2);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> DocumentoHc.java-- function siExisteFacturacion --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> DocumentoHc.java-- function siExisteFacturacion --> Exception --> " + e.getMessage());
        }
        return cn.exito;
    }

    public int lateralidadDelProcedimiento() {
        /* total procedimientos en la cuenta */
        // int ban= 0;
        StringBuffer sql2 = new StringBuffer();
        this.lateralidad = "";
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append(
                        " SELECT id_lateralidad, id_procedimiento FROM cirugia.liquidacion_procedimientos where id_liquidacion = ? ");
                this.cn.prepareStatementSEL(Constantes.PS3, sql2);
                cn.ps3.setInt(1, this.idLiquidacion);

                System.out.println(
                        "SQLlateralidadDelProcedimiento =****= " + ((LoggableStatement) cn.ps3).getQueryString());

                if (cn.selectSQL(Constantes.PS3)) {
                    while (cn.rs3.next()) {
                        this.lateralidad = cn.rs3.getString("id_lateralidad");
                        this.idProcedimiento = cn.rs3.getString("id_procedimiento");
                        cn.cerrarPS(Constantes.PS3);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("Error --> Facturacion.java-- function lateralidadDelProcedimiento --> SQLException --> "
                    + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> Facturacion.java-- function lateralidadDelProcedimiento --> Exception --> "
                    + e.getMessage());
        }
        return cantProcedimientos;
    }

    public boolean tipoUnidadPrecio(String procedimiento) {
        /* tipo de unidad precio para liquidar valor segun tabla */
        boolean ban = true;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("SELECT ");
                sql2.append("  p.id_tarifario, ");
                sql2.append("  p.porcen_variabilidad,  ");
                sql2.append("  td.precio, ");
                sql2.append("  td.id_tipo_unidad ");
                sql2.append("FROM ");
                sql2.append("  facturacion.planes p ");
                sql2.append("inner join facturacion.tarifarios t on p.id_tarifario = t.id  ");
                sql2.append("inner join facturacion.tarifarios_detalle td on td.id_tarifario = t.id  ");
                sql2.append("WHERE p.id = ?::integer ");
                sql2.append("and td.id_cargo = ? ");

                this.cn.prepareStatementSEL(Constantes.PS3, sql2);
                cn.ps3.setInt(1, this.idPlan);
                cn.ps3.setString(2, this.idProcedimiento);

                System.out.println("\n id_tipo_unidad =****= " + ((LoggableStatement) cn.ps3).getQueryString());

                if (cn.selectSQL(Constantes.PS3)) {
                    while (cn.rs3.next()) {
                        this.id_tipo_unidad = cn.rs3.getString("id_tipo_unidad");
                        this.id_grupo_quirurgico = cn.rs3.getInt("precio");
                        this.porcentage = cn.rs3.getInt("porcen_variabilidad");
                        cn.cerrarPS(Constantes.PS3);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> Facturacion.java-- function tipoUnidadPrecio --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> Facturacion.java-- function tipoUnidadPrecio --> Exception --> " + e.getMessage());
        }
        return ban;
    }

    public int grupoYPorcentajeProcedimiento() {
        /* total procedimientos en la cuenta */
        StringBuffer sql2 = new StringBuffer();
        int porcentaje = 0;
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append(" xxxxxxxxxxxxxxxx = ? ");
                this.cn.prepareStatementSEL(Constantes.PS3, sql2);
                cn.ps3.setInt(1, this.idLiquidacion);

                System.out.println(
                        "grupoYPorcentajeProcedimiento =****= " + ((LoggableStatement) cn.ps3).getQueryString());

                if (cn.selectSQL(Constantes.PS3)) {
                    while (cn.rs3.next()) {
                        this.lateralidad = cn.rs3.getString("id_lateralidad");
                        cn.cerrarPS(Constantes.PS3);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("Error --> Facturacion.java-- function lateralidadDelProcedimiento --> SQLException --> "
                    + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> Facturacion.java-- function lateralidadDelProcedimiento --> Exception --> "
                    + e.getMessage());
        }
        return porcentaje;
    }

    public boolean insertarEnLiquidacionDetalleSOAT(String procedimiento) {
        /* tipo de unidad precio para liquidar valor segun tabla */
        boolean ban = true;
        StringBuffer sql2 = new StringBuffer();
        int honorarios = 0, anestesiologo = 0, ayudantia = 0, derechos_sala = 0, materiales = 0, sumatoriaDerechos = 0;

        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("SELECT ");
                sql2.append("  soat.honorarios *  cast(li.sw_cirujano as int) honorarios, ");
                sql2.append("  soat.anestesiologo *  cast(li.sw_anestesiologo as int) anestesiologo, ");
                sql2.append("  soat.ayudantia *  cast(li.sw_ayudante as int) ayudantia, ");
                sql2.append("  soat.derechos_sala *  cast(li.sw_sala as int) derechos_sala, ");
                sql2.append("  soat.materiales *  cast(li.sw_materiales as int) materiales ");
                sql2.append("FROM  ");
                sql2.append("  cirugia.soat_grupos_precios  soat,  cirugia.liquidacion  li ");
                sql2.append("where soat.id_grupo_quirurgico = ? ");
                sql2.append("and li.id = ? ");

                this.cn.prepareStatementSEL(Constantes.PS3, sql2);
                cn.ps3.setInt(1, this.id_grupo_quirurgico);
                cn.ps3.setInt(2, this.idLiquidacion);

                // System.out.println("\n
                // llllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll=\n
                // "+((LoggableStatement)cn.ps3).getQueryString());
                if (cn.selectSQL(Constantes.PS3)) {
                    while (cn.rs3.next()) {
                        honorarios = cn.rs3.getInt("honorarios");
                        anestesiologo = cn.rs3.getInt("anestesiologo");
                        ayudantia = cn.rs3.getInt("ayudantia");
                        derechos_sala = cn.rs3.getInt("derechos_sala");
                        materiales = cn.rs3.getInt("materiales");
                        cn.cerrarPS(Constantes.PS3);
                        break;
                    }
                }

                System.out.println(".......2............" + honorarios + "::" + anestesiologo + "::" + ayudantia + "::"
                        + derechos_sala + "::" + materiales);
                sumatoriaDerechos = honorarios + anestesiologo + ayudantia + derechos_sala + materiales;

                sumatoriaDerechos = sumatoriaDerechos - (sumatoriaDerechos * this.porcentage * -1) / 100;
                /* grupoYPorcentajeProcedimiento */

                sql.delete(0, sql.length());
                sql.append(
                        "INSERT INTO cirugia.liquidacion_derechos ( id_liquidacion, id_procedimiento, id_derechos, valor_unitario) ");
                sql.append("values(?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?)");
                cn.prepareStatementSEL(Constantes.PS1, sql);

                cn.ps1.setInt(1, this.idLiquidacion);
                cn.ps1.setString(2, procedimiento);
                cn.ps1.setString(3, "H");
                cn.ps1.setInt(4, honorarios);

                cn.ps1.setInt(5, this.idLiquidacion);
                cn.ps1.setString(6, procedimiento);
                cn.ps1.setString(7, "A");
                cn.ps1.setInt(8, anestesiologo);

                cn.ps1.setInt(9, this.idLiquidacion);
                cn.ps1.setString(10, procedimiento);
                cn.ps1.setString(11, "Y");
                cn.ps1.setInt(12, ayudantia);

                cn.ps1.setInt(13, this.idLiquidacion);
                cn.ps1.setString(14, procedimiento);
                cn.ps1.setString(15, "D");
                cn.ps1.setInt(16, derechos_sala);

                cn.ps1.setInt(17, this.idLiquidacion);
                cn.ps1.setString(18, procedimiento);
                cn.ps1.setString(19, "M");
                cn.ps1.setInt(20, materiales);

                System.out.println(
                        "-------------------1---------------\n" + ((LoggableStatement) cn.ps1).getQueryString());
                cn.iduSQL(Constantes.PS1);

                sql.delete(0, sql.length());
                sql.append(
                        "UPDATE cirugia.liquidacion_procedimientos SET valor_unitario = ?  WHERE id_liquidacion = ? ");
                cn.prepareStatementSEL(Constantes.PS1, sql);
                cn.ps1.setInt(1, sumatoriaDerechos);
                cn.ps1.setInt(2, this.idLiquidacion);
                System.out.println(
                        "------------------2----------------\n" + ((LoggableStatement) cn.ps1).getQueryString());
                cn.iduSQL(Constantes.PS1);

            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> Facturacion.java-- function insertarEnLiquidacionDetalleSOAT --> SQLException --> "
                            + e.getMessage());
        } catch (Exception e) {
            System.out
                    .println("Error --> Facturacion.java-- function insertarEnLiquidacionDetalleSOAT --> Exception --> "
                            + e.getMessage());
        }
        return ban;
    }

    public boolean liquidarSegunTipoUnidad() {
        /* UNILATERAL Y VARIOS PROCEDIMIENTOS */
        boolean ban = true;

        if (this.id_tipo_unidad.equals("01")) {
            /* pesos */

            System.out.println("---ES CERO UNO");

        } else if (this.id_tipo_unidad.equals("02")) {
            /* UVRS */

        } else if (this.id_tipo_unidad.equals("03")) {
            /* SMMLV */

        } else if (this.id_tipo_unidad.equals("04")) {
            /* GQ */

            insertarEnLiquidacionDetalleSOAT(this.idProcedimiento);

        } else if (this.id_tipo_unidad.equals("05")) {
            /* UVRS */

        } else if (this.id_tipo_unidad.equals("06")) {
            /* P/min */

        } else if (this.id_tipo_unidad.equals("07")) {
            /* P/hra */

        } else if (this.id_tipo_unidad.equals("GQ")) {
            /* pesos */

        }
        return ban;
    }

    public int soatUnicoProcedimiento() {
        /* UNICO PROCEDIMIENTO jas */
        int ban = 0;

        lateralidadDelProcedimiento();

        if (this.lateralidad.equals("U")) {
            /* UNILATERAL */
            System.out.println("this.lateralidad==== " + this.lateralidad);
            tipoUnidadPrecio(this.idProcedimiento);

            System.out.println("this.id_tipo_unidad=" + this.id_tipo_unidad);
            System.out.println("this.id_grupo_quirurgico=" + this.id_grupo_quirurgico);
            System.out.println("this.porcentage=" + this.porcentage);

            liquidarSegunTipoUnidad();

        } else {
            /* BILATERAL */
            System.out.println("this.lateralidad dos de un procedimiento soat====== " + this.lateralidad);
        }

        return ban;
    }

    /*
     * public int cuantosProcedimientosProgramados(){ /*total procedimientos en la
     * cuenta* // int ban= 0; StringBuffer sql2 = new StringBuffer(); try{
     * if(cn.isEstado()){ sql2.delete(0,sql2.length()); sql2.
     * append(" SELECT count(id_liquidacion) total_procedimientos FROM   cirugia.liquidacion_procedimientos where id_liquidacion = ?::integer"
     * );
     *
     * this.cn.prepareStatementSEL(Constantes.PS2,sql2);
     * cn.ps2.setInt(1,this.idLiquidacion);
     *
     * //
     * System.out.println("SQL total_procedimientos = "+((LoggableStatement)cn.ps2).
     * getQueryString());
     *
     * if(cn.selectSQL(Constantes.PS2)){ while(cn.rs2.next()){
     * this.cantProcedimientos = cn.rs2.getInt("total_procedimientos"); //
     * System.out.println("this.cantProcedimientos = "+this.cantProcedimientos);
     * cn.cerrarPS(Constantes.PS2); } } } }catch(SQLException e){ System.out.
     * println("Error --> DocumentoHc.java-- function siExistePaciente --> SQLException --> "
     * +e.getMessage()); }catch(Exception e){ System.out.
     * println("Error --> DocumentoHc.java-- function siExistePaciente --> Exception --> "
     * + e.getMessage()); } return cantProcedimientos; }
     */
    /**
     * Valida si una cuenta ya puede ser trasladada una factura.
     *
     * @return
     */
    public boolean validaNumeracionFactura() {
        System.out.println("---------------------VALIDAR NUMERACION FACTURA---------------------");
        cn.exito = true;

        seAjustoCuenta = false;
        noticia = "";
        estadoAjuste = "";

        try {
            if (cn.isEstado()) {

                System.out.println("lblIdFactura= " + idFactura);

                revisarValorNoCubiertoFactura();

                if (seAjustoCuenta) {
                    noticia = noticiaCuenta;
                    estadoAjuste = AJUSTE_VALOR_MAXIMO_CUENTA;
                } else {
                    noticia = NOTICIA_FACTURA;
                    estadoAjuste = AJUSTE_NUMERAR;
                }

                System.out.println("noticia Final= " + noticia);
                System.out.println("Tipo de Ajuste= " + estadoAjuste);

            }
        } catch (Exception e) {

            System.out.println(e.getMessage() + " en metodo liquidar() de Facturacion.java");
            cn.exito = false;

        }
        return cn.exito;
    }

    public String revisarValorNoCubiertoFactura() {
        System.out.println("---------------------VALOR NO CUBIERTO---------------------");
        cn.exito = true;
        // StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS2, cn.traerElQuery(669));
                cn.ps2.setString(1, this.idFactura);

                System.out.println(
                        "SQL revisarValorNoCubiertoDeEstaCuenta = " + ((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        System.out.println("__________1___________");
                        String aux = cn.rs2.getString("noticia");

                        System.out.println("Noticia ajuste cuenta: " + aux);

                        if (aux.equals(NOTICIA_FACTURA)) {
                            estadoAjuste = AJUSTE_NUMERAR;
                        } else {
                            estadoAjuste = AJUSTE_VALOR_MAXIMO_CUENTA;
                            seAjustoCuenta = true;
                            noticiaCuenta = aux;
                        }

                        // cn.cerrarPS(Constantes.PS2);
                        break;
                    }
                }

            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> Facturacion.java-- function revisarValorNoCubiertoDeEstaCuenta --> SQLException --> "
                            + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> Facturacion.java-- function revisarValorNoCubiertoDeEstaCuenta --> Exception --> "
                            + e.getMessage());
        }
        return this.noticia;
    }

    public String revisarValorNoCubiertoAcumuladoAnio() {
        System.out.println("--------------AJUSTE ANUAL-------------");
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS2, cn.traerElQuery(670));
                cn.ps2.setInt(1, id);
                cn.ps2.setString(2, idFactura);

                System.out.println(
                        "SQL revisarValorNoCubiertoAcumuladoAnio = " + ((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        String aux = cn.rs2.getString("noticia");

                        System.out.println("Noticia ajuste anual: " + aux);

                        if (aux.equals(NOTICIA_FACTURA)) {
                            estadoAjuste = AJUSTE_NUMERAR;
                        } else {
                            estadoAjuste = AJUSTE_VALOR_MAXIMO_ANUAL;
                            seAjustoAnual = true;
                            noticiaAnual = aux;
                        }

                        cn.cerrarPS(Constantes.PS2);
                        break;
                    }
                }

            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> Fcturacion.java-- function revisarValorNoCubiertoAcumuladoAnio --> SQLException --> "
                            + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> Fcturacion.java-- function revisarValorNoCubiertoAcumuladoAnio --> Exception --> "
                            + e.getMessage());
        }
        return this.noticia;
    }

    public String revisarAjusteDoble() {
        System.out.println("---------------AJUSTE DOBLE---------------");

        StringBuffer sql3 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql3.delete(0, sql3.length());
                sql3.append("SELECT\n" + "CASE WHEN (val.valor_max_anual-acumulado.valor_nocubierto)>val.valor\n"
                        + "THEN 'MAXIMO_CUENTA' ELSE 'MAXIMO_ANUAL' END noticia\n" + "FROM facturacion.facturas f\n"
                        + "INNER JOIN facturacion.valor_maximo_evento val on val.valor !=0\n" + "INNER JOIN (\n"
                        + "  SELECT\n" + "  SUM(f.valor_nocubierto) valor_nocubierto,f.id_paciente\n"
                        + "  FROM facturacion.facturas f\n"
                        + "  INNER JOIN facturacion.valor_maximo_evento val ON val.valor !=0\n" + "  WHERE \n"
                        + "  f.id_paciente =  ? \n"
                        + "  AND EXTRACT( YEAR FROM f.fecha_numeracion) = EXTRACT(YEAR FROM NOW())\n"
                        + "  AND f.id_estado = 'F'\n" + "  GROUP BY f.id_paciente\n"
                        + ") acumulado ON f.id_paciente = acumulado.id_paciente\n"
                        + "WHERE f.id_factura = ?::integer;");

                this.cn.prepareStatementSEL(Constantes.PS3, sql3);
                cn.ps3.setInt(1, id);
                cn.ps3.setString(2, idFactura);

                System.out.println("SQL AjusteDoble = " + ((LoggableStatement) cn.ps3).getQueryString());
                if (cn.selectSQL(Constantes.PS3)) {
                    while (cn.rs3.next()) {
                        String aux = cn.rs3.getString("noticia");

                        if (aux.equals(AJUSTE_VALOR_MAXIMO_CUENTA)) {
                            estadoAjuste = AJUSTE_VALOR_MAXIMO_CUENTA;
                            noticia = noticiaCuenta;
                        } else {
                            estadoAjuste = AJUSTE_VALOR_MAXIMO_ANUAL;
                            noticia = noticiaAnual;
                        }

                        cn.cerrarPS(Constantes.PS3);
                        break;
                    }
                }

            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> Facturacion.java-- function revisarAjusteDoble --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> Facturacion.java-- function revisarAjusteDoble --> Exception --> " + e.getMessage());
        }
        return this.noticia;
    }

    public String revisarEstadoDeLaCuenta() {
        this.noticia = "";
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append(" SELECT CASE WHEN estado = 'A' then 'PERMITE_NUMERAR' "
                        + "else 'LA CUENTA ESTA CONTABILIZADA'  END noticia  " + "FROM  facturacion.cuentas "
                        + "WHERE id = ? ");

                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setInt(1, this.idCuenta);

                System.out.println("SQL revisarEstadoDeLaCuenta = " + ((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        this.noticia = cn.rs2.getString("noticia");
                        cn.cerrarPS(Constantes.PS2);
                        break;
                    }
                }

            }
        } catch (SQLException e) {
            System.out.println("Error --> Fcturacion.java-- function informacionPlanLiquidacion --> SQLException --> "
                    + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> Fcturacion.java-- function informacionPlanLiquidacion --> Exception --> "
                    + e.getMessage());
        }
        return this.noticia;
    }

    public String siExisteFacturacion() {
        this.FacturacionTodo = "";
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append(" SELECT ID, TIPO_ID, IDENTIFICACION, NOMBRE_COMPLETO");
                sql2.append(" FROM HC.VI_Facturacion WHERE TIPO_ID =? AND IDENTIFICACION = ? ");
                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                // cn.ps2.setString(1,this.tipoId);
                // cn.ps2.setString(2,this.Identificacion);
                // System.out.println("SQL idqueryyyyyyyyyyyyyyy =
                // "+((LoggableStatement)cn.ps2).getQueryString());

                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        this.FacturacionTodo = cn.rs2.getString("ID") + "-" + cn.rs2.getString("TIPO_ID")
                                + cn.rs2.getString("IDENTIFICACION") + " " + cn.rs2.getString("NOMBRE_COMPLETO");
                        cn.cerrarPS(Constantes.PS2);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> DocumentoHc.java-- function siExisteFacturacion --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> DocumentoHc.java-- function siExisteFacturacion --> Exception --> " + e.getMessage());
        }

        return this.FacturacionTodo;
    }

    public String totalEnvioRips(String idEnvio) {
        System.out.println("--------------------------------------------------->");
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS3, cn.traerElQuery(224));
                cn.ps3.setString(1, idEnvio);
                System.out.println(((LoggableStatement) cn.ps3).getQueryString());
                if (cn.selectSQL(Constantes.PS3)) {
                    if (cn.rs3 != null) {
                        while (cn.rs3.next()) {
                            return cn.rs3.getString("total_cuenta");
                        }
                    }
                }
            }
            return "";
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Facturacion --> function totalEnvioRips --> SQLException --> " + e.getMessage());
            return "";
        } catch (Exception e) {
            System.out.println(
                    "Error --> clase Facturacion --> function totalEnvioRips --> Exception --> " + e.getMessage());
            return "";
        }
    }

    public boolean auditarFactura() {

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        System.out.println("---------------AUDITANDO FACTURA-------------");

        cn.exito = true;

        try {
            insercionFacturacion = new InsercionFacturacion();
            port = insercionFacturacion.getInsercionFacturacionPort();

            auditarPaciente(1);

            DatosTerceros datosTerceros = new DatosTerceros();
            DatosFacturas datosFacturas = new DatosFacturas();

            if (cn.isEstado()) {
                System.out.println("---------------CONSULTANDO DATOS DE FACTURA-------------");
                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQuery(134));
                cn.ps1.setString(1, idFactura);
                System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    while (cn.rs1.next()) {

                        FacturaVenta facturaVenta = new FacturaVenta();

                        Detalles detalles = new Detalles();
                        Tercero tercero = new Tercero();
                        Recibos recibos = new Recibos();

                        System.out.println("**************************");
                        System.out.println("DATOS DE TERCERO -- INICIADO");
                        tercero.setTipoDocumento(cn.rs1.getString("tipo_documento"));
                        tercero.setIdentificacion(cn.rs1.getString("nit"));
                        tercero.setPrimApe(cn.rs1.getString("apellido1"));
                        tercero.setSegApe(cn.rs1.getString("apellido2"));
                        tercero.setPrimNom(cn.rs1.getString("nombre1"));
                        tercero.setSegNom(cn.rs1.getString("nombre2"));
                        tercero.setDireccion(cn.rs1.getString("direccion"));
                        tercero.setTelefono(cn.rs1.getString("telefono"));
                        tercero.setEmail(cn.rs1.getString("email"));
                        tercero.setPais(cn.rs1.getString("pais"));
                        tercero.setDepartamento(cn.rs1.getString("departamento"));
                        tercero.setMunicipio(cn.rs1.getString("municipio"));
                        tercero.setRegimen(cn.rs1.getString("regimen"));
                        tercero.setRazonSocial(cn.rs1.getString("razon_social"));

                        datosTerceros.agregarTercero(tercero);
                        System.out.println("DATOS DE TERCERO -- COMPLETO");
                        System.out.println("**************************");

                        System.out.println("****INFORMCION DE FACTURA****");
                        System.out.println("DATOS DE FACTURA -- INICIADO");
                        facturaVenta.setPrefijo(cn.rs1.getString("id_prefijo"));
                        facturaVenta.setNoFactura(cn.rs1.getString("numero_factura"));
                        facturaVenta.setFechaFactura(cn.rs1.getString("fecha_factura"));
                        facturaVenta.setTipoCliente(cn.rs1.getString("tipo_cliente"));
                        facturaVenta.setNitCliente(cn.rs1.getString("nit"));
                        facturaVenta.setCedula(cn.rs1.getString("cedula"));
                        facturaVenta.setSubtotal(Float.parseFloat(cn.rs1.getString("subtotal")));
                        facturaVenta.setDescuento(Float.parseFloat(cn.rs1.getString("descuento")));
                        facturaVenta.setTotal(Float.parseFloat(cn.rs1.getString("total")));
                        facturaVenta.setIva(Float.parseFloat(cn.rs1.getString("iva")));
                        System.out.println("DATOS DE FACTURA -- COMPLETADO");

                        String sw_auditar_automatico = cn.rs1.getString("sw_auditar_automatico");
                        String id_estado_auditoria = cn.rs1.getString("id_estado_auditoria");

                        facturaVenta.setRecibosAsociados(recibosFactura());

                        detalles.setDetallesFactura(detallesFactura());
                        facturaVenta.setDetalles(detalles);

                        datosFacturas.agregarFacturaVenta(facturaVenta);
                        System.out.println("INFORMACION DE FACTURA -- COMPLETO");

                        if(sw_auditar_automatico.equals("S")){
                            if(!id_estado_auditoria.equals("2")){
                                System.out.println("***********************************************PREPARANDO ENVIO FACTURA WS************************************");
                                InfoFacturacion infoFacturacion = new InfoFacturacion();
    
                                infoFacturacion.setDatosTerceros(datosTerceros);
                                infoFacturacion.setDatosFacturas(datosFacturas);
    
                                System.out.println("Factura:" + infoFacturacion.toString());
    
                                InfoFACResult result = port.recibirInfoFactura(infoFacturacion);
    
                                System.out.println("Result:" + result.resultado());
                                System.out.println("Result:" + result.auditado());
    
                                System.out.println("*********");
                                System.out.println(result);
                                System.out.println("*********");
                                resultadoAuditarFacturas(result);
                                System.out.println("-----------------------------------------------FIN DE ENVIO DE FACTURA (ID "+idFactura+")-------------------------------------");
                            }else{
                                System.out.println("****************************************LA FACTURA YA SE ENVIO ANTES (ID "+idFactura+")********************************");
                            }
                        }else{
                            System.out.println("****************************************NO SE ENVIA LA FACTURA (ID "+idFactura+")********************************");
                        }
                        break;
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Facturacion --> function auditarFactura --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> clase Facturacion --> function auditarFactura --> Exception --> " + e.getMessage());
        }
        return cn.exito;

    }

    private List<DetalleFactura> detallesFactura() {

        List<DetalleFactura> detallesFactura = new ArrayList<DetalleFactura>();
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS2, cn.traerElQuery(135));
                cn.ps2.setString(1, idFactura);
                cn.ps2.setString(2, idFactura);
                System.out.println(((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    if (cn.rs2 != null) {
                        while (cn.rs2.next()) {
                            DetalleFactura detalle_factura = new DetalleFactura();

                            detalle_factura.setCentroCosto(cn.rs2.getString("centro_costo"));
                            detalle_factura.setDescripcionConcepto(cn.rs2.getString("descripcion"));
                            detalle_factura.setCodigoGrupo(cn.rs2.getString("codigo_grupo"));
                            detalle_factura.setTotal(Float.parseFloat(cn.rs2.getString("total")));
                            detalle_factura.setSubtotal(Float.parseFloat(cn.rs2.getString("sub_total")));
                            detalle_factura.setDescuento(Float.parseFloat(cn.rs2.getString("descuento")));
                            detalle_factura.setTarifaIVA(Float.parseFloat(cn.rs2.getString("tarifa_iva")));
                            detalle_factura.setGravado(cn.rs2.getString("gravado"));
                            detalle_factura.setIva(Float.parseFloat(cn.rs2.getString("iva")));

                            detallesFactura.add(detalle_factura);
                        }
                    }
                    cn.cerrarPS(Constantes.PS2);
                }
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Facturacion --> function detallesFactura --> SQLException --> " + e.getMessage());
        } catch (Exception e) {

            System.out.println(
                    "Error --> clase Facturacion --> function detallesFactura --> Exception --> " + e.toString());
        }
        return detallesFactura;
    }

    private Recibos recibosFactura() {

        System.out.println("****RECIBOS ASOCIADOS -- INICIADO****");

        Recibos recibos = new Recibos();

        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS3, cn.traerElQuery(150));
                cn.ps3.setString(1, idFactura);
                System.out.println(((LoggableStatement) cn.ps3).getQueryString());
                if (cn.selectSQL(Constantes.PS3)) {
                    if (cn.rs3 != null) {
                        while (cn.rs3.next()) {
                            System.out.println("ID RECIBO =====================================================> ");
                            recibos.agregarRecibo(auditarRecibo(cn.rs3.getString("id_recibo")));
                            System.out.println("ID RECIBO =====================================================> ");
                        }
                    }
                    System.out.println("****RECIBOS ASOCIADOS -- COMPLETO****");
                }
                System.out.println(
                        "****************************** EVALUANDO AJUSTE DE FACTURA (COPAGO)*********************************");
                for (int i = 0; i < recibos.getRecibos().size(); i++) {
                    if (recibos.getRecibos().get(i).getTipoRecibo() == 14) {
                        System.out.println("RECIBO SOBRANTE -> " + recibos.getRecibos().get(i).getNoRecibo());
                        for (int j = 0; j < recibos.getRecibos().size(); j++) {
                            if (recibos.getRecibos().get(j).getTipoRecibo() == 2) {
                                System.out.println("RECIBO -> " + recibos.getRecibos().get(j).getNoRecibo()
                                        + " APLICADO SOBRANTE");
                                if ((recibos.getRecibos().get(j).getValor()
                                        - recibos.getRecibos().get(i).getValor()) >= 0) {
                                    float valor = recibos.getRecibos().get(j).getValor()
                                            - recibos.getRecibos().get(i).getValor();
                                    recibos.getRecibos().get(j).setValor(valor);
                                    break;
                                }
                            }
                        }
                        System.out.println("RECIBO SOBRANTE -> " + recibos.getRecibos().get(i).getNoRecibo()
                                + " ELIMINADO DE ENVIO");
                        recibos.getRecibos().remove(i);
                    }
                }
                System.out.println(
                        "****************************** AJUSTE DE FACTURA TERMIANDO (COPAGO)*********************************");
                return recibos;
            }
            return null;
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Facturacion --> function recibosFactura --> SQLException --> " + e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.println(
                    "Error --> clase Facturacion --> function recibosFactura --> Exception --> " + e.getMessage());
            return null;
        }
    }

    public RecibodeCaja auditarRecibo(String id_recibo) {
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        
        System.out.println("*************AUDITANDO RECIBO DE CAJA*******");

        setIdRecibo(id_recibo);

        Recibos recibos = new Recibos();

        insercionFacturacion = new InsercionFacturacion();
        port = insercionFacturacion.getInsercionFacturacionPort();

        auditarPaciente(2);

        cn.exito = true;

        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS2, cn.traerElQuery(156));
                cn.ps2.setString(1, idRecibo);
                System.out.println(((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    if (cn.rs2 != null) {
                        while (cn.rs2.next()) {

                            RecibodeCaja reciboCaja = new RecibodeCaja();

                            reciboCaja.setPrefijo(cn.rs2.getString("id_prefijo"));
                            reciboCaja.setTipoRecibo(cn.rs2.getInt("id_concepto"));
                            reciboCaja.setNoRecibo(cn.rs2.getString("numero_recibo"));
                            reciboCaja.setCedula(cn.rs2.getString("identificacion_cliente"));
                            reciboCaja.setValor(cn.rs2.getFloat("valor"));
                            reciboCaja.setPrefijoFactura(cn.rs2.getString("prefijo_factura"));
                            reciboCaja.setNoFactura(cn.rs2.getString("numero_factura"));

                            recibos.agregarRecibo(reciboCaja);
 
                            if(cn.rs2.getString("sw_auditar_automatico").equals("S")){
                                if(!cn.rs2.getString("id_estado_auditoria").equals("2")){
                                    System.out.println("Recibo:" + recibos.toString());
                                    InfoFACResult result = port.recibosdeCaja(recibos);
    
                                    System.out.println("Resultado Recibos:\n" + result.toString());
                                    System.out.println("Result:" + result.resultado());
                                    System.out.println("Result:" + result.auditado());
    
                                    cn.cerrarPS(Constantes.PS2);
    
                                    resultadoAuditarRecibos(result, idRecibo);
                                    System.out.println("-************RECIBO ENVIADO-*************");
                                }else{
                                    System.out.println("-************EL RECIBO YA SE ENVIO ANTES- (ID "+idRecibo+")*************");
                                }
                            }else{
                                System.out.println("-************EL RECIBO NO SE ENVIA (ID "+idRecibo+")*************");
                            }
                            return reciboCaja;
                        }
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Facturacion --> function auditarRecibo --> SQLException --> " + e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.println(
                    "Error --> clase Facturacion --> function auditarRecibo --> Exception --> " + e.getMessage());
            return null;
        }
        return null;
    }

    private void resultadoAuditarRecibos(InfoFACResult result, String id_recibo) {
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                cn.prepareStatementIDU(Constantes.PS2, cn.traerElQuery(158));
                cn.ps2.setInt(1, ((result.auditado()) ? 2 : 3));
                cn.ps2.setString(2, result.resultado());
                cn.ps2.setString(3, id_recibo);
                System.out.println(((LoggableStatement) cn.ps2).getQueryString());

                cn.iduSQL(Constantes.PS2);
                cn.fintransaccion();

                System.out.println("Inserto en tabla recibos auditado: " + id_recibo + " - " + result.auditado() + " - "
                        + result.resultado());

            }
        } catch (SQLException e) {
            System.out.println("Error --> clase Facturacion --> function resultadoAuditarRecibos --> SQLException --> "
                    + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> clase Facturacion --> function resultadoAuditarRecibos --> Exception --> "
                    + e.getMessage());
        }

    }

    private void auditarPaciente(int tipo) {

        DatosTerceros datosTerceros = new DatosTerceros();

        Tercero paciente = new Tercero();

        switch (tipo) {
        case 1:
            System.out.println("---ENTRA A AUDITAR PACIENTE CON TIPO 1 (FACTURA)--");
            paciente = pacienteFactura();
            break;

        case 2:
            System.out.println("---ENTRA A AUDITAR PACIENTE CON TIPO 2 (RECIBO)---");
            paciente = pacienteRecibo();
            break;

        }

        if (paciente.getIdentificacion() != null) {

            datosTerceros.agregarTercero(paciente);

            System.out.println("Paciente: " + datosTerceros.toString());

            InfoFacturacion infoFacturacion = new InfoFacturacion();

            infoFacturacion.setDatosTerceros(datosTerceros);

            System.out.println("Factura:" + infoFacturacion.toString());

            InfoFACResult result = port.recibirInfoFactura(infoFacturacion);

            System.out.println("*******RESULTADO ENVIO*******");
            System.out.println("Result:" + result.resultado());
            System.out.println("-----------------------------");
            System.out.println("Result:" + result.auditado());
            System.out.println("*****************************");
        } else {
            System.out.println("Paciente vacio - idFactura: " + idFactura);
        }

    }

    private Tercero pacienteFactura() {

        Tercero paciente = new Tercero();

        cn.exito = true;
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS3, cn.traerElQuery(146));
                cn.ps3.setString(1, idFactura);
                System.out.println(((LoggableStatement) cn.ps3).getQueryString());

                if (cn.selectSQL(Constantes.PS3)) {
                    if (cn.rs3 != null) {
                        while (cn.rs3.next()) {

                            paciente.setTipoDocumento(cn.rs3.getString("tipo_documento"));
                            paciente.setIdentificacion(cn.rs3.getString("identificacion"));
                            paciente.setPrimApe(cn.rs3.getString("apellido1"));
                            paciente.setSegApe(cn.rs3.getString("apellido2"));
                            paciente.setPrimNom(cn.rs3.getString("nombre1"));
                            paciente.setSegNom(cn.rs3.getString("nombre2"));
                            paciente.setDireccion(cn.rs3.getString("direccion"));
                            paciente.setTelefono(cn.rs3.getString("telefono"));
                            paciente.setEmail(cn.rs3.getString("email"));
                            paciente.setPais(cn.rs3.getString("pais"));
                            paciente.setDepartamento(cn.rs3.getString("departamento"));
                            paciente.setMunicipio(cn.rs3.getString("municipio"));
                            paciente.setRegimen(cn.rs3.getString("regimen"));

                        }
                    }
                    System.out.println("-------------------------------------");
                    cn.cerrarPS(Constantes.PS3);
                }
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Facturacion --> function pacienteFactura --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> clase Facturacion --> function pacienteFactura --> Exception --> " + e.getMessage());
        }
        return paciente;

    }

    private Tercero pacienteRecibo() {

        Tercero paciente = new Tercero();

        cn.exito = true;
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS2, cn.traerElQuery(159));
                cn.ps2.setString(1, idRecibo);
                System.out.println(((LoggableStatement) cn.ps2).getQueryString());

                if (cn.selectSQL(Constantes.PS2)) {
                    if (cn.rs2 != null) {
                        while (cn.rs2.next()) {

                            paciente.setTipoDocumento(cn.rs2.getString("tipo_documento"));
                            paciente.setIdentificacion(cn.rs2.getString("identificacion"));
                            paciente.setPrimApe(cn.rs2.getString("apellido1"));
                            paciente.setSegApe(cn.rs2.getString("apellido2"));
                            paciente.setPrimNom(cn.rs2.getString("nombre1"));
                            paciente.setSegNom(cn.rs2.getString("nombre2"));
                            paciente.setDireccion(cn.rs2.getString("direccion"));
                            paciente.setTelefono(cn.rs2.getString("telefono"));
                            paciente.setEmail(cn.rs2.getString("email"));
                            paciente.setPais(cn.rs2.getString("pais"));
                            paciente.setDepartamento(cn.rs2.getString("departamento"));
                            paciente.setMunicipio(cn.rs2.getString("municipio"));
                            paciente.setRegimen(cn.rs2.getString("regimen"));
                            break;

                        }
                    }
                    cn.cerrarPS(Constantes.PS2);
                }
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Facturacion --> function pacienteRecibo --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> clase Facturacion --> function pacienteRecibo --> Exception --> " + e.getMessage());
        }
        return paciente;
    }

    private void resultadoAuditarFacturas(InfoFACResult result) {

        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementIDU(Constantes.PS2, cn.traerElQuery(136));

                cn.ps2.setInt(1, ((result.auditado()) ? 2 : 3));
                cn.ps2.setString(2, result.resultado());
                cn.ps2.setString(3, idFactura);

                // cn.selectSQL(Constantes.PS2)
                cn.iduSQL(Constantes.PS2);
                cn.cerrarPS(Constantes.PS2);

                System.out.println("Inserto en tabla facturacion auditado: " + idFactura + " - " + result.auditado()
                        + " - " + result.resultado());

                /*
                 * if (cn.selectSQL(Constantes.PS2)) { while (cn.rs2.next()) {
                 *
                 * System.out.println("Inserto en tabla facturacion auditado: "+idFactura+" - "
                 * +result.auditado()+" - "+ result.resultado());
                 *
                 * } cn.cerrarPS(Constantes.PS2); }
                 */
            }
        } catch (SQLException e) {
            System.out.println("Error --> clase Facturacion --> function resultadoAuditarFacturas --> SQLException --> "
                    + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> clase Facturacion --> function resultadoAuditarFacturas --> Exception --> "
                    + e.getMessage());
        }

    }

    public boolean anularFacturas() {

        System.out.println("---------------anularFactura()-------------");
        cn.exito = true;

        try {

            insercionFacturacion = new InsercionFacturacion();
            port = insercionFacturacion.getInsercionFacturacionPort();

            AnulaFactura anulaFactura = new AnulaFactura();

            if (cn.isEstado()) {

                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQuery(152));
                cn.ps1.setString(1, idFactura);
                System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    while (cn.rs1.next()) {
                        anulaFactura.setPrefijo(cn.rs1.getString("id_prefijo"));
                        anulaFactura.setNoFactura(cn.rs1.getString("numero"));
                        anulaFactura.setFechaAnulacion(cn.rs1.getString("fecha"));
                        anulaFactura.setMotivo(cn.rs1.getString("motivo"));

                    }

                    AnulaFacturas anulaFacturas = new AnulaFacturas();

                    anulaFacturas.agregarAnulaFactura(anulaFactura);

                    System.out.println("Factura:" + anulaFactura.toString());

                    InfoFACResult result = port.anularFacturas(anulaFacturas);

                    System.out.println("Resultado Anular:" + result.resultado());
                    System.out.println("Resultado :" + result.auditado());

                    resultadoAuditarFacturas(result);

                    cn.cerrarPS(Constantes.PS1);

                }

            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Facturacion --> function anularFacturas --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> clase Facturacion --> function anularFacturas --> Exception --> " + e.getMessage());
        }
        return cn.exito;
    }

    public boolean buscarTienePendienteLE() {
        cn.exito = true;
        try {
            if (cn.isEstado()) // if(TienePendienteLE().equals("NO_PENDIENTE")){
            {
                existe = "N";
            }
            // System.out.println("OKxxxxxxxxxxxxxxxxNO_PENDIENTE ");
            /*
             * } else {
             */
            existe = "S";
            // System.out.println("OKxxxxxxxxxxxxxxxx____PENDIENTE ");
            // }
        } catch (Exception exception) {
            System.out.println((new StringBuilder()).append(exception.getMessage())
                    .append(" en m\351todo crearFacturacion() de Facturacion.java").toString());
            cn.exito = false;
        }
        return cn.exito;
    }

    public boolean buscarSiFolioTieneDiligenciadosElementos(int idQuery) {
        cn.exito = true;
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS3, cn.traerElQuery(idQuery));
                cn.ps3.setInt(1, this.idCuenta);
                System.out.println(((LoggableStatement) cn.ps3).getQueryString());
                if (cn.selectSQL(Constantes.PS3)) {
                    if (cn.rs3 != null) {
                        while (cn.rs3.next()) {
                            if (cn.rs3.getString("NOTICIA").equals("DILIGENCIADO")) {
                                System.out.println("SI SE ENCUENTRA DILIGENCIADO EL  MOTIVO_ENFERMEDAD");
                                return true;
                            } else {
                                this.noticia = cn.rs3.getString("NOTICIA");
                                System.out.println("NO SE ENCUENTRA DILIGENCIADO EL  MOTIVO_ENFERMEDAD");
                                return false;
                            }
                        }
                    }
                    cn.cerrarPS(Constantes.PS3);
                }
            }
        } catch (SQLException e) {
            System.out.println(
                    e.getMessage() + " en metodo buscarSiFolioTieneDiligenciadosElementos(), de Facturacion.java");
            return false;
        } catch (Exception e) {
            System.out.println(
                    e.getMessage() + " en metodo buscarSiFolioTieneDiligenciadosElementos(), de Facturacion.java");
            return false;
        }
        return false;
    }

    public boolean buscarElementosObligatoriosFolio() { // System.out.println("var1= "+this.var1);
                                                        // System.out.println("var2= "+this.var2);
        boolean estado = true;
        this.noticia = "";
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS2, cn.traerElQuery(443));
                // cn.ps2.setString(1,this.var2.trim());

                // System.out.println("ccccccccccccccc="+
                // ((LoggableStatement)cn.ps2).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {

                        if (cn.rs2.getString("SW_MOTIVO_ENFERMEDAD").equals("S")) {
                            estado = buscarSiFolioTieneDiligenciadosElementos(450);
                            if (!estado) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_ANTECEDENTES").equals("S")) {
                            estado = buscarSiFolioTieneDiligenciadosElementos(451);
                            if (!estado) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_POS_OPERATORIO").equals("S")) {
                            estado = buscarSiFolioTieneDiligenciadosElementos(454);
                            if (!estado) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_EXAMEN_FISICO").equals("S")) {
                            estado = buscarSiFolioTieneDiligenciadosElementos(452);
                            if (!estado) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_DIAGNOSTICOS").equals("S")) {
                            estado = buscarSiFolioTieneDiligenciadosElementos(453);
                            if (!estado) {
                                return false;
                            }
                        }
                        if (cn.rs2.getString("SW_SIGNOS_VITALES").equals("S")) {
                            estado = buscarSiFolioTieneDiligenciadosElementos(452);
                            if (!estado) {
                                return false;
                            }
                        }

                        /*
                         * if( cn.rs2.getString("SW_CONDUCTA").equals("S") ){ estado =
                         * buscarSiFolioTieneDiligenciadosElementos(446); if(!estado) return false; }
                         * if( cn.rs2.getString("SW_ARCHIVOS_ADJUNTOS").equals("S") ){ estado =
                         * buscarSiFolioTieneDiligenciadosElementos(446); if(!estado) return false; }
                         * if( cn.rs2.getString("SW_ADMIN_MEDICAMENTO").equals("S") ){ estado =
                         * buscarSiFolioTieneDiligenciadosElementos(446); if(!estado) return false; }
                         * if( cn.rs2.getString("SW_HOJA_GASTOS").equals("S") ){ estado =
                         * buscarSiFolioTieneDiligenciadosElementos(446); if(!estado) return false; }
                         */
                    }
                    cn.cerrarPS(Constantes.PS2);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en metodo buscarElementosObligatoriosFolio(),, de Facturacion.java");
            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en metodo buscarElementosObligatoriosFolio(), de Facturacion.java");
            return false;
        }
        return estado;
    }

    public boolean limpiarCandados() {
        boolean estado = true;
        try {
            sql.delete(0, sql.length());
            sql.append(" DELETE FROM INVENTARIO.BODEGAS_EXISTENCIAS_TMP; ");
            sql.append(" DELETE FROM INVENTARIO.BODEGAS_EXISTENCIAS_LOTE_TMP; ");

            sql.append(
                    " delete from inventario.candado; DELETE FROM  INVENTARIO.CANDADO_TMP; DELETE FROM  INVENTARIO.TRANSACCIONES_TMP; delete from inventario.candado_documentos; ");
            sql.append(" DELETE FROM INVENTARIO.TRANSACCIONES_TMP  ");

            cn.prepareStatementIDU(Constantes.PS1, sql);
            System.out.println(
                    "LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLIMPIANDO " + ((LoggableStatement) cn.ps1).getQueryString());
            cn.iduSQL(Constantes.PS1);
        } catch (SQLException e) {
            System.out.println(e.getMessage() + " en metodo 1 limpiarCandados(), de Facturacion.java");
            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en metodo 2 limpiarCandados(), de Facturacion.java");
            return false;
        }
        return estado;
    }

    public boolean hacerTrasladoEntreBodegas() {

        // System.out.println("-----------------------------------------------------------------------------------------\n
        // hacerTrasladoEntreBodegas::ID_DOC origen= "+this.var1 + "
        // id_BODEGA_destino="+this.var6);
        this.esTraslado = "SI";
        boolean estado = true;
        this.noticia = "xxx";
        try {
            cn.iniciatransaccion();

            this.noticia = "DOCUMENTOS DE TRASLADOS FINALIZADOS CON EXITO";
            /* actualizar las fechas de los dos documentos para que queden iguales */
            sql.delete(0, sql.length());
            sql.append(" UPDATE inventario.documentos ");
            sql.append(
                    " SET fecha_crea = (select fecha_crea from inventario.documentos where id = (select id_doc from inventario.candado)), ");
            sql.append(
                    "   fecha_cierre = (select fecha_cierre from inventario.documentos where id = (select id_doc from inventario.candado)) ");
            sql.append(" WHERE id =( SELECT  id_doc FROM inventario.candado_documentos ) ; ");
            cn.prepareStatementIDU(Constantes.PS2, sql);
            System.out.println("-- UPDATE FECHAS DOC DE TRASLADO -- " + ((LoggableStatement) cn.ps2).getQueryString());
            cn.iduSQL(Constantes.PS2);

            limpiarCandados();

            cn.fintransaccion();
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en metodo 2 verificaTransaccionesInventario(), de Facturacion.java");
            return false;
        }
        return estado;
    }

    public boolean cerrarDocumentoSolicitudesInventario() { // System.out.println("ID_DOC= "+this.var2);
        boolean estado = true;
        this.esTraslado = "NO";
        this.noticia = "xxx";
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();

                // if(hacerSalidasInventario()){
                this.noticia = "DOCUMENTO FINALIZADO CON EXITO..";
                /*
                 * } else{ this.noticia =
                 * "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS"; }
                 */
                limpiarCandados();
                cn.fintransaccion();
            }

        } catch (Exception e) {
            System.out.println(
                    e.getMessage() + " en metodo 2 cerrarDocumentoSolicitudesInventario(), de Facturacion.java");
            return false;
        }
        return estado;
    }

    public boolean verificaTransaccionesInventario() { // System.out.println("ID_DOC= "+this.var2);
        boolean estado = true;
        this.esTraslado = "NO";
        this.noticia = "xxx";
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();

                // if(hacerSalidasInventario()){ System.out.println("tttttttttttttttttttt");
                this.noticia = "DOCUMENTO FINALIZADO CON EXITO..";
                // }
                /*
                 * else{ System.out.println("ffffffffffffffffffffff"); this.noticia =
                 * "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS"; }
                 */
                limpiarCandados();
                cn.fintransaccion();
            }

        } catch (Exception e) {
            System.out.println(e.getMessage() + " en metodo 2 verificaTransaccionesInventario(), de Facturacion.java");
            return false;
        }
        return estado;
    }

    /*
     * MeTODO USADO POR DESPACHO DE PROGRAMACION Y RESPUESTA A SOLICITUDES NO MUEVE
     * INVENTARIOS
     */
    public boolean salidaTransaccionesInventario() { // System.out.println("ID_DOC= "+this.var2);
        boolean estado = true;
        this.esTraslado = "SI";
        this.noticia = "xxx";
        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();

                // if(hacerSalidasInventario()){
                this.noticia = "DOCUMENTO FINALIZADO CON EXITO..";
                /*
                 * } else{ this.noticia =
                 * "NO SE PERMITE FINALIZAR POR EL CONTROL DE EXISTENCIAS"; }
                 */
                limpiarCandados();
                cn.fintransaccion();
            }

        } catch (Exception e) {
            System.out.println(e.getMessage() + " en metodo 2 verificaTransaccionesInventario(), de Facturacion.java");
            return false;
        }
        return estado;
    }

    public int maximoValorTablaDocInventario() {
        int valor = 0;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append(" SELECT id_doc FROM inventario.candado_documentos;");
                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        // this.idDocumento = cn.rs2.getInt("id_doc") ;
                        cn.cerrarPS(Constantes.PS2);
                    }
                }
            }
        } catch (SQLException e) {
            // System.out.println("Error --> clase -->DocumentoHc.java-- function
            // maximoValorTabla --> SQLException --> "+e.getMessage());
        } catch (Exception e) {
            // System.out.println("Error --> clase -->DocumentoHc.java-- function
            // maximoValorTabla --> Exception --> " + e.getMessage());
        }
        if (valor == 0) {
            valor = 1;
        }
        return valor;
    }

    public boolean preguntarMedicamentoNoPost() {
        FacturacionTodo = "";
        cn.exito = true;
        StringBuffer str = new StringBuffer();
        try {
            if (cn.isEstado()) {
                str.delete(0, str.length());
                str.append(" SELECT POS FROM INVENTARIO.ARTICULO WHERE ID = ?");
                cn.prepareStatementSEL(2, str);
                // cn.ps2.setInt( 1, this.var1);

                System.out.println((new StringBuilder()).append("OKPOSsss = ")
                        .append(((LoggableStatement) cn.ps2).getQueryString()).toString());
                if (cn.selectSQL(2)) {
                    for (; cn.rs2.next(); cn.cerrarPS(2)) {
                        FacturacionTodo = cn.rs2.getString("POS");
                    }
                }
            }
        } catch (SQLException sqlexception) {
            System.out.println((new StringBuilder()).append("Error --> Facturacion.java-- esNoPos--> SQLException --> ")
                    .append(sqlexception.getMessage()).toString());
        } catch (Exception exception) {
            System.out.println((new StringBuilder()).append("Error --> Facturacion.java-- esNoPos --> Exception --> ")
                    .append(exception.getMessage()).toString());
        }
        return cn.exito;
    }

    public boolean preguntarProcedimientoNoPost() {
        FacturacionTodo = "";
        cn.exito = true;
        StringBuffer str = new StringBuffer();
        try {
            if (cn.isEstado()) {
                str.delete(0, str.length());
                str.append("SELECT POS FROM FACTURACION.PROCEDIMIENTOS WHERE ID = ?");
                cn.prepareStatementSEL(2, str);
                // cn.ps2.setString( 1, this.var2);

                System.out.println((new StringBuilder()).append("OK POS = ")
                        .append(((LoggableStatement) cn.ps2).getQueryString()).toString());
                if (cn.selectSQL(2)) {
                    for (; cn.rs2.next(); cn.cerrarPS(2)) {
                        FacturacionTodo = cn.rs2.getString("POS");
                    }
                }
            }
        } catch (SQLException sqlexception) {
            System.out.println((new StringBuilder()).append("Error --> Facturacion.java-- esNoPos--> SQLException --> ")
                    .append(sqlexception.getMessage()).toString());
        } catch (Exception exception) {
            System.out.println((new StringBuilder()).append("Error --> Facturacion.java-- esNoPos --> Exception --> ")
                    .append(exception.getMessage()).toString());
        }
        return cn.exito;
    }

    public Object buscarInformacionParaMedicamentoNoPos() {
        ArrayList<FacturacionVO> resultado = new ArrayList<FacturacionVO>();
        String var = "";
        FacturacionVO parametro;

        try {
            if (cn.isEstado()) {
                sql.delete(0, sql.length());
                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQuery(17));
                // cn.ps1.setInt(1,this.var1);
                // System.out.println("xxx44---------"+((LoggableStatement)cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    while (cn.rs1.next()) {
                        parametro = new FacturacionVO();
                        parametro.setVar1(cn.rs1.getString("VAR1"));
                        parametro.setVar2(cn.rs1.getString("VAR2"));
                        parametro.setVar3(cn.rs1.getString("VAR3"));
                        parametro.setVar4(cn.rs1.getString("VAR4"));
                        parametro.setVar5(cn.rs1.getString("VAR5"));
                        parametro.setVar6(cn.rs1.getString("VAR6"));
                        /*
                         * parametro.setVar7(cn.rs1.getString("VAR7"));
                         */

                        resultado.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Facturacion --> function buscarInformacionFacturacion --> SQLException --> "
                            + e.getMessage());
        } catch (Exception e) {
            System.out
                    .println("Error --> clase Facturacion --> function buscarInformacionFacturacion --> Exception --> "
                            + e.getMessage());
        }
        return resultado;
    }

    /**
     * Consulta los valores de la cuenta y de la factura y los retorna a
     * guardar_Hc_xml.jsp
     *
     * @return
     */
    public boolean consultarValoresCuenta() {

        System.out.println("---------------consultarValoresCuenta()-------------");
        cn.exito = true;

        if (cn.isEstado()) {
            try {
                recibosCopagoFactura = new ArrayList<String>();
                otrosConceptosFactura = new ArrayList<String>();
                idReciboAjuste = "";

                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQuery(Integer.parseInt(idQuery)));
                cn.ps1.setString(1, idFactura);
                System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                if (cn.selectSQL(Constantes.PS1)) {
                    while (cn.rs1.next()) {

                        // totalCuenta = cn.rs1.getString("total_cuenta");
                        valorNoCubierto = cn.rs1.getString("valor_nocubierto");
                        valorCubierto = cn.rs1.getString("valor_cubierto");
                        valorDescuento = cn.rs1.getString("valor_descuento");
                        medioPago = cn.rs1.getString("medio_pago");
                        totalFactura = cn.rs1.getString("total_factura");
                        idEstado = cn.rs1.getString("id_estado");
                        estado = cn.rs1.getString("estado");
                        idFactura = cn.rs1.getString("id_factura");
                        numeroFactura = cn.rs1.getString("numero_factura");
                        prefijoFactura = cn.rs1.getString("id_prefijo");
                        formaPago = cn.rs1.getString("tipo_pago");
                        if (!cn.rs1.getString("id_recibo").equals("")) {
                            switch (cn.rs1.getInt("concepto")) {
                            case 2:
                                recibosCopagoFactura.add(cn.rs1.getString("id_recibo"));
                                break;

                            case 14:
                                idReciboAjuste = cn.rs1.getString("id_recibo");
                                break;

                            default:
                                otrosConceptosFactura.add(cn.rs1.getString("id_recibo"));
                                break;
                            }
                        }
                    }
                    cn.cerrarPS(Constantes.PS1);
                }

            } catch (SQLException e) {
                System.out
                        .println("Error --> clase Facturacion --> function consultarValoresCuenta --> SQLException --> "
                                + e.getMessage());
            } catch (Exception e) {
                System.out.println("Error --> clase Facturacion --> function consultarValoresCuenta --> Exception --> "
                        + e.getMessage());
            }
        }
        return cn.exito;
    }

    public Sgh.Utilidades.Conexion getCn() {
        return cn;
    }

    public void setCn(Sgh.Utilidades.Conexion value) {
        cn = value;
    }

    public java.lang.Integer getId() {
        return id;
    }

    public void setId(java.lang.Integer value) {
        id = value;
    }

    public java.lang.String getEsTraslado() {
        return esTraslado;
    }

    public void setEsTraslado(java.lang.String value) {
        esTraslado = value;
    }

    public java.lang.String getFacturacionTodo() {
        return FacturacionTodo;
    }

    public void setFacturacionTodo(java.lang.String value) {
        FacturacionTodo = value;
    }

    public java.lang.String getExiste() {
        return existe;
    }

    public void setExiste(java.lang.String value) {
        existe = value;
    }

    public java.lang.String getVarStr4() {
        return varStr4;
    }

    public void setVarStr4(java.lang.String value) {
        varStr4 = value;
    }

    public java.lang.String getVarStr5() {
        return varStr5;
    }

    public void setVarStr5(java.lang.String value) {
        varStr5 = value;
    }

    public java.lang.String getNoticia() {
        return noticia;
    }

    public void setNoticia(java.lang.String value) {
        noticia = value;
    }

    public Sgh.Utilidades.Constantes getConstantes() {
        return constantes;
    }

    public void setConstantes(Sgh.Utilidades.Constantes value) {
        constantes = value;
    }

    public java.lang.Integer getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(java.lang.Integer value) {
        idCuenta = value;
    }

    public java.lang.Integer getVarInt1() {
        return varInt1;
    }

    public void setVarInt1(java.lang.Integer value) {
        varInt1 = value;
    }

    public java.lang.Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(java.lang.Integer value) {
        idPlan = value;
    }

    public java.lang.Integer getIdTarifario() {
        return idTarifario;
    }

    public void setIdTarifario(java.lang.Integer value) {
        idTarifario = value;
    }

    public java.lang.String getIdProcedimiento() {
        return idProcedimiento;
    }

    public void setIdProcedimiento(java.lang.String value) {
        idProcedimiento = value;
    }

    public java.lang.Integer getIdCita() {
        return idCita;
    }

    public void setIdCita(java.lang.Integer value) {
        idCita = value;
    }

    public java.lang.Integer getCantProcedimientos() {
        return cantProcedimientos;
    }

    public void setCantProcedimientos(java.lang.Integer value) {
        cantProcedimientos = value;
    }

    public java.lang.String getTipoTarifarioPlan() {
        return tipoTarifarioPlan;
    }

    public void setTipoTarifarioPlan(java.lang.String value) {
        tipoTarifarioPlan = value;
    }

    public java.lang.Integer getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(java.lang.Integer value) {
        idLiquidacion = value;
    }

    public java.lang.String getId_tipo_unidad() {
        return id_tipo_unidad;
    }

    public void setId_tipo_unidad(java.lang.String value) {
        id_tipo_unidad = value;
    }

    public String getIdQuery() {
        return idQuery;
    }

    public void setIdQuery(String value) {
        idQuery = value;
    }

    public String getTotalCuenta() {
        return totalCuenta;
    }

    public void setTotalCuenta(String totalCuenta) {
        this.totalCuenta = totalCuenta;
    }

    public String getValorNoCubierto() {
        return valorNoCubierto;
    }

    public void setValorNoCubierto(String valorNoCubierto) {
        this.valorNoCubierto = valorNoCubierto;
    }

    public String getValorCubierto() {
        return valorCubierto;
    }

    public void setValorCubierto(String valorCubierto) {
        this.valorCubierto = valorCubierto;
    }

    public String getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public String getTotalFactura() {
        return totalFactura;
    }

    public void setTotalFactura(String totalFactura) {
        this.totalFactura = totalFactura;
    }

    public String getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(String idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(String idFactura) {
        this.idFactura = idFactura;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public String getValorDescuento() {
        return valorDescuento;
    }

    public void setValorDescuento(String valorDescuento) {
        this.valorDescuento = valorDescuento;
    }

    public String getEstadoAjuste() {
        return estadoAjuste;
    }

    public void setEstadoAjuste(String estadoAjuste) {
        this.estadoAjuste = estadoAjuste;
    }

    public String getIdEstadoCuenta() {
        return idEstadoCuenta;
    }

    public void setIdEstadoCuenta(String idEstadoCuenta) {
        this.idEstadoCuenta = idEstadoCuenta;
    }

    public String getEstadoCuenta() {
        return estadoCuenta;
    }

    public void setEstadoCuenta(String estadoCuenta) {
        this.estadoCuenta = estadoCuenta;
    }

    public String getIdRecibo(){
        return idRecibo;
    }

    public void setIdRecibo(String idRecibo) {
        this.idRecibo = idRecibo;
    }

    public List<String> getRecibosCopagoFactura() {
        return recibosCopagoFactura;
    }

    public String getReciboAjuste() {
        return idReciboAjuste;
    }

    public List<String> getOtrosConceptosFactura() {
        return otrosConceptosFactura;
    }

    public String getPrefijoFactura() {
        return prefijoFactura;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void test() {
    };
}
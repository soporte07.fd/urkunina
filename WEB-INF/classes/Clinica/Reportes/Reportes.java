package Clinica.Reportes;
import Sgh.Utilidades.*;
import Clinica.Presentacion.*;
import java.io.*;

import java.text.*;
import java.util.*;
import java.util.Date;
import java.lang.*;
import java.text.SimpleDateFormat;
import java.sql.*;
import java.io.File;
import java.lang.String;
import java.sql.*;
/**
*@(#)Clase Conexion.java version 1.02 2015/12/09
*Copyright(c) 2015 Firmas Digital.
* JAS
*/

public class Reportes
{
   private int contador;
   private String id;
   private String nombre;
   private String idProceso;
   private String idTipo;
   private String fechaDesde;
   private String fechaHasta;
   private String parametro1;
   private String parametrosFiltros;
   public Constantes constantes;
   public int cantFiltros;
   //public Fecha fecha;
   private Conexion cn;
   private StringBuffer sql = new StringBuffer();
   private StringBuffer sql2 = new StringBuffer();
   private StringBuffer sql3 = new StringBuffer();


   public Reportes(){
       contador=0;
       cantFiltros = 0;
       parametrosFiltros ="";
       id="";
       nombre="";
       idProceso="";
       idTipo="";
       fechaDesde="";
       fechaHasta="";
       parametro1="";
   }

   public Object buscarReportes(int sidx, String sord, int start, int limit){
     ArrayList<ReportesVO> resultado=new ArrayList<ReportesVO>();
     String var="", anio="",mes="", dia="", hora="", minuto="";
     ReportesVO parametro;
     int cont=1;
     try{
        if(cn.isEstado()){
            this.cn.prepareStatementSEL(Constantes.PS1,cn.traerElQuery(26));

            cn.ps1.setString(1,this.nombre);
            cn.ps1.setString(2,this.idProceso);
            cn.ps1.setString(3,this.idTipo);

            if(cn.selectSQL(Constantes.PS1)){
               while(cn.rs1.next()) {
                  parametro = new ReportesVO();
                  parametro.setId(cn.rs1.getString("id"));
                  parametro.setNomProceso(cn.rs1.getString("nom_proceso"));
                  parametro.setNombre(cn.rs1.getString("nombre"));
                  parametro.setExplicacion(cn.rs1.getString("explicacion"));
                  parametro.setDesTipo(cn.rs1.getString("descripcion"));
                  parametro.setCantColumnas(cn.rs1.getString("totColum"));
                  resultado.add(parametro);
               }
                 cn.cerrarPS(Constantes.PS1);
             }
          }
       }catch(SQLException e){
           System.out.println("Error --> Reportes --> function buscarReportes --> SQLException --> "+e.getMessage());
       }catch(Exception e){
           System.out.println("Error --> Reportes --> function buscarReportes --> Exception --> " + e.getMessage());
       }
       return resultado;
   }

   public String traerElQuery(){
       String valor = "";
       StringBuffer sql2 = new StringBuffer();
       try{
       if(cn.isEstado()){
       sql2.delete(0,sql2.length());
       if(cn.getMotor().equals("sqlserver"))
            sql2.append(" select query from reporte.reporte where id= ? ");
       else sql2.append(" select query from reporte.reporte where id= ?::integer ");
       this.cn.prepareStatementSEL(Constantes.PS2,sql2);
       cn.ps2.setString(1,this.id);
       if(cn.selectSQL(Constantes.PS2)){
          while(cn.rs2.next()){
               valor =  cn.rs2.getString("query") ;
               cn.cerrarPS(Constantes.PS2);
          }
        }
       }
      }catch(SQLException e){
       //    System.out.println("Error --> clase  -->Reportes-- function traerElQuery --> SQLException --> "+e.getMessage());
        //   e.printStackTrace();
      }catch(Exception e){
        //   System.out.println("Error --> clase  -->Reportes-- function traerElQuery --> Exception --> " + e.getMessage());
        //   e.printStackTrace();
      }
       return  valor;
   }






  /*para informes */
   public String traerElQueryInforme(){
       String valor = "";
       StringBuffer sql2 = new StringBuffer();
       try{
       if(cn.isEstado()){
       sql2.delete(0,sql2.length());
       if(cn.getMotor().equals("sqlserver")){
			sql2.append(" select query from reporte.informe where id= ? ");
	   }
	   else{            System.out.println("--POSTGRES A ");
			sql2.append(" select query from reporte.informe where id= ?::integer ");
	   }
       this.cn.prepareStatementSEL(Constantes.PS2,sql2);
       cn.ps2.setString(1,this.id);
       if(cn.selectSQL(Constantes.PS2)){
          while(cn.rs2.next()){
               valor =  cn.rs2.getString("query") ;
               cn.cerrarPS(Constantes.PS2);
          }
        }
       }
      }catch(SQLException e){
           //System.out.println("Error --> clase  -->Reportes-- function traerElQueryInforme --> SQLException --> "+e.getMessage());
      }catch(Exception e){
         //  System.out.println("Error --> clase  -->Reportes-- function traerElQueryInforme --> Exception --> " + e.getMessage());
      }
       return  valor;
   }



     /*para informes */
   public Object buscarDatosDelReporte(){
     ArrayList<ReportesVO> resultado=new ArrayList<ReportesVO>();
     ReportesVO parametro;
     int contColumnas=0;
     String nomCampo="nombre_campo" , query="";
     try{
        if(cn.isEstado()){          /*----------------------------------------para extraer el nombre de las columnas                                   */
            sql.delete(0,sql.length());
			if(cn.getMotor().equals("sqlserver")){
				sql.append(" SELECT  nombre_campo FROM  reporte.informe_columnas where id_reporte = ? order by ID ASC ");
			}
			else{            System.out.println("--POSTGRES A ");
				sql.append(" SELECT  nombre_campo FROM  reporte.informe_columnas where id_reporte = ?::integer order by ID ASC ");
			}
            this.cn.prepareStatementSEL(Constantes.PS1,sql);
            cn.ps1.setString(1,this.id);

            System.out.println(((LoggableStatement)cn.ps1).getQueryString());
            if(cn.selectSQL(Constantes.PS1)){
               while(cn.rs1.next()) {
                  contColumnas++;
                  parametro = new ReportesVO();
                  parametro.setC1(cn.rs1.getString(nomCampo));
                  resultado.add(parametro);
               }
                 cn.cerrarPS(Constantes.PS1);
             }

             /*----- insertar fechas parametros --------------------------------------------------*/

             cn.iniciatransaccion();
             sql.delete(0,sql.length());
             if(cn.getMotor().equals("sqlserver")){
				sql.append(" delete from reporte.informe_parametro_temporal; insert into reporte.informe_parametro_temporal(fecha_desde, fecha_hasta) values(?,?) ");
			 }
			 else{            System.out.println("--POSTGRES A ");
				sql.append(" delete from reporte.informe_parametro_temporal; insert into reporte.informe_parametro_temporal(fecha_desde, fecha_hasta) values(?::timestamp,?::timestamp) ");
			 }
             cn.prepareStatementIDU(Constantes.PS1,sql);
             cn.ps1.setString(1,this.fechaDesde +" 00:00:00");
             cn.ps1.setString(2,this.fechaHasta +" 23:59:59");

   			 cn.iduSQL(Constantes.PS1);
             cn.fintransaccion();

             String [] temp = null;
             temp = parametrosFiltros.split("_-");
            /* ingresar los parametros*/

             sql3.delete(0,sql3.length());
             for(int i=1;i<temp.length;i++){
                  System.out.println("LENGTH= "+temp.length+"---------------------Temp -> " + i+"= " + temp[i]);
                  if(temp[i].equals("x.X.x")){
                  //   cn.ps1.setString(i,"");
					 System.out.println("Parametro vacio");
			      }else{
                 //    cn.ps1.setString(i,temp[i]);
					 System.out.println("* Parametro: " + i+"=> " + temp[i]);

                     sql3.append(" UPDATE reporte.informe_parametro_temporal  SET p"+ i +" = '" +temp[i]+ "'; ");
				  }
              }

             cn.prepareStatementIDU(Constantes.PS3,sql3);
             if( cn.imprimirConsola )
                System.out.println("--parametros:::::::::: "+((LoggableStatement)cn.ps3).getQueryString());
             cn.iduSQL(Constantes.PS3);



            /* fin ingresar los parametros*/



            nomCampo="";
            sql.delete(0,sql.length());
            sql.append(traerElQueryInforme());

            this.cn.prepareStatementSEL(Constantes.PS1,sql);

            if(this.cantFiltros !=0){
               /*   String [] temp = null;
                  temp = parametrosFiltros.split("_-");
                 */

            /*    for(int i=1;i<temp.length;i++){
                      System.out.println("LENGTH= "+temp.length+"---------------------Temp -> " + i+"= " + temp[i]);
                      if(temp[i].equals("x.X.x")){
                         cn.ps1.setString(i,"");
						 System.out.println("Parametro vacio");
				      }else{
                         cn.ps1.setString(i,temp[i]);
						 System.out.println("* Parametro: " + i+"=> " + temp[i]);

                        // sql3.append(" UPDATE reporte.informe_parametro_temporal  SET p"+ i +" = " +temp[i]+ "; ");
					  }
                  }		*/
            }






            if( cn.imprimirConsola )
               System.out.println(((LoggableStatement)cn.ps1).getQueryString());

            if(cn.selectSQL(Constantes.PS1)){
               while(cn.rs1.next()) {
                  for(int j=1; j<=contColumnas;j++){
                     parametro = new ReportesVO();
                     parametro.setC1(cn.rs1.getString("c"+String.valueOf(j)));
                     resultado.add(parametro);
                  }
               }
               cn.cerrarPS(Constantes.PS1);
             }

         }

       }catch(SQLException e){
       }catch(Exception e){
       }
       return resultado;
   }


    /*para historia clinica vista previa */
   public Object buscarDatosDelReporteParametros(){
     ArrayList<ReportesVO> resultado=new ArrayList<ReportesVO>();
     ReportesVO parametro;
     int contColumnas=0;
     String nomCampo="nombre_campo" , query="";
     try{
        if(cn.isEstado()){
            sql.delete(0,sql.length());


            if(cn.getMotor().equals("sqlserver"))
                  sql.append(" SELECT  nombre_campo FROM  reporte.columnas where  id_reporte = ? order by id ASC ");
            else  sql.append(" SELECT  nombre_campo FROM  reporte.columnas where  id_reporte = ?::integer order by id ASC ");

            this.cn.prepareStatementSEL(Constantes.PS1,sql);
            cn.ps1.setString(1,this.id);

            if(cn.selectSQL(Constantes.PS1)){
               while(cn.rs1.next()) {
                  contColumnas++;
                  parametro = new ReportesVO();
                  parametro.setC1(cn.rs1.getString(nomCampo));
                  resultado.add(parametro);
               }
                 cn.cerrarPS(Constantes.PS1);
             }

             cn.iniciatransaccion();
             sql.delete(0,sql.length());
             sql.append(" delete from reporte.parametro_temporal ; insert into reporte.parametro_temporal(parametro1) values(?) ");
             cn.prepareStatementIDU(Constantes.PS1,sql);
             cn.ps1.setString(1,this.parametro1 );
             if( cn.imprimirConsola )
                System.out.println("this.parametro1= " +this.parametro1+ "--INSERTAR COMO PARAMETRO=\n "+((LoggableStatement)cn.ps1).getQueryString());
             cn.iduSQL(Constantes.PS1);
             cn.fintransaccion();

             nomCampo="";
             
             sql.delete(0,sql.length());
             sql.append(traerElQuery());
             //sql.append(" select 'dc1' as c1 , 'dc2' as c2 ,'dc3' as c3        union all  select 'xc1' as c1 , 'xc2' as c2 , 'xc3' as c3    union all  select 'mc1' as c1 , 'mc2' as c2 , 'mc3' as c3  ");
             this.cn.prepareStatementSEL(Constantes.PS1,sql);

             if( cn.imprimirConsola )
              System.out.println( contColumnas+" SELECT DEL REPORTE=\n "+((LoggableStatement)cn.ps1).getQueryString());

             if(cn.selectSQL(Constantes.PS1)){
               while(cn.rs1.next()) {
                  for(int j=1; j<=contColumnas;j++){
                     parametro = new ReportesVO();
                     parametro.setC1(cn.rs1.getString("c"+String.valueOf(j)));
                     resultado.add(parametro);
                  }
               }
               cn.cerrarPS(Constantes.PS1);
             }
/*----- fin datos del select --------------------------------------------------*/
         }
       }catch(SQLException e){
           System.out.println("Error --> clase Reportes --> function buscarDatosDelReporteParametros --> SQLException --> "+e.getMessage());
       //    e.printStackTrace();
       }catch(Exception e){
           System.out.println("Error --> clase Reportes --> function buscarDatosDelReporteParametros --> Exception --> " + e.getMessage());
         //  e.printStackTrace();
       }
       return resultado;
   }







   public int maximoValorIdTabla(String nombreTabla) {
       int valor =0;
       StringBuffer sql2 = new StringBuffer();
       try{
       if(cn.isEstado()){
       sql2.delete(0,sql2.length());
       sql2.append("select MAX(id)+1 as maximo from "+nombreTabla);
       this.cn.prepareStatementSEL(Constantes.PS2,sql2);
       if(cn.selectSQL(Constantes.PS2)){
          while(cn.rs2.next()){
               valor =  cn.rs2.getInt("maximo") ;
               cn.cerrarPS(Constantes.PS2);
          }
        }
       }
      }catch(SQLException e){
          // System.out.println("Error --> clase  -->Reportes-- function maximoValor --> SQLException --> "+e.getMessage());
          // e.printStackTrace();
      }catch(Exception e){
          // System.out.println("Error --> clase  -->Reportes-- function maximoValor --> Exception --> " + e.getMessage());
          // e.printStackTrace();
      }
       return  valor;
   }



    public Sgh.Utilidades.Conexion getCn() {
        return cn;
    }

    public void setCn(Sgh.Utilidades.Conexion value) {
        cn = value;
    }

    public int getContador() {
        return contador;
    }


    public void setContador(int value) {
        contador = value;
    }


    public java.lang.String getId() {
        return id;
    }

    public void setId(java.lang.String value) {
        id = value;
    }

    public java.lang.String getNombre() {
        return nombre;
    }

    public void setNombre(java.lang.String value) {
        nombre = value;
    }

    public java.lang.String getIdProceso() {
        return idProceso;
    }

    public void setIdProceso(java.lang.String value) {
        idProceso = value;
    }

    public java.lang.String getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(java.lang.String value) {
        idTipo = value;
    }

    public java.lang.String getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(java.lang.String value) {
        fechaDesde = value;
    }

    public java.lang.String getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(java.lang.String value) {
        fechaHasta = value;
    }

    public java.lang.String getParametro1() {
        return parametro1;
    }

    public void setParametro1(java.lang.String value) {
        parametro1 = value;
    }
    public java.lang.String getParametrosFiltros() {
        return parametrosFiltros;
    }

    public void setParametrosFiltros(java.lang.String value) {
        parametrosFiltros = value;
    }

    public int getCantFiltros() {
        return cantFiltros;
    }

    public void setCantFiltros(int value) {
        cantFiltros = value;
    }

}
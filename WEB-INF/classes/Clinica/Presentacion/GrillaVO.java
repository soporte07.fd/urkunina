/**
*@(#)Clase Conexion.java version 1.02 2015/12/09
*Copyright(c) 2015 Firmas Digital.
* JAS
*/
package Clinica.Presentacion;

public class GrillaVO {

   private int contador;
   private String id;
   private String nombre;
   private String explicacion;
   private String cantColumnas;
   private String c1, c2, c3, c4, c5, c6,c7, c8, c9, c10, c11, c12, c13, 
           c14, c15, c16, c17, c18, c19, c20, c21, c22, c23, c24, c25, c26, 
           c27, c28, c29, c30, c31, c32, c33, c34, c35, c36, c37, c38, c39,
           c40,c41,c42,c43,c44,c45, c46, c47, c48, c49, c50, c51, c52, c53,
           c54, c55, c56, c57,c58, c59, c60;
   private String tipoRow;




   public GrillaVO(){
       contador=0;
       id="";
       nombre="";
       explicacion="";
       cantColumnas="";
       c1=""; c2=""; c3=""; c4=""; c5=""; c6="";c7=""; c8=""; c9=""; c10=""; 
       c11=""; c12="";c13=""; c14=""; c15=""; c16=""; c17=""; c18=""; c19=""; 
       c20=""; c21=""; c22=""; c23=""; c24="";c25=""; c26=""; c27=""; c28=""; 
       c29=""; c30=""; c31=""; c32=""; c33=""; c34=""; c35=""; c36="";c37="";
       c38=""; c39="";c40=""; c41="";c42="";c43="";c44="";c45=""; c46=""; c47="";
       c48=""; c49=""; c50=""; c51=""; c52=""; c53=""; c54=""; c55=""; c56="";
       c57=""; c58=""; c59=""; c60="";
       tipoRow="";
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int value) {
        contador = value;
    }

    public java.lang.String getId() {
        return id;
    }

    public void setId(java.lang.String value) {
        id = value;
    }

    public java.lang.String getNombre() {
        return nombre;
    }

    public void setNombre(java.lang.String value) {
        nombre = value;
    }

    public java.lang.String getExplicacion() {
        return explicacion;
    }

    public void setExplicacion(java.lang.String value) {
        explicacion = value;
    }

    public java.lang.String getCantColumnas() {
        return cantColumnas;
    }

    public void setCantColumnas(java.lang.String value) {
        cantColumnas = value;
    }

    public java.lang.String getC1() {
        return c1;
    }

    public void setC1(java.lang.String value) {
       if(value==null)
             c1 = "";
       else  c1 = value;

    }

    public java.lang.String getC2() {
        return c2;
    }

    public void setC2(java.lang.String value) {
       if(value==null)
             c2 = "";
       else  c2 = value;
    }

    public java.lang.String getC3() {
        return c3;
    }

    public void setC3(java.lang.String value) {
       if(value==null)
             c3 = "";
       else  c3 = value;
    }

    public java.lang.String getC4() {
        return c4;
    }

    public void setC4(java.lang.String value) {
       if(value==null)
             c4 = "";
       else  c4 = value;
    }

    public java.lang.String getC5() {
        return c5;
    }

    public void setC5(java.lang.String value) {
       if(value==null)
             c5 = "";
       else  c5 = value;
    }

    public java.lang.String getC6() {
        return c6;
    }

    public void setC6(java.lang.String value) {
       if(value==null)
             c6 = "";
       else  c6 = value;
    }

    public java.lang.String getC7() {
        return c7;
    }

    public void setC7(java.lang.String value) {
       if(value==null)
             c7 = "";
       else  c7 = value;
    }

    public java.lang.String getC8() {
        return c8;
    }

    public void setC8(java.lang.String value) {
       if(value==null)
             c8 = "";
       else  c8 = value;
    }

    public java.lang.String getC9() {
        return c9;
    }

    public void setC9(java.lang.String value) {
       if(value==null)
             c9 = "";
       else  c9 = value;
    }

    public java.lang.String getC10() {
        return c10;
    }

    public void setC10(java.lang.String value) {
      if(value==null)
             c10 = "";
       else  c10 = value;
    }

    public java.lang.String getC11() {
        return c11;
    }

    public void setC11(java.lang.String value) {
       if(value==null)
             c11 = "";
       else  c11 = value;
    }

    public java.lang.String getC12() {
        return c12;
    }

    public void setC12(java.lang.String value) {
       if(value==null)
             c12 = "";
       else  c12 = value;
    }

    public java.lang.String getC13() {
        return c13;
    }

    public void setC13(java.lang.String value) {
       if(value==null)
             c13 = "";
       else  c13 = value;
    }

    public java.lang.String getC14() {
        return c14;
    }

    public void setC14(java.lang.String value) {
       if(value==null)
             c14 = "";
       else  c14 = value;
    }

    public java.lang.String getC15() {
        return c15;
    }

    public void setC15(java.lang.String value) {
       if(value==null)
             c15 = "";
       else  c15 = value;
    }

    public java.lang.String getC16() {
        return c16;
    }

    public void setC16(java.lang.String value) {
       if(value==null)
             c16 = "";
       else  c16 = value;
    }

    public java.lang.String getC17() {
        return c17;
    }

    public void setC17(java.lang.String value) {
       if(value==null)
             c17 = "";
       else  c17 = value;
    }

    public java.lang.String getC18() {
        return c18;
    }

    public void setC18(java.lang.String value) {
       if(value==null)
             c18 = "";
       else  c18 = value;
    }

    public java.lang.String getC19() {
        return c19;
    }

    public void setC19(java.lang.String value) {
       if(value==null)
             c19 = "";
       else  c19 = value;
    }

    public java.lang.String getC20() {
        return c20;
    }

    public void setC20(java.lang.String value) {
       if(value==null)
             c20 = "";
       else  c20 = value;
    }

    public java.lang.String getC21() {
        return c21;
    }

    public void setC21(java.lang.String value) {
       if(value==null)
             c21 = "";
       else  c21 = value;
    }

    public java.lang.String getC22() {
        return c22;
    }

    public void setC22(java.lang.String value) {
       if(value==null)
             c22 = "";
       else  c22 = value;
    }

    public java.lang.String getC23() {
        return c23;
    }

    public void setC23(java.lang.String value) {
       if(value==null)
             c23 = "";
       else  c23 = value;
    }

    public java.lang.String getC24() {
        return c24;
    }

    public void setC24(java.lang.String value) {
       if(value==null)
             c24 = "";
       else  c24 = value;
    }

    public java.lang.String getC25() {
        return c25;
    }

    public void setC25(java.lang.String value) {
       if(value==null)
             c25 = "";
       else  c25 = value;
    }

    public java.lang.String getC26() {
        return c26;
    }

    public void setC26(java.lang.String value) {
       if(value==null)
             c26 = "";
       else  c26 = value;
    }

    public java.lang.String getC27() {
        return c27;
    }

    public void setC27(java.lang.String value) {
       if(value==null)
             c27 = "";
       else  c27 = value;
    }

    public java.lang.String getC28() {
        return c28;
    }

    public void setC28(java.lang.String value) {
       if(value==null)
             c28 = "";
       else  c28 = value;
    }

    public java.lang.String getC29() {
        return c29;
    }

    public void setC29(java.lang.String value) {
       if(value==null)
             c29 = "";
       else  c29 = value;
    }

    public java.lang.String getC30() {
        return c30;
    }

    public void setC30(java.lang.String value) {
       if(value==null)
             c30 = "";
       else  c30 = value;
    }

    public java.lang.String getTipoRow() {
        return tipoRow;
    }

    public void setTipoRow(java.lang.String value) {
        tipoRow = value;
    }
    public java.lang.String getC31() {
        return c31;
    }

    public void setC31(java.lang.String value) {
        c31 = value;
    }

    public java.lang.String getC32() {
        return c32;
    }

    public void setC32(java.lang.String value) {
        c32 = value;
    }

    public java.lang.String getC33() {
        return c33;
    }

    public void setC33(java.lang.String value) {
        c33 = value;
    }

    public java.lang.String getC34() {
        return c34;
    }

    public void setC34(java.lang.String value) {
        c34 = value;
    }

    public java.lang.String getC35() {
        return c35;
    }

    public void setC35(java.lang.String value) {
        c35 = value;
    }

    public java.lang.String getC36() {
        return c36;
    }

    public void setC36(java.lang.String value) {
        c36 = value;
    }

    public java.lang.String getC37() {
        return c37;
    }

    public void setC37(java.lang.String value) {
        c37 = value;
    }

    public java.lang.String getC38() {
        return c38;
    }

    public void setC38(java.lang.String value) {
        c38 = value;
    }

    public java.lang.String getC39() {
        return c39;
    }

    public void setC39(java.lang.String value) {
        c39 = value;
    }

	public java.lang.String getC40() {
        return c40;
    }

    public void setC40(java.lang.String value) {
        c40 = value;
    }

    public String getC41() {
        return c41;
    }

    public void setC41(String value) {
        c41 = value;
    }

     public String getC42() {
        return c42;
    }

    public void setC42(String value) {
        c42 = value;
    }

     public String getC43() {
        return c43;
    }

    public void setC43(String value) {
        c43 = value;
    }

     public String getC44() {
        return c44;
    }

    public void setC44(String value) {
        c44 = value;
    }

     public String getC45() {
        return c45;
    }

    public void setC45(String value) {
        c45 = value;
    }

    public String getC46() {
        return c46;
    }

    public void setC46(String value) {
        c46 = value;
    }

    public String getC47() {
        return c47;
    }

    public void setC47(String value) {
        c47 = value;
    }

    public String getC48() {
        return c48;
    }

    public void setC48(String value) {
        c48 = value;
    }

    public String getC49() {
        return c49;
    }

    public void setC49(String value) {
        c49 = value;
    }

    public String getC50() {
        return c50;
    }

    public void setC50(String value) {
        c50 = value;
    }

    public String getC51() {
        return c51;
    }

    public void setC51(String c51) {
        this.c51 = c51;
    }

    public String getC52() {
        return c52;
    }

    public void setC52(String c52) {
        this.c52 = c52;
    }

    public String getC53() {
        return c53;
    }

    public void setC53(String c53) {
        this.c53 = c53;
    }

    public String getC54() {
        return c54;
    }

    public void setC54(String c54) {
        this.c54 = c54;
    }

    public String getC55() {
        return c55;
    }

    public void setC55(String c55) {
        this.c55 = c55;
    }

    public String getC56() {
        return c56;
    }

    public void setC56(String c56) {
        this.c56 = c56;
    }

    public String getC57() {
        return c57;
    }

    public void setC57(String c57) {
        this.c57 = c57;
    }

    public String getC58() {
        return c58;
    }

    public void setC58(String c58) {
        this.c58 = c58;
    }

    public String getC59() {
        return c59;
    }

    public void setC59(String c59) {
        this.c59 = c59;
    }

    public String getC60() {
        return c60;
    }

    public void setC60(String c60) {
        this.c60 = c60;
    }
    
 }























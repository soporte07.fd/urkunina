package Clinica.Utilidades;

import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.commons.fileupload.*;
import java.util.*;
import java.text.*;
import Sgh.Utilidades.*;
/**
*@(#)Clase Conexion.java version 1.02 2015/12/09
*Copyright(c) 2015 Firmas Digital.
* JAS
*/
public class UPC extends HttpServlet {
    String mensaje="";
    String fileName="";
    String FechaFileName ="";
    String nombreFile ="";
    String nombreFileComprobar ="";
    String evento ="";
    String subCarpeta ="";
    String extension =".pdf";
    String archivoTamano ="";
    String var1 ="", var2 ="", var3 ="", var4 ="";
    String usuario_crea ="";
    int serial = 0;
    public Conexion cn;
    public Sesion sesion;
    private StringBuffer sql = new StringBuffer();
	private StringBuffer sql2 = new StringBuffer();

    public UPC() {
     // sesion= new Sesion();
      var1 = "";
      var2 = "";
      var3 = "";
      var4 = "";
      evento ="";
      usuario_crea ="";
      serial=0;
      fileName ="";
      archivoTamano="";
      nombreFileComprobar ="";
    }
     /*
    public  boolean existeArchivitoCandado(){      // si existe el archivo entonces lo elimina para insertar en el caso de modificaciones

	    File listaarchivos 	= new File(Constantes.pathDocPDF+"/zcandados/"+"/"); // Constantes.pathDocPDF+"/candado.txt"
	    String[] lista = listaarchivos.list();
        String archivo="";

        try{
		    archivo = lista[0];
		    System.out.println("archivos adentro: "+archivo);

            if(archivo.equals("candado.txt")){
               System.out.println("El fichero SI existe");
               return true;
            }
            else{
                System.out.println("El fichero NO existe");
               return false;
            }
	    }
        catch(Exception e){
            return false;
        }

    }
     */

     public boolean crearCandado(){
          try{
              if(!this.subCarpeta.equals("ayudaDx") ){   //  System.out.println("CREAR CANDADOo");
                 sql.delete(0,sql.length());
    			 sql.append("delete from archivos.candado where archivo = 'null.pdf'; \n");
    			 sql.append("delete from archivos.candado_dx where archivo = 'null.pdf'; \n");
    			 sql.append("INSERT INTO archivos.candado(id, usuario, archivo,  archivo_tamano, sub_carpeta  ) VALUES (1,?,?,?,?)");
                 cn.prepareStatementIDU(Constantes.PS1,sql);
                 cn.ps1.setString(1,this.usuario_crea);
                 cn.ps1.setString(2,this.nombreFile + extension);
                 cn.ps1.setString(3,this.archivoTamano);
                 cn.ps1.setString(4,this.subCarpeta);
                 cn.iduSQL(Constantes.PS1);
                 return true;
             }else{
                 sql.delete(0,sql.length());              // System.out.println("CREAR CANDADO DXx");
    			 sql.append("delete from archivos.candado where archivo = 'null.pdf';\n");
    			 sql.append("delete from archivos.candado_dx where archivo = 'null.pdf';\n");
    			 sql.append("INSERT INTO archivos.candado_dx(id, usuario, archivo,  archivo_tamano, sub_carpeta  ) VALUES (1,?,?,?,?)");
                 cn.prepareStatementIDU(Constantes.PS1,sql);
                 cn.ps1.setString(1,this.usuario_crea);
                 cn.ps1.setString(2,this.nombreFile + extension);
                 cn.ps1.setString(3,this.archivoTamano);
                 cn.ps1.setString(4,this.subCarpeta);
                 cn.iduSQL(Constantes.PS1);
                 return true;
             }
          }
          catch(Exception e){
            return false;
          }
    }
     public  boolean  eliminarCandado(){
          try{
              if(!this.subCarpeta.equals("ayudaDx") ){     //  System.out.println("BORRANDO CANDADO");
                 sql.delete(0,sql.length());
    			 sql.append("DELETE FROM  archivos.candado ");
                 cn.prepareStatementIDU(Constantes.PS1,sql);
                 cn.iduSQL(Constantes.PS1);
                 return true;
              }else{                                   //  System.out.println("BORRANDO CANDADO dx");
                 sql.delete(0,sql.length());
    			 sql.append("DELETE FROM  archivos.candado_dx");
                 cn.prepareStatementIDU(Constantes.PS1,sql);
                 cn.iduSQL(Constantes.PS1);
                 return true;
              }
          }
          catch(Exception e){
            return false;
          }
    }

    protected void subirArchivo(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if(crearCandado()){
                if(   subirArchivo(request)  ){
                       mensaje="111.ARCHIVO SE ADJUNTO CORRECTAMENTE.!!!";
                       eliminarCandado();
                       if(comprobarSiExisteEnBaseDatos())
                           if(comprobarSiExisteArchivoFisico()){
                              cn.destroy();
                              out.println("<SCRIPT language=javascript>  window.open('http://190.147.111.78:8383/docPDF/"+this.subCarpeta+"/"+this.nombreFileComprobar+this.extension+"?tamano="+this.archivoTamano+  "' )</SCRIPT>");
                           }
                           else{
                               cn.destroy();
                               mensaje="5. ALERTA AL SUBIR EL ARCHIVO, \n VERIFIQUE ANTES DE VOLVER A INTANTAR";
                               out.println("<SCRIPT language=javascript>alert('"+mensaje+"');</SCRIPT>");
                               out.close();
                           }
                }else{
                    if(mensaje.equals(""))
                       cn.destroy();
                       mensaje="2.Hay una peticion en curso, por favor espere un momento e intente nuevamente!!";
                       out.println("<SCRIPT language=javascript>alert('"+mensaje+"');</SCRIPT>");
                       out.close();
                }
        }
        else{
                cn.destroy();
                mensaje="3.Error al enviar, Revise el archivo subido e intente nuevamente !!!!!!!!!!!!";
                out.println("<SCRIPT language=javascript>alert('"+mensaje+"');</SCRIPT>");
                out.close();
        }

    }

     public  boolean comprobarSiExisteArchivoFisico(){      // si existe el archivo entonces lo elimina para insertar en el caso de modificaciones
//	    File listaarchivos 	= new File(Constantes.pathDocPDF+"/"+ this.subCarpeta+"/"+"/");


        File   listaarchivos = new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+"/" );
	    String[] lista = listaarchivos.list();
        String archivo="";
        try{
            for(int n=0;n<lista.length;n++){ //busca en todos  los directorios (Usuarios)
			    archivo = lista[n];
                if(archivo.equals(this.nombreFileComprobar+this.extension)){
                        File   fichero = new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+this.nombreFileComprobar+this.extension );
                        DecimalFormat df = new DecimalFormat("#");
                        float longitud = fichero.length();

                              System.out.println("El fichero ha sido subido satisfactoriamente tamano enviado=" +this.archivoTamano +" y tamano en servidor= " + df.format(longitud/1024)  );                        //
                        if(   this.archivoTamano.equals( df.format(longitud/1024) ) )
                              System.out.println("ESTE SI ES EL ARCHIVO");
                        else {
                              System.out.println("ESTE NO ES EL ARCHIVO");
                              return false;
                        }


/*
		if(longitud>1024000000)
			System.out.println(archivo+"  : " + df.format(longitud/1024000000) + " Gb");
		else if(longitud>1024000)
			System.out.println(archivo+"  : " + df.format(longitud/1024000) + " Mb");
		else if(longitud>1024)

			System.out.println(archivo+"  : " + df.format(longitud/1024) + " Kb");
		else
			System.out.println(archivo+"  : " + df.format(longitud) + " bytes");
*/



                       return true;
                }
	        }
	    }
        catch(Exception e){
             System.out.println("fallo al comprobarSiExisteArchivoFisico");
            return false;
        }
		return true;
    }






 public boolean comprobarSiExisteEnBaseDatos(){
    boolean van = false;
    try{
       if(cn.isEstado()){
           if(this.subCarpeta.equals("fisico") ){
			  sql2.delete(0,sql2.length());
			  sql2.append("SELECT id_paciente FROM archivos.hc_fisica WHERE id_paciente = " +this.var1);
			  this.cn.prepareStatementSEL(Constantes.PS2,sql2);
			  if(cn.selectSQL(Constantes.PS2)){
				 while(cn.rs2.next()){
				      this.nombreFileComprobar =  cn.rs2.getString("id_paciente");
					//  System.out.println("/////fisico base de datos////="+ this.nombreFileComprobar);
					  cn.cerrarPS(Constantes.PS2);
					  van = true;
				 }
			   }
           }
           else if(this.subCarpeta.equals("identificacion") ){

			  sql2.delete(0,sql2.length());
			  sql2.append("SELECT  tipo_id||identificacion identificacion FROM archivos.paciente_documento where identificacion LIKE substring('" +this.nombreFile+ "' from 3 for 22) ");
			  this.cn.prepareStatementSEL(Constantes.PS2,sql2);
            //  System.out.println("queryyyyyyyyyyyyyyy = "+((LoggableStatement)cn.ps2).getQueryString());

			  if(cn.selectSQL(Constantes.PS2)){
				 while(cn.rs2.next()){
				      this.nombreFileComprobar =  cn.rs2.getString("identificacion");
				//	  System.out.println("/////identificacion base de datos////="+ this.nombreFileComprobar);
					  cn.cerrarPS(Constantes.PS2);
					  van = true;
				 }
			   }
           }
           else if(this.subCarpeta.equals("remision") ){
			  sql2.delete(0,sql2.length());
			  sql2.append("SELECT id_cita FROM archivos.remision_admision where id_cita = " +this.var1 );
			  this.cn.prepareStatementSEL(Constantes.PS2,sql2);
              System.out.println("remisioonnn = "+((LoggableStatement)cn.ps2).getQueryString());

			  if(cn.selectSQL(Constantes.PS2)){
				 while(cn.rs2.next()){
				      this.nombreFileComprobar =  cn.rs2.getString("id_cita");
				//	  System.out.println("/////remision base de datos////="+ this.nombreFileComprobar);
					  cn.cerrarPS(Constantes.PS2);
					  van = true;
				 }
			   }
           }
           else if(this.subCarpeta.equals("carnet") ){
			  sql2.delete(0,sql2.length());
			  sql2.append("SELECT id_cita FROM archivos.cita_carnet where id_cita = " +this.var1 );
			  this.cn.prepareStatementSEL(Constantes.PS2,sql2);
              System.out.println("carnet = "+((LoggableStatement)cn.ps2).getQueryString());

			  if(cn.selectSQL(Constantes.PS2)){
				 while(cn.rs2.next()){
				      this.nombreFileComprobar =  cn.rs2.getString("id_cita");
				//	  System.out.println("/////carnet base de datos////="+ this.nombreFileComprobar);
					  cn.cerrarPS(Constantes.PS2);
					  van = true;
				 }
			   }
           }
           else if(this.subCarpeta.equals("ayudaDx") ){
			  sql2.delete(0,sql2.length());
			  sql2.append("SELECT id FROM archivos.evolucion_ayuda_dx where id = " +this.nombreFile );
			  this.cn.prepareStatementSEL(Constantes.PS2,sql2);
              System.out.println("ayudaDx = "+((LoggableStatement)cn.ps2).getQueryString());

			  if(cn.selectSQL(Constantes.PS2)){
				 while(cn.rs2.next()){
				      this.nombreFileComprobar =  cn.rs2.getString("id");
					//  System.out.println("/////evolucion_ayuda_dx base de datos////="+ this.nombreFileComprobar);
					  cn.cerrarPS(Constantes.PS2);
					  van = true;
				 }
			   }
           }
		   /* insertar documentos varios */
		   else if(this.subCarpeta.equals("otros") ){
			  sql2.delete(0,sql2.length());
			  sql2.append("SELECT id FROM archivos.otros where id = " +this.nombreFile );
			  this.cn.prepareStatementSEL(Constantes.PS2,sql2);
            //  System.out.println("otros = "+((LoggableStatement)cn.ps2).getQueryString());
			  if(cn.selectSQL(Constantes.PS2)){
				 while(cn.rs2.next()){
				      this.nombreFileComprobar =  cn.rs2.getString("id");
					//  System.out.println("/////otros base de datos////="+ this.nombreFileComprobar);
					  cn.cerrarPS(Constantes.PS2);
					  van = true;
				 }
			   }
           }
       }
    }
    catch(Exception e){
     System.out.println(e.getMessage()+" en m�todo comprobarSiExisteEnBaseDatos() de UPC.java");
    }
    return van;
  }




  protected void processLimpiaDeLaBaseDatos(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();

    if(quitarArchivoDB() ){
        mensaje="ARCHIVO SE ELIMINO CORRECTAMENTE DE LA BASE DE DATOS";
    }else{
        if(mensaje.equals(""))
           mensaje="Error al eliminar";
    }
    out.println("<html>");
    out.println("<head>");
    out.println("<SCRIPT language=javascript>alert('"+mensaje+"');</SCRIPT>");
    out.println("</head>");
    out.println("<body>");
    out.println("</body>");
    out.println("</html>");
    out.close();
 }


  protected void processElimina(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();

    if( eliminaArchivoFisico() ){
        mensaje="ARCHIVO SE ELIMINO CORRECTAMENTE.";
    }else{
        if(mensaje.equals(""))
           mensaje="Error al eliminar";
    }
    out.println("<html>");
    out.println("<head>");
    out.println("<SCRIPT language=javascript>alert('"+mensaje+"');</SCRIPT>");
    out.println("</head>");
    out.println("<body>");
    out.println("</body>");
    out.println("</html>");
    out.close();
 }
 public  boolean eliminaArchivoFisico(){   // si existe el archivo entonces lo elimina para insertar en el caso de modificaciones
    System.out.println("******** 1");
    File listaarchivos 	= new File(Constantes.pathDocPDF+"/"+ this.subCarpeta+"/"+"/");
    String[] lista = listaarchivos.list();
    String archivo="";

    try{
        for(int n=0;n<lista.length;n++){ //busca en todos  los directorios (Usuarios)
		    archivo = lista[n];

            if(archivo.equals(this.nombreFile+this.extension)){
                File fichero = new File(Constantes.pathDocPDF+"/"+ this.subCarpeta+"/"+archivo);

                if (fichero.delete()){
                   System.out.println("El fichero ha sido borrado satisfactoriamente 2");
                   quitarArchivoDB();
                }
                else{
                   System.out.println("El fichero NOO ha sido borrado ");
                   return false;
                }
            }
        }
    }
    catch(Exception e){
        return false;
    }
	return true;
}
  public boolean quitarArchivoDB(){
    try{
       if(cn.isEstado()){

           if(this.subCarpeta.equals("documentoHC") ){
             sql.delete(0,sql.length());
			 sql.append("UPDATE  HC.EVOLUCION SET  ADJUNTO = 'N' WHERE ID = ?::integer");
             cn.prepareStatementIDU(Constantes.PS1,sql);
             cn.ps1.setString(1, this.nombreFile);  //idDocumento
             cn.iduSQL(Constantes.PS1);
           }
           else if(this.subCarpeta.equals("identificacion") ){
             sql.delete(0,sql.length());
			 sql.append("DELETE FROM ARCHIVOS.PACIENTE_DOCUMENTO WHERE ID =?::integer");
             cn.prepareStatementIDU(Constantes.PS1,sql);
             cn.ps1.setString(1, this.var1);
             cn.iduSQL(Constantes.PS1);
           }
           else if(this.subCarpeta.equals("fisico") ){
             sql.delete(0,sql.length());
			 sql.append("DELETE FROM ARCHIVOS.HC_FISICA WHERE ID =?::integer");
             cn.prepareStatementIDU(Constantes.PS1,sql);
             cn.ps1.setString(1, this.var1);
             cn.iduSQL(Constantes.PS1);
           }

           else if(this.subCarpeta.equals("remision") ){
             sql.delete(0,sql.length());
             sql.append("DELETE FROM ARCHIVOS.REMISION_ADMISION WHERE ID_CITA = ?::integer");
			 cn.prepareStatementIDU(Constantes.PS1,sql);
             cn.ps1.setString(1, this.nombreFile);
             cn.iduSQL(Constantes.PS1);
           }
           else if(this.subCarpeta.equals("carnet") ){
             sql.delete(0,sql.length());
			 sql.append("DELETE FROM ARCHIVOS.CITA_CARNET WHERE ID_CITA = ?::integer");

			 cn.prepareStatementIDU(Constantes.PS1,sql);
             cn.ps1.setString(1, this.nombreFile);
             cn.iduSQL(Constantes.PS1);
           }
           else if(this.subCarpeta.equals("ayudaDx") ){
             sql.delete(0,sql.length());
			 sql.append("DELETE FROM ARCHIVOS.EVOLUCION_AYUDA_DX WHERE id = ?::integer");
			 cn.prepareStatementIDU(Constantes.PS1,sql);
             cn.ps1.setString(1, this.nombreFile);
             cn.iduSQL(Constantes.PS1);
           }
		   else if(this.subCarpeta.equals("otros") ){
             sql.delete(0,sql.length());
			 sql.append("DELETE FROM ARCHIVOS.OTROS WHERE ID =?::integer");
			 cn.prepareStatementIDU(Constantes.PS1,sql);
             cn.ps1.setString(1, this.var1);
             cn.iduSQL(Constantes.PS1);
           }
       }
    }
    catch(Exception e){
     System.out.println(e.getMessage()+" en m�todo quitarArchivoDB() de UPC.java");
     e.printStackTrace();
     cn.exito=false;
    }
    return cn.exito;
  }





     public  boolean existeArchivo(){      // si existe el archivo entonces lo elimina para insertar en el caso de modificaciones

	    File listaarchivos 	= new File(Constantes.pathDocPDF+"/"+ this.subCarpeta+"/"+"/");
	    String[] lista = listaarchivos.list();
        String archivo="";

        try{
            for(int n=0;n<lista.length;n++){ //busca en todos  los directorios (Usuarios)
			    archivo = lista[n];
                if(archivo.equals(this.nombreFile)){
                    File fichero = new File(this.nombreFile);
                   //  eliminarArchivoUpc();
                    if (fichero.delete()) {
                       System.out.println("El fichero ha sido borrado satisfactoriamentex");
                    }
                    else{
                       System.out.println("El fichero NOO ha sido borrado satisfactoriamentex");
                       return false;
                    }
                }
	        }
	    }
        catch(Exception e){
            return false;
        }
		return true;
    }

               /*

 public boolean verArchivosFisicos(){
    boolean van = true;
    String ruta= Constantes.pathDocPDF + this.subCarpeta;
    System.out.println("--------ruta-----------" + ruta);

    File listaarchivos 	= new File(ruta);
    String[] lista = listaarchivos.list();
    String archivo="";

    try{
        for(int n=0;n<lista.length;n++){ //busca en todos  los directorios (Usuarios)
		    archivo = archivo + lista[n] +"\n";
        }
        System.out.println("--------archivo-----------" + archivo);

        FileWriter ficheroW = null;
        PrintWriter pw = null;
        try
        {
            ficheroW = new FileWriter(ruta+"/listado.txt");
            pw = new PrintWriter(ficheroW);

                pw.println(archivo);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para
           // asegurarnos que se cierra el fichero.
           if (null != ficheroW)
              ficheroW.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }

    }
    catch(Exception e){
        return false;
    }
	return true;
 }
    */

/* public boolean subirArchivo(HttpServletRequest req, PrintWriter out ){         System.out.println("seguimiento al guardar fichero 1");
    try{

        DiskFileUpload fu = new DiskFileUpload();// construimos el objeto que es capaz de parsear la perici�n
     //   fu.setSizeMax(102400000); //  maximo numero de bytes512 K
    //    fu.setSizeThreshold(90000000); //   90000000 999'999.999 MIENTRAS EL ARCHIVO ESTE POR DEBAJO DE ESTE TAMA�O (k), SE GUARDARA EN DISCO DIRECTAMENTE
   //     fu.setRepositoryPath(Constantes.pathDocPDF+"/"+this.subCarpeta+"/tmp/"); // si se excede de la anterior tama�o, se ira guardando temporalmente, en la sgte direccion

System.out.println(".seguimiento al guardar fichero 2");

       List fileItems = fu.parseRequest(req);    //ordenamos procesar los ficheros
/*        if(fileItems == null){
            //depura("La lista es nula");
            return false;
        }

System.out.println("seguimiento al guardar fichero 3");

        Iterator i = fileItems.iterator();        //Iteramos por cada fichero
        FileItem actual = null;
//        while (i.hasNext()){
           // System.out.println(i.hasNext()+"::iiiiiiiiiii="+i);
            actual = (FileItem)i.next();
            fileName = actual.getName();          // construimos un objeto file para recuperar el trayecto completo
            File fichero = new File(fileName);
            fichero = new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+this.nombreFile+this.extension );
            actual.write(fichero);   //escribimos el fichero colgando del nuevo path

System.out.println("seguimiento al guardar fichero 4");
          //  break;
 //       }
System.out.println("seguimiento al guardar fichero 5");
        actual.delete();
System.out.println("seguimiento al guardar fichero 6");
       return guardarArchivoDB();
    }
    catch(Exception e) {
        System.out.println("Error de Aplicaci�n UPC procesaFicheros" + e.getMessage());
        return false;
    }
 }
*/
 public boolean subirArchivo(HttpServletRequest req ){
    try{
      int var = 0;
        DiskFileUpload fu = new DiskFileUpload(); // construimos el objeto que es capaz de parsear la perici�n
        List fileItems = fu.parseRequest(req);    //ordenamos procesar los ficheros
        Iterator i = fileItems.iterator();       //Iteramos por cada fichero
        Iterator j = fileItems.iterator();
        Iterator k = fileItems.iterator();
        Iterator l = fileItems.iterator();
        Iterator m = fileItems.iterator();
        Iterator n = fileItems.iterator();
        Iterator o = fileItems.iterator();
        Iterator p = fileItems.iterator();
        Iterator q = fileItems.iterator();
        FileItem actual = null;
        var = (int) (Math.random() * 8) + 1;


      switch(var){
       case 1:
            System.out.println("switch1");
            actual = (FileItem)i.next();
            fileName = actual.getName();          // construimos un objeto file para recuperar el trayecto completo
            File fichero = new File(fileName);
            fichero= new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+this.nombreFile+this.extension );
            actual.write(fichero);   //escribimos el fichero colgando del nuevo path
       break;
       case 2:
            System.out.println("switch2");
            actual = (FileItem)j.next();
            fileName = actual.getName();
            File fichero_2 = new File(fileName);
            fichero_2= new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+this.nombreFile+this.extension );
            actual.write(fichero_2);
       break;
       case 3:
            System.out.println("switch3");
            actual = (FileItem)k.next();
            fileName = actual.getName();
            File fichero_3 = new File(fileName);
            fichero_3= new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+this.nombreFile+this.extension );
            actual.write(fichero_3);
       break;
       case 4:
            System.out.println("switch4");
            actual = (FileItem)l.next();
            fileName = actual.getName();
            File fichero_4 = new File(fileName);
            fichero_4= new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+this.nombreFile+this.extension );
            actual.write(fichero_4);
       break;
       case 5:
            System.out.println("switch5");
            actual = (FileItem)m.next();
            fileName = actual.getName();
            File fichero_5 = new File(fileName);
            fichero_5= new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+this.nombreFile+this.extension );
            actual.write(fichero_5);
       break;
       case 6:
            System.out.println("switch6");
            actual = (FileItem)n.next();
            fileName = actual.getName();
            File fichero_6 = new File(fileName);
            fichero_6= new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+this.nombreFile+this.extension );
            actual.write(fichero_6);
       break;
       case 7:
            System.out.println("switch7");
            actual = (FileItem)o.next();
            fileName = actual.getName();
            File fichero_7 = new File(fileName);
            fichero_7= new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+this.nombreFile+this.extension );
            actual.write(fichero_7);
       break;
       case 8:
            System.out.println("switch8");
            actual = (FileItem)p.next();
            fileName = actual.getName();
            File fichero_8 = new File(fileName);
            fichero_8= new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+this.nombreFile+this.extension );
            actual.write(fichero_8);
       break;
       case 9:
            System.out.println("switch9");
            actual = (FileItem)q.next();
            fileName = actual.getName();
            File fichero_9 = new File(fileName);
            fichero_9= new  File( Constantes.pathDocPDF+ this.subCarpeta+"/"+this.nombreFile+this.extension );
            actual.write(fichero_9);
       break;
      }
            actual.delete();
            actual = null;
            return guardarArchivoDB();
    }
    catch(Exception e) {
        eliminarCandado();
        System.out.println("Error de Aplicaci�n UPC subirArchivo" + e.getMessage());
        return false;
    }
 }
  public boolean guardarArchivoDB(){
    try{
       if(cn.isEstado()){
           if(this.subCarpeta.equals("ayudaDx") ){
               sql.delete(0,sql.length());
			   sql.append("INSERT INTO ARCHIVOS.EVOLUCION_AYUDA_DX (ID,ID_EVOLUCION,ID_PACIENTE,  OBSERVACION, ID_SITIO,  USUARIO_CREA) VALUES (?::integer,?::integer,?::integer,?,?::integer,?);");
   			   sql.append("DELETE FROM  archivos.candado_dx;");
			   cn.prepareStatementIDU(Constantes.PS1,sql);
               cn.ps1.setString(1, this.nombreFile);
               cn.ps1.setString(2, this.var1);
               cn.ps1.setString(3, this.var2);
               cn.ps1.setString(4, this.var3);
               cn.ps1.setString(5, this.var4);
               cn.ps1.setString(6, this.usuario_crea);
               cn.iduSQL(Constantes.PS1);
               return true;
           }
           else if(this.subCarpeta.equals("documentoHC") ){
               sql.delete(0,sql.length());
			   sql.append("UPDATE   HC.EVOLUCION SET  ADJUNTO = 'S' WHERE ID = ?::integer");
               cn.prepareStatementIDU(Constantes.PS1,sql);
               cn.ps1.setString(1, this.nombreFile);  //idDocumento
               cn.iduSQL(Constantes.PS1);
           }
           else if(this.subCarpeta.equals("fisico") ){
               sql.delete(0,sql.length());
			   sql.append("INSERT INTO ARCHIVOS.HC_FISICA ( ID_PACIENTE,USUARIO_CREA) VALUES (?::integer,?);");
			   cn.prepareStatementIDU(Constantes.PS1,sql);
               cn.ps1.setString(1, this.var1);
			   cn.ps1.setString(2, this.usuario_crea);
               cn.iduSQL(Constantes.PS1);
           }
           else if(this.subCarpeta.equals("identificacion") ){
               sql.delete(0,sql.length());
			   sql.append("INSERT INTO ARCHIVOS.PACIENTE_DOCUMENTO ( ID_PACIENTE, TIPO_ID, IDENTIFICACION, USUARIO_CREA) VALUES (?::integer,?,?,?);");
			   cn.prepareStatementIDU(Constantes.PS1,sql);
               cn.ps1.setString(1, this.var1);
               cn.ps1.setString(2, this.var2);
               cn.ps1.setString(3, this.var3);
               cn.ps1.setString(4, this.usuario_crea);
               cn.iduSQL(Constantes.PS1);
           }
           else if(this.subCarpeta.equals("remision") ){
               sql.delete(0,sql.length());
			   sql.append("INSERT INTO ARCHIVOS.REMISION_ADMISION (ID_CITA, USUARIO_CREA) VALUES (?::integer,?)");
			   cn.prepareStatementIDU(Constantes.PS1,sql);
               cn.ps1.setString(1, this.var1);
               cn.ps1.setString(2, this.usuario_crea);
               cn.iduSQL(Constantes.PS1);
           }
           else if(this.subCarpeta.equals("carnet") ){
               sql.delete(0,sql.length());
               sql.append("INSERT INTO ARCHIVOS.CITA_CARNET(ID_CITA, ID_PACIENTE, USUARIO_CREA) VALUES (?::integer,?::integer,?)");
			   cn.prepareStatementIDU(Constantes.PS1,sql);
               cn.ps1.setString(1, this.var1);
               cn.ps1.setString(2, this.var2);
               cn.ps1.setString(3, this.usuario_crea);
               cn.iduSQL(Constantes.PS1);
           }
		   else if(this.subCarpeta.equals("otros") ){
               sql.delete(0,sql.length());
			   sql.append("INSERT INTO ARCHIVOS.OTROS ( ID, ID_PACIENTE,ID_TIPO,OBSERVACION,USUARIO_CREA) VALUES (?::integer,?::integer,?::integer,?,?);");
			   cn.prepareStatementIDU(Constantes.PS1,sql);
               cn.ps1.setInt(1, this.serial);
               cn.ps1.setString(2, this.var1);
               cn.ps1.setString(3, this.var2);
               cn.ps1.setString(4, this.var3);
			   cn.ps1.setString(5, this.usuario_crea);
               cn.iduSQL(Constantes.PS1);
           }
       }
    }
    catch(Exception e){
     System.out.println(e.getMessage()+" en m�todo guardarArchivoDB() de UPC.java");
     cn.exito=false;
    }
    return true;
  }

  public int maximoValorTablaOtros(){
      int valor =0;
      try{
		  if(cn.isEstado()){
			  sql2.delete(0,sql2.length());
			  sql2.append("SELECT  MAX(id)+1 id FROM ARCHIVOS.OTROS");
			  this.cn.prepareStatementSEL(Constantes.PS2,sql2);
			  if(cn.selectSQL(Constantes.PS2)){
				 while(cn.rs2.next()){
					  valor =  cn.rs2.getInt("id");
					  cn.cerrarPS(Constantes.PS2);
				 }
			   }
		  }
     }catch(Exception e){
          System.out.println("Error --> clase  -->UPC.java-- function maximoValorTablaOtros --> SQLException --> "+e.getMessage());
     }

      return  valor;
  }

    public Sgh.Utilidades.Conexion getCn() {
        return cn;
    }

    public void setCn(Sgh.Utilidades.Conexion value) {
        cn = value;
    }
    public java.lang.String getSubCarpeta() {
        return subCarpeta;
    }

    public void setSubCarpeta(java.lang.String value) {
        subCarpeta = value;
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response )
    throws ServletException, IOException {
        mensaje="";
        fileName="";
        FechaFileName ="";
        cn=new Conexion();

//         Constantes.pathDocPDF = "c:/tomcat/webapps/docPDF/";
         Constantes.pathDocPDF = "P:/";

//         Constantes.pathDocPDF = "/opt/tomcat8/webapps/docPDF/";


     try{
            this.archivoTamano = request.getParameter("archivoTamano");
            this.nombreFile = request.getParameter("nombreArchivo");
            this.evento =  request.getParameter("evento");
            this.subCarpeta = request.getParameter("subCarpeta");

            this.var1 = request.getParameter("var1");
            this.var2 = request.getParameter("var2");
            this.var3 = request.getParameter("var3");
            this.var4 = request.getParameter("var4");
            this.usuario_crea =request.getParameter("usuario_crea");

          //  System.out.println("Constantes.pathDocPDF= "+Constantes.pathDocPDF);

         //   System.out.println("archivoTamano="+this.archivoTamano  +" :: idDocumento "+ this.nombreFile   +" evento="+this.evento +" subCarpeta.="+this.subCarpeta+" var1="+this.var1);

            if(this.subCarpeta.equals("otros") && this.evento.equals("crear")){
               	  this.serial = maximoValorTablaOtros();
                  this.nombreFile =   String.valueOf(this.serial);
            }

            if(this.evento.equals("crear"))
              subirArchivo(request, response);
            else if(this.evento.equals("elimina"))
              processElimina(request, response);
            else if(this.evento.equals("limpia"))
              processLimpiaDeLaBaseDatos(request, response);

      } catch (Exception e) {
            System.out.println("********************************************************************** Hubo un error **********************************************************************");
       }
    }
}
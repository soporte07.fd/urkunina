package Clinica.Utilidades;

import Sgh.Utilidades.ConexionArchivo;
import Sgh.Utilidades.LoggableStatement;
import Sgh.Utilidades.Sesion;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @(#)Clase Conexion.java version 1.02 2015/12/09 Copyright(c) 2015 Firmas
 * Digital. JAS
 */
public class UPC2 extends HttpServlet {

    String mensaje = "";
    String fileName = "";
    String FechaFileName = "";
    String nombreFile = "";
    String nombreFileComprobar = "";
    String evento = "";
    String subCarpeta = "";
    String extension = ".pdf";
    String archivoTamano = "";
    String var1 = "", var2 = "", var3 = "", var4 = "";
    String usuario_crea = "";
    int serial = 0;
    public ConexionArchivo cn;
    public Sesion sesion;
    private StringBuffer sql = new StringBuffer();
    private StringBuffer sql2 = new StringBuffer();

    public UPC2() {
        // sesion= new Sesion();
        mensaje = "";
        FechaFileName = "";
        nombreFile = "";
        nombreFileComprobar = "";
        subCarpeta = "";
        extension = ".pdf";
        archivoTamano = "";
        evento = "";
        var1 = "";
        var2 = "";
        var3 = "";
        var4 = "";
        evento = "";
        usuario_crea = "";
        serial = 0;
        fileName = "";
        archivoTamano = "";
        nombreFileComprobar = "";
    }

    public boolean crearCandado() {
        try {
            if (!subCarpeta.equals("ayudaDx") || !subCarpeta.equals("ordenProgramacion")) {   //  System.out.println("CREAR CANDADOo");
                sql.delete(0, sql.length());
                sql.append("delete from archivos.candado where archivo = 'null.pdf'; \n");
                sql.append("delete from archivos.candado_dx where archivo = 'null.pdf'; \n");
                sql.append("INSERT INTO archivos.candado(id, usuario, archivo,  archivo_tamano, sub_carpeta  ) VALUES (1,?,?,?,?)");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.ps1.setString(1, this.usuario_crea);
                cn.ps1.setString(2, this.nombreFile + extension);
                cn.ps1.setString(3, this.archivoTamano);
                cn.ps1.setString(4, this.subCarpeta);
                cn.iduSQL(Constantes.PS1);
                return true;
            } else {
                sql.delete(0, sql.length());
                sql.append("delete from archivos.candado where archivo = 'null.pdf';\n");
                sql.append("delete from archivos.candado_dx where archivo = 'null.pdf';\n");
                sql.append("INSERT INTO archivos.candado_dx(id, usuario, archivo,  archivo_tamano, sub_carpeta  ) VALUES (1,?,?,?,?)");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.ps1.setString(1, this.usuario_crea);
                cn.ps1.setString(2, this.nombreFile + extension);
                cn.ps1.setString(3, this.archivoTamano);
                cn.ps1.setString(4, this.subCarpeta);
                cn.iduSQL(Constantes.PS1);
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminarCandado() {
        try {
            if (!this.subCarpeta.equals("ayudaDx") || !this.subCarpeta.equals("ordenProgramacion")) {     //  System.out.println("BORRANDO CANDADO");
                sql.delete(0, sql.length());
                sql.append("DELETE FROM  archivos.candado ");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.iduSQL(Constantes.PS1);
                return true;
            } else {                                   //  System.out.println("BORRANDO CANDADO dx");
                sql.delete(0, sql.length());
                sql.append("DELETE FROM  archivos.candado_dx");
                cn.prepareStatementIDU(Constantes.PS1, sql);
                cn.iduSQL(Constantes.PS1);
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    protected void subirArchivo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if (crearCandado()) {
            if (subirArchivo(request)) {
                mensaje = "111.ARCHIVO SE ADJUNTO CORRECTAMENTE.!!!";
                eliminarCandado();
                if (comprobarSiExisteEnBaseDatos()) {
                    if (comprobarSiExisteArchivoFisico()) {
                        cn.destroy();
                        eliminarCandado();
                        out.println("<SCRIPT language=javascript>  window.open('http://" + cn.getServidorIpPublica() + "/docPDF/" + this.subCarpeta + "/" + this.nombreFileComprobar + this.extension + "?tamano=" + this.archivoTamano + "' )</SCRIPT>");
                        cn = null;
                    } else {
                        cn.destroy();
                        eliminarCandado();
                        cn = null;
                        mensaje = "5. ALERTA AL SUBIR EL ARCHIVO, \n VERIFIQUE ANTES DE VOLVER A INTANTAR";
                        out.println("<SCRIPT language=javascript>alert('" + mensaje + "');</SCRIPT>");
                        out.close();
                    }
                }
            } else {
                if (mensaje.equals("")) {
                    cn.destroy();
                }
                eliminarCandado();
                cn = null;
                mensaje = "2.Hay una peticion en curso, por favor espere un momento e intente nuevamente!";
                out.println("<SCRIPT language=javascript>alert('" + mensaje + "');</SCRIPT>");
                out.close();
            }
        } else {
            cn.destroy();
            mensaje = "3.Alerta al enviar, Revise el archivo subido y si es el caso intente nuevamente !!!!!!!!!!!!";
            out.println("<SCRIPT language=javascript>alert('" + mensaje + "');</SCRIPT>");
            out.close();
        }

    }

    public boolean comprobarSiExisteArchivoFisico() {      // si existe el archivo entonces lo elimina para insertar en el caso de modificaciones

        File listaarchivos = new File(Constantes.pathDocPDF + this.subCarpeta + "/" + "/");
        String[] lista = listaarchivos.list();
        String archivo = "";
        try {
            for (int n = 0; n < lista.length; n++) { //busca en todos  los directorios (Usuarios)
                archivo = lista[n];
                if (archivo.equals(this.nombreFileComprobar + this.extension)) {
                    File fichero = new File(Constantes.pathDocPDF + this.subCarpeta + "/" + this.nombreFileComprobar + this.extension);
                    DecimalFormat df = new DecimalFormat("#");
                    float longitud = fichero.length();

                    System.out.println("El fichero ha sido subido satisfactoriamente tamano enviado=" + this.archivoTamano + " y tamano en servidor= " + df.format(longitud / 1024));                        //
                    if (this.archivoTamano.equals(df.format(longitud / 1024))) {
                        System.out.println("ESTE SI ES EL ARCHIVO");
                    } else {
                        System.out.println("ESTE NO ES EL ARCHIVO");
                        return false;
                    }

                    return true;
                }
            }
        } catch (Exception e) {
            System.out.println("fallo al comprobarSiExisteArchivoFisico");
            return false;
        }
        return true;
    }

    public boolean comprobarSiExisteEnBaseDatos() {
        boolean van = false;
        try {
            if (cn.isEstado()) {

                if (this.subCarpeta.equals("ordenProgramacion")) {

                    sql2.delete(0, sql2.length());
                    sql2.append("SELECT id FROM archivos.orden_programacion WHERE id::TEXT = " + nombreFile);
                    this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                    if (cn.selectSQL(Constantes.PS2)) {
                        while (cn.rs2.next()) {
                            this.nombreFileComprobar = nombreFile;
                            cn.cerrarPS(Constantes.PS2);
                            van = true;
                        }
                    }

                } else if (this.subCarpeta.equals("fisico")) {
                    sql2.delete(0, sql2.length());
                    sql2.append("SELECT id_paciente FROM archivos.hc_fisica WHERE id_paciente = " + this.var1);
                    this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                    if (cn.selectSQL(Constantes.PS2)) {
                        while (cn.rs2.next()) {
                            this.nombreFileComprobar = cn.rs2.getString("id_paciente");
                            //  System.out.println("/////fisico base de datos////="+ this.nombreFileComprobar);
                            cn.cerrarPS(Constantes.PS2);
                            van = true;
                        }
                    }
                } else if (this.subCarpeta.equals("identificacion")) {
                    sql2.delete(0, sql2.length());
                    sql2.append("SELECT  tipo_id||identificacion identificacion FROM archivos.paciente_documento where identificacion LIKE substring('" + this.nombreFile + "' from 3 for 22) ");
                    this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                    //  System.out.println("queryyyyyyyyyyyyyyy = "+((LoggableStatement)cn.ps2).getQueryString());
                    if (cn.selectSQL(Constantes.PS2)) {
                        while (cn.rs2.next()) {
                            this.nombreFileComprobar = cn.rs2.getString("identificacion");
                            //	  System.out.println("/////identificacion base de datos////="+ this.nombreFileComprobar);
                            cn.cerrarPS(Constantes.PS2);
                            van = true;
                        }
                    }
                } else if (this.subCarpeta.equals("remision")) {
                    sql2.delete(0, sql2.length());
                    sql2.append("SELECT id_cita FROM archivos.remision_admision where id_cita = " + this.var1);
                    this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                    System.out.println("remisioonnn = " + ((LoggableStatement) cn.ps2).getQueryString());
                    if (cn.selectSQL(Constantes.PS2)) {
                        while (cn.rs2.next()) {
                            this.nombreFileComprobar = cn.rs2.getString("id_cita");
                            //	  System.out.println("/////remision base de datos////="+ this.nombreFileComprobar);
                            cn.cerrarPS(Constantes.PS2);
                            van = true;
                        }
                    }
                } else if (this.subCarpeta.equals("carnet")) {

                    sql2.delete(0, sql2.length());
                    sql2.append("SELECT id_cita FROM archivos.cita_carnet where id_cita = " + this.var1);
                    this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                    System.out.println("carnet = " + ((LoggableStatement) cn.ps2).getQueryString());
                    if (cn.selectSQL(Constantes.PS2)) {
                        while (cn.rs2.next()) {
                            this.nombreFileComprobar = cn.rs2.getString("id_cita");
                            //	  System.out.println("/////carnet base de datos////="+ this.nombreFileComprobar);
                            cn.cerrarPS(Constantes.PS2);
                            van = true;
                        }
                    }
                } else if (this.subCarpeta.equals("ayudaDx")) {

                    sql2.delete(0, sql2.length());
                    sql2.append("SELECT id FROM archivos.evolucion_ayuda_dx where id = " + this.nombreFile);
                    this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                    System.out.println("ayudaDx = " + ((LoggableStatement) cn.ps2).getQueryString());
                    if (cn.selectSQL(Constantes.PS2)) {
                        while (cn.rs2.next()) {
                            this.nombreFileComprobar = cn.rs2.getString("id");
                            //  System.out.println("/////evolucion_ayuda_dx base de datos////="+ this.nombreFileComprobar);
                            cn.cerrarPS(Constantes.PS2);
                            van = true;
                        }
                    }
                } else if (this.subCarpeta.equals("otros")) {

                    sql2.delete(0, sql2.length());
                    sql2.append("SELECT id FROM archivos.otros where id = " + this.nombreFile);
                    this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                    //  System.out.println("otros = "+((LoggableStatement)cn.ps2).getQueryString());
                    if (cn.selectSQL(Constantes.PS2)) {
                        while (cn.rs2.next()) {
                            this.nombreFileComprobar = cn.rs2.getString("id");
                            //  System.out.println("/////otros base de datos////="+ this.nombreFileComprobar);
                            cn.cerrarPS(Constantes.PS2);
                            van = true;
                        }
                    }
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en motodo comprobarSiExisteEnBaseDatos() de UPC2.java");
        }
        return van;
    }

    protected void processLimpiaDeLaBaseDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if (quitarArchivoDB()) {
            mensaje = "ARCHIVO SE ELIMINO CORRECTAMENTE DE LA BASE DE DATOS";
        } else if (mensaje.equals("")) {
            mensaje = "Error al eliminar";
        }
        out.println("<html>");
        out.println("<head>");
        out.println("<SCRIPT language=javascript>alert('" + mensaje + "');</SCRIPT>");
        out.println("</head>");
        out.println("<body>");
        out.println("</body>");
        out.println("</html>");
        out.close();
    }

    protected void processElimina(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if (eliminaArchivoFisico()) {
            mensaje = "ARCHIVO SE ELIMINO CORRECTAMENTE.";
        } else if (mensaje.equals("")) {
            mensaje = "Error al eliminar";
        }
        out.println("<html>");
        out.println("<head>");
        out.println("<SCRIPT language=javascript>alert('" + mensaje + "');</SCRIPT>");
        out.println("</head>");
        out.println("<body>");
        out.println("</body>");
        out.println("</html>");
        out.close();
    }

    public boolean eliminaArchivoFisico() {   // si existe el archivo entonces lo elimina para insertar en el caso de modificaciones
        System.out.println("******** 1");
        File listaarchivos = new File(Constantes.pathDocPDF + "/" + this.subCarpeta + "/" + "/");
        String[] lista = listaarchivos.list();
        String archivo = "";

        try {
            for (int n = 0; n < lista.length; n++) { //busca en todos  los directorios (Usuarios)
                archivo = lista[n];

                if (archivo.equals(this.nombreFile + this.extension)) {
                    File fichero = new File(Constantes.pathDocPDF + "/" + this.subCarpeta + "/" + archivo);

                    if (fichero.delete()) {
                        System.out.println("El fichero ha sido borrado satisfactoriamente 2");
                        quitarArchivoDB();
                    } else {
                        System.out.println("El fichero NOO ha sido borrado ");
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean quitarArchivoDB() {
        try {
            if (cn.isEstado()) {

                if (this.subCarpeta.equals("documentoHC")) {
                    sql.delete(0, sql.length());
                    sql.append("UPDATE  HC.EVOLUCION SET  ADJUNTO = 'N' WHERE ID = ?::integer");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.nombreFile);  //idDocumento
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("identificacion")) {
                    sql.delete(0, sql.length());
                    sql.append("DELETE FROM ARCHIVOS.PACIENTE_DOCUMENTO WHERE ID =?::integer");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.var1);
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("fisico")) {
                    sql.delete(0, sql.length());
                    sql.append("DELETE FROM ARCHIVOS.HC_FISICA WHERE ID =?::integer");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.var1);
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("remision")) {
                    sql.delete(0, sql.length());
                    sql.append("DELETE FROM ARCHIVOS.REMISION_ADMISION WHERE ID_CITA = ?::integer");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.nombreFile);
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("carnet")) {
                    sql.delete(0, sql.length());
                    sql.append("DELETE FROM ARCHIVOS.CITA_CARNET WHERE ID_CITA = ?::integer");

                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.nombreFile);
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("ayudaDx")) {
                    sql.delete(0, sql.length());
                    sql.append("DELETE FROM ARCHIVOS.EVOLUCION_AYUDA_DX WHERE id = ?::integer");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.nombreFile);
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("otros")) {
                    sql.delete(0, sql.length());
                    sql.append("DELETE FROM ARCHIVOS.OTROS WHERE ID =?::integer");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.var1);
                    cn.iduSQL(Constantes.PS1);
                }else if (this.subCarpeta.equals("ordenProgramacion")) {
                    sql.delete(0, sql.length());
                    sql.append("DELETE FROM archivos.orden_programacion WHERE ID =?::integer");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, nombreFile);
                    cn.iduSQL(Constantes.PS1);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en metodo quitarArchivoDB() de UPC2.java");
        }
        return cn.exito;
    }

    public boolean existeArchivo() {      // si existe el archivo entonces lo elimina para insertar en el caso de modificaciones

        File listaarchivos = new File(Constantes.pathDocPDF + "/" + this.subCarpeta + "/" + "/");
        String[] lista = listaarchivos.list();
        String archivo = "";

        try {
            for (int n = 0; n < lista.length; n++) { //busca en todos  los directorios (Usuarios)
                archivo = lista[n];
                if (archivo.equals(this.nombreFile)) {
                    File fichero = new File(this.nombreFile);
                    //  eliminarArchivoUpc();
                    if (fichero.delete()) {
                        System.out.println("El fichero ha sido borrado satisfactoriamentex");
                    } else {
                        System.out.println("El fichero NOO ha sido borrado satisfactoriamentex");
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean subirArchivo(HttpServletRequest req) {
        System.out.println("subirArchivo");
        Archivo archivo = new Archivo();
        if (archivo.subirArchivo(req, Constantes.pathDocPDF, this.subCarpeta, this.nombreFile, this.extension)) {
            archivo = null;
            guardarArchivoDB();
            return true;
        } else {
            archivo = null;
            eliminarCandado();
            return false;
        }

    }

    public boolean guardarArchivoDB() {
        try {
            if (cn.isEstado()) {
                if (this.subCarpeta.equals("ayudaDx")) {
                    sql.delete(0, sql.length());
                    sql.append("INSERT INTO ARCHIVOS.EVOLUCION_AYUDA_DX (ID,ID_EVOLUCION,ID_PACIENTE,  OBSERVACION, ID_SITIO,  USUARIO_CREA) VALUES (?::integer,?::integer,?::integer,?,?::integer,?);");
                    sql.append("DELETE FROM  archivos.candado_dx;");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.nombreFile);
                    cn.ps1.setString(2, this.var1);
                    cn.ps1.setString(3, this.var2);
                    cn.ps1.setString(4, this.var3);
                    cn.ps1.setString(5, this.var4);
                    cn.ps1.setString(6, this.usuario_crea);
                    cn.iduSQL(Constantes.PS1);
                    return true;
                } else if (this.subCarpeta.equals("ordenProgramacion")) {
                    sql.delete(0, sql.length());
                    sql.append("INSERT INTO archivos.orden_programacion(id,id_paciente,observacion,usuario_crea)VALUES (?::INTEGER,?::INTEGER,?,?);");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.nombreFile);
                    cn.ps1.setString(2, this.var1);
                    cn.ps1.setString(3, this.var2);
                    cn.ps1.setString(4, this.usuario_crea);
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("documentoHC")) {
                    sql.delete(0, sql.length());
                    sql.append("UPDATE   HC.EVOLUCION SET  ADJUNTO = 'S' WHERE ID = ?::integer");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.nombreFile);  //idDocumento
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("fisico")) {
                    sql.delete(0, sql.length());
                    sql.append("INSERT INTO ARCHIVOS.HC_FISICA ( ID_PACIENTE,USUARIO_CREA) VALUES (?::integer,?);");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.var1);
                    cn.ps1.setString(2, this.usuario_crea);
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("identificacion")) {
                    sql.delete(0, sql.length());
                    sql.append("INSERT INTO ARCHIVOS.PACIENTE_DOCUMENTO ( ID_PACIENTE, TIPO_ID, IDENTIFICACION, USUARIO_CREA) VALUES (?::integer,?,?,?);");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.var1);
                    cn.ps1.setString(2, this.var2);
                    cn.ps1.setString(3, this.var3);
                    cn.ps1.setString(4, this.usuario_crea);
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("remision")) {
                    sql.delete(0, sql.length());
                    sql.append("INSERT INTO ARCHIVOS.REMISION_ADMISION (ID_CITA, USUARIO_CREA) VALUES (?::integer,?)");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.var1);
                    cn.ps1.setString(2, this.usuario_crea);
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("carnet")) {
                    sql.delete(0, sql.length());
                    sql.append("INSERT INTO ARCHIVOS.CITA_CARNET(ID_CITA, ID_PACIENTE, USUARIO_CREA) VALUES (?::integer,?::integer,?)");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setString(1, this.var1);
                    cn.ps1.setString(2, this.var2);
                    cn.ps1.setString(3, this.usuario_crea);
                    cn.iduSQL(Constantes.PS1);
                } else if (this.subCarpeta.equals("otros")) {
                    sql.delete(0, sql.length());
                    sql.append("INSERT INTO ARCHIVOS.OTROS ( ID, ID_PACIENTE,ID_TIPO,OBSERVACION,USUARIO_CREA) VALUES (?::integer,?::integer,?::integer,?,?);");
                    cn.prepareStatementIDU(Constantes.PS1, sql);
                    cn.ps1.setInt(1, this.serial);
                    cn.ps1.setString(2, this.var1);
                    cn.ps1.setString(3, this.var2);
                    cn.ps1.setString(4, this.var3);
                    cn.ps1.setString(5, this.usuario_crea);
                    cn.iduSQL(Constantes.PS1);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en metodo guardarArchivoDB() de UPC2.java");
            cn.exito = false;
        }
        return true;
    }

    public int maximoValorTablaOtros() {
        int valor = 0;
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("SELECT  MAX(id)+1 id FROM ARCHIVOS.OTROS");
                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        valor = cn.rs2.getInt("id");
                        cn.cerrarPS(Constantes.PS2);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error --> clase  -->UPC2.java-- function maximoValorTablaOtros --> SQLException --> " + e.getMessage());
        }

        return valor;
    }

    public Sgh.Utilidades.ConexionArchivo getCn() {
        return cn;
    }

    public void setCn(Sgh.Utilidades.ConexionArchivo value) {
        System.out.println("CN=" + cn);
        cn = value;
    }

    public java.lang.String getSubCarpeta() {
        return subCarpeta;
    }

    public void setSubCarpeta(java.lang.String value) {
        subCarpeta = value;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        mensaje = "";
        FechaFileName = "";
        nombreFile = "";
        nombreFileComprobar = "";
        subCarpeta = "";
        extension = ".pdf";
        archivoTamano = "";
        evento = "";
        var1 = "";
        var2 = "";
        var3 = "";
        var4 = "";
        evento = "";
        usuario_crea = "";
        serial = 0;
        fileName = "";
        archivoTamano = "";
        nombreFileComprobar = "";

        System.out.println("---------Ingreso a UPC2-------------");

        Connection conect = (Connection) this.getServletConfig().getServletContext().getAttribute("userCn");

        if (conect != null) {
            System.out.println("Llego Parametro.");
            cn = new ConexionArchivo(conect);
        } else {
            System.out.println("Error no llego Parametro.");
            cn = new ConexionArchivo();
        }

        Constantes.pathDocPDF = "/opt/tomcat8/webapps/docPDF/";

        try {
            this.archivoTamano = request.getParameter("archivoTamano");
            this.nombreFile = request.getParameter("nombreArchivo");
            this.evento = request.getParameter("evento");
            this.subCarpeta = request.getParameter("subCarpeta");

            this.var1 = request.getParameter("var1");
            this.var2 = request.getParameter("var2");
            this.var3 = request.getParameter("var3");
            this.var4 = request.getParameter("var4");
            this.usuario_crea = request.getParameter("usuario_crea");

            if (this.subCarpeta.equals("otros") && this.evento.equals("crear")) {
                this.serial = maximoValorTablaOtros();
                this.nombreFile = String.valueOf(this.serial);
            }

              if(evento.equals("crear")){
                  subirArchivo(request, response);
              }else if(evento.equals("elimina")){
                  processElimina(request, response);
              }else if(evento.equals("limpia")){
                   processLimpiaDeLaBaseDatos(request, response);
              }
            
            
        } catch (IOException | ServletException e) {
            System.out.println("Hubo un error UPC2: " + e.getMessage());
        }
    }
}

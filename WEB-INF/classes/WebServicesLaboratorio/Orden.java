package WebServicesLaboratorio;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "orden")
@XmlAccessorType(XmlAccessType.FIELD)
public class Orden {

    @XmlElement(name = "datosGenerales")
    private OrdenDatoGeneral datoGeneral;

    @XmlElement(name = "paciente")
    private PacienteLaboratorio paciente;

    @XmlElement(name = "examen")
    private final List<Examen> listaExamenes;

    public Orden() {
        listaExamenes = new ArrayList<>();
    }

    public PacienteLaboratorio getPaciente() {
        return paciente;
    }

    public void setPaciente(PacienteLaboratorio paciente) {
        this.paciente = paciente;
    }

    public OrdenDatoGeneral getDatoGeneral() {
        return datoGeneral;
    }

    public void setDatoGeneral(OrdenDatoGeneral datoGeneral) {
        this.datoGeneral = datoGeneral;
    }

    public List<Examen> getListaExamenes() {
        return listaExamenes;
    }

    public void agregarExamen(Examen examen) {
        listaExamenes.add(examen);
    }

    @Override
    public String toString() {
        return "Orden{" + "datoGeneral=" + datoGeneral + ", paciente=" + paciente + ", listaExamenes=" + listaExamenes + '}';
    }

}

package WebServicesLaboratorio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "analito")
@XmlAccessorType(XmlAccessType.FIELD)
public class Analito {

    @XmlElement(name = "codigo", required = true)
    private String codigo;

    @XmlElement(name = "nombre", required = true)
    private String nombre;

    @XmlElement(name = "resultado", required = true)
    private String resultado;

    @XmlElement(name = "valorMinimo", required = true)
    private String ValorMinimo;

    @XmlElement(name = "valorMaximo", required = true)
    private String ValorMaximo;

    @XmlElement(name = "unidades", required = true)
    private String unidades;

    @XmlElement(name = "valorReferencia", required = true)
    private String valorReferencia;

    private String orden;

    public Analito() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getValorMinimo() {
        return ValorMinimo;
    }

    public void setValorMinimo(String ValorMinimo) {
        this.ValorMinimo = ValorMinimo;
    }

    public String getValorMaximo() {
        return ValorMaximo;
    }

    public void setValorMaximo(String ValorMaximo) {
        this.ValorMaximo = ValorMaximo;
    }

    public String getUnidades() {
        return unidades;
    }

    public void setUnidades(String unidades) {
        this.unidades = unidades;
    }

    public String getValorReferencia() {
        return valorReferencia;
    }

    public void setValorReferencia(String valorReferencia) {
        this.valorReferencia = valorReferencia;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    @Override
    public String toString() {
        return "Analito{" + "codigo=" + codigo + ", nombre=" + nombre + ", resultado=" + resultado + ", ValorMinimo=" + ValorMinimo + ", ValorMaximo=" + ValorMaximo + ", unidades=" + unidades + ", valorReferencia=" + valorReferencia + '}';
    }

}

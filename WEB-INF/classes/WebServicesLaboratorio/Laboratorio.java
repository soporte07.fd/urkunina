package WebServicesLaboratorio;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "laboratorio")
@XmlAccessorType(XmlAccessType.FIELD)
public class Laboratorio {

    @XmlElement(name = "numeroOrden", required = true)
    private String numeroOrden;

    @XmlElement(name = "tipoDocumento", required = true)
    private String tipoDocumento;

    @XmlElement(name = "documento", required = true)
    private String Documento;

    @XmlElement(name = "cups", required = true)
    private String cups;

    @XmlElement(name = "fechaResultado", required = true)
    private String fecha;

    @XmlElement(name = "documentoValida", required = true)
    private String documentoValida;

    @XmlElement(name = "resultado4505", required = true)
    private String resultado4505;

    @XmlElement(name = "tecnica", required = true)
    private String tecnica;

    @XmlElement(name = "analito", required = true)
    private final List<Analito> listaAnalitos;

    public Laboratorio() {
        listaAnalitos = new ArrayList<>();
    }

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDocumento() {
        return Documento;
    }

    public void setDocumento(String Documento) {
        this.Documento = Documento;
    }

    public String getCups() {
        return cups;
    }

    public void setCups(String cups) {
        this.cups = cups;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public List<Analito> getListaAnalitos() {
        return listaAnalitos;
    }

    public void agregarAnalito(Analito analito) {
        listaAnalitos.add(analito);
    }

    public String getDocumentoValida() {
        return documentoValida;
    }

    public void setDocumentoValida(String documentoValida) {
        this.documentoValida = documentoValida;
    }

    public String getResultado4505() {
        return resultado4505;
    }

    public void setResultado4505(String resultado4505) {
        this.resultado4505 = resultado4505;
    }

    public String getTecnica() {
        return tecnica;
    }

    public void setTecnica(String tecnica) {
        this.tecnica = tecnica;
    }

    @Override
    public String toString() {
        return "Laboratorio{" + "numeroOrden=" + numeroOrden + ", tipoDocumento=" + tipoDocumento + ", Documento=" + Documento + ", cups=" + cups + ", fecha=" + fecha + ", documentoValida=" + documentoValida + ", resultado4505=" + resultado4505 + ", tecnica=" + tecnica + ", listaAnalitos=" + listaAnalitos + '}';
    }

}

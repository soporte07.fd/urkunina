package WebServicesLaboratorio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ValidarCamposWebService {

    final private String ERROR1 = "Falta número de la orden.\n";
    final private String ERROR2 = "Falta tipo de documento del paciente.\n";
    final private String ERROR3 = "Falta documento o identificación del paciente.\n";
    final private String ERROR4 = "Falta cups de la orden.\n";
    final private String ERROR5 = "Falta la fecha del resultado.\n";
    final private String ERROR6 = "Falta el documento o identificación del profesional que Valida los resultados.\n";
    final private String ERROR7 = "Falta resultado 4505.\n";
    final private String ERROR8 = "Falta técnica.\n";
    final private String ERROR9 = "Faltan los analitos.\n";
    final private String ERROR10 = "Falta código del analito #";
    final private String ERROR11 = "Falta nombre del analito #";
    final private String ERROR12 = "Falta resultado del analito #";
    final private String ERROR13 = "Falta valorMinimo del analito #";
    final private String ERROR14 = "Falta valorMaximo del analito #";
    final private String ERROR15 = "Falta unidades del analito #";
    final private String ERROR16 = "Falta valorReferencia del analito #";
    final private String ERROR17 = "Falta el usuario.\n";
    final private String ERROR18 = "Falta la contraseña.\n";
    final private String ERROR19 = "Falta el token.\n";
    final private String ERROR20 = "El usuario no puede estar vacío.\n";
    final private String ERROR21 = "La contraseña no puede estar vacía.\n";
    final private String ERROR22 = "El token no puede estar vacío.\n";
    final public String ERROR23 = "Credenciales o token invalido.\n";
    final public String ERROR24 = "Error en el sistema Cristal, comuníquese con el administrador.\n";
    final private String ERROR25 = "Se envió fechaHasta pero no fechaDesde.\n";
    final private String ERROR26 = "Se envió fechaDesde pero no fechaHasta.\n";
    final private String ERROR27 = "Se envió fechaHasta pero fechaDesde esta vacío.\n";
    final private String ERROR28 = "Se envió fechaDesde pero fechaHasta esta vacío.\n";
    final private String ERROR29 = "Formato de fechaDesde erroneo, el formato debe ser dd/MM/yyyy.\n";
    final private String ERROR30 = "Formato de fechaHasta erroneo, el formato debe ser dd/MM/yyyy.\n";
    final private String ERROR31 = "fechaDesde no puede ser mayor que fechaHasta.\n";
    final private String ERROR32 = "No se envió ninguna orden.\n";
    final private String ERROR33 = "Faltan el número de la orden #";
    final private String ERROR34 = "No puede estar vacío el número de la orden #";
    final private String ERROR35 = "Formato erróneo, solo se admite números en el número de la orden #";
    final private String ERROR36 = "El numero debe ser mayor a cero en la orden #";
    final public String ERROR37 = "No se editó ninguna orden, por favor revise los números.\n";
    final private String ERROR38 = "Falta el resultado.\n";
    final private String ERROR39 = "El número de la orden no puede estar vacía.\n";
    final private String ERROR40 = "El tipoDocumento no puede estar vacío.\n";
    final private String ERROR41 = "El documento no puede estar vacío.\n";
    final private String ERROR42 = "El cups no puede estar vacío.\n";
    final private String ERROR43 = "La fechaResultado de no puede estar vacía.\n";
    final private String ERROR44 = "El documentoValida no puede estar vacío.\n";
    final private String ERROR45 = "No puede estar vacío código del analito #";
    final private String ERROR46 = "Formato de fechaResultado erróneo, el formato debe ser dd/MM/yyyy HH:mm:ss.\n";
    final private String ERROR47 = "Formato erróneo, solo se admite números en el número de la orden.\n";
    final private String ERROR48 = "El numero debe ser mayor a cero en la orden.\n";
    String error;

    public ValidarCamposWebService() {
    }

    /**
     * Valida los que los campos del resultados no sean nulos, que algunos no
     * esten vacios y el tipo de dato
     *
     * @param laboratorio
     * @return error
     */
    public String ValidarResultados(Laboratorio laboratorio) {
        error = "";

        //Valida si es resultado es null
        if (laboratorio == null) {
            error = ERROR38;
            return error;
        }

        //Si su atributos del resultado son null (operador ternario)
        error += (laboratorio.getNumeroOrden() == null) ? ERROR1 : "";
        error += (laboratorio.getTipoDocumento() == null) ? ERROR2 : "";
        error += (laboratorio.getDocumento() == null) ? ERROR3 : "";
        error += (laboratorio.getCups() == null) ? ERROR4 : "";
        error += (laboratorio.getFecha() == null) ? ERROR5 : "";
        error += (laboratorio.getDocumentoValida() == null) ? ERROR6 : "";
        error += (laboratorio.getResultado4505() == null) ? ERROR7 : "";
        error += (laboratorio.getTecnica() == null) ? ERROR8 : "";
        error += (laboratorio.getListaAnalitos().isEmpty()) ? ERROR9 : "";

        //Si sus analitos y sus atributos son null
        for (int i = 0; i < laboratorio.getListaAnalitos().size(); i++) {
            Analito analito = laboratorio.getListaAnalitos().get(i);

            error += (analito.getCodigo() == null) ? ERROR10 + (i + 1) + ".\n" : "";
            error += (analito.getNombre() == null) ? ERROR11 + (i + 1) + ".\n" : "";
            error += (analito.getResultado() == null) ? ERROR12 + (i + 1) + ".\n" : "";
            error += (analito.getValorMinimo() == null) ? ERROR13 + (i + 1) + ".\n" : "";
            error += (analito.getValorMaximo() == null) ? ERROR14 + (i + 1) + ".\n" : "";
            error += (analito.getUnidades() == null) ? ERROR15 + (i + 1) + ".\n" : "";
            error += (analito.getValorReferencia() == null) ? ERROR16 + (i + 1) + ".\n" : "";

        }

        //Si todos los elementos no hay null ingresa al if
        if (error.isEmpty()) {

            //Si los atributos del resultado son vacios
            error += (laboratorio.getNumeroOrden().isEmpty()) ? ERROR39 : "";
            error += (laboratorio.getTipoDocumento().isEmpty()) ? ERROR40 : "";
            error += (laboratorio.getDocumento().isEmpty()) ? ERROR41 : "";
            error += (laboratorio.getCups().isEmpty()) ? ERROR42 : "";
            error += (laboratorio.getFecha().isEmpty()) ? ERROR43 : "";
            error += (laboratorio.getDocumentoValida().isEmpty()) ? ERROR44 : "";

            //Si la fecha no es vacia, valida su formato de fecha
            if (!laboratorio.getFecha().isEmpty()) {
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                format.setLenient(false);

                try {
                    Date date = format.parse(laboratorio.getFecha());
                } catch (ParseException e) {
                    error += ERROR46;
                }

            }

            //Si el numero de orden no es vacio, valida si es numero y mayor a cero
            if (!laboratorio.getNumeroOrden().isEmpty()) {
                try {
                    int n = Integer.parseInt(laboratorio.getNumeroOrden());
                    if (n <= 0) {
                        error += ERROR48;
                    }
                } catch (NumberFormatException e) {
                    error += ERROR47;
                }
            }

            //Si sus analitos y sus codigo esta vacio
            for (int i = 0; i < laboratorio.getListaAnalitos().size(); i++) {
                Analito analito = laboratorio.getListaAnalitos().get(i);
                error += (analito.getCodigo().isEmpty()) ? ERROR45 + (i + 1) + ".\n" : "";
            }

        }

        return error;
    }

    /**
     * Valida que los datos del usuario no sean null ni vacios
     *
     * @param listaUsuario
     * @param listaContrasena
     * @param listaToken
     * @return error
     */
    public String validarUsuario(List listaUsuario, List listaContrasena, List listaToken) {

        error = "";

        //Valida el usuario
        if (listaUsuario != null) {
            String usuario = listaUsuario.get(0).toString().trim();
            error += (usuario.isEmpty()) ? ERROR20 : "";
        } else {
            error += ERROR17;
        }

        //Valida la contraseña
        if (listaContrasena != null) {
            String contrasena = listaContrasena.get(0).toString().trim();
            error += (contrasena.isEmpty()) ? ERROR21 : "";
        } else {
            error += ERROR18;
        }

        //Valida el token
        if (listaToken != null) {
            String token = listaToken.get(0).toString().trim();
            error += (token.isEmpty()) ? ERROR22 : "";
        } else {
            error += ERROR19;
        }

        return error;

    }

    /**
     * Valida que las fechas no sean null, vacias y su formato
     *
     * @param fechaDesde
     * @param fechaHasta
     * @return error
     */
    public String validarFechas(String fechaDesde, String fechaHasta) {
        error = "";

        //Valida si alguna de las dos fechas es null
        if (fechaDesde == null && fechaHasta != null) {
            error += ERROR25;
        }

        if (fechaDesde != null && fechaHasta == null) {
            error += ERROR26;
        }

        //Si ninguna es null ingresa al if
        if (fechaDesde != null && fechaHasta != null) {

            //Valida si alguna de las dos fechas es null
            if (fechaDesde.isEmpty() && !fechaHasta.isEmpty()) {
                error += ERROR27;
            }

            if (!fechaDesde.isEmpty() && fechaHasta.isEmpty()) {
                error += ERROR28;
            }

            //Si ninguna es vacia ingresa al if
            if (!fechaDesde.isEmpty() && !fechaHasta.isEmpty()) {

                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                format.setLenient(false);

                Date dateDesde = null, dateHasta = null;

                //Se valida el formato de las fechas
                try {
                    dateDesde = format.parse(fechaDesde);
                } catch (ParseException e) {
                    error += ERROR29;
                }

                try {
                    dateHasta = format.parse(fechaHasta);
                } catch (ParseException e) {
                    error += ERROR30;
                }

                //Se valida que la fechaDesde no sea mayor a la fechaHasta
                if (dateDesde != null && dateHasta != null) {
                    if (dateDesde.compareTo(dateHasta) > 0) {
                        error += ERROR31;
                    }
                }

            }
        }

        return error;
    }

    /**
     * Valida que las ordenes no sean null, vacias y su formato
     *
     * @param listaOrdenes
     * @return error
     */
    public String validarOrdenes(List<OrdenTomada> listaOrdenes) {

        error = "";

        //Valida que la lista no este vacia
        error += (listaOrdenes.isEmpty()) ? ERROR32 : "";

        //Valida cada orden de la lista
        for (int i = 0; i < listaOrdenes.size(); i++) {
            OrdenTomada orden = listaOrdenes.get(i);

            //Valida que el numero de la orden no sea null ni vacio
            if (orden.getNumero() == null) {
                error += ERROR33 + (i + 1) + ".\n";
            } else if (orden.getNumero().isEmpty()) {
                error += ERROR34 + (i + 1) + ".\n";
            } else {

                //Valida que el numero sea un entero
                try {
                    int n = Integer.parseInt(orden.getNumero());
                    if (n <= 0) {
                        error += ERROR36 + (i + 1) + ".\n";
                    }

                } catch (NumberFormatException e) {
                    error += ERROR35 + (i + 1) + ".\n";
                }

            }

        }

        return error;
    }
}

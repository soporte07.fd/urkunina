
package WebServicesLaboratorio.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "consultaOrdenesResponse", namespace = "http://cristalweb.emssanar.org.co/clinica/WebServiceLaboratorio")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultaOrdenesResponse", namespace = "http://cristalweb.emssanar.org.co/clinica/WebServiceLaboratorio")
public class ConsultaOrdenesResponse {

    @XmlElement(name = "listaOrdenes", namespace = "")
    private WebServicesLaboratorio.Respuesta listaOrdenes;

    /**
     * 
     * @return
     *     returns Respuesta
     */
    public WebServicesLaboratorio.Respuesta getListaOrdenes() {
        return this.listaOrdenes;
    }

    /**
     * 
     * @param listaOrdenes
     *     the value for the listaOrdenes property
     */
    public void setListaOrdenes(WebServicesLaboratorio.Respuesta listaOrdenes) {
        this.listaOrdenes = listaOrdenes;
    }

}

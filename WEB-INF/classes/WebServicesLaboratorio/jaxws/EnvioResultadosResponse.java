
package WebServicesLaboratorio.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "envioResultadosResponse", namespace = "http://cristalweb.emssanar.org.co/clinica/WebServiceLaboratorio")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "envioResultadosResponse", namespace = "http://cristalweb.emssanar.org.co/clinica/WebServiceLaboratorio")
public class EnvioResultadosResponse {

    @XmlElement(name = "respuesta", namespace = "")
    private WebServicesLaboratorio.Respuesta respuesta;

    /**
     * 
     * @return
     *     returns Respuesta
     */
    public WebServicesLaboratorio.Respuesta getRespuesta() {
        return this.respuesta;
    }

    /**
     * 
     * @param respuesta
     *     the value for the respuesta property
     */
    public void setRespuesta(WebServicesLaboratorio.Respuesta respuesta) {
        this.respuesta = respuesta;
    }

}

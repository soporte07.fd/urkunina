
package WebServicesLaboratorio.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "envioResultados", namespace = "http://cristalweb.emssanar.org.co/clinica/WebServiceLaboratorio")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "envioResultados", namespace = "http://cristalweb.emssanar.org.co/clinica/WebServiceLaboratorio")
public class EnvioResultados {

    @XmlElement(name = "resultado", namespace = "", required = true)
    private WebServicesLaboratorio.Laboratorio resultado;

    /**
     * 
     * @return
     *     returns Laboratorio
     */
    public WebServicesLaboratorio.Laboratorio getResultado() {
        return this.resultado;
    }

    /**
     * 
     * @param resultado
     *     the value for the resultado property
     */
    public void setResultado(WebServicesLaboratorio.Laboratorio resultado) {
        this.resultado = resultado;
    }

}

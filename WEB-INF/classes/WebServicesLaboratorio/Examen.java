package WebServicesLaboratorio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "examen")
@XmlAccessorType(XmlAccessType.FIELD)
public class Examen {

    @XmlElement(name = "numeroOrden")
    private String numeroOrden;

    @XmlElement(name = "cups")
    private String cups;

    @XmlElement(name = "nombre")
    private String nombre;

    @XmlElement(name = "codigoDiagnostico")
    private String codigoDiagnostico;

    @XmlElement(name = "diagnostico")
    private String diagnostico;

    @XmlElement(name = "numeroAutorizacion")
    private String numeroAutorizacion;

    @XmlElement(name = "codigoNecesidad")
    private String codigoNecesidad;

    public Examen() {
    }

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public String getCups() {
        return cups;
    }

    public void setCups(String cups) {
        this.cups = cups;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigoDiagnostico() {
        return codigoDiagnostico;
    }

    public void setCodigoDiagnostico(String codigoDiagnostico) {
        this.codigoDiagnostico = codigoDiagnostico;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public String getCodigoNecesidad() {
        return codigoNecesidad;
    }

    public void setCodigoNecesidad(String codigoNecesidad) {
        this.codigoNecesidad = codigoNecesidad;
    }

    @Override
    public String toString() {
        return "Examen{" + "numeroOrden=" + numeroOrden + ", cups=" + cups + ", nombre=" + nombre + ", codigoDiagnostico=" + codigoDiagnostico + ", diagnostico=" + diagnostico + ", numeroAutorizacion=" + numeroAutorizacion + ", codigoNecesidad=" + codigoNecesidad + '}';
    }

}

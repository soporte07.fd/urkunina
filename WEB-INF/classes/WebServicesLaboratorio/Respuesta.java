package WebServicesLaboratorio;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Respuesta {

    @XmlElement(name = "error")
    private String error;

    @XmlElement(name = "exito")
    private String exito;

    @XmlElement(name = "orden")
    private List<Orden> listaOrdenes;

    public Respuesta() {
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        if (error.equals("")) {
            this.error = null;
        } else {
            this.error = error;

        }
    }

    public String getExito() {
        return exito;
    }

    public void setExito(String exito) {
        this.exito = exito;
    }

    public List<Orden> getListaOrdenes() {
        return listaOrdenes;
    }

    public void setListaOrdenes(List<Orden> listaOrdenes) {
        this.listaOrdenes = listaOrdenes;
    }

}

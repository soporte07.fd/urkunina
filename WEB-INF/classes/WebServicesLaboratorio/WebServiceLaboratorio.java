package WebServicesLaboratorio;

import Sgh.Utilidades.Des;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

@WebService(serviceName = "WebServiceLaboratorio",
        name = "WebServiceLaboratorio",
        portName = "WebServiceLaboratorio",
        targetNamespace = "http://cristalweb.emssanar.org.co/clinica/WebServiceLaboratorio")
public class WebServiceLaboratorio {

    @Resource
    WebServiceContext context;

    /**
     * Retorna el listado de ordenes de un laboratorio
     *
     * @param fechaDesde
     * @param fechaHasta
     * @return
     */
    @WebMethod
    @WebResult(name = "listaOrdenes")
    public Respuesta consultaOrdenes(@WebParam(name = "fechaDesde") @XmlElement(required = true) String fechaDesde, @WebParam(name = "fechaHasta") @XmlElement(required = true) String fechaHasta) {
        MessageContext messageContext = this.context.getMessageContext();
        Map httpHeaders = (Map) messageContext.get("javax.xml.ws.http.request.headers");

        //Se obtienen credenciales del header
        List listaUsuario = (List) httpHeaders.get("usuario");
        List listaContrasena = (List) httpHeaders.get("contrasena");
        List listaToken = (List) httpHeaders.get("token");

        Respuesta respuesta = new Respuesta();
        ValidarCamposWebService validar = new ValidarCamposWebService();

        //Se valida que no esten vacias las credenciales
        String error = validar.validarUsuario(listaUsuario, listaContrasena, listaToken);

        if (!error.isEmpty()) {
            respuesta.setError(error);
            return respuesta;
        }

        //Se validan el formato de fechas
        error = validar.validarFechas(fechaDesde, fechaHasta);
        if (!error.isEmpty()) {
            respuesta.setError(error);
            return respuesta;
        }

        boolean fechas = false;
        if (fechaDesde == null && fechaHasta == null) {
            fechas = true;
        } else if (fechaDesde.isEmpty() && fechaHasta.isEmpty()) {
            fechas = true;
        }
        if (fechas) {

            fechaDesde = "01/01/2020";
            fechaHasta = (new SimpleDateFormat("dd/MM/yyyy")).format(new Date());
        }

        Des des = new Des();
        List<Orden> listaOrdenes = new ArrayList<>();

        String usuario = listaUsuario.get(0).toString().trim();
        String contrasena = listaContrasena.get(0).toString().trim();
        String token = listaToken.get(0).toString().trim();
        String idEmpresa = "";
        boolean continuar = true;

        Connection con = null;
        ConexionLaboratorio conexion = null;
        PreparedStatement ps = null, ps2 = null;
        ResultSet rs = null, rs2 = null;

        try {
            //Se crea una nueva conexión a la BD
            conexion = new ConexionLaboratorio();
            con = conexion.getConnection();

            //Se prepara el query para verificar las credenciales
            String query = conexion.consultarQuery(282, ps, rs);

            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            ps.setString(2, des.encriptar(contrasena));
            ps.setString(3, token);

            System.out.println(ps.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                idEmpresa = rs.getString("id_empresa");
            }

            //Si el id_empresa es vacio fallo el login
            if (idEmpresa.isEmpty()) {
                respuesta.setError(validar.ERROR23);
                continuar = false;
            }

            if (continuar) {

                //Se prepara el query para consultar los folios
                query = conexion.consultarQuery(281, ps, rs);

                //Se prepara el query para consultar los examenes
                String queryOrdenes = conexion.consultarQuery(283, ps, rs);

                ps = con.prepareStatement(query);
                ps.setString(1, idEmpresa);
                ps.setString(2, fechaDesde);
                ps.setString(3, fechaHasta);

                System.out.println(ps.toString());
                rs = ps.executeQuery();

                while (rs.next()) {

                    Orden o = new Orden();
                    OrdenDatoGeneral dg = new OrdenDatoGeneral();

                    //String idEvolucionDetalle = rs.getString("id_evolucion_detalle");
                    String idEvolucion = rs.getString("id_evolucion");

                    dg.setHistoriaClinica(rs.getString("id_evolucion"));
                    dg.setFechaOrdenado(rs.getString("fecha_finalizado"));
                    dg.setDocumentoMedico(rs.getString("id_medico"));
                    dg.setNombreMedico(rs.getString("medico"));
                    dg.setCodigoCosto(rs.getString("id_centro_costo"));
                    dg.setNombreCosto(rs.getString("centro_costo"));
                    dg.setCodigoSede(rs.getString("id_sede"));
                    dg.setCodigoCliente(rs.getString("codigo_cliente"));
                    dg.setNombreCliente(rs.getString("cliente"));
                    dg.setCama(rs.getString("cama"));
                    dg.setCodigoProgramaSalud(rs.getString("id_programa_salud"));
                    dg.setNombreProgramaSalud(rs.getString("programa_salud"));
                    dg.setObservaciones(rs.getString("observaciones"));
                    dg.setCodigoPlanFacturacion(rs.getString("id_plan_facturacion"));
                    dg.setNombrePlanFacturacion(rs.getString("plan_facturacion"));

                    PacienteLaboratorio p = new PacienteLaboratorio();
                    p.setCodigoAfiliacion(rs.getString("id_afiliacion"));
                    p.setCodigoRegimen(rs.getString("id_regimen"));
                    p.setTipoDocumento(rs.getString("tipo_identificacion"));
                    p.setDocumento(rs.getString("identificacion"));
                    p.setNombre1(rs.getString("nombre1"));
                    p.setNombre2(rs.getString("nombre2"));
                    p.setApellido1(rs.getString("apellido1"));
                    p.setApellido2(rs.getString("apellido2"));
                    p.setSexo(rs.getString("id_sexo"));
                    p.setFechaNacimiento(rs.getString("fecha_nacimiento"));
                    p.setEnEmbarazo(rs.getString("embarazo"));
                    p.setCodigoMunicipio(rs.getString("id_municipio"));
                    p.setCodigoZona(rs.getString("id_zona"));
                    p.setDireccion(rs.getString("direccion"));
                    p.setTelefono(rs.getString("telefono"));
                    p.setCorreo(rs.getString("email"));
                    p.setCaracteristicasMedicas(rs.getString("caracteristicas_medicas"));

                    o.setPaciente(p);
                    o.setDatoGeneral(dg);

                    ps2 = con.prepareStatement(queryOrdenes);
                    ps2.setString(1, idEvolucion);

                    System.out.println(ps2.toString());
                    rs2 = ps2.executeQuery();

                    while (rs2.next()) {
                        Examen e = new Examen();
                        e.setNumeroOrden(rs2.getString("id_orden"));
                        e.setCups(rs2.getString("cups"));
                        e.setNombre(rs2.getString("nombre_examen"));
                        e.setCodigoDiagnostico(rs2.getString("id_diagnostico"));
                        e.setDiagnostico(rs2.getString("diagnostico"));
                        e.setNumeroAutorizacion(rs2.getString("numero_autorizacion"));
                        e.setCodigoNecesidad(rs2.getString("id_grado_necesidad"));

                        o.agregarExamen(e);
                    }

                    listaOrdenes.add(o);
                }

                respuesta.setListaOrdenes(listaOrdenes);
            }

        } catch (SQLException e) {
            System.out.println("Error al ejecutar querys: " + e.getMessage());
            respuesta.setError(validar.ERROR24);
        } finally {
            //Se cierra la conexión a la BD
            try {
                if (rs != null) {
                    rs.close();
                }

                if (rs2 != null) {
                    rs2.close();
                }

                if (ps != null) {
                    ps.close();
                }

                if (ps2 != null) {
                    ps2.close();
                }

                if (con != null) {
                    con.close();
                }

                if (conexion != null) {
                    conexion.cerrarConexion();
                }
            } catch (SQLException e) {
                System.out.println("Error al ejecutar querys: " + e.getMessage());
                respuesta.setError(validar.ERROR24);
            }
        }

        return respuesta;
    }

    /**
     * Actualiza el estado de las ordenes a tomadas
     *
     * @param ListaOrdenes
     * @return
     */
    @WebMethod
    @WebResult(name = "respuesta")
    public Respuesta envioOrdenesTomadas(@WebParam(name = "orden") @XmlElement(required = true) List<OrdenTomada> ListaOrdenes) {
        MessageContext messageContext = this.context.getMessageContext();
        Map httpHeaders = (Map) messageContext.get("javax.xml.ws.http.request.headers");

        //Se obtienen credenciales del header
        List listaUsuario = (List) httpHeaders.get("usuario");
        List listaContrasena = (List) httpHeaders.get("contrasena");
        List listaToken = (List) httpHeaders.get("token");

        Respuesta respuesta = new Respuesta();
        ValidarCamposWebService validar = new ValidarCamposWebService();

        //Se valida que no esten vacias las credenciales
        String error = validar.validarUsuario(listaUsuario, listaContrasena, listaToken);

        if (!error.isEmpty()) {
            respuesta.setError(error);
            return respuesta;
        }

        //Se valida las ordenes enviadas
        error = validar.validarOrdenes(ListaOrdenes);
        if (!error.isEmpty()) {
            respuesta.setError(error);
            return respuesta;
        }

        Des des = new Des();

        String usuario = listaUsuario.get(0).toString().trim();
        String contrasena = listaContrasena.get(0).toString().trim();
        String token = listaToken.get(0).toString().trim();
        String idEmpresa = "";
        boolean continuar = true;

        Connection con = null;
        ConexionLaboratorio conexion = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            //Se crea una nueva conexión a la BD
            conexion = new ConexionLaboratorio();
            con = conexion.getConnection();

            //Se prepara el query para verificar las credenciales
            String query = conexion.consultarQuery(282, ps, rs);

            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            ps.setString(2, des.encriptar(contrasena));
            ps.setString(3, token);

            System.out.println(ps.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                idEmpresa = rs.getString("id_empresa");
            }

            //Si el id_empresa es vacio fallo el login
            if (idEmpresa.isEmpty()) {
                respuesta.setError(validar.ERROR23);
                continuar = false;
            }

            if (continuar) {

                //Se prepara el query que actualiza el estado de la orden
                query = conexion.consultarQuery(284, ps, rs);

                String ordenes = "";
                int editadas = 0;

                for (OrdenTomada orden : ListaOrdenes) {
                    ps = con.prepareStatement(query);
                    ps.setString(1, usuario);
                    ps.setString(2, orden.getNumero());
                    ps.setString(3, idEmpresa);

                    System.out.println(ps.toString());

                    //Si la orden fue actualizada se aumenta un contador
                    int row = ps.executeUpdate();
                    if (row > 0) {
                        ordenes = ordenes + orden.getNumero() + ",";
                        editadas += row;
                    }
                }

                //Si editadas es igual a cero, ninguna orden se actualizo
                if (editadas > 0) {
                    ordenes = ordenes.substring(0, ordenes.length() - 1);
                    con.commit();
                    respuesta.setExito("Las siguientes ordenes fueron recibidas: " + ordenes);
                } else {
                    respuesta.setError(validar.ERROR37);
                }
            }
        } catch (SQLException e) {
            System.out.println("Error al ejecutar querys: " + e.getMessage());
            respuesta.setError(validar.ERROR24);
        } finally {
            //Se cierra la conexión a la BD
            try {
                if (rs != null) {
                    rs.close();
                }

                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }

                if (conexion != null) {
                    conexion.cerrarConexion();
                }
            } catch (SQLException e) {
                System.out.println("Error al ejecutar querys: " + e.getMessage());
                Objects.requireNonNull(validar);
                respuesta.setError(validar.ERROR24);
            }
        }

        return respuesta;
    }

    /**
     * Recibe los resultado de una orden
     *
     * @param laboratorio
     * @return
     */
    @WebMethod
    @WebResult(name = "respuesta")
    public Respuesta envioResultados(@WebParam(name = "resultado") @XmlElement(required = true) Laboratorio laboratorio) {
        MessageContext messageContext = this.context.getMessageContext();
        Map httpHeaders = (Map) messageContext.get("javax.xml.ws.http.request.headers");

        //Se obtienen credenciales del header
        List listaUsuario = (List) httpHeaders.get("usuario");
        List listaContrasena = (List) httpHeaders.get("contrasena");
        List listaToken = (List) httpHeaders.get("token");

        Respuesta respuesta = new Respuesta();
        ValidarCamposWebService validar = new ValidarCamposWebService();

        //Se valida que no esten vacias las credenciales
        String error = validar.validarUsuario(listaUsuario, listaContrasena, listaToken);

        if (!error.isEmpty()) {
            respuesta.setError(error);
            return respuesta;
        }

        //Se valida los resultados enviados
        error = validar.ValidarResultados(laboratorio);
        if (!error.isEmpty()) {
            respuesta.setError(error);
            return respuesta;
        }

        Des des = new Des();

        String usuario = listaUsuario.get(0).toString().trim();
        String contrasena = listaContrasena.get(0).toString().trim();
        String token = listaToken.get(0).toString().trim();
        String idEmpresa = "", mensaje = "";
        boolean continuar = true;

        Connection con = null;
        ConexionLaboratorio conexion = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            //Se crea una nueva conexión a la BD
            conexion = new ConexionLaboratorio();
            con = conexion.getConnection();

            //Se prepara el query para verificar las credenciales
            String query = conexion.consultarQuery(282, ps, rs);

            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            ps.setString(2, des.encriptar(contrasena));
            ps.setString(3, token);

            System.out.println(ps.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                idEmpresa = rs.getString("id_empresa");
            }

            //Si el id_empresa es vacio fallo el login
            if (idEmpresa.isEmpty()) {
                respuesta.setError(validar.ERROR23);
                continuar = false;
            }

            if (continuar) {

                //Se prepara el query que valida si los resultados pertenecen
                // a esa orden
                query = conexion.consultarQuery(285, ps, rs);
                String codigos = "";

                ps = con.prepareStatement(query);
                ps.setString(1, laboratorio.getDocumento());
                ps.setString(2, laboratorio.getTipoDocumento());
                ps.setString(3, laboratorio.getCups());
                ps.setString(4, laboratorio.getDocumentoValida());
                ps.setString(5, laboratorio.getNumeroOrden());
                ps.setString(6, idEmpresa);
                ps.setString(7, laboratorio.getNumeroOrden());
                ps.setString(8, laboratorio.getDocumento());
                ps.setString(9, laboratorio.getTipoDocumento());
                ps.setString(10, laboratorio.getNumeroOrden());
                ps.setString(11, laboratorio.getCups());
                ps.setString(12, laboratorio.getNumeroOrden());

                System.out.println(ps.toString());
                rs = ps.executeQuery();

                while (rs.next()) {
                    mensaje = rs.getString("mensaje");
                }

                if (!mensaje.isEmpty()) {
                    respuesta.setError(mensaje);
                    continuar = false;
                }

                if (continuar) {

                    //Se prepara el query que valida que los analitos existan
                    query = conexion.consultarQuery(291, ps, rs);

                    List<Analito> listaAnalitos = laboratorio.getListaAnalitos();

                    for (int i = 0; i < listaAnalitos.size(); i++) {
                        Analito analito = (Analito) listaAnalitos.get(i);

                        ps = con.prepareStatement(query);
                        ps.setString(1, laboratorio.getNumeroOrden());
                        ps.setString(2, analito.getCodigo());

                        System.out.println(ps.toString());
                        rs = ps.executeQuery();

                        boolean encontro = false;

                        while (rs.next()) {
                            analito.setOrden(rs.getString("orden"));
                            encontro = true;
                        }

                        if (!encontro) {
                            codigos = codigos + analito.getCodigo() + ",";
                        } else {
                            laboratorio.getListaAnalitos().set(i, analito);
                        }
                    }

                    if (!codigos.isEmpty()) {
                        respuesta.setError("Los siguientes codigos de analitos no fueron encontrados en el sistema: " + codigos + " informe al administrador.");

                        continuar = false;
                    }

                    if (continuar) {

                        //Se prepara el query que inserta los resultados
                        query = conexion.consultarQuery(292, ps, rs);

                        int insertadas = 0;

                        for (Analito analito : laboratorio.getListaAnalitos()) {

                            ps = con.prepareStatement(query);
                            ps.setString(1, analito.getCodigo());
                            ps.setString(2, analito.getNombre());
                            ps.setString(3, analito.getResultado());
                            ps.setString(4, analito.getValorMinimo());
                            ps.setString(5, analito.getValorMaximo());
                            ps.setString(6, analito.getUnidades());
                            ps.setString(7, analito.getValorReferencia());
                            ps.setString(8, analito.getOrden());
                            ps.setString(9, laboratorio.getNumeroOrden());

                            System.out.println(ps.toString());

                            //Se guarda en una variable cuantos registros se insertaron
                            int row = ps.executeUpdate();
                            if (row > 0) {
                                insertadas += row;
                            }
                        }

                        //Si el numero insertado es igual al numero enviado
                        //se actualiza la orden a realizado
                        if (insertadas == laboratorio.getListaAnalitos().size()) {

                            int editadas = 0;

                            query = conexion.consultarQuery(293, ps, rs);
                            ps = con.prepareStatement(query);
                            ps.setString(1, laboratorio.getFecha());
                            ps.setString(2, laboratorio.getResultado4505());
                            ps.setString(3, laboratorio.getTecnica());
                            ps.setString(4, usuario);
                            ps.setString(5, laboratorio.getDocumentoValida());
                            ps.setString(6, laboratorio.getNumeroOrden());

                            System.out.println(ps.toString());

                            int row = ps.executeUpdate();
                            if (row > 0) {
                                editadas += row;
                            }

                            if (editadas > 0) {
                                con.commit();
                                respuesta.setExito("Resultado de la orden " + laboratorio.getNumeroOrden() + " recibido con éxito.");
                            } else {
                                respuesta.setError(validar.ERROR24);
                            }
                        } else {
                            respuesta.setError(validar.ERROR24);
                        }

                    }

                }

            }
        } catch (SQLException e) {
            System.out.println("Error al ejecutar querys: " + e.getMessage());
            respuesta.setError(validar.ERROR24);
        } finally {
            //Se cierra la conexión a la BD   
            try {
                if (rs != null) {
                    rs.close();
                }

                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }

                if (conexion != null) {
                    conexion.cerrarConexion();
                }
            } catch (SQLException e) {
                System.out.println("Error al ejecutar querys: " + e.getMessage());
                respuesta.setError(validar.ERROR24);
            }
        }

        return respuesta;
    }
}

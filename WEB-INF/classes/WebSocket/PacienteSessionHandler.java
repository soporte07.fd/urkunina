
package WebSocket;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.json.spi.JsonProvider;
import javax.websocket.Session;



@ApplicationScoped
public class PacienteSessionHandler {

    public static final String AGREGAR = "agregarTurno";
    public static final String ELIMINAR = "eliminarTurno";
    public static final String LLAMAR = "llamarTurno";
    public static final String MODIFICAR = "modificarTurno";

    private final Set<Session> sesiones = new HashSet<Session>();
    private final Set<PacienteSocket> pacientes = new HashSet<PacienteSocket>();

    public void agregarSesion(Session session) {

        sesiones.add(session);

        //Manda todos los pacientes a la nueva sesión agregada.
        for (PacienteSocket paciente : pacientes) {
            JsonObject mensaje = crearJson(paciente,AGREGAR);
            enviarMensajeCliente(session, mensaje);
        }
    }

    public void eliminarSesion(Session session) {

        sesiones.remove(session);
    }

    public void agregarPaciente(PacienteSocket paciente) {

        pacientes.add(paciente);
        enviarParaTodos(crearJson(paciente,AGREGAR));
    }

    public void eliminarPaciente(String id){

        PacienteSocket paciente = buscarPaciente(id);
        if (paciente!=null){
             pacientes.remove(paciente);
             enviarParaTodos(crearJson(paciente,ELIMINAR));
        }
    }

    public void llamarPaciente(String id){

        PacienteSocket paciente = buscarPaciente(id);
        if (paciente!=null){
             enviarParaTodos(crearJson(paciente,LLAMAR));
        }
    }

    public void modificarPaciente(String id){

       PacienteSocket paciente = buscarPaciente(id);
        if (paciente!=null){
             enviarParaTodos(crearJson(paciente,MODIFICAR));
        }
    }


    private PacienteSocket buscarPaciente(String id){

        for(PacienteSocket paciente : pacientes){
            if(paciente.getId().equals(id)){
                return paciente;
            }
        }
        return null;
    }


    private JsonObject crearJson(PacienteSocket paciente,String accion) {

        JsonProvider provider = JsonProvider.provider();

        JsonObject json = provider.createObjectBuilder()
                .add("accion", accion)
                .add("id", paciente.getId())
                .add("nombre", paciente.getNombre())
                .add("consultorio", paciente.getConsultorio())
                .add("estado", paciente.getEstado())
                .add("turno", paciente.getTurno())
                .build();

        return json;
    }

    private void enviarParaTodos(JsonObject mensaje) {

        for (Session session : sesiones) {
            enviarMensajeCliente(session, mensaje);
        }
    }

    private void enviarMensajeCliente(Session session, JsonObject mensaje) {

        try {
            session.getBasicRemote().sendText(mensaje.toString());
        } catch (IOException ex) {
            eliminarSesion(session);
            System.out.println("Error al enviar mensaje: " + ex.getMessage());
        }

    }

}